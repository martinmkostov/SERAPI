﻿using SERApp.Data.Models;
using SERApp.Models;

namespace SERApp.Service.Extensions
{
    public static class PremiseTypeExtensions
    {
        public static PremiseTypeModel ToModel(this PremiseType entity)
        {
            return new PremiseTypeModel
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description
            };
        }
    }
}