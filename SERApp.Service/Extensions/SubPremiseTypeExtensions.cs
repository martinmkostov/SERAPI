﻿using SERApp.Data.Models;
using SERApp.Models;

namespace SERApp.Service.Extensions
{
    public static class SubPremiseTypeExtensions
    {
        public static SubPremiseTypeModel ToModel(this SubPremiseType entity)
        {
            return new SubPremiseTypeModel
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description
            };
        }
    }
}