﻿using SERApp.Data.Models;
using SERApp.Models;

namespace SERApp.Service.Extensions
{
    public static class SiteExtensions
    {
        public static SiteModel ToModel(this Site entity)
        {
            entity = entity ?? new Site();
            return new SiteModel
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }
    }
}