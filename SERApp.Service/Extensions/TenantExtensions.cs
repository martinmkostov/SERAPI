﻿using SERApp.Data.Models;
using SERApp.Models;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Service.Extensions
{
    public static class TenantExtensions
    {
        public static TenantModel ToModel(this Tenant entity)
        {
            return new TenantModel
            {
                Id = entity.Id,
                Site = entity.Site.ToModel(),
                SiteId = entity.SiteId,
                SiteName = entity.Site?.Name,
                SiteManagerName = entity.SiteManagerName,
                SiteManagerMobilePhone = entity.SiteManagerPhone,
                SiteManagerPhone = entity.SiteManagerPhone,
                SiteManagerEmail = entity.SiteManagerEmail,
                ImageUrl = entity.Site?.CompanyLogo ?? string.Empty,
                Name = entity.Name,
                Email = entity.Email,
                EmailTemplate = entity.EmailTemplate,
                Image = entity.Image ?? string.Empty,
                Phone = entity.Phone,
                VisitingAddress = entity.VisitingAddress,
                UserName = entity.UserName,
                Password = entity.Password,
                InsuranceCompany = entity.InsuranceCompany,
                SecurityCompany = entity.SecurityCompany,
                ContractName = entity.ContractName,
                ContractVatNo = entity.ContractVatNo,
                InvoiceAddress = entity.InvoiceAddress,
                InvoiceZipAndCity = entity.InvoiceZipAndCity,
                ContactComments = entity.ContactComments,
                CCTV = entity.CCTV,
                IntrusionAlarm = entity.IntrusionAlarm,
                LastUpdatedDate = entity.LastUpdatedDate,
                CreatedDate = entity.CreatedDate,
                LastUpdatedByCustomer = entity.LastUpdatedByCustomer,
                LastSentUpdateEmail = entity.LastSentUpdateEmail,
                Flag = entity.Flag,
                IsActive = entity.IsActive,
                PropertyDesignation = entity.PropertyDesignation,
                UseWrittenStatement = entity.UseWrittenStatement,
                TenantType = entity.TenantType ?? 0,
                TenantTypeName = entity.TenantTypeEnt?.TypeName ?? "N / A",
                PermiseType = entity.PermiseType ?? 0,
                PremiseTypeName = entity.PremiseTypeEnt?.TypeName ?? "N / A",
                SubPremiseType = entity.SubPremiseType ?? 0,
                SubPremiseTypeName = entity.SubPremiseTypeEnt?.TypeName ?? "N / A",
                City = entity.City,
                ZipCode = entity.ZipCode,
                PostAddress = entity.PostAddress,
                PostCity = entity.PostCity,
                PostZipCode = entity.PostZipCode,
                LastUpdatedBy = entity.LastUpdatedBy,
                // LastUpdatedByName = 
            };
        }
    }
}