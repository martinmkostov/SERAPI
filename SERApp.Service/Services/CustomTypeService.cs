﻿
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ICustomTypeService
    {
        void AddCustomType(CustomTypeModel model);
        void UpdateCustomType(CustomTypeModel model);
        void DeleteCustomTypeById(int id);
        IEnumerable<CustomTypeModel> GetCustomTypes();
        CustomTypeModel GetCustomTypeById(int id);
    }

    public class CustomTypeService : ICustomTypeService
    {
        private IRepository<CustomType> _repository;
        public CustomTypeService()
        {
            _repository = new Repository<CustomType>();
        }

        public void AddCustomType(CustomTypeModel model)
        {
            if (model.Id == 0)
            {
                _repository.Save(new CustomType()
                {
                    CreatedDate = DateTime.Now,
                    Id = model.Id,
                    LastUpdatedDate = model.LastUpdatedDate,
                    Name = model.Name
                });

            }
            else
            {
                UpdateCustomType(model);
            }

        }

        public void UpdateCustomType(CustomTypeModel model)
        {
            var existingCustomType = _repository.Get(model.Id);
            existingCustomType.LastUpdatedDate = DateTime.Now;
            existingCustomType.Name = model.Name;
            
            _repository.Update(existingCustomType);
        }

        public void DeleteCustomTypeById(int id)
        {
            _repository.Delete(id);
        }

        public IEnumerable<CustomTypeModel> GetCustomTypes()
        {
            return _repository.GetAll().Select(x=> new CustomTypeModel()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name
            }).ToList();
        }

        public CustomTypeModel GetCustomTypeById(int id)
        {
            //use automapper for easy mapping
            var model = _repository.Get(id);
            
            return new CustomTypeModel()
            {
                CreatedDate = model.CreatedDate,
                LastUpdatedDate = model.LastUpdatedDate,
                Name = model.Name,
                Id = model.Id
            };
        }
    }
}
