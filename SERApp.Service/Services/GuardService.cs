﻿using MoreLinq;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
namespace SERApp.Service.Services
{
    public interface IGuardService
    {
        IEnumerable<GuardModel> GetGuards(int accountId, int SiteId);
        IEnumerable<GuardModel> GetAllGuardsPerDayAndAccount(string date = "", int accountId = 0, int siteId = 0);
        IEnumerable<GuardModel> GetAllGuardsByDayAccountAndSiteId(string date = "", int accountId = 0, int siteId = 0);
        bool DeleteGuardById(int id);
        bool AddGuard(GuardModel model);
        bool AddGuardEmployee(EmployeeModel model);
        GuardModel GetGuardById(int id);
        EmployeeModel GetGuardEmployeeById(int id);
        GuardModel GetByName(string name, int siteId);
    }
    public class GuardService : IGuardService
    {
        private IGuardRepository _repository;
        private IIncidentRepository _incidentRepository;
        private IRepository<Employee> _employeeRepository;
        private SiteRepository _siteRepository;
        
        //private readonly SERAppDBContext _dbContext;
        public GuardService()
        {
            _repository = new GuardRepository();
            //_dbContext = new SERAppDBContext();
            _incidentRepository = new IncidentRepository();
            _employeeRepository = new Repository<Employee>();
            _siteRepository = new SiteRepository();
        }


        public bool AddGuard(GuardModel model)
        {
            if (model.Id != 0)
            {
                var existingGuard = _repository.Get(model.Id);
                existingGuard.LastUpdatedDate = DateTime.Now;
                existingGuard.SiteId = model.SiteId;
                existingGuard.Name = model.Name;
                _repository.Update(existingGuard);
            }
            else
            {
                if(_repository.GetAll().Any(x=>x.Name == model.Name && x.SiteId == model.SiteId))
                {
                    return false;
                }

                _repository.Save(new Guard()
                {
                    CreatedDate = DateTime.Now,
                    Name = model.Name,
                    SiteId = model.SiteId
                });
            }

            return true;
        }

        public bool AddGuardEmployee(EmployeeModel model)
        {
            if (model.Id != 0)
            {
                var existingGuard = _employeeRepository.Get(model.Id);
                //existingGuard.LastUpdatedDate = DateTime.Now;
                existingGuard.SiteId = model.SiteId;
                existingGuard.Name = model.Name;
                _employeeRepository.Update(existingGuard);
            }
            else
            {
                if (_employeeRepository.GetAll().Any(x => x.SiteId == model.SiteId && x.Name == model.Name))
                {
                    return false;
                }

                _employeeRepository.Save(new Employee()
                {
                    //CreatedDate = DateTime.Now,
                    Name = model.Name,
                    SiteId = model.SiteId
                });
            }

            return true;
        }

        public bool DeleteGuardById(int id)
        {
            var empGuardName = _employeeRepository.Get(id).Name;

            if (_incidentRepository.GetAllIncidentsChildIncluded(0).Any(r => r.IncidentGuard.Any(z => z.Guard.Name == empGuardName)))
            {
                return false;
            }

            _employeeRepository.Delete(id);
            return true;
        }
        public GuardModel GetGuardById(int id)
        {
            var value = _repository.Get(id);
            var t = value.IncidentGuard;
            return new GuardModel()
            {
                CreatedDate = value.CreatedDate,
                Id = value.Id,
                IncidentGuard = value.IncidentGuard.Select(x => new IncidentGuardModel() { }).ToList(),
                LastUpdatedDate = value.LastUpdatedDate,
                Name = value.Name,
                SiteId = value.SiteId
            };
        }


        public GuardModel GetByName(string name, int siteId)
        {
            var value = _repository.GetByPredicate(r => r.Name == name && r.SiteId == siteId).FirstOrDefault();
            var t = value.IncidentGuard;
           return new GuardModel()
           {
               CreatedDate = value.CreatedDate,
               Id = value.Id,
               IncidentGuard = value.IncidentGuard.Select(x=>new IncidentGuardModel() { }).ToList(),
               LastUpdatedDate = value.LastUpdatedDate,
               Name = value.Name,              
               SiteId = value.SiteId
           };
        }

        public EmployeeModel GetGuardEmployeeById(int id)
        {
            var value = _employeeRepository.Get(id);
           // var t = value.IncidentGuard;
            return new EmployeeModel()
            {
             //   CreatedDate = value.CreatedDate,
                Id = value.Id,
               // IncidentGuard = value.IncidentGuard.Select(x => new IncidentGuardModel() { }).ToList(),
               // LastUpdatedDate = value.LastUpdatedDate,
                Name = value.Name,
                SiteId = value.SiteId
            };
        }

        public IEnumerable<GuardModel> GetAllGuardsPerDayAndAccount(string date = "", int accountId = 0, int siteId = 0)
        {
            DateTime.TryParse(date, out DateTime dateValue);

            var data = _repository.GetBySiteIdAndDate(siteId, dateValue);
            return data
                 .Select(x => new GuardModel()
                 {
                     CreatedDate = x.CreatedDate,
                     Id = x.Id,
                     LastUpdatedDate = x.LastUpdatedDate,
                     Name = x.Name,
                     SiteId = x.SiteId
                 })
                 .Distinct()
                 .ToList();
        }

        public IEnumerable<GuardModel> GetAllGuardsByDayAccountAndSiteId(string date = "", int accountId = 0, int siteId =0)
        {
            DateTime dateValue;
            DateTime.TryParse(date, out dateValue);

            //var guardIdsToday = _incidentRepository
            //    .GetByPredicate(x => x.Time.ToShortDateString()
            //.Equals(dateValue.ToShortDateString()))
            //.ToList()
            //.SelectMany(x => x.IncidentGuard)
            //.Select(x => x.GuardId).ToList();

            //var data = _repository.GetAll().Where(x => guardIdsToday.Contains(x.Id))
            //    .Where(x => x.Site.AccountId == accountId)
            //    .ToList();

            var data = new List<Guard>();

            return data
                 .Select(x => new GuardModel()
                 {
                     CreatedDate = x.CreatedDate,
                     Id = x.Id,
                     LastUpdatedDate = x.LastUpdatedDate,
                     Name = x.Name,
                     SiteId = x.SiteId
                 })
                 .DistinctBy(r=>r.Name)
                 .ToList();
        }

        public IEnumerable<GuardModel> GetGuards(int accountId,int SiteId)
        {
            var data = _repository.GetAllGuardsIncldeChild();

            if (SiteId != 0)
            {
                data = data.Where(x => x.SiteId == SiteId);
            }

            if (accountId != 0)
            {
                data = data.Where(x => x.Site.AccountId == accountId);
            }


            var returnModel = data.Select(x => new GuardModel()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name,
                SiteId = x.SiteId,
                Site = new SiteModel()
                {
                    Name = x.Site.Name,
                },
            }).ToList();

            return returnModel;
        }

        public IEnumerable<EmployeeModel> GetGuardEmployees(int accountId, int SiteId)
        {
            var data = _employeeRepository.GetAll();
         
            if (SiteId > 0)
            {
                data = data.Where(x => x.SiteId == SiteId);
               
            }

            //if (accountId > 0)
            //{
            //    data = data.Where(x => x.Site.AccountId == accountId);
            //    var test = data.ToList();
            //}

            
            return data.Select(x => new EmployeeModel()
            {
                Id = x.Id,
                Name = x.Name,
                SiteId =x.SiteId,
      
            }).ToList();
            
        }
    }
}
