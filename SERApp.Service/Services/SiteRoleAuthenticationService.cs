﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ISiteRoleAuthenticationService
    {
        
    }
    public class SiteRoleAuthenticationService : ISiteRoleAuthenticationService
    {

        private UserModuleRoleRepository _repository;
        private IRepository<Site> _siteRepository;
        private IRepository<Module> _moduleRepos;
        private IRepository<UserModuleRoles> _userModuleRoleRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<User> _userRepository;
        public SiteRoleAuthenticationService()
        {
            _repository = new UserModuleRoleRepository();
            _siteRepository = new Repository<Site>();
            _moduleRepos = new Repository<Module>();
            _userModuleRoleRepository = new Repository<UserModuleRoles>();
            _roleRepository = new Repository<Role>();
            _userRepository = new Repository<User>();
        }

        public RoleModel GetRole(int userId, int moduleId,int siteId) 
        {

            var isAnAccountAdmin = (_repository.GetUserRole(userId) != null ? _repository.GetUserRole(userId).Name : "") == "admin";

            if (!isAnAccountAdmin)
            {
                if (moduleId == 1 && siteId == -1)
                {
                    isAnAccountAdmin = _repository.GetByUsersId(userId).Where(x => x.ModuleId == 1 && x.RoleId == 1).Any();
                }
                else if (moduleId == 4 && siteId == -1) 
                {
                    isAnAccountAdmin = _userRepository.Get(userId).IsActivityAdmin;
                }
                else
                {
                    //var test = _repository.GetByUsersId(userId).Where(r => r.RoleId == 1 && r.SiteId == siteId && r.ModuleId == moduleId);
                    isAnAccountAdmin = _repository.GetByUsersId(userId).Any(r => r.RoleId == 1 && r.SiteId == siteId && r.ModuleId == moduleId);
                }
            }
            

            if (isAnAccountAdmin)
            {
                var data = new RoleModel();
                data.Description = "Admin";
                data.Id = 1;
                data.IsModule = true;
                data.IsUser = true;
                data.Name = "admin";

                return data;
            }


            if (_repository.GetAllBySite(siteId).SingleOrDefault(x => x.UserId == userId && x.ModuleId == moduleId) != null)
            {
                var role = _repository.GetAllBySite(siteId).SingleOrDefault(x => x.UserId == userId && x.ModuleId == moduleId).Role;
                if (role != null)
                {
                    var data = new RoleModel();
                    data.Description = role.Description;
                    data.Id = role.Id;
                    data.IsModule = role.IsModule;
                    data.IsUser = role.IsUser;
                    data.Name = role.Name;

                    return data;
                }
            }
             
            return null;
        }

        public List<UserModuleRoleModel> GetRolesByModule(int userId, int moduleId)
        {
            var data = new List<UserModuleRoleModel>();
            var roles = _repository.GetAllByModule(moduleId, userId);

            foreach (var role in roles)
            {
                data.Add(new UserModuleRoleModel
                {
                    UserId = userId,
                    ModuleId = moduleId,
                    RoleId = role.RoleId,
                    SiteId = role.SiteId
                });
            }
            return data;
        }

        public List<UserModuleRoleModel> GetUsers(int moduleId, int siteId)
        {
            var userModuleRoles = _userModuleRoleRepository.GetAllByRawString($"SELECT * FROM UserModuleRoles WHERE ModuleId = {moduleId} AND SiteId = {siteId} AND RoleId IN (1,2)");

            var data = userModuleRoles.Select(x => new UserModuleRoleModel()
            {
                UserId = x.UserId,
                ModuleId = x.ModuleId,
                RoleId = x.RoleId
            }).ToList();

            var userIds = string.Join(",", data.Select(r => r.UserId).ToList());
            var users = _userRepository.GetAllByRawString($"SELECT * FROM Users WHERE Id in ({userIds})");

            var ids = string.Join(",", data.Select(r=>r.RoleId).ToList());
            var roles = _roleRepository.GetAllByRawString($"SELECT * FROM Roles WHERE Id IN ({ids})");

            data.ForEach(x => 
            {
                var r = roles.SingleOrDefault(t => t.Id == x.RoleId);
                x.Role = new RoleModel()
                {
                    Name = r.Name,
                    Description = r.Description,
                    Id = r.Id,
                    IsUser = r.IsUser,
                    IsModule = r.IsModule
                };
                var u = users.SingleOrDefault(t=>t.Id == x.UserId);
                x.User = new UserModel() {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,                    
                };
            });

            return data;
        }
    }
}
