﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class ModuleHelpTemplateService
    {
        ModuleHelpTemplateRepository _moduleHelpTempRepository;
        public ModuleHelpTemplateService()
        {
            _moduleHelpTempRepository = new ModuleHelpTemplateRepository();
        }

        public ModuleHelpTemplateModel Get(int moduleId) {
            var data = _moduleHelpTempRepository.Get(moduleId);
            return data != null ? entityToModel(data) : new ModuleHelpTemplateModel();
        }

        public List<ModuleHelpTemplateModel> GetAll()
        {
            return _moduleHelpTempRepository.GetAll()
                .Select(m => new ModuleHelpTemplateModel
                {
                    Id = m.Id,
                    ModuleId = m.ModuleId,
                    Name = m.Name,
                    ContentValue = m.ContentValue
                }).ToList();
        }

        public void Save(ModuleHelpTemplateModel model) {
            _moduleHelpTempRepository.Save(model);
        }

        public void Delete(int id) {
            _moduleHelpTempRepository.Delete(id);
        }

        public ModuleHelpTemplateModel entityToModel(ModuleHelpTemplate entity) {
            return new ModuleHelpTemplateModel()
            {
                Id = entity.Id,
                ModuleId = entity.ModuleId,
                Name = entity.Name,
                ContentValue = entity.ContentValue
            };
        }
    }
}
