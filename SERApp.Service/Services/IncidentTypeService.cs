﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIncidentTypeService
    {
        IEnumerable<IncidentTypeModel> GetIncidentTypes();
        IncidentTypeModel GetIncidentType(int id);

        void DeleteIncidentTypeById(int id);
        void AddIncidentType(IncidentTypeModel model);
        IncidentTypeModel GetIncidentTypeById(int id);
        IEnumerable<ReportTypeModel> GetReportTypes();
    }
    public class IncidentTypeService : IIncidentTypeService
    {
        private IRepository<IncidentType> _repository;
        private IRepository<ReportType> _reportTypeRepository;
        public IncidentTypeService()
        {
            _repository = new Repository<IncidentType>();
            _reportTypeRepository = new Repository<ReportType>();
        }

        
        public void AddIncidentType(IncidentTypeModel model)
        {

            if (model.Id > 0)
            {
                var existingIncidentType = _repository.Get(model.Id);
                existingIncidentType.IsReportRequired = model.IsReportRequired;
                existingIncidentType.Name = model.Name;
                existingIncidentType.RequiredReport = String.Join(",", model.RequiredReport.Select(r => r.Id));
                existingIncidentType.ShownReport = String.Join(",", model.ShownReport.Select(r => r.Id));

                if (string.IsNullOrEmpty(existingIncidentType.RequiredReport))
                {
                    existingIncidentType.RequiredReport = "0";
                }

                if (string.IsNullOrEmpty(existingIncidentType.ShownReport))
                {
                    existingIncidentType.ShownReport = "0";
                }


                _repository.Update(existingIncidentType);
            }
            else
            {
                string shownReport = String.Join(",", model.ShownReport.Select(r => r.Id));
                string requiredReport = String.Join(",", model.RequiredReport.Select(r => r.Id));

                if (!model.ShownReport.Any())
                {
                    shownReport = "0";
                }

                if (!model.RequiredReport.Any())
                {
                    requiredReport = "0";
                }


                _repository.Save(new IncidentType()
                {
                    IsReportRequired = model.IsReportRequired,
                    ShownReport = shownReport,
                    RequiredReport = requiredReport,
                    Name = model.Name                    
                });
            }
        }

        public void DeleteIncidentTypeById(int id)
        {
            _repository.Delete(id);
        }

        public IncidentTypeModel GetIncidentType(int id)
        {
            var existingData = _repository.Get(id);
            var data = new IncidentTypeModel()
            {
                Id = existingData.Id,
                IsReportRequired = existingData.IsReportRequired,
                Name = existingData.Name,               
            };


            var requiredReportsIds = existingData.RequiredReport.Split(',').Select(x=>Convert.ToInt32(x)).ToList();
            var shownReportsIds = existingData.ShownReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();

            data.RequiredReport = _reportTypeRepository.GetByPredicate(x => requiredReportsIds.Contains(x.Id))
            .Select(x => new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();

            data.ShownReport = _reportTypeRepository.GetByPredicate(x => shownReportsIds.Contains(x.Id))
            .Select(x => new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();
     
            return data;
        }

        public IncidentTypeModel GetIncidentTypeById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IncidentTypeModel> GetIncidentTypes()
        {
            var reportTypes =_reportTypeRepository.GetAll().ToList();

            var data = _repository.GetAll().ToList();

            var returnModel = new List<IncidentTypeModel>();
            data.ForEach(t => 
            {
                var requiredReportsIds = t.RequiredReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                var shownReportsIds = t.ShownReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();

                returnModel.Add(new IncidentTypeModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                    IsReportRequired = t.IsReportRequired,
                    RequiredReport = reportTypes.Where(x => requiredReportsIds.Contains(x.Id))
                    .Select(x => new ReportTypeModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }).ToList(),
                    ShownReport = reportTypes.Where(x => shownReportsIds.Contains(x.Id))
                    .Select(x => new ReportTypeModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }).ToList(),
                });
            });


            return returnModel;
            //.Select(x => new IncidentTypeModel()
            //{
            //    Id = x.Id,
            //    IsReportRequired = x.IsReportRequired,
            //    Name = x.Name,               
            //}).ToList();
        }

        public IEnumerable<ReportTypeModel> GetReportTypes()
        {
            return _reportTypeRepository.GetAll().Select(x=> new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        
    }
}
