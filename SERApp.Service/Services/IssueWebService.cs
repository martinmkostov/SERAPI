﻿using MoreLinq;
using OfficeOpenXml;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueWebService
    {
        IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId =0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null);
        IssueWebModel SaveIssueWeb(IssueWebModel model, bool isConfirm = false);
        void SaveIssueWebLog(IssueWebHistoryModel model);
        void SendDetailsModel(SendDetailsModel model);
        void CloseIssue(IssueWebHistoryModel model);
        void ReOpenIssue(IssueWebModel model);
        void DeleteIssueWeb(int id);
        IssueWebModel GetById(int id);
        string GetTimezoneSetting(int accountId);
        IssueWebModel GetByGuid(Guid guid);
        byte[] ExportXLS(int accountId, int siteId = 0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null);
        void sendReminder(IssueWebModel model);
        SettingModel GetIssueSettingByAccountId(int accountId, string settingName, string defaultValue = "1");
        IEnumerable<IssueWebHistoryModel> GetIssueWebLog(int issueWebId, bool showHidden = false);
        IssuesDropdownModel GetIssueDropdowns(int accountId, int siteId);
    }

    public class IssueWebService : IIssueWebService
    {
        private int defaulSettingtInterval = 1;

        private readonly IIssueWebRepository _repository;
        private readonly AccountService _accountService;
        private readonly IIssueRecipientRepository _recipientRepository;
        private readonly EmailRepository _emailRepository;
        private readonly IIssueSettingsRepository _settingsRepository;
        private readonly IRepository<Issue_IssueType3> _issueIssueType3Repository;
        private readonly IRepository<Setting> _settingRepository;
        private readonly IRepository<ConfigSetting> _configSettingRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        private readonly IRepository<IssueType1> _issueType1Repository;
        private readonly IRepository<IssueType2> _issueType2Repository;
        private readonly IRepository<Issue_IssueType3> _issueType3Repository;
        private readonly IRepository<IssueWeb> _issueWebRepository;
        private readonly IRepository<IssueWebHistory> _issueWebHistoryRepository;
        private readonly IRepository<Account> _accountRepository;
        // private readonly IRepository<Contact> _contactRepository;
        private readonly IRepository<Site> _siteRepository;
        private SettingRepository _settingRepositoryData;
        public IssueWebService()
        {
            _settingRepositoryData = new SettingRepository();
            _issueWebRepository = new Repository<IssueWeb>();
            _issueWebHistoryRepository = new Repository<IssueWebHistory>();
            _repository = new IssueWebRepository();
            _accountService = new AccountService();
            _recipientRepository = new IssueRecipientRepository();
            _emailRepository = new EmailRepository();
            _settingsRepository = new IssueSettingsRepository();
            _issueIssueType3Repository = new Repository<Issue_IssueType3>();
            _settingRepository = new Repository<Setting>();
            _configSettingRepository = new Repository<ConfigSetting>();
            _tenantRepository = new Repository<Tenant>();
            _issueType1Repository = new Repository<IssueType1>();
            _issueType2Repository = new Repository<IssueType2>();
            _issueType3Repository = new Repository<Issue_IssueType3>();
            _siteRepository = new Repository<Site>();
            _accountRepository = new Repository<Account>();
        }

        public SettingModel GetIssueSettingByAccountId(int accountId, string settingName, string defaultValue = "1")
        {
            var configSetting = _configSettingRepository.GetByPredicate(x => x.SettingName == settingName).SingleOrDefault();

            var data = _settingRepository.GetByPredicate(x=>x.AccountId == accountId && x.ConfigSettingId == configSetting.Id).SingleOrDefault();

            if (data == null)
            {
                //_settingsRepository.Save(new IssueSettings()
                //{
                //    AccountId = accountId,
                //    StatusInterval = defaulSettingtInterval
                //});

                //data = _settingsRepository.GetByAccountId(accountId);

                _settingRepository.Save(new Setting()
                {
                    Name = settingName,
                    Value = configSetting.DefaultValue,
                    ConfigSettingId = configSetting.Id,
                    AccountId = accountId,                    
                });

                data = _settingRepository.GetByPredicate(x => x.AccountId == accountId && x.ConfigSettingId == configSetting.Id).SingleOrDefault();

            }

            var completeData = data;
            return new SettingModel()
            {
                AccountId = completeData.AccountId,
                ConfigSettingId = completeData.ConfigSettingId,
                SettingId = completeData.SettingId,
                Name = completeData.Name,
                Value = completeData.Value
            };
        }

        public void DeleteIssueWeb(int id)
        {
            _repository.Delete(id);
        }

        public string GetTimezoneSetting(int accountId)
        {
            var timezonetext = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {accountId}").SingleOrDefault().Value;
            return timezonetext;
        }

        public IssueWebModel GetById(int id)
        {
            var data = _repository.GetInlcudingChildrenId(id);
            if (data.TenantId != 0)
            {
                data.Tenant = _tenantRepository.Get(data.TenantId);
            }

            data.IssueWebHistories = _repository.GetIssueWebHistoryByIssueWebId(data.IssueWebId).OrderByDescending(x=>x.Date).ToList();

            var SigBase64 = "";
            if (!string.IsNullOrEmpty(data.Image) && data.Image.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase64 = $"{data.Image.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }

            var SigBase642 = "";
            if (!string.IsNullOrEmpty(data.Image2) && data.Image2.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image2.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase642 = $"{data.Image2.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }

            var SigBase643 = "";
            if (!string.IsNullOrEmpty(data.Image3) && data.Image3.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image3.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase643 = $"{data.Image3.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }

            var SigBase644 = "";
            if (!string.IsNullOrEmpty(data.Image4) && data.Image4.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image4.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase644 = $"{data.Image4.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }

            var SigBase645 = "";
            if (!string.IsNullOrEmpty(data.Image5) && data.Image5.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image5.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase645 = $"{data.Image5.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }



            //var timezone = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {data.AccountId}").SingleOrDefault().Value;

            var timezonetext = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {data.AccountId}").SingleOrDefault().Value;

            var timezone = Convert.ToInt32(timezonetext.Split('(')[1].Split(')')[0].Replace("UTC", "").Split(':')[0]);

            data.DateCreated = data.DateCreated.AddHours(Convert.ToInt32(timezone));

            return new IssueWebModel()
            {
                Timezone = timezonetext,
                IssueWebId = data.IssueWebId,
                Date = new DateModel()
                {
                    day = data.DateCreated.Day,
                    month = data.DateCreated.Month,
                    year = data.DateCreated.Year
                },
                Time = new TimeModel()
                {
                    minute = data.DateCreated.Minute,
                    hour = data.DateCreated.Hour,
                    second = data.DateCreated.Second
                },
                Guid = data.Guid,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Image2 = data.Image2,
                Image3 = data.Image3,
                Image4 = data.Image4,
                Image5 = data.Image5,
                ImageThumb = SigBase64,
                ImageThumb2 = SigBase642,
                ImageThumb3 = SigBase643,
                ImageThumb4 = SigBase644,
                ImageThumb5 = SigBase645,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                IssueNumber =data.IssueNumber,
                IsPublic = data.IsPublic,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = data.Tenant != null? new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                }: new TenantModel(),
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                ReminderEmailSentCount = data.ReminderEmailSentCount,
                IssueType3 = data.Issue_IssueType3.Select(t=> new Issue_IssueType3Model()
                {
                    Id = t.Id,
                    IssueType3Id = t.IssueType3Id,
                    IssueWebId = t.IssueWebId,
                    Value = t.Value
                }).ToList(),
                IssueWebHistories = data.IssueWebHistories != null || data.IssueWebHistories.Any() ? data.IssueWebHistories.Select(x => new IssueWebHistoryModel()
                {
                    IssueWebHistoryId = x.IssueWebHistoryId,
                    Date = new SERApp.Models.Common.DateModel() {
                        day = x.Date.Day,
                        month = x.Date.Month,
                        year = x.Date.Year,
                    },
                    Time = new SERApp.Models.Common.TimeModel() {
                        hour = x.Date.Hour,
                        minute = x.Date.Minute,
                        second = x.Date.Second
                    },
                    DateTime = x.Date,
                    //Description = x.Description,
                    DescriptionExternal = x.DescriptionExternal,
                    DescriptionInternal = x.DescriptionInternal,
                    IsDamage = x.IsDamage,
                    IsRent = x.IsRent,
                    IsWarranty = x.IsWarranty,
                    MaterialCost = x.MaterialCost,
                    Minutes = x.Minutes,
                    ReactedBy =x.ReactedBy,
                    IssueWebId = x.IssueWebId,
                    IssueWebHistoryType = x.IssueWebHistoryType
                }).ToList() : new List<IssueWebHistoryModel>(),
                
            };
        }

      

        public void ReloadIssueNumbersTemp()
        {
            var data = _repository.GetAll();

            var groupings = data.GroupBy(x => x.AccountId);

            groupings.ToList().ForEach(x => 
            {
                int id = 0;
                x.OrderBy(r=>r.IssueWebId).ToList().ForEach(y => 
                {
                    y.IssueNumber = id + 1;

                    _repository.Update(y);

                    id++;
                });
            });

        }

        public IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId= 0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null)
        {
            var fsDate = new DateTime(fromDate.year,fromDate.month,fromDate.day);
            var tsDate = new DateTime(toDate.year, toDate.month, toDate.day).AddDays(1);
            var dataList = _repository.GetAllIncludingChildren(accountId, siteId, fsDate, tsDate, statusId,contactId, issueType1Id, issueType2Id, issueType3Id);

            //if (fromDate != null && toDate != null)
            //{
            //    var fDate = new DateTime(fromDate.year, fromDate.month, fromDate.day);
            //    var tDate = new DateTime(toDate.year, toDate.month, toDate.day);

            //    dataList = dataList.Where(x => x.DateCreated >= fDate && x.DateCreated <= tDate).ToList();
            //}

            //if (siteId != 0)
            //{
            //    dataList = dataList.Where(x => x.SiteId == siteId).ToList();
            //}

            //if (statusId != -1)
            //{
            //    if (statusId == 0)
            //    {
            //        dataList = dataList.Where(x => x.Status == 0 || x.Status == 1).ToList();
            //    }
            //    else
            //    {
            //        dataList = dataList.Where(x => x.Status == statusId).ToList();
            //    }

                
            //}

            //if (contactId != 0)
            //{
            //    dataList = dataList.Where(x => x.TenantId == contactId).ToList();
            //}


            //if (issueType1Id != 0)
            //{
            //    dataList = dataList.Where(x => x.IssueType1Id == issueType1Id).ToList();
            //}
            //if (issueType2Id != 0)
            //{
            //    dataList = dataList.Where(x => x.IssueType2Id == issueType2Id).ToList();
            //}

            if (!string.IsNullOrEmpty(searchText))
            {
                dataList = dataList.Where(x=>!string.IsNullOrEmpty(x.Description) && !string.IsNullOrEmpty(x.ContactName)).Where(x => x.Description.ToLower().Contains(searchText.ToLower()) || x.ContactName.ToLower().Contains(searchText.ToLower()) || x.IssueNumber.ToString().Contains(searchText)).ToList();
            }
            
            var toCheck = dataList.Where(x => x.Status == 0).ToList();
            var accountInterval = GetIssueSettingByAccountId(accountId, "IssueInterval");
            GetIssueSettingByAccountId(accountId, "SendToSupervisorLimit");
            var b = new System.Collections.Concurrent.ConcurrentBag<int>();
            Parallel.ForEach(toCheck, x => 
            {
                var dateMax = x.DateCreated.AddHours(Convert.ToInt32(accountInterval.Value));

                var dateNow = DateTime.Now;

                if (dateMax < dateNow)
                {
                    b.Add(x.IssueWebId);
                    x.Status = 1;
                    //_repository.Update(x);
                }
            });

            if (b.Any())
            {
                _repository.UpdateStatus(b.ToList());
            }

            //b.ToList();           
            var timezonetext = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {accountId}").SingleOrDefault().Value;

            var timezone = Convert.ToInt32(timezonetext.Split('(')[1].Split(')')[0].Replace("UTC", "").Split(':')[0]);


            return
                dataList.Select(data => new IssueWebModel()
            {
                 Guid = data.Guid,
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated.AddHours(timezone),
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                IsPublic = data.IsPublic,
                Status = (IssueStatusEnum)data.Status,
                IssueNumber = data.IssueNumber,
                Site = data.Site != null? new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                }: new SiteModel(),
                Tenant = data.Tenant != null? new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                }: new TenantModel(),
                IssueType1 = data.IssueType1 != null ?new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                }: new IssueType1Model(),
                IssueType2 = data.IssueType2!=null?new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                }: new IssueType2Model(),
                    ReminderEmailSentCount = data.ReminderEmailSentCount
                }).OrderByDescending(x=>x.DateCreated);
        }

        public IssueWebModel SaveIssueWeb(IssueWebModel model, bool isConfirm = false)
        {
            SMS sms = new SMS();
            var data = new IssueWeb();
            if (model.IssueWebId == 0)
            {
                var lastIssueNumber = 0;

                var siteDetails = _siteRepository.Get(model.SiteId);

                if (model.AccountId == 0 && model.SiteId != 0)
                {
                    model.AccountId = siteDetails.AccountId;
                }

                if ((bool)model.IsPublic)
                {
                    var siteName = siteDetails.Name;
                    var notAllowedSites = new List<string>() { "Solna Centrum", "Mall Of Scandinavia",
                    "Nacka Forum", "Täby Centrum"};

                    if (notAllowedSites.Contains(siteName))
                    {
                        throw new Exception("Unable to save through this page.");
                    }
                }

                var ss = _repository.GetAllByRawString($"SELECT TOP 5 IssueWebId,DateCreated,DateChecked,DateFinished,AccountId,SiteId,TenantId,IssueType1Id,IssueType2Id,Email,Phone,ContactName,Address,Description,IsPolice,IsPublic,Status,ReminderEmailSentCount,ApartmentNumber,IssueNumber,Guid,null as Image,null as Image2,null as Image3,null as Image4,null as Image5 FROM IssueWeb WHERE AccountId = {model.AccountId} AND IssueType2Id IS NOT NULL ORDER BY IssueNumber Desc");
                //var ss = _repository.GetByPredicate(x => x.AccountId == model.AccountId);
                if (ss.Any())
                {
                    lastIssueNumber = ss.Max(x => x.IssueNumber);
                }

                var newIssueNumber = lastIssueNumber + 1;

                var timezonetext = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {model.AccountId}").SingleOrDefault().Value;

                var timezone = Convert.ToInt32(timezonetext.Split('(')[1].Split(')')[0].Replace("UTC", "").Split(':')[0]);

                if (model.IsDateSetFromUI)
                {
                    model.DateCreated = new DateTime(model.Date.year, model.Date.month, model.Date.day, model.Time.hour, model.Time.minute, model.Time.second).AddHours(-Convert.ToInt32(timezone));
                }
                else
                {
                    model.DateCreated = DateTime.UtcNow ;
                }
              
                data = new IssueWeb()
                {
                    
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Image2 = model.Image2,
                    Image3 = model.Image3,
                    Image4 = model.Image4,
                    Image5 = model.Image5,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateCreated = model.DateCreated,
                    Status = (int)IssueStatusEnum.NotConfirmed,
                    ReminderEmailSentCount = 0,
                    IssueNumber = newIssueNumber,
                    Guid = Guid.NewGuid(),
                    IsPublic = model.IsPublic,
                    Issue_IssueType3 = model.IssueType3!=null ? model.IssueType3.Select(x => new Issue_IssueType3()
                    {
                        IssueType3Id = x.IssueType3Id,
                        Value = x.Value,
                    }).ToList(): new List<Issue_IssueType3>()
                };

                _repository.Save(data);

               
            }
            else {
                var existingType3s = _issueIssueType3Repository.GetByPredicate(t => t.IssueWebId == model.IssueWebId).ToList();
                foreach (var s in existingType3s)
                {
                    _issueIssueType3Repository.Delete(s.Id);
                }

                model.IssueType3.ForEach(r =>
                {
                    _issueIssueType3Repository.Save(new Issue_IssueType3()
                    {
                        IssueType3Id = r.IssueType3Id,
                        IssueWebId = model.IssueWebId,
                        Value = r.Value,
                    });
                });


                var timezonetext = _settingRepository.GetAllByRawString($"SELECT * FROM Setting WHERE ConfigSettingId = 25 AND AccountId = {model.AccountId}").SingleOrDefault().Value;

                var timezone = Convert.ToInt32(timezonetext.Split('(')[1].Split(')')[0].Replace("UTC", "").Split(':')[0]);


                if (model.IsDateSetFromUI)
                {
                    model.DateCreated = new DateTime(model.Date.year, model.Date.month, model.Date.day, model.Time.hour, model.Time.minute, model.Time.second).AddHours(-Convert.ToInt32(timezone));
                }
                else
                {
                    model.DateCreated = DateTime.UtcNow;
                }
               
                data =new IssueWeb()
                {
                    Guid = model.Guid,
                    DateCreated = model.DateCreated,
                    IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Image2 = model.Image2,
                    Image3 = model.Image3,
                    Image4 = model.Image4,
                    Image5 = model.Image5,

                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    IsPublic = _repository.Get(model.IssueWebId).IsPublic,
                    DateChecked = model.DateChecked,
                    DateFinished = model.DateFinished,
                    IssueNumber = model.IssueNumber,
                    Status = (int)model.Status
                };

                _repository.Update(data);
            }

            if (data.TenantId != 0)
            {
                data = _repository.GetInlcudingChildrenId(data.IssueWebId);
            }
            else {
                data = _repository.GetInlcudingChildrenIdWithoutTenant(data.IssueWebId);
                data.Tenant = new Tenant()
                {
                    Name = "Lägenheter"
                };
            }



            if (!model.SaveOnlyData)
            {
                if (isConfirm)
                {
                    _repository.AddIssueWebHistory(new IssueWebHistory()
                    {
                        Date = DateTime.Now,
                        DescriptionExternal = "Technician confirmed the issue",
                        DescriptionInternal = "Technician confirmed the issue",
                        ReactedBy = "Technician",
                        IssueWebId = data.IssueWebId,
                        //to do add this to enum , confirm = 1, reminder =2
                        IssueWebHistoryType = "warning"
                    });

                    var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("ConfirmIssue");

                    var tenant = _tenantRepository.Get(data.TenantId);

                    if (string.IsNullOrEmpty(tenant.Email))
                    {
                        tenant.Email = "";
                    }

                

                    Service.Tools.Email.SendEmail(new EmailModel(model.AccountId)
                    {
                        From = "",
                        To = data.Email,
                        Subject = creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                        Body = creatorTemplate.Body

                    });


                    sms.SendSMS("", data.Phone, creatorTemplate.SMSBody).Wait();

                }
                else
                {

                    var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                    string AppUrl = ConfigurationManager.AppSettings["appurl"];

                    recipients.ForEach(recipient =>
                    {
                        var routineName = Regex.Replace(recipient.IssueRecipientType.Name, @"\s+", "");
                        var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                        if (!string.IsNullOrEmpty(recipient.Email))
                        {

                            Service.Tools.Email.SendEmail(new EmailModel(model.AccountId)
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                                Body = template.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                                .Replace("{object}", data.Site.Name)
                                .Replace("{tenant}", data.Tenant.Name)
                                .Replace("{type1}", data.IssueType1.Name)
                                .Replace("{phone}", data.Phone)
                                .Replace("{contactname}", data.ContactName)
                                .Replace("{address}", data.Address)
                                .Replace("{email}", data.Email)
                                .Replace("{apartment}", data.ApartmentNumber)
                                .Replace("{type}", data.IssueType2.Name)
                                .Replace("{description}", data.Description)
                                .Replace("{ispolice}", data.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", data.Guid.ToString()),

                            });

                        }

                        if (!string.IsNullOrEmpty(recipient.Mobile))
                        {
                            var smsService = new SMS();
                            smsService.SendSMS("SER4 Application", recipient.Mobile, template.SMSBody
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", data.Guid.ToString())).Wait();
                        }

                    });

                    //send email to creator of issue

                    var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("CreateIssue");
                    var tenant = _tenantRepository.Get(data.TenantId);

                    if (tenant != null)
                    {
                        if (string.IsNullOrEmpty(tenant.Email))
                        {
                            tenant.Email = "";
                        }
                    }

                 
                    Service.Tools.Email.SendEmail(new EmailModel(model.AccountId)
                    {
                        From = "",
                        To = data.Email,
                        Subject = creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                        Body = creatorTemplate.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                               .Replace("{sender}", data.Email)
                               .Replace("{object}", data.Site.Name)
                               .Replace("{tenant}", data.Tenant.Name)
                               .Replace("{type1}", data.IssueType1.Name)
                               .Replace("{phone}", data.Phone)
                               .Replace("{contactname}", data.ContactName)
                               .Replace("{address}", data.Address)
                               .Replace("{email}", data.Email)
                               .Replace("{apartment}", data.ApartmentNumber)
                               .Replace("{type}", data.IssueType2.Name)
                               .Replace("{description}", data.Description)
                               .Replace("{ispolice}", data.IsPolice.ToString())
                               .Replace("{appurl}", AppUrl)
                               .Replace("{IssueWebId}", data.Guid.ToString()),

                    });



                    if (!string.IsNullOrEmpty(data.Phone))
                    {
                        var smsService = new SMS();
                        smsService.SendSMS("SER4 Application", data.Phone, creatorTemplate.SMSBody
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.Guid.ToString())).Wait();
                    }
                }
            }

            model.IssueNumber = data.IssueNumber;
            model.IssueWebId = data.IssueWebId;

            return model;
        }

        public void SaveIssueWebLog(IssueWebHistoryModel model)
        {
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                //Date = new DateTime(model.Date.year,model.Date.month,model.Date.day,model.Time.hour,model.Time.minute,model.Time.second),
                //Description = model.Description,
                Date = DateTime.Now,
                DescriptionExternal = model.DescriptionExternal,
                DescriptionInternal = model.DescriptionInternal,
                ReactedBy = model.ReactedBy,
                Minutes = model.Minutes,
                MaterialCost = model.MaterialCost,
                IsWarranty = model.IsWarranty,
                IsRent = model.IsRent,
                IsDamage = model.IsDamage,
                IssueWebHistoryType = "info",
                IssueWebId = model.IssueWebId,
                IsPrivate = model.IsPrivate
            });

         
            if (!model.IsPrivate)
            {
                string AppUrl = ConfigurationManager.AppSettings["appurl"];
                var routineName1 = "AddIssueAction";

                var template1 = _emailRepository.GetEmailTemplateFromRoutineName(routineName1);

                var data = _repository.Get(model.IssueWebId);
                var tenant = _tenantRepository.Get(data.TenantId);


                //Service.Tools.Email.SendEmail(new EmailModel()
                //{
                //    From = "",
                //    To = data.Email,
                //    Subject = template1.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                //     Body = template1.Body
                //    .Replace("{appurl}", AppUrl)
                //    .Replace("{IssueWebId}", data.IssueWebId.ToString())

                //});

                //var sms = new SMS();
                //sms.SendSMS("", data.Phone, template1.Body
                //    .Replace("{appurl}", AppUrl)
                //    .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();




                var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                //string AppUrl = ConfigurationManager.AppSettings["appurl"];
                var settingVal = _settingRepositoryData.GetSettingByAccountIdAndName("NotifyIssueActions", data.AccountId);
                var valueToSend = false;
                var s = settingVal != null || settingVal.Any()? settingVal.SingleOrDefault(): null;

                if (s != null) 
                {
                    if (Convert.ToBoolean(s.Value)) 
                    {
                        valueToSend = true;
                    }
                }

                if (valueToSend)
                {
                    recipients.ForEach(recipient =>
                    {
                    //var routineName = Regex.Replace(recipient.IssueRecipientType.Name, @"\s+", "");
                    var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName1);

                        if (!string.IsNullOrEmpty(recipient.Email))
                        {

                            Service.Tools.Email.SendEmail(new EmailModel(data.AccountId)
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                                Body = template1.Body
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", data.Guid.ToString()),

                            });

                        }

                        if (!string.IsNullOrEmpty(recipient.Mobile))
                        {
                            var smsService = new SMS();
                            smsService.SendSMS("SER4 Application", recipient.Mobile, template1.Body
                        .Replace("{appurl}", AppUrl)
                        .Replace("{IssueWebId}", data.Guid.ToString())).Wait();
                        }

                    });
                }

            }
        }

        public void CloseIssue(IssueWebHistoryModel model)
        {
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                //Description = model.Description,
                DescriptionExternal = model.DescriptionExternal,
                DescriptionInternal = model.DescriptionInternal,
                ReactedBy = model.ReactedBy,
                //Minutes = model.Minutes,
                //MaterialCost = model.MaterialCost,
                //IsWarranty = model.IsWarranty,
                //IsRent = model.IsRent,
                //IsDamage = model.IsDamage,
                IssueWebHistoryType = "success",
                IssueWebId = model.IssueWebId
            });

            var issue = _repository.Get(model.IssueWebId);
            issue.Status = 3;
            _repository.Update(issue);

            string AppUrl = ConfigurationManager.AppSettings["appurl"];
            var routineName = "CloseIssue";

            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

            var data = _repository.Get(model.IssueWebId);
            var tenant = _tenantRepository.Get(data.TenantId);
            
            
            //Service.Tools.Email.SendEmailWithAttachment("", data.Email, template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
            //template.Body
            //.Replace("{appurl}", AppUrl)
            //.Replace("{description}",data.Description)
            //.Replace("{IssueWebId}", data.IssueWebId.ToString()),
            //"", data.Image, null, true, new List<string>()
            //{
            //    tenant.Email
            //});

            Service.Tools.Email.SendEmail(new EmailModel(data.AccountId)
            {
                From = "",
                To = data.Email,
                Subject = template.Subject.Replace("{IssueWebId}", data.IssueNumber.ToString()),
                Body = template.Body
                .Replace("{appurl}", AppUrl)
                .Replace("{description}", data.Description)
                .Replace("{IssueWebId}", data.Guid.ToString())

            });
        }

        public void sendReminder(IssueWebModel model)
        {
            var issue = _repository.Get(model.IssueWebId);

            var setting = GetIssueSettingByAccountId(model.AccountId, "SendToSupervisorLimit");
           // setting.Value
            var recipientType = issue.ReminderEmailSentCount >= Convert.ToInt32(setting.Value) ? 6 : 1;
            var recipients = _recipientRepository.GetAllRecipients(model.SiteId).Where(x => x.IssueRecipientTypeId == recipientType).ToList();
            string AppUrl = ConfigurationManager.AppSettings["appurl"];

            bool technicianReminderHasBeenSent = false;
            bool supervisorReminderHasBeenSent = false;

            recipients.ForEach(recipient =>
            {

                switch (recipientType)
                {
                    //reminder sent to technician
                    case 1:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmail";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
                         
                            var list = new List<string>();
                            if (!technicianReminderHasBeenSent)
                            {
                                var tenant = _tenantRepository.Get(model.TenantId);
                                list.Add(tenant.Email);
                            }
                    
                            //Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            //template.Body.Replace("{name}", recipient.Name)
                            //    .Replace("{object}", model.Site.Name)
                            //    .Replace("{tenant}", model.Tenant.Name)
                            //    .Replace("{type1}", model.IssueType1.Name)
                            //    .Replace("{phone}", model.Phone)
                            //    .Replace("{contactname}", model.ContactName)
                            //    .Replace("{address}", model.Address)
                            //    .Replace("{email}", model.Email)
                            //    .Replace("{apartment}", model.ApartmentNumber.ToString())
                            //    .Replace("{type}", model.IssueType2.Name)
                            //    .Replace("{description}", model.Description)
                            //    .Replace("{ispolice}", model.IsPolice.ToString())
                            //    .Replace("{appurl}", AppUrl)
                            //    .Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            //"", model.Image, null, true, list
                            //    );

                            Service.Tools.Email.SendEmail(new EmailModel(issue.AccountId)
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", model.IssueNumber.ToString()),
                                Body = template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber)
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.Guid.ToString())

                            });
                        }

                        technicianReminderHasBeenSent = true;
                        break;

                    //reminder sent to supervisor
                    case 6:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmailLimitExceed";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
                           
                            var list = new List<string>();
                            if (!supervisorReminderHasBeenSent)
                            {
                                var tenant = _tenantRepository.Get(model.TenantId);
                                list.Add(tenant.Email);
                            }

                            Service.Tools.Email.SendEmail(new EmailModel(issue.AccountId)
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", model.IssueNumber.ToString()),
                                Body = template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber)
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.Guid.ToString())

                            });
                            
                        }
                        break;
                }


               
            });

            issue.ReminderEmailSentCount = issue.ReminderEmailSentCount + 1;
            _repository.Update(issue);

        
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                DescriptionExternal = recipientType == 1? "A reminder has been sent by the issuer to the technician": "A reminder has been sent by the issuer to the supervisor",
                DescriptionInternal = recipientType == 1 ? "A reminder has been sent by the issuer to the technician" : "A reminder has been sent by the issuer to the supervisor",
                ReactedBy = model.ContactName + " ("+model.Email+")",
                IssueWebId = model.IssueWebId,
                IssueWebHistoryType = "warning"
            });
        }

        public IEnumerable<IssueWebHistoryModel> GetIssueWebLog(int issueWebId, bool showHidden = false)
        {

            var data = _repository.GetIssueWebHistoryByIssueWebId(issueWebId);

            if (!showHidden)
            {
               data = data.Where(x => !x.IsPrivate).ToList();
            }

            return 
            data  
            .Select(x=> new IssueWebHistoryModel()
            {
                IssueWebHistoryId = x.IssueWebHistoryId,
                Date = new SERApp.Models.Common.DateModel()
                {
                    day = x.Date.Day,
                    month = x.Date.Month,
                    year = x.Date.Year,
                },
                Time = new SERApp.Models.Common.TimeModel()
                {
                    hour = x.Date.Hour,
                    minute = x.Date.Minute,
                    second = x.Date.Second
                },
                DateTime = x.Date,
                DescriptionExternal = x.DescriptionExternal,
                DescriptionInternal = x.DescriptionInternal,
                IsDamage = x.IsDamage,
                IsRent = x.IsRent,
                IsWarranty = x.IsWarranty,
                MaterialCost = x.MaterialCost,
                Minutes = x.Minutes,
                ReactedBy = x.ReactedBy,
                IssueWebId = x.IssueWebId,
                IssueWebHistoryType = x.IssueWebHistoryType
            });
        }

        public void SendDetailsModel(SendDetailsModel model)
        {
            var body = model.additionalMessage;
            var routineName = "IssueDetails";
            string AppUrl = ConfigurationManager.AppSettings["appurl"];
            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
            var issueWeb = _repository.Get(model.IssueWebId);
            
            //throw new NotImplementedException();
            model.Recipients.Where(x => x.IsChecked).ToList().ForEach(x => 
            {               
                var completeBody = "";
                if (!string.IsNullOrEmpty(body))
                {
                    var templ = template.Body
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", issueWeb.Guid.ToString());

                    completeBody = $"{body}<br/><br><hr/>{templ}";
                }
                else {
                    completeBody = template.Body
                               .Replace("{appurl}", AppUrl)
                               .Replace("{IssueWebId}", issueWeb.Guid.ToString());
                }

                var list = new List<string>();
                //var tenant = _tenantRepository.Get(issueWeb.TenantId);
                //list.Add(tenant.Email);

                if (issueWeb.TenantId != 0)
                {
                    var tenant = _tenantRepository.Get(issueWeb.TenantId);
                    list.Add(tenant.Email);
                }

                Service.Tools.Email.SendEmailWithAttachment("", x.Email, template.Subject.Replace("{IssueWebId}", model.IssueNumber.ToString()),
                            completeBody,
                            "", issueWeb.Image, null, true, list);
              
            });


            model.otherRecipients.ForEach(x => 
            {
                var completeBody = "";
                if (!string.IsNullOrEmpty(body))
                {
                    var templ = template.Body
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", issueWeb.Guid.ToString());

                    completeBody = $"{body}<br/><br><hr/>{templ}";
                }
                else
                {
                    completeBody = template.Body
                               .Replace("{appurl}", AppUrl)
                               .Replace("{IssueWebId}", issueWeb.Guid.ToString());
                }


                var list = new List<string>();
                if (issueWeb.TenantId != 0) {
                    var tenant = _tenantRepository.Get(issueWeb.TenantId);
                    list.Add(tenant.Email);
                }
              

                Service.Tools.Email.SendEmailWithAttachment("", x.Email, template.Subject.Replace("{IssueWebId}", model.IssueNumber.ToString()),
                        completeBody,
                        "", issueWeb.Image, null, true,list);
            });
        }

        public IssuesDropdownModel GetIssueDropdowns(int accountId,int siteId)
        {
            var model = new IssuesDropdownModel();
          
            model.IssueType1 = _issueType1Repository.GetAllByRawString($"SELECT * FROM IssueType1 WHERE AccountId = {accountId}").Select(x=> new DropdownModel()
            {
                Id = x.IssueType1Id,
                Name = x.Name
            }).ToList();

            model.IssueType2 = _issueType2Repository.GetAllByRawString($"SELECT * FROM IssueType2 WHERE AccountId = {accountId}").Select(x => new DropdownModel()
            {
                Id = x.IssueType2Id,
                Name = x.Name
            }).ToList();

            
            model.Contacts = _tenantRepository.GetAllByRawString($"SELECT * FROM Tenants WHERE SiteId = {siteId}").Select(x => new DropdownModel()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return model;

        }

        public IssueWebModel GetByGuid(Guid guid)
        {
            var data = _issueWebRepository.GetAllByRawString($"SELECT * FROM IssueWeb WHERE Guid = '{guid}'").SingleOrDefault();

            data.Site = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE Id = {data.SiteId}").SingleOrDefault();
            data.Tenant = _tenantRepository.GetAllByRawString($"SELECT * FROM Tenants WHERE Id = {data.TenantId}").SingleOrDefault();
            data.IssueType1 = _issueType1Repository.GetAllByRawString($"SELECT * FROM IssueType1 WHERE IssueType1Id = {data.IssueType1Id}").SingleOrDefault();
            data.IssueType2 = _issueType2Repository.GetAllByRawString($"SELECT * FROM IssueType2 WHERE IssueType2Id = {data.IssueType2Id}").SingleOrDefault();
            data.Issue_IssueType3 = _issueType3Repository.GetAllByRawString($"SELECT * FROM Issue_IssueType3 WHERE IssueWebId = {data.IssueWebId}").ToList();
           // Thread.Sleep(1000);
            //data.IssueWebHistories = new List<IssueWebHistory>();
            //data.IssueWebHistories = _issueWebHistoryRepository.GetAllByRawString($"SELECT * FROM IssueWebHistory WHERE IssueWebId = {data.IssueWebId}").OrderByDescending(x => x.Date).ToList();
       
            data.IssueWebHistories = _repository.GetIssueWebHistoryByIssueWebId(data.IssueWebId).OrderByDescending(x => x.Date).ToList();
            var SigBase64 = "";
            if (!string.IsNullOrEmpty(data.Image) && data.Image.Split(',').Length > 1)
            {
                byte[] imageBytes = Convert.FromBase64String(data.Image.Split(',')[1]);

                // Convert byte[] to Image
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    var bitmap = ResizeImage(image, 200, 200);
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var byteData = ImageToByte2(bitmap);
                    SigBase64 = $"{data.Image.Split(',')[0]},{Convert.ToBase64String(byteData)}";
                }

            }



            return new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                Guid = data.Guid,                
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ImageThumb = SigBase64,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                IssueNumber = data.IssueNumber,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = data.Tenant != null? new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                }: new TenantModel(),
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                ReminderEmailSentCount = data.ReminderEmailSentCount,
                IssueType3 = data.Issue_IssueType3.Select(t => new Issue_IssueType3Model()
                {
                    Id = t.Id,
                    IssueType3Id = t.IssueType3Id,
                    IssueWebId = t.IssueWebId,
                    Value = t.Value
                }).ToList(),
                IssueWebHistories = data.IssueWebHistories != null || data.IssueWebHistories.Any() ? data.IssueWebHistories.DistinctBy(r=>r.IssueWebHistoryId).Select(x => new IssueWebHistoryModel()
                {
                    IssueWebHistoryId = x.IssueWebHistoryId,
                    Date = new SERApp.Models.Common.DateModel()
                    {
                        day = x.Date.Day,
                        month = x.Date.Month,
                        year = x.Date.Year,
                    },
                    Time = new SERApp.Models.Common.TimeModel()
                    {
                        hour = x.Date.Hour,
                        minute = x.Date.Minute,
                        second = x.Date.Second
                    },
                    DateTime = x.Date,
                    //Description = x.Description,
                    DescriptionExternal = x.DescriptionExternal,
                    DescriptionInternal = x.DescriptionInternal,
                    IsDamage = x.IsDamage,
                    IsRent = x.IsRent,
                    IsWarranty = x.IsWarranty,
                    MaterialCost = x.MaterialCost,
                    Minutes = x.Minutes,
                    ReactedBy = x.ReactedBy,
                    IssueWebId = x.IssueWebId,
                    IssueWebHistoryType = x.IssueWebHistoryType
                }).ToList() : new List<IssueWebHistoryModel>(),

            };
        }
        public byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public void ReOpenIssue(IssueWebModel model)
        {
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                DescriptionExternal = "Issue has been re-opened",
                DescriptionInternal = "Issue has been re-opened",
                ReactedBy = "Technician",
                IssueWebId = model.IssueWebId,
                //to do add this to enum , confirm = 1, reminder =2
                IssueWebHistoryType = "warning"
            });

            var data = _repository.Get(model.IssueWebId);
            data.Status = 2;
            _repository.Update(data);
        }

        public Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public byte[] ExportXLS(int accountId, int siteId = 0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null)
        {

            using (var excel = new ExcelPackage())
            {

                var se = new CultureInfo("sv-SE");
                var issues = GetIssueWeb(accountId, siteId, statusId, contactId, issueType1Id, issueType2Id, issueType3Id, searchText, fromDate, toDate);

                var summarySheet = excel.Workbook.Worksheets.Add($"Issues");
                summarySheet.Column(1).Width = 30;
                summarySheet.Column(2).Width = 30;
                summarySheet.Column(3).Width = 30;
                summarySheet.Column(4).Width = 30;
                summarySheet.Column(5).Width = 30;
                summarySheet.Column(6).Width = 30;
                summarySheet.Column(7).Width = 30;

                summarySheet.Cells[1, 1].Value = "Issue #";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Date/Time Created";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

                summarySheet.Cells[1, 3].Value = "Status";
                summarySheet.Cells[1, 3].Style.Font.Bold = true;

                summarySheet.Cells[1, 4].Value = "Contact";
                summarySheet.Cells[1, 4].Style.Font.Bold = true;


                summarySheet.Cells[1, 5].Value = "Description";
                summarySheet.Cells[1, 5].Style.Font.Bold = true;


                summarySheet.Cells[1, 6].Value = "Contact Person";
                summarySheet.Cells[1, 6].Style.Font.Bold = true;

                summarySheet.Cells[1, 7].Value = "Object";
                summarySheet.Cells[1, 7].Style.Font.Bold = true;



                int startRow = 2;



                if (issues.Select(x => x.SiteId).Distinct().Any())
                {
                    var siteIds = String.Join(",", issues.Select(x => x.SiteId).Distinct());

                    var sites = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE Id IN ({siteIds})").ToList();


                    issues.ForEach(x =>
                    {

                        summarySheet.Cells[startRow, 1].Value = x.IssueNumber;
                        summarySheet.Cells[startRow, 2].Value = x.DateCreated.ToString("yyyy-MM-dd HH:mm:ss");
                        summarySheet.Cells[startRow, 3].Value = x.Status;

                        summarySheet.Cells[startRow, 4].Value = x.Tenant.Name;
                        summarySheet.Cells[startRow, 5].Value = x.Description;
                        summarySheet.Cells[startRow, 6].Value = x.ContactName;


                        summarySheet.Cells[startRow, 7].Value = sites.SingleOrDefault(r => r.Id == x.SiteId).Name;


                        startRow++;
                    });


                }


                return excel.GetAsByteArray();
            }
        }
    }
}
