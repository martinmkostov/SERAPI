﻿
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IType2Service
    {
        bool AddType2(Type2Model model);
        void UpdateType2(Type2Model model);
        bool DeleteType2ById(int id);
        IEnumerable<Type2Model> GetAllType2(int accountId, int siteId);
        Type2Model GetType2ById(int id);
    }

    public class Type2Service : IType2Service
    {
        private IRepository<Type2> _repository;
        private IIncidentRepository _incidentRepository;
        public Type2Service()
        {
            _incidentRepository = new IncidentRepository();
            _repository = new Repository<Type2>();
        }

        public bool AddType2(Type2Model model)
        {
            if (model.Id == 0)
            {
                if (_repository.GetAll().Any(x => x.Name == model.Name && x.AccountId == model.AccountId && model.SiteId == x.SiteId))
                {
                    return false;
                }

                _repository.Save(new Type2()
                {
                    CreatedDate = DateTime.Now,
                    Id = model.Id,
                    LastUpdatedDate = model.LastUpdatedDate,
                    Name = model.Name,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId
                });

            }
            else
            {
                UpdateType2(model);
            }
            return true;
        }

        public void UpdateType2(Type2Model model)
        {
            var existingCustomType = _repository.Get(model.Id);
            existingCustomType.LastUpdatedDate = DateTime.Now;
            existingCustomType.Name = model.Name;
            existingCustomType.AccountId = model.AccountId;
            existingCustomType.SiteId = model.SiteId;
            _repository.Update(existingCustomType);
        }

        public bool DeleteType2ById(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type2Id.HasValue && r.Type2Id == id))
            {
                return false;
            }

            _repository.Delete(id);

            return true;
        }

        public IEnumerable<Type2Model> GetAllType2(int accountId, int siteId)
        {
            var data = _repository.GetAllByRawString($"SELECT * FROM Type2 WHERE AccountId = {accountId}").Select(x=> new Type2Model()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name,
                AccountId = x.AccountId,
                SiteId = x.SiteId
            }).ToList();

            if (siteId != 0)
            {
                data = data.Where(x => x.SiteId == siteId).ToList();
            }

            return data;
        }

        public Type2Model GetType2ById(int id)
        {
            //use automapper for easy mapping
            var model = _repository.Get(id);
            
            return new Type2Model()
            {
                CreatedDate = model.CreatedDate,
                LastUpdatedDate = model.LastUpdatedDate,
                Name = model.Name,
                Id = model.Id,
                SiteId = model.SiteId
            };
        }
    }
}
