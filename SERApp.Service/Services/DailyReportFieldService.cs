﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IDailyReportFieldService
    {
        ICollection<DailyReportFieldModel> GetDailyReportFields();
    }

    public class DailyReportFieldService : IDailyReportFieldService
    {
        private readonly IDailyReportFieldsRepository repository;

        public DailyReportFieldService()
        {
            this.repository = new DailyReportFieldsRepository();
        }

        public ICollection<DailyReportFieldModel> GetDailyReportFields()
        {
            var fields = this.repository.GetAll().ToList();

            return fields.Select(e => new DailyReportFieldModel
            {
                Id = e.Id,
                Label = e.DefaultLabel,
                DataSource = e.DefaultDataSource,
                Value = e.DefaultValue,
                FieldType = e.FieldType.Name,
                FieldTypeFlag = e.FieldType.Flag,
                Type = e.Type,
                IsRequired = e.DefaultIsRequired,
                SortOrder = e.SortOrder,
                CustomEditorName = e.CustomEditorName,
                FieldName = e.ControlName,
                CustomScript = e.DefaultCustomScript
            }).ToList();
        }
    }
}
