﻿using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueRecipientService {
        List<IssueRecipientModel> GetAll(int siteId);
        List<IssueRecipientTypeModel> GetAllTypes();
        bool Save(IssueRecipientModel model);
        IssueRecipientModel Get(int id);
        bool Delete(int id);
    }
    public class IssueRecipientService : IIssueRecipientService
    {
        private IIssueRecipientRepository _repository;
        public IssueRecipientService()
        {
            _repository = new IssueRecipientRepository();
        }

        public bool Delete(int id)
        {
            try {
                _repository.Delete(id);
            } catch { return false; }
            

            return true;
        }

        public List<IssueRecipientModel> GetAll(int siteId)
        {
            return _repository.GetAllRecipients(siteId).Select(x=> new IssueRecipientModel()
            {
                Email = x.Email,
                IssueRecipientId = x.IssueRecipientId,
                IssueRecipientTypeId = x.IssueRecipientTypeId,
                Name = x.Name,
                Mobile = x.Mobile,
                SiteId = x.SiteId,
                Site = new SiteModel()
                {
                    Name = x.Site.Name,
                },
                IssueRecipientType = new IssueRecipientTypeModel()
                {
                    IssueRecipientTypeId = x.IssueRecipientType.IssueRecipientTypeId,
                    Name = x.IssueRecipientType.Name
                }
            }).ToList();
        }

        public List<IssueRecipientTypeModel> GetAllTypes()
        {
            return _repository.GetAllTypes().Where(x=>x.Hidden == false).Select(x => new IssueRecipientTypeModel()
            {
                IssueRecipientTypeId = x.IssueRecipientTypeId,
                Name = x.Name
            }).ToList();
        }


        public IssueRecipientModel Get(int id)
        {
            var data = _repository.Get(id);
            return new IssueRecipientModel()
            {
                IssueRecipientTypeId = data.IssueRecipientTypeId,
                Name = data.Name,
                Email = data.Email,
                Mobile = data.Mobile,
                IssueRecipientId = data.IssueRecipientId,
                IssueRecipientType = new IssueRecipientTypeModel() {
                    IssueRecipientTypeId = data.IssueRecipientTypeId,
                    Name = data.IssueRecipientType.Name
                },
                SiteId = data.SiteId
            };
        
        }

        public bool Save(IssueRecipientModel model)
        {
            
            if (model.IssueRecipientId == 0)
            {
                if (_repository.GetAll().Where(x => x.SiteId == model.SiteId && x.IssueRecipientTypeId == model.IssueRecipientTypeId).Any(x => x.Email == model.Email))
                {
                    //already exists!
                    return false;
                }

                _repository.Save(new Data.Models.IssueRecipients()
                {
                    Email = model.Email,
                    Name = model.Name,
                    Mobile = model.Mobile,
                    IssueRecipientTypeId = model.IssueRecipientTypeId,
                    SiteId = model.SiteId
                });
                return true;
            }
            else {

                var existingData = _repository.Get(model.IssueRecipientId);
                existingData.Email = model.Email;
                existingData.Mobile = model.Mobile;
                existingData.Name = model.Name;
                existingData.IssueRecipientTypeId = model.IssueRecipientTypeId;

                _repository.Update(existingData);
                return true;
            }
        }
    }
}
