﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ICalendarService
    {
        CalendarModel AddCalendar(CalendarModel model);
        CalendarModel GetCalendar(int id);
        IEnumerable<CalendarModel> GetCalendars(CalendarFilters filter);
        bool DeleteCalendar(int id);
    }

    public class CalendarService : ICalendarService
    {
        private IRepository<Calendar> _calendarRepository;
        private IRepository<Setting> _repositorySet;
        private SettingRepository _settingRepository;
        private SiteRepository _siteRepository;
        private SiteService _siteService;

        public CalendarService()
        {
            _calendarRepository = new Repository<Calendar>();
            _settingRepository = new SettingRepository();
            _siteService = new SiteService();
            _siteRepository = new SiteRepository();
            _repositorySet = new Repository<Setting>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CalendarModel AddCalendar(CalendarModel model)
        {
            DateTime _calendarDate = new DateTime();
            if (model != null)
            {
                if (model.Id == 0)
                {
                    var accountId = _siteRepository.Get(model.SiteId).AccountId;
                    reReadCall:
                    var data = _settingRepository.GetSettingByAccountIdAndName("Timezone", accountId);
                    if (!data.Any())
                    {
                        _repositorySet.Save(new Setting()
                        {
                            AccountId = accountId,
                            Name = "Timezone",
                            Value = "(UTC+02:00) Europe/Stockholm",
                            ConfigSettingId = 25,
                            SiteId = 0
                        });
                        goto reReadCall;
                    }

                    var _date = new DateTime(model.CalendarDateModel.year, model.CalendarDateModel.month, model.CalendarDateModel.day, model.CalendarTimeModel.hour, model.CalendarTimeModel.minute, model.CalendarTimeModel.second);
                    _date = _date.AddHours(Convert.ToInt32(data.FirstOrDefault().Value.Split(':')[0].Replace("(UTC", string.Empty)) * -1);

                    Calendar calendar = new Calendar()
                    {
                        Name = model.Name,
                        Desription = model.Desription,
                        CalendarTime = _date,
                        CalendarDate = _date,
                        SiteId = model.SiteId,
                        UserId = model.UserId,
                        CreatedBy = model.CreatedBy,
                        CreatedDate = DateTime.Now,
                        IsActive = true
                    };

                    _calendarRepository.Save(calendar);
                    model.Id = calendar.Id;
                    model.CalendarDate = calendar.CalendarDate;
                    model.CalendarTime = calendar.CalendarTime;

                }
                else
                {
                    var _date = new DateTime(model.CalendarDateModel.year, model.CalendarDateModel.month, model.CalendarDateModel.day, model.CalendarTimeModel.hour, model.CalendarTimeModel.minute, model.CalendarTimeModel.second);

                    Calendar calendar = _calendarRepository.Get(model.Id);
                    calendar.Name = model.Name;
                    calendar.Desription = model.Desription;
                    calendar.IsActive = model.IsActive;
                    calendar.CalendarDate = _date;
                    calendar.CalendarTime = _date;
                    calendar.SiteId = model.SiteId;
                    calendar.LastUpdatedDate = DateTime.Now;
                    calendar.UpdatedBy = model.UpdatedBy;

                    _calendarRepository.Update(calendar);

                    model.CalendarDate = calendar.CalendarDate;
                    model.CalendarTime = calendar.CalendarTime;
                }
            }

            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CalendarModel GetCalendar(int id)
        {
            var model = _calendarRepository.Get(id);

            return new CalendarModel()
            {
                Id = model.Id,
                Name = model.Name,
                Desription = model.Desription,
                CalendarDate = model.CalendarDate,
                CalendarTime = model.CalendarTime,
                CalendarDateModel = new DateModel()
                {
                    day = model.CalendarDate.Day,
                    month = model.CalendarDate.Month,
                    year = model.CalendarDate.Year
                },
                CalendarTimeModel = new TimeModel()
                {
                    hour = model.CalendarTime.Hour,
                    minute = model.CalendarTime.Minute,
                    second = model.CalendarTime.Second
                },
                IsActive  = model.IsActive,
                SiteId = model.SiteId,
                UserId = model.UserId,
                CreatedBy = model.CreatedBy
                //SiteName = _siteRepository.Get(model.SiteId).Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<CalendarModel> GetCalendars(CalendarFilters filter)
        {

            var data = new List<Calendar>();
            if (filter.SiteId == -1)
            {
                var sitestoFind = _siteService.GetByAccountIdAndUserId(filter.AccountId, filter.UserId, 32).Select(x => x.Id);
                if (sitestoFind.Any())
                {
                    data = _calendarRepository.GetAllByRawString($"SELECT * FROM dbo.Calendars INNER JOIN dbo.Sites ON Sites.Id = Calendars.SiteId WHERE SiteId IN ({string.Join(",", sitestoFind)})").ToList();
                }
                else
                {
                    return new List<CalendarModel>();
                }
            }
            else
            {
                data = _calendarRepository.GetAllByRawString($"SELECT * FROM dbo.Calendars WHERE SiteId = {filter.SiteId}").ToList();
            }

            var result = data.Select(d => new CalendarModel()
            {
                Name = d.Name,
                Desription = d.Desription,
                CalendarDate = d.CalendarDate,
                CalendarTime = d.CalendarTime,
                IsActive = d.IsActive,
                Id = d.Id,
                SiteId = d.SiteId,
                UserId = d.SiteId,
                CreatedBy = d.CreatedBy,
                CreatedDate = d.CreatedDate,
                SiteName = _siteRepository.Get(d.SiteId).Name
            }).ToList();


            return result;
        }

        public bool DeleteCalendar(int id)
        {
            _calendarRepository.Delete(id);
            return true;
        }
    }
}
