﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IReportTypeScheduleService
    {
        List<ReportTypeScheduleModel> GetAll();
        bool Add(ReportTypeScheduleModel model);
        bool Delete(string jobId);
        bool ToggleEnable(string jobId);
        ReportTypeScheduleModel GetByJobId(string jobId);
    }

    public class ReportTypeScheduleService : IReportTypeScheduleService
    {
        private IRepository<ReportTypeSchedule> _repository;

        public ReportTypeScheduleService()
        {
            _repository = new Repository<ReportTypeSchedule>();
        }

        public List<ReportTypeScheduleModel> GetAll()
        {
            return this._repository.GetAll().Select(e => new ReportTypeScheduleModel
            {
                //Id = e.Id,
                //ReportTypeId = ((ReportTypes)e.ReportTypeId).ToString(),
               // ModuleId = e.ModuleId,
               // AccountId = e.AccountId,
                Schedule = e.Schedule,
                JobId = e.JobId,
                TimeZone = e.TimeZone,
                BaseUTCOffset = e.BaseUTCOffset,
                IsEnabled =e.IsEnabled,
                DataToSend = e.DataToSend
               // Cron = e.Cron
            }).ToList();
        }

        public ReportTypeScheduleModel GetByJobId(string jobId)
        {
            return this._repository.GetByPredicate(x=>x.JobId == jobId).Select(e=>new ReportTypeScheduleModel()
            {
               
                JobId  =e.JobId,
                Schedule = e.Schedule,
                TimeZone =e.TimeZone,
                BaseUTCOffset = e.BaseUTCOffset,
                IsEnabled = e.IsEnabled,
                DataToSend =e.DataToSend,
            }).SingleOrDefault();
            //return this._repository.GetByPredicate(e => e.ReportTypeId == reportTypeId && e.ModuleId == moduleId).Select(e => new ReportTypeScheduleModel
            //{
            //    //Id = e.Id,
            //    ReportTypeId = ((ReportTypes)e.ReportTypeId).ToString(),
            //    ModuleId = e.ModuleId,
            //    AccountId = e.AccountId,
            //    Schedule = e.Schedule,
            //    Cron = e.Cron
            //}).FirstOrDefault();
        }

        public ReportTypeScheduleModel Get(int accountId, int reportTypeId, int moduleId)
        {
            return null;
            //return this._repository.GetByPredicate(e => e.ReportTypeId == reportTypeId && e.ModuleId == moduleId).Select(e => new ReportTypeScheduleModel
            //{
            //    //Id = e.Id,
            //    ReportTypeId = ((ReportTypes)e.ReportTypeId).ToString(),
            //    ModuleId = e.ModuleId,
            //    AccountId = e.AccountId,
            //    Schedule = e.Schedule,
            //    Cron = e.Cron
            //}).FirstOrDefault();
        }

        public bool Add(ReportTypeScheduleModel model)
        {
            var schedule = this._repository.GetByPredicate(e => e.JobId == model.JobId).FirstOrDefault();

            if (schedule != null)
            {
                schedule.Schedule = model.Schedule;
                schedule.BaseUTCOffset = model.BaseUTCOffset;
                schedule.TimeZone = model.TimeZone;
                schedule.IsEnabled = model.IsEnabled;
                schedule.DataToSend = model.DataToSend;
                //schedule.Cron = model.Cron;

                this._repository.Update(schedule);
            }
            else
            {
                schedule = this._repository.Save(new ReportTypeSchedule
                {
                    // ReportTypeId = Convert.ToInt32((ReportTypes)Enum.Parse(typeof(ReportTypes), model.ReportTypeId)),
                    //ModuleId = model.ModuleId,
                    // AccountId = model.AccountId,
                    Schedule = model.Schedule,
                    JobId = model.JobId,
                    TimeZone = model.TimeZone,
                    BaseUTCOffset = model.BaseUTCOffset,
                    IsEnabled = model.IsEnabled,
                    DataToSend = model.DataToSend
                    // Cron = model.Cron
                });
            }

            //model.Id = schedule.Id;

            return true;
        }

        public bool Delete(string jobId)
        {
            try {
                var data = this._repository.GetByPredicate(x => x.JobId == jobId).SingleOrDefault();
                this._repository.Delete(data.Id);
            } catch (Exception ex) {
                return false;
            }
           
            return true;
        }

        public bool ToggleEnable(string jobId)
        {
            var data = this._repository.GetByPredicate(x => x.JobId == jobId);
            if (data.Any())
            {
                var d = data.FirstOrDefault();
                d.IsEnabled = !d.IsEnabled;
                this._repository.Update(d);
            }
            return true;
        }
    }
}
