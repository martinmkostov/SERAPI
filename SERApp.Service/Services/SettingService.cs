﻿using Hangfire;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ISettingService {

        IEnumerable<SettingModel> GetSettings(int accountId, int moduleId, int siteId);
        SettingModel GetValue(string setting, int accountId, int siteId);
        SettingModel GetValueById(int id);
        void Save(SettingModel model);
    }
    public class SettingService : ISettingService
    {

        private List<LabelSettings> CustomizableLabel = new List<LabelSettings>()
        {
            new LabelSettings()
            {
                Name = "Type1",
                Value = "Type 1",
                ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "Type2",
                Value = "Type 2",
                ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "Type3",
                Value = "Type 3",
                ModuleId = 6
            },
            new LabelSettings()
            {
                    Name = "StartRond",
                    Value = "",
                    ModuleId = 6
            },
            new LabelSettings()
            {
                    Name = "StopRond",
                    Value = "",
                    ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "IssueType1",
                Value = "Issue Type 1",
                ModuleId = 7
            },
                  new LabelSettings()
            {
                Name = "IssueType2",
                    Value = "Issue Type 2",
                    ModuleId = 7
                },
                new LabelSettings()
            {
                Name = "IssueType3",
                    Value = "Issue Type 3",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                        Name = "WithSpecialOption",
                        Value = "false",
                        ModuleId = 6
                },
                new LabelSettings()
                {
                        Name = "SpecialOptions",
                        Value = "",
                        ModuleId = 6
                },
                new LabelSettings()
                {
                        Name = "PublicText",
                        Value = "To set this text message, please login to SER and update setting Public Text under Settings menu on Issues Module.",
                        ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="PublicPageContactOptions",
                    Value="",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="ContactType",
                    Value = "0",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="Option1",
                    Value = "true-Affärslokal",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="Option2",
                    Value = "true-Lägenheter",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="Option3",
                    Value = "true",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="AccountFolderName",
                    Value="UR Underlag",
                    ModuleId = 4
                },
                new LabelSettings()
                {
                    Name ="ObjectFolderName",
                    Value="Aktivitetsunderlag",
                    ModuleId = 4
                },
                new LabelSettings()
                {
                    Name="Timezone",
                    Value="(UTC+02:00) Europe/Stockholm",
                    ModuleId = 1
                },
                new LabelSettings(){
                    Name="AccidentFolderName",
                    Value="Accident Folder",
                    ModuleId = 1
                },
                  new LabelSettings(){
                    Name="DailyReportFolder",
                    Value="Daily Report Folder",
                    ModuleId = 1
                },
                    new LabelSettings(){
                    Name="SecurityFolder",
                    Value="Security Folder",
                    ModuleId = 1
                },
                new LabelSettings()
                {
                    Name="Type4",
                    Value="GMType",
                    ModuleId = 6
                },
                new LabelSettings()
                {
                    Name="AccidentEmail",
                    Value="",
                    ModuleId = 30
                },
                new LabelSettings(){
                    Name="NotifyIssueActions",
                    Value="false",
                    ModuleId = 7
                },
                new LabelSettings(){
                    Name="DefaultJobCategories",
                    Value="SiteManager,EmergencyContact#1,EmergencyContact#2,EmergencyContact#3",
                    ModuleId = 11
                },
                new LabelSettings()
                {
                    Name ="ContactPublicPageData",
                    Value="",
                    ModuleId = 11
                },
                new LabelSettings()
                {
                    Name ="ContactEmailData",
                    Value="",
                    ModuleId = 11
                },

        };
        
        private IRepository<Setting> _repository;
        private IRepository<ConfigSetting> _configSettingRepository;
        private SettingRepository _settingRepository;
        private IRepository<Tenant> _tenantRepository;
        private IRepository<JobTitle> _jobTitles;
        private IRepository<TenantContact> _tenantContactRepository;
        private IRepository<Site> _siteRepository;
        private IRepository<Contact> _contactRepository;
        public SettingService()
        {
            _repository = new Repository<Setting>();
            _configSettingRepository = new Repository<ConfigSetting>();
            _settingRepository = new SettingRepository();
            _tenantRepository = new Repository<Tenant>();
            _jobTitles = new Repository<JobTitle>();
            _tenantContactRepository = new Repository<TenantContact>();
            _siteRepository = new Repository<Site>();
            _contactRepository = new Repository<Contact>();
        }

        public IEnumerable<SettingModel> GetSettings(int accountId, int moduleId,int siteId)
        {

            var data = _settingRepository.GetSettingByAccountIdAndModuleIdAndSiteId(accountId, moduleId, 0).ToList();
           
            if (siteId != 0)
            {



                var siteSettings = _settingRepository.GetSettingByAccountIdAndModuleIdAndSiteId(accountId, moduleId, siteId).ToList();
               
                var intDataToRemove = new List<int>();
                data.ForEach(x =>
                {
                    if (siteSettings.Any(r => r.Name.ToLower() == x.Name.ToLower()))
                    {
                        _repository.Delete(x.SettingId);
                        intDataToRemove.Add(x.SettingId);
                    }
                });

                data = data.Where(r => !intDataToRemove.Contains(r.SettingId)).ToList();

                data.AddRange(siteSettings);
            }


            CustomizableLabel.Where(x=>x.ModuleId == moduleId).ToList().ForEach(r => 
            {
                if (r.Name.ToLower() == "type1")
                {
                    r.Value = "Vad";
                }

                if (r.Name.ToLower() == "type2")
                {
                    r.Value = "Var";
                }

                if (r.Name.ToLower() == "type3")
                {
                    r.Value = "Vem";
                }


                if (!data.Any(v => v.Name.ToLower() == r.Name.ToLower()))
                {

                    var isMultiple = _configSettingRepository.GetByPredicate(x => x.SettingName.ToLower() == r.Name.ToLower()).Count() > 1;

                    if (isMultiple)
                    {
                        var da = _configSettingRepository.GetByPredicate(x => x.SettingName.ToLower() == r.Name.ToLower());
                        var ga = da.Max(x => x.Id);
                        _configSettingRepository.Delete(ga);
                    }
                    

                    var configSetting = _configSettingRepository.GetByPredicate(x => x.SettingName.ToLower() == r.Name.ToLower()).SingleOrDefault();



                    int setSiteId = 0;
                    if (r.Name == "PublicText" || r.Name== "Option1" || r.Name == "Option2" || r.Name == "Option3")
                    {
                        setSiteId = siteId;
                    }

                    

                    _repository.Save(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value,
                        ConfigSettingId = configSetting.Id,
                        SiteId = setSiteId
                    });

                    data.Add(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value,
                        ConfigSettingId = configSetting.Id,
                        SiteId = setSiteId
                    });



                }
            });

            data = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == 0).ToList();

            if (siteId != 0)
            {
                var siteSettings = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == siteId).ToList();

                data.AddRange(siteSettings);
            }

            data.ForEach(x => 
            {
                if (x.Name == "URUnderlagFolderName")
                {
                    x.Name = "UR Underlag Folder Name";
                }
            });

            return data.Select(r=> new SettingModel()
            { 
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value,
                ConfigSettingId = r.ConfigSettingId,  
                SiteId = r.SiteId
            }).ToList();
        }

        public SettingModel GetValue(string setting, int accountId, int siteId)
        {
            
            if (siteId != 0)
            {
                //var d = _repository.GetAll();
                //var data = d.Where(x => x.SiteId == siteId && x.Name.ToLower() == setting.ToLower()).ToList();
                var data = _settingRepository.GetSettingBySiteAndName(setting, siteId);
                if (data != null && data.Any())
                {
                   return _settingRepository.GetSettingBySiteAndNameAccountId(setting,siteId,accountId).Select(r => new SettingModel()
                    {
                        AccountId = r.AccountId,
                        Name = r.Name,
                        SettingId = r.SettingId,
                        Value = r.Value
                    })
                    .SingleOrDefault();

                }
                else {
                    return _settingRepository.GetSettingBySiteAndNameAccountId(setting, 0, accountId).Select(r => new SettingModel()
                    {
                        AccountId = r.AccountId,
                        Name = r.Name,
                        SettingId = r.SettingId,
                        Value = r.Value
                    })
                    .SingleOrDefault();
                 
                }
            }

            reReadCall:
            var est = _settingRepository.GetSettingByAccountIdAndName(setting,accountId).Select(r => new SettingModel()
            {
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value,
                SiteId = r.SiteId,
            }).ToList();
           
            if (est.Count > 1)
            {
                return est.Where(x => x.SiteId != 0).SingleOrDefault();
            }

            if (est.Count == 0 && setting.ToLower() == "timezone")
            {
                _repository.Save(new Setting()
                {
                    AccountId = accountId,
                    Name = "Timezone",
                    Value = "(UTC+02:00) Europe/Stockholm",
                    ConfigSettingId = 25,
                    SiteId = 0
                });
                goto reReadCall;

            }


            if (est.Count == 0 && setting.ToLower() == "accidentfoldername")
            {
                _repository.Save(new Setting()
                {
                    AccountId = accountId,
                    Name = "AccidentFolderName",
                    Value = "Accident Folder",
                    ConfigSettingId = 28,
                    SiteId = 0
                });
                goto reReadCall;

            }

            if (est.Count == 0 && setting.ToLower() == "dailyreportfolder")
            {
                _repository.Save(new Setting()
                {
                    AccountId = accountId,
                    Name = "DailyReportFolder",
                    Value = "Daily Report Folder",
                    ConfigSettingId = 29,
                    SiteId = 0
                });
                goto reReadCall;

            }

            if (est.Count == 0 && setting.ToLower() == "securityfolder")
            {
                _repository.Save(new Setting()
                {
                    AccountId = accountId,
                    Name = "SecurityFolder",
                    Value = "Security Folder",
                    ConfigSettingId = 30,
                    SiteId = 0
                });
                goto reReadCall;

            }

            return _settingRepository.GetSettingByAccountIdAndName(setting, accountId)
                .Select(r => new SettingModel()
                {
                    AccountId = r.AccountId,
                    Name = r.Name,
                    SettingId = r.SettingId,
                    Value = r.Value,
                    SiteId = r.SiteId,
                })
                .SingleOrDefault();
        }

        public SettingModel GetValueById(int id)
        {
            var r = _repository.Get(id);

            r.ConfigSetting = _configSettingRepository.Get(r.ConfigSettingId);

            return new SettingModel()
            {
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value,
                InputType = r.ConfigSetting.InputType
            };
        }

        public void Save(SettingModel model)
        {
            if (model.SettingId == 0)
            {
                return;
            }

            var data = _repository.Get(model.SettingId);

            if (model.InputType == "checkbox")
            {
                if (model.tempValue == true)
                {
                    data.Value = "true";
                }
                else
                {
                    data.Value = "false";
                }
            }
            else {


                if (model.Name == "SpecialOptions")
                {
                    data.Value = model.Value.TrimEnd(',');
                }
                else
                {
                    data.Value = model.Value;
                }
               
            }

            if (data.Name == "FolderName")
            {
                data.SiteId = 0;
            }
            else
            {
                data.SiteId = model.SiteId;
            }

            if (data.Name == "PublicPageImage")
            {
                var randomGuid = Guid.NewGuid();
                var ext = System.IO.Path.GetExtension(model.extension);
                data.Value = Service.Tools.File.SaveFileToBlob(new FileModel()
                {
                    Extension = ext,
                    AccountId = data.AccountId,
                    FileName = $"{randomGuid}-PublicPageImage{data.AccountId}", // using loan id as the name for now
                    ModuleType = "Tenants",
                    ByteString = model.Value
                });
            }
            
           

            _repository.Update(data);


            if (data.Name == "DefaultJobCategories") 
            {
                BackgroundJob.Enqueue(() => SaveDefaultJobCateg(data));
            }


        }

        public void SaveDefaultJobCateg(Setting data) 
        {
            var names = "";
            var a = data.Value.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();

            if (a.Any())
            {
                var jobTitles = _jobTitles.GetAllByRawString($"SELECT * FROM JobTitles WHERE AccountId = {data.AccountId}").ToList();

                var jj = jobTitles.Where(x => a.Any(v => x.TitleName.ToLower().Replace(" ", string.Empty) == v.ToLower())).ToList();

                var sites = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE AccountId = {data.AccountId} AND IsActive = 1");
                var vv = sites.Select(x => x.Id).Distinct().ToList();
                if (vv.Any())
                {
                    string joinedsites = string.Join(",", vv);
                    var t = _tenantRepository.GetAllByRawString($"SELECT * FROM Tenants WHERE SiteId IN ({joinedsites})");
                    var tIds = t.Select(x => x.Id).Distinct().ToList();

                    if (tIds.Any())
                    {
                        string joined = string.Join(",", tIds);
                        var tcrs = _tenantContactRepository.GetAllByRawString($"SELECT * FROM TenantContacts WHERE TenantId IN ({joined})").ToList();


                        tcrs.GroupBy(x => x.TenantId).ToList().ForEach(v =>
                        {
                            var ta = v.ToList();
                            jj.ForEach(q =>
                            {
                                if (!ta.Any(b => b.JobTypeId == q.Id))
                                {
                                    var cct = _contactRepository.Save(new Contact()
                                    {
                                        CreatedDate = DateTime.Now,
                                        LastUpdatedDate = DateTime.Now,
                                    });
                                    _tenantContactRepository.Save(new TenantContact()
                                    {

                                        ContactId = cct.Id,
                                        JobTypeId = q.Id,
                                        TenantId = v.Key,
                                    });
                                }
                            });
                        });
                    }
                }

            }
        }
    }
}
