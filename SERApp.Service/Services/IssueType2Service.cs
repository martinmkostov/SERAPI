﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueType2Service
    {
        IEnumerable<IssueType2Model> GetAll(int accountId);
        bool Save(IssueType2Model model);
        bool Delete(int id);
        IssueType2Model Get(int id);
    }

    public class IssueType2Service : IIssueType2Service
    {
        private readonly IRepository<IssueType2> _repository;
        public IssueType2Service()
        {
            _repository = new Repository<IssueType2>();
        }

        public IssueType2Model Get(int id)
        {
            var model = new IssueType2Model();
            var data = _repository.Get(id);
            model.AccountId = data.AccountId;
            model.IssueType2Id = data.IssueType2Id;
            model.Name = data.Name;

            return model;
        }

        public IEnumerable<IssueType2Model> GetAll(int accountId)
        {
            return _repository.GetAll().Where(x => x.AccountId == accountId).Select(x => new IssueType2Model()
            {
                AccountId = x.AccountId,
                IssueType2Id = x.IssueType2Id,
                Name = x.Name
            });
        }

        public bool Save(IssueType2Model model)
        {
            try
            {
                if (model.IssueType2Id == 0)
                {
                    var data = new IssueType2();
                    data.AccountId = model.AccountId;
                    data.Name = model.Name;

                    _repository.Save(data);
                    return true;
                }
                else
                {
                    var data = _repository.Get(model.IssueType2Id);
                    data.Name = model.Name;
                    data.AccountId = model.AccountId;
                    _repository.Update(data);
                    return true;
                }

            }
            catch (Exception ex) { return false; }

        }
        public bool Delete(int id)
        {
            _repository.Delete(id);
            return true;
        }
    }
}
