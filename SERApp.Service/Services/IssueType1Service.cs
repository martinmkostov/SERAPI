﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueType1Service
    {
        IEnumerable<IssueType1Model> GetAll(int accountId);
        bool Save(IssueType1Model model);
        bool Delete(int id);
        IssueType1Model Get(int id);
    }

    public class IssueType1Service : IIssueType1Service
    {
        private readonly IRepository<IssueType1> _repository;
        public IssueType1Service()
        {
            _repository = new Repository<IssueType1>();
        }

        public IssueType1Model Get(int id)
        {
            var model = new IssueType1Model();
            var data = _repository.Get(id);
            model.AccountId = data.AccountId;
            model.IssueType1Id = data.IssueType1Id;
            model.Name = data.Name;

            return model;
        }

        public IEnumerable<IssueType1Model> GetAll(int accountId)
        {
            return _repository.GetAll().Where(x => x.AccountId == accountId).Select(x => new IssueType1Model()
            {
                AccountId = x.AccountId,
                IssueType1Id = x.IssueType1Id,
                Name = x.Name
            });
        }

        public bool Save(IssueType1Model model)
        {
            try
            {
                if (model.IssueType1Id == 0)
                {
                    var data = new IssueType1();
                    data.AccountId = model.AccountId;
                    data.Name = model.Name;

                    _repository.Save(data);
                    return true;
                }
                else
                {
                    var data = _repository.Get(model.IssueType1Id);
                    data.Name = model.Name;
                    data.AccountId = model.AccountId;
                    _repository.Update(data);
                    return true;
                }

            }
            catch (Exception ex) { return false; }
           
        }

        public bool Delete(int id) {
            _repository.Delete(id);
            return true;
        }
    }
}
