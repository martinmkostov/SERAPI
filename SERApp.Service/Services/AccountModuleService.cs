﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class AccountModuleService
    {
        AccountRepository _accountRepository;
        AccountModuleRepository _accountModuleRepository;
        ModuleRepository _moduleRepository;
        UserModuleRoleRepository _userModuleRoleRepository;
        UserRepository _userRepository;
        public AccountModuleService()
        {
            _accountRepository = new AccountRepository();
            _accountModuleRepository = new AccountModuleRepository();
            _moduleRepository = new ModuleRepository();
            _userModuleRoleRepository = new UserModuleRoleRepository();
            _userRepository = new UserRepository();
        }

        public AccountModuleModel Get(int id)
        {
            var accountModule = _accountModuleRepository.Get(id);
            var accountModules = _accountModuleRepository.GetAll().Where(am => am.AccountId == id && am.Module.IsActive == true)
                            .Select(m => new ModuleModel()
                            {
                                Id = m.ModuleId,
                                ShortName = m.Module.ShortName,
                                Title = m.Module.Title,
                                Description = m.Module.Description,
                                NodeLevel = m.Module.NodeLevel,
                                IsActive = m.Module.IsActive.Value,
                                IsInAccount = true,
                                DateAdded = m.CreatedDate
                            })
                            .Distinct()
                            .ToList();
            var allModules = _moduleRepository.GetAll().ToList()
                              .Select(m => new ModuleModel()
                              {
                                  Id = m.Id,
                                  ShortName = m.ShortName,
                                  Title = m.Title,
                                  Description = m.Description,
                                  NodeLevel = m.NodeLevel,
                                  IsActive = m.IsActive.Value,
                                  IsInAccount = false,
                                  DateAdded = null
                              })
                              .Distinct()
                              .ToList();

            var mergedModulesNew = accountModules.Union(allModules)
                .GroupBy(a => a.Id, a => new
                {
                    Id = a.Id,
                    ShortName = a.ShortName,
                    Title = a.Title,
                    Description = a.Description,
                    NodeLevel = a.NodeLevel,
                    IsActive = a.IsActive,
                    IsInAccount = a.IsInAccount,
                    DateAdded = a.DateAdded
                })
                .Select(n =>
                {
                    var ent = n.FirstOrDefault();
                    if (ent == null) return null;

                    var model = new ModuleModel()
                    {
                        Id = ent.Id,
                        ShortName = ent.ShortName,
                        Title = ent.Title,
                        Description = ent.Description,
                        NodeLevel = ent.NodeLevel,
                        IsActive = ent.IsActive,
                        IsInAccount = ent.IsInAccount,
                        DateAdded = ent.DateAdded
                    };
                    return model;
                })
                .Where(x => x != null)
                .OrderBy(m => m.Id)
                .ToList();

            return new AccountModuleModel()
            {
                AccountId = accountModule.AccountId,
                ModuleId = accountModule.ModuleId,
                Modules = mergedModulesNew.ToList(),
                Account = entityToModel(accountModule.Account)
            };
        }

        public AccountModuleModel GetByAccountId(int id) {
            //var accountModule = _accountModuleRepository.Get(id);
            var account = _accountRepository.Get(id);
            var accountModules = _accountModuleRepository.GetAll().Where(am => am.AccountId == id && am.Module.IsActive == true)
                            .Select(m => new ModuleModel()
                            {
                                Id = m.ModuleId,
                                ShortName = m.Module.ShortName,
                                Title = m.Module.Title,
                                Description = m.Module.Description,
                                NodeLevel = m.Module.NodeLevel,
                                IsActive = m.Module.IsActive.Value,
                                IsInAccount = true,
                                DateAdded = m.CreatedDate
                            })
                            .Distinct()
                            .ToList();
            var allModules = _moduleRepository.GetAll().ToList()
                              .Where(m => m.IsDefault != true)
                              .Select(m => new ModuleModel()
                              {
                                  Id = m.Id,
                                  ShortName = m.ShortName,
                                  Title = m.Title,
                                  Description = m.Description,
                                  NodeLevel = m.NodeLevel,
                                  IsActive = m.IsActive.Value,
                                  IsInAccount = false,
                                  DateAdded = null
                              })
                              .Distinct()
                              .ToList();

            var mergedModulesNew = accountModules.Union(allModules)
                .GroupBy(a => a.Id, a => new
                {
                    Id = a.Id,
                    ShortName = a.ShortName,
                    Title = a.Title,
                    Description = a.Description,
                    NodeLevel = a.NodeLevel,
                    IsActive = a.IsActive,
                    IsInAccount = a.IsInAccount,
                    DateAdded = a.DateAdded
                })
                .Select(n => new ModuleModel()
                {
                    Id = n.FirstOrDefault().Id,
                    ShortName = n.FirstOrDefault().ShortName,
                    Title = n.FirstOrDefault().Title,
                    Description = n.FirstOrDefault().Description,
                    NodeLevel = n.FirstOrDefault().NodeLevel,
                    IsActive = n.FirstOrDefault().IsActive,
                    IsInAccount = n.FirstOrDefault().IsInAccount,
                    DateAdded = n.FirstOrDefault().DateAdded
                })
                .OrderBy(m => m.Title)
                .ToList();

            return new AccountModuleModel()
            {
                AccountId = account.Id,
                Modules = mergedModulesNew.ToList(),
                Account = entityToModel(account)
            };
        }

        public List<AccountModuleModel> GetAll()
        {

            var finalData = _accountRepository
                .GetAllActive()
                .Select(a => new AccountModuleModel()
            {
                Id = a.Id,
                Account = entityToModel(a),
                Modules = GetByAccountId(a.Id).Modules,
                CreatedDate = a.CreatedDate
            })
            .OrderBy(a => a.Account.Name)
            .ToList();
            return finalData;
        }

        public List<ModuleModel> GetModulesByAccountId(int accountId, int userId = 0) {
            var data = _accountModuleRepository.GetModulesByAccountId(accountId);

            if (userId != 0)
            {
                if (_userRepository.GetRoleByUserId(userId).Role.Name.ToLower() != "admin")
                {
                    var umr = _userModuleRoleRepository.GetUserModuleRolesById(userId);
                    data = data.Where(r => umr.Select(x => x.ModuleId).Any(z => z == r.ModuleId)).ToList();
                }
                
            }


            return data.Select(a => new ModuleModel()
            {
                Id = a.Module.Id,
                ShortName = a.Module.ShortName,
                NodeLevel = a.Module.NodeLevel,
                Title = a.Module.Title
            })
            .OrderBy(m => m.Title)
            .ToList();
        }

        private bool IsInAccount(int moduleId, int accountId)
        {
            return _accountModuleRepository.GetAll().Where(a => a.ModuleId == moduleId && a.AccountId == accountId).Count() > 0;
        }

        public void SaveAccountModule(AccountModuleModel model)
        {
            try
            {
                _accountModuleRepository.SaveAccountModule(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAccountModule(AccountModuleModel model)
        {
            try
            {
                _accountModuleRepository.DeleteAccountModule(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static ModuleModel entityToModel(Module entity)
        {
            return new ModuleModel()
            {
                Id = entity.Id,
                ShortName = entity.ShortName,
                Title = entity.Title,
                Description = entity.Description,
                NodeLevel = entity.NodeLevel,
                IsActive = entity.IsActive.Value
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

    }
}
