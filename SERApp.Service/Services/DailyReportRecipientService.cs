﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IDailyReportRecipientService
    {
        bool SaveDailyReportRecipient(DailyReportRecipientModel model);
        IEnumerable<DailyReportRecipientModel> GetDailyReportRecipients(int accountId, int siteId, string reportType);
        IEnumerable<DailyReportRecipientModel> GetDailyReportRecipientsByAccountAndSite(int accountId, int siteId);
        bool DeleteDailyReportRecipient(int id);
    }

    public class DailyReportRecipientService : IDailyReportRecipientService
    {
        private readonly IRepository<DailyReportRecipient> _repository;
        public DailyReportRecipientService()
        {
            _repository = new Repository<DailyReportRecipient>();
        }

        public IEnumerable<DailyReportRecipientModel> GetDailyReportRecipients(int accountId, int siteId, string reportType)
        {
            ReportTypes enumValue;
            if (reportType == "IB-Larm och Ryckrapport (internal)")
            {
                //reportType = "Ryckrapport";
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), "Ryckrapport");
            }
            else if (reportType == "Oppettidsrapport")
            {
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), "Oppettidesrapport");
            }
            else if (reportType == "Skadegorelse/Klotterrapport")
            {
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), "SkadegorelserKlotterrapport");
            }
            else if (reportType == "DailyReport")
            {
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), "DayReport");
            }
            else if (reportType == "WeeklyReport")
            {
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), "WeekReport");
            }
            else if (reportType == "Bistro Solna")
            {
                var r = reportType.Replace(" ", "");
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), r);
            }
            else
            {                
                enumValue = (ReportTypes)Enum.Parse(typeof(ReportTypes), reportType);
            }
            

            var reportTypeValue = (int)enumValue;
            var ggg = _repository.GetAll().ToList();

            var tt = ggg.Where(r => r.AccountId == accountId).ToList();
            var rr = tt.Where(r => r.SiteId == siteId).ToList();
            var ee = rr.Where(r => r.ReportType == reportTypeValue).ToList();

            return _repository.GetAll().Where(x => x.AccountId == accountId && x.SiteId == siteId && x.ReportType == reportTypeValue)
                .Select(x=> new DailyReportRecipientModel()
                {
                    AccountId = x.AccountId,
                    Email = x.Email,
                    RecipientId = x.RecipientId,
                    ReportType = (ReportTypes)x.ReportType,
                    SiteId = x.SiteId
                });
        }
        public IEnumerable<DailyReportRecipientModel> GetDailyReportRecipientsByAccountAndSite(int accountId, int siteId)
        {                        
            return _repository.GetAll().Where(x => x.AccountId == accountId && x.SiteId == siteId)
                .Select(x => new DailyReportRecipientModel()
                {
                    AccountId = x.AccountId,
                    Email = x.Email,
                    RecipientId = x.RecipientId,
                    ReportType = (ReportTypes)x.ReportType,
                    SiteId = x.SiteId
                });
        }        

        public bool SaveDailyReportRecipient(DailyReportRecipientModel model)
        {
            try {

                //if already exists
                var reportTypeValue = (int)model.ReportType;
                if (_repository.GetAll().Where(r => r.SiteId == model.SiteId && r.AccountId == model.AccountId 
                && r.ReportType == reportTypeValue && r.Email.ToLower() == model.Email.ToLower()).Any())
                {
                    return true;
                }
                var repoType = Convert.ToInt32(model.ReportType);
                if (_repository.GetAll().Any(r => r.Email == model.Email && r.AccountId == model.AccountId && r.ReportType == repoType && r.SiteId == model.SiteId))
                {
                    return false;
                }

                var data = new DailyReportRecipient()
                {
                    AccountId = model.AccountId,
                    Email = model.Email,
                    RecipientId = model.RecipientId,
                    ReportType = Convert.ToInt32(model.ReportType),
                    SiteId = model.SiteId
                };
                _repository.Save(data);

                return true;
            } catch(Exception ex)
            {
                return false;
            }
            
        }

        public bool DeleteDailyReportRecipient(int id)
        {
            try {
                _repository.Delete(id);
                return true;
            } catch (Exception ex) { return false; }
        }

      
    }
}
