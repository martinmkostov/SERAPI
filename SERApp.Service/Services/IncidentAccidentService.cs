﻿using OfficeOpenXml;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SERApp.Service.Services
{
    public interface IIncidentAccidentService
    {
        IEnumerable<IncidentAccidentModel> GetAllIncidentAccident(
            DateTime date,
            DateTime toDay,
            int accidentType,
            int siteId,
            int typeData,
            string searchText,
            int accountId,
            int status,
            int userId,
            int moduleId);

        IncidentAccidentModel Get(int id);
        int AddIncident(IncidentAccidentModel model);
        void SendIncident(IncidentAccidentModel model);
        void AddSkadetype(SkadetypModel model);
        void ToggleStatus(IncidentAccidentModel model);
        void Delete(IncidentAccidentModel model);
        void DeleteComment(int id);
        bool DeleteSkadetyp(SkadetypModel model);
        IEnumerable<SkadetypModel> GetSkadetypes(int type, int accountId);
        IEnumerable<SkadetypModel> GetAllSkadetypes(int accountId);
        List<IncidentAccidentCommentModel> AddComment(IncidentAccidentCommentModel model);
        SkadetypModel GetSkadetyp(int id);
        byte[] ExportXLSList(DateTime date, DateTime date1, int siteId, int type, int status, string search, int accountId, int userId, int accidentType);
    }

    public class IncidentAccidentService : IIncidentAccidentService
    {
        private IRepository<IncidentAccident> _repository;
        private IRepository<Skadetyp> _skadetypeRepository;
        private IRepository<IncidentAccidentComment> _commentRepository;
        private EmailRepository _emailRepository;
        private SiteRepository _siteRepository;
        private SettingRepository _settingRepository;
        private IRepository<Setting> _repositorySet;
        private IRepository<Site> _siteRepo;
        private readonly UserModuleRoleRepository _userModuleRoleRepository;
        UserRepository _userRepository;
        private IncidentAccidentRepository _incidentAccidentRepository;

        public IncidentAccidentService()
        {
            _repository = new Repository<IncidentAccident>();
            _skadetypeRepository = new Repository<Skadetyp>();
            _commentRepository = new Repository<IncidentAccidentComment>();
            _emailRepository = new EmailRepository();
            _siteRepository = new SiteRepository();
            _settingRepository = new SettingRepository();
            _repositorySet = new Repository<Setting>();
            _siteRepo = new Repository<Site>();
            _userModuleRoleRepository = new UserModuleRoleRepository();
            _userRepository = new UserRepository();
            _incidentAccidentRepository = new IncidentAccidentRepository();
        }


        public IEnumerable<SkadetypModel> GetSkadetypes(int type, int accountId)
        {

            return _skadetypeRepository.GetByPredicate(x => x.Type == type && x.AccountId == accountId).Select(x => new SkadetypModel()
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                AccountId = x.AccountId
            }).ToList();
        }

        public void SendIncident(IncidentAccidentModel model)
        {
            if (!string.IsNullOrEmpty(model.DefaultEmail) && model.SendToEmail)
            {
                var returnData = _repository.Get(model.Id);
                var emailTemplate = _emailRepository.GetEmailTemplateFromRoutineName("CreateIncidentAccident");
                string AppUrl = ConfigurationManager.AppSettings["appurl"] + "IncidentAccidentDetails/" + returnData.Id;

                var body = emailTemplate.Body.Replace("{url}", AppUrl)
                    .Replace("{id}", returnData.Id.ToString())
                    .Replace("{date}", returnData.Date.ToString("yyyy-MM-dd"))
                    .Replace("{time}", returnData.Time.ToString("HH:mm:ss"))
                    .Replace("{siteName}", _siteRepository.Get(returnData.SiteId).Name)
                    .Replace("{location}", returnData.Location)
                    .Replace("{affected}", returnData.Affected)
                    .Replace("{event}", returnData.Event)
                    .Replace("{probableCause}", returnData.ProbableCause)
                    .Replace("{damages}", returnData.Damages)
                    .Replace("{reporter}", returnData.Reporter)
                    .Replace("{skadetyp}", returnData.SkadetypId != 0 ? _skadetypeRepository.Get(returnData.SkadetypId).Name : "")
                    .Replace("{accidentType}", returnData.AccidentType == 0 ? "Incident" : returnData.AccidentType == 1 ? "Personskada" : "Fastighetsskada")
                    .Replace("{status}", returnData.Status.ToString())
                    .Replace("{internalCaseNumber}", string.IsNullOrEmpty(returnData.InternalCaseNumber) ? "" : returnData.InternalCaseNumber)
                    .Replace("{externalCaseNumber}", string.IsNullOrEmpty(returnData.ExternalCaseNumber) ? "" : returnData.ExternalCaseNumber)
                    .Replace("{email}", returnData.Email)
                    .Replace("{phone}", returnData.Phone);

                //var accountId = _siteRepository.Get(model.SiteId);

                //Service.Tools.Email.SendEmail(new SERApp.Models.Common.EmailModel()
                //{
                //    AccountId = accountId,
                //    To = model.DefaultEmail,
                //    Body = body,
                //    Subject = emailTemplate.Subject + " with Id#: " + returnData.Id,
                //});


                Service.Tools.Email.SendEmailWithAttachmentToMultipleEmails(model.filepath, "", new List<string>() { model.DefaultEmail }, emailTemplate.Subject, body, string.Empty, model.content, $"Incident-Accident-{model.Id}.pdf", null, false, null, $"SER4");

                //Service.Tools.Email.SendEmailWithAttachmentToMultipleEmails(model.filepath, "", new List<string>() { model.DefaultEmail }, emailTemplate.Subject, body, string.Empty, model.content, $"Incident/Accident #{model.Id}.pdf", null, false, null, $"SER4 - {name}");


            }
        }

        public int AddIncident(IncidentAccidentModel model)
        {
            if (model.Id == 0)
            {

                IncidentAccident returnData;

                var accountId = _siteRepository.Get(model.SiteId).AccountId;
            reReadCall:
                var data = _settingRepository.GetSettingByAccountIdAndName("Timezone", accountId);
                if (!data.Any())
                {
                    _repositorySet.Save(new Setting()
                    {
                        AccountId = accountId,
                        Name = "Timezone",
                        Value = "(UTC+02:00) Europe/Stockholm",
                        ConfigSettingId = 25,
                        SiteId = 0
                    });
                    goto reReadCall;
                }

                var dateData = new DateTime(model.Date.year, model.Date.month, model.Date.day, model.Time.hour, model.Time.minute, model.Time.second);
                dateData = dateData.AddHours(Convert.ToInt32(data.FirstOrDefault().Value.Split(':')[0].Replace("(UTC", string.Empty)) * -1);
                if (model.AccidentType == 0)
                {
                    returnData = _repository.Save(new IncidentAccident()
                    {
                        SkadetypId = 0,
                        Date = dateData,
                        Time = dateData,
                        SiteId = model.SiteId,
                        Location = model.Location,
                        Affected = model.Affected,
                        Event = model.Event,
                        ProbableCause = model.ProbableCause,
                        Damages = model.Damages,
                        AccidentType = model.AccidentType,
                        Status = model.Status,
                        InternalCaseNumber = model.InternalCaseNumber,
                        ExternalCaseNumber = model.ExternalCaseNumber,
                        Cost = model.Cost,
                        Reporter = model.Reporter,
                        Email = model.Email,
                        Phone = model.Phone
                    });
                }
                else
                {
                    returnData = _repository.Save(new IncidentAccident()
                    {
                        SkadetypId = model.Skadetyp,
                        Date = dateData,
                        Time = dateData,
                        SiteId = model.SiteId,
                        Location = model.Location,
                        Affected = model.Affected,
                        Event = model.Event,
                        ProbableCause = model.ProbableCause,
                        Cost = model.Cost,
                        AccidentType = model.AccidentType,
                        Status = model.Status,
                        Reporter = model.Reporter,
                        InternalCaseNumber = model.InternalCaseNumber,
                        ExternalCaseNumber = model.ExternalCaseNumber,
                        Email = model.Email,
                        Phone = model.Phone
                    });
                }

                return returnData.Id;
                //send here             


            }
            else
            {


                var accountId = _siteRepository.Get(model.SiteId).AccountId;
            reReadCall:
                var data = _settingRepository.GetSettingByAccountIdAndName("Timezone", accountId);
                if (!data.Any())
                {
                    _repositorySet.Save(new Setting()
                    {
                        AccountId = accountId,
                        Name = "Timezone",
                        Value = "(UTC+02:00) Europe/Stockholm",
                        ConfigSettingId = 25,
                        SiteId = 0
                    });
                    goto reReadCall;
                }

                var dateData = new DateTime(model.Date.year, model.Date.month, model.Date.day, model.Time.hour, model.Time.minute, model.Time.second);
                dateData = dateData.AddHours(Convert.ToInt32(data.FirstOrDefault().Value.Split(':')[0].Replace("(UTC", string.Empty)) * -1);


                var existingData = _repository.Get(model.Id);
                existingData.SkadetypId = model.Skadetyp;
                existingData.Date = dateData;
                existingData.Time = dateData;
                existingData.SiteId = model.SiteId;
                existingData.Location = model.Location;
                existingData.Affected = model.Affected;
                existingData.Event = model.Event;
                existingData.ProbableCause = model.ProbableCause;
                existingData.Damages = model.Damages;
                existingData.AccidentType = model.AccidentType;
                existingData.Cost = model.Cost;
                existingData.Reporter = model.Reporter;
                existingData.Status = model.Status;

                existingData.InternalCaseNumber = model.InternalCaseNumber;
                existingData.ExternalCaseNumber = model.ExternalCaseNumber;

                existingData.Email = model.Email;
                existingData.Phone = model.Phone;
                existingData.ChangedBy = model.ChangedBy;
                existingData.ChangedTime = DateTime.Now;

                _repository.Update(existingData);

                return model.Id;
                //send here             
                //if (!string.IsNullOrEmpty(model.Email))
                //{
                //    var emailTemplate = _emailRepository.GetEmailTemplateFromRoutineName("UpdateIncidentAccident");
                //    string AppUrl = ConfigurationManager.AppSettings["appurl"] + "IncidentAccidentDetails/" + existingData.Id;

                //    var body = emailTemplate.Body.Replace("{url}", AppUrl);

                //    var accountId = _siteRepository.Get(model.SiteId);

                //    Service.Tools.Email.SendEmail(new SERApp.Models.Common.EmailModel()
                //    {
                //        AccountId = accountId.Id,
                //        To = model.Email,
                //        Body = body,
                //        Subject = emailTemplate.Subject + " with Id#: " + existingData.Id,
                //    });
                //}
            }



        }

        private List<Site> GetValidSitesByUserIdAndModuleId(int accountId, int userId, int moduleId)
        {
            var role = _userRepository.GetRoleByUserId(userId).Role.Name.ToLower();
            if (role == "admin")
                return _siteRepository.GetAllByAccountId(accountId);

            return _userModuleRoleRepository.GetUserModuleRolesByUserIdAndModuleId(userId, moduleId)
                //.Where(x=>x.Role.Name != Role.Viewer.Name)
                .Where(x => x.SiteId.HasValue)
                .Select(x => x.Site).ToList();
        }

        public IEnumerable<IncidentAccidentModel> GetAllIncidentAccident(
            DateTime date,
            DateTime toDay,
            int accidentType,
            int siteId,
            int typeData,
            string searchText,
            int status,
            int accountId,
            int userId,
            int moduleId)
        {

            var sites = siteId == 0 ?
                GetValidSitesByUserIdAndModuleId(accountId, userId, moduleId).ToList() :
                new List<Site> { _siteRepository.Get(siteId) };
            var siteIds = string.Join(",", sites.Select(x => x.Id));

            var data = new List<IncidentAccidentModel>();
            if (typeData == 0)
            {
                data =
                     _repository.GetAllByRawString($"SELECT * FROM IncidentAccident WHERE AccidentType={accidentType} AND SiteId IN ({siteIds}) AND Date >= '{date.ToString("yyyy-MM-dd")}' AND Date <='{toDay.ToString("yyyy-MM-dd")}'")
                     //_repository.GetAll().ToList().Where(x => x.AccidentType == accidentType && x.SiteId == siteId && x.Date >= date && x.Date <= toDay)
                     .OrderBy(x => x.Time)
                     .Select(model => new IncidentAccidentModel()
                     {
                         Skadetyp = 0,
                         Date = new SERApp.Models.Common.DateModel()
                         {
                             day = model.Date.Day,
                             month = model.Date.Month,
                             year = model.Date.Year
                         },
                         Time = new SERApp.Models.Common.TimeModel()
                         {
                             hour = model.Time.Hour,
                             minute = model.Time.Minute,
                             second = model.Time.Second
                         },
                         SiteId = model.SiteId,
                         SiteName = sites.SingleOrDefault(rv => rv.Id == model.SiteId).Name,
                         Location = model.Location,
                         Affected = model.Affected,
                         Event = model.Event,
                         ProbableCause = model.ProbableCause,
                         Damages = model.Damages,
                         AccidentType = model.AccidentType,
                         Reporter = model.Reporter,
                         Id = model.Id,
                         Status = model.Status,
                         InternalCaseNumber = model.InternalCaseNumber,
                         ExternalCaseNumber = model.ExternalCaseNumber,
                         Email = model.Email,
                         Phone = model.Phone,
                         ChangedTime = model.ChangedTime,
                         ChangedBy = model.ChangedBy
                     }).OrderByDescending(x => x.Id)
                 .ToList();
            }
            else if (typeData == 1 || typeData == 2)
            {
                if (accidentType == 0)
                {
                    data =
                         _repository.GetAllByRawString($"SELECT * FROM IncidentAccident WHERE AccidentType={typeData} AND SiteId IN ({siteIds}) AND Date >= '{date.ToString("yyyy-MM-dd")}' AND Date <='{toDay.ToString("yyyy-MM-dd")}'")
                        //_repository.GetAll().ToList().Where(x => x.AccidentType == typeData && x.SiteId == siteId && x.Date >= date && x.Date <= toDay)
                        .OrderBy(x => x.Time)
                        .Select(model => new IncidentAccidentModel()
                        {
                            Skadetyp = model.SkadetypId,
                            Date = new SERApp.Models.Common.DateModel()
                            {
                                day = model.Date.Day,
                                month = model.Date.Month,
                                year = model.Date.Year
                            },
                            Time = new SERApp.Models.Common.TimeModel()
                            {
                                hour = model.Time.Hour,
                                minute = model.Time.Minute,
                                second = model.Time.Second
                            },
                            SiteId = model.SiteId,
                            SiteName = sites.SingleOrDefault(rv => rv.Id == model.SiteId).Name,
                            Location = model.Location,
                            Affected = model.Affected,
                            Event = model.Event,
                            ProbableCause = model.ProbableCause,
                            EstimatedCost = model.EstimatedCost,
                            AccidentType = model.AccidentType,
                            Reporter = model.Reporter,
                            Id = model.Id,
                            Status = model.Status,
                            InternalCaseNumber = model.InternalCaseNumber,
                            ExternalCaseNumber = model.ExternalCaseNumber,
                            Email = model.Email,
                            Phone = model.Phone,
                            ChangedBy = model.ChangedBy,
                            ChangedTime = model.ChangedTime
                        }).OrderByDescending(x => x.Id)
                    .ToList();
                }
                else
                {
                    data =
                        _repository.GetAllByRawString($"SELECT * FROM IncidentAccident WHERE AccidentType={typeData} AND SkadetypId = {accidentType} AND SiteId IN ({siteIds}) AND Date >= '{date.ToString("yyyy-MM-dd")}' AND Date <='{toDay.ToString("yyyy-MM-dd")}'")

                       //_repository.GetAll().ToList().Where(x => x.AccidentType == typeData && x.SiteId == siteId && x.Date >= date && x.Date <= toDay && x.SkadetypId == accidentType)
                       .OrderBy(x => x.Time)
                       .Select(model => new IncidentAccidentModel()
                       {
                           Skadetyp = model.SkadetypId,
                           Date = new SERApp.Models.Common.DateModel()
                           {
                               day = model.Date.Day,
                               month = model.Date.Month,
                               year = model.Date.Year
                           },
                           Time = new SERApp.Models.Common.TimeModel()
                           {
                               hour = model.Time.Hour,
                               minute = model.Time.Minute,
                               second = model.Time.Second
                           },
                           SiteId = model.SiteId,
                           SiteName = sites.SingleOrDefault(rv => rv.Id == model.SiteId).Name,
                           Location = model.Location,
                           Affected = model.Affected,
                           Event = model.Event,
                           ProbableCause = model.ProbableCause,
                           EstimatedCost = model.EstimatedCost,
                           AccidentType = model.AccidentType,
                           Reporter = model.Reporter,
                           Id = model.Id,
                           Status = model.Status,
                           InternalCaseNumber = model.InternalCaseNumber,
                           ExternalCaseNumber = model.ExternalCaseNumber,
                           Email = model.Email,
                           Phone = model.Phone,
                           ChangedBy = model.ChangedBy,
                           ChangedTime = model.ChangedTime
                       })
                   .ToList();
                }
            }
            else if (typeData == 3)
            {


                data =
                    _repository.GetAllByRawString($"SELECT * FROM IncidentAccident WHERE SiteId IN ({siteIds}) AND Date >= '{date.ToString("yyyy-MM-dd")}' AND Date <='{toDay.ToString("yyyy-MM-dd")}'")
                    //_repository.GetAll().ToList().Where(x => x.SiteId == siteId && x.Date >= date && x.Date <= toDay)
                    .OrderBy(x => x.Time)
                    .Select(model => new IncidentAccidentModel()
                    {
                        Skadetyp = model.SkadetypId,
                        Date = new SERApp.Models.Common.DateModel()
                        {
                            day = model.Date.Day,
                            month = model.Date.Month,
                            year = model.Date.Year
                        },
                        Time = new SERApp.Models.Common.TimeModel()
                        {
                            hour = model.Time.Hour,
                            minute = model.Time.Minute,
                            second = model.Time.Second
                        },
                        SiteId = model.SiteId,
                        SiteName = sites.SingleOrDefault(rv => rv.Id == model.SiteId).Name,
                        Location = model.Location,
                        Affected = model.Affected,
                        Event = model.Event,
                        ProbableCause = model.ProbableCause,
                        Damages = model.Damages,
                        AccidentType = model.AccidentType,
                        Reporter = model.Reporter,
                        Id = model.Id,
                        Status = model.Status,
                        InternalCaseNumber = model.InternalCaseNumber,
                        ExternalCaseNumber = model.ExternalCaseNumber,
                        Email = model.Email,
                        Phone = model.Phone,
                        ChangedBy = model.ChangedBy,
                        ChangedTime = model.ChangedTime
                    })
                .ToList();
            }


            if (status != 2)
            {
                data = data.Where(x => x.Status == status).ToList();
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                data = data.Where(x => x.Location.Contains(searchText) || x.Affected.Contains(searchText) || x.Event.Contains(searchText) || x.ProbableCause.Contains(searchText) || x.Reporter.Contains(searchText)).ToList();

            }

            return data;
        }

        public void AddSkadetype(SkadetypModel model)
        {
            if (model.Id != 0)
            {
                var existing = _skadetypeRepository.Get(model.Id);

                existing.Name = model.Name;
                existing.Type = model.Type;
                _skadetypeRepository.Update(existing);

            }
            else
            {
                _skadetypeRepository.Save(new Skadetyp()
                {
                    Name = model.Name,
                    Type = model.Type,
                    AccountId = model.AccountId
                });
            }


        }

        public IncidentAccidentModel Get(int id)
        {
            var model = _repository.Get(id);


            var data = new IncidentAccidentModel()
            {
                Skadetyp = model.SkadetypId,
                Date = new SERApp.Models.Common.DateModel()
                {
                    day = model.Date.Day,
                    month = model.Date.Month,
                    year = model.Date.Year
                },
                Time = new SERApp.Models.Common.TimeModel()
                {
                    hour = model.Time.Hour,
                    minute = model.Time.Minute,
                    second = model.Time.Second
                },
                SiteId = model.SiteId,
                SiteName = _siteRepo.Get(model.SiteId).Name,
                Location = model.Location,
                Affected = model.Affected,
                Event = model.Event,
                ProbableCause = model.ProbableCause,
                EstimatedCost = model.EstimatedCost,
                AccidentType = model.AccidentType,
                Reporter = model.Reporter,
                Damages = model.Damages,
                Id = model.Id,
                Status = model.Status,
                InternalCaseNumber = model.InternalCaseNumber,
                ExternalCaseNumber = model.ExternalCaseNumber,
                Email = model.Email,
                Phone = model.Phone,
                Cost = model.Cost,
                ChangedBy = model.ChangedBy,
                ChangedTime = model.ChangedTime
            };
            data.Comments = _incidentAccidentRepository.GetComments(model.Id);

            return data;
        }

        public IEnumerable<SkadetypModel> GetAllSkadetypes(int accountId)
        {
            return _skadetypeRepository.GetByPredicate(x => x.AccountId == accountId).Select(x => new SkadetypModel()
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                AccountId = x.AccountId,
            }).ToList();
        }

        public void Delete(IncidentAccidentModel model)
        {
            _repository.Delete(model.Id);
        }

        public List<IncidentAccidentCommentModel> AddComment(IncidentAccidentCommentModel model)
        {
            model.Date = new DateTime(model.DateModel.year, model.DateModel.month, model.DateModel.day, model.TimeModel.hour, model.TimeModel.minute, model.TimeModel.second);
            _commentRepository.Save(new IncidentAccidentComment()
            {
                IncidentAccidentId = model.IncidentAccidentId,
                CompensationFromInsurance = model.CompensationFromInsurance,
                Comment = model.Comment,
                CompensationFromOthers = model.CompensationFromOthers,
                Disbursed = model.Disbursed,
                Date = model.Date,
                Requirements = model.Requirements,
                Name = model.Name
            });

            return _incidentAccidentRepository.GetComments(model.IncidentAccidentId);
        }

        public bool DeleteSkadetyp(SkadetypModel model)
        {
            if (_repository.GetByPredicate(x => x.SkadetypId == model.Id).Any())
            {
                return false;
            }

            _skadetypeRepository.Delete(model.Id);
            return true;
        }

        public SkadetypModel GetSkadetyp(int id)
        {
            var data = _skadetypeRepository.Get(id);
            return new SkadetypModel()
            {
                AccountId = data.AccountId,
                Id = data.Id,
                Name = data.Name,
                Type = data.Type
            };
        }

        public void ToggleStatus(IncidentAccidentModel model)
        {
            var data = _repository.Get(model.Id);

            if (data.Status == 0)
            {
                _commentRepository.Save(new IncidentAccidentComment()
                {
                    Comment = "Entry has been OPENED.",
                    Name = model.Name,
                    Date = DateTime.Now,
                    IncidentAccidentId = data.Id,
                });
            }
            else
            {
                _commentRepository.Save(new IncidentAccidentComment()
                {
                    Comment = "Entry has been CLOSED.",
                    Name = model.Name,
                    Date = DateTime.Now,
                    IncidentAccidentId = data.Id,
                });
            }
        }

        public void DeleteComment(int id)
        {
            _commentRepository.Delete(id);
        }


        public byte[] ExportXLSList(DateTime date, DateTime date1, int siteId, int type, int status, string search, int accountId, int userId, int accidentType)
        {
            using (var excel = new ExcelPackage())
            {

                var data = GetAllIncidentAccident(date, date1, accidentType, siteId, type, search, status, accountId, userId, 30).OrderByDescending(e => new DateTime(e.Date.year, e.Date.month, e.Date.day, e.Time.hour, e.Time.minute, e.Time.second)).ToList();
                var skadetypList = GetAllSkadetypes(accountId);

                var summarySheet = excel.Workbook.Worksheets.Add("IncidentsAccidents");
                summarySheet.Column(1).Width = 30;
                summarySheet.Column(2).Width = 30;
                summarySheet.Column(3).Width = 30;
                summarySheet.Column(4).Width = 50;
                summarySheet.Column(4).Style.WrapText = true;
                summarySheet.Column(5).Width = 30;
                summarySheet.Column(6).Width = 30;
                summarySheet.Column(7).Width = 30;
                summarySheet.Column(8).Width = 30;

                summarySheet.Cells[1, 1].Value = "Site";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Date/Time";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

                summarySheet.Cells[1, 3].Value = "Type";
                summarySheet.Cells[1, 3].Style.Font.Bold = true;

                summarySheet.Cells[1, 4].Value = "Description";
                summarySheet.Cells[1, 4].Style.Font.Bold = true;

                summarySheet.Cells[1, 5].Value = "Location";
                summarySheet.Cells[1, 5].Style.Font.Bold = true;

                summarySheet.Cells[1, 6].Value = "Skadetyp";
                summarySheet.Cells[1, 6].Style.Font.Bold = true;

                summarySheet.Cells[1, 7].Value = "Status";
                summarySheet.Cells[1, 7].Style.Font.Bold = true;

                summarySheet.Cells[1, 8].Value = "InternalCaseNumber";
                summarySheet.Cells[1, 8].Style.Font.Bold = true;

                int startRow = 2;

                if (data.Select(x => x.SiteId).Distinct().Any())
                {
                    var siteIds = String.Join(",", data.Select(x => x.SiteId).Distinct());
                    var sites = _siteRepo.GetAllByRawString($"SELECT * FROM Sites WHERE Id IN ({siteIds})").ToList();

                    data.ForEach(x =>
                    {
                        var skadetype = skadetypList.FirstOrDefault(e => e.Id == x.Skadetyp);
                        var hour = x.Time.hour + 1;

                        summarySheet.Cells[startRow, 1].Value = x.SiteName;
                        summarySheet.Cells[startRow, 2].Value = $"{x.Date.year}-{(x.Date.month < 10 ? $"0{x.Date.month}":$"{x.Date.month}")}-{(x.Date.day < 10 ? $"0{x.Date.day}" : $"{x.Date.day}")} {(hour < 10 ? $"0{hour}" : $"{hour}")}:{(x.Time.minute < 10 ? $"0{x.Time.minute}" : $"{x.Time.minute}")}";
                        summarySheet.Cells[startRow, 3].Value = x.AccidentType == 0 ? "Incident" : x.AccidentType == 1 ? "Personskada" : "Fastighetsskada";
                        summarySheet.Cells[startRow, 4].Value = x.Event;
                        summarySheet.Cells[startRow, 5].Value = x.Location;
                        summarySheet.Cells[startRow, 6].Value = skadetype == null ? "": skadetype.Name;
                        summarySheet.Cells[startRow, 7].Value = x.Status == 0 ? "Open": "Closed";
                        summarySheet.Cells[startRow, 8].Value = x.InternalCaseNumber;

                        startRow++;
                    });

                }

                return excel.GetAsByteArray();
            }
        }
    }
}
