﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class FileManagerService
    {
        private readonly string storageConString;
        public FileManagerService()
        {
            storageConString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];
        }

        public List<Files> GetFilesAndFolders()
        {
            var list = new List<Files>();


            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this.storageConString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("ser-files");

            BlobContinuationToken continuationToken = null;
            do
            {
                var resultSegment = container.ListBlobsSegmented("FileManager", true, BlobListingDetails.All, null, continuationToken, null, null);
                continuationToken = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    if (item is CloudBlobDirectory directory)
                    {
                        Console.WriteLine($"Folder: {directory.Prefix}");
                        list.Add(new Files()
                        {
                            Name = directory.Prefix
                        });
                        // You can perform additional operations for each folder if needed
                    }
                    else if (item is CloudBlockBlob blockBlob)
                    {
                        Console.WriteLine($"File: {blockBlob.Name}");
                        list.Add(new Files()
                        {
                            Name = blockBlob.Name
                        });
                        // You can perform additional operations for each file if needed
                    }
                }
            } while (continuationToken != null);

            //if (CloudStorageAccount.TryParse(this.storageConString, out CloudStorageAccount storageAccount))
            //{
            //    var blobClient = storageAccount.CreateCloudBlobClient();
            //    var container = blobClient.GetContainerReference("ser-files");

            //    var aa = container.ListBlobs(prefix: "FileManager");

            //    var bb = aa.ToList().Select(e => new 
            //    { 
            //        Name = e.Uri.AbsoluteUri
            //    });;





            //    /*
            //     * 
            //var blobServiceClient = new BlobServiceClient(this.storageConnectionString);
            //var containerClient = blobServiceClient.GetBlobContainerClient(this.storagePath);
            //var blobs = containerClient.GetBlobs(prefix: $"user/{exhibitorId}/media-library").OrderByDescending(e => e.Properties.CreatedOn);

            //List<string> names = new List<string>();

            //foreach (var blob in blobs)
            //{
            //    names.Add(string.Format("https://mintfilestorage.blob.core.windows.net/files/{0}", blob.Name));
            //}
            //     */
            //}

            return list;
        }
    }
}
