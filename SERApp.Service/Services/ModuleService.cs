﻿using SERApp.Models;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class ModuleService
    {
        ModuleRepository _moduleRepository;
        public ModuleService()
        {
            _moduleRepository = new ModuleRepository();
        }

        public ModuleModel Get(int id)
        {
            var module = _moduleRepository.Get(id);
            return new ModuleModel()
            {
                ShortName = module.ShortName,
                Title = module.Title,
                Description = module.Description,
                IsActive = module.IsActive.Value,
                NodeLevel = module.NodeLevel
            };
        }

        public ModuleModel GetByTitle(string title)
        {            
            var module = _moduleRepository.GetAll().SingleOrDefault(x=> x.Title.ToLower() == title.ToLower());
            return new ModuleModel()
            {
                Id = module.Id,
                ShortName = module.ShortName,
                Title = module.Title,
                Description = module.Description,
                IsActive = module.IsActive.Value,
                NodeLevel = module.NodeLevel
            };
        }

        public List<ModuleModel> GetAll()
        {
            var data = _moduleRepository.GetAll().ToList()
                .Select(m => new ModuleModel()
                {
                    Id = m.Id,
                    ShortName = m.ShortName,
                    Title = m.Title,
                    Description = m.Description,
                    IsActive = m.IsActive.Value,
                    NodeLevel = m.NodeLevel
                }).ToList();
            return data;
        }

        public List<ModuleModel> GetAllActive()
        {
            var data = _moduleRepository.GetAllActive()
                .Select(m => new ModuleModel()
                {
                    ShortName = m.ShortName,
                    Title = m.Title,
                    Description = m.Description,
                    IsActive = m.IsActive.Value,
                    NodeLevel = m.NodeLevel
                }).ToList();
            return data;
        }
    }
}
