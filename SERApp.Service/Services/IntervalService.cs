﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIntervalService
    {
        void Add(IntervalModel model);
        void Update(IntervalModel model);
        IEnumerable<IntervalModel> GetIntervals(int accountId);
        IntervalModel Get(int id);
    }

    public class IntervalService : IIntervalService
    {
        private IRepository<Interval> _repository;
        private IRepository<Site> _siteRepostory;
        private List<IntervalModel> defaultIntervals = new List<IntervalModel>()
        {
            new IntervalModel()
            {
                Name = "Dagligen",
                IntervalType = IntervalEnum.Day,
                Value  = 1,
            },
             new IntervalModel()
            {
                Name = "1g/veckan",
                IntervalType = IntervalEnum.Day,
                Value  = 7,
            },
               new IntervalModel()
            {
                Name = "1g/månad",
                IntervalType = IntervalEnum.Month,
                Value  = 1,
            },
                  new IntervalModel()
            {
                Name = "1/2 år",
                IntervalType = IntervalEnum.Month,
                Value  = 6,
            },
                new IntervalModel()
            {
                Name = "1 år",
                IntervalType = IntervalEnum.Month,
                Value  = 12,
            },
                 new IntervalModel()
            {
                Name = "1 1/2 år",
                IntervalType = IntervalEnum.Month,
                Value  = 18,
            },
                 new IntervalModel()
            {
                Name = "2 år",
                IntervalType = IntervalEnum.Month,
                Value  = 24,
            },
        };

        public IntervalService()
        {
            _repository = new Repository<Interval>();
            _siteRepostory = new Repository<Site>();
        }

        public void Add(IntervalModel model)
        {
            if (model.IntervalId == 0)
            {
                var data = new Interval();
                data.Name = model.Name;
                data.IntervalType = (int)model.IntervalType;
                data.Value = model.Value;
                data.AccountId = model.AccountId;

                _repository.Save(data);
            }
            else {
                var data = _repository.Get(model.IntervalId);
                data.Name = model.Name;
                data.IntervalType = (int)model.IntervalType;
                data.Value = model.Value;

                _repository.Update(data);
            }
        }

        public IntervalModel Get(int id)
        {
            var data = _repository.Get(id);

            return new IntervalModel()
            {
                IntervalId = data.IntervalId,
                IntervalType = (IntervalEnum)data.IntervalType,
                Name = data.Name,
                Value = data.Value,
                AccountId = data.AccountId
            };
        }

        public IEnumerable<IntervalModel> GetIntervals(int accountId)
        {
            
            var d = _repository.GetByPredicate(r => r.AccountId == accountId).ToList();

            if (!d.Any() && accountId > 0)
            {
                defaultIntervals.ForEach(r => 
                {
                    _repository.Save(new Interval()
                    {
                        Name = r.Name,
                        IntervalType = (int)r.IntervalType,
                        Value = r.Value,
                        AccountId = accountId
                    });
                });

                d = _repository.GetByPredicate(r => r.AccountId == accountId).ToList();
            }

            return d.Select(data => new IntervalModel()
            {
                IntervalId = data.IntervalId,
                IntervalType = (IntervalEnum)data.IntervalType,
                Name = data.Name,
                Value = data.Value,
                AccountId = data.AccountId
            }).OrderBy(r=>r.IntervalType).ThenBy(r=>r.Value).ToList();
        }

        public void Update(IntervalModel model)
        {
            var data = _repository.Get(model.IntervalId);
            data.IntervalType = (int)model.IntervalType;
            data.Name = model.Name;
            data.Value = model.Value;

            _repository.Update(data);
        }
    }
}
