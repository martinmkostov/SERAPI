﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ISummaryService
    {
        SummaryModel AddSummary(SummaryModel model, out bool response);
        int UpdateSummary(SummaryModel model);
        SummaryModel GetSummary(DateTime date, int accountId, int siteId);
    }

    public class SummaryService : ISummaryService
    {
        private IRepository<Summary> _repository;

        public SummaryService()
        {
            _repository = new Repository<Summary>();
        }

        public SummaryModel AddSummary(SummaryModel model, out bool response)
        {
            response = false;
            if (model.SummaryValue.Length > 1000)
            {
                return model;
            }
            var existingdata = _repository.Get(model.SummaryId);
            
            if (existingdata != null)
            {
                model.SummaryId = UpdateSummary(model);
            }
            else {

                var m = new Summary()
                {
                    AccountId = model.AccountId,
                    Date = model.Date,
                    SummaryValue = model.SummaryValue,
                    SiteId = model.SiteId,
                };
                _repository.Save(m);
                model.SummaryId = m.SummaryId;
            }
            response = true;
            return model;
        }

        public SummaryModel GetSummary(DateTime date, int accountId, int siteId)
        {
            var existingData = _repository.GetByPredicate(x => x.Date.ToShortDateString() == date.ToShortDateString() && x.AccountId == accountId && x.SiteId == siteId).SingleOrDefault();

            if (existingData == null)
            {
                return new SummaryModel();
            }

            return new SummaryModel()
            {
                SummaryId = existingData.SummaryId,
                SummaryValue = existingData.SummaryValue,
                Date = existingData.Date
            };
        }

        public int UpdateSummary(SummaryModel model)
        {
            var existingData = _repository.Get(model.SummaryId);
            if (existingData != null)
            {
                existingData.SummaryValue = model.SummaryValue;
                //existingData.Date = model.Date;

                _repository.Update(existingData);
                return existingData.SummaryId;
            }

            return 0;
        }
    }
}
