﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IType3Service
    {
        IEnumerable<Type3Model> GetAllType3(int accountId, int siteId);
        Type3Model GetType3(int id);

        bool DeleteType3(int id);
        bool AddType3(Type3Model model);
   
        
    }
    public class Type3Service : IType3Service
    {
        IRepository<Type3TenantType> _type3TenantType;

        IType3Repository _repository;
        IIncidentRepository _incidentRepository;
        IRepository<Tenant> _tenantRepository;
        public Type3Service()
        {
            _type3TenantType = new Repository<Type3TenantType>();

            _repository = new Type3Repository();
            _incidentRepository = new IncidentRepository();
            _tenantRepository = new Repository<Tenant>();
        }


        public bool AddType3(Type3Model model)
        {
            try {
                if (model.Id != 0)
                {
                    var existingData = _repository.Get(model.Id);

                    existingData.Name = model.Name;
                    existingData.IsTenantRequired = model.IsTenantRequired;
                    existingData.AccountId = model.AccountId;
                    existingData.TenantTypeId = model.TenantTypeId;
                    existingData.SiteId = model.SiteId;

                    _repository.Update(existingData);

                    var existingRecords = _type3TenantType.GetByPredicate(e => e.Type3Id == model.Id).ToList();
                    
                    foreach (var existingRecord in existingRecords)
                    {
                        _type3TenantType.Delete(existingRecord.Id);
                    }

                    foreach (var type3TenantTypeId in model.TenantTypeTypeIds)
                    {
                        _type3TenantType.Save(new Type3TenantType
                        {
                            Type3Id = model.Id,
                            TenantTypeId = type3TenantTypeId
                        });
                    }
                }
                else
                {
                    if (_repository.GetAll().Any(r => r.Name == model.Name && r.AccountId == model.AccountId))
                    {
                        return false;
                    }

                    var type3 = new Type3()
                    {
                        Name = model.Name,
                        IsTenantRequired = model.IsTenantRequired,
                        AccountId = model.AccountId,
                        TenantTypeId = model.TenantTypeId,
                        SiteId = model.SiteId,
                        Type3TenantTypes = model.TenantTypeTypeIds != null? model.TenantTypeTypeIds.Select(e => new Type3TenantType
                        {
                            TenantTypeId = e
                        }).ToList() : new List<Type3TenantType>()
                    };

                    _repository.Save(type3);
                }
          
                return true;
            } catch (Exception ex) {
                return false;
            }         
        }

        public bool DeleteType3(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type3Id.HasValue && r.Type3Id == id))
            {
                return false;
            }

            _repository.Delete(id);
            return true;
        }

        public IEnumerable<Type3Model> GetAllType3(int accountId, int siteId)
        {
            var se = new CultureInfo("sv-SE");
            var data = _repository.GetAllType3IncludeChildren();
         
            if (accountId != 0)
            {
                data = data.Where(x => x.AccountId == accountId);
            }

            //if (siteId != 0)
            //{
            //    data = data.Where(x=>x.SiteId == siteId);
            //}

            //if (siteId != 0)
            //{
            //    data = data.Where(x => x.Tenant != null);
            //    data = data.Where(x => x.Tenant.SiteId == siteId);

            //}

            var repositoryData = _tenantRepository.GetAllByRawString($"SELECT * FROM Tenants WHERE SiteId = {siteId}").ToList();

            var listofTenantsTypeIds = data.Where(x => x.IsTenantRequired == true).Select(x => x.TenantTypeId).ToList();
            var tenants = repositoryData.Where(x => x.TenantType.HasValue? listofTenantsTypeIds.Contains(x.TenantType.Value) : false).Select(x=> new TenantModel()
            {
                Name = x.Name,
                Id = x.Id,
                TenantType = x.TenantType.Value
            }).ToList();

        
            return data.Select(x=> new Type3Model()
            {
                Id = x.Id,
                Name =x.Name,
                IsTenantRequired = x.IsTenantRequired,
                AccountId = x.AccountId,
                TenantTypeId = x.TenantTypeId,                
                Tenants = tenants.Where(r=>r.TenantType == x.TenantTypeId).ToList()
            }).OrderBy(a => a.Name, StringComparer.Create(se, false)).ToList();
        }

        public Type3Model GetType3(int id)
        {
            var model = _repository.GetType3IncldeChildren(id);

            return new Type3Model()
            {
                Id = model.Id,
                Name = model.Name,
                IsTenantRequired = model.IsTenantRequired,
                AccountId = model.AccountId,
                TenantTypeId = model.TenantTypeId,
                TenantTypeTypeIds = model.Type3TenantTypes.Select(e => e.TenantTypeId).ToList()
                //Tenant = model.Tenant != null ? new TenantModel()
                //{
                //    Name = model.Tenant.Name,
                //    Id = model.Tenant.Id,                    
                //}: new TenantModel(),
                //TenantId = model.TenantId
            };
        }

        
    }
}
