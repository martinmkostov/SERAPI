﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Service.Services
{
    public class AccountSettingsService
    {
        SettingRepository _settingRepository;
        ModuleRepository _moduleRepository;
        public AccountSettingsService()
        {
            _settingRepository = new SettingRepository();
            _moduleRepository = new ModuleRepository();
        }

        public AccountSettingModel Get(int id)
        {
            return entityToModel(_settingRepository.Get(id));
        }

        public AccountSettingModel Get(int configSettingsId, int accountid)
        {
            return entityToModel(_settingRepository.Get(configSettingsId, accountid));
        }

        public AccountSettingModel Get(string name, int accountid)
        {
            var accountSettings = _settingRepository.Get(name, accountid);
            if (accountSettings == null)
            {
                var configSettings = _settingRepository.GetConfigSetting(name);
                return new AccountSettingModel() {
                    ConfigSettingId = configSettings.Id,
                    Name = configSettings.SettingName,
                    Label = configSettings.SettingLabel,
                    Value = configSettings.DefaultValue,
                    AccountId = accountid
                };
            }
            return entityToModel(accountSettings);
        }

        public void Save(AccountSettingModel model)
        {
            _settingRepository.Save(model);
        }

        public List<AccountSettingModel> GetAccountSettings(int accountId)
        {
            var accountSettings = (from a in _settingRepository.GetAccountSettings(accountId)
                                   join cs in _settingRepository.GetConfigSettings() on a.ConfigSettingId equals cs.Id
                                   select new
                                   {
                                       Id = a.SettingId,
                                       ConfigSettingId = a.ConfigSettingId,
                                       AccountId = a.AccountId,
                                       InputType = _settingRepository.GetConfigSetting(a.ConfigSettingId).InputType,
                                       Label = _settingRepository.GetConfigSetting(a.ConfigSettingId).SettingLabel,
                                       Name = a.Name,
                                       Value = a.Value,
                                       ModuleId = cs.ModuleId
                                   }).ToList();

            var configSettings = (from s in _settingRepository.GetConfigSettings()
                                  where !(accountSettings.Any(a => a.ConfigSettingId == s.Id))
                                  select new
                                  {
                                      Id = 0,
                                      ConfigSettingId = s.Id,
                                      AccountId = 0,
                                      InputType = s.InputType,
                                      Label = s.SettingLabel,
                                      Name = s.SettingName,
                                      Value = s.DefaultValue,
                                      ModuleId = s.ModuleId
                                  })
                                  .ToList();

            

            var finalSettings = configSettings.Union(accountSettings).ToList();
            var modules = _moduleRepository.GetAll().ToList();

            return finalSettings
                .Select(s => new AccountSettingModel()
                {
                    Id = s.Id,
                    ConfigSettingId = s.ConfigSettingId,
                    InputType = s.InputType,
                    AccountId = s.AccountId,
                    Label = s.Label,
                    Name = s.Name,
                    Value = s.Value,
                    Module = s.ModuleId > 0
                        ? modules.Any(r => r.Id == s.ModuleId)
                            ?
                            modules.SingleOrDefault(r => r.Id == s.ModuleId)?.Title
                            : string.Empty
                        : string.Empty
                })
                .OrderBy(s => s.ConfigSettingId)
                .ToList();
        }

        public AccountSettingModel entityToModel(Setting entity)
        {
            return new AccountSettingModel()
            {
                Id = entity.SettingId,
                ConfigSettingId = entity.ConfigSettingId,
                AccountId = entity.AccountId,
                Name = entity.Name,
                Value = entity.Value
            };
        }
    }
}
