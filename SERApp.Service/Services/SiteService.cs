﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Service.Services
{
    public class SiteService
    {
        SiteRepository _siteRepository;
        LogRepository _logRepository;
        AccountRepository _accountRepository;
        TenantRepository _tenantRepository;
        GuardRepository guardRepository;
        UserModuleRoleRepository _userModuleRoleRepository;
        UserRepository userRepository;
        IRepository<Site> _siteRepo;
        public SiteService()
        {
            _siteRepository = new SiteRepository();
            _logRepository = new LogRepository();
            _accountRepository = new AccountRepository();
            _tenantRepository = new TenantRepository();
            guardRepository = new GuardRepository();
            _userModuleRoleRepository = new UserModuleRoleRepository();
            userRepository = new UserRepository();
            _siteRepo = new Repository<Site>();
        }

        public SiteModel GetByGuid(string id)
        {
            try
            {
                var site = _siteRepo.GetAllByRawString($"SELECT * FROM Sites WHERE Guid = '{id}'").FirstOrDefault();
                return new SiteModel()
                {
                    Id = site.Id,
                    AccountId = site.AccountId,
                    Name = site.Name,
                    Address = site.Address,
                    City = site.City,
                    PostalCode = site.PostalCode,
                    AdditionalLocationInfo = site.AdditionalLocationInfo,
                    PhoneNumber = site.PhoneNumber,
                    FaxNumber = site.FaxNumber,
                    ContactPerson = site.ContactPerson,
                    ContactPersonNumber = site.ContactPersonNumber,
                    CreatedDate = site.CreatedDate,
                    LastUpdatedDate = site.LastUpdatedDate,
                    IsActive = site.IsActive,
                    IsDeleted = site.IsDeleted,
                    Guid = site.Guid,
                    FacebookLink = site.FacebookLink,
                    InstagramLink = site.InstagramLink,
                    YoutubeLink = site.YoutubeLink,
                    PublicEmailAddress = site.PublicEmailAddress,
                    PublicContactNumber = site.PublicContactNumber,
                    CompanyLogo = site.CompanyLogo
                };
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }

        }

        public SiteModel Get(int id) {
            try
            {
                var site = _siteRepository.Get(id);
                return new SiteModel()
                {
                    Id = site.Id,
                    AccountId = site.AccountId,
                    Name = site.Name,
                    Address = site.Address,
                    City = site.City,
                    PostalCode = site.PostalCode,
                    AdditionalLocationInfo = site.AdditionalLocationInfo,
                    PhoneNumber = site.PhoneNumber,
                    FaxNumber = site.FaxNumber,
                    ContactPerson = site.ContactPerson,
                    ContactPersonNumber = site.ContactPersonNumber,
                    CreatedDate = site.CreatedDate,
                    LastUpdatedDate = site.LastUpdatedDate,
                    IsActive = site.IsActive,
                    ObjectNumber = site.ObjectNumber,
                    IsDeleted = site.IsDeleted,
                    Guid = site.Guid,
                    FacebookLink = site.FacebookLink,
                    InstagramLink = site.InstagramLink,
                    YoutubeLink = site.YoutubeLink,
                    PublicEmailAddress = site.PublicEmailAddress,
                    PublicContactNumber = site.PublicContactNumber,
                    CompanyLogo = site.CompanyLogo,
                    GDPRTitle = site.GDPRTitle,
                    GDPRContent = string.IsNullOrEmpty(site.GDPRContent) ? site.GDPRContent: site.GDPRContent.Replace("<br>", "\n")
                };
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
            
        }
        public List<SiteModel> GetByAccountIdAndUserId(int accountId,int userId, int moduleId)
        {
            try
            {
                var dataList = new List<Site>();
                if (accountId == 0)
                {
                    dataList = _siteRepo.GetAllByRawString("SELECT * FROM Sites").ToList();//.GetAll();
                }
                else
                {
                    dataList = _siteRepo.GetAllByRawString($"SELECT * FROM Sites WHERE AccountId = {accountId}").ToList();//.GetAllByAccountId(accountId);
                }

                var data = dataList.Where(x => x.IsDeleted == false && x.IsActive == true).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                   //Account = entityToModel(s.Account),
                   Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted,
                    Guid = s.Guid,
                    FacebookLink = s.FacebookLink,
                    InstagramLink = s.InstagramLink,
                    YoutubeLink = s.YoutubeLink,
                    PublicEmailAddress = s.PublicEmailAddress,
                    PublicContactNumber = s.PublicContactNumber,
                    CompanyLogo = s.CompanyLogo
                })
                .OrderBy(s => s.Name)
                .ToList();

                var rt = userRepository.GetRoleByUserId(userId);
                if (rt.Role.Name.ToLower() != "admin")
                {
                    var modules = _userModuleRoleRepository.GetUserModuleRolesById(userId).Where(x=>x.ModuleId == moduleId);

                    if (modules.Any())
                    {
                        data = data.Where(x => modules.Select(r => r.SiteId).Contains(x.Id)).ToList();
                    }
                    else
                    {
                        return new List<SiteModel>();
                    }
                }


                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }

        }

        public List<SiteModel> GetByAccountIdAndUserIdArray(int accountId, int userId, int[] moduleId)
        {
            try
            {
                var dataList = new List<Site>();
                if (accountId == 0)
                {
                    dataList = _siteRepository.GetAll();
                }
                else
                {
                    dataList = _siteRepository.GetAllByAccountId(accountId);
                }

                var data = dataList.Where(x => x.IsDeleted == false).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Guid = s.Guid,
                    //Account = entityToModel(s.Account),
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted,
                    FacebookLink = s.FacebookLink,
                    InstagramLink = s.InstagramLink,
                    YoutubeLink = s.YoutubeLink,
                    PublicEmailAddress = s.PublicEmailAddress,
                    PublicContactNumber = s.PublicContactNumber,
                    CompanyLogo = s.CompanyLogo
                })
                .OrderBy(s => s.Name)
                .ToList();

                var rt = userRepository.GetRoleByUserId(userId);
                if (rt.Role.Name.ToLower() != "admin")
                {
                    var modules = _userModuleRoleRepository.GetUserModuleRolesById(userId).Where(x => moduleId.Contains(x.ModuleId));

                    if (modules.Any())
                    {
                        data = data.Where(x => modules.Select(r => r.SiteId).Contains(x.Id)).ToList();
                    }
                    else
                    {
                        return new List<SiteModel>();
                    }
                }


                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }

        }

        public List<SiteModel> GetByAccountId(int id)
        {
            try
            {
                var dataList = new List<Site>();
                if (id == 0)
                {
                    dataList = _siteRepository.GetAll();
                }
                else
                {
                    dataList = _siteRepository.GetAllByAccountId(id);
                }

               var data = dataList.Where(x => x.IsDeleted == false && x.IsActive == true).
               Select(s => new SiteModel()
               {
                   Id = s.Id,
                   AccountId = s.AccountId,
                   //Account = entityToModel(s.Account),
                   Name = s.Name,
                   Address = s.Address,
                   City = s.City,
                   PostalCode = s.PostalCode,
                   AdditionalLocationInfo = s.AdditionalLocationInfo,
                   PhoneNumber = s.PhoneNumber,
                   FaxNumber = s.FaxNumber,
                   ContactPerson = s.ContactPerson,
                   Guid = s.Guid,
                   ContactPersonNumber = s.ContactPersonNumber,
                   CreatedDate = s.CreatedDate,
                   LastUpdatedDate = s.LastUpdatedDate,
                   IsActive = s.IsActive,
                   IsDeleted = s.IsDeleted,
                   FacebookLink = s.FacebookLink,
                   InstagramLink = s.InstagramLink,
                   YoutubeLink = s.YoutubeLink,
                   PublicEmailAddress = s.PublicEmailAddress,
                   PublicContactNumber = s.PublicContactNumber,
                   CompanyLogo = s.CompanyLogo,
               })
               .OrderBy(s => s.Name)
               .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
            
        }

        public List<SiteModel> GetByAccountId2(int id)
        {
            try
            {
                var dataList = new List<Site>();
                if (id == 0)
                {
                    dataList = _siteRepository.GetAll();
                }
                else
                {
                    dataList = _siteRepository.GetAllByAccountId(id);
                }

                var data = dataList.Where(x => x.IsDeleted == false).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Guid = s.Guid,
                    //Account = entityToModel(s.Account),
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted,
                    FacebookLink = s.FacebookLink,
                    InstagramLink = s.InstagramLink,
                    YoutubeLink = s.YoutubeLink,
                    PublicEmailAddress = s.PublicEmailAddress,
                    PublicContactNumber = s.PublicContactNumber,
                    CompanyLogo = s.CompanyLogo
                })
                .OrderBy(s => s.Name)
                .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }

        }

        public List<SiteModel> GetAll() {
            try
            {
                var data = _siteRepository.GetAll().Where(x=>x.IsDeleted == false).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Guid = s.Guid,
                    Account = entityToModel(s.Account),
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted,
                    FacebookLink = s.FacebookLink,
                    InstagramLink = s.InstagramLink,
                    YoutubeLink = s.YoutubeLink,
                    PublicEmailAddress = s.PublicEmailAddress,
                    PublicContactNumber = s.PublicContactNumber,
                    CompanyLogo = s.CompanyLogo
                })
                .OrderBy(s => s.Account.Name)
                .ThenBy(s => s.Name)
                .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }
            
        }

        public List<SiteModel> GetAllByAccountId(int accountId)
        {
            try
            {
                var data = _siteRepository.GetAllByAccountId(accountId).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Account = entityToModel(s.Account),
                    Guid = s.Guid,
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted,
                    FacebookLink = s.FacebookLink,
                    InstagramLink = s.InstagramLink,
                    YoutubeLink = s.YoutubeLink,
                    PublicEmailAddress = s.PublicEmailAddress,
                    PublicContactNumber = s.PublicContactNumber,
                    CompanyLogo = s.CompanyLogo
                })
                .OrderBy(s => s.Account.Name)
                .ThenBy(s => s.Name)
                .ToList();
                return data;
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
        }

        public bool SaveSite(SiteModel model) {
            try
            {

                //if (model.Id == 0 || model.Id == null)
                //{
                //    return false;
                //}

                if (!string.IsNullOrEmpty(model.CompanyLogoBase64)) 
                {
                    model.CompanyLogo = Tools.File.SaveFileToBlob(new FileModel()
                    {
                        Id = model.Id,
                        AccountId = model.AccountId,
                        FileName = $"Photo_{model.Id}",
                        ModuleType = "SiteLogo",
                        ByteString = model.CompanyLogoBase64.Split(',')[1],
                        FileType = ".jpg"
                    });
                }


                _siteRepository.SaveSite(model);
                if (model.Id == 0)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                     LogMessagingSettings.Save,
                     LogMessagingSettings.MessageCreateSite,
                     model.AccountId, 0);
                }
                
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
            }

            return true;
        }

        public void DeleteSite(int id) {
            try
            {
                _siteRepository.DeleteSite(id);
                _logRepository.Log(LogTypeEnum.Information,
                     LogMessagingSettings.Delete,
                     LogMessagingSettings.MessageDeleteSite, 0, 0);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
            }
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Guid = entity.Guid,
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted,
                CompanyLogo = entity.CompanyLogo
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        public List<SiteTaskModel> GetNotReadyTasks(int accountId)
        {
            var tasks = new List<SiteTaskModel>();

            return tasks;
        }
    }
}
