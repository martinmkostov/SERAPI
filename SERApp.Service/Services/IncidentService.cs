﻿using MoreLinq;
using Newtonsoft.Json;
using OfficeOpenXml;
using RazorEngine;
using RazorEngine.Templating;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.HtmlTemplatesModel;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml.Drawing.Chart.Style;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;

namespace SERApp.Service.Services
{
    public interface IIncidentService
    {
        int AddIncident(IncidentModel model);
        IEnumerable<IncidentListModel> GetIncidents(int accountId = 0, int siteId = 0, string selectedDate = "", string selectedEndDate = "", int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "");
        WeekReportOverallReportModel GetIncidentRange(WeekReportModel model);
        IncidentModel GetIncident(int id);
        void DeleteIncident(int id);
        void EditIncident(IncidentModel model);
        IEnumerable<SectionModel> GetSectionModels();
        void AddReports(List<ReportModel> models);
        string GetHtmlTemplateDayReport(DateTime date, int siteId);
        byte[] ExportStatisticsByCategory(List<IncidentStatisticsModel> model, bool isStacked);
        byte[] ExportStatisticsByDate(List<IncidentStatisticsModel> model, bool isStacked);
    }
    public class IncidentService : IIncidentService
    {
        private IIncidentRepository _repository;
        private IType3Repository _type3Repository;
        private IRepository<Type1> _type1Repository;
        private ISectionRepository _sectionRepository;
        private IRepository<Type2> _type2Repository;
        private IRepository<IncidentGuard> _incidentGuardRepository;
        private IRepository<Guard> _guardRepository;
        private ISummaryService _summaryService;
        private IRepository<Report> _reportRepository;
        private IRepository<ReportType> _reportTypeRepository;
        private IRyckrapportDataRepository _ryckyrapportDataRepository;
        private IRepository<Site> _siteRepository;
        private IRepository<Type1FieldValue> _type1FieldValueRepository;
        private IRepository<Account> _accountRepository;
        
        public IncidentService()
        {
            _siteRepository = new Repository<Site>();
            _repository = new IncidentRepository();
            _incidentGuardRepository = new Repository<IncidentGuard>();
            _reportRepository = new Repository<Report>();
            _reportTypeRepository = new Repository<ReportType>();
            _guardRepository = new Repository<Guard>();
            _summaryService = new SummaryService();
            _type3Repository = new Type3Repository();
            _type1Repository = new Repository<Type1>();
            _type2Repository = new Repository<Type2>();
            _sectionRepository = new SectionRepository();
            _ryckyrapportDataRepository = new RyckrapportDataRepository();
            _type1FieldValueRepository = new Repository<Type1FieldValue>();
            _accountRepository = new Repository<Account>();

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
        }

        public int AddIncident(IncidentModel model)
        {
            //convert the dates
            var d = model.TimeDate;
            var t = model.TimeHour;

            if (d != null && d != null)
            {
                model.Time = new DateTime(d.year, d.month, d.day, t.hour, t.minute, t.second);
            }
            else {
                model.Time = DateTime.Now;
            }

            if (model.TimeZoneOffset != 0)
            {
                //model.Time = model.Time.AddHours(model.TimeZoneOffset * -1);
            }
            
            if (model.Reports != null)
            {
                foreach (var r in model.Reports)
                {
                    var nbDate = r.nbDate;
                    var nbTime = r.nbTime;
                    if (nbDate != null)
                    {
                        r.Date = new DateTime(nbDate.year, nbDate.month, nbDate.day);
                    }

                    if (nbTime != null)
                    {
                        r.Time = new DateTime(nbDate.year, nbDate.month, nbDate.day, nbTime.hour, nbTime.minute, nbTime.second);
                    }             

                    if (r.RyckrapportDatas != null)
                    {
                        foreach (var s in r.RyckrapportDatas)
                        {

                            s.Tenant = null;
                            //var rModel = new RyckrapportData()
                            //{
                            //    Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, s.Time.hour, s.Time.minute, s.Time.second),
                            //    TenantId = s.TenantId,
                            //};
                            //_ryckyrapportDataRepository.Save(rModel);

                            //s = null;
                            //s.RyckrapportDataId = rModel.RyckrapportDataId;


                            foreach (var ds in s.RyckrapportDataSections)
                            {
                                if (ds.SectionId <= 0)
                                {
                                    var m = new Section()
                                    {
                                        SectionValue = ds.Section.SectionValue,
                                        SiteId = model.SiteId,
                                    };
                                    _sectionRepository.Save(m);

                                    ds.Section = null;
                                    ds.SectionId = m.SectionId;
                                }
                                else
                                {
                                    ds.Section = null;
                                }

                            }
                        }
                    }

                }
            }

            foreach (var newGuard in model.IncidentGuard)
            {
                var record = _guardRepository.GetByPredicate(x => x.Name == newGuard.Guard.Name && x.SiteId == model.SiteId).FirstOrDefault();

                if (record != null)
                {
                    newGuard.GuardId = record.Id;
                }
                else
                {
                    var nGuard = _guardRepository.Save(new Guard()
                    {
                        Name = newGuard.Guard.Name,
                        CreatedDate = DateTime.Now,
                        SiteId = model.SiteId
                    });

                    newGuard.GuardId = nGuard.Id;
                }
            }

            if (model.Id > 0)
            {
              
                var existingIncidentGuards = _incidentGuardRepository.GetByPredicate(x =>  x.IncidentId == model.Id).ToList();
                existingIncidentGuards.ForEach(x => 
                {
                    _incidentGuardRepository.Delete(x.Id);
                });

                var existingReports = _reportRepository.GetByPredicate(x => x.IncidentId == model.Id).ToList();
                existingReports.ForEach(x => 
                {
                    _reportRepository.Delete(x.Id);
                });


                var existingIncident = _repository.Get(model.Id);

                existingIncident.Type1Id = model.Type1Id;
                existingIncident.Type2Id = model.Type2Id;
                existingIncident.Type3Id = model.Type3Id;
                existingIncident.PoliceResponse = model.PoliceResponse;
                existingIncident.Quantity = model.Quantity;
                existingIncident.TenantId = model.TenantId;
                existingIncident.Time = model.Time;
                existingIncident.ActionDescription = model.ActionDescription;
                existingIncident.WithBlueLight = model.WithBlueLight;
                existingIncident.BlueLightOption = model.BlueLightOption;
                existingIncident.Image1 = model.Image1;
                existingIncident.Image2 = model.Image2;
                existingIncident.Image3 = model.Image3;
                existingIncident.Image4 = model.Image4;
                existingIncident.Image5 = model.Image5;

                    

                existingIncident.IncidentGuard = model.IncidentGuard.Select(x => new IncidentGuard() {
                    IncidentId = existingIncident.Id,
                    GuardId = x.GuardId
                }).ToList();

                existingIncident.Reports = model.Reports != null?  model.Reports.Select(x => new Report()
                {                    
                    VisibleEffect = x.VisibleEffect,
                    AlarmReset = x.AlarmReset,
                    PoliceCalled = x.PoliceCalled,
                    KManInformed = x.KManInformed,
                    KManUnavailable = x.KManUnavailable,
                    AlarmTime = x.alarmTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.alarmTime.hour, x.alarmTime.minute, x.alarmTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                    GuardTime = x.guardTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.guardTime.hour, x.guardTime.minute, x.guardTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                    ActionTime = x.actionTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.actionTime.hour, x.actionTime.minute, x.actionTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                    OrderNumber = x.OrderNumber,
                    AdditionalText = x.AdditionalText,
                    Address = x.Address,
                    PoliceAuthority = x.PoliceAuthority,
                    Cause = x.Cause,
                    Date = x.Date,
                    By = x.By,
                    GuardId = x.GuardId,
                    IncidentText = x.IncidentText,
                    Phone = x.Phone,
                    Location = x.Location,
                    Plaintiff = x.Plaintiff,
                    PoliceReport = x.PoliceReport,
                    Resolution = x.Resolution,
                    Time = x.Time,
                    TenantId = x.TenantId,
                    ReportTypeId = x.ReportTypeId, //x.ReportTypeId,
                    ReportImages = x.ReportImages!=null? x.ReportImages.Select(y => new ReportImage()
                    {
                        Image = y.Image
                    }).ToList(): new List<ReportImage>(),
                    ClosedFrom = x.nbTimeClosedFrom != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.nbTimeClosedFrom.hour, x.nbTimeClosedFrom.minute, x.nbTimeClosedFrom.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                    ClosedTo = x.nbTimeClosedTo != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.nbTimeClosedTo.hour, x.nbTimeClosedTo.minute, x.nbTimeClosedTo.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                    RyckrapportDatas = x.RyckrapportDatas!=null? x.RyckrapportDatas.Select(r => new RyckrapportData()
                    {
                        TenantId = r.TenantId,
                        Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, r.Time.hour, r.Time.minute, r.Time.second),
                        RyckrapportDataSections = r.RyckrapportDataSections!= null? r.RyckrapportDataSections.Select(v => new RyckrapportDataSection()
                        {
                            SectionId = v.SectionId,
                        }).ToList(): new List<RyckrapportDataSection>()
                    }).ToList(): new List<RyckrapportData>()
                }).ToList() : new List<Report>();

                _repository.Update(existingIncident);

                //var existingFieldValues = this._type1FieldValueRepository.GetByPredicate(e => e.IncidentId == existingIncident.Id).ToList();

                var existingFieldValues = this._type1FieldValueRepository.GetAllByRawString($"SELECT * FROM Field_Values WHERE IncidentId = {existingIncident.Id}").ToList();

                var existingFieldValueIds = existingFieldValues.Select(x => x.Id);

                string csv = String.Join(",", existingFieldValueIds.Select(x => x.ToString()).ToArray());

                if (existingFieldValueIds.Any()) 
                {
                    this._type1FieldValueRepository.ExecuteRawSql($"DELETE Field_Values WHERE Id IN ({csv})");
                }

                //existingFieldValues.ForEach(efv => 
                //{
                //    _type1FieldValueRepository.Delete(efv.Id);
                //});

                model.FieldValues.ForEach(fieldValue =>
                {
                    //if (fieldValue.Type == "TIME")
                    //{
                    //    var test = JsonConvert.SerializeObject(fieldValue.Value);

                    //    var g = JsonConvert.DeserializeObject<TimeModel>(test);
                    //    g.hour = model.Time.Hour;
                    //    g.minute = model.Time.Minute;
                    //    g.second = model.Time.Second;

                    //    fieldValue.Value = JsonConvert.SerializeObject(g);

                    //    //fieldValue.Value = JsonConvert.SerializeObject("{'hour': "+model.Time.Hour+", 'minute': "+model.Time.Minute+"}");
                    //}


                    //if (fieldValue.Type == "DATE")
                    //{
                    //    var test = JsonConvert.SerializeObject(fieldValue.Value);

                    //    var g = JsonConvert.DeserializeObject<DateModel>(test);
                    //    g.year = model.Time.Year;
                    //    g.month = model.Time.Month;
                    //    g.day = model.Time.Day;

                    //    fieldValue.Value = JsonConvert.SerializeObject(g);

                    //    //fieldValue.Value = JsonConvert.SerializeObject("{'year': " + model.Time.Year + ", 'month': " + model.Time.Month + ", 'day': "+model.Time.Day+"}");
                    //}


                    _type1FieldValueRepository.Save(new Type1FieldValue
                    {
                        IncidentId = existingIncident.Id,
                        Type1_FieldId = fieldValue.FieldId,
                        Value = JsonConvert.SerializeObject(fieldValue.Value),
                        Type = fieldValue.Type
                    });
                });
            }
            else
            {
                var newIncident = new Incident()
                {
                    ActionDescription = model.ActionDescription,
                    Type2Id = model.Type2Id,
                    Type1Id = model.Type1Id,
                    Type3Id = model.Type3Id,
                    PoliceResponse = model.PoliceResponse,
                    Quantity = model.Quantity,
                    TenantId = model.TenantId,
                    SiteId = model.SiteId,
                    Time = model.Time,
                    AccountId = model.AccountId,
                    IsDeleted = false,
                    BlueLightOption = model.BlueLightOption,
                    WithBlueLight = model.WithBlueLight,
                    IncidentGuard = model.IncidentGuard.Select(x => new IncidentGuard()
                    {
                        GuardId = x.GuardId
                    }).ToList(),
                    Reports = model.Reports != null ? model.Reports.Select(x =>
                    {
                        return new Report
                        {
                            VisibleEffect = x.VisibleEffect,
                            AlarmReset = x.AlarmReset,
                            PoliceCalled = x.PoliceCalled,
                            KManInformed = x.KManInformed,
                            KManUnavailable = x.KManUnavailable,
                            AlarmTime = x.alarmTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.alarmTime.hour, x.alarmTime.minute, x.alarmTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                            GuardTime = x.guardTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.guardTime.hour, x.guardTime.minute, x.guardTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                            ActionTime = x.actionTime != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.actionTime.hour, x.actionTime.minute, x.actionTime.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                            OrderNumber = x.OrderNumber,
                            AdditionalText = x.AdditionalText,
                            Address = x.Address,
                            By = x.By,
                            PoliceAuthority = x.PoliceAuthority,
                            Cause = x.Cause,
                            Date = x.Date,
                            GuardId = x.GuardId,
                            IncidentText = x.IncidentText,
                            Phone = x.Phone,
                            Location = x.Location,
                            Plaintiff = x.Plaintiff,
                            PoliceReport = x.PoliceReport,
                            Resolution = x.Resolution,
                            Time = x.Time,
                            TenantId = x.TenantId,
                            ReportTypeId = x.ReportTypeId, //x.ReportTypeId,
                            ReportImages = x.ReportImages != null ? x.ReportImages.Select(y => new ReportImage()
                            {
                                Image = y.Image
                            }).ToList() : new List<ReportImage>(),
                            ClosedFrom = x.nbTimeClosedFrom != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.nbTimeClosedFrom.hour, x.nbTimeClosedFrom.minute, x.nbTimeClosedFrom.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                            ClosedTo = x.nbTimeClosedTo != null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, x.nbTimeClosedTo.hour, x.nbTimeClosedTo.minute, x.nbTimeClosedTo.second) : new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1),
                            RyckrapportDatas = x.RyckrapportDatas == null ? null : x.RyckrapportDatas.Select(r => new RyckrapportData()
                            {
                                TenantId = r.TenantId,
                                Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, r.Time.hour, r.Time.minute, r.Time.second),
                                RyckrapportDataSections = r.RyckrapportDataSections.Select(v => new RyckrapportDataSection()
                                {
                                    SectionId = v.SectionId,
                                }).ToList()
                            }).ToList()
                        };
                    }).ToList() : new List<Report>(),
                    Image1 = model.Image1,
                    Image2 = model.Image2,
                    Image3 = model.Image3,
                    Image4 = model.Image4,
                    Image5 = model.Image5,
                };

                _repository.Save(newIncident);

                model.FieldValues.ForEach(fieldValue =>
                {
                    _type1FieldValueRepository.Save(new Type1FieldValue
                    {
                        IncidentId = newIncident.Id,
                        Type1_FieldId = fieldValue.FieldId,
                        Value = JsonConvert.SerializeObject(fieldValue.Value),
                        Type = fieldValue.Type
                    });
                });

                model.Id = newIncident.Id;
            }

            return model.Id;
        }

        public void AddReports(List<ReportModel> models)
        {
            throw new NotImplementedException();
        }

        public void DeleteIncident(int id)
        {
            //var existingData = _repository.Get(id);
            //existingData.IsDeleted = true;
            //_repository.Update(existingData);
            _repository.Delete(id);
        }

        public void EditIncident(IncidentModel model)
        {
            var existingData = _repository.Get(model.Id);
            existingData.Type1Id = model.Type1Id;
            existingData.PoliceResponse = model.PoliceResponse;
            existingData.Quantity = model.Quantity;
            existingData.TenantId = model.TenantId;
            existingData.Time = model.Time;
            _repository.Update(existingData);
        }

        public IncidentModel GetIncident(int id)
        {
            var x = _repository.GetWithChildsIncludedV2(id);
            var reports = this._reportRepository.GetAllByRawString($"SELECT * FROM Reports WHERE IncidentId = {id}").ToList();

            try
            {
                var model = new IncidentModel()
                {
                    ActionDescription = x.ActionDescription,
                    Type2Id = x.Type2Id,
                    Id = x.Id,
                    Type1Id = x.Type1Id,
                    Type3Id = x.Type3Id,
                    Quantity = x.Quantity,
                    PoliceResponse = x.PoliceResponse,
                    TenantId = x.TenantId,
                    SiteId = x.SiteId,
                    Time = x.Time,
                    BlueLightOption = x.BlueLightOption,
                    WithBlueLight = x.WithBlueLight,
                    Type3 = x.Type3 != null ? new Type3Model()
                    {
                        Id = x.Type3.Id,
                        Name = x.Type3.Name,
                        // TenantId = x.Type3.TenantId
                    } : new Type3Model(),
                    Type2 = x.Type2 != null ? new Type2Model()
                    {
                        Id = x.Type2.Id,
                        Name = x.Type2.Name
                    } : new Type2Model(),
                    Type1 = x.Type1 != null ? new Type1Model()
                    {
                        Id = x.Type1.Id,
                        Name = x.Type1.Name,
                        IsReportRequired = x.Type1.IsReportRequired,
                        RequiredReport = x.Type1.RequiredReport.Split(',')
                        .Select(r => new ReportTypeModel()
                        {
                            Id = ConvertToInt(r),
                        })
                        .ToList()
                    } : new Type1Model(),
                    Tenant = x.Tenant != null ? new TenantModel()
                    {
                        Id = x.Tenant.Id,
                        Name = x.Tenant.Name,
                        InvoiceAddress = x.Tenant.VisitingAddress,
                        City = x.Tenant.City,
                        ZipCode = x.Tenant.ZipCode,
                        Email = x.Tenant.Email,
                        Phone = x.Tenant.Phone,
                    } : new TenantModel(),
                    IncidentGuard = x.IncidentGuard?.Select(y => new IncidentGuardModel()
                    {
                        Id = y.Id,
                        Guard = y.Guard != null ? new GuardModel()
                        {
                            Id = y.Guard.Id,
                            Name = y.Guard.Name,
                        }: null
                    }).ToList(),
                    Reports = new List<ReportModel>(),
                    AccountId = x.AccountId,
                    Image1 = x.Image1,
                    Image2 = x.Image2,
                    Image3 = x.Image3,
                    Image4 = x.Image4,
                    Image5 = x.Image5,
                    
                };


               
                foreach (var r in reports)
                {
                 
                    var type = _reportTypeRepository.Get(r.ReportTypeId);

                    var rm = new ReportModel();

                    rm.nbTimeClosedFrom = r.ClosedFrom.HasValue ? new TimeModel() { hour = r.ClosedFrom.Value.Hour, minute = r.ClosedFrom.Value.Minute, second = r.ClosedFrom.Value.Second } : new TimeModel();
                    rm.nbTimeClosedTo = r.ClosedTo.HasValue ? new TimeModel() { hour = r.ClosedTo.Value.Hour, minute = r.ClosedTo.Value.Minute, second = r.ClosedTo.Value.Second } : new TimeModel();
                    rm.VisibleEffect = r.VisibleEffect;
                    rm.AlarmReset = r.AlarmReset;
                    rm.PoliceCalled = r.PoliceCalled;
                    rm.KManInformed = r.KManInformed;
                    rm.KManUnavailable = r.KManUnavailable;
                    rm.AlarmTime = r.AlarmTime;
                    rm.GuardTime = r.GuardTime;
                    rm.ActionTime = r.ActionTime;
                    rm.OrderNumber = r.OrderNumber;
                    rm.actionTime = r.ActionTime.HasValue ? new TimeModel()
                    {
                        hour = r.ActionTime.Value.Hour,
                        minute = r.ActionTime.Value.Minute,
                        second = r.ActionTime.Value.Second,
                    } : new TimeModel() { };

                    rm.alarmTime = r.AlarmTime.HasValue ? new TimeModel()
                    {
                        hour = r.AlarmTime.Value.Hour,
                        minute = r.AlarmTime.Value.Minute,
                        second = r.AlarmTime.Value.Second,
                    } : new TimeModel() { };

                    rm.guardTime = r.GuardTime.HasValue ? new TimeModel()
                    {
                        hour = r.GuardTime.Value.Hour,
                        minute = r.GuardTime.Value.Minute,
                        second = r.GuardTime.Value.Second,
                    } : new TimeModel() { };


                    rm.AdditionalText = r.AdditionalText;
                    rm.Address = r.Address;
                    rm.PoliceAuthority = r.PoliceAuthority;
                    rm.Cause = r.Cause;
                    rm.By = r.By;
                    rm.Date = r.Date;
                    rm.GuardId = r.GuardId;
                    rm.Id = r.Id;
                    rm.IncidentText = r.IncidentText;
                    rm.Location = r.Location;
                    rm.Phone = r.Phone;
                    rm.Plaintiff = r.Plaintiff;
                    rm.ReportTypeId = r.ReportTypeId;
                    rm.PoliceReport = r.PoliceReport;
                    rm.Time = r.Time;
                    rm.TenantId = r.TenantId;
                    rm.Resolution = r.Resolution;
                    rm.ReportImages = r.ReportImages.Select(e => new ReportImageModel()
                    {
                        ReportId = e.ReportId,
                        ReportImageId = e.ReportImageId,
                        Image = e.Image
                    }).ToList();

                    if (type != null)
                    {
                        rm.ReportType = new ReportTypeModel()
                        {
                            Id = type.Id,
                            Name = type.Name
                        };
                    }
                  
                    model.Reports.Add(rm);
                }

                
                foreach (var r in model.Reports)
                {
                    if (r.Date.HasValue)
                    {
                        r.nbDate = new SERApp.Models.Common.DateModel()
                        {
                            year = r.Date.Value.Year,
                            month = r.Date.Value.Month,
                            day = r.Date.Value.Day
                        };
                    }

                    if (r.Time.HasValue)
                    {
                        r.nbTime = new SERApp.Models.Common.TimeModel()
                        {
                            hour = r.Time.Value.Hour,
                            minute = r.Time.Value.Minute,
                            second = r.Time.Value.Second
                        };
                    }

                    r.RyckrapportDatas = _ryckyrapportDataRepository.GetAllRyckrapportChildIncluded().Where(t => t.ReportId == r.Id).ToList().Select(t => new RyckrapportDataModel()
                    {
                        RyckrapportDataId = t.RyckrapportDataId,
                        RyckrapportDataSections = t.RyckrapportDataSections.Select(v => new RyckrapportDataSectionModel()
                        {
                            SectionId = v.SectionId,
                            RyckrapportDataSectionId = v.RyckrapportDataSectionId,
                            Section = new SectionModel()
                            {
                                SectionId = v.SectionId,
                                SectionValue = v.Section.SectionValue,
                                SiteId = v.Section.SiteId,
                            }
                        }),
                        Tenant = new TenantModel()
                        {
                            Id = t.TenantId,
                            Name = t.Tenant.Name
                        },
                        Time = new TimeModel()
                        {
                            hour = t.Time.Hour,
                            minute = t.Time.Minute,
                            second = t.Time.Second
                        }
                    }).ToList();

                }


                var fieldValues = _repository.GetType1FieldValue(id).ToList();
                //var fieldValues = this._type1FieldValueRepository.GetByPredicate(e => e.IncidentId == id).ToList();
                foreach (var fieldValue in fieldValues)
                {
                    try
                    {
                        model.FieldValues.Add(new IncidentReportFieldValue
                        {
                            FieldId = fieldValue.Type1_FieldId,
                            Type = fieldValue.Type,
                            Value = JsonConvert.DeserializeObject<object>(fieldValue.Value)
                        });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<IncidentModel> GetIncidentsBySiteId(int siteId)
        {
           return _repository.GetByPredicate(x => x.SiteId == siteId).Select(x => new IncidentModel()
           {
               AccountId = x.AccountId,
               ActionDescription = x.ActionDescription,
               Type2Id = x.Type2Id,
               Id = x.Id,
               Type1Id = x.Type1Id,
               Type3Id = x.Type3Id,
               Quantity = x.Quantity,
               PoliceResponse = x.PoliceResponse,
               TenantId = x.TenantId,
               Time = x.Time,
               WithBlueLight = x.WithBlueLight,
               BlueLightOption = x.BlueLightOption,
               SiteId = x.SiteId,
               Reports = x.Reports.Select(r => new ReportModel()
               {
                   AdditionalText = r.AdditionalText,
                   Address = r.Address,
                   PoliceAuthority = r.PoliceAuthority,
                   Cause = r.Cause,
                   Date = r.Date,
                   GuardId = r.GuardId,
                   Id = r.Id,
                   IncidentText = r.IncidentText,
                   Location = r.Location,
                   Phone = r.Phone,
                   Plaintiff = r.Plaintiff,
                   ReportTypeId = r.ReportTypeId,
                   PoliceReport = r.PoliceReport,
                   Time = r.Time,
                   TenantId = r.TenantId,
                   Resolution = r.Resolution,
                   By = r.By,
                   ReportType = new ReportTypeModel()
                   {
                       Id = r.ReportType.Id,
                       Name = r.ReportType.Name
                   }
               }).ToList(),
               Type3 = x.Type3 != null ? new Type3Model()
               {
                   Id = x.Type3.Id,
                   Name = x.Type3.Name,
                   // TenantId = x.Type3.TenantId
               } : new Type3Model(),
               Type2 = x.Type2 != null ? new Type2Model()
               {
                   Id = x.Type2.Id,
                   Name = x.Type2.Name
               } : new Type2Model(),
               Type1 = x.Type1 != null ? new Type1Model()
               {
                   Id = x.Type1.Id,
                   Name = x.Type1.Name,
                   IsReportRequired = x.Type1.IsReportRequired,
                   RequiredReport = x.Type1.RequiredReport.Split(',')
                    .Select(r => new ReportTypeModel()
                    {
                        Id = ConvertToInt(r),
                    })
                    .ToList()
               } : new Type1Model(),
               Tenant = x.Tenant != null ? new TenantModel()
               {
                   Id = x.Tenant.Id,
                   Name = x.Tenant.Name,

               } : new TenantModel(),
               IncidentGuard = x.IncidentGuard.Select(y => new IncidentGuardModel()
               {
                   Id = y.Id,
                   Guard = new GuardModel()
                   {
                       Id = y.Guard.Id,
                       Name = y.Guard.Name,
                   }
               }).ToList()
           }).ToList();
        }
        public WeekReportOverallReportModel GetIncidentRange(WeekReportModel model)
        {
            var data = _repository.GetAllIncidentsChildIncluded(model.SiteId);
            var comepleteData = data.ToList();
            if (model.AccountId > 0)
            {
                data = data.Where(x => x.AccountId == model.AccountId);
            }

            if (model.SiteId > 0)
            {
                data = data.Where(x => x.SiteId == model.SiteId);
            }
            
            var from = DateTime.Parse($"{model.From.year}-{model.From.month}-{model.From.day}");
            var to = DateTime.Parse($"{model.To.year}-{model.To.month}-{model.To.day}");

            var tempData = data.Where(x => !x.IsDeleted).Where(x=>x.Time >= from && x.Time <= to).Select(x => new IncidentModel()
            {
                ActionDescription = x.ActionDescription,
                Type2Id = x.Type2Id,
                Id = x.Id,
                Type1Id = x.Type1Id,
                Type3Id = x.Type3Id,
                Quantity = x.Quantity,
                PoliceResponse = x.PoliceResponse,
                TenantId = x.TenantId,
                Time = x.Time,
                WithBlueLight = x.WithBlueLight,
                BlueLightOption = x.BlueLightOption,
                Reports = x.Reports.Select(r => new ReportModel()
                {
                    AdditionalText = r.AdditionalText,
                    Address = r.Address,
                    PoliceAuthority = r.PoliceAuthority,
                    Cause = r.Cause,
                    Date = r.Date,
                    By = r.By,
                    GuardId = r.GuardId,
                    Id = r.Id,
                    IncidentText = r.IncidentText,
                    Location = r.Location,
                    Phone = r.Phone,
                    Plaintiff = r.Plaintiff,
                    ReportTypeId = r.ReportTypeId,
                    PoliceReport = r.PoliceReport,
                    Time = r.Time,
                    TenantId = r.TenantId,
                    Resolution = r.Resolution,
                    ReportType = new ReportTypeModel()
                    {
                        Id = r.ReportType.Id,
                        Name = r.ReportType.Name
                    }
                }).ToList(),
                Type3 = x.Type3 != null ? new Type3Model()
                {
                    Id = x.Type3.Id,
                    Name = x.Type3.Name,
                    // TenantId = x.Type3.TenantId
                } : new Type3Model(),
                Type2 = x.Type2 != null ? new Type2Model()
                {
                    Id = x.Type2.Id,
                    Name = x.Type2.Name
                } : new Type2Model(),
                Type1 = x.Type1 != null ? new Type1Model()
                {
                    Id = x.Type1.Id,
                    Name = x.Type1.Name,
                    IsReportRequired = x.Type1.IsReportRequired,
                    RequiredReport = x.Type1.RequiredReport.Split(',')
                    .Select(r => new ReportTypeModel()
                    {
                        Id = ConvertToInt(r),
                    })
                    .ToList()
                } : new Type1Model(),
                Tenant = x.Tenant != null ? new TenantModel()
                {
                    Id = x.Tenant.Id,
                    Name = x.Tenant.Name,

                } : new TenantModel(),
                IncidentGuard = x.IncidentGuard.Select(y => new IncidentGuardModel()
                {
                    Id = y.Id,
                    Guard = new GuardModel()
                    {
                        Id = y.Guard.Id,
                        Name = y.Guard.Name,
                    }
                }).ToList()
            }).ToList();


            var toReturn = new List<WeekReportDisplayModel>();

            var difference = from.Date - to.Date;
            var count = Math.Abs(difference.Days);

            for (int x = 0; x <= count; x++)
            {
                var date = from.Date.AddDays(x);
                var pResp = "";
                var qResp = "";
                if (tempData.Where(r => r.Time.ToShortDateString().Equals(date.ToShortDateString())).Any())
                {
                    pResp = tempData.Where(r => r.Time.ToShortDateString()
                    .Equals(date.ToShortDateString()))
                    .Sum(r => r.PoliceResponse).ToString();

                    qResp = tempData.Where(r => r.Time.ToShortDateString()
                       .Equals(date.ToShortDateString()))
                       .Sum(r => r.Quantity).ToString();
                }

                toReturn.Add(new WeekReportDisplayModel()
                {
                    Date = date.DayOfWeek.ToString(),
                    PoliceResponse = pResp,
                    Quantity = qResp,
                    Summary = _summaryService.GetSummary(date, model.AccountId, model.SiteId).SummaryValue
                });
            };
            
            var allIncidents = _repository.GetAllIncidentsChildIncluded(model.SiteId);
            var alltype1 = _type1Repository.GetAll().Where(x=>x.AccountId == model.AccountId && x.SiteId > 0);
            var type1s = new List<WeekTypeReportModel>();
           
            for (int x = 0; x <= count; x++)
            {
                var date = from.Date.AddDays(x);
                var dic = new Dictionary<string, string>();
                foreach (var t in alltype1)
                {
                    var counttype1 = allIncidents.Where(r => r.Time.ToShortDateString().Equals(date.ToShortDateString())
                    && r.Type1 != null && r.Type1.Name == t.Name).Count().ToString();
                    dic.Add(t.Name, counttype1);
                }
                type1s.Add(new WeekTypeReportModel()
                {
                    Day = date.DayOfWeek.ToString(),
                    Columns = dic
                });
            }


            var alltype2 = _type2Repository.GetAll().Where(x => x.AccountId == model.AccountId && x.SiteId > 0).ToList();
            
            var type2s = new List<WeekTypeReportModel>();

            for (int x = 0; x <= count; x++)
            {
                var date = from.Date.AddDays(x);
                var dic = new Dictionary<string, string>();
                foreach (var t in alltype2)
                {
                    var counttype2 = allIncidents.Where(r => r.Time.ToShortDateString().Equals(date.ToShortDateString())
                    && r.Type2 != null && r.Type2.Name == t.Name).Count().ToString();
                    dic.Add(t.Name, counttype2);
                    
                }
                type2s.Add(new WeekTypeReportModel()
                {
                    Day = date.DayOfWeek.ToString(),
                    Columns = dic
                });
            }

            var alltype3 = _type3Repository.GetAll().Where(x=>x.AccountId == model.AccountId && x.SiteId > 0);
            var type3s = new List<WeekTypeReportModel>();

            for (int x = 0; x <= count; x++)
            {
                var date = from.Date.AddDays(x);
                var dic = new Dictionary<string, string>();
                foreach (var t in alltype3)
                {
                    var counttype3 = allIncidents.Where(r => r.Time.ToShortDateString().Equals(date.ToShortDateString())
                    && r.Type3 != null && r.Type3.Name == t.Name).Count().ToString();
                    dic.Add(t.Name, counttype3);
                }
                type3s.Add(new WeekTypeReportModel()
                {
                    Day = date.DayOfWeek.ToString(),
                    Columns = dic
                });
            }

            var monthReportModel = new List<MonthReportModel>();
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.WithoutEmergencyService,
                Id =1,
                EventName = "Health troubles, relief and people assistance",
                EventDescription ="",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 2,
                EventName = "Evacuation alarm without real incident",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 3,
                EventName = "Other incidents",
                EventDescription = "",
            });

            ///
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.EmergencyService,
                Id = 4,
                EventName = "Health troubles, relief and people assistance",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 5,
                EventName = "Real fire or fire detection",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 6,
                EventName = "Indoor accident",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 7,
                EventName = "Outdoor accident",
                EventDescription = "",
            });

            ///
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.WithoutPolice,
                Id = 8,
                EventName = "Undesirables eviction",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 9,
                EventName = "Observations",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 10,
                EventName = "Degradation",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 11,
                EventName = "Theft inside tenants' area",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 12,
                EventName = "Theft in common areas",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 13,
                EventName = "Intrusion on building perimetry",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 14,
                EventName = "Disturbances done by young people",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 15,
                EventName = "Other incivilities / troubles / events",
                EventDescription = "",
            });
            ///

            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.WithPolice,
                Id = 16,
                EventName = "Prostitution/ indecent assault",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 17,
                EventName = "Narcotics traffic or group consumption",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 18,
                EventName = "Other incivilities / troubles",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 19,
                EventName = "Degradations",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 20,
                EventName = "Physical aggression",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 21,
                EventName = "Theft of visitors belongings (pickpockets)",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 22,
                EventName = "Theft inside tenants' area",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 23,
                EventName = "Theft in areas managed by UR",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 24,
                EventName = "Car theft or Theft inside cars",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 25,
                EventName = "Burglary",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 26,
                EventName = "Armed / violent robbery on tenants areas",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 27,
                EventName = "Armed / violent robbery on common areas",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 28,
                EventName = "Crowd movement",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 29,
                EventName = "Cyber attack",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 30,
                EventName = "Suspicious object / Bomb alert by phone",
                EventDescription = "",
            });
            monthReportModel.Add(new MonthReportModel()
            {
                authoritiesInterventionEnum = AuthoritiesInterventionEnum.Hide,
                Id = 31,
                EventName = "Real terrorism event",
                EventDescription = "",
            });

            var sitesIds = comepleteData.Where(r=>r.AccountId == model.AccountId).Select(r => r.SiteId).Distinct();
            var sites = _siteRepository.GetAll().Where(g => g.AccountId == model.AccountId).ToList();
            monthReportModel.ForEach(r => 
            {
                foreach (var id in sitesIds)
                {
                    var sss = comepleteData.Where(t => t.SiteId == id).ToList();
                    
                    r.SiteCount.Add(new SiteCountModel()
                    {
                        Name = sites.SingleOrDefault(b => b.Id == id) != null ? sites.SingleOrDefault(b=>b.Id == id).Name : "",
                        Count = sss.Where(t=>!string.IsNullOrEmpty(t.BlueLightOption)).Count(t => t.BlueLightOption.Trim() == r.EventName.Trim())
                    });
                }
                
                r.Count = comepleteData.Where(t => !string.IsNullOrEmpty(t.BlueLightOption)).Count(t => t.BlueLightOption.Trim() == r.EventName.Trim());
                switch (r.authoritiesInterventionEnum)
                {
                    case AuthoritiesInterventionEnum.WithoutEmergencyService:
                        r.authoritiesInterventionEnumString = "Without emergency services intervention"; 
                        break;
                    case AuthoritiesInterventionEnum.EmergencyService:
                        r.authoritiesInterventionEnumString = "Emergency services call and/or intervention";
                        break;
                    case AuthoritiesInterventionEnum.WithoutPolice:
                        r.authoritiesInterventionEnumString = "Without Police intervention";
                        break;
                    case AuthoritiesInterventionEnum.WithPolice:
                        r.authoritiesInterventionEnumString = "Police call and/or intervention";
                        break;
                    case AuthoritiesInterventionEnum.Hide:
                        r.authoritiesInterventionEnumString = " ";
                        break;
                    default:
                        r.authoritiesInterventionEnumString = " ";
                        break;
                }

               
            });


            return new WeekReportOverallReportModel()
            {
                Model = monthReportModel,
                WeekSummaryDisplay = toReturn,
                Type1Report = type1s,
                Type2Report = type2s,
                Type3Report = type3s
            };
        }

        public IEnumerable<GuardModel> GetGuardsToday(int accountId = 0, int siteId = 0, string selectedDate = "")
        {
            var data = _repository.GetAllIncidentsChildIncluded(siteId);
            if (accountId > 0)
            {
                data = data.Where(x => x.AccountId == accountId);
            }

            if (siteId > 0)
            {
                data = data.Where(x => x.SiteId == siteId);
            }

            if (!string.IsNullOrEmpty(selectedDate))
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParse(selectedDate, out date))
                {
                    data = data.Where(x => x.Time.ToShortDateString().Equals(date.ToShortDateString()));
                }
            }

            var incidents = data.Where(x => !x.IsDeleted).Select(x => new IncidentModel()
            {
                ActionDescription = x.ActionDescription,
                Type2Id = x.Type2Id,
                Id = x.Id,
                Type1Id = x.Type1Id,
                Type3Id = x.Type3Id,
                Quantity = x.Quantity,
                PoliceResponse = x.PoliceResponse,
                TenantId = x.TenantId,
                Time = x.Time,
                Reports = x.Reports.Select(r => new ReportModel()
                {
                    AdditionalText = r.AdditionalText,
                    Address = r.Address,
                    PoliceAuthority = r.PoliceAuthority,
                    Cause = r.Cause,
                    Date = r.Date,
                    GuardId = r.GuardId,
                    Id = r.Id,
                    IncidentText = r.IncidentText,
                    Location = r.Location,
                    Phone = r.Phone,
                    Plaintiff = r.Plaintiff,
                    ReportTypeId = r.ReportTypeId,
                    PoliceReport = r.PoliceReport,
                    Time = r.Time,
                    TenantId = r.TenantId,
                    Resolution = r.Resolution,
                    By = r.By,
                    ReportType = new ReportTypeModel()
                    {
                        Id = r.ReportType.Id,
                        Name = r.ReportType.Name
                    }
                }).ToList(),
                Type3 = x.Type3 != null ? new Type3Model()
                {
                    Id = x.Type3.Id,
                    Name = x.Type3.Name,
                    // TenantId = x.Type3.TenantId
                } : new Type3Model(),
                Type2 = x.Type2 != null ? new Type2Model()
                {
                    Id = x.Type2.Id,
                    Name = x.Type2.Name
                } : new Type2Model(),
                Type1 = x.Type1 != null ? new Type1Model()
                {
                    Id = x.Type1.Id,
                    Name = x.Type1.Name,
                    IsReportRequired = x.Type1.IsReportRequired,
                    RequiredReport = x.Type1.RequiredReport.Split(',')
                      .Select(r => new ReportTypeModel()
                      {
                          Id = ConvertToInt(r),
                      })
                      .ToList()
                } : new Type1Model(),
                Tenant = x.Tenant != null ? new TenantModel()
                {
                    Id = x.Tenant.Id,
                    Name = x.Tenant.Name,

                } : new TenantModel(),
                IncidentGuard = x.IncidentGuard.Select(y => new IncidentGuardModel()
                {
                    Id = y.Id,
                    Guard = new GuardModel()
                    {
                        Id = y.Guard.Id,
                        Name = y.Guard.Name,
                    }
                }).ToList()
            }).ToList();
            var guards = incidents.SelectMany(r => r.IncidentGuard.Select(t => t.Guard)).DistinctBy(x=>x.Name);
            return guards;
        }

        public IEnumerable<IncidentListModel> GetIncidents(int accountId = 0, int siteId = 0, string selectedDate = "",string selectedEndDate= "",int selectedType1 =0,int selectedType2 = 0,int selectedType3= 0, string searchText = "")
        {
            var d =DateTime.Parse(selectedDate);

            var c = DateTime.Parse(selectedEndDate);

            var data = _repository.GetAllForList(siteId, accountId, d,c);

            if (selectedType1 > 0)
            {
                data = data.Where(x => x.Type1Id == selectedType1);
            }
            if (selectedType2 > 0)
            {
                data = data.Where(x => x.Type2Id == selectedType2);
            }
            if (selectedType3 > 0)
            {
                data = data.Where(x => x.Type3Id == selectedType3);
            }

            if (accountId > 0)
            {
                data = data.Where(x => x.AccountId == accountId);
            }

            if (siteId > 0)
            {
                data = data.Where(x => x.SiteId == siteId);
            }

            if (!string.IsNullOrEmpty(searchText) && searchText != "sortAscending")
            {
                data = data.Where(x => !string.IsNullOrEmpty(x.Description) && x.Description.ToLower().Contains(searchText.ToLower()) || x.Guards.Any(v=>v.Name.ToLower().Contains(searchText.ToLower())));
            }

            //if (!string.IsNullOrEmpty(selectedDate))
            //{
            //    DateTime date = DateTime.MinValue;
            //    if (DateTime.TryParse(selectedDate, out date))
            //    {
            //        data = data.Where(x => x.Time.ToShortDateString().Equals(date.ToShortDateString()));
            //    }
            //}

            
            if (!string.IsNullOrEmpty(searchText) && searchText.Contains("sortAscending"))
            {
                return data.OrderBy(x => x.Time).ToList();
            }
            else
            {
                return data.OrderByDescending(x => x.Time).ToList();
            }
        }
        private int ConvertToInt(string number)
        {
            int num = 0;
            Int32.TryParse(number, out num);
            return num;
        }

        public IEnumerable<SectionModel> GetSectionModels()
        {            
            return _sectionRepository.GetSectionIncludeChildren().ToList().Select(x => new SectionModel()
            {
                SectionId = x.SectionId,
                SectionValue = x.SectionValue,
                SiteId = x.SiteId
                //RyckrapportDataSections = x.RyckrapportDataSections.Select(r=>new RyckrapportDataSectionModel()
                //{
                //    SectionId = r.SectionId,
                //    RyckrapportDataId = r.RyckrapportDataId,
                //    RyckrapportDataSectionId = r.RyckrapportDataSectionId,
                //    Section = new SectionModel()
                //    {
                //        SectionValue = r.Section.SectionValue,
                //        SectionId = r.Section.SectionId,                        
                //    }
                //}).ToList()
             
            });
        }

        public byte[] ExportXLS(int accountId = 0, int siteId = 0, string selectedDate = "", string selectedEndDate = "", int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
           
            using (var excel = new ExcelPackage())
            {

                var se = new CultureInfo("sv-SE");
                var incidents = GetIncidents(accountId, siteId, selectedDate, selectedEndDate, selectedType1, selectedType2, selectedType3, searchText);

                var summarySheet = excel.Workbook.Worksheets.Add($"Incidents");
                summarySheet.Column(1).Width = 30;
                summarySheet.Column(2).Width = 30;
                summarySheet.Column(3).Width = 30;

                summarySheet.Column(4).Width = 30;
                summarySheet.Column(5).Width = 30;

                summarySheet.Column(6).Width = 30;
                summarySheet.Column(7).Width = 30;
                summarySheet.Column(8).Width = 30;
                summarySheet.Column(9).Width = 30;
                summarySheet.Column(10).Width = 40;
                summarySheet.Column(11).Width = 30;
                summarySheet.Column(12).Width = 60;

                summarySheet.Cells[1, 1].Value = "Object";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Date";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

                summarySheet.Cells[1, 3].Value = "Time";
                summarySheet.Cells[1, 3].Style.Font.Bold = true;

                summarySheet.Cells[1, 4].Value = "Closed From";
                summarySheet.Cells[1, 4].Style.Font.Bold = true;

                summarySheet.Cells[1, 5].Value = "Closed To";
                summarySheet.Cells[1, 5].Style.Font.Bold = true;


                summarySheet.Cells[1, 6].Value = "Vad";
                summarySheet.Cells[1, 6].Style.Font.Bold = true;


                summarySheet.Cells[1, 7].Value = "Var";
                summarySheet.Cells[1, 7].Style.Font.Bold = true;


                summarySheet.Cells[1, 8].Value = "Vem";
                summarySheet.Cells[1, 8].Style.Font.Bold = true;

                summarySheet.Cells[1, 9].Value = "Quantity";
                summarySheet.Cells[1, 9].Style.Font.Bold = true;

                summarySheet.Cells[1, 10].Value = "Police Response";
                summarySheet.Cells[1, 10].Style.Font.Bold = true;


                summarySheet.Cells[1, 11].Value = "Guards";
                summarySheet.Cells[1, 11].Style.Font.Bold = true;


                summarySheet.Cells[1, 12].Value = "Description";
                summarySheet.Cells[1, 12].Style.Font.Bold = true;


                int startRow = 2;


              
                if (incidents.Select(x => x.SiteId).Distinct().Any())
                {
                    var siteIds = String.Join(",", incidents.Select(x => x.SiteId).Distinct());

                    var sites = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE Id IN ({siteIds})").ToList();


                    incidents.ForEach(x =>
                    {

                        summarySheet.Cells[startRow, 1].Value = sites.SingleOrDefault(r=>r.Id == x.SiteId).Name;
                        summarySheet.Cells[startRow, 2].Value = x.Time.ToString("yyyy-MM-dd");
                        summarySheet.Cells[startRow, 3].Value = x.Time.ToString("HH:mm");

                        if (string.IsNullOrEmpty(x.ClosedFrom) || x.ClosedFrom == "null")
                        {
                            summarySheet.Cells[startRow, 4].Value = "";
                        }
                        else {
                            var stringToDate = JsonConvert.DeserializeObject<TimeModel>(x.ClosedFrom);
                            var time = $"{stringToDate.hour.ToString("00")} : {stringToDate.minute.ToString("00")}";

                            summarySheet.Cells[startRow, 4].Value = $"{time}";
                        }

                        if (string.IsNullOrEmpty(x.ClosedTo) || x.ClosedTo == "null")
                        {
                            summarySheet.Cells[startRow, 5].Value = "";
                        }
                        else {
                            var stringToDate = JsonConvert.DeserializeObject<TimeModel>(x.ClosedTo);
                            var time = $"{stringToDate.hour.ToString("00")} : {stringToDate.minute.ToString("00")}";
                            
                            summarySheet.Cells[startRow, 5].Value = $"{time}";
                        }


                        summarySheet.Cells[startRow, 6].Value = x.Type1Name;
                        summarySheet.Cells[startRow, 7].Value = x.Type2Name;
                        summarySheet.Cells[startRow, 8].Value = x.Type3Name;


                        summarySheet.Cells[startRow, 9].Value = x.Q;
                        summarySheet.Cells[startRow, 10].Value = x.P;

                        var listOfGuards = x.Guards.Select(r => r.Name).ToList();
                        summarySheet.Cells[startRow, 11].Value = String.Join(",", listOfGuards); ;

                        summarySheet.Cells[startRow, 12].Value = x.Description;


                        startRow++;
                    });

                   
                }


                return excel.GetAsByteArray();
            }
        }

        public string GetHtmlTemplateDayReport(DateTime date, int siteId)
        {
            var sb = new StringBuilder();

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var testing = assembly.GetManifestResourceStream($"SERApp.Service.HtmlTemplates.DayReport.html.cshtml");

            using (StreamReader sr = new StreamReader(testing))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();

                    if (!line.StartsWith("@model"))
                    {
                        sb.AppendLine(line);
                    }
                }
            }

            var d = _repository.GetAllByRawString($"SELECT * FROM Incidents WHERE SiteId = {siteId}").Where(x => x.Time.ToShortDateString().Equals(date.ToShortDateString())).ToList();
            var t3 = new List<Type3>();

            var ddd = d.Where(x=>x.Type3Id != null).Select(x => x.Type3Id).Distinct();
            if (d.Where(x => x.Type3Id.HasValue).Select(x => x.Type3Id.Value).Distinct().Any())
            {
                t3 = _type3Repository.GetAllByRawString($"SELECT * FROM Type3 WHERE Id in ({String.Join(",", d.Where(x => x.Type3Id != null && x.Type3Id.HasValue).Select(x => x.Type3Id).Distinct().ToArray())})").ToList();

            }

            var t2 = new List<Type2>();
            if (d.Where(x => x.Type2Id.HasValue).Select(x => x.Type2Id).Distinct().Any())
            {

              
                    t2 = _type2Repository.GetAllByRawString($"SELECT * FROM Type2 WHERE Id in ({String.Join(",", d.Where(x => x.Type2Id != null && x.Type2Id.HasValue).Select(x => x.Type2Id).Distinct().ToArray())})").ToList();
                


                
            }

            //var t2 = 

            var t1 = new List<Type1>();
            if (d.Where(x => x.Type1Id.HasValue).Select(x => x.Type2Id).Distinct().Any())
            {

                    t1 = _type1Repository.GetAllByRawString($"SELECT * FROM Type1 WHERE Id in ({String.Join(",", d.Where(x => x.Type1Id != null && x.Type1Id.HasValue).Select(x => x.Type1Id).Distinct().ToArray())})").ToList();
                



            }
                //var t1 = 

            var iguard = _incidentGuardRepository.GetAllByRawString($"SELECT * FROM IncidentGuard WHERE IncidentId IN ({String.Join(",", d.Select(x => x.Id).Distinct().ToArray())})");

            d.ForEach(x => 
            {
                var tt1 = t1.Where(r => x.Type1Id == r.Id);
                if (tt1.Any())
                {
                    x.Type1 = new Type1()
                    {
                        Id = tt1.FirstOrDefault().Id,
                        Name = tt1.FirstOrDefault().Name
                    };
                }

                var tt2 = t2.Where(r => x.Type2Id == r.Id);
                if (tt2.Any())
                {
                    x.Type2 = new Type2()
                    {
                        Id = tt2.FirstOrDefault().Id,
                        Name = tt2.FirstOrDefault().Name
                    };
                }

                var tt3 = t3.Where(r => x.Type3Id == r.Id);
                if (tt3.Any())
                {
                    x.Type3 = new Type3()
                    {
                        Id = tt3.FirstOrDefault().Id,
                        Name = tt3.FirstOrDefault().Name
                    };
                }

                var iiguard = iguard.Where(r => x.Id == r.IncidentId);
                if (iiguard.Any())
                {
                    var gua = _guardRepository.GetAllByRawString($"SELECT * FROM Guards WHERE Id IN ({String.Join(",", iiguard.Select(r => r.GuardId).Distinct().ToArray())})");


                    x.IncidentGuard = iiguard.Select(v => new IncidentGuard()
                    {
                        GuardId = v.GuardId,
                        IncidentId = v.IncidentId,
                        Guard = new Guard()
                        {
                            Name = gua.FirstOrDefault(t=>t.Id == v.GuardId).Name,
                            Id = v.GuardId
                        }
                    }).ToList();
                }


            });


            var data = d//_repository.GetAllIncidentsChildIncluded(siteId).Where(x => x.Time.ToShortDateString().Equals(date.ToShortDateString()))
            .Select(x => new IncidentModel()
            {
                Id = x.Id,
                Time = x.Time,
                AccountId = x.AccountId,
                Type1 = x.Type1 != null ? new Type1Model()
                {
                    Id =  x.Type1.Id,
                    Name = x.Type1.Name,
                }: new Type1Model(),
                Type2 = x.Type2 != null? new Type2Model()
                {
                    Id = x.Type2.Id,
                    Name = x.Type2.Name,
                }: new Type2Model(),
                Type3 = x.Type3 != null ? new Type3Model()
                {
                    Id = x.Type3.Id,
                    Name = x.Type3.Name,
                }: new Type3Model(),
                Quantity = x.Quantity,
                PoliceResponse = x.PoliceResponse,
                IncidentGuard = x.IncidentGuard.Any() ? x.IncidentGuard.Select(r => new IncidentGuardModel()
                {
                    Guard = r.Guard != null ? new GuardModel()
                    {
                        Id = r.Guard.Id,
                        Name = r.Guard.Name
                    } : new GuardModel(),
                }).ToList() : new List<IncidentGuardModel>(),
                ActionDescription = x.ActionDescription,
            }).ToList();


            //var guards = data.Select(r => r.Id);

            //var g =  _incidentGuardRepository.GetByPredicate(x => guards.Contains(x.IncidentId)).Select(r=>r.GuardId);
            //var gList = _guardRepository.GetByPredicate(x => g.Contains(x.Id)).ToList();
            
            string html = "";
            if (data.Any())
            {
                var incidentGuards = data.SelectMany(x => x.IncidentGuard).DistinctBy(r => r.Guard.Name);
                var dayreport = new SERApp.Models.HtmlTemplatesModel.DayReport();
                dayreport.Incidents = data;
                dayreport.Date = date;
                dayreport.SiteName = _siteRepository.Get(siteId).Name;
                dayreport.Title = "Daily Report";
                dayreport.AccountName = _accountRepository.Get(data.First().AccountId).Company;
                dayreport.Summary = _summaryService.GetSummary(date,data.First().AccountId,siteId).SummaryValue;
                dayreport.Guards = String.Join(",", incidentGuards.Select(r => r.Guard.Name));
                dayreport.TotalP = data.Sum(x => x.PoliceResponse);
                dayreport.TotalQ = data.Sum(x => x.Quantity);
                html = Engine.Razor.RunCompile(sb.ToString(), "SERApp.Service.DayReport.html.cshtml", typeof(SERApp.Models.HtmlTemplatesModel.DayReport), dayreport, null);
                
            }

            return html;
        }

        public string GetHtmlTemplateWeekReport(DateTime date1, DateTime date2, int siteId)
        {
            var sb = new StringBuilder();

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var testing = assembly.GetManifestResourceStream($"SERApp.Service.HtmlTemplates.WeekReport.html.cshtml");

            using (StreamReader sr = new StreamReader(testing))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();

                    if (!line.StartsWith("@model"))
                    {
                        sb.AppendLine(line);
                    }
                }
            }
            var siteService = new SiteService();
            var accountService = new AccountService();
            var weekreport = new WeekReport();
            var site = siteService.Get(siteId);
            var accountId = site.AccountId;
            weekreport.AccountName = accountService.Get(accountId).Company;
            weekreport.Title = "Week Report";
            weekreport.SiteName = site.Name;
            weekreport.Date1 = date1.ToShortDateString();
            weekreport.Date2 = date2.ToShortDateString();

            var data = _repository.GetAllForList(siteId,accountId,null, null).Where(x => x.Time >= date1 && x.Time <= date2)
            .Select(x => new IncidentModel()
            {
                Id = x.Id,
                Time = x.Time,
                AccountId = x.AccountId,
                Type1 = x.Type1Id.HasValue ? new Type1Model()
                {
                    Id = x.Type1Id.Value,
                    Name = x.Type1Name,
                } : new Type1Model(),
                Type2 = x.Type2Id.HasValue  ? new Type2Model()
                {
                    Id = x.Type2Id.Value,
                    Name = x.Type2Name,
                } : new Type2Model(),
                Type3 = x.Type3Id.HasValue ? new Type3Model()
                {
                    Id = x.Type3Id.Value,
                    Name = x.Type3Name,
                } : new Type3Model(),
                Quantity = x.Q,
                PoliceResponse = x.P,
               
            }).ToList();

            weekreport.WeekReportMain = new List<WeekReportSub1>();
            var main = data.GroupBy(x => x.Time.ToShortDateString());
            var sub = data.Where(r=>r.Type1 != null).GroupBy(x => x.Type1.Id);
            var summaryRepository = new Repository<Summary>();
            var summaries = summaryRepository.GetByPredicate(x => x.Date >= date1 && x.Date <= date2 && x.SiteId == siteId).ToList();

            weekreport.Type1WeekReport = new List<WeekReportSubType>();
            weekreport.Type2WeekReport = new List<WeekReportSubType>();
            weekreport.Type3WeekReport = new List<WeekReportSubType>();


            foreach (var d in main)
            {
                var date = Convert.ToDateTime(d.Key);
                var summary = summaries.Where(x => x.Date.ToShortDateString() == d.Key);
                weekreport.WeekReportMain.Add(new WeekReportSub1()
                {
                    WeekDay = date.ToString("dddd"),
                    Summary = summary.Any()? summary.SingleOrDefault().SummaryValue: "",
                    P = d.Select(x => x.PoliceResponse).Sum(),
                    Q = d.Select(x => x.Quantity).Sum(),
                });
            }

            var allType1 = data.Select(r => new Type1()
            {
                Id = r.Type1.Id,
                Name = r.Type1.Name
            }).DistinctBy(x=>x.Id);
            foreach (var t1 in sub)
            {
                var type1 = allType1.SingleOrDefault(r=>r.Id == t1.Key);

                var monday = t1.Where(x => x.Time.ToShortDateString() == date1.ToShortDateString() && x.Type1Id == t1.Key).Count();
                var tuesday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(1).ToShortDateString() && x.Type1Id == t1.Key).Count();
                var wednesday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(2).ToShortDateString() && x.Type1Id == t1.Key).Count();
                var thursday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(3).ToShortDateString() && x.Type1Id == t1.Key).Count();
                var friday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(4).ToShortDateString() && x.Type1Id == t1.Key).Count();
                var saturday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(5).ToShortDateString() && x.Type1Id == t1.Key).Count();
                var sunday = t1.Where(x => x.Time.ToShortDateString() == date1.AddDays(6).ToShortDateString() && x.Type1Id == t1.Key).Count();

                weekreport.Type1WeekReport.Add(new WeekReportSubType()
                {
                    Name = type1.Name,
                    Monday = monday,
                    Tuesday = tuesday,
                    Wednesday = wednesday,
                    Thursday = thursday,
                    Friday = friday,
                    Saturday = saturday,
                    Sunday = sunday,
                    Total = monday + tuesday + wednesday + thursday + friday + saturday + sunday
                });
            }



            return sb.ToString();
        }

        public List<dynamic> GetMinMaxDates(int accountId, int siteId)
        {
            return _repository.GetMinMaxStatsDate(accountId, siteId);
        }

        public byte[] ExportStatisticsByDate(List<IncidentStatisticsModel> model, bool isStacked)
        {
            using (var package = new ExcelPackage())
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.Add("Sheet1");

                var column = 2;
                foreach (var item in model)
                {
                    var rowDate = 2;
                    foreach (var label in item.labels)
                    {
                        worksheet.Cells[rowDate, column - 1].Value = label;
                        rowDate++;
                    }

                    var chartType = isStacked ? eChartType.ColumnStacked : eChartType.ColumnClustered;
                    var chart = worksheet.Drawings.AddChart($"chart-{column}", chartType);

                    var bottomRow = item.labels.Count + 1;

                    foreach (var dataset in item.datasets)
                    {
                        worksheet.Cells[1, column].Value = dataset.label;
                        var rowTitle = 2;
                        foreach (var data in dataset.data)
                        {
                            worksheet.Cells[rowTitle, column].Value = data;
                            rowTitle++;
                        }

                        var columnName = GetColumnName(column);

                        var series = (ExcelBarChartSerie)chart.Series.Add($"{columnName}2:{columnName}{bottomRow}", $"A2:A{bottomRow}");
                        series.HeaderAddress = new ExcelAddress($"'Sheet1'!{columnName}1");
                        if (!isStacked)
                        {
                            series.DataLabel.Fill.Color = System.Drawing.Color.White;
                        }
                        series.DataLabel.ShowValue = true;


                        column++;
                    }

                    chart.StyleManager.SetChartStyle(ePresetChartStyle.ColumnChartStyle1);
                    chart.XAxis.TextBody.Rotation = -45;
                    chart.Title.Text = item.title;
                    var pxlT = column - 2 == item.datasets.Count ? 100 : 450;
                    chart.SetPosition(pxlT, 400);
                    chart.SetSize(1000, 300);

                    column += 4;
                    //excelColumnTitle = column;
                }


                return package.GetAsByteArray();
            }
        }

        public byte[] ExportStatisticsByCategory(List<IncidentStatisticsModel> model, bool isStacked)
        {
            using (var package = new ExcelPackage())
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.Add("Sheet1");

                var rowTitle = 1;

                if (!isStacked)
                {
                    var column = 1;

                    foreach (var data in model)
                    {
                        //worksheet.Cells[1, column].Value = data.title;
                        //rowTitle = 2;
                        foreach (var item in data.datasets)
                        {
                            worksheet.Cells[rowTitle, column].Value = item.label;
                            worksheet.Cells[rowTitle, column + 1].Value = item.data;
                            rowTitle++;
                        }

                        rowTitle = 1;
                        var bottomRow = data.labels.Count;
                        var chart = worksheet.Drawings.AddChart($"chart-{column}", eChartType.ColumnClustered);

                        var firstCol = GetColumnName(column);
                        var secCol = GetColumnName(column + 1);
                        var series = (ExcelBarChartSerie)chart.Series.Add($"{secCol}1:{secCol}{bottomRow}", $"{firstCol}1:{firstCol}{bottomRow}");
                        series.HeaderAddress = new ExcelAddress($"{firstCol}{rowTitle}:{firstCol}{bottomRow}");
                        series.DataLabel.ShowValue = true;
                        series.DataLabel.Fill.Color = System.Drawing.Color.White;

                        chart.StyleManager.SetChartStyle(ePresetChartStyle.ColumnChartStyle1);
                        chart.Legend.Remove();
                        chart.XAxis.TextBody.Rotation = -45;
                        chart.Title.Text = data.title;

                        var pxlT = column == 1 ? 1 : 350;
                        chart.SetPosition(pxlT, 400);
                        chart.SetSize(1000, 300);
                        column += 3;
                    }

                }
                else
                {
                    var column = 2;

                    var info = model.First();
                    worksheet.Column(1).Width = 25;
                    foreach (var item in info.datasets)
                    {
                        worksheet.Cells[1, column].Value = item.label;
                        column++;
                    }

                    rowTitle = 2;
                    foreach (var data in model)
                    {
                        column = 2;

                        worksheet.Cells[rowTitle, 1].Value = data.title;
                        foreach (var item in data.datasets)
                        {
                            worksheet.Cells[rowTitle, column].Value = item.data;
                            column++;
                        }

                        rowTitle++;
                    }

                    var chart = (ExcelBarChart)worksheet.Drawings.AddChart($"chart-{rowTitle}", eChartType.ColumnStacked);

                    column = 2;
                    rowTitle = 2;
                    var bottomRow = model.Count + 1;
                    foreach (var data in model.First().labels)
                    {
                        var columnName = GetColumnName(column);

                        var series = chart.Series.Add($"{columnName}2:{columnName}{bottomRow}", $"A2:A{bottomRow}");
                        series.HeaderAddress = new ExcelAddress($"'Sheet1'!{columnName}1");
                        series.DataLabel.ShowValue = true;

                        var aa = series.DataLabel.Fill.SolidFill; 
                        column++;
                    }

                    chart.StyleManager.SetChartStyle(ePresetChartStyle.ColumnChartStyle1);

                    var pxlT = worksheet.Drawings.Count == 1 ? 110 : 460;
                    chart.SetPosition(70, 100);
                    chart.SetSize(500, 300);

                }

                return package.GetAsByteArray();
            }
            
        }

        private string GetColumnName(int index)
        {
            String columnName = "";

            while (index > 0)
            {

                // Find remainder
                int rem = index % 26;

                // If remainder is 0, then a
                // 'Z' must be there in output
                if (rem == 0)
                {
                    columnName += "Z";
                    index = (index / 26) - 1;
                }

                // If remainder is non-zero
                else
                {
                    columnName += (char)((rem - 1) + 'A');
                    index = index / 26;
                }
            }

            // Reverse the string
            columnName = reverse(columnName);
            return columnName;
        }

        static string reverse(String input)
        {
            char[] reversedString = input.ToCharArray();
            Array.Reverse(reversedString);
            return new String(reversedString);
        }
    }
}
