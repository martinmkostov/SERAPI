﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IType4Service
    {
        bool AddType4(Type4Model model);
        void UpdateType4(Type4Model model);
        bool DeleteType4ById(int id);
        IEnumerable<Type4Model> GetAllType4(int accountId, int siteId);
        Type4Model GetType4ById(int id);
    }

    public class Type4Service : IType4Service
    {
        private IRepository<Type4> _repository;

        public Type4Service()
        {
            _repository = new Repository<Type4>();
        }

        public bool AddType4(Type4Model model)
        {
            if (model.Id == 0)
            {
                if (_repository.GetAll().Any(x => x.Name == model.Name && x.AccountId == model.AccountId && model.SiteId == x.SiteId))
                {
                    return false;
                }

                _repository.Save(new Type4()
                {
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now,
                    Id = model.Id,
                    Name = model.Name,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId
                });

            }
            else
            {
                UpdateType4(model);
            }
            return true;
        }

        public void UpdateType4(Type4Model model)
        {
            var existingCustomType = _repository.Get(model.Id);
            existingCustomType.LastUpdatedDate = DateTime.Now;
            existingCustomType.Name = model.Name;
            existingCustomType.AccountId = model.AccountId;
            existingCustomType.SiteId = model.SiteId;
            _repository.Update(existingCustomType);
        }

        public bool DeleteType4ById(int id)
        {
            _repository.Delete(id);

            return true;
        }

        public IEnumerable<Type4Model> GetAllType4(int accountId, int siteId)
        {
            var data = _repository.GetAll().Where(x => x.AccountId == accountId).Select(x => new Type4Model()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name,
                AccountId = x.AccountId,
                SiteId = x.SiteId
            }).ToList();

            if (siteId != 0)
            {
                data = data.Where(x => x.SiteId == siteId).ToList();
            }

            return data;
        }

        public Type4Model GetType4ById(int id)
        {
            var model = _repository.Get(id);

            return new Type4Model()
            {
                CreatedDate = model.CreatedDate,
                LastUpdatedDate = model.LastUpdatedDate,
                Name = model.Name,
                Id = model.Id,
                SiteId = model.SiteId
            };
        }
    }
}
