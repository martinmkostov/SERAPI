﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IType1Service
    {
        IEnumerable<Type1Model> GetAllType1(int accountId, int siteId);
        Type1Model GetType1(int id);

        bool DeleteType1 (int id);
        bool AddType1(Type1Model model);
        Type1Model GetType1ById(int id);
        IEnumerable<ReportTypeModel> GetReportTypes();
    }
    public class Type1Service : IType1Service
    {
        private IRepository<Type1> _repository;
        private IRepository<ReportType> _reportTypeRepository;
        private IType1FieldRepository _type1FieldRepository;
        private IIncidentRepository _incidentRepository;
        private IRepository<Type1Field> _type1FieldRep;
        public Type1Service()
        {
            _repository = new Repository<Type1>();
            _reportTypeRepository = new Repository<ReportType>();
            _type1FieldRepository = new Type1FieldRepository();
            _incidentRepository = new IncidentRepository();
            _type1FieldRep = new Repository<Type1Field>();
        }

        public bool AddType1(Type1Model model)
        {

            if (model.Id > 0)
            {
                var existingIncidentType = _repository.Get(model.Id);
                existingIncidentType.IsReportRequired = model.IsReportRequired;
                existingIncidentType.Name = model.Name;
                existingIncidentType.RequiredReport = String.Join(",", model.RequiredReport.Select(r => r.Id));
                existingIncidentType.ShownReport = String.Join(",", model.ShownReport.Select(r => r.Id));
                existingIncidentType.AccountId = model.AccountId;
                existingIncidentType.SiteId = model.SiteId;
                if (string.IsNullOrEmpty(existingIncidentType.RequiredReport))
                {
                    existingIncidentType.RequiredReport = "0";
                }

                if (string.IsNullOrEmpty(existingIncidentType.ShownReport))
                {
                    existingIncidentType.ShownReport = "0";
                }


                _repository.Update(existingIncidentType);

                var currentFields = this._type1FieldRepository.GetByPredicate(e => e.Type1Id == model.Id).ToList();
                    currentFields.ForEach(field => 
                    {
                        this._type1FieldRepository.Delete(field.Id);
                    });

                model.Fields.ForEach(field =>
                {
                    _type1FieldRepository.Save(new Type1Field
                    {
                        Type1Id = existingIncidentType.Id,
                        FieldId = field.Id,
                        Label = field.Label,
                        DataSource = field.DataSource,
                        Value = field.Value,
                        IsRequired = field.IsRequired,
                        SortOrder = field.SortOrder,
                        ExcludedValues = field.ExcludedValues
                    });
                });
            }
            else
            {
                string shownReport = String.Join(",", model.ShownReport.Select(r => r.Id));
                string requiredReport = String.Join(",", model.RequiredReport.Select(r => r.Id));

                if (!model.ShownReport.Any())
                {
                    shownReport = "0";
                }

                if (!model.RequiredReport.Any())
                {
                    requiredReport = "0";
                }

                if (_repository.GetAll().Any(x => x.Name == model.Name && x.AccountId == model.AccountId))
                {
                    return false;
                }

                var entity = _repository.Save(new Type1()
                {
                    IsReportRequired = model.IsReportRequired,
                    ShownReport = shownReport,
                    RequiredReport = requiredReport,
                    Name = model.Name,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId
                });

                model.Fields.ForEach(field => 
                {
                    _type1FieldRepository.Save(new Type1Field
                    {
                        Type1Id = entity.Id,
                        FieldId = field.Id,
                        Label = field.Label,
                        DataSource = field.DataSource,
                        Value = field.Value,
                        IsRequired = field.IsRequired,
                        SortOrder = field.SortOrder,
                        ExcludedValues = field.ExcludedValues
                    });
                });
            }

            return true;
        }

        public bool DeleteType1(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type1Id == id))
            {
                return false;
            }

            var currentFields = this._type1FieldRepository.GetByPredicate(e => e.Type1Id == id).ToList();
                currentFields.ForEach(field =>
                {
                    this._type1FieldRepository.Delete(field.Id);
                });

            _repository.Delete(id);
            return true;
        }

        public Type1Model GetType1(int id)
        {
            var existingData = _repository.GetAllByRawString($"SELECT * FROM Type1 WHERE Id = {id}").SingleOrDefault();// _repository.Get(id);
            var data = new Type1Model()
            {
                Id = existingData.Id,
                IsReportRequired = existingData.IsReportRequired,
                Name = existingData.Name, 
                AccountId = existingData.AccountId,
                SiteId = existingData.SiteId
            };


            var requiredReportsIds = existingData.RequiredReport.Split(',').Select(x=>Convert.ToInt32(x)).ToList();
            var shownReportsIds = existingData.ShownReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();

            data.RequiredReport = _reportTypeRepository.GetByPredicate(x => requiredReportsIds.Contains(x.Id))
            .Select(x => new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();

            data.ShownReport = _reportTypeRepository.GetByPredicate(x => shownReportsIds.Contains(x.Id))
            .Select(x => new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();
            data.RequiredReport.OrderBy(x => x.Name);

            var fields = _type1FieldRepository.FindByTypeId(id);

            data.Fields = fields.OrderBy(e => e.DailyReportField.SortOrder).ThenBy(e => e.DailyReportField.ControlGroup).Select(x => new DailyReportFieldModel
            {
                Id = x.FieldId,
                Label = x.Label,
                Value = x.Value,
                DataSource = x.DataSource,
                IsRequired = x.IsRequired,
                CustomEditorName = x.DailyReportField.CustomEditorName,
                FieldTypeFlag = x.DailyReportField.FieldType.Flag,
                SortOrder = x.SortOrder,
                ExcludedValues = x.ExcludedValues,
                FieldName = x.DailyReportField.ControlName,
                FieldGroup = x.DailyReportField.ControlGroup,
                CustomScript = x.CustomScript ?? x.DailyReportField.DefaultCustomScript
            })
            .OrderBy(e => e.SortOrder)
            .ToList();

            return data;
        }

        public Type1Model GetType1ById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Type1Model> GetAllType1(int accountId, int siteId)
        {
            var reportTypes =_reportTypeRepository.GetAllByRawString($"SELECT * FROM ReportTypes").ToList();

            var data = _repository.GetAllByRawString($"SELECT * FROM Type1 WHERE AccountId={accountId}").ToList();

            //if (siteId != 0)
            //{
            //    data = data.Where(x=>x.SiteId == siteId).ToList();
            //}

            var returnModel = new List<Type1Model>();
            data.ForEach(t => 
            {
                var requiredReportsIds = t.RequiredReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                var shownReportsIds = t.ShownReport.Split(',').Select(x => Convert.ToInt32(x)).ToList();

                returnModel.Add(new Type1Model()
                {
                    Id = t.Id,
                    Name = t.Name,
                    IsReportRequired = t.IsReportRequired,
                    RequiredReport = reportTypes.Where(x => requiredReportsIds.Contains(x.Id))
                    .Select(x => new ReportTypeModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }).ToList(),
                    ShownReport = reportTypes.Where(x => shownReportsIds.Contains(x.Id))
                    .Select(x => new ReportTypeModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }).ToList(),
                });
            });


            return returnModel;
            //.Select(x => new IncidentTypeModel()
            //{
            //    Id = x.Id,
            //    IsReportRequired = x.IsReportRequired,
            //    Name = x.Name,               
            //}).ToList();
        }

        public IEnumerable<ReportTypeModel> GetReportTypes()
        {
            return _reportTypeRepository.GetAll().Select(x=> new ReportTypeModel()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }
    }
}
