﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SERApp.Data;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SERApp.Service.Services
{
    public interface ITaskService
    {
        SiteTaskModel GetSiteTask(int siteId, int taskId);
        IEnumerable<MainCategoryModel> GetAllMainCategories(int accountId);
        MainCategoryModel GetAllMainCategory(int id);
        SubCategoryModel GetSubCategory(int id);
        IEnumerable<SubCategoryModel> GetAllSubCategories(int accountId);
        IEnumerable<SiteTaskModel> GetPublishedTask(int Id);
        IEnumerable<TaskModel> GetAllTasks(int accountId, string searchText);
        IEnumerable<ActivityRoleModel> GetActivityRoles(int accountId);
        ActivityRoleModel GetActivityRole(int id);
        void AddActivityRole(ActivityRoleModel model);
        List<IGrouping<string, SiteTaskModel>> GetByUserId(int id);
        IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId = 0, int userId = 0, string searchValue = "", int accountId = 0, bool forEmail = false);
        byte[] GetXls(int siteId, int userId = 0, string searchValue = "");
        byte[] GetXlsAllTask(int accountId, string searchText);
        string GetOngoingTasksByUserIdEmailBody(int accountId, UserModel user);
        bool AddTask(TaskModel model);
        void DeleteTask(int id);
        DeleteResultModel DeleteMainCategory(int id);
        DeleteResultModel DeleteSubCategory(int id);
        DeleteResultModel DeleteActivityRole(int id);
        DeleteResultModel DeleteInterval(int id);
        DeleteResultModel DeleteSiteTaskLog(int id);
        ArchiveDataModel ArchiveTask(int id);
        ArchiveDataModel UnarchiveTask(int id);
        void AddMainCategory(MainCategoryModel model);
        void AddSubCategory(SubCategoryModel model);
        void Transfer(SiteTaskModel model);
        void PublishTask(SiteTaskModel model);
        void UnPublishTask(SiteTaskModel model);
        SiteTaskLogModel Confirm(SiteTaskLogModel model);
        SiteTaskLogModel ConfirmBimControl(SiteTaskLogModel model, List<BimControlItemModel> bimControlItem);
        void ReAssign(SiteTaskModel model);
        ConfirmMultipleResultModel ConfirmMultiple(SiteTaskLogModel model);
        void PerformAction(SiteTaskLogModel model);
        TaskModel GetTask(int id);
        int GetTaskByTaskNumber(string taskNumber, int accountId);
        TaskModel GetTaskBySiteIdAndTaskId(int taskId,int siteId);
        IEnumerable<SiteTaskLogModel> GetHistory(int siteTaskId);
        SiteTaskLogModel GetSingleHistory(int id);
        SiteTaskLogModel GetHistoryByAccountSiteTask(int accountId, int siteId, int taskId);
        DateTime GetCurrentIntervalDate(int siteTaskId);
    }

    public class TaskService : ITaskService
    {
        private ITaskRepository _taskRepository;
        private IRepository<MainCategory> _mainCategoryRepository;
        private IRepository<SubCategory> _subCategoryRepository;
        private IRepository<TaskMainCategory> _taskMainCategory;
        private IRepository<TaskSubCategory> _taskSubCategory;
        private IRepository<ActivityRole> _activityRoleRepository;
        //private IRepository<SiteTask> _siteTaskRepository;
        private ISiteTaskRepository _siteTaskRepository;
        private IRepository<SiteTaskLog> _siteTaskLogRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<Interval> _intervalRepository;
        private IRepository<User> _userRepository;
        private IRepository<Site> _siteRepository;
        private IRepository<UserModuleRoles> _userModuleRoleRepository;
        private IRepository<TaskHistory> _taskHistory;
        private IRepository<BimControlItem> _bimControlRepository;
        private IRepository<BimControlItemImage> _bimControlImageRepository;
        private UserModuleRoleService _userModuleRoleService;
        private IRepository<Account> _accountRepository;

        public TaskService()
        {
            _bimControlRepository = new Repository<BimControlItem>();
            _bimControlImageRepository = new Repository<BimControlItemImage>();
            _taskRepository = new TaskRepository();
            _mainCategoryRepository = new Repository<MainCategory>();
            _subCategoryRepository = new Repository<SubCategory>();
            _taskMainCategory = new Repository<TaskMainCategory>();
            _taskSubCategory = new Repository<TaskSubCategory>();
            _siteTaskRepository = new SiteTaskRepository();
            _siteTaskLogRepository = new Repository<SiteTaskLog>();
            _roleRepository = new Repository<Role>();
            _intervalRepository = new Repository<Interval>();
            _userRepository = new Repository<User>();
            _siteRepository = new Repository<Site>();
            _userModuleRoleRepository = new Repository<UserModuleRoles>();
            _activityRoleRepository = new Repository<ActivityRole>();
            _userModuleRoleService = new UserModuleRoleService();
            _taskHistory = new Repository<TaskHistory>();
            _accountRepository = new Repository<Account>();
        }

        public void AddMainCategory(MainCategoryModel model)
        {

            if (model.Id == 0)
            {
                _mainCategoryRepository.Save(new MainCategory()
                {
                    Name = model.Name,
                    AccountId = model.AccountId,
                });
            }
            else
            {
                var data = _mainCategoryRepository.Get(model.Id);
                data.Name = model.Name;
                data.AccountId = model.AccountId;
                _mainCategoryRepository.Update(data);
            }
           
        }

        public void AddSubCategory(SubCategoryModel model)
        {
            if (model.Id == 0)
            {
                _subCategoryRepository.Save(new SubCategory()
                {
                    Name = model.Name,
                    AccountId = model.AccountId
                });
            }
            else
            {
                var data = _subCategoryRepository.Get(model.Id);
                data.Name = model.Name;
                data.AccountId = model.AccountId;

                _subCategoryRepository.Update(data);
            }

        }

        public bool AddTask(TaskModel model)
        {
            if (model.Id == 0)
            {
                if (_taskRepository.GetTasksFromTaskNoAndAccount(model.TaskNo,model.AccountId).Any())
                {
                    return false;
                }

                var dd = new Data.Models.Task()
                {
                    TaskSubCategories = model.TaskSubCategories.Select(r => new TaskSubCategory()
                    {
                        SubCategoryId = r.SubCategoryId,
                    }).ToList(),
                    TaskMainCategory = model.TaskMainCategory.Select(r => new TaskMainCategory()
                    {
                        MainCategoryId = r.MainCategoryId
                    }).ToList(),
                    Description = model.Description,
                    Interval = model.Interval,
                    NuberOfDays = model.NuberOfDays,
                    Purpose = model.Purpose,
                    TaskNo = model.TaskNo,
                    RemindAfter = model.RemindAfter,
                    Title = model.Title,
                    RoleId = model.RoleId,
                    AccountId = model.AccountId,
                    IsFileRequired = model.IsFileRequired,
                    ExtensionsAllowed = model.ExtensionsAllowed,
                    DayToComplete = model.DayToComplete,
                    IsArchived = false,
                    RecommendedRole = model.RecommendedRole,
                    LastUpdatedBy = model.LastUpdatedBy,
                    LastUpdatedTime = DateTime.Now
                };

                _taskRepository.Save(dd);
                model.Id = dd.Id;

                _taskHistory.Save(new TaskHistory()
                {
                    Actions =$"Added new task {model.TaskNo}",
                    LogDateTime = dd.LastUpdatedTime.Value,
                    TaskId = model.Id,
                    UserId = dd.LastUpdatedBy
                });

            }
            else
            {
                var actions = new List<string>();
                var task = _taskRepository.GetWithChildsIncluded(model.Id);
                task.Description = CheckIfSameValue("Description", model.Description,task.Description, ref actions);
                task.Interval = CheckIfSameValue("Interval", model.Interval, task.Interval, ref actions);
                task.NuberOfDays = CheckIfSameValue("Number Of Days", model.NuberOfDays, task.NuberOfDays, ref actions);
                task.Purpose = CheckIfSameValue("Purpose", model.Purpose, task.Purpose, ref actions);
                task.IsArchived = CheckIfSameValue("Is Archived", model.IsArchived, task.IsArchived, ref actions);
                task.IsFileRequired = CheckIfSameValue("Is File Required", model.IsFileRequired, task.IsFileRequired, ref actions);
                task.TaskNo = CheckIfSameValue("Task No", model.TaskNo, task.TaskNo, ref actions);
                task.Title = CheckIfSameValue("Title", model.Title, task.Title, ref actions);
                task.AccountId = CheckIfSameValue("Account Id", model.AccountId, task.AccountId, ref actions);
                task.RemindAfter = CheckIfSameValue("Remind After", model.RemindAfter, task.RemindAfter, ref actions);
                task.RoleId = CheckIfSameValue("Role Id", model.RoleId, task.RoleId, ref actions);
                task.ExtensionsAllowed = CheckIfSameValue("Extensions Allowed", model.ExtensionsAllowed, task.ExtensionsAllowed, ref actions);
                task.DayToComplete = CheckIfSameValue("Day To Complete", model.DayToComplete, task.DayToComplete, ref actions);
                task.RecommendedRole = CheckIfSameValue("Recommended Role", model.RecommendedRole, task.RecommendedRole, ref actions);

                task.LastUpdatedBy = model.LastUpdatedBy;
                task.LastUpdatedTime = DateTime.Now;

                task.TaskMainCategory.ToList().ForEach(x => 
                {
                    _taskMainCategory.Delete(x.Id);
                });

                task.TaskSubCategories.ToList().ForEach(x =>
                {
                    _taskSubCategory.Delete(x.Id);
                });

                model.TaskMainCategory.ForEach(x => 
                {
                    _taskMainCategory.Save(new TaskMainCategory()
                    {
                        MainCategoryId = x.MainCategoryId,
                        TaskId = task.Id
                    });
                });

                model.TaskSubCategories.ForEach(x =>
                {
                    _taskSubCategory.Save(new TaskSubCategory()
                    {
                        SubCategoryId = x.SubCategoryId,
                        TaskId = task.Id
                    });
                });


                _taskRepository.Update(task);
                string csv = String.Join(",", actions);
                _taskHistory.Save(new TaskHistory()
                {
                    Actions = $"Updated task {model.TaskNo} : {csv}",
                    LogDateTime = task.LastUpdatedTime.Value,
                    TaskId = model.Id,
                    UserId = task.LastUpdatedBy
                });
            }

            return true;
        }
        private T CheckIfSameValue<T>(string propertyName,T data1, T data2, ref List<string> actions)
        {
            if(data1 == null && data2 == null) 
            {
                return data1;
            }

            if (data1 == null && data2 != null) 
            {
                return data2;
            }


            if (!data1.Equals(data2)) {
                actions.Add($"{propertyName} updated to {data1.ToString()}");
            }

            return data1;
        }

        public SiteTaskLogModel Confirm(SiteTaskLogModel model)
        {
            var sitetasklog = _siteTaskRepository.GetSiteTaskLogs(model.SiteTaskId).ToList();

            

            var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
            int instance = 0;
            if (sitetasklog.Any())
            {
                 instance = sitetasklog.Max(x => x.Instance) + 1;
            }

            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = model.LogDate,
                LogType = (int)TaskLogType.Confirm,
                SiteTaskId = model.SiteTaskId,
                Instance = instance,
                Owner = model.Owner,
                IntervalDate = model.IntervalDate,
                IsFromSer4 = true,
            };
            model.LogDate = tasklog.LogDate;
            var result = _siteTaskLogRepository.Save(tasklog);

            model.Id = result.Id;
            return model;
        }
        public SiteTaskLogModel ConfirmBimControl(SiteTaskLogModel model, List<BimControlItemModel> bimControlItem)
        {
            var sitetasklog = _siteTaskRepository.GetSiteTaskLogs(model.SiteTaskId).ToList();

            var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
            int instance = 0;
            if (sitetasklog.Any())
            {
                instance = sitetasklog.Max(x => x.Instance) + 1;
            }

            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = model.LogDate,
                LogType = (int)TaskLogType.Confirm,
                SiteTaskId = model.SiteTaskId,
                Instance = instance,
                Owner = model.Owner,
                IntervalDate = model.IntervalDate,
                IsFromSer4 = true,
            };
            model.LogDate = tasklog.LogDate;
            var result = _siteTaskLogRepository.Save(tasklog);

            if (bimControlItem != null && bimControlItem.Any())
            {
                bimControlItem.ForEach(x => 
                {

                    var it = new BimControlItem()
                    {
                        ErrorDescription = x.ErrorDescription,
                        LittraId = x.LittraId,
                        SiteTaskLogId = result.Id,
                        Date = x.Date,
                        Description = x.Description,
                        IssueId = x.IssueId,
                        Status = (int)x.Status
                    };

                    if (x.Photos!= null && x.Photos.Any()) 
                    {
                        it.Photos = new List<BimControlItemImage>();

                        int xr = 0;
                        x.Photos.ForEach(r => {
                            if (!string.IsNullOrEmpty(r.ImageBase64))
                            {
                                var datamimeType = r.ImageBase64.Split(';')[0].Replace("data:", "");

                                if (r.ImageBase64.Contains("data:image"))
                                {
                                    it.Photos.Add(new BimControlItemImage()
                                    {
                                        Url = SERApp.Service.Tools.File.SaveFileToBlob(new SERApp.Models.Common.FileModel()
                                        {
                                            ByteString = r.ImageBase64.Split(',')[1],
                                            FileName = $"file-{xr}",
                                            Extension = $".{datamimeType.Split('/')[1]}"
                                        })
                                    });
                                }
                            }
                            xr++;
                        });
                    }

                    _bimControlRepository.Save(it);
                });
            }

            model.Id = result.Id;
            return model;
        }

        public void DeleteTask(int id)
        {
            _taskRepository.Delete(id);
        }

        public DeleteResultModel DeleteMainCategory(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskMainCategory.GetByPredicate(r => r.MainCategoryId == id);

            if (data.Any()) {
                model.IsDeleted = false;
                return model;
            }


            _mainCategoryRepository.Delete(id);
            return model;
        }

        public DeleteResultModel DeleteSubCategory(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskSubCategory.GetByPredicate(r => r.SubCategoryId == id);

            if (data.Any())
            {
                model.IsDeleted = false;
                return model;
            }


            _subCategoryRepository.Delete(id);
            return model;
        }

        public DeleteResultModel DeleteInterval(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskRepository.GetByPredicate(r => r.Interval == id);

            if (data.Any())
            {
                model.IsDeleted = false;
                return model;
            }


            _intervalRepository.Delete(id);
            return model;
        }

        public IEnumerable<MainCategoryModel> GetAllMainCategories(int accountId)
        {
            return _mainCategoryRepository.GetAll().Where(x=>x.AccountId == accountId).Select(x => new MainCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,                
            });
        }

        public IEnumerable<SubCategoryModel> GetAllSubCategories(int accountId)
        {
            return _subCategoryRepository.GetAll().Where(x=>x.AccountId == accountId).Select(x => new SubCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,
            });
        }

        public IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId = 0, int userId = 0, string searchValue="", int accountId = 0, bool forEmail = false)
        {
            //var tasksites = _siteTaskRepository.GetByPredicate(x => x.SiteId == siteId && x.IsCurrentlyPublished).ToList();
            var siteIdQuery = siteId == 0 ? "" : $"AND SiteId = {siteId}";
            var userIdQuery = forEmail && userId != 0 ? $"AND PerformerId = {userId}" : ""; 
            var tasksites = _siteTaskRepository.GetAllByRawString($"SELECT * FROM SiteTask WHERE IsCurrentlyPublished = 1 {siteIdQuery} {userIdQuery}").ToList();
       
            if (siteId == -2)
            {
                tasksites = _siteTaskRepository.GetAllByRawString($"SELECT ST.* FROM SiteTask ST LEFT JOIN Sites S ON ST.SiteId = S.Id LEFT JOIN Accounts A ON A.Id = S.AccountId WHERE A.Id = {accountId} AND ST.IsCurrentlyPublished = 1").ToList();

                var r = _roleRepository.GetAllByRawString($"SELECT Roles.* FROM UserRoles INNER JOIN Roles ON Roles.Id = UserRoles.RoleId WHERE UserRoles.UserId = {userId}").ToList();

                if (r.Any() && r.FirstOrDefault().Name != "admin") 
                {
                    var usmr = _userModuleRoleRepository.GetAllByRawString($"SELECT * FROM UserModuleRoles WHERE UserId = {userId}").ToList();
                    var taskToAdd = new List<SiteTask>();
                    var allowedSiteIds = usmr.Where(x=>x.Role.Name != "viewer").Select(x => x.SiteId);
                    if (allowedSiteIds.Any()) 
                    {
                        allowedSiteIds = allowedSiteIds.Distinct().Where(x=> x.HasValue);
                        //tasksites = tasksites.Where(rv => allowedSiteIds.Any(v => rv.SiteId == v.Value)).ToList();

                        taskToAdd.AddRange(tasksites.Where(rv => allowedSiteIds.Any(v => rv.SiteId == v.Value)).ToList());
                    }

                    var allowedSitesViewerRole = usmr.Where(x => x.Role.Name == "viewer").Select(x =>  new {
                        SiteId = x.SiteId,
                        UserId = x.UserId
                    });

                    if (allowedSitesViewerRole.Any()) 
                    {
                        var tt = tasksites.Where(rv => allowedSitesViewerRole.Any(v => rv.SiteId == v.SiteId && v.UserId == rv.PerformerId)).ToList();
                        var test = tasksites.Where(rv => allowedSitesViewerRole.Any(v => rv.SiteId == v.SiteId)).ToList(); 
                        taskToAdd.AddRange(tt);
                    }

                    tasksites = taskToAdd;
                }
            }
      


          
            var models = new List<TaskModel>();

            var tasksiteids = tasksites.Select(x => x.TaskId).ToList();

            if (tasksiteids.Any())
            {
                var tasks = _taskRepository.GetAllWithChildsIncluded(tasksiteids);

                var roleIds = tasks.Select(v => v.RoleId).ToList();
                var activityRoles = _activityRoleRepository.GetByPredicate(x => roleIds.Contains(x.ActivityRoleId)).ToList();

                var sitetaskIds = tasksites.Select(v => v.Id).ToList();
                var logs = _taskRepository.GetSiteTaskLogs(sitetaskIds).ToList();

                if (accountId == 0)
                {
                    accountId = _siteRepository.Get(siteId).AccountId;
                }

                var intervals = _intervalRepository.GetByPredicate(x => x.AccountId == accountId).ToList();

              
                var users = _userModuleRoleService.GetUserModuleRolesBySiteId(siteId);

                if (!users.Any()) 
                {
                    var objectIds = tasksites.Select(x => x.SiteId).Distinct().ToList();
                    users = _userModuleRoleService.GetUserModuleRolesBySiteId(objectIds);
                }
            
                tasksites.ForEach(x =>
                {
                    var task = tasks.SingleOrDefault(r => r.Id == x.TaskId && r.AccountId == accountId);

                    if (task != null)
                    {
                        var role = activityRoles.SingleOrDefault(r => r.ActivityRoleId == task.RoleId);
                        var test = task.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                        {
                            Id = r.Id,
                            MainCategoryId = r.MainCategoryId,
                            TaskId = r.TaskId,
                            MainCategory = new MainCategoryModel
                            {
                                Name = r.MainCategory.Name
                            }
                        }).ToList();
                        var test2 = task.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                        {
                            Id = r.Id,
                            SubCategoryId = r.SubCategoryId,
                            TaskId = r.TaskId,
                            SubCategory = new SubCategoryModel
                            {
                                Name = r.SubCategory.Name
                            }
                        }).ToList();


                        var user = users.SingleOrDefault(r => r.Id == x.PerformerId);

                        if (user != null)
                        {
                            x.Performer = $"{user.FirstName} {user.LastName}";
                        }

                        var nn = x.Site !=null? x.Site.Name:"";
                      
                        var tModel = new TaskModel()
                        {
                            SiteId = x.SiteId,
                            SiteName = nn,
                            Id = x.TaskId,
                            Description = task.Description,
                            Interval = task.Interval,
                            NuberOfDays = task.NuberOfDays,
                            Purpose = task.Purpose,
                            TaskNo = task.TaskNo,
                            Title = task.Title,
                            LastUpdatedTime = task.LastUpdatedTime,
                            LastUpdatedBy = task.LastUpdatedBy,
                            AccountId = task.AccountId,
                            RemindAfter = task.RemindAfter,
                            ExtensionsAllowed = task.ExtensionsAllowed,
                            DayToComplete = task.DayToComplete,
                            RecommendedRole = x.Performer,
                            TaskMainCategory = task.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                            {
                                Id = r.Id,
                                MainCategoryId = r.MainCategoryId,
                                TaskId = r.TaskId,
                                MainCategory = new MainCategoryModel
                                {
                                    Name = r.MainCategory.Name
                                }
                            }).Any() ? task.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                            {
                                Id = r.Id,
                                MainCategoryId = r.MainCategoryId,
                                TaskId = r.TaskId,
                                MainCategory = new MainCategoryModel
                                {
                                    Name = r.MainCategory.Name
                                }
                            }).ToList() : new List<TaskMainCategoryModel>(),
                            TaskSubCategories = task.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                            {
                                Id = r.Id,
                                SubCategoryId = r.SubCategoryId,
                                TaskId = r.TaskId,
                                SubCategory = new SubCategoryModel
                                {
                                    Name = r.SubCategory.Name
                                }
                            }).Any() ? task.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                            {
                                Id = r.Id,
                                SubCategoryId = r.SubCategoryId,
                                TaskId = r.TaskId,
                                SubCategory = new SubCategoryModel
                                {
                                    Name = r.SubCategory.Name
                                }
                            }).ToList() : new List<TaskSubCategoryModel>(),
                            RoleId = task.RoleId,
                            Role = task.RoleId != 0 ? new ActivityRoleModel()
                            {
                                ActivityRoleId = role.ActivityRoleId,
                                Name = role.Name,
                                AccountId = role.AccountId
                            } : new ActivityRoleModel()
                        };

                        tModel.StartDate = x.StartDate;
                        var logtype = (int)TaskLogType.Confirm;

                        logs.Where(r => r.SiteTaskId == x.Id && r.LogType == logtype).OrderByDescending(t => !t.IsFromSer4).ToList().ForEach(r =>
                        {
                            if (r.IsFromSer4)
                            {
                                var interval = _intervalRepository.Get(task.Interval);

                                if (interval != null)
                                {
                                    switch ((IntervalEnum)interval.IntervalType)
                                    {
                                        case IntervalEnum.Day:
                                            tModel.StartDate = tModel.StartDate.AddDays(interval.Value);
                                            break;
                                        case IntervalEnum.Month:
                                            tModel.StartDate = tModel.StartDate.AddMonths(interval.Value);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                tModel.StartDate = tModel.StartDate;                            
                            }
                        });

                        var intervalData = _intervalRepository.Get(task.Interval);
                        if (intervalData != null && intervalData.IntervalType == 1 && intervalData.Value == 1)
                        {
                            tModel.EndDate = tModel.StartDate.AddDays(tModel.NuberOfDays - 1);
                        }
                        else
                        {
                            tModel.EndDate = tModel.StartDate.AddDays(tModel.NuberOfDays);
                        }

                        var extensionLogType = (int)TaskLogType.ActionExtension;
                        if (logs.Where(r => r.SiteTaskId == x.Id && r.LogType == extensionLogType).Any())
                        {
                            var maxInstance = logs.Where(r => r.SiteTaskId == x.Id).Max(r => r.Instance);
                            logs.Where(r => r.SiteTaskId == x.Id && r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                            {
                                tModel.EndDate = tModel.EndDate.AddDays(r.ExtendDay);
                            });
                        }

                        if (tModel.StartDate > DateTime.Now)
                        {
                            tModel.Status = "Ready";
                        }
                        else if (tModel.StartDate <= DateTime.Now)
                        {
                            tModel.Status = "Ongoing";
                        }

                        if (tModel.EndDate <= DateTime.Now)
                        {
                            tModel.Status = "Delayed";
                        }

                        models.Add(tModel);
                    }

                });



                if (userId != 0)
                {


                    var umr = _userModuleRoleRepository.GetAllByRawString($"SELECT * FROM UserModuleRoles WHERE UserId={userId} AND ModuleId =4 AND SiteId = {siteId}").ToList();

                    //_userModuleRoleRepository.GetByPredicate(r => r.UserId == userId && r.ModuleId == 4 && r.SiteId == siteId).ToList();
                    if (umr.Any())
                    {
                        if (umr.SingleOrDefault().Role.Name.ToLower() == "viewer")
                        {

                            var tempModel = new List<TaskModel>();

                            var sitetasklist = _siteTaskRepository.GetAllByRawString($"SELECT * FROM SiteTask WHERE SiteId ={siteId} AND IsCurrentlyPublished = 1 AND PerformerId= {userId}");
                            //_siteTaskRepository.GetByPredicate(r => r.SiteId == siteId && r.IsCurrentlyPublished && r.PerformerId == userId).ToList();
                            models.ForEach(v => {
                                var sitetask = sitetasklist.Where(r => r.TaskId == v.Id).ToList();

                                if (sitetask.Any())
                                {
                                    tempModel.Add(v);
                                }

                            });


                            models = tempModel;

                        }
                    }

                }

                if (string.IsNullOrEmpty(searchValue))
                {
                    return models;
                }
            }

            var testinger = models.Where(x => x.Title.ToLower().Contains(searchValue.ToLower()) || x.TaskNo.ToLower().Contains(searchValue.ToLower())).ToList();


            return models.Where(x=>x.Title.ToLower().Contains(searchValue.ToLower()) || x.TaskNo.ToLower().Contains(searchValue.ToLower())).ToList();
        }

        public string GetOngoingTasksByUserIdEmailBody(int accountId, UserModel user)
        {
            var tasks = GetAllTaskBySiteId(0, user.Id, "", accountId, true).Where(e => e.Status.ToLower() == "ongoing").OrderByDescending(e => e.Status);
            if (!tasks.Any()) return null;

            var sites = tasks.Select(e => new { Id = e.SiteId, Name = e.SiteName }).Distinct().ToList();
            var content = 
                $@"<!DOCTYPE html>
                    <html>
                    <head>
                    <div style='margin-bottom: 25px'>
	                Hi (firstname) (lastname), <br><br>
                    Here are the lists of your ongoing tasks as of (datenow). 
                    </div>
                    (tables)
                    <br><br>
                    Click here to login (<a href='https://s4.serapp.com/#/login' target='_blank'>https://s4.serapp.com/#/login</a>)<br><br>
                    If you have any questions, please contact support on sersupport@ekfelt.se <br><br>
                    Best regards<br>
                    SeR Team
                    </head>
                    </html>";
            content = content.Replace("(firstname)", user.FirstName)
                            .Replace("(lastname)", user.LastName)
                            .Replace("(datenow)", $"{DateTime.Now:yyyy-MM-dd}");
            var tables = string.Empty;
            foreach (var site in sites)
            {
                var tasksBySiteId = tasks.Where(e => e.SiteId == site.Id);

                var tableContent =
                    $@"
	            <div style='font-weight: bold; padding-top: 10px'>(sitename)</div>
                <table style='font-size: 12px; border: 1px solid black; border-collapse: collapse;'>
		            <tr>
			            <td style='width: 50px; font-weight: bold; border: 1px solid; border-spacing: 0px;border-collapse: collapse;'>
				            Status
			            </td>
			            <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; font-weight: bold;border-collapse: collapse;'>
				            Task No.
			            </td>
			            <td style='width: 400px; padding-left: 3px; padding-right: 3px; border: 1px solid black; font-weight:bold; border-collapse: collapse;'>
				            Title
			            </td>
			            <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; font-weight:bold;border-collapse: collapse;'>
				            Start Date
			            </td>
			            <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; font-weight:bold;border-collapse: collapse;'>
				            End Date
			            </td>
		            </tr>
                    (rows)
	            </table>
                ";

                var rows = string.Empty;
                tableContent = tableContent.Replace("(sitename)", site.Name);
                foreach (var row in tasksBySiteId)
                {
                    var rowContent =
                        $@"
                        <tr>
			                <td style='background-color: (color); border: 1px solid black; border-collapse: collapse'>(status)</td>
			                <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; border-collapse: collapse'>(taskno)</td>
			                <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; border-collapse: collapse'>(title)</td>
			                <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; border-collapse: collapse'>(startdate)</td>
			                <td style='padding-left: 3px; padding-right: 3px; border: 1px solid black; border-collapse: collapse'>(enddate)</td>
		                </tr>
                        ";
                    rowContent = rowContent.Replace("(color)", row.Status.ToLower() == "delayed" ? "red" : "yellow");
                    rowContent = rowContent.Replace("(status)", row.Status);
                    rowContent = rowContent.Replace("(taskno)", row.TaskNo);
                    rowContent = rowContent.Replace("(title)", row.Title);
                    rowContent = rowContent.Replace("(startdate)", $"{row.StartDate:yyyy-MM-dd}");
                    rowContent = rowContent.Replace("(enddate)", $"{row.EndDate:yyyy-MM-dd}");

                    rows = $"{rows} {rowContent}";
                }
                tableContent = tableContent.Replace("(rows)", rows);
                tables = $"{tables} {tableContent}";
            }
            content = content.Replace("(tables)", tables);
            return content;
        }

        public IEnumerable<TaskModel> GetAllTasks(int accountId, string searchText)
        {
            if (!string.IsNullOrEmpty(searchText))
            {
                return _taskRepository.GetAllTasksChildIncluded(accountId, searchText).Select(x => new TaskModel()
                {
                    LastUpdatedBy = x.LastUpdatedBy,
                    LastUpdatedTime = x.LastUpdatedTime,
                    Description = x.Description,
                    Interval = x.Interval,
                    NuberOfDays = x.NuberOfDays,
                    Purpose = x.Purpose,
                    TaskNo = x.TaskNo,
                    Title = x.Title,
                    RemindAfter = x.RemindAfter,
                    Id = x.Id,
                    RoleId = x.RoleId,
                    ExtensionsAllowed = x.ExtensionsAllowed,
                    DayToComplete = x.DayToComplete,
                    RecommendedRole = x.RecommendedRole,
                    IsArchived = x.IsArchived
                })
                .Where(x=>x.Title.ToLower().Contains(searchText.ToLower()) || x.TaskNo.ToLower().Contains(searchText.ToLower()))
                .ToList();
            }


            return _taskRepository.GetAllTasksChildIncluded(accountId, searchText).Select(x => new TaskModel()
            {
                LastUpdatedBy = x.LastUpdatedBy,
                LastUpdatedTime = x.LastUpdatedTime,
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                Id = x.Id,
                RoleId = x.RoleId,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete = x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                IsArchived = x.IsArchived
            }).ToList();
        }

        public IEnumerable<SiteTaskLogModel> GetHistory(int siteTaskId)
        {

            var sitetasks = _siteTaskRepository.Get(siteTaskId);

           // var previousHistories = _taskRepository.GetSiteTaskBySiteIdTaskIdCurrentPublished(sitetasks.SiteId, sitetasks.TaskId, false).ToList();

            var sitetaskIds = new List<int>() { siteTaskId };

           // sitetaskIds.AddRange(previousHistories.Select(x=>x.Id).ToList());
            var logs = _taskRepository.GetSiteTaskLogs(sitetaskIds).ToList();

            var data = logs.Where(r => sitetaskIds.Contains(r.SiteTaskId)).OrderByDescending(r => r.LogDate).Select(r => new SiteTaskLogModel()
            {
                Comment = r.Comment,
                Id = r.Id,
                Instance = r.Instance,
                LogDate = r.LogDate,
                LogType = (TaskLogType)r.LogType,
                SiteTaskId = r.SiteTaskId,
                Owner = r.Owner,
                IntervalDate = r.IntervalDate,
                IsPreviousHistory = false,
                IsFromSer4 = true,
            }).ToList();

            var task = _siteTaskRepository.Get(siteTaskId);
            var allPreviousTasks = _taskRepository.GetSiteTaskBySiteIdTaskIdCurrentPublished(task.SiteId, task.TaskId, false).ToList();//_siteTaskRepository.GetByPredicate(r=>r.SiteId == task.SiteId && task.TaskId == task.TaskId && !r.IsCurrentlyPublished).ToList();
            if (allPreviousTasks.Any())
            {

                //_siteTaskLogRepository.GetByPredicate(x => x.SiteTaskId == r.Id)
                var sitetaskIdsdata = allPreviousTasks.Select(x => x.Id).Distinct().ToList();
                var sitetasksdatas = _siteTaskRepository.GetSiteTaskLogs(sitetaskIdsdata);
                allPreviousTasks.ForEach(r => 
                {
                    var prevlog = sitetasksdatas.Where(rx=>rx.SiteTaskId == r.Id).Select(q => new SiteTaskLogModel()
                    {
                        Comment = q.Comment,
                        Id = q.Id,
                        Instance = q.Instance,
                        LogDate = q.LogDate,
                        LogType = (TaskLogType)q.LogType,
                        SiteTaskId = q.SiteTaskId,
                        Owner = q.Owner,
                        IntervalDate = q.IntervalDate,
                        IsPreviousHistory = true
                    }).ToList();

                    data.AddRange(prevlog);
                });
            }


            var accountId = _siteRepository.Get(task.SiteId).AccountId;
            var fileDirectoryServerLocal = ConfigurationManager.AppSettings["fileDirectoryServerLocal"];

            var theWebRequest = HttpWebRequest.Create($"{fileDirectoryServerLocal}/FileManager.aspx/HasOldSystem");
            theWebRequest.Method = "POST";
            theWebRequest.ContentType = "application/json; charset=utf-8";
            theWebRequest.Headers.Add(HttpRequestHeader.Pragma, "no-cache");

            using (var writer = theWebRequest.GetRequestStream())
            {
               var obj = JsonConvert.SerializeObject(new
                {
                    accountId= accountId,
                    moduleId = 4,
                    taskId = task.TaskId,
                    siteId = task.SiteId,
                    folder = "confirms",
                });


                string send = obj;
                //send = "";
                var vdata = Encoding.ASCII.GetBytes(send);

                writer.Write(vdata, 0, vdata.Length);
            }

            var theWebResponse = (HttpWebResponse)theWebRequest.GetResponse();
            var theResponseStream = new StreamReader(theWebResponse.GetResponseStream());
            var result = "";
            if (theWebResponse.StatusCode == HttpStatusCode.OK)
            {
                result = theResponseStream.ReadToEnd();                
            }

            if (result.Contains("true"))
            {
                data.Add(new SiteTaskLogModel()
                {
                    Id = -1,
                    LogType = TaskLogType.OldSystem,
                    Comment = "Files from old ser that could not be mapped to an action",
                });
            }           

            return data;
        }

        public SiteTaskLogModel GetSingleHistory(int id)
        {

            var r = _siteTaskLogRepository.Get(id);


            var siteTask = _siteTaskRepository.Get(r.SiteTaskId);
            var task = _taskRepository.Get(siteTask.TaskId);

            var data =  new SiteTaskLogModel()
            {
                Taskname = task.Title,
                TaskNo = task.TaskNo,
                Comment = r.Comment,
                Id = r.Id,
                Instance = r.Instance,
                LogDate = r.LogDate,
                LogType = (TaskLogType)r.LogType,
                SiteTaskId = r.SiteTaskId,
                Owner = r.Owner,
                IntervalDate = r.IntervalDate,
                IsFromSer4 = true
            };


            data.IsBimControl = task.IsBimControl;

            if (data.IsBimControl) 
            {
                var ddd = _bimControlRepository.GetAllByRawString($"SELECT * FROM BimControlItem WHERE SiteTaskLogId = {id}").ToList();

                data.Items = ddd.Select(x=> new BimControlItemModel() 
                {
                    ErrorDescription = x.ErrorDescription,
                    LittraId =x.LittraId,
                    Date = x.Date,
                    Description = x.Description,
                    Id = x.Id,
                    IssueId = x.IssueId,
                    Status = (BimControlItemStatus)x.Status,
                    Photos = !x.Photos.Any()? new List<BimControlItemImageModel>(): x.Photos.Select(tr=> new BimControlItemImageModel(){ 
                        Url =tr.Url
                    }).ToList()
                }).ToList();
            }

            return data;
        }

        public IEnumerable<SiteTaskModel> GetPublishedTask(int Id)
        {
            
            return _siteTaskRepository.GetByPredicate(x => x.TaskId == Id && x.IsCurrentlyPublished == true).Select(x=> new SiteTaskModel()
            {
                Id = x.Id,
                //InfoRoles = x.InfoRoles,
                Performer = x.Performer,
                PerformerId = x.PerformerId,
                SiteId = x.SiteId,
                TaskId = x.TaskId,
                StartDate = new SERApp.Models.Common.DateModel()
                {
                    day = x.StartDate.Day,
                    month = x.StartDate.Month,
                    year = x.StartDate.Year
                },
               
            }).ToList();

        }

        public TaskModel GetTask(int id)
        {
            var x = _taskRepository.GetWithChildsIncluded(id);
            var role = _activityRoleRepository.Get(x.RoleId);

            var archivedData = _siteTaskRepository.GetByPredicate(r => r.TaskId == id && r.SiteId !=0).ToList();
            archivedData.ForEach(r => 
            {
                r.Site = _siteRepository.Get(r.SiteId);
            });

            var name = "";
            if (x.LastUpdatedBy > 0)
            {
                var ss = _userRepository.GetAllByRawString($"SELECT * FROM Users WHERE Id = {x.LastUpdatedBy}").SingleOrDefault();
                name = ss.FirstName+ " "+ ss.LastName;
            }


            var gg = _taskHistory.GetAllByRawString($"SELECT * FROM TaskHistory WHERE TaskId = {x.Id}");
            var dd = new List<TaskHistoryModel>();


            if (gg.Any())
            {
                var users = gg.Select(r => r.UserId).ToList();
                string csv = String.Join(",", users);

                var uu = _userRepository.GetAllByRawString($"SELECT * FROM Users WHERE Id IN ({csv})").ToList();

                dd =  gg.Select(v => new TaskHistoryModel()
                {
                    Actions = v.Actions,
                    Id = v.Id,
                    LogDateTime = v.LogDateTime,
                    TaskId = v.TaskId,
                    UserId = v.UserId,
                    User = new UserModel()
                    {
                        FirstName = uu.SingleOrDefault(r=>v.UserId == r.Id).FirstName,
                        LastName = uu.SingleOrDefault(r => v.UserId == r.Id).LastName,
                    }
                }).OrderByDescending(t=>t.LogDateTime)
                .ToList();
            }
           



            return new TaskModel()
            {
                Logs = dd,
                LastUpdatedByName = name,
                LastUpdatedBy = x.LastUpdatedBy,
                LastUpdatedTime = x.LastUpdatedTime,
                Id = x.Id,
                IsArchived = x.IsArchived,
                Description = !string.IsNullOrEmpty(x.Description)? x.Description.Replace(System.Environment.NewLine, "<br/>") : "",
                Interval = x.Interval,
                CurrentServerDate = DateTime.Now,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete = x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                IsFileRequired = x.IsFileRequired,
                ArchivedData = archivedData.Select(r => new SiteTaskModel()
                {
                    Site = r.Site != null ? new SiteModel()
                    {
                        Name = r.Site.Name
                    } : new SiteModel()
                    {
                        Name = "Site Already Deleted"
                    },
                    Performer = r.Performer,
                    PerformerId = r.PerformerId,
                    Id = r.Id,
                    StartDate = new SERApp.Models.Common.DateModel()
                    {
                        day = r.StartDate.Day,
                        month = r.StartDate.Month,
                        year = r.StartDate.Year
                    },
                    StartDateString = r.StartDate.ToString("yyyy-MM-dd"),
                    PublishDate = r.PublishDate
                }).ToList(),
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList() ,
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList() ,
                RoleId = x.RoleId,
                Role = x.RoleId != 0 ? new ActivityRoleModel()
                {
                    ActivityRoleId = role.ActivityRoleId,
                    Name = role.Name,
                    AccountId = role.AccountId
                } : new ActivityRoleModel()
            };
        }

        public TaskModel GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            var x = _taskRepository.GetWithChildsIncluded(taskId);
            var role = _taskRepository.GetActivityRoleByRoleId(x.RoleId);

            var task = new TaskModel()
            {
                LastUpdatedBy = x.LastUpdatedBy,
                LastUpdatedTime = x.LastUpdatedTime,
                Id = x.Id,
                IsFileRequired = x.IsFileRequired,
                IsBimControl = x.IsBimControl,
                Description = x.Description.Replace(System.Environment.NewLine, "<br/>"),
                Interval = x.Interval,
                CurrentServerDate = DateTime.Now,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete = x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                Role = x.RoleId !=0? new ActivityRoleModel
                {
                    ActivityRoleId = role.ActivityRoleId,
                    Name = role.Name,
                    AccountId = role.AccountId
                }: new ActivityRoleModel(),
                RoleId = x.RoleId,
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList(),
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList(),
            };


            var sitetask = _taskRepository.GetSiteTaskBySiteIdTaskIdCurrentPublished(siteId, taskId, true).SingleOrDefault(); //_siteTaskRepository.GetByPredicate(v => v.SiteId == siteId && v.TaskId == taskId &&  v.IsCurrentlyPublished).SingleOrDefault();

            task.StartDate = sitetask.StartDate;
            var logtype = (int)TaskLogType.Confirm;
            //var previousPublishData = _taskRepository.GetSiteTaskBySiteIdTaskIdCurrentPublished(siteId,taskId,false).ToList();

            var sitetaskIds = new List<int>() { sitetask.Id };

            //sitetaskIds.AddRange(previousPublishData.Select(r=>r.Id).ToList());

            var logs = _taskRepository.GetSiteTaskLogs(sitetaskIds).ToList();
           
            logs.Where(r => r.SiteTaskId == sitetask.Id && r.LogType == logtype).OrderByDescending(t => !t.IsFromSer4).ToList().ForEach(r =>
            {
                if (r.IsFromSer4)
                {
                    var interval = _intervalRepository.Get(task.Interval);

                    if (interval != null)
                    {
                        switch ((IntervalEnum)interval.IntervalType)
                        {
                            case IntervalEnum.Day:
                                task.StartDate = task.StartDate.AddDays(interval.Value);
                                break;
                            case IntervalEnum.Month:
                                task.StartDate = task.StartDate.AddMonths(interval.Value);
                                break;
                        }
                    }
                }
                else
                {
                    task.StartDate = sitetask.StartDate;
                    //if (logs.Where(v => v.SiteTaskId == x.Id && v.LogType == logtype).Any())
                    //{
                    //    task.StartDate = logs.Where(v => v.SiteTaskId == x.Id && v.LogType == logtype).Max(z => z.IntervalDate);
                    //}
                }
            });

            var intervalData = _taskRepository.GetIntervalById(task.Interval);
            if (intervalData != null && intervalData.IntervalType == 1 && intervalData.Value == 1)
            {
                task.EndDate = task.StartDate.AddDays(x.NuberOfDays -1);
            }
            else
            {
                task.EndDate = task.StartDate.AddDays(x.NuberOfDays);
            }

            
          
            var extensionLogType = (int)TaskLogType.ActionExtension;
            bool hasInstance = false;
            var maxI = 0;
            if (logs.Where(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType).Any())
            {
                var maxInstance = logs.Where(r => r.SiteTaskId == sitetask.Id).Max(r => r.Instance);
                logs.Where(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                {
                    task.EndDate = task.EndDate.AddDays(r.ExtendDay);
                });
                hasInstance = true;
                maxI = maxInstance;
            }
          
            task.SiteTaskId = sitetask.Id;

            if (task.StartDate > DateTime.Now)
            {
                task.Status = "Ready";
            }
            else if (task.StartDate <= DateTime.Now)
            {
                task.Status = "Ongoing";
            }

            if (task.EndDate <= DateTime.Now)
            {
                task.Status = "Delayed";
            }

            if (hasInstance)
            {
                int extensionsCount = 0;
                logs.Where(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType && r.Instance == maxI).ToList().ForEach(r =>
                {
                    extensionsCount++;
                });

                task.RemainingExtensionsAllowed = task.ExtensionsAllowed - extensionsCount;
            }
            else {
                int extensionsCount = 0;
                logs.Where(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType).ToList().ForEach(r =>
                {
                    extensionsCount++;
                });

                task.RemainingExtensionsAllowed = task.ExtensionsAllowed - extensionsCount;
            }

            if (DateTime.Now >= task.StartDate)
            {
                task.IsAllowedToAddExtension = true;
            }
            else {
                task.IsAllowedToAddExtension = false;
            }

            task.StartDateModel = new SERApp.Models.Common.DateModel()
            {
                day = task.StartDate.Day,
                month = task.StartDate.Month,
                year = task.StartDate.Year
            };

            _taskRepository.GetSiteTaskBySiteTaskId(task.SiteTaskId);
            var ma = _taskRepository.GetSiteTaskBySiteTaskId(task.SiteTaskId);//_siteTaskRepository.Get(task.SiteTaskId);
            task.SiteTask = new SiteTaskModel()
            {
                Id = ma.Id,
                Performer = ma.Performer,
                PerformerId = ma.PerformerId,
                IsCurrentlyPublished = ma.IsCurrentlyPublished,
                TaskId = ma.TaskId,
                SiteId = ma.SiteId
            };

            return task;

        }

        public void PerformAction(SiteTaskLogModel model)
        {
            var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
            sitetask.Task = _taskRepository.Get(sitetask.TaskId);

            var data = _taskRepository.GetSiteTaskLogBySiteTaskId(sitetask.Id);
            var maxInstance = 0;
            if (data.Any())
            {
                maxInstance = data.Max(r => r.Instance);
            }
           

            if (model.LogType == TaskLogType.ActionExtension)
            {              
                var extensionLogType = (int)TaskLogType.ActionExtension;
                int extensionsCount = 0;

                if (data.Any())
                {
                    _taskRepository.GetSiteTaskLogBySiteTaskId(sitetask.Id).Where(r=> r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                    {
                        extensionsCount++;
                    });
                }
                else
                {
                    _taskRepository.GetSiteTaskLogBySiteTaskId(sitetask.Id).Where(r => r.LogType == extensionLogType).ToList().ForEach(r =>
                    {
                        extensionsCount++;
                    });
                }
              


                if (sitetask.Task.ExtensionsAllowed <= extensionsCount)
                {
                    return;
                }
            }

            

            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = TimeZoneInfo.ConvertTimeToUtc(model.LogDate),
                LogType = (int)model.LogType,
                SiteTaskId = model.SiteTaskId,
                Instance = maxInstance,
                Owner = model.Owner,
                IntervalDate = model.IntervalDate,
                ExtendDay = model.ExtendDay,
                IsFromSer4 = true
            };
            //model.LogDate = tasklog.LogDate;
            _siteTaskLogRepository.Save(tasklog);


            var d = _siteTaskLogRepository.Get(tasklog.Id);
            model.LogDate = d.LogDate;
        }

        public void PublishTask(SiteTaskModel model)
        {
            var user = _userRepository.Get(model.PerformerId);
            var role = _userModuleRoleRepository.GetByPredicate(r => r.ModuleId == 4 && r.UserId == user.Id && r.SiteId == model.SiteId).SingleOrDefault();
            role.Role = _roleRepository.Get(role.Id);
            var datetimenow = DateTime.Now;
            _siteTaskRepository.Save(new SiteTask()
            {
                SiteId = model.SiteId,
                InfoRoles = model.InfoRoles,
                Performer = user.FirstName+ " "+user.LastName+" - "+role.Role.Name,
                PerformerId = model.PerformerId,
                StartDate =  new DateTime(model.StartDate.year,model.StartDate.month,model.StartDate.day, datetimenow.Hour, datetimenow.Minute, datetimenow.Second),
                TaskId = model.TaskId,
                IsCurrentlyPublished = true,
                PublishDate = DateTime.Now,              
            });
        }

        public void UnPublishTask(SiteTaskModel model)
        {
            var data = _siteTaskRepository.Get(model.Id);

            data.IsCurrentlyPublished = false;

            _siteTaskRepository.Update(data);
        }

        public ConfirmMultipleResultModel ConfirmMultiple(SiteTaskLogModel model)
        {
            var result = new ConfirmMultipleResultModel();
            result.Ids = new List<int>();
            var loop = model.loopConfirm;
            //var dates = new List<DateTime>();
            //dates.Add(model.LogDate);
            result.Dates = new List<DateTime>();

            for (int w = 0; w < loop; w++) {

                var sitetasklog = _taskRepository.GetSiteTaskLogBySiteTaskId(model.SiteTaskId); //_siteTaskLogRepository.GetByPredicate(x => x.SiteTaskId == model.SiteTaskId);
                var sitetask = _taskRepository.GetSiteTaskBySiteTaskId(model.SiteTaskId);
                var task = _taskRepository.Get(sitetask.TaskId);

                int instance = 0;
                if (sitetasklog.Any())
                {
                    instance = sitetasklog.Max(x => x.Instance) + 1;
                }

                var tasklog = new SiteTaskLog()
                {
                    Comment = model.Comment,
                    LogDate = model.LogDate,
                    LogType = (int)TaskLogType.Confirm,
                    SiteTaskId = model.SiteTaskId,
                    Instance = instance,
                    Owner = model.Owner,
                    IntervalDate = model.IntervalDate,
                    IsFromSer4 = true,
                };


              

                var d =  _siteTaskLogRepository.Save(tasklog);
                result.Ids.Add(d.Id);
                result.Dates.Add(model.IntervalDate);
                try
                {
                    var interval = _intervalRepository.Get(task.Interval);

                    if (interval != null)
                    {
                        switch ((IntervalEnum)interval.IntervalType)
                        {
                            case IntervalEnum.Day:
                                model.IntervalDate = model.IntervalDate.AddDays(interval.Value);
                                break;
                            case IntervalEnum.Month:
                                model.IntervalDate = model.IntervalDate.AddMonths(interval.Value);
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                //var d = GetTaskBySiteIdAndTaskId(sitetask.TaskId, sitetask.SiteId);
                //model = new SiteTaskLogModel()
                //{
                //    Comment = model.Comment,
                //    LogDate = DateTime.Now,
                //    SiteTaskId = model.SiteTaskId,
                //    LogType = TaskLogType.Confirm,
                //    Instance = instance,
                //    Owner = model.Owner,
                //    IntervalDate = d.StartDate,
                //    IsFromSer4 = true
                //};

                //Thread.Sleep(5);
                //dates.Add(tasklog.LogDate);
            }

            return result;
        }

        public void ReAssign(SiteTaskModel model)
        {
            var datetimenow = DateTime.Now;
            var data = _siteTaskRepository.Get(model.Id);
            data.Performer = model.Performer;
            data.PerformerId = model.PerformerId;
            data.StartDate = new DateTime(model.StartDate.year,model.StartDate.month,model.StartDate.day, datetimenow.Hour, datetimenow.Minute, datetimenow.Second);

            _siteTaskRepository.Update(data);
        }

        public ArchiveDataModel ArchiveTask(int id)
        {
            var task = _taskRepository.Get(id);
            task.IsArchived = true;
            _taskRepository.Update(task);

            return new ArchiveDataModel() { IsArchived = true };
        }

        public ArchiveDataModel UnarchiveTask(int id)
        {
            var task = _taskRepository.Get(id);
            task.IsArchived = false;
            _taskRepository.Update(task);

            return new ArchiveDataModel() { IsArchived = false };
        }

        public MainCategoryModel GetAllMainCategory(int id)
        {
            var data = _mainCategoryRepository.Get(id);
            return new MainCategoryModel()
            {
                Id = data.Id,
                Name = data.Name
            };
        }

        public SubCategoryModel GetSubCategory(int id)
        {
            var data = _subCategoryRepository.Get(id);
            return new SubCategoryModel()
            {
                Id = data.Id,
                Name = data.Name,
            };
        }

        public List<IGrouping<string, SiteTaskModel>> GetByUserId(int id)
        {
            var data = _siteTaskRepository.GetAllSiteTasksChildIncluded().Where(r => r.PerformerId == id && r.IsCurrentlyPublished)
                .Select(x=> new SiteTaskModel()
                {
                    Id = x.Id,
                    InfoRoles = x.InfoRoles,
                    IsCurrentlyPublished = x.IsCurrentlyPublished,
                    IsPublished = x.IsCurrentlyPublished,
                    Performer = x.Performer,
                    PerformerId = x.PerformerId,
                    PublishDate = x.PublishDate,
                    Site = new SiteModel()
                    {
                        Name = x.Site.Name,
                        Id = x.Site.Id
                    },
                    SiteId = x.SiteId,
                    StartDate = new SERApp.Models.Common.DateModel()
                    {
                        day = x.StartDate.Day,
                        month = x.StartDate.Month,
                        year = x.StartDate.Year
                    },
                    TaskId = x.TaskId,
                    Task = new TaskModel()
                    {
                        Id = x.Task.Id,
                        IsArchived = x.Task.IsArchived,
                        CurrentServerDate = new DateTime(),
                        DayToComplete = x.Task.DayToComplete,
                        NuberOfDays = x.Task.NuberOfDays,
                        Title = x.Task.Title,
                        TaskNo = x.Task.TaskNo,
                        Interval = x.Task.Interval,
                        Description = x.Task.Description,
                        Purpose = x.Task.Purpose,
                        RoleId= x.Task.RoleId,
                        
                    }
                })
                .ToList();

            var model = data.GroupBy(v => v.Site.Name).ToList();
            
            return model;
        }

        public void Transfer(SiteTaskModel model)
        {
            var data = _siteTaskRepository.Get(model.Id);
            data.PerformerId = model.PerformerId;

            var users = _userModuleRoleService.GetUserModuleRolesBySiteId(model.SiteId);

            var user = users.SingleOrDefault(x => x.Id == model.PerformerId);

            if (user != null)
            {
                data.Performer = $"{user.FirstName} {user.LastName} - {user.RoleName}";
            }

            _siteTaskRepository.Update(data);
        }

        public IEnumerable<ActivityRoleModel> GetActivityRoles(int accountId)
        {
            var data = _activityRoleRepository
                .GetByPredicate(x=>x.AccountId == accountId)
                .Select(x=> new ActivityRoleModel()
            {
                ActivityRoleId = x.ActivityRoleId,
                Name = x.Name,
                AccountId = x.AccountId
            });

            return data;
        }

        public void AddActivityRole(ActivityRoleModel model)
        {
            if (model.ActivityRoleId == 0)
            {
                _activityRoleRepository.Save(new ActivityRole()
                {
                    Name = model.Name,
                    AccountId = model.AccountId
                });
            }
            else {
                var existingData = _activityRoleRepository.Get(model.ActivityRoleId);
                existingData.Name = model.Name;
                existingData.AccountId = model.AccountId;

                _activityRoleRepository.Update(existingData);

            }
        }

        public ActivityRoleModel GetActivityRole(int id)
        {
            var data = _activityRoleRepository.Get(id);
            return new ActivityRoleModel()
            {
                AccountId = data.AccountId,
                ActivityRoleId = data.ActivityRoleId,
                Name = data.Name
            };
        }

        public DeleteResultModel DeleteActivityRole(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskRepository.GetByPredicate(r => r.RoleId == id);

            if (data.Any())
            {
                model.IsDeleted = false;
                return model;
            }


            _activityRoleRepository.Delete(id);
            return model;
        }

        public byte[] GetXls(int siteId, int userId = 0, string searchValue = "")
        {
            using (var excel = new ExcelPackage())
            {
                var se = new CultureInfo("sv-SE");
                var activities = this.GetAllTaskBySiteId(siteId, userId,searchValue).OrderBy(x => x.TaskNo, StringComparer.Create(se, false)).ToList();

                var summarySheet = excel.Workbook.Worksheets.Add($"Activity");
                summarySheet.Column(1).Width = 10;
                summarySheet.Column(2).Width = 30;
                summarySheet.Column(3).Width = 30;
                summarySheet.Column(4).Width = 12;
                summarySheet.Column(5).Width = 12;
                summarySheet.Column(6).Width = 30;

                summarySheet.Cells[1, 1].Value = "Status";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Task Number";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

                summarySheet.Cells[1, 3].Value = "Title";
                summarySheet.Cells[1, 3].Style.Font.Bold = true;

                summarySheet.Cells[1, 4].Value = "Start Date";
                summarySheet.Cells[1, 4].Style.Font.Bold = true;

                summarySheet.Cells[1, 5].Value = "End Date";
                summarySheet.Cells[1, 5].Style.Font.Bold = true;

                summarySheet.Cells[1, 6].Value = "Assigned to";
                summarySheet.Cells[1, 6].Style.Font.Bold = true;

                int startRow = 2;
                activities.ForEach(x => 
                {
                    summarySheet.Cells[startRow, 1].Value = x.Status;
                    summarySheet.Cells[startRow, 2].Value = x.TaskNo;
                    summarySheet.Cells[startRow, 3].Value = x.Title;
                    summarySheet.Cells[startRow, 4].Value = x.StartDate.ToString("yyyy-MM-dd");
                    summarySheet.Cells[startRow, 5].Value = x.EndDate.ToString("yyyy-MM-dd");
                    summarySheet.Cells[startRow, 6].Value = x.RecommendedRole;

                    startRow++;
                });

                return excel.GetAsByteArray();
            }
        }

        public byte[] GetXlsAllTask(int accountId, string searchText)
        {
            using (var excel = new ExcelPackage())
            {

                var se = new CultureInfo("sv-SE");
                var activities = GetAllTasks(accountId, searchText).OrderBy(c => c.TaskNo, StringComparer.Create(se, false)).ToList();

                var summarySheet = excel.Workbook.Worksheets.Add($"Activity");
                summarySheet.Column(1).Width = 30;
                summarySheet.Column(2).Width = 30;
            

                summarySheet.Cells[1, 1].Value = "Task Number";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Title";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

          

                int startRow = 2;
                activities.ForEach(x =>
                {
                    summarySheet.Cells[startRow, 1].Value = x.TaskNo;
                    summarySheet.Cells[startRow, 2].Value = x.Title;                
                    startRow++;
                });

                return excel.GetAsByteArray();
            }
        }

        public DeleteResultModel DeleteSiteTaskLog(int id)
        {
            var result = new DeleteResultModel();
            try {
                this._siteTaskLogRepository.Delete(id);
                result.IsDeleted = true;
            } catch (Exception ex) 
            {
                result.IsDeleted = false;
            }

            return result;
        }

        public DateTime GetCurrentIntervalDate(int siteTaskId)
        {
            var data = _siteTaskLogRepository.GetAllByRawString($"SELECT * FROM SiteTaskLog WHERE SiteTaskId = {siteTaskId}").ToList();

            if (data != null && data.Any())
            {
                var siteTask = _siteTaskRepository.Get(siteTaskId);
                var task = _taskRepository.Get(siteTask.TaskId);
                var interval = _intervalRepository.Get(task.Interval);
                var dateToReturn = siteTask.StartDate;
                data.ForEach(x => 
                {
                    

                    if (interval != null)
                    {
                        switch ((IntervalEnum)interval.IntervalType)
                        {
                            case IntervalEnum.Day:
                                dateToReturn = dateToReturn.AddDays(interval.Value);
                                break;
                            case IntervalEnum.Month:
                                dateToReturn = dateToReturn.AddMonths(interval.Value);
                                break;
                        }
                    }
                });


                return dateToReturn;
            }
            else 
            {
                return _siteTaskRepository.Get(siteTaskId).StartDate;            
            }

        }

        public SiteTaskLogModel GetHistoryByAccountSiteTask(int accountId, int siteId, int taskId)
        {
            var r = _siteTaskLogRepository.GetAllByRawString($"SELECT STL.* FROM SiteTaskLog STL INNER JOIN SiteTask ST ON ST.Id = STL.SiteTaskId WHERE ST.SiteId={siteId} AND ST.TaskId={taskId} AND ST.IsCurrentlyPublished = 1").FirstOrDefault();
          
            var siteTask = _siteTaskRepository.Get(r.SiteTaskId);
            var task = _taskRepository.Get(siteTask.TaskId);

            var data = new SiteTaskLogModel()
            {
                Taskname = task.Title,
                TaskNo = task.TaskNo,
                Comment = r.Comment,
                Id = r.Id,
                Instance = r.Instance,
                LogDate = r.LogDate,
                LogType = (TaskLogType)r.LogType,
                SiteTaskId = r.SiteTaskId,
                Owner = r.Owner,
                IntervalDate = r.IntervalDate,
                IsFromSer4 = true
            };


            data.IsBimControl = task.IsBimControl;

            if (data.IsBimControl)
            {
                var ddd = _bimControlRepository.GetAllByRawString($"SELECT * FROM BimControlItem WHERE SiteTaskLogId = {r.Id}").ToList();

                data.Items = ddd.Select(x => new BimControlItemModel()
                {
                    LittraId = x.LittraId,
                    ErrorDescription = x.ErrorDescription,
                    Date = x.Date,
                    Description = x.Description,
                    Id = x.Id,
                    IssueId = x.IssueId,
                    Status = (BimControlItemStatus)x.Status,
                    Photos = !x.Photos.Any() ? new List<BimControlItemImageModel>() : x.Photos.Select(tr => new BimControlItemImageModel()
                    {
                        Url = tr.Url
                    }).ToList()
                }).ToList();
            }

            return data;
        }

        public SiteTaskModel GetSiteTask(int siteId, int taskId)
        {
           var data=  _siteTaskRepository.GetAllByRawString($"SELECT * FROM SiteTask WHERE SiteId = {siteId} AND TaskId = {taskId} AND IsCurrentlyPublished = 1" );

            if (data != null && data.Any()) 
            {
                var singleReuslt = data.FirstOrDefault();
                return new SiteTaskModel()
                {
                    Id = singleReuslt.Id,
                    InfoRoles = singleReuslt.InfoRoles,
                    IsCurrentlyPublished  = singleReuslt.IsCurrentlyPublished,
                    Performer = singleReuslt.Performer,
                    PerformerId = singleReuslt.PerformerId,
                    PublishDate = singleReuslt.PublishDate,
                    SiteId = singleReuslt.SiteId,
                    //StartDate = singleReuslt.StartDate,
                   TaskId = singleReuslt.TaskId
                };
            }

            return null;
        }

        public int GetTaskByTaskNumber(string taskNumber, int accountId)
        {
            var tasks = _taskRepository.GetAllByRawString($"SELECT * FROM Task WHERE TaskNo = '{taskNumber}' AND AccountId ={accountId}").ToList();
            if (tasks.Any()) 
            {
                return tasks.FirstOrDefault().Id;
            }

            return 0;
        }

        public string GetCompanyNameByTaskId(int taskId)
        {
            var accountId = _taskRepository.Get(taskId).AccountId;
            return _accountRepository.Get(accountId).Company;
        }
    }
}
