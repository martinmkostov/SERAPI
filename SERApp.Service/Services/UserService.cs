﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class UserService
    {
        UserRepository _userRepository;
        UserModuleRoleRepository _userRoleRepository;
        CommonRepository _commonRepository;
        AccountRepository _accountRepository;
        LogRepository _logRepository;
        SettingRepository _settingRepository;
        IRepository<Log> _logRepo1;
        IRepository<Department> _departmentRepository;
        IRepository<Data.Models.SiteTask> _sitetataskRepository;
        public UserService() {
            _userRepository = new UserRepository();
            _userRoleRepository = new UserModuleRoleRepository();
            _commonRepository = new CommonRepository();
            _accountRepository = new AccountRepository();
            _logRepository = new LogRepository();
            _logRepo1 = new Repository<Log>();
            _sitetataskRepository = new Repository<Data.Models.SiteTask>();
            _departmentRepository = new Repository<Department>();
            _settingRepository = new SettingRepository();
        }
        private UserModel CreateAuth(UserModel model) {
            AuthenticationService authService = new AuthenticationService();
            return authService.CreateAuth(model.Password);
        }

        public UserModel Get(int id) {
            try {
                var user = _userRepository.Get(id);
                return entityToModel(user);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, id);
                throw ex;
            }
            
        }

        public List<UserModel> GetByAccountId(int id) {
            try
            {
                var se = new CultureInfo("sv-SE");
                TextInfo textInfo = se.TextInfo;

                var data = _userRepository.GetAll()
                .Where(u => u.AccountId == id).ToList();

                var accountAdmin = data.FirstOrDefault();

                var ddd = data.Select(u => u.Id).Distinct().ToList();
                var roles = _userRoleRepository.GetUserRoles(ddd);
                var departments = _departmentRepository.GetAllByRawString($"SELECT * FROM Departments WHERE AccountId = {id}").ToList();
                var userRoles = _userRoleRepository.GetUserRolesData(ddd);

                var logs = new List<Log>();
                if (ddd.Any()) 
                {
                    string csv = String.Join(",", ddd.Select(x => x.ToString()).ToArray());
                    logs = _logRepo1.GetAllByRawString($"SELECT * FROM Logs WHERE UserId IN ({csv})").ToList();
                }
                return data
                .Select(u =>
                {
                    var role = "User";
                    if (u.Id == accountAdmin.Id)
                    {
                        role = "Account Admin";
                    }
                    else if (u.IsAdministrationAdmin)
                    {
                        role = "Admin";
                    }

                    var user = new UserModel()
                    {
                        Id = u.Id,
                        IsActive = u.IsActive,
                        IsDeleted = u.IsDeleted,
                        CreatedDate = u.CreatedDate,
                        LastUpdatedDate = u.LastUpdatedDate,
                        FirstName = textInfo.ToTitleCase(u.FirstName == null ? "" : u.FirstName),
                        LastName = textInfo.ToTitleCase(u.LastName == null ? "" : u.LastName),
                        Mobile = u.Mobile,
                        PhoneNumber = u.PhoneNumber,
                        EmailAddress = u.EmailAddress,
                        UserName = u.UserName,
                        AccountId = u.AccountId,
                        DepartmentId = u.DepartmentId,
                        Department = u.DepartmentId != 0
                            ? new DepartmentModel()
                            {
                                Name = departments.FirstOrDefault(x => x.Id == u.DepartmentId)?.Name
                            }
                            : new DepartmentModel(),
                        Account = entityToModel(u.Account),
                        Role = entityToModel(_userRoleRepository.GetUserRole(u.Id, roles, userRoles)),
                        RoleName = role,
                        LastLoginDateTime = LastLoginDateTimeByUserId(u.Id, logs, u.AccountId)
                    };

                    return user;
                })
                .OrderBy(u => u.FirstName,StringComparer.Create(se,false))
                .ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, id);
                return null;
            }
            
        }

        public List<UserModel> GetAll()
        {
            try
            {
                var data = _userRepository.GetAll();
                var ddd = data.Select(u => u.Id).Distinct().ToList();
                var logs = new List<Log>();
                if (ddd.Any())
                {
                    string csv = String.Join(",", ddd.Select(x => x.ToString()).ToArray());
                    logs = _logRepo1.GetAllByRawString($"SELECT * FROM Logs WHERE UserId IN ({csv})").ToList();
                }

                var t = data
                .Select(u => new UserModel()
                {
                    Id = u.Id,
                    IsActive = u.IsActive,
                    IsDeleted = u.IsDeleted,
                    CreatedDate = u.CreatedDate,
                    LastUpdatedDate = u.LastUpdatedDate,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Mobile = u.Mobile,
                    PhoneNumber = u.PhoneNumber,
                    EmailAddress = u.EmailAddress,
                    UserName = u.UserName,
                    AccountId = u.AccountId,
                    Account = entityToModel(u.Account),
                    RoleName = entityToModel(_userRoleRepository.GetUserRole(u.Id)).Name,
                    LastLoginDateTime = LastLoginDateTimeByUserId(u.Id, logs, u.AccountId)
                }).ToList();

                var se = new CultureInfo("sv-SE");

                return t.OrderBy(x=>x.FirstName,StringComparer.Create(se,false)).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }
            
        }

        public List<UserModel> GetAllActiveFromActiveSites(int accountId, int moduleId)
        {
            return _userRepository.GetAllActiveFromActiveSites(accountId, moduleId).Select(e => new UserModel
            {
                Id = e.Id,
                EmailAddress = e.EmailAddress,
                FirstName = e.FirstName,
                LastName= e.LastName,
            }).ToList();
        }

        public List<UserModel> GetAllActiveUsers()
        {
            try
            {
                var data = _userRepository.GetAllActiveUsers();
                var ddd = data.Select(u => u.Id).Distinct().ToList();
                var logs = new List<Log>();
                if (ddd.Any())
                {
                    string csv = String.Join(",", ddd.Select(x => x.ToString()).ToArray());
                    logs = _logRepo1.GetAllByRawString($"SELECT * FROM Logs WHERE UserId IN ({csv})").ToList();
                }


                return data
                .Select(u => new UserModel()
                {
                    Id = u.Id,
                    IsActive = u.IsActive,
                    IsDeleted = u.IsDeleted,
                    CreatedDate = u.CreatedDate,
                    LastUpdatedDate = u.LastUpdatedDate,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Mobile = u.Mobile,
                    PhoneNumber = u.PhoneNumber,
                    EmailAddress = u.EmailAddress,
                    UserName = u.UserName,
                    AccountId = u.AccountId,
                    Account = entityToModel(u.Account),
                    RoleName = entityToModel(_userRoleRepository.GetUserRole(u.Id)).Name,
                    LastLoginDateTime = LastLoginDateTimeByUserId(u.Id, logs, u.AccountId)
                }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }

        }

        public UserModel SaveUser(UserModel model) {
            try
            {
                if (model.UpdateRoleOnly)
                {
                    _userRepository.SaveUser(model, false);
                    return model;
                }

                if ((_userRepository.Get(model.Id) == null && _userRepository.Get(model.UserName) != null) || CheckUsernameDuplicate(model))
                {
                    throw new Exception("Could not save this user since it's already been taken. Choose another username.");
                }
                else if (_userRepository.Get(model.Id) == null)
                {
                    CreateUser(model);
                    _logRepository.Log(LogTypeEnum.Information,
                        LogMessagingSettings.Save,
                        LogMessagingSettings.MessageCreateUser, model.AccountId, 0);

                }

                if (_userRepository.Get(model.Id) == null || !string.IsNullOrEmpty(model.Password))
                {
                    string tempPass = Security.GenerateTempPassword();

                    model.Password = !string.IsNullOrEmpty(model.Password) ? model.Password : tempPass;

                    UserModel authModel = CreateAuth(model);
                    model.HashedPassword = authModel.HashedPassword;
                    model.RandomSecret = authModel.RandomSecret;
                }

                bool updatePass = true;
                if (model.IsUpdate && string.IsNullOrEmpty(model.Password))
                {
                    updatePass = false;
                }

                var newModel = _userRepository.SaveUser(model, updatePass);
                if (_userRepository.Get(model.Id) == null && model.IsSendCreds == true)
                {
                    Email.SendCreateUserConfirmation(newModel);
                }
                return newModel;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, model.AccountId, 0);
                throw ex;
            }
        }

        public UserModel UpdateUser(UserModel model)
        {
            try
            {
                if (model.UpdateRoleOnly)
                {
                    _userRepository.SaveUser(model, false);
                    return model;
                }

                if (!string.IsNullOrEmpty(model.Password))
                {
                    UserModel authModel = CreateAuth(model);
                    model.HashedPassword = authModel.HashedPassword;
                    model.RandomSecret = authModel.RandomSecret;
                }

                bool updatePass = true;
                if (model.IsUpdate && string.IsNullOrEmpty(model.Password))
                {
                    updatePass = false;
                }

                var updUser = _userRepository.SaveUser(model, updatePass);

                if (_userRepository.Get(model.Id) != null && model.IsSendCreds == true)
                {
                    Email.SendCreateUserConfirmation(updUser);
                }

                return updUser;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, model.AccountId, 0);
                throw ex;
            }
        }

        public bool ResendCreateUserConfirmation(UserModel model)
        {
            try
            {
                var user = _userRepository.Get(model.Id);
                if (user != null) {
                    AuthenticationService service = new AuthenticationService();
                    service.SendPasswordToEmailAddress(user.UserName, model.EmailAddress, model.EmailAddress);
                }
                return true;
            } catch
            {
                return false;
            }            
        }

        private UserModel CreateUser(UserModel model) {
            //for now, should always be a user
            var role = _commonRepository.GetRoleByName("user");
            model.RoleId = role.Id;
            model.RoleName = role.Name;
            model.Account = entityToModel(_accountRepository.Get(model.AccountId));
            return model;
        }

        public UserModel CreateAdminUser(AccountModel accountModel) {
            try
            {
                string tempPass = Security.GenerateTempPassword();
                var role = _commonRepository.GetRoleByName("admin");
                string adminFirstName = string.Empty;
                string adminLastName = string.Empty;

                if (!string.IsNullOrEmpty(accountModel.ContactName)) {
                    if (accountModel.ContactName.Split(' ').Length == 1)
                    {
                        adminFirstName = accountModel.ContactName.Split(' ')[0];
                    }
                    else if (accountModel.ContactName.Split(' ').Length == 2 || accountModel.ContactName.Split(' ').Length > 1)
                    {
                        adminFirstName = accountModel.ContactName.Split(' ')[0];
                        adminLastName = accountModel.ContactName.Split(' ')[1];
                    }
                }

                var model = new UserModel()
                {
                    EmailAddress = accountModel.EmailAddress,
                    Mobile = accountModel.Mobile,
                    AccountId = accountModel.Id,
                    UserName = "admin_" + accountModel.Name.ToLower().Replace(" ", "_"),
                    Password = tempPass, //candidate for change
                    RoleId = role.Id,
                    RoleName = role.Name,
                    Account = accountModel,
                    FirstName = adminFirstName,
                    LastName = adminLastName
                };

                UserModel authModel = CreateAuth(model);
                model.HashedPassword = authModel.HashedPassword;
                model.RandomSecret = authModel.RandomSecret;
                return _userRepository.SaveUser(model);
                //NO NEED FOR LOGGING, CREATE ADMIN USER LOG IS DONE ON THE EMAIL SECTION

            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Create, 0, 0);
                return null;
            }
           
        }

        public DateTime? LastLoginDateTimeByUserId(int userId, List<Log> logs, int accountId) {
            //var logs = _logRepo1.GetAllByRawString($"SELECT * FROM Logs WHERE UserId = {userId}");
            var log = _logRepository.GetLogFromLogs(LogTypeEnum.Information, "LOGIN", 0, userId, logs);
            if (log?.CreatedDate == null)
            {
                return DateTime.MinValue;
            }
            var loginDate = Convert.ToDateTime(log?.CreatedDate);

            var offset = _settingRepository.Get("Timezone", accountId).Value.Split(')')[0].Replace("(","");
            var timeZoneId = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfo.GetSystemTimeZones().LastOrDefault(x => x.DisplayName.Contains(offset)).Id);
            
            var converted = TimeZoneInfo.ConvertTime(DateTime.SpecifyKind(loginDate, DateTimeKind.Local), timeZoneId);
            
            return converted;
        }

        public bool DeleteUser(int id)
        {
            try
            {
                var act = _sitetataskRepository.GetAllByRawString($"SELECT * FROM SiteTask WHERE PerformerId = {id}");
                if (act.Any()) 
                {
                    return false;
                }

                _userRepository.DeleteUser(id);
                _logRepository.Log(LogTypeEnum.Notification,
                    LogMessagingSettings.Delete,
                    LogMessagingSettings.MessageDeleteUser, 0, id);

                return true;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, id);
                return false;
            }
        }

        private bool CheckUsernameDuplicate(UserModel model) {
            var user = _userRepository.Get(model.Id);
            if (user != null)
            {
                return (user.UserName.ToLower() != model.UserName.ToLower() && _userRepository.Get(model.UserName.ToLower()) != null) ;
            }
            return false;
        }
        //@TODO : have a common service for this one
        public static RoleModel entityToModel(Role entity)
        {
            if (entity == null)
                return null;

            return new RoleModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                IsModule = entity.IsModule,
                IsUser = entity.IsUser
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        public UserModel entityToModel(User entity)
        {

            var logs = _logRepo1.GetAllByRawString($"SELECT * FROM Logs WHERE UserId = {entity.Id}").ToList();

            return new UserModel()
            {
                Id = entity.Id,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                FirstName = entity.FirstName,
                EmailAddress = entity.EmailAddress,
                LastName = entity.LastName,
                Mobile = entity.Mobile,
                PhoneNumber = entity.PhoneNumber,
                UserName = entity.UserName,
                HashedPassword = entity.HashedPassword,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Role = entityToModel(_userRoleRepository.GetUserRole(entity.Id)),
                DepartmentId = entity.DepartmentId,
                LastLoginDateTime = LastLoginDateTimeByUserId(entity.Id, logs, entity.AccountId),  
                IsActivityAdmin = entity.IsActivityAdmin,
                IsAdministrationAdmin = entity.IsAdministrationAdmin,
                EnablePopupMobileMessage = entity.EnablePopupMobileMessage
            };
        }
    }
}
