﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using OfficeOpenXml;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SERApp.Service.Services
{
    public interface IContractService
    {
        IEnumerable<ContractRoleModel> GetContractRoles(int accountId);
        int AddContractRole(ContractRoleModel model);


        bool DeleteContractRole(int Id);
        ContractRoleModel UpdateContractRole(ContractRoleModel model);
        ContractRoleModel GetContractRoleById(int id);

        IEnumerable<LegalHolderModel> GetLegalHoldersByAccountId(int accountId, bool? isActive = null);
        IEnumerable<ContractCategoryModel> GetAllContractCategory(int accountId, bool? isActive = null);
        IEnumerable<OpponentModel> GetOpponentsByAccountId(int accountId);
        IEnumerable<ContractModel> GetContracts(ContractFilters filter);
        IEnumerable<TagWordModel> GetTagWords(int accountId);
        IEnumerable<ContractTagWordModel> GetContractTagWords(int accoundId);

        TagWordModel GetTagWordById(int id);
        void AddTagWord(TagWordModel model);
        byte[] GetXls(int siteId, int accountId = 0, int userId = 0, string searchValue = "");
        IEnumerable<ContractFilesModel> GetFiles(int id);
        LegalHolderModel AddLegalHolder(LegalHolderModel model);
        ContractCategoryModel AddContractCategory(ContractCategoryModel model);
        OpponentModel AddOpponent(OpponentModel model);
        ContractModel AddContract(ContractModel model);

        LegalHolderModel GetLegalHolder(int id);
        ContractCategoryModel GetContractCategory(int id);
        OpponentModel GetOpponent(int id);
        ContractModel GetContract(int id);
        List<ContractModel> GetContractsByIds(string ids);
        List<ContractModel> GetAllContractsForExport(ContractFilters filters);


        bool DeleteLegalHolder(int id);
        bool DeleteContractCategory(int id);
        bool DeleteOpponent(int id);
        bool DeleteTagWord(int id);
        bool DeleteContract(int id);

        bool SendFiles(EmailModel model);
        bool CopyContracts(CopyContractModel model);

        byte[] DownloadFiles(ContractFilters filters);
        bool BulkUpdateYear(UpdateContractYear model);
        void SaveEmailRecipient(SaveEmailRecipient model);
    }
    public class ContractService : IContractService
    {
        IRepository<LegalHolders> _legalHoldersRepository;
        IRepository<ContractCategory> _contractCategoryRepository;
        IRepository<Opponent> _opponentRepository;
        IRepository<ContractTagWord> _contractTagWordRepository;
        IRepository<Contract> _contractRepository;
        IRepository<Site> _siteRepository;
        IRepository<TagWord> _tagWordRepository;
        IRepository<ContractLog> _contractLogRepository;
        IRepository<User> _userRepositry;
        IRepository<Tenant> _tenantRepository;
        IRepository<ContractRole> _contractRoleRepository;
        IRepository<EmailRecipient> _emailRecipientRepository;
        IRepository<Module> _moduleRepository;
        IContractRepository _icontractRepository;

        private TenantService _tenantService;

        SiteService _siteService;
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public ContractService()
        {
            _moduleRepository = new Repository<Module>();
            _emailRecipientRepository = new Repository<EmailRecipient>();
            _contractRoleRepository = new Repository<ContractRole>();
            _legalHoldersRepository = new Repository<LegalHolders>();
            _contractCategoryRepository = new Repository<ContractCategory>();
            _opponentRepository = new Repository<Opponent>();
            _contractRepository = new Repository<Contract>();
            _siteRepository = new Repository<Site>();
            _siteService = new SiteService();
            _tagWordRepository = new Repository<TagWord>();
            _contractTagWordRepository = new Repository<ContractTagWord>();
            _contractLogRepository = new Repository<ContractLog>();
            _userRepositry = new Repository<User>();
            _icontractRepository = new ContractRepository();
            _tenantRepository = new Repository<Tenant>();
            _tenantService = new TenantService();

            db = new SERAppDBContext(connString);
        }

        public IEnumerable<LegalHolderModel> GetLegalHoldersByAccountId(int accountId, bool? isActive = null)
        {

            var query = $"SELECT * FROM LegalHolders WHERE AccountId = {accountId}";
            if (isActive.HasValue)
            {
                query = $"{query} AND IsActive = '{isActive.Value}'";
            }

           var data = _legalHoldersRepository.GetAllByRawString(query);
            var se = new CultureInfo("sv-SE");
            return data.Select(x=> new LegalHolderModel()
            {
                AccountId = x.AccountId,
                Id = x.Id,
                LegalHolder = x.LegalHolder,
                IsActive = x.IsActive
            }).ToList().OrderBy(x => !x.IsActive).ThenBy(x => x.LegalHolder, StringComparer.Create(se, false));
        }

        public IEnumerable<ContractCategoryModel> GetAllContractCategory(int accountId, bool? isActive = null)
        {
            var query = $"SELECT * FROM ContractCategories WHERE GroupId={accountId}";

            if (isActive.HasValue && isActive.Value)
            {
                query = query + $" and IsActive=1";
            }

            var data = _contractCategoryRepository.GetAllByRawString(query);
            var se = new CultureInfo("sv-SE");
            return data.Select(x => new ContractCategoryModel()
            {
                CategoryDescription = x.CategoryDescription,         
                CategoryName = x.CategoryName,
                GroupId = x.GroupId,
                Id = x.Id,
                IsActive = x.IsActive
            }).ToList().OrderBy(x=>!x.IsActive).ThenBy(x=>x.CategoryName, StringComparer.Create(se, false));
        }

        public IEnumerable<OpponentModel> GetOpponentsByAccountId(int accountId)
        {
            var data = _opponentRepository.GetAllByRawString($"SELECT * FROM Opponents WHERE AccountId = {accountId}");
            var se = new CultureInfo("sv-SE");
            return data.Select(x => new OpponentModel()
            {
               AccountId= x.AccountId,
               OpponentHolderName = x.OpponentHolderName,
               Id = x.Id
            }).ToList().OrderBy(x => x.OpponentHolderName, StringComparer.Create(se, false));
        }

        public IEnumerable<ContractModel> GetContracts(ContractFilters filter)
        {

           
            var data = new List<Contract>();
            if (filter.SiteId == -1)
            {
                var sitestoFind = _siteService.GetByAccountIdAndUserId(filter.AccountId, filter.UserId, 5).Select(x => x.Id);
                if (sitestoFind.Any())
                {
                    data = _contractRepository.GetAllByRawString($"SELECT * FROM Contracts INNER JOIN Sites ON Sites.Id = Contracts.SiteId WHERE SiteId IN ({string.Join(",", sitestoFind)})").ToList();
                }
                else
                {
                    return new List<ContractModel>();
                }
            }
            else
            {
                data = _contractRepository.GetAllByRawString($"SELECT * FROM Contracts WHERE SiteId = {filter.SiteId}").ToList();
            }

            if (filter.Ramvtal == "0")
            {

            }
            else if (filter.Ramvtal == "1")
            {
                data = data.Where(x => x.Ramavtal).ToList();
            }
            else if (filter.Ramvtal == "2")
            {
                data = data.Where(x => !x.Ramavtal).ToList();
            }

            var fks = data.Select(x => new { SiteId = x.SiteId, CategoryId = x.CategoryId, LegalHolderId = x.LegalHolderId }).ToList();
            var se = new CultureInfo("sv-SE");
            var sites = new List<Site>();
            var categories = new List<ContractCategory>();
            var legalholders = new List<LegalHolders>();
            var opponents = new List<Opponent>();
            if (fks.Any())
            {
                sites = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE Id IN ({ string.Join(",", fks.Select(x => x.SiteId).ToArray())})").ToList();
                categories = _contractCategoryRepository.GetAllByRawString($"SELECT * FROM ContractCategories WHERE Id IN ({ string.Join(",", fks.Select(x => x.CategoryId).ToArray())})").ToList();
                legalholders = _legalHoldersRepository.GetAllByRawString($"SELECT * FROM LegalHolders WHERE Id IN ({ string.Join(",", fks.Select(x => x.LegalHolderId).ToArray())})").ToList();
               // opponents = _opponentRepository.GetAllByRawString($"SELECT * FROM Opponents WHERE Id IN ({ string.Join(",", fks.Select(x => x.OpponentId).ToArray())})").ToList();
            }
                                   
            Parallel.ForEach(data, x => 
            {
                x.Site = sites.SingleOrDefault(r => r.Id == x.SiteId);
                x.Category = categories.SingleOrDefault(r => r.Id == x.CategoryId);
                x.LegalHolder = legalholders.SingleOrDefault(r => r.Id == x.LegalHolderId);
               // x.Opponent = opponents.SingleOrDefault(r => r.Id == x.OpponentHolderId);
            });

            data.ForEach(x => {
                //currently disabled
                if (x.ValidTo < DateTime.Now && x.Ramavtal && filter.AccountId == 117)
                {
                    var autopostponemonthcount = Convert.ToInt32(x.PostPoneMonths);

                    var remindDateIntervalCount = (x.RemindDate - x.ValidFrom).TotalDays;
                    var cancelDateIntervalCount = (x.CancelLatest - x.ValidFrom).TotalDays;

                    var newValidFrom = x.ValidTo.AddDays(1);
                    var newValidTo = x.ValidTo.AddMonths(autopostponemonthcount);
                    var newRemindDate = newValidFrom.AddDays(remindDateIntervalCount);
                    var newCancelDate = newValidFrom.AddDays(cancelDateIntervalCount);


                    if (newValidTo > newValidFrom)
                    {
                        x.ValidFrom = newValidFrom;
                        x.ValidTo = newValidTo;
                        x.RemindDate = newRemindDate;
                        x.CancelLatest = newCancelDate;

                    }

                }
            });

            //calculate status
            data.ForEach( x =>
            {
                var gww = x.ContractNo;
                var isUpdated = false;
                if (x.RemindDate < DateTime.Now)
                {
                    x.Status = 2;
                    isUpdated = true;
                }

                if (x.ValidTo < DateTime.Now)
                {
                    x.Status = 3;
                    isUpdated = true;
                }

                if (!isUpdated)
                {
                    x.Status = 1;
                }
            });

            var returnData = data.Select(x=> new ContractModel()
            {              
                Status = x.Status,
                UserId = x.UserId,
                Id =x.Id,
                Active = x.Active,
                AmountYearly = x.AmountYearly,
                CancelLatest= x.CancelLatest,
                AmountYearlyDK = x.AmountYearlyDK,
                CategoryId = x.CategoryId,
                ChangedBy = x.ChangedBy,
                ChangedTime = x.ChangedTime,
                ContractNo = x.ContractNo,
                FileDir = x.FileDir,
                InformationText = x.InformationText,
                Note = x.Note,
                LegalHolderId = x.LegalHolderId,
                Ramavtal = x.Ramavtal,                
                PostPoneMonths = x.PostPoneMonths,
                RemindDate = x.RemindDate,
                SiteId = x.SiteId,
                ValidFrom = x.ValidFrom,
                SiteName = x.Site.Name,
                ValidTo = x.ValidTo,
                Category = new ContractCategoryModel()
                {
                    Id = x.Category.Id,
                    CategoryDescription = x.Category.CategoryDescription,
                    GroupId = x.Category.GroupId,
                    CategoryName = x.Category.CategoryName,
                    IsActive = x.Category.IsActive
                },
                LegalHolder = new LegalHolderModel()
                {
                    Id = x.Category.Id,
                    AccountId = x.LegalHolder.AccountId,
                    LegalHolder = x.LegalHolder.LegalHolder
                },
                OpponentHolder = x.OpponentHolder ?? (x.TenantId.HasValue ? _tenantRepository.Get(x.TenantId.Value)?.Name: string.Empty)

            }).ToList();


            if (filter.LegalHolderId != 0)
            {
                returnData = returnData.Where(x => x.LegalHolderId == filter.LegalHolderId).ToList();
            }

            if (filter.CategoryId != 0)
            {
                returnData = returnData.Where(x => x.CategoryId == filter.CategoryId).ToList();
            }
            if(filter.TagWordId != 0)
            {
                var tagWordContractIds = _contractTagWordRepository.GetAll().Where(x => x.TagWordId == filter.TagWordId).Select(x => x.ContractId).ToList();
                returnData = returnData.Where(x => tagWordContractIds.Contains(x.Id)).ToList();
            }
            if (filter.Active.HasValue)
            {
                if (filter.Active.Value == true) {
                    returnData = returnData.Where(x => x.Active == true).ToList();
                }
                else {
                    returnData = returnData.Where(x => x.Active == false).ToList();
                }
              
            }

            if (filter.isRemindList)
            {
                returnData = returnData.Where(x => x.RemindDate <= DateTime.Now).ToList();
            }

            if (!string.IsNullOrEmpty(filter.SearchText))
            {
                returnData = returnData.Where(x => x.ContractNo.ToLower().Contains(filter.SearchText.ToLower()) || x.OpponentHolder.ToLower().Contains(filter.SearchText.ToLower())).ToList();
            }

            return returnData.OrderBy(x=>x.RemindDate);
        }

        public LegalHolderModel AddLegalHolder(LegalHolderModel model)
        {
            if (model.Id == 0)
            {
                var nData = new LegalHolders()
                {
                    LegalHolder = model.LegalHolder,
                    AccountId = model.AccountId,
                    IsActive = model.IsActive
                };
                _legalHoldersRepository.Save(nData);
                model.Id = nData.Id;
            }
            else
            {
                var data = _legalHoldersRepository.Get(model.Id);
                data.AccountId = model.AccountId;
                data.LegalHolder = model.LegalHolder;
                data.IsActive = model.IsActive;
                _legalHoldersRepository.Update(data);
            }

            return model;
        }

        public ContractCategoryModel AddContractCategory(ContractCategoryModel model)
        {
            if (model.Id == 0)
            {
                var nData = new ContractCategory()
                {
                    CategoryDescription = model.CategoryDescription,
                    GroupId = model.GroupId,
                    CategoryName = model.CategoryName,
                    IsActive = model.IsActive
                };
                _contractCategoryRepository.Save(nData);
                model.Id = nData.Id;
            }
            else
            {
                var data = _contractCategoryRepository.Get(model.Id);
                data.CategoryDescription = model.CategoryDescription;
                data.CategoryName = model.CategoryName;
                data.GroupId = model.GroupId;
                data.IsActive = model.IsActive;
                _contractCategoryRepository.Update(data);
            }

            return model;
        }

        public OpponentModel AddOpponent(OpponentModel model)
        {

            if (model.Id == 0)
            {
                var nData = new Opponent()
                {
                    AccountId = model.AccountId,
                    OpponentHolderName = model.OpponentHolderName
                };
                _opponentRepository.Save(nData);
                model.Id = nData.Id;
            }
            else
            {
                var data = _opponentRepository.Get(model.Id);
                data.AccountId = model.AccountId;
                data.OpponentHolderName = model.OpponentHolderName;                

                _opponentRepository.Update(data);
            }

            return model;
        }

        public ContractModel AddContract(ContractModel model)
        {
            if (model.Id == 0)
            {
                var nData = new Contract()
                {
                   Status = model.Status,
                   Active = true,
                   AmountYearly = model.AmountYearly,
                   AmountYearlyDK = model.AmountYearlyDK,
                   CancelLatest = model.CancelLatest,
                   CategoryId = model.CategoryId,
                   ChangedBy = model.ChangedBy,
                   ChangedTime = DateTime.Now,
                   FileDir = model.FileDir,
                   ContractNo = model.ContractNo,
                   InformationText = model.InformationText,
                   LegalHolderId = model.LegalHolderId,
                   Note = model.Note,
                    //OpponentHolderId = model.OpponentHolderId,
                    UserId = model.UserId,
                    OpponentHolder = model.OpponentHolder,
                   PostPoneMonths = model.PostPoneMonths,
                   RemindDate = model.RemindDate,
                   SiteId = model.SiteId,
                   ValidFrom = model.ValidFrom,
                   ValidTo =model.ValidTo,
                   Ramavtal = model.Ramavtal,
                   TenantId = model.TenantId
                };
                _contractRepository.Save(nData);           
                model.Id = nData.Id;

                model.ContractTagWords.Where(x => x.IsSelected).ToList().ForEach(x =>
                {
                    _contractTagWordRepository.Save(new ContractTagWord()
                    {
                        ContractId = model.Id,
                        TagWordId = x.TagWordId
                    });
                });

                _contractLogRepository.Save(new ContractLog()
                {
                    ContractId = nData.Id,
                    Description ="Contract Created",
                    LogDateTime = DateTime.Now,
                    UserId = model.ChangedById
                });
                

            }
            else
            {
                var data = _contractRepository.Get(model.Id);
                data.Ramavtal = model.Ramavtal;
                data.Status = model.Status;
                data.UserId = model.UserId;
                data.Active = model.Active;
                data.AmountYearly = model.AmountYearly;
                data.AmountYearlyDK = model.AmountYearlyDK;
                data.CancelLatest = model.CancelLatest;
                data.CategoryId = model.CategoryId;
                data.ChangedBy = model.ChangedBy;
                data.ChangedTime = DateTime.Now;
                data.FileDir = model.FileDir;
                data.ContractNo = model.ContractNo;
                data.InformationText = model.InformationText;
                data.LegalHolderId = model.LegalHolderId;
                data.Note = model.Note;
               // data.OpponentHolderId = model.OpponentHolderId;
                data.PostPoneMonths = model.PostPoneMonths;
                data.RemindDate = model.RemindDate;
                data.SiteId = model.SiteId;
                data.ValidFrom = model.ValidFrom;
                data.ValidTo = model.ValidTo;
                data.OpponentHolder = model.OpponentHolder;
                data.TenantId = model.TenantId;

                _contractRepository.Update(data);
                var tagsToDelete = _contractTagWordRepository.GetAllByRawString($"SELECT * FROM ContractTagWord WHERE ContractId = {data.Id}").ToList();
                tagsToDelete.ForEach(r => 
                {
                    _contractTagWordRepository.Delete(r.Id);
                });

                model.ContractTagWords.Where(x=>x.IsSelected).ToList().ForEach(x => 
                {
                    _contractTagWordRepository.Save(new ContractTagWord()
                    {
                        ContractId = data.Id,
                        TagWordId = x.TagWordId
                    });
                });

                _contractLogRepository.Save(new ContractLog()
                {
                    ContractId = model.Id,
                    Description = "Contract Updated",
                    LogDateTime = DateTime.Now,
                    UserId = model.ChangedById
                });

            }

            return model;
        }

        public LegalHolderModel GetLegalHolder(int id)
        {
            var model = _legalHoldersRepository.Get(id);
            return new LegalHolderModel()
            {
                AccountId = model.AccountId,                
                LegalHolder = model.LegalHolder,
                Id = model.Id,
                IsActive = model.IsActive
            };
        }

        


        public ContractCategoryModel GetContractCategory(int id)
        {
            var model = _contractCategoryRepository.Get(id);
            return new ContractCategoryModel()
            {
                CategoryDescription = model.CategoryDescription,
                CategoryName = model.CategoryName,
                GroupId = model.GroupId,
                Id = model.Id,
                IsActive = model.IsActive
            };
        }

        public OpponentModel GetOpponent(int id)
        {
            var model = _opponentRepository.Get(id);
            return new OpponentModel()
            {
               AccountId = model.AccountId,
               OpponentHolderName = model.OpponentHolderName,
                Id = model.Id
            };
        }

        public ContractModel GetContract(int id)
        {
            var model = _contractRepository.Get(id);

           // var site = _siteRepository.Get(model.SiteId);
           // var opponent = _opponentRepository.Get(model.OpponentHolderId);
            var category = _contractCategoryRepository.Get(model.CategoryId);
            var legalholder = _legalHoldersRepository.Get(model.LegalHolderId);

            var accountId = _siteRepository.GetAllByRawString($"SELECT * FROM Sites WHERE Id = {model.SiteId}").FirstOrDefault().AccountId;

            var contractTagWords = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE AccountId = {accountId}").ToList();
            var selectedContractTagWords = _contractTagWordRepository.GetAllByRawString($"SELECT * FROM ContractTagWord WHERE ContractId ={model.Id}");
            var list = new List<ContractTagWordModel>();


            contractTagWords.ForEach(x => 
            {
                if (selectedContractTagWords.Any(r => r.TagWordId == x.Id))
                {
                    list.Add(new ContractTagWordModel()
                    {
                        IsSelected = true,
                        ContractId = model.Id,
                        TagWordId = x.Id,
                        TagWord = new TagWordModel()
                        {
                            AccountId = x.AccountId,
                            Id = x.Id,
                            Name = x.Name
                        }
                    });
                }
                else
                {
                    list.Add(new ContractTagWordModel()
                    {
                        IsSelected = false,
                        ContractId = model.Id,
                        TagWordId = x.Id,
                        TagWord = new TagWordModel()
                        {
                            AccountId = x.AccountId,
                            Id =x.Id,
                            Name = x.Name
                        }
                    });
                }
            });


            //currently disabled
            if (model.ValidTo < DateTime.Now && model.Ramavtal && accountId == 117)
            {
                var autopostponemonthcount = Convert.ToInt32(model.PostPoneMonths);

                var remindDateIntervalCount = (model.RemindDate - model.ValidFrom).TotalDays;
                var cancelDateIntervalCount = (model.CancelLatest - model.ValidFrom).TotalDays;

                var newValidFrom = model.ValidTo.AddDays(1);
                var newValidTo = model.ValidTo.AddMonths(autopostponemonthcount);
                var newRemindDate = newValidFrom.AddDays(remindDateIntervalCount);
                var newCancelDate = newValidFrom.AddDays(cancelDateIntervalCount);

                if (newValidTo > newValidFrom)
                {
                    model.ValidFrom = newValidFrom;
                    model.ValidTo = newValidTo;
                    model.RemindDate = newRemindDate;
                    model.CancelLatest = newCancelDate;

                    var m = _contractRepository.Get(model.Id);
                    m.ValidFrom = model.ValidFrom;
                    m.ValidTo = model.ValidTo;
                    m.RemindDate = model.RemindDate;
                    m.CancelLatest = model.CancelLatest;
                    _contractRepository.Update(m);

                    _contractLogRepository.Save(new ContractLog()
                    {
                        ContractId = model.Id,
                        Description = "Valid To Date: Auto postponed",
                        LogDateTime = DateTime.Now,
                        UserId = 0
                    });
                }
            }

            if (model.RemindDate < DateTime.Now)
            {
                model.Status = 2;

                var toUpdate = _contractRepository.Get(model.Id);
                toUpdate.Status = 2;
                _contractRepository.Update(toUpdate);
            }

            if (model.ValidTo < DateTime.Now)
            {
                model.Status = 3;
                var toUpdate = _contractRepository.Get(model.Id);
                toUpdate.Status = 3;
                _contractRepository.Update(toUpdate);
            }

            //if (isUpdated)
            //{
            //    _contractLogRepository.Save(new ContractLog()
            //    {
            //        ContractId = model.Id,
            //        Description = "Contract Status Updated",
            //        LogDateTime = DateTime.Now,
            //        UserId = 0
            //    });
            //}

            var contractLogs = _contractLogRepository.GetAllByRawString($"SELECT * FROM ContractLog WHERE ContractId = {model.Id}");
            var cLogs = new List<ContractLog>();
            if (contractLogs.Any()) {
                cLogs = contractLogs.OrderByDescending(x => x.LogDateTime).ToList();
            }
            string csv = String.Join(",", contractLogs.Where(x=>x.UserId !=0).Select(x => x.UserId).ToArray());
            var users = new List<User>();
            if (!string.IsNullOrEmpty(csv))
            {
                users = _userRepositry.GetAllByRawString($"SELECT * FROM Users WHERE Id IN ({csv})").ToList();

            }

            string contactName = string.Empty;
            if (model.TenantId.HasValue)
            {
                contactName = _tenantRepository.Get(model.TenantId.Value)?.Name;
            }

            return new ContractModel()
            {
                Logs = cLogs.Select(x=> new ContractLogModel()
                {
                    ContractId = x.ContractId,
                    Description = x.Description,
                    Id = x.Id,
                    LogDateTime = x.LogDateTime,
                    UserId = x.UserId,
                    User = x.UserId == 0 || !users.Any()? new UserModel()
                    {
                        LastName = "",
                        FirstName = "System Update"
                    } : users.Where(r=>r.Id == x.UserId).Select(v=>new UserModel()
                    {
                        LastName = v.LastName,
                        FirstName = v.FirstName
                    }).SingleOrDefault(),
                }).ToList(),
                Status = model.Status,
                ContractTagWords = list,
                Active = model.Active,
                AmountYearly = model.AmountYearly,
                CancelLatest = model.CancelLatest,
                AmountYearlyDK = model.AmountYearlyDK,
                UserId = model.UserId,
                CancelLatestModel = new DateModel()
                {
                    day = model.CancelLatest.Day,
                    month = model.CancelLatest.Month,
                    year = model.CancelLatest.Year
                },
                RemindDateModel = new DateModel()
                {
                    day = model.RemindDate.Day,
                    month = model.RemindDate.Month,
                    year = model.RemindDate.Year
                },
                ValidFromModel = new DateModel()
                {
                    day = model.ValidFrom.Day,
                    month = model.ValidFrom.Month,
                    year = model.ValidFrom.Year
                },
                ValidToModel = new DateModel()
                {
                    day = model.ValidTo.Day,
                    month = model.ValidTo.Month,
                    year = model.ValidTo.Year
                },
                ValidTo = model.ValidTo,
                CategoryId = model.CategoryId,
                ChangedBy = model.ChangedBy,
                ChangedTime = model.ChangedTime,
                FileDir = model.FileDir,
                ContractNo = model.ContractNo,
                Id = model.Id,
                InformationText = model.InformationText,
                LegalHolderId = model.LegalHolderId,
                Note = model.Note,
               // OpponentHolderId = model.OpponentHolderId,               
                PostPoneMonths = model.PostPoneMonths,
                SiteId = model.SiteId,
                ValidFrom = model.ValidFrom,
                RemindDate = model.RemindDate,
                Category = new ContractCategoryModel()
                {
                    CategoryDescription = category.CategoryDescription,
                    CategoryName = category.CategoryName,
                    GroupId = category.GroupId,
                    Id = category.Id,
                    IsActive = category.IsActive
                },
                OpponentHolder = model.OpponentHolder ?? contactName,
                Ramavtal = model.Ramavtal,
                LegalHolder = new LegalHolderModel()
                {
                    AccountId = legalholder.AccountId,
                    Id = legalholder.Id,
                    LegalHolder = legalholder.LegalHolder,
                },
                TenantId = model.TenantId,
                Tenant = model.TenantId.HasValue ? _tenantService.Get(model.TenantId.Value) : null,
            };

            //return new OpponentModel()
            //{
            //    AccountId = model.AccountId,
            //    OpponentHolderName = model.OpponentHolderName,
            //    Id = model.Id
            //};
        }

        public bool DeleteLegalHolder(int id)
        {
            if (_contractRepository.GetAllByRawString($"SELECT * FROM Contracts WHERE LegalHolderId = {id}").Any())
            {
                return false;
            }

            _legalHoldersRepository.Delete(id);
            return true;
        }

        public bool DeleteContractCategory(int id)
        {
            if (_contractRepository.GetAllByRawString($"SELECT * FROM Contracts WHERE CategoryId = {id}").Any())
            {
                return false;
            }

            _contractCategoryRepository.Delete(id);
            return true;
        }

        public bool DeleteOpponent(int id)
        {
            if (_contractRepository.GetAllByRawString($"SELECT * FROM Contracts WHERE OpponentHolderId = {id}").Any())
            {
                return false;
            }

            _opponentRepository.Delete(id);
            return true;
        }

        public bool DeleteContract(int id)
        {

            _contractRepository.Delete(id);
            return true;
            //throw new NotImplementedException();
        }

        public IEnumerable<ContractFilesModel> GetFiles(int id)
        {
            var storageConnectionString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];

            if (CloudStorageAccount.TryParse(storageConnectionString, out CloudStorageAccount storageAccount))
            {
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("ser-files");
                container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);

                var azureBlobs = container.ListBlobs(useFlatBlobListing: true);

                foreach (var t in azureBlobs)
                {
                    var blob = t.Container.GetBlockBlobReference("Contracts");
                }

                //var blobs = azureBlobs.Select(x=> new ContractFilesModel()
                //{
                //    Url = x.Uri.AbsoluteUri,

                //});

                //var blob = container.GetBlockBlobReference($"{model.ModuleType}/{model.FileName}.jpg");
                //var bytes = Convert.FromBase64String(model.ByteString);

                //using (var stream = new MemoryStream(bytes))
                //{
                //    blob.UploadFromStream(stream);
                //}

                //return blob.Uri.AbsoluteUri;
            }

            throw new NotImplementedException();
        }

        public byte[] GetXls(int siteId, int accountId = 0,int userId = 0, string searchValue = "")
        {
            using (var excel = new ExcelPackage())
            {
                var se = new CultureInfo("sv-SE");
                var activities = this.GetContracts(new ContractFilters()
                {
                    SiteId = siteId,
                    AccountId = accountId,
                    UserId = userId
                }).ToList();

                var summarySheet = excel.Workbook.Worksheets.Add($"Contracts");
                summarySheet.Column(1).Width = 25;
                summarySheet.Column(2).Width = 20;
                summarySheet.Column(3).Width = 25;
                summarySheet.Column(4).Width = 25;
                summarySheet.Column(5).Width = 25;
                summarySheet.Column(6).Width = 25;
                summarySheet.Column(7).Width = 20;
                summarySheet.Column(8).Width = 20;
                summarySheet.Column(9).Width = 20;
                summarySheet.Column(10).Width = 10;
                summarySheet.Column(11).Width = 10;
                summarySheet.Column(12).Width = 40;
                summarySheet.Column(13).Width = 25;
                summarySheet.Column(14).Width = 25;

                summarySheet.Column(15).Width = 25;
                summarySheet.Column(16).Width = 25;
                summarySheet.Column(17).Width = 12;
                summarySheet.Column(18).Width = 30;

                summarySheet.Cells[1, 1].Value = "Object";
                summarySheet.Cells[1, 1].Style.Font.Bold = true;

                summarySheet.Cells[1, 2].Value = "Status";
                summarySheet.Cells[1, 2].Style.Font.Bold = true;

                summarySheet.Cells[1, 3].Value = "Opponent";
                summarySheet.Cells[1, 3].Style.Font.Bold = true;

                summarySheet.Cells[1, 4].Value = "Contract";
                summarySheet.Cells[1, 4].Style.Font.Bold = true;

                summarySheet.Cells[1, 5].Value = "Category";
                summarySheet.Cells[1, 5].Style.Font.Bold = true;

                summarySheet.Cells[1, 6].Value = "Valid From";
                summarySheet.Cells[1, 6].Style.Font.Bold = true;

                summarySheet.Cells[1, 7].Value = "Valid to";
                summarySheet.Cells[1, 7].Style.Font.Bold = true;

                summarySheet.Cells[1, 8].Value = "Remind Date";
                summarySheet.Cells[1, 8].Style.Font.Bold = true;

                summarySheet.Cells[1, 9].Value = "Cancel Date";
                summarySheet.Cells[1, 9].Style.Font.Bold = true;



                summarySheet.Cells[1, 10].Value = "Framework Agreement";
                summarySheet.Cells[1, 10].Style.Font.Bold = true;


                summarySheet.Cells[1, 11].Value = "Active";
                summarySheet.Cells[1, 11].Style.Font.Bold = true;


                summarySheet.Cells[1, 12].Value = "Note";
                summarySheet.Cells[1, 12].Style.Font.Bold = true;


                summarySheet.Cells[1, 13].Value = "Last Updated By";
                summarySheet.Cells[1, 13].Style.Font.Bold = true;

                summarySheet.Cells[1, 14].Value = "Last Update Time";
                summarySheet.Cells[1, 14].Style.Font.Bold = true;

                //added
                summarySheet.Cells[1, 15].Value = "Legal Holder";
                summarySheet.Cells[1, 15].Style.Font.Bold = true;

                summarySheet.Cells[1, 16].Value = "Contract Manager";
                summarySheet.Cells[1, 16].Style.Font.Bold = true;


                summarySheet.Cells[1, 17].Value = "Auto Postpone";
                summarySheet.Cells[1, 17].Style.Font.Bold = true;

                summarySheet.Cells[1, 18].Value = "Tags";
                summarySheet.Cells[1, 18].Style.Font.Bold = true;

                var activitiesusers = activities.Select(r => r.UserId).ToList();

                var users = new List<User>();
                if (activitiesusers.Any())
                {
                    var csv12 = String.Join(",", activitiesusers.Select(r => r.ToString()).ToArray());

                    users = _userRepositry.GetAllByRawString($"SELECT * FROM Users WHERE Id IN ({csv12})").ToList();

                }

           
                var tags = new List<ContractTagWord>();
                var tagg = new List<TagWord>();
                if (activitiesusers.Any())
                {
                    var csv123 = String.Join(",", activities.Select(r => r.Id).ToArray());

                    tags = _contractTagWordRepository.GetAllByRawString($"SELECT * FROM ContractTagWord WHERE ContractId IN ({csv123})").ToList();

                    var csv4 = String.Join(",", tags.Select(r => r.TagWordId).ToArray());

                    if (!string.IsNullOrEmpty(csv4))
                    {
                        tagg = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE Id IN ({csv4})").ToList();
                    }
                }


                int startRow = 2;
                activities.ForEach(x =>
                {
                    summarySheet.Cells[startRow, 1].Value = x.SiteName;

                    switch (x.Status)
                    {
                        case 1:
                            summarySheet.Cells[startRow, 2].Value = "Ongoing";
                            break;
                        case 2:
                            summarySheet.Cells[startRow, 2].Value = "Passed Remind Date";
                            break;
                        case 3:
                            summarySheet.Cells[startRow, 2].Value = "Passed Valid Date";
                            break;
                        default:
                            summarySheet.Cells[startRow, 2].Value = "";
                            break;
                    }
                   

                    summarySheet.Cells[startRow, 3].Value = x.OpponentHolder;
                    summarySheet.Cells[startRow, 4].Value = x.ContractNo;
                    summarySheet.Cells[startRow, 5].Value = x.Category.CategoryName;
                                                         
                    summarySheet.Cells[startRow, 6].Value = x.ValidFrom.ToString("yyyy-MM-dd");
                    summarySheet.Cells[startRow, 7].Value = x.ValidTo.ToString("yyyy-MM-dd");
                    summarySheet.Cells[startRow, 8].Value = x.RemindDate.ToString("yyyy-MM-dd");
                    summarySheet.Cells[startRow, 9].Value = x.CancelLatest.ToString("yyyy-MM-dd");

                    summarySheet.Cells[startRow, 10].Value = x.Ramavtal? "Yes":"No";

                    summarySheet.Cells[startRow, 11].Value = x.Active? "Active":"Inactive";


                    summarySheet.Cells[startRow, 12].Value = x.Note;
                    summarySheet.Cells[startRow, 13].Value = x.ChangedBy;
                    summarySheet.Cells[startRow, 14].Value = x.ChangedTime.ToString("yyyy-MM-dd HH:mm");

                    summarySheet.Cells[startRow, 15].Value = x.LegalHolder.LegalHolder;

                    var user = users.SingleOrDefault(r => r.Id == x.UserId);

                    if (user != null)
                    {
                        summarySheet.Cells[startRow, 16].Value = $"{users.SingleOrDefault(r => r.Id == x.UserId).FirstName} {users.SingleOrDefault(r => r.Id == x.UserId).LastName}";
                    }

                    summarySheet.Cells[startRow, 17].Value = x.PostPoneMonths;

                    if (tags.Where(r=>r.ContractId == x.Id).Any())
                    {
                        tags.ForEach(r =>
                        {
                            r.TagWord = tagg.SingleOrDefault(v => v.Id == r.TagWordId);
                        });

                        var t = tags.Where(r => r.ContractId == x.Id);
                     
                        string csv = String.Join(",", t.Select(r => r.TagWord.Name).ToArray());

                        summarySheet.Cells[startRow, 18].Value = csv;
                    }
              
                    startRow++;
                });

                return excel.GetAsByteArray();
            }
        }

        public IEnumerable<TagWordModel> GetTagWords(int accountId)
        {
            var result = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE AccountId = {accountId}");

            return result.Select(x => new TagWordModel()
            {
                AccountId = x.AccountId,
                Id = x.Id,
                Name = x.Name
            });
        }

        public void AddTagWord(TagWordModel model)
        {
            if (model.Id != 0)
            {

                var m = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE Id = {model.Id}").SingleOrDefault();
                m.Name = model.Name;

                _tagWordRepository.Update(m);
            }
            else
            {
                _tagWordRepository.Save(new TagWord()
                {
                    AccountId = model.AccountId,
                    Name = model.Name
                });


            }

          
        }

        public TagWordModel GetTagWordById(int id)
        {
            var m = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE Id = {id}").SingleOrDefault();
            return new TagWordModel()
            {
                AccountId = m.AccountId,
                Id = m.Id,
                Name = m.Name
            };
        }

        public bool DeleteTagWord(int id)
        {

            if (_contractTagWordRepository.GetAllByRawString($"SELECT * FROM ContractTagWord WHERE TagWordId = {id}").Any())
            {
                return false;
            }
            //if (_tag.GetAllByRawString($"SELECT * FROM Contracts WHERE OpponentHolderId = {id}").Any())
            //{
            //    return false;
            //}

            _tagWordRepository.Delete(id);
            return true;
        }

        public IEnumerable<ContractTagWordModel> GetContractTagWords(int accoundId)
        {
            var words = _tagWordRepository.GetAllByRawString($"SELECT * FROM TagWords WHERE AccountId = {accoundId}").ToList();

            return words.Select(t => new ContractTagWordModel()
            {
                IsSelected = false,
                TagWordId = t.Id,
                TagWord = new TagWordModel()
                {
                    Name = t.Name,
                    Id = t.Id,
                    AccountId = t.AccountId
                }
            });
        }

        public IEnumerable<ContractRoleModel> GetContractRoles(int accountId)
        {
            var d = _contractRoleRepository.GetAllByRawString($"SELECT * FROM ContractRole WHERE AccountId = {accountId}").ToList();
            return d.Select(x => new ContractRoleModel()
            {
                Id = x.Id,
                RoleName = x.RoleName,
                AccountId = x.AccountId
            });
        }

        public int AddContractRole(ContractRoleModel model)
        {
            var d = 0;
            if (model.Id == 0)
            {
                var t = _contractRoleRepository.Save(new ContractRole()
                {
                    AccountId = model.AccountId,
                    RoleName = model.RoleName
                });
                d = t.Id;
            }
            else
            {
                var data = _contractRoleRepository.Get(model.Id);
                data.RoleName = model.RoleName;
                data.AccountId = model.AccountId;

                _contractRoleRepository.Update(data);
                d = model.Id;
            }

            return d;
        }

        public bool DeleteContractRole(int Id)
        {
            if (_contractRepository.GetAllByRawString($"SELECT * FROM Contracts WHERE UserId = {Id}").Any())
            {
                return false;
            }

            _contractRoleRepository.Delete(Id);
            return true;
        }

        public ContractRoleModel UpdateContractRole(ContractRoleModel model)
        {
            var data = _contractRoleRepository.Get(model.Id);
            data.RoleName = model.RoleName;
            data.AccountId = model.AccountId;

            return model;

        }

        public ContractRoleModel GetContractRoleById(int id)
        {
            var data = _contractRoleRepository.Get(id);
          
            return new ContractRoleModel()
            {
                AccountId = data.AccountId,
                RoleName = data.RoleName,
                Id = data.Id
            };
        }

        public bool SendFiles(EmailModel model)
        {
            return Service.Tools.Email.SendEmail(model);
        }

        public bool CopyContracts(CopyContractModel model)
        {
            foreach (var id in model.ContractIds)
            {
                var contract = _contractRepository.Get(id);
                var contractModel = new ContractModel
                {
                    LegalHolderId = contract.LegalHolderId,
                    CategoryId = contract.CategoryId,
                    Active = contract.Active,
                    CancelLatest = contract.CancelLatest,
                    ChangedBy = contract.ChangedBy,
                    ContractNo = contract.ContractNo,
                    Note = contract.Note,
                    OpponentHolder = contract.OpponentHolder,
                    PostPoneMonths = contract.PostPoneMonths,
                    Ramavtal = contract.Ramavtal,
                    RemindDate = contract.RemindDate,
                    SiteId = model.SiteId,
                    UserId = contract.UserId,
                    ValidFrom = contract.ValidFrom,
                    ValidTo = contract.ValidTo
                };
                var tagWords = _contractTagWordRepository.GetAllByRawString($"SELECT * FROM ContractTagWord WHERE ContractId = {id}").Select(x => new ContractTagWordModel
                {
                    Id = x.Id,
                    ContractId = x.ContractId,
                    TagWordId = x.TagWordId,
                    IsSelected = true
                }).ToList();

                contractModel.ContractTagWords = tagWords;
                AddContract(contractModel);
            }
            return true;
        }

        public byte[] DownloadFiles(ContractFilters filters)
        {
            var contracts = GetContracts(filters);
            var siteName = _siteRepository.Get(filters.SiteId)?.Name ?? "";
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string env = ConfigurationManager.AppSettings["server"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{filters.AccountId}");
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            if (env.ToLower() != "development")
            {
                fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, ".."));
            }

            fileManagerRoot = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT", $"{filters.SiteId}");

            var name = new DirectoryInfo(fileManagerRoot).Name;
            var rootFolder = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", siteName));

            if (!Directory.Exists(rootFolder))
            {
                Directory.CreateDirectory(rootFolder);
            }


            foreach (var contract in contracts)
            {
                var contractFolder = Path.Combine(rootFolder, $"{contract.ContractNo} - {contract.OpponentHolder}");
                if (!Directory.Exists(contractFolder))
                {
                    Directory.CreateDirectory(contractFolder);
                }
                var filemanagerContract = Path.Combine(fileManagerRoot, $"{contract.Id}");
                if (Directory.Exists(filemanagerContract))
                {
                    CopyDirectory(filemanagerContract, contractFolder, true);
                }
            }

            var zipPath = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", $"{name}.zip"));

            ZipFile.CreateFromDirectory(rootFolder, zipPath);

            var bytes = File.ReadAllBytes(zipPath);

            if (!string.IsNullOrEmpty(zipPath))
                File.Delete(zipPath);
            if (!string.IsNullOrEmpty(rootFolder))
                Directory.Delete(rootFolder, true);

            return bytes;
        }

        static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
        {
            // Get information about the source directory
            var dir = new DirectoryInfo(sourceDir);

            // Check if the source directory exists
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            // Cache directories before we start copying
            DirectoryInfo[] dirs = dir.GetDirectories();

            // Create the destination directory
            Directory.CreateDirectory(destinationDir);

            // Get the files in the source directory and copy to the destination directory
            foreach (FileInfo file in dir.GetFiles())
            {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            }

            // If recursive and copying subdirectories, recursively call this method
            if (recursive)
            {
                foreach (DirectoryInfo subDir in dirs)
                {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyDirectory(subDir.FullName, newDestinationDir, true);
                }
            }
        }

        public List<ContractModel> GetContractsByIds(string ids)
        {
            return _icontractRepository.GetContractsByIds(ids);
        }

        public List<ContractModel> GetAllContractsForExport(ContractFilters filters)
        {
            return _icontractRepository.GetAllContractsForExport(filters);
        }

        public bool BulkUpdateYear(UpdateContractYear model)
        {
            var ids = string.Join(",",model.ContractIds);
            var query = $@"
                UPDATE Contracts
                SET ValidTo = dateadd(year, ({model.Year} - year(ValidTo)), ValidTo),
                    RemindDate = dateadd(year, ({model.Year} - year(RemindDate)), RemindDate),
                    CancelLatest = dateadd(year, ({model.Year} - year(CancelLatest)), CancelLatest)
                WHERE Id in ({ids})
            ";
            return _contractRepository.ExecuteRawSql(query) > 0;
        }
        
        public void SaveEmailRecipient(SaveEmailRecipient model)
        {
            var moduleId = _moduleRepository
                .GetByPredicate(e => e.ShortName.ToLower() == "contract")
                .Select(e => e.Id)
                .FirstOrDefault();
            var data = _emailRecipientRepository
                .GetByPredicate(e => e.ModuleId == moduleId && e.Recipient == model.Recipient 
                            && e.AccountId == model.AccountId).FirstOrDefault();

            if (data != null)
            {
                data.SentDate = DateTime.Now;
                _emailRecipientRepository.Update(data);
            }
            else
            {
                _emailRecipientRepository.Save(new EmailRecipient
                {
                    AccountId = model.AccountId,
                    ModuleId = moduleId,
                    Recipient = model.Recipient,
                    SentDate = DateTime.Now
                });
            }
        }
    }
}
