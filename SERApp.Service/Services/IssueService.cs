﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueService
    {
        bool SaveIssue(IssueModel model);
    }
    public class IssueService : IIssueService
    {
        private readonly IRepository<Issue> _repository;
        private readonly AccountService _accountService;
        public IssueService()
        {
            _repository = new Repository<Issue>();
            _accountService = new AccountService();
        }

        public bool SaveIssue(IssueModel model)
        {

            try
            {
                var result = _repository.Save(new Issue()
                {
                    Object = model.Object,
                    Tenant = model.Tenant,
                    Yta=model.Yta,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    Email = model.Email,
                    ApartmentNumber = model.ApartmentNumber,
                    Typ = model.Typ,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice
                });

                var emailTo =_accountService.Get(model.AccountId);
                if (emailTo != null)
                {
                    if (!string.IsNullOrEmpty(emailTo.EmailAddress))
                    {

                        result.Description = result.Description == null ? "" : result.Description;
                        result.Image = result.Image.Contains("upload-empty.png") ? "" : result.Image;
                        Service.Tools.Email.SendEmailWithAttachment("", emailTo.EmailAddress, "Issue #" + result.IssueId,
                            "Hi Admin, <br/><br/> A new issue has been created.<br/><br/>" +
                            "Here are the details:<br/><br/>" +

                            $"Object: {result.Object}<br/>" +
                            $"Tenant: {result.Tenant}<br/>" +
                            $"Yta: {result.Yta}<br/>" +
                            $"Phone: {result.Phone}<br/>" +
                            $"ContactName: {result.ContactName}<br/>" +
                            $"Address: {result.Address}<br/>" +
                            $"Email: {result.Email}<br/>" +
                            $"ApartmentNumber: {result.ApartmentNumber}<br/>" +
                            $"Typ: {result.Typ}<br/>" +
                            $"Description: {result.Description}<br/>" +
                            $"IsPolice: {result.IsPolice}<br/>" +                              
                            $"and an image is also attached to this email.<br/><br/>" +
                            "Regards,<br/>SER4 Admin",
                            "",result.Image,null,true);
                    }
                }

                return true;
            } catch (Exception ex)
            {
                return false;
            }
          
        }
    }
}
