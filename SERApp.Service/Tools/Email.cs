﻿using SendGrid;
using SendGrid.Helpers.Mail;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Extensions;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Tools
{
    public class Email
    {
        LogRepository _logRepository;
        EmailService _emailService;
        static IRepository<Account> _accountRepository;
        IRepository<EmailRecipient> _emailRecipientRepository;
        public Email() {
            _logRepository = new LogRepository();
            _emailService = new EmailService();
            _accountRepository = new Repository<Account>();
            _emailRecipientRepository = new Repository<EmailRecipient>();
        }
        public static string AppUrl = ConfigurationManager.AppSettings["appurl"];
        public void SendLoanConfirmation() { }
        public void SendCreateAccountConfiramation() { }
        public static bool IsValidEmailAddress(string email) {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public AccountEmailTemplateModel getAccountEamilTemplate(string routineName, int accountId) {
            return _emailService.GetAccountEmailTemplate(routineName, accountId);
        }

        public static AccountEmailTemplateModel GetAccountEamilTemplate(string routineName, int accountId)
        {
            Email emailService = new Email();
            return emailService.getAccountEamilTemplate(routineName, accountId);
        }

        public void LogException(Exception ex, string actionMessage, int accountId, int userId) {
            _logRepository.LogException(ex, actionMessage, accountId, userId);
        }

        public void Log(LogTypeEnum logType, string actionMessage, string message, int accountId, int userId)
        {
            _logRepository.Log(logType, actionMessage, message, accountId, userId);
        }

        public static void SendCreateUserConfirmation(UserModel model) {
            var emailTemplate = GetAccountEamilTemplate(model.RoleName == "admin" ? "AccountCreate" : "AccountUserCreate", 0);
            SendCreateUserConfirmation(model, emailTemplate);
        }

        public static void SendCreateUserConfirmation(UserModel model, AccountEmailTemplateModel emailTemplate) {
            string appUrl = ConfigurationManager.AppSettings["appurl"];
            string subject = string.Empty;
            string body = string.Empty;


            if (emailTemplate != null) {
                subject = emailTemplate.Subject;
                body = emailTemplate.Body;
            }
            else {
                if (model.RoleName == "admin")
                {
                    subject = MailMessagingSettings.AccountAdminCreateEmailSubject;
                    body = MailMessagingSettings.AccountAdminCreateEmailBody;
                }
                else
                {
                    subject = MailMessagingSettings.AccountUserCreateEmailSubject;
                    body = MailMessagingSettings.AccountUserCreateEmailBody;
                }
            }
           

            body = body.Replace("{accountName}", model.Account.Name);
            body = body.Replace("{userName}", model.UserName);
            body = body.Replace("{password}", model.Password);
            body = body.Replace("{link}", appUrl + "?login=" + Security.encrypt64baseUserPass(model.UserName,model.Password) + "");

            SendEmail(new EmailModel(model.AccountId)
            {
                From = string.Empty,
                To = model.EmailAddress,
                Subject = subject,
                Body = body,
                BCC = string.Empty,
                AccountId = model.AccountId,
                UserId = model.Id
            });

        }

        public static void SendLoanEmail(LoanModel model, string loanRoutine) {
            string appUrl = ConfigurationManager.AppSettings["appurl"];
            string subject = string.Empty;
            string body = string.Empty;

            var emailTemplate = GetAccountEamilTemplate(loanRoutine, model.Site.AccountId);

            subject = emailTemplate.Subject;
            body = emailTemplate.Body;

            body = body.Replace("{accountName}", model.Site.Account.Name);
            body = body.Replace("{siteName}", model.Site.Name);

            //if (loanRoutine == EmailTemplates.LoanCreate)
            //{
            //    body = body.Replace("{link}", appUrl + "confirm/loan?code=" + Security.encrypt64baseLoanEmail(model.Customer.Email) + ":" + model.Id);
            //}
            body = body.Replace("{link}", appUrl + "confirm/loan?code=" + Security.encrypt64baseLoanEmail(model.Customer.Email) + ":" + model.Id);
            body = body.Replace("{customerName}", model.Customer.Name);
            
            body = body.Replace("{loanId}", model.LoanId);
            body = body.Replace("{loanItemNameId}", model.ItemNameId);
            body = body.Replace("{loanDescription}", model.Description);
            body = body.Replace("{loanGivenBy}", model.GivenBy);
            body = body.Replace("{loanReceivedBy}", model.ReceivedBy);
            body = body.Replace("{loanedDate}", model.LoanedDate.ToShortDateString());
            body = body.Replace("{confirmedDate}", model.ConfirmedDate.HasValue && model.ConfirmedDate != DateTime.MinValue ? model.ConfirmedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{loanConfirmedDate}", model.ConfirmedDate.HasValue && model.ConfirmedDate != DateTime.MinValue ? model.ConfirmedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{returnedDate}", model.ReturnedDate.HasValue && model.ReturnedDate != DateTime.MinValue ? model.ReturnedDate.Value.ToShortDateString() : "N / A");
            body = body.Replace("{actualReturnedDate}", model.ActualReturnedDate.HasValue && model.ActualReturnedDate != DateTime.MinValue ? model.ActualReturnedDate.Value.ToShortDateString() : "N / A");
            
            
            SendEmail(new EmailModel(model.Site.AccountId)
            {
                From = string.Empty,
                To =model.CustomerTypeId == 1?  model.Customer.Email : model.Email,
                Subject = subject,
                Body = body,
                BCC = string.Empty,
                AccountId = model.Site.AccountId,
                UserId = 0
            });
        }

        public static void SendCreateLoandConfirmation(LoanModel model) {
            SendLoanEmail(model, EmailTemplates.LoanCreate);
        }

        public static void SendLoanUpdateNotification(LoanModel model)
        {
            SendLoanEmail(model, EmailTemplates.LoanUpdate);
        }

        public static void SendLoanNotifcation(LoanModel model)
        {
            SendLoanEmail(model, EmailTemplates.LoanNotify);
        }

        public static void SendLoanPassedDateNotification(LoanModel model)
        {
            SendLoanEmail(model, EmailTemplates.LoanPassedDate);
        }

        public static void SendLoanConfirm(LoanModel model) {
            SendLoanEmail(model, EmailTemplates.LoanConfirm);
        }      

        public static void SendLoanReturn(LoanModel model)
        {
            SendLoanEmail(model, EmailTemplates.LoanReturn);
        }

        public static bool SendEmail(EmailModel emailModel)
        {
            if (string.IsNullOrEmpty(emailModel.Name)) 
            {
                if (emailModel.AccountId != 0) 
                {

                    _accountRepository = new Repository<Account>();
                    emailModel.Name = _accountRepository.Get(emailModel.AccountId).Company;
                }
            }

            return SendEmail(emailModel.From, emailModel.To, emailModel.Subject, emailModel.Body, emailModel.BCC, emailModel, overrideName:emailModel.Name, attachmentsPath: emailModel.AttachmentPaths);
        }

        public static bool SendEmail(string from, string to, string subject, string body, string BCC, EmailModel model = null, string replyTo ="", string overrideName = "", List<string> attachmentsPath = null) 
        {
            
            var ress = false;
            if (string.IsNullOrEmpty(to))
            {
                return false;
            }

            from = string.IsNullOrEmpty(from) ? ConfigurationManager.AppSettings["from"] : from;
            MailMessage message = new MailMessage(from, to, subject, body);

            if (BCC != null)
            {
                foreach (var BCCs in BCC.Split(';'))
                {
                    if (BCCs != "")
                    {
                        message.Bcc.Add(BCCs);
                    }
                }
            }
            if (!string.IsNullOrEmpty(replyTo))
            {
                message.ReplyToList.Add(replyTo);
                //message.ReplyTo = new MailAddress(replyTo);
            }
            body = body == null ? "" : body;
            if (!body.Contains("<html>"))
            {
                body = $"<html>{body}<html>";
            }

            message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, new ContentType("text/html")));
            message.IsBodyHtml = true;
            try
            {
                var useEmailAppSettings = ConfigurationManager.AppSettings["useEmailAppSettings"];
                var client = new System.Net.Mail.SmtpClient();
                //SET THIS TO FALSE ON AppConfig to use the SMTP Server
                if (bool.Parse(useEmailAppSettings))
                {
                    string smtpClient = ConfigurationManager.AppSettings["mailserver"];
                    string userName = ConfigurationManager.AppSettings["username"];
                    string password = ConfigurationManager.AppSettings["password"];
                    string fromName = ConfigurationManager.AppSettings["fromname"];

                    if (!string.IsNullOrEmpty(fromName))
                    {
                        message.From = new MailAddress(from, fromName);
                    }

                    client = new System.Net.Mail.SmtpClient(smtpClient);
                    if (smtpClient == "smtp.gmail.com")
                    {
                        client.Port = 587;
                    }
                    client.EnableSsl = true;
                    client.Credentials = new System.Net.NetworkCredential(userName, password);
                }

                var result = Execute("", subject, new List<string>() { to }, body,fromOverride: overrideName, attachmentPaths: attachmentsPath);
                //client.Send(message);
                ress = result;
            }
            catch (Exception ex)
            {
                Email email = new Email();
                email.LogException(ex, LogMessagingSettings.ActionSendEmail, model.AccountId, model.UserId);
                return false;
            }
            finally
            {
                Email email = new Email();
                //log user email audit trail
                if (model != null)
                {
                    email.Log(LogTypeEnum.Information,
                                     LogMessagingSettings.ActionSendEmailAudit,
                                     body,
                                     model.AccountId,
                                     model.UserId);
                }
             
            }

            return ress;
        }

        public static void SendEmailWithAttachment(string from, string to, string subject, string body, string BCC, string base64File, EmailModel model = null, bool hasInlineAttachment = false, List<string> CC = null)
        {
            if (!string.IsNullOrEmpty(base64File))
            {
                var data = base64File.Split(',');
                var file = Convert.FromBase64String(data[1]);
                var stream = new MemoryStream(file);

                SendEmailWithAttachment("",from, to, subject, body, BCC, stream, string.Empty, model, hasInlineAttachment, CC);

                stream.Close();

                return;
            }

            
        }

        public static void SendEmailWithAttachment(string path,string from, string to, string subject, string body, string BCC, Stream file, string fileName = "", EmailModel model = null,bool hasInlineAttachment = false, List<string> CC =null)
        {
            from = string.IsNullOrEmpty(from) ? ConfigurationManager.AppSettings["from"] : from;
            MailMessage message = new MailMessage(from, to, subject, body);

            if (CC != null)
            {
                if (CC.Any())
                {
                    CC.ForEach(r =>
                    {
                        if (!string.IsNullOrEmpty(r))
                        {
                            message.CC.Add(r);
                        }
                    });
                }
            }

            if (BCC != null)
            {
                foreach (var BCCs in BCC.Split(';'))
                {
                    if (BCCs != "")
                    {
                        message.Bcc.Add(BCCs);
                    }
                }
            }

            if (file != null)
            {
                message.Attachments.Add(new System.Net.Mail.Attachment(file, fileName, MediaTypeNames.Application.Pdf));
            }

            //if (!string.IsNullOrEmpty(base64File))
            //{


            //    var data = base64File.Split(',');
            //    var contentType = data[0].Replace("data:", "").Replace(";base64", "");
            //    var file = Convert.FromBase64String(data[1]);
            //    Stream stream = new MemoryStream(file);
            //    ContentType ct;

            //    if (hasInlineAttachment)
            //    {
            //        ct = new ContentType(MediaTypeNames.Image.Jpeg);
            //        message.Attachments.Add(new Attachment(stream, ct));
            //        message.Attachments[0].ContentId = "image-attachment";
            //    }
            //    else
            //    {
            //        ct = new ContentType(MediaTypeNames.Application.Pdf);
            //        message.Attachments.Add(new Attachment(stream, ct));
            //    }
            //}
           
            message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, new ContentType("text/html")));
            message.IsBodyHtml = true;
            try
            {
                var useEmailAppSettings = ConfigurationManager.AppSettings["useEmailAppSettings"];
                var client = new System.Net.Mail.SmtpClient();
                //SET THIS TO FALSE ON AppConfig to use the SMTP Server
                if (bool.Parse(useEmailAppSettings))
                {
                    string smtpClient = ConfigurationManager.AppSettings["mailserver"];
                    string userName = ConfigurationManager.AppSettings["username"];
                    string password = ConfigurationManager.AppSettings["password"];
                    string fromName = ConfigurationManager.AppSettings["fromname"];

                    if (!string.IsNullOrEmpty(fromName))
                    {
                        message.From = new MailAddress(from, fromName);
                    }

                    client = new System.Net.Mail.SmtpClient(smtpClient);
                    if (smtpClient == "smtp.gmail.com")
                    {
                        client.Port = 587;
                    }

                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;
                    client.Credentials = new System.Net.NetworkCredential(userName, password);
                }
                
                Execute(path,subject, new List<string>() { to },body,file,fileName);
                //client.Send(message);
            }
            catch (Exception ex)
            {
                Email email = new Email();
                email.LogException(ex, LogMessagingSettings.ActionSendEmailReport, model.AccountId, model.UserId);
            }
            finally
            {
                //Email email = new Email();
                ////log user email audit trail
                //email.Log(LogTypeEnum.Information,
                //    LogMessagingSettings.ActionSendEmailAudit,
                //    body,
                //    model.AccountId,
                //    model.UserId);
            }
        }

        public static void SendEmailWithAttachmentToMultipleEmails(string path,string from, List<string> to, string subject, string body, string BCC, Stream file, string fileName = "", EmailModel model = null, bool hasInlineAttachment = false, List<string> CC = null, string fromname = "", List<string> attachments = null)
        {
            from = string.IsNullOrEmpty(from) ? ConfigurationManager.AppSettings["from"] : from;
            MailMessage message = new MailMessage();
            //from, to.First(), subject, body
            message.From = new MailAddress(from);
            message.Subject = subject;
            message.Body = body;

            if (to.Any())
            {
                to.ForEach(x => 
                {
                    message.To.Add(new MailAddress(x));
                });
            }

            if (CC != null)
            {
                if (CC.Any())
                {
                    CC.ForEach(r =>
                    {
                        if (!string.IsNullOrEmpty(r))
                        {
                            message.CC.Add(r);
                        }
                    });
                }
            }

            if (BCC != null)
            {
                foreach (var BCCs in BCC.Split(';'))
                {
                    if (BCCs != "")
                    {
                        message.Bcc.Add(BCCs);
                    }
                }
            }

            if (file != null)
            {
                message.Attachments.Add(new System.Net.Mail.Attachment(file, fileName, MediaTypeNames.Application.Pdf));
            }

          
            message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, new ContentType("text/html")));
            message.IsBodyHtml = true;
            try
            {
                var useEmailAppSettings = ConfigurationManager.AppSettings["useEmailAppSettings"];
                var client = new System.Net.Mail.SmtpClient();
                //SET THIS TO FALSE ON AppConfig to use the SMTP Server
                if (bool.Parse(useEmailAppSettings))
                {
                    string smtpClient = ConfigurationManager.AppSettings["mailserver"];
                    string userName = ConfigurationManager.AppSettings["username"];
                    string password = ConfigurationManager.AppSettings["password"];
                    string fromName = !string.IsNullOrEmpty(fromname)? fromname: ConfigurationManager.AppSettings["fromname"];

                    if (!string.IsNullOrEmpty(fromName))
                    {
                        message.From = new MailAddress(from, fromName);
                    }

                    client = new System.Net.Mail.SmtpClient(smtpClient);
                    if (smtpClient == "smtp.gmail.com")
                    {
                        client.Port = 587;
                    }

                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;
                    client.Credentials = new System.Net.NetworkCredential(userName, password);
                }

                if (to.Any())
                {
                    // client.Send(message);
                    Execute(path, subject, to, body, file, fileName, images: attachments);
                    
                }               
            }
            catch (Exception ex)
            {
                Email email = new Email();
                email.LogException(ex, LogMessagingSettings.ActionSendEmailReport, model.AccountId, model.UserId);
            }
            finally
            {
                //Email email = new Email();
                ////log user email audit trail
                //email.Log(LogTypeEnum.Information,
                //    LogMessagingSettings.ActionSendEmailAudit,
                //    body,
                //    model.AccountId,
                //    model.UserId);
            }
        }


        public static bool Execute(SendFilesRequest request)
        {
            var apiKey = ConfigurationManager.AppSettings["sendgridkey"];
            var client = new SendGridClient(apiKey);

            var from = new EmailAddress(request.From);
            //var subject = "Testing";

            var listOfRecipients = new List<EmailAddress>();

            List<string> tos = new List<string>();
            tos.Add(request.To);

            tos.ForEach(x =>
            {
                listOfRecipients.Add(new EmailAddress(x));
            });

            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, listOfRecipients, request.Subject, request.Message, request.Message);

            request.Files.ForEach(x =>
            {
                msg.AddAttachment(x.Name, x.Base64);
                System.Threading.Thread.Sleep(1000);
            });

            var response = client.SendEmailAsync(msg).Result;
            
            return true;
        }

        public static bool Execute(string path,string subject, List<string> tos, string content, Stream file = null, string filename = "", string fromOverride = "", List<string> attachmentPaths = null, List<string> images = null)
        {
            //content = $"{content} <br><br> <small>Skickat via SER4 en tjänst från</small>";

            var apiKey = ConfigurationManager.AppSettings["sendgridkey"];
            var client = new SendGridClient(apiKey);

            var from = new EmailAddress("s4@serapp.com", string.IsNullOrEmpty(fromOverride)? "SER4": fromOverride);
            //var subject = "Testing";

            var listOfRecipients = new List<EmailAddress>();

            tos.ForEach(x => 
            {
                listOfRecipients.Add(new EmailAddress(x));
            });

            if (!string.IsNullOrEmpty(path))
            {
                using (var ff = System.IO.File.OpenRead(path))
                {

                    var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, listOfRecipients, subject, content, content);

                    if (file != null)
                    {
                        var test = ReadFully(file);
                        //msg.AddAttachment(filename, $"data:application/pdf;{ReadFully(file)}", "application/pdf","attachment");
                        //msg.AddAttachment(new SendGrid.Helpers.Mail.Attachment()
                        //{
                        //    Content = ReadFully(file),
                        //    Disposition = "attachment",
                        //    Filename = filename,
                        //    Type = "application/pdf",                    
                        //});

                        msg.AddAttachment(filename, ff.ConvertToBase64(), "application/pdf", "attachment");
                        if (images != null)
                        {
                            var ctr = 1;
                            images.ForEach(x =>
                            {
                                msg.AddAttachment($"report_image_{ctr}.png", x.Replace("data:image/png;base64,", "").Replace("data:image/jpeg;base64,", ""));
                                ctr++;
                            });
                        }

                        if (msg.Attachments == null)
                        {
                            msg.AddAttachment(filename, test);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(path) && ff != null)
                        {
                            var bytes = System.IO.File.ReadAllBytes(path);
                            var filea = Convert.ToBase64String(bytes);

                            msg.AddAttachment(filename, filea);
                            System.Threading.Thread.Sleep(2000);
                        }
                    }

        
                    var response = client.SendEmailAsync(msg).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                    {
                        return true;
                    }

                    var body = response.Body;
                    return false;
                }
            }
            else if (attachmentPaths != null)
            {
                var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, listOfRecipients, subject, content, content);

                attachmentPaths.ForEach(a =>
                {
                    var attr = System.IO.File.GetAttributes(a);
                    var zipPath = "";
                    var bytes = new byte[0];

                    var name = new DirectoryInfo(a).Name;
                    if (attr.HasFlag(FileAttributes.Directory))
                    { 
                        string startPath = a;
                        zipPath = Path.GetFullPath(Path.Combine(a, "..", $"{name}.zip"));


                        ZipFile.CreateFromDirectory(startPath, zipPath);

                        bytes = System.IO.File.ReadAllBytes(zipPath);
                        String base64 = Convert.ToBase64String(bytes);

                        msg.AddAttachment(Path.GetFileName(zipPath), base64);
                    }
                    else
                    {
                        bytes = System.IO.File.ReadAllBytes(a);
                        String base64 = Convert.ToBase64String(bytes);

                        msg.AddAttachment(Path.GetFileName(a), base64);
                    }
                    if (!string.IsNullOrEmpty(zipPath))
                        System.IO.File.Delete(zipPath);
                });

                var response = client.SendEmailAsync(msg).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, listOfRecipients, subject, content, content);
                
                var response = client.SendEmailAsync(msg).Result;
            }
                    
            return true;
        }

        public static String ReadFully(Stream input)
        {
            using (var memoryStream = new MemoryStream())
            {
                input.CopyTo(memoryStream);
                return Convert.ToBase64String(memoryStream.ToArray());
            }
            //byte[] buffer = new byte[16 * 1024];
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    int read;
            //    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            //    {
            //        ms.Write(buffer, 0, read);
            //    }

            //    return Convert.ToBase64String(ms.ToArray());
            //}
        }
    }
}
