﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace SERApp.Service.Tools
{
    public class File
    {
        public static string fileDirectory = @"\" + ConfigurationManager.AppSettings["filesDirectory"];
        public static string fileDirectoryServer = ConfigurationManager.AppSettings["fileDirectoryServer"];
        /// <summary>
        /// Handles the actual directories before saving file. Also creates the folder if it's missing
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string FileDirectoryManager(FileModel model)
        {
            string path = string.Empty;
            string rootPath = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;
            string relativePath = rootPath + fileDirectory + @"\" + model.ModuleType + @"\";
            if (Directory.Exists(relativePath))
            {
                Directory.CreateDirectory(relativePath);
            }
            string fileName = model.FileName + ".jpg";
            model.FileName = fileName;
            string imgPath = Path.Combine(relativePath, fileName);
            return imgPath;
        }
        /// <summary>
        /// ONLY ACCEPTS SINGLE FILE FOR NOW
        /// ACCEPTS HTTP POSTED FILE
        /// </summary>
        /// <param name="http"></param>
        /// <returns></returns>
        public static string SaveFile(System.Web.HttpContext http) {
            var httpRequest = http.Request;
            string fileName = string.Empty;
            if (httpRequest.Files.Count > 0)
            {
                int i = 0;
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[i];
                    var filePath = HttpContext.Current.Server.MapPath(fileDirectory + postedFile.FileName);
                    try
                    {
                        postedFile.SaveAs(filePath);
                        fileName = filePath;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    i++;
                }
            }
            return fileName;
        }
        /// <summary>
        /// ONLY ACCEPTS SINGLE FILE FOR NOW
        /// ACCEPTS STRING BASED
        /// </summary>
        /// <param name="imageString"></param>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public static string SaveFile(string imageString, string imagePath)
        {
            byte[] imageBytes = Convert.FromBase64String(imageString);
            System.IO.File.WriteAllBytes(imagePath, imageBytes);
            return imagePath;
        }

        public static bool DeleteFile(string fileName)
        {
            var storageConnectionString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];
            if (CloudStorageAccount.TryParse(storageConnectionString, out CloudStorageAccount storageAccount))
            {
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("ser-files");
                container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);
                var blob = container.GetBlockBlobReference(fileName);
                blob.DeleteIfExists();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Handles the File Management before saving
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string SaveFile(FileModel model)
        {
            string actualImagePath = FileDirectoryManager(model);
            SaveFile(model.ByteString, actualImagePath);
            return model.ModuleType + @"\" + model.FileName;
        }

        public static string SaveFileToBlob(FileModel model)
        {
            var storageConnectionString = ConfigurationManager.AppSettings["azurestorageconnectionstring"];

            if (CloudStorageAccount.TryParse(storageConnectionString, out CloudStorageAccount storageAccount))
            {
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("ser-files");
                container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);
                if (model.Extension == ".jpg") 
                {
                    model.FileName = $"{model.FileName}-image-jpg";
                }
                var blob = container.GetBlockBlobReference($"{model.ModuleType}/{model.FileName}{model.Extension}");
                var bytes = string.IsNullOrEmpty(model.ByteString) ? model.Bytes: Convert.FromBase64String(model.ByteString);

                using (var stream = new MemoryStream(bytes))
                {
                    blob.UploadFromStream(stream);
                }

                return blob.Uri.AbsoluteUri;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the file using the File Server
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFilePath(string fileName) {
            return fileDirectoryServer + fileName;
        }
        /// <summary>
        /// Gets the file by accessing the actual file on the server and return as bytestring
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileToString(string path) {
            if (System.IO.File.Exists(path))
            {
                byte[] bytes = System.IO.File.ReadAllBytes(path);
                string imageString = Convert.ToBase64String(bytes);
                return "data:image/jpeg;base64," + imageString;
            }
            else {
                return !string.IsNullOrEmpty(path) ? GetFilePath(path) : "";
            }
        }
    }
}
