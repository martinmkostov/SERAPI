IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[Sites]') 
         AND name = 'ObjectNumber'
)
BEGIN
	ALTER TABLE [dbo].[Sites]
	ADD [ObjectNumber] nvarchar(50) NULL; 
END