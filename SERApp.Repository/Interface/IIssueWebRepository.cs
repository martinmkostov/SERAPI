﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IIssueWebRepository : IRepository<IssueWeb>
    {
        IEnumerable<IssueWeb> GetAllIncludingChildren(int accountId, int siteId, DateTime fromDate, DateTime toDate,int statusId, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0,int issueType3Id = 0);
        IssueWeb GetInlcudingChildrenId(int id);
        void AddIssueWebHistory(IssueWebHistory data);
        List<IssueWebHistory> GetIssueWebHistoryByIssueWebId(int IssueWebId);
        IssueWeb GetInlcudingChildrenIdWithoutTenant(int id);
        void UpdateStatus(List<int> issuesToUpdate);
    }
}
