﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IType3Repository : IRepository<Type3>
    {
        IEnumerable<Type3> GetAllType3IncludeChildren();
        Type3 GetType3IncldeChildren(int id);
        Type3 GetType3ByTenantTypeIdAndSiteId(int tenantType, int siteId);
    }
}
