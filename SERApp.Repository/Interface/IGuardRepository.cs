﻿using SERApp.Data.Models;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IGuardRepository : IRepository<Guard>
    {
        IEnumerable<Guard> GetAllGuardsIncldeChild();
        IEnumerable<Guard> GetBySiteIdAndDate(int siteId, DateTime date);
    }
}
