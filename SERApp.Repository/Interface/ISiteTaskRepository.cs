﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface ISiteTaskRepository : IRepository<SiteTask>
    {
        IEnumerable<SiteTask> GetAllSiteTasksChildIncluded();
        IEnumerable<SiteTaskLog> GetSiteTaskLogs(int siteTaskId);
        IEnumerable<SiteTaskLog> GetSiteTaskLogs(List<int> siteTaskId);
        IEnumerable<SiteTask> GetAllNotReadyTasksByUserId(int accountId, int userId);
    }
}
