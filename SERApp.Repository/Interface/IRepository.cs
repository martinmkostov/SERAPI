﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IRepository<T> where T : class
    {
        void Delete(int id);
        T Save(T data);
        IQueryable<T> GetAll();
        IQueryable<T> GetByPredicate(Func<T, bool> predicate);
        T Get(int Id);
        IQueryable<T> GetAllByRawString(string query);
        void Update(T data);
        int UpdateAll(string query);
        int ExecuteRawSql(string command);
    }
}
