﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface ITagLogRepository : IRepository<TagLog>
    {
        IEnumerable<TagLog> GetAllTagLogsIncludeChilds();
        TagLog GetTagLogsIncludeChilds(int id);
    }
}
