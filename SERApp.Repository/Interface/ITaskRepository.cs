﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SERApp.Repository.Interface
{
    public interface ITaskRepository : IRepository<Task>
    {
        IEnumerable<Task> GetAllTasksChildIncluded(int accountId, string searchText);
        IEnumerable<Task> GetAllTasksSiteTaskIncluded();
        Task GetWithChildsIncluded(int id);
        List<Task> GetAllWithChildsIncluded(List<int> ids);
        List<SiteTaskLog> GetSiteTaskLogs(List<int> ids);
        IEnumerable<Task> GetTasksFromTaskNo(string taskNo);
        IEnumerable<Task> GetTasksFromTaskNoAndAccount(string taskNo, int accountId);
        IEnumerable<SiteTaskLog> GetSiteTaskLogBySiteTaskId(int siteTaskId);
        SiteTask GetSiteTaskBySiteTaskId(int siteTaskId);
        ActivityRole GetActivityRoleByRoleId(int roleId);
        IEnumerable<SiteTask> GetSiteTaskBySiteIdTaskIdCurrentPublished(int siteId,int taskId,bool isCurrentPublished);
        Interval GetIntervalById(int Id);
    }
}
