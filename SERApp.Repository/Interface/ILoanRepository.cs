﻿using SERApp.Data.Models;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface ILoanRepository
    {
        Loan Get(int id);
        List<Loan> GetAll();
        List<Loan> GetAllByCustomerId(int customerId);
        List<Loan> GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType);
        void DeleteLoan(int id);
        void SaveLoan(LoanModel model);
    }
}
