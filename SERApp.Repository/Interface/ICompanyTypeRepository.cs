﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface ICompanyTypeRepository : IRepository<CompanyType>
    {
        IEnumerable<CompanyType> GetAllGuardsIncldeChild();
    }
}
