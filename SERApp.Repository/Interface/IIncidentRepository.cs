﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IIncidentRepository : IRepository<Incident>
    {
        IEnumerable<Incident> GetAllIncidentsChildIncluded(int siteId);
        Incident GetWithChildsIncluded(int id);
        IEnumerable<IncidentListModel> GetAllForList(int siteId, int? accountId, DateTime? date, DateTime? endDate);
        Incident GetWithChildsIncludedV2(int id);
        List<Type1FieldValue> GetType1FieldValue(int id);
        IncidentStatisticsModel GetStatistics(DateTime start, DateTime end, DailyReportStatisticsRequestModel model);
        List<dynamic> GetMinMaxStatsDate(int accountId, int siteId);
        void FixImages();
        string GetImage(int order, int id);
    }
}
