﻿using SERApp.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IContractRepository
    {
        List<ContractModel> GetContractsByIds(string ids);
        List<ContractModel> GetAllContractsForExport(ContractFilters filters);
    }
}
