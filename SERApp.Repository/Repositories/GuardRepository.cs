﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SERApp.Repository.Repositories
{
    public class GuardRepository : SERApp.Repository.Repositories.Repository<Guard>, IRepository<Guard>, IGuardRepository
    {
        public IEnumerable<Guard> GetAllGuardsIncldeChild()
        {
            var data = dbContext.Set<Guard>()
                .Include(x => x.Site);
               // .Include(x => x.IncidentGuard);
            return data;
        }

        public IEnumerable<Guard> GetBySiteIdAndDate(int siteId, DateTime date)
        {
            var query = $@"SELECT		* 
                            FROM		Guards g
                            INNER JOIN	IncidentGuard ig on ig.GuardId = g.Id
                            INNER JOIN	Incidents i on i.Id = ig.IncidentId and i.SiteId = {siteId} and FORMAT(i.Time, 'yyyy-MM-dd') = '{date:yyyy-MM-dd}'";

            return dbContext.Database.SqlQuery<Guard>(query);
        }
    }
}
