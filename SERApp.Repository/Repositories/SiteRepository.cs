﻿using SERApp.Data.Models;
using SERApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class SiteRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        private readonly UserModuleRoleRepository _userModuleRoleRepository;
        private readonly UserRepository _userRepository;

        public SiteRepository()
        {
            db = new SERAppDBContext(connString);
            _userModuleRoleRepository = new UserModuleRoleRepository();
            _userRepository = new UserRepository();
        }

        public Site Get(int id)
        {
            return db.Sites.SingleOrDefault(f => f.Id == id);
        }

        public List<Site> GetAllByAccountId(int id)
        {
            return db.Sites.Where(s => s.AccountId == id).ToList();
        }

        public List<Site> GetAll()
        {
            return db.Sites.ToList();
        }

        public void SaveSite(SiteModel model) {
            var isAdd = (model.Id == 0);

            Site site;
            if (isAdd)
            {
                site = new Site
                {
                    AccountId = model.AccountId,
                    Guid = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                };
            }
            else
            {
                site = Get(model.Id);
            }

            site.Name = model.Name;
            site.Address = model.Address;
            site.City = model.City;
            site.PostalCode = model.PostalCode;
            site.AdditionalLocationInfo = model.AdditionalLocationInfo;
            site.PhoneNumber = model.PhoneNumber;
            site.FaxNumber = model.FaxNumber;
            site.ContactPerson = model.ContactPerson;
            site.ContactPersonNumber = model.ContactPersonNumber;
            site.LastUpdatedDate = DateTime.Now;
            site.IsActive = model.IsActive;
            site.ObjectNumber = model.ObjectNumber;

            site.FacebookLink = model.FacebookLink;
            site.InstagramLink = model.InstagramLink;
            site.YoutubeLink = model.YoutubeLink;
            site.PublicEmailAddress = model.PublicEmailAddress;
            site.PublicContactNumber = model.PublicContactNumber;
            site.CompanyLogo = model.CompanyLogo;
            site.IsDeleted = model.IsDeleted;
            site.GDPRTitle = model.GDPRTitle;
            site.GDPRContent = model.GDPRContent != null ? model.GDPRContent.Replace("\n", "<br>") : "";
            
            if (isAdd)
                db.Sites.Add(site);

            db.SaveChanges();
        }

        public void DeleteSite(int id) {
            var site = Get(id);
            if (site == null) return;

            //site.IsDeleted = true;
            //site.LastUpdatedDate = DateTime.Now;
            //db.SaveChanges();
            db.Sites.Attach(site);
            db.Sites.Remove(site);
            db.SaveChanges();
        }

        public void UpdateEmailSender(int siteId, string email, string name)
        {
            var site = db.Sites.Where(x => x.Id == siteId).FirstOrDefault();
            site.SenderEmailAddress = email;
            site.SenderName = name;

            db.SaveChanges();
        }

        public IQueryable<Site> GetValidSites(int accountId, int userId, int moduleId)
        {
            var role = _userRepository.GetRoleByUserId(userId).Role.Name.ToLower();
            if (role == Role.Admin.Name)
                return db.Sites.Where(s => s.AccountId.Equals(accountId));

            return _userModuleRoleRepository.GetUserModuleRolesByUserIdAndModuleId(userId, moduleId)
                //.Where(x=>x.Role.Name != Role.Viewer.Name)
                .Where(x => x.SiteId.HasValue)
                .Select(x => x.Site);
        }
    }
}
