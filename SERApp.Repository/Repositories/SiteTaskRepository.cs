﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class SiteTaskRepository : Repository<SiteTask>, IRepository<SiteTask>, ISiteTaskRepository
    {
        public IEnumerable<SiteTask> GetAllNotReadyTasksByUserId(int accountId, int userId)
        {
            var query = $@"select st.* from sitetask st
                            join [sites] s on s.id = st.siteid
                            join [Users] u on u.id = st.performerid
                            join Task t on t.id = st.taskid
                            where s.accountid={accountId} and st.performerId = {userId} and st.iscurrentlypublished = 1 and 
                            (st.StartDate <= GETDATE() or DATEADD(day, t.NuberOfDays, st.StartDate) <= GETDATE()) 
                            and s.isactive = 1 and u.isactive != 0 and u.isdeleted != 1";
            return dbContext.Database.SqlQuery<SiteTask>(query);
        }

        public IEnumerable<SiteTask> GetAllSiteTasksChildIncluded()
        {
            return dbContext.SiteTasks.Include(v => v.Site).Include(v => v.Task)                
                .ToList();
        }

        public IEnumerable<SiteTaskLog> GetSiteTaskLogs(int siteTaskId)
        {
            var query = $"SELECT * FROM SiteTaskLog WHERE SiteTaskId = {siteTaskId}";

            var tasks = dbContext.Database.SqlQuery<SiteTaskLog>(query).ToList();

            return tasks;
        }

        public IEnumerable<SiteTaskLog> GetSiteTaskLogs(List<int> siteTaskId)
        {
            var idsstring = string.Join(",", siteTaskId.Select(x => x.ToString()).ToArray());
            var query = $"SELECT * FROM SiteTaskLog WHERE SiteTaskId IN ({idsstring})";

            var tasks = dbContext.Database.SqlQuery<SiteTaskLog>(query).ToList();

            return tasks;
        }
    }
}
