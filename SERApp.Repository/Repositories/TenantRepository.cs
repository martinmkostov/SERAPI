﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class TenantRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public TenantRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Tenant Get(int id) {
            return db.Tenants.Where(t => t.Id == id).SingleOrDefault();
        }

        public PremiseType GetPremiseType(int id) {
            return db.PremiseTypes.Where(s => s.Id == id).SingleOrDefault();
        }

        public SubPremiseType GetSubPremiseType(int id) {
            return db.SubPremiseTypes.Where(s => s.Id == id).SingleOrDefault();
        }

        public JobTitle GetJobTitle(int id) {
            return db.JobTitles.Where(s => s.Id == id).SingleOrDefault();
        }

        public IQueryable<Tenant> GetAll() => 
            db.Tenants.Include(x => x.Site)
                .Include(x => x.TenantTypeEnt)
                .Include(x => x.SubPremiseTypeEnt)
                .Include(x => x.PremiseTypeEnt);

        public IQueryable<Tenant> GetAllByAccountId(int accountId) =>  GetAll().Where(x => x.Site.AccountId.Equals(accountId));

        public IQueryable<Tenant> GetAllBySiteId(int siteId) => GetAll().Where(t => t.SiteId == siteId);

        public List<ContactModel> GetAllTenantContactsByTenantId(int tenantId) {
            var contacts = (from tc in db.TenantContacts
                            join c in db.Contacts on tc.ContactId equals c.Id
                            where tc.TenantId == tenantId
                            select new { c, tc })
                            .Select(c => new ContactModel
                            {
                                Id = c.c.Id,
                                TenantContactId = c.tc.Id,
                                FullName = c.c.FullName,
                                EmailAddress = c.c.EmailAddress,
                                MobileNumber = c.c.MobileNumber,
                                PhoneNumber = c.c.PhoneNumber,
                                JobTypeId = c.tc.JobTypeId,
                                JobTitle = c.c.JobTitle,
                                IsActive = c.c.IsActive,
                            })

                            .ToList();
            return contacts;
        }

        public TenantContact GetTenantContact(int tenantId, int contactId) {
            return db.TenantContacts.Where(tc => tc.TenantId == tenantId && tc.ContactId == contactId).FirstOrDefault();
        }

        public void SaveTenantContacts(TenantContactModel tenantContactModel) {
            //CONTACT
            int contactId = 0;
            ContactModel contactModel = tenantContactModel;
            var contactData = db.Contacts.Where(c => c.Id == tenantContactModel.Id).SingleOrDefault();
            if (contactData != null)
            {
                contactData.FullName = contactModel.FullName;
                contactData.EmailAddress = contactModel.EmailAddress;
                contactData.PhoneNumber = contactModel.PhoneNumber;
                contactData.MobileNumber = contactModel.MobileNumber;
                contactData.LastUpdatedDate = DateTime.Now;
                contactData.JobTitle = contactModel.JobTitle;
                contactData.IsActive = contactModel.IsActive ;
                db.SaveChanges();
            }
            else {
                var newContact = new Contact() {
                    FullName = contactModel.FullName,
                    EmailAddress = contactModel.EmailAddress,
                    PhoneNumber = contactModel.PhoneNumber,
                    MobileNumber = contactModel.MobileNumber,
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now,
                    JobTitle = contactModel.JobTitle,
                    IsActive = contactModel.IsActive
                };
                db.Contacts.Add(newContact);
                db.SaveChanges();
                contactId = newContact.Id;
            }


            //TENANT CONTACT
            var tenantContact = db.TenantContacts.Where(tc => tc.Id == tenantContactModel.TenantContactId).SingleOrDefault();
            if (tenantContact != null) {
                tenantContact.JobTypeId = tenantContactModel.JobTypeId;
            }
            else {
                var newTenantContact = new TenantContact()
                {
                    ContactId = contactId,
                    TenantId = tenantContactModel.TenantId,
                    JobTypeId = tenantContactModel.JobTypeId
                };
                db.TenantContacts.Add(newTenantContact);
            }
            db.SaveChanges();
        }

        public int SaveTenant(TenantModel model, bool isPublic = false)
        {
            int IdToReturn = 0;
            var data = db.Tenants.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.Name = model.Name;
                data.Email = model.Email;
                data.Phone = model.Phone;
                data.PermiseType = model.PermiseType;
                data.VisitingAddress = model.VisitingAddress;
                data.UserName = model.UserName;
                data.Password = model.Password;
                data.InsuranceCompany = model.InsuranceCompany;
                data.SecurityCompany = model.SecurityCompany;
                data.SiteManagerName = model.SiteManagerName;
                data.SiteManagerMobilePhone = model.SiteManagerPhone;
                data.SiteManagerPhone = model.SiteManagerPhone;
                data.ContractName = model.ContractName;
                data.ContractVatNo = model.ContractVatNo;
                data.InvoiceAddress = model.InvoiceAddress;
                data.InvoiceZipAndCity = model.InvoiceZipAndCity;
                data.ContactComments = model.ContactComments;
                data.CCTV = model.CCTV;
                data.IntrusionAlarm = model.IntrusionAlarm;

                if (!isPublic)
                {
                    data.LastUpdatedDate = DateTime.Now;
                }

                data.EmailTemplate = model.EmailTemplate;
                data.Image = model.Image;
                data.SiteManagerEmail = model.SiteManagerEmail;
                data.Flag = model.Flag;
                data.PropertyDesignation = model.PropertyDesignation;
                data.UseWrittenStatement = model.UseWrittenStatement;
                data.SiteId = model.SiteId;
                data.TenantType = model.TenantType;
                data.SubPremiseType = model.SubPremiseType;
                data.City = model.City;
                data.ZipCode = model.ZipCode;
                data.IsActive = model.IsActive;
                data.PostZipCode = model.PostZipCode;
                data.PostAddress = model.PostAddress;
                data.PostCity = model.PostCity;
                data.LastUpdatedBy = model.LastUpdatedBy;
                if (isPublic) {
                    data.LastUpdatedByCustomer = DateTime.Now;
                }
                
                IdToReturn = data.Id;
                db.SaveChanges();
            }
            else {
                Tenant newTenant = new Tenant()
                {
                    Name = model.Name,
                    Email = model.Email,
                    EmailTemplate = model.EmailTemplate,
                    Image = model.Image,
                    Phone = model.Phone,
                    PermiseType = model.PermiseType,
                    VisitingAddress = model.VisitingAddress,
                    UserName = model.UserName,
                    Password = model.Password,
                    InsuranceCompany = model.InsuranceCompany,
                    SecurityCompany = model.SecurityCompany,
                    SiteManagerName = model.SiteManagerName,
                    SiteManagerMobilePhone = model.SiteManagerPhone,
                    SiteManagerPhone = model.SiteManagerPhone,
                    ContractName = model.ContractName,
                    ContractVatNo = model.ContractVatNo,
                    InvoiceAddress = model.InsuranceCompany,
                    InvoiceZipAndCity = model.InvoiceZipAndCity,
                    ContactComments = model.ContactComments,
                    CCTV = model.CCTV,
                    IntrusionAlarm = model.IntrusionAlarm,
                    LastUpdatedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    SiteManagerEmail = model.SiteManagerEmail,
                    Flag = false,
                    PropertyDesignation = model.PropertyDesignation,
                    UseWrittenStatement = model.UseWrittenStatement,
                    SiteId = model.SiteId,
                    IsActive = model.IsActive,
                    TenantType = model.TenantType,
                    SubPremiseType = model.SubPremiseType,
                    City = model.City,
                    ZipCode = model.ZipCode,
                    PostCity = model.PostCity,
                    PostAddress = model.PostAddress,
                    PostZipCode = model.PostZipCode,
                    LastUpdatedBy = model.LastUpdatedBy
                };
                db.Tenants.Add(newTenant);
                db.SaveChanges();
                IdToReturn = newTenant.Id;
            }


          
            return IdToReturn;
        }

        public void DeleteTenant(int id) {
            var data = db.Tenants.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.Tenants.Remove(data);
                db.SaveChanges();
            }
        }

        public void SaveTenantType(TenantTypeModel model) {
            var data = db.TenantTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null) {
                data.TypeName = model.TypeName;
                data.Description = model.Description;
            }
            else {
                var newTenantType = new TenantType()
                {
                    TypeName = model.TypeName,
                    Description = model.Description,
                    AccountId = model.AccountId
                };

                db.TenantTypes.Add(newTenantType);
            }
            db.SaveChanges();
        }

        public void SavePremiseType(PremiseTypeModel model) {
            var data = db.PremiseTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.TypeName = model.TypeName;
                data.Description = model.Description;
            }
            else
            {
                var newPremiseType = new PremiseType()
                {
                    TenantTypeId = model.TenantTypeId,
                    TypeName = model.TypeName,
                    Description = model.Description,
                    AccountId = model.AccountId
                };

                db.PremiseTypes.Add(newPremiseType);
            }
            db.SaveChanges();
        }

        public void SaveSubPremiseType(SubPremiseTypeModel model) {
            var data = db.SubPremiseTypes.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.PremiseTypeId = model.PremiseTypeId;
                data.TypeName = model.TypeName;
                data.Description = model.Description;
            }
            else
            {
                var newSubPremiseType = new SubPremiseType()
                {
                    TypeName = model.TypeName,
                    PremiseTypeId = model.PremiseTypeId,
                    Description = model.Description,
                    AccountId = model.AccountId
                };

                db.SubPremiseTypes.Add(newSubPremiseType);
            }
            db.SaveChanges();
        }

        public void SaveJobTite(JobTitleModel model) {
            var data = db.JobTitles.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.TitleName = model.TitleName;
                data.Description = model.Description;
                data.IncludedOnExcelExport = model.IncludedOnExcelExport;
            }
            else
            {
                var newJobTitle = new JobTitle()
                {
                    TitleName = model.TitleName,
                    Description = model.Description,
                    AccountId = model.AccountId,
                    IncludedOnExcelExport = model.IncludedOnExcelExport
                };

                db.JobTitles.Add(newJobTitle);
            }
            db.SaveChanges();
        }

        public void DeleteTenantType(int id) {
            var data = db.TenantTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.TenantTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeletePremiseType(int id) {
            var data = db.PremiseTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.PremiseTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteSubPremiseType(int id) {
            var data = db.SubPremiseTypes.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.SubPremiseTypes.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteJobTitle(int id) {
            var data = db.JobTitles.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.JobTitles.Remove(data);
                db.SaveChanges();
            }
        }

        public void DeleteTenantContact(int id) {
            var data = db.TenantContacts.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.TenantContacts.Remove(data);
                db.SaveChanges();
            }
        }

        public List<TenantModel> GetTenantsByIds(List<int> ids)
        {
            var concat = string.Join(",", ids);
            var lists = db.Database.SqlQuery<TenantModel>($"SELECT Id, Name, SiteId FROM Tenants WHERE Id in ({concat})").ToList();
            return lists;
        }
    }
}
