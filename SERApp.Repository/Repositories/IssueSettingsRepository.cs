﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class IssueSettingsRepository : Repository<IssueSettings>, IRepository<IssueSettings>, IIssueSettingsRepository
    {

        public IssueSettings GetByAccountId(int accountId)
        {
            return dbContext.IssueSettings.SingleOrDefault(x => x.AccountId == accountId);
        }
    }
}
