﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SERApp.Repository.Repositories
{
    public class TagLogRepository : Repository<TagLog>, IRepository<TagLog>, ITagLogRepository
    {
        public IEnumerable<TagLog> GetAllTagLogsIncludeChilds()
        {
            var data = dbContext.Set<TagLog>()
               .Include(x => x.Guard)
               .Include(x => x.Tag)
               .Include(x => x.Tag.Tenant)
               .Include(x=>x.Tag.Tenant.Site);

            return data;
        }

        public TagLog GetTagLogsIncludeChilds(int id)
        {
            var data = dbContext.Set<TagLog>()
             .Include(x => x.Guard)
                .Include(x => x.Tag)
                .Include(x => x.Tag.Tenant)
                .Include(x => x.Tag.Tenant.Site)
                 .SingleOrDefault(x => x.Id == id);

            return data;
        }
    }
}
