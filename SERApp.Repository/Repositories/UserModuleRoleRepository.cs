﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class UserModuleRoleRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public UserModuleRoleRepository()
        {
            db = new SERAppDBContext(connString);
        }
        #region ROLES
        public List<Role> GetAllUserRoles()
        {
            return db.Roles.Where(r => r.IsUser).ToList();
        }

        public List<Role> GetAllModuleRoles()
        {
            return db.Roles.Where(r => r.IsModule).ToList();
        }

        public List<Role> GetAllRoles()
        {
            return db.Roles.ToList();
        }

        public Role GetUserRole(int userId) {
            var data = db.UserRoles.Where(u => u.UserId == userId).FirstOrDefault();
            var role = db.Roles.Where(r => r.Id == data.RoleId).SingleOrDefault();
            return role; // assuming there should only be one role per user
        }


        public Role GetUserRole(int userId,List<Role> roles, List<UserRole> userRoles)
        {

          

            var data = userRoles.Where(u => u.UserId == userId).FirstOrDefault();

            var role = roles.Where(r => r.Id == data.RoleId).SingleOrDefault();
            return role; // assuming there should only be one role per user
        }

        public List<Role> GetUserRoles(List<int> userIds)
        {

            var idsstring = string.Join(",", userIds.Select(x => x.ToString()).ToArray());

            var query = $"SELECT * FROM UserRoles WHERE UserId IN({idsstring})";

            var usersRole = db.Database.SqlQuery<UserRole>(query).ToList();

            var roleIds = usersRole.Select(v => v.RoleId).Distinct();

            var roles = db.Roles.Where(v => roleIds.Contains(v.Id)).ToList();

            return roles;

            //var data = db.UserRoles.Where(u => u.UserId == userId).FirstOrDefault();
            //var role = db.Roles.Where(r => r.Id == data.RoleId).SingleOrDefault();
            //return role; // assuming there should only be one role per user
        }

        public void SaveUserRole()
        {

        }
        #endregion

        #region USERMODULEROLES
        
        public IQueryable<UserModuleRoles> GetUserModuleRolesByUserId(int userId) => 
            db.UserModuleRoles.Where(u => u.UserId == userId);
        public IQueryable<UserModuleRoles> GetUserModuleRolesByUserIdAndModuleId(int userId, int moduleId) => 
            GetUserModuleRolesByUserId(userId).Where(u => u.ModuleId == moduleId);

        public List<UserModuleRoles> GetUserModuleRolesById(int userId) {
            return db.UserModuleRoles.Where(u => u.UserId == userId && u.Site.IsActive.HasValue && u.Site.IsActive.Value).ToList();
        }

        public List<UserRole> GetUserRolesData(List<int> userIds)
        {
            return db.UserRoles.Where(u => userIds.Contains(u.UserId)).ToList();
        }

        public List<UserModuleRoles> GetUserModuleRolesBySiteId(int siteId)
        {
            if (siteId == -1)
            {
                return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).ToList();
            }

            return db.UserModuleRoles.Include(r=>r.User).Include(r=>r.Role).Where(u => u.SiteId == siteId).ToList();
        }

        public List<UserModuleRoles> GetUserModuleRolesBySiteId(List<int> siteIds)
        {
            //if (siteId == -1)
            //{
            //    return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).ToList();
            //}

            return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).Where(u => u.SiteId.HasValue && siteIds.Contains(u.SiteId.Value)).ToList();
        }

        public List<UserModuleRoles> GetByUsersId(int userId)
        {
            return db.UserModuleRoles.Include(x=>x.Role).Where(x => x.UserId == userId).ToList();
        }

        public List<UserModuleRoles> GetUserModuleRolesBySiteIds(List<int> siteId)
        {
            return db.UserModuleRoles.Include(r => r.User).Include(r => r.Role).Where(u => siteId.Contains(u.SiteId.Value)).ToList();
        }

        public void SaveUserModuleRole(UserModuleRoleModel model) {
            var data = db.UserModuleRoles.Where(u => u.ModuleId == model.ModuleId && u.UserId == model.UserId && u.SiteId == model.SiteId).SingleOrDefault();
            if (data != null || model.RoleId == 0)
            {
                db.UserModuleRoles.Remove(data);
                db.SaveChanges();

                if (model.RoleId > 0) {
                    UserModuleRoles userModuleRole = new UserModuleRoles()
                    {
                        RoleId = model.RoleId,
                        UserId = model.UserId,
                        SiteId = model.SiteId,
                        ModuleId = model.ModuleId,
                        CreatedDate = DateTime.Now
                    };
                    db.UserModuleRoles.Add(userModuleRole);
                    db.SaveChanges();
                }
            }
            else {
                UserModuleRoles userModuleRole = new UserModuleRoles()
                {
                    RoleId = model.RoleId,
                    UserId = model.UserId,
                    SiteId = model.SiteId,
                    ModuleId = model.ModuleId,
                    CreatedDate = DateTime.Now
                };
                db.UserModuleRoles.Add(userModuleRole);
                db.SaveChanges();
            }

        }

        public void DeleteUserRole() {

        }

        public void DeleteUserModuleRoleByUserId(int userId) {
            db.UserModuleRoles.ToList().RemoveAll(u => u.UserId == userId);
            db.SaveChanges();
        }

        public List<UserModuleRoles> GetAllBySite(int SiteId)
        {
            return db.UserModuleRoles.Where(x => x.SiteId == SiteId).ToList();
        }

        public List<UserModuleRoles> GetAllByModule(int moduleId, int userId)
        {
            return db.UserModuleRoles.Where(x => x.ModuleId == moduleId && x.UserId == userId).ToList();
        }
        #endregion
    }
}
