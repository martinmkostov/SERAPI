﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace SERApp.Repository.Repositories
{
    public class TaskRepository : Repository<Task>, IRepository<Task>, ITaskRepository
    {
        public IEnumerable<Task> GetAllTasksChildIncluded(int accountId, string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return dbContext.Tasks.Include(x => x.TaskMainCategory).Include(x => x.TaskSubCategories)
                             .Include(x => x.TaskMainCategory.Select(r => r.MainCategory)).Include(x => x.TaskSubCategories.Select(r => r.SubCategory))
                             .Where(x => x.AccountId == accountId)
                             .ToList();
            }

            return dbContext.Tasks.Include(x => x.TaskMainCategory).Include(x => x.TaskSubCategories)
                .Include(x => x.TaskMainCategory.Select(r => r.MainCategory)).Include(x => x.TaskSubCategories.Select(r => r.SubCategory))
                .Where(x=>x.AccountId == accountId && (x.Title.Contains(searchText) || x.TaskNo.Contains(searchText)))
                .ToList();
        }

        public IEnumerable<Task> GetAllTasksSiteTaskIncluded()
        {
            return dbContext.Tasks.Include(x => x.SiteTask)
                .ToList();
        }

        public Task GetWithChildsIncluded(int id)
        {
            //return dbContext.Tasks.Include(x => x.TaskMainCategory).Include(x => x.TaskSubCategories)
            //.Include(x => x.TaskMainCategory.Select(r => r.MainCategory)).Include(x => x.TaskSubCategories.Select(r => r.SubCategory))
            //.SingleOrDefault(x => x.Id == id);

            var query = $"SELECT * FROM Task WHERE Id = '{id}'";
            var data = dbContext.Database.SqlQuery<Task>(query).SingleOrDefault();

            var queryTaskMainCategory = $"SELECT * FROM TaskMainCategory WHERE TaskId = '{data.Id}'";
            var dataTaskMainCategory = dbContext.Database.SqlQuery<TaskMainCategory>(queryTaskMainCategory).ToList();


            var queryTaskSubCategory = $"SELECT * FROM TaskSubCategory WHERE TaskId = '{data.Id}'";
            var dataTaskSubCategory = dbContext.Database.SqlQuery<TaskSubCategory>(queryTaskSubCategory).ToList();


            var idsstringMainCategory = string.Join(",", dataTaskMainCategory.Select(x => x.MainCategoryId).ToArray());

            var idsstringSubCategory = string.Join(",", dataTaskSubCategory.Select(x => x.SubCategoryId).ToArray());

            var mainCategories = $"SELECT * FROM MainCategory WHERE Id IN ({idsstringMainCategory})";

            var mainCategoriesData = new List<MainCategory>();
            if (!string.IsNullOrEmpty(idsstringMainCategory))
            {
                mainCategoriesData = dbContext.Database.SqlQuery<MainCategory>(mainCategories).ToList();
            }

            var subCategories = $"SELECT * FROM SubCategory WHERE Id IN ({idsstringSubCategory})";

            var subCategoriesData = new List<SubCategory>();
            if (!string.IsNullOrEmpty(idsstringSubCategory))
            {
                subCategoriesData = dbContext.Database.SqlQuery<SubCategory>(subCategories).ToList();
            }                        


            dataTaskMainCategory.ForEach(x => 
            {
                x.MainCategory = mainCategoriesData.SingleOrDefault(r => r.Id == x.MainCategoryId);
            });

            dataTaskSubCategory.ForEach(x =>
            {
                x.SubCategory = subCategoriesData.SingleOrDefault(r => r.Id == x.SubCategoryId);
            });

            data.TaskMainCategory = dataTaskMainCategory;
            data.TaskSubCategories = dataTaskSubCategory;


            return data;

        }

        public List<Task> GetAllWithChildsIncluded(List<int> ids)
        {
            var idsstring = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            var querytasks = $"SELECT t.IsBimControl, t.IsFileRequired, t.Id, t.TaskNo,t.Title,t.Purpose,t.Description,t.Interval,t.NuberOfDays,t.RemindAfter,t.RoleId,t.ExtensionsAllowed,t.DayToComplete,t.RecommendedRole,t.IsArchived,t.AccountId,t.LastUpdatedTime,t.LastUpdatedBy FROM Task t WHERE t.Id IN ({idsstring})";
            var querytaskmaincategories = $"SELECT tmc.Id,tmc.MainCategoryId,tmc.TaskId FROM TaskMainCategory tmc WHERE tmc.TaskId IN({idsstring})";
            var querytasksubcategories = $"SELECT tsc.Id,tsc.SubCategoryId,tsc.TaskId FROM TaskSubCategory tsc WHERE tsc.TaskId IN({idsstring}) ";

            var tasks = dbContext.Database.SqlQuery<Task>(querytasks).ToList();
            var taskmaincategories = dbContext.Database.SqlQuery<TaskMainCategory>(querytaskmaincategories).ToList();
            var tasksubcategories = dbContext.Database.SqlQuery<TaskSubCategory>(querytasksubcategories).ToList();

            var accountId = tasks.Where(r => r.AccountId != 0).Select(r => r.AccountId).Distinct();

            var maincategories = new List<MainCategory>();
            var subcategories = new List<SubCategory>();

            var accountids = string.Join(",", accountId.Select(x => x.ToString()).ToArray());
            if (accountids.Any())
            {
                var querymaincategories = $"SELECT mc.Id,mc.Name,mc.AccountId FROM MainCategory mc WHERE mc.AccountId IN ({accountids})";
                var querysubcategories = $"SELECT sc.Id,sc.Name,sc.AccountId FROM SubCategory sc WHERE sc.AccountId IN ({accountids})";

                maincategories = dbContext.Database.SqlQuery<MainCategory>(querymaincategories).ToList();
                subcategories = dbContext.Database.SqlQuery<SubCategory>(querysubcategories).ToList();
            }

            System.Threading.Tasks.Parallel.ForEach(tasks, x =>
            {
                x.TaskMainCategory = taskmaincategories.Where(r => r.TaskId == x.Id).ToList();
                x.TaskSubCategories = tasksubcategories.Where(r => r.TaskId == x.Id).ToList();

                if (maincategories.Any())
                {
                    foreach (var tmc in x.TaskMainCategory)
                    {
                        tmc.MainCategory = maincategories.SingleOrDefault(m => m.Id == tmc.MainCategoryId);
                    }
                }

                if (subcategories.Any())
                {
                    foreach (var tsc in x.TaskSubCategories)
                    {
                        tsc.SubCategory = subcategories.SingleOrDefault(m => m.Id == tsc.SubCategoryId);
                    }
                }

            });

            return tasks;

            //return dbContext.Tasks.Include(x => x.TaskMainCategory).Include(x => x.TaskSubCategories)
            //.Include(x => x.TaskMainCategory.Select(r => r.MainCategory)).Include(x => x.TaskSubCategories.Select(r => r.SubCategory))
            //.SingleOrDefault(x => x.Id == id);
        }

        public List<SiteTaskLog> GetSiteTaskLogs(List<int> ids)
        {
            var idsstring = string.Join(",", ids.Select(x => x.ToString()).ToArray());

            var query = $"SELECT * FROM SiteTaskLog WHERE SiteTaskId IN ({idsstring})";

            var data = dbContext.Database.SqlQuery<SiteTaskLog>(query).ToList();

            return data;
        }

        public IEnumerable<Task> GetTasksFromTaskNo(string taskNo)
        {
            var query = $"SELECT * FROM Task WHERE TaskNo = '{taskNo}'";
            var data = dbContext.Database.SqlQuery<Task>(query).ToList();

            return data;
            
        }

        public IEnumerable<Task> GetTasksFromTaskNoAndAccount(string taskNo, int accountId)
        {
            var query = $"SELECT * FROM Task WHERE TaskNo = '{taskNo}' AND AccountId = {accountId}";
            var data = dbContext.Database.SqlQuery<Task>(query).ToList();

            return data;
        }

        public IEnumerable<SiteTaskLog> GetSiteTaskLogBySiteTaskId(int siteTaskId)
        {
            var query = $"SELECT * FROM SiteTaskLog WHERE SiteTaskId = '{siteTaskId}'";
            var data = dbContext.Database.SqlQuery<SiteTaskLog>(query).ToList();

            return data;
        }

        public SiteTask GetSiteTaskBySiteTaskId(int siteTaskId)
        {
            var query = $"SELECT * FROM SiteTask WHERE Id = '{siteTaskId}'";
            var data = dbContext.Database.SqlQuery<SiteTask>(query).FirstOrDefault();

            return data;
        }

        public ActivityRole GetActivityRoleByRoleId(int roleId)
        {
            var query = $"SELECT * FROM ActivityRole WHERE ActivityRoleId = '{roleId}'";
            var data = dbContext.Database.SqlQuery<ActivityRole>(query).FirstOrDefault();

            return data;
        }

        public IEnumerable<SiteTask> GetSiteTaskBySiteIdTaskIdCurrentPublished(int siteId, int taskId, bool isCurrentPublished)
        {
            var query = $"SELECT * FROM SiteTask WHERE SiteId = '{siteId}' AND TaskId = '{taskId}' AND IsCurrentlyPublished = '{isCurrentPublished}'";
            var data = dbContext.Database.SqlQuery<SiteTask>(query).ToList();

            return data;
        }

        public Interval GetIntervalById(int Id)
        {
            var query = $"SELECT * FROM Interval WHERE IntervalId = '{Id}'";
            var data = dbContext.Database.SqlQuery<Interval>(query).SingleOrDefault();

            return data;
        }
    }
}
