﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class ModuleHelpTemplateRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public ModuleHelpTemplateRepository()
        {
            db = new SERAppDBContext(connString);
        }

        //public ModulelHelpTemplate Get(int id)
        //{
        //    return db.ModulelHelpTemplates.Where(f => f.Id == id).SingleOrDefault();
        //}

        public ModuleHelpTemplate Get(int moduleId)
        {
            return db.ModulelHelpTemplates.Where(f => f.ModuleId == moduleId).SingleOrDefault();
        }

        public List<ModuleHelpTemplate> GetAll() {
            return db.ModulelHelpTemplates.ToList();
        }

        public void Save(ModuleHelpTemplateModel model) {
            var data = db.ModulelHelpTemplates.Where(m => m.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.Name = model.Name;
                data.ContentValue = model.ContentValue;
            }
            else {
                var newModuleHelpTemplate = new ModuleHelpTemplate()
                {
                    ModuleId = model.ModuleId,
                    Name = model.Name,
                    ContentValue = model.ContentValue
                };
                db.ModulelHelpTemplates.Add(newModuleHelpTemplate);
            }
            db.SaveChanges();
        }

        public void Delete(int id) {
            var data = db.ModulelHelpTemplates.Where(m => m.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.ModulelHelpTemplates.Remove(data);
                db.SaveChanges();
            }
        }
    }
}
