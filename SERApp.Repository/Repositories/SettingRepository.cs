﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SERApp.Models.Common;


namespace SERApp.Repository.Repositories
{
    public class SettingRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public SettingRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Setting Get(int id) {
            return db.Settings.Where(s => s.SettingId == id).SingleOrDefault();
        }


        public Setting Get(int configSettingId, int accountId) {
            return db.Settings.Where(s => s.ConfigSettingId == configSettingId && s.AccountId == accountId).SingleOrDefault();
        }

        public Setting Get(string name, int accountId) {
            return db.Settings.Where(s => s.Name == name && s.AccountId == accountId).SingleOrDefault();
        }

        public void Save(AccountSettingModel model)
        {
            var data = db.Settings.Where(s => s.AccountId == model.AccountId && s.ConfigSettingId == model.ConfigSettingId).SingleOrDefault();
            if (data != null)
            {
                data.Value = model.Value;
            }
            else
            {
                var newSetting = new Setting()
                {
                    AccountId = model.AccountId,
                    ConfigSettingId = model.ConfigSettingId,
                    Name = model.Name,
                    Value = model.Value
                };
                db.Settings.Add(newSetting);
            }
            db.SaveChanges();
        }

        public List<Setting> GetAccountSettings(int accountId) {
            return db.Settings.Where(a => a.AccountId == accountId).ToList();
        }

        public List<ConfigSetting> GetConfigSettings() {
            return db.ConfigSettings.ToList();
        }

        public ConfigSetting GetConfigSetting(int id) {
            return db.ConfigSettings.Where(c => c.Id == id).SingleOrDefault();
        }

        public ConfigSetting GetConfigSetting(string name)
        {
            return db.ConfigSettings.Where(c => c.SettingName == name).SingleOrDefault();
        }

        public List<Setting> GetSettingBySiteAndName(string name, int siteId)
        {
            var query = $"SELECT * FROM Setting WHERE Name = '{name}' AND SiteId = {siteId}";

            return db.Database.SqlQuery<Setting>(query).ToList();
        }
        public List<Setting> GetSettingBySiteAndNameAccountId(string name, int siteId,int accountId)
        {
            var query = $"SELECT * FROM Setting WHERE Name = '{name}' AND SiteId = {siteId} AND AccountId = {accountId}";

            return db.Database.SqlQuery<Setting>(query).ToList();
        }
        public List<Setting> GetSettingByAccountIdAndName(string name, int accountId)
        {
            var query = $"SELECT * FROM Setting WHERE Name = '{name}' AND AccountId = {accountId}";

            return db.Database.SqlQuery<Setting>(query).ToList();
        }
        public List<Setting> GetSettingByAccountIdAndModuleIdAndSiteId(int accountId,int moduleId,int siteId)
        {
            var query = $"SELECT * FROM Setting LEFT JOIN ConfigSettings ON ConfigSettings.Id = Setting.ConfigSettingId WHERE AccountId = {accountId} AND ConfigSettings.ModuleId = {moduleId} AND SiteId = {siteId}";

            return db.Database.SqlQuery<Setting>(query).ToList();
        }
    }
}
