﻿using System.Collections.Generic;
using System.Linq;
using SERApp.Data.Models;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class ModuleRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;

        public ModuleRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Module Get(int id) => db.Modules.Find(id);

        public IQueryable<Module> GetAll() => db.Modules.Where(m => m.IsActive == true);

        public List<Module> GetAllActive()
        {
            return db.Modules.Where(a => a.IsActive == true).ToList();
        }
    }
}