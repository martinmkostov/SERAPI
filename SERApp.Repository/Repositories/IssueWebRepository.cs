﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class IssueWebRepository : Repository<IssueWeb>, IRepository<IssueWeb>, IIssueWebRepository
    {
        public IEnumerable<IssueWeb> GetAllIncludingChildren(int accountId, int siteId, DateTime fromDate, DateTime toDate, int statusId, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0,int issueType3Id =0)
        {
            var issues =  dbContext.IssueWebs.Where(x =>
                            x.AccountId == accountId &&
                            x.DateCreated >= fromDate &&
                            x.DateCreated <= toDate);

            if (siteId != -1)
            {
                issues = issues.Where(x => x.SiteId == siteId);
            }
            if (contactId != 0)
            {
                issues = issues.Where(x => x.TenantId == contactId);
            }
            if (issueType1Id != 0)
            {
                issues = issues.Where(x => x.IssueType1Id == issueType1Id);
            }
            if (issueType2Id != 0)
            {
                issues = issues.Where(x => x.IssueType1Id == issueType2Id);
            }

            if (statusId != -1)
            {
                if (statusId == 0)
                {
                    issues = issues.Where(x => x.Status == 0 || x.Status == 1 || x.Status == 2);
                }
                else
                {
                    issues = issues.Where(x => x.Status == statusId);
                }
            }

            dbContext.Configuration.ProxyCreationEnabled = false;
            var data = issues.ToList();

            if (data.Any())
            {
                var issueType1Ids = data.Where(x => x.IssueType1Id != 0).Select(x => x.IssueType1Id).ToList();
                var issueType2Ids = data.Where(x => x.IssueType2Id != 0).Select(x => x.IssueType2Id).ToList();
                var tenantIds = data.Where(x => x.TenantId != 0).Select(x => x.TenantId).ToList();
                var siteids = data.Where(x => x.SiteId != 0).Select(x => x.SiteId).ToList();

                var idsstring1 = string.Join(",", issueType1Ids.Select(x => x.ToString()).ToArray());
                var idsstring2 = string.Join(",", issueType2Ids.Select(x => x.ToString()).ToArray());
                var tenants = string.Join(",", tenantIds.Select(x => x.ToString()).ToArray());
                var sites = string.Join(",", siteids.Select(x => x.ToString()).ToArray());

                var queryIssueType1 = $"SELECT * FROM IssueType1 WHERE IssueType1Id IN ({idsstring1})";
                var queryIssueType2 = $"SELECT * FROM IssueType2 WHERE IssueType2Id IN ({idsstring2})";


                var queryTenant = $"SELECT * FROM Tenants WHERE Id IN ({tenants})";
                var querySite = $"SELECT * FROM Sites WHERE Id IN ({sites})";

                var issueType1 = dbContext.Database.SqlQuery<IssueType1>(queryIssueType1).ToList();
                var issueType2 = dbContext.Database.SqlQuery<IssueType2>(queryIssueType2).ToList();

                var tenant = new List<Tenant>();
                if (!string.IsNullOrEmpty(tenants))
                {
                    tenant = dbContext.Database.SqlQuery<Tenant>(queryTenant).ToList();
                }
              

                var site = dbContext.Database.SqlQuery<Site>(querySite).ToList();

                Parallel.ForEach(data, x =>
                {
                    if (x.SiteId != 0)
                    {
                        x.Site = site.SingleOrDefault(r => r.Id == x.SiteId);
                    }

                    if (x.TenantId != 0)
                    {
                        x.Tenant = tenant.SingleOrDefault(r => r.Id == x.TenantId);
                    }

                    if (x.IssueType1Id != 0)
                    {
                        x.IssueType1 = issueType1.SingleOrDefault(r => r.IssueType1Id == x.IssueType1Id);
                    }

                    if (x.IssueType2Id != 0)
                    {
                        x.IssueType2 = issueType2.SingleOrDefault(r => r.IssueType2Id == x.IssueType2Id);
                    }


                });
            }
            

            return data;
            //return dbContext.IssueWebs.Include(x=>x.Site)
            //    .Include(x => x.IssueType1)
            //    .Include(x => x.IssueType2)
            //    .Include(x => x.Tenant)      
            //    .Where(x=>x.AccountId == accountId)
            //    .ToList();
        }

        public IssueWeb GetInlcudingChildrenId(int id)
        {
            var d = dbContext.IssueWebs
                 .Include(x => x.Site)
                 .Include(x => x.IssueType1)
                 //.Include(x => x.IssueType2)
                 //       .Include(x => x.Tenant)
                 .Include(x => x.IssueWebHistories)
                 .SingleOrDefault(r => r.IssueWebId == id);

            if (d.TenantId != 0)
            {
                d.Tenant = dbContext.Tenants.SingleOrDefault(x=>x.Id == d.TenantId);
            }

            if (d.IssueType2Id != 0)
            {
                d.IssueType2 = dbContext.IssueType2.SingleOrDefault(x=>x.IssueType2Id == d.IssueType2Id);
            }

            return d;
        }



        public void AddIssueWebHistory(IssueWebHistory data)
        {
            dbContext.IssueWebHistories.Add(data);
            dbContext.SaveChanges();
        }

        public List<IssueWebHistory> GetIssueWebHistoryByIssueWebId(int IssueWebId)
        {
            return dbContext.IssueWebHistories.Where(x => x.IssueWebId == IssueWebId).ToList();
        }

        public IssueWeb GetInlcudingChildrenIdWithoutTenant(int id)
        {
            return dbContext.IssueWebs
             .Include(x => x.Site)
             .Include(x => x.IssueType1)
             .Include(x => x.IssueType2)
            // .Include(x => x.Tenant)
             .Include(x => x.IssueWebHistories)
             .SingleOrDefault(r => r.IssueWebId == id);
        }

        public void UpdateStatus(List<int> issuesToUpdate)
        {
            var idsstring1 = string.Join(",", issuesToUpdate.Select(x => x.ToString()).ToArray());
            var query = $"UPDATE IssueWeb SET Status=1 WHERE IssueWebId IN ({idsstring1})";

            dbContext.Database.ExecuteSqlCommand(query);


        }

        //IssueWeb IIssueWebRepository.Update(IssueWeb model)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
