﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SERApp.Models.Common;


namespace SERApp.Repository.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        #region DYNAMIC CONFIG
        //Use this if we want to use a connection string that can be located else where in the application directory
        //ConfigParserManager.ConnectionStringReader(ConfigParserManager.ConfigSourcePath(), ConfigSettings.SERAppEntitiesKey);
        #endregion
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public CustomerRepository() {
            db = new SERAppDBContext(connString);
        }

        public Customer Get(int id)
        {
            return db.Customers
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public List<Customer> GetAll()
        {
            return db.Customers
                .ToList();
        }

        public List<Customer> GetAllByAccountId(int accountId) {
            return db.Customers.Where(c => c.AccountId == accountId).ToList();
        }

        public List<Customer> GetAllActive() {
            return db.Customers
                .Where(c => c.IsDeleted == false || c.IsDeleted == null).ToList();
        }

        public void ClearCustomerInfo(int id) {
            var customer = db.Customers.Where(l => l.Id == id).SingleOrDefault();
            if (customer != null)
            {
                customer.Name = string.Empty;
                customer.Address1 = string.Empty;
                customer.City = string.Empty;
                customer.State = string.Empty;
                customer.ZipCode = string.Empty;
                customer.Company = string.Empty;
                customer.Email = string.Empty;
                customer.MobileNumber = string.Empty;
                db.SaveChanges();
            }
        }

        public int LoanCounter(int id) {
            return db.Loans.Where(l => l.CustomerTypeId == 1 && l.CustomerId == id && (l.IsDeleted == false || l.IsDeleted == null)).Count();
        }

        public void DeleteCustomer(int id) {
            var customer = db.Customers.Where(l => l.Id == id).SingleOrDefault();
            if (customer != null)
            {
                ClearCustomerInfo(customer.Id);
                customer.IsDeleted = true;
                db.SaveChanges();
            }
         }

        public void SaveCustomer(CustomerModel model)
        {
            var customer = db.Customers.Where(l => l.Id == model.Id).SingleOrDefault();
            if (customer != null)
            {
                customer.Company = model.Company;
                customer.Name = model.Name;
                customer.Address1 = model.Address1;
                customer.Address2 = model.Address2;
                customer.City = model.City;
                customer.ZipCode = model.ZipCode;
                customer.Email = model.Email;
                customer.MobileNumber = model.MobileNumber;
                customer.LastModifiedDate = DateTime.Now;
                customer.IsDeleted = model.IsDeleted;
            }
            else {
                Customer newCustomer = new Customer()
                {
                    Company = model.Company,
                    Name = model.Name,
                    Address1 = model.Address1,
                    Address2 = model.Address2,
                    City = model.City,
                    ZipCode = model.ZipCode,
                    Email = model.Email,
                    MobileNumber = model.MobileNumber,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    IsDeleted = model.IsDeleted
                };
                db.Customers.Add(newCustomer);
                model.Id = newCustomer.Id;
            }
            db.SaveChanges();
        }
    }
}
