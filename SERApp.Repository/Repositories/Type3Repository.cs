﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class Type3Repository : SERApp.Repository.Repositories.Repository<Type3>, IRepository<Type3>, IType3Repository
    {
        public IEnumerable<Type3> GetAllType3IncludeChildren()
        {
            return dbContext.Type3
                //.Include(x => x.Tenant)
                //.Include(x=>x.Tenant.Site)
                .ToList();
        }

        public Type3 GetType3ByTenantTypeIdAndSiteId(int tenantType, int siteId)
        {
            return dbContext.Type3TenantTypes
                //.Include(x=>x.Tenant)
                .Include(x => x.Type3)
                .FirstOrDefault(x => x.TenantTypeId == tenantType && x.Type3.SiteId == siteId)?.Type3;
        }

        public Type3 GetType3IncldeChildren(int id)
        {
            return dbContext.Type3
                //.Include(x=>x.Tenant)
                .Include(x => x.Type3TenantTypes)
                .SingleOrDefault(x => x.Id.Equals(id));
        }
    }
}
