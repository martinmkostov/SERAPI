﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class IncidentAccidentRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public IncidentAccidentRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public List<IncidentAccidentCommentModel> GetComments(int id)
        {
            var comments = db.IncidentAccidentComments.Where(x => x.IncidentAccidentId == id).OrderByDescending(y => y.Date).ToList();

            return comments.Select(r => new IncidentAccidentCommentModel()
            {
                Requirements = r.Requirements,
                IncidentAccidentId = r.IncidentAccidentId,
                Id = r.Id,
                Disbursed = r.Disbursed,
                Date = r.Date,
                Comment = r.Comment,
                CompensationFromInsurance = r.CompensationFromInsurance,
                CompensationFromOthers = r.CompensationFromOthers,
                Name = r.Name,
            }).OrderByDescending(x => x.Date).ToList();
        }
    }
}
