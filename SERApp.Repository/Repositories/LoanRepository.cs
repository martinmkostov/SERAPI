﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class LoanRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public LoanRepository() {
            db = new SERAppDBContext(connString);
        }

        public Loan Get(int id)
        {
            return db.Loans
                .Where(c => c.Id == id).SingleOrDefault();
        }

        public Loan Get(string loanId)
        {
            return db.Loans
                .Where(c => c.LoanId == loanId).SingleOrDefault();
        }

        public List<Loan> GetAll()
        {
            return db.Loans
                .OrderByDescending(l => l.CreatedDate).ToList();
        }

        public List<Loan> GetAllByAccountIdAndUserId(int accountId, int userId)
        {
            var sites = db.UserModuleRoles.Where(x => x.ModuleId == 8 && x.UserId == userId).Select(y => y.SiteId).ToList();

            var loans = GetAllByAccountId(accountId);
            return loans.Where(x => sites.Contains(x.SiteId))
                .OrderByDescending(l => l.CreatedDate).ToList();
        }

        public List<Loan> GetByLoanId(string loanId) {
            return db.Loans.Where(l => l.LoanId.StartsWith(loanId)).ToList();
        }

        public List<Loan> GetAllByCustomerId(int customerId)
        {
            return db.Loans
                .Where(c => c.CustomerId == customerId && c.IsDeleted == false || c.IsDeleted == null).ToList();
        }

        public List<Loan> GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType) {
            List<Loan> loans = db.Loans.Where(l => l.IsDeleted == false || l.IsDeleted == null).ToList();
            if (customerId > 0)
            {
                loans = loans.Where(l => l.CustomerId == customerId).ToList();
            }
            if (typeId > 0)
            {
                loans = loans.Where(l => l.TypeId == typeId).ToList();
            }
            if (statusId > 0)
            {
                loans = loans.Where(l => l.StatusId == statusId).ToList();
            }
            if (!string.IsNullOrEmpty(itemName))
            {
                loans = loans.Where(l => l.ItemNameId.StartsWith(itemName)).ToList();
            }
            if (!string.IsNullOrEmpty(LoanedDate))
            {
                loans = loans.Where(l => l.LoanedDate.Value == DateTime.Parse(LoanedDate)).ToList();
            }
            if (!string.IsNullOrEmpty(ReturnedDate))
            {
                loans = loans.Where(l => l.ReturnedDate.Value == DateTime.Parse(ReturnedDate)).ToList();
            }
            else if (!string.IsNullOrEmpty(LoanedDate) && !string.IsNullOrEmpty(ReturnedDate))
            {
                loans = loans.Where(l => l.ReturnedDate.Value >= DateTime.Parse(LoanedDate) && l.LoanedDate.Value <= DateTime.Parse(ReturnedDate)).ToList();
            }
            return loans;
        }

        public void DeleteLoan(int id) {
            var loan = db.Loans.Where(l => l.Id == id).SingleOrDefault();
            if (loan != null)
            {
                loan.IsDeleted = true;
                loan.LastModifiedDate = DateTime.Now;
                loan.LastModifiedBy = "Admin";
                db.SaveChanges();
            }
        }

        public LoanModel SaveLoan(LoanModel model)
        {
            var loan = db.Loans.Where(l => l.Id == model.Id).SingleOrDefault();
            if (loan != null)
            {
                loan.SiteId = model.SiteId;
                loan.CustomerId = model.CustomerId;
                loan.CustomerTypeId = model.CustomerTypeId;
                loan.TypeId = model.TypeId;
                loan.StatusId = model.StatusId;
                loan.Description = model.Description;
                loan.Notes = model.Notes;
                loan.ItemNameId = model.ItemNameId;
                loan.LoanId = model.LoanId;
                //loan.PhotoUrl = model.PhotoUrl; THIS IS SAVED AFTER THE LOAN IS SAVED
                loan.MessageTypeId = model.MessageTypeId;
                loan.LoanedDate = model.LoanedDate;
                loan.ReturnedDate = model.ReturnedDate;
                loan.LastModifiedDate = DateTime.Now;
                loan.IsDeleted = model.IsDeleted;
                loan.GivenBy = model.GivenBy;
                loan.ReceivedBy = model.ReceivedBy;
                loan.LastModifiedBy = model.LastModifiedBy;
                loan.FirstName = model.FirstName;
                loan.Mobile = model.Mobile;
                loan.Email = model.Email;
                if(model.ConfirmedDate > DateTime.MinValue)
                {
                    loan.ConfirmedDate = model.ConfirmedDate;
                }
                if (model.ActualReturnedDate > DateTime.MinValue)
                {
                    loan.ActualReturnedDate = model.ActualReturnedDate;
                }

                if (loan.CustomerTypeId == 1)
                {
                    SaveCustomer(model);
                }
                
                db.SaveChanges();
            }
            else {
                if (model.CustomerId == 0)
                {
                    model.CustomerId = SaveCustomer(model);
                    model.CustomerTypeId = 1;
                }
                Loan newLoan = new Loan()
                {
                    SiteId = model.SiteId,
                    CustomerId = model.CustomerId,
                    CustomerTypeId = model.CustomerTypeId,
                    TypeId = model.TypeId,
                    StatusId = 4, //initially set it to 4 (NotConfirmed)
                    Description = model.Description,
                    Notes = model.Notes,
                    //PhotoUrl = model.PhotoUrl, THIS IS SAVED AFTER THE LOAN IS SAVED
                    MessageTypeId = model.MessageTypeId,
                    LoanedDate = model.LoanedDate,
                    ReturnedDate = model.ReturnedDate,
                    LastModifiedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    IsDeleted = false,
                    GivenBy = model.GivenBy,
                    ReceivedBy = model.ReceivedBy,
                    CreatedBy = model.CreatedBy,
                    LastModifiedBy = model.LastModifiedBy,
                    ItemNameId = model.ItemNameId,
                    LoanId = model.LoanId,
                    FirstName = model.FirstName,
                    Email = model.Email,
                    Mobile = model.Mobile
                   
            };
                
                db.Loans.Add(newLoan);
                db.SaveChanges();
                model.Id = newLoan.Id;
            }

            return model;
        }

        public void SaveLoanPhoto(int id, string photoURL) {
            var loan = db.Loans.Where(l => l.Id == id).SingleOrDefault();
            if (loan != null)
            {
                loan.PhotoUrl = photoURL;
                db.SaveChanges();
            }
        }

        public int SaveCustomer(LoanModel model) {
            var customer = model.Customer;
            var data = db.Customers.Where(c => c.Id == customer.Id).SingleOrDefault();
            if (data != null) {

                data.Name = customer.Name;
                data.Company = customer.Company;
                data.Address1 = customer.Address1;
                data.City = customer.City;
                data.ZipCode = customer.ZipCode;
                data.State = customer.State;
                data.Email = customer.Email;
                data.MobileNumber = customer.MobileNumber;
                data.CreatedBy = model.CreatedBy;
                data.LastModifiedBy = model.LastModifiedBy;
                data.LastModifiedDate = DateTime.Now;
                db.SaveChanges();
                return data.Id;
            }
            else {
                Customer newCustomer = new Customer()
                {
                    Name = customer.Name,
                    AccountId = customer.AccountId,
                    Company = customer.Company,
                    Address1 = customer.Address1,
                    City = customer.City,
                    ZipCode = customer.ZipCode,
                    State = customer.State,
                    Email = customer.Email,
                    MobileNumber = customer.MobileNumber,
                    CreatedBy = model.CreatedBy,
                    LastModifiedBy = model.LastModifiedBy,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    IsDeleted = false
                };
                db.Customers.Add(newCustomer);
                db.SaveChanges();
                return newCustomer.Id;
            }
            
        }

        public void ConfirmLoan(int loanId)
        {
            var loan = db.Loans.Where(l => l.Id == loanId).SingleOrDefault();
            if (loan != null)
            {
                loan.StatusId = 1; //confirm - Active
                loan.ConfirmedDate = DateTime.Now;
                loan.LastModifiedDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        public void ReturnLoan(int loanId)
        {
            var loan = db.Loans.Where(l => l.Id == loanId).SingleOrDefault();
            if (loan != null)
            {
                loan.StatusId = 2; //Returned
                loan.ActualReturnedDate = DateTime.Now;
                loan.LastModifiedDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        public List<Loan> GetAllPassedDateLoans() {
            return db.Loans.Where(l => (DateTime.Now > l.ReturnedDate.Value.Date))
                .ToList();
        }

        public List<Loan> GetAllReturnedLoans() {
            return db.Loans.Where(l => l.StatusId == 2)
            .ToList();
        }

        #region Loan Type

        public LoanType GetLoanType(int id) {
            return db.LoanTypes.Where(l => l.Id == id).SingleOrDefault();
        }

        public List<LoanType> GetAllLoanTypes() {
            return db.LoanTypes.ToList();
        }
        //by account id
        public List<LoanType> GetLoanTypesByAccountId(int accountId) {
            return db.LoanTypes.Where(l => l.AccountId == accountId).ToList();
        }

        public void SaveLoanType(LoanTypeModel model) {
            var data = db.LoanTypes.Where(l => l.Id == model.Id).SingleOrDefault();
            if (data != null) {
                data.Name = model.Name;
                data.Description = model.Description;
                data.IsPhotoRequired = model.IsPhotoRequired;
                data.ShowReturnDate = model.ShowReturnDate;
            }
            else {
                var newLoanType = new LoanType() {
                    Name = model.Name,
                    Description = model.Description,
                    AccountId = model.AccountId,
                    IsPhotoRequired = model.IsPhotoRequired,
                    ShowReturnDate = model.ShowReturnDate
                };
                db.LoanTypes.Add(newLoanType);
            }
            db.SaveChanges();
        }

        public void DeleteLoanType(int id) {
            var data = db.LoanTypes.Where(l => l.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.LoanTypes.Remove(data);
                db.SaveChanges();
            }
        }
        #endregion

        #region STANDARD QUERY
        public List<Loan> GetAllByAccountId(int accountId)
        {
            var allActiveSites = db.Sites.Where(a => a.IsActive == true && a.AccountId == accountId).Select(y => y.Id).ToList();
            return db.Loans.Where(x => x.IsDeleted != true && allActiveSites.Contains(x.SiteId)).OrderByDescending(l => l.CreatedDate).ToList();
        }
        //@TODO : IMPROVE QUERY
        public List<Loan> GetAllWithFilter(int accountId, string keyword)
        {
            string query = "SELECT l.* FROM Loans l " +
                          "LEFT JOIN Sites s ON l.SiteId = s.Id " +
                          "LEFT JOIN Customers c ON l.CustomerId = c.id " +
                          "LEFT JOIN Tenants t ON l.CustomerId = t.id " +
                          "LEFT JOIN LoanTypes lt ON l.TypeId = lt.Id " +
                          "WHERE (l.IsDeleted IS NULL OR l.IsDeleted = 0) AND (s.AccountId = " + accountId + ") AND (" +
                          "l.LoanId LIKE '%" + keyword + "%' OR l.ItemNameId LIKE '%" + keyword + "%' OR l.Description LIKE '%" + keyword + "%' " +
                          "OR s.Name LIKE '%" + keyword + "%' OR c.Name LIKE '%" + keyword + "%' OR t.Name LIKE '%" + keyword + "%' OR lt.Name LIKE '%" + keyword + "%')";

            return db.Database.SqlQuery<Loan>(query).ToList();
        }
        #endregion
    }
}
