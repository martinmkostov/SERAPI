﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class AccountModuleRepository
    {
        private string connString = AppSettings.ConnectionString;
        private SERAppDBContext db;
        public AccountModuleRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public AccountModule Get(int id)
        {
            return db.AccountModules
                .Include("Account")
                .Include("Module")
                .Where(c => c.Id == id && c.Module.IsActive == true)
                .SingleOrDefault();
        }

        public List<AccountModule> GetAll()
        {
            return db.AccountModules
                .Include("Account")
                .Include("Module")
                .ToList();
        }

        public List<AccountModule> GetModulesByAccountId(int accountId) {
            return db.AccountModules
                .Include("Module")
                .Where(a => a.AccountId == accountId && a.Module.IsActive == true)
                .ToList();
        }

        public void SaveAllActiveModuleToAccount(int accountId) {
            var data = db.Modules.Where(m => m.IsActive == true).ToList();
            foreach (var module in data) {
                SaveAccountModule(accountId, module.Id);
            };
        }

        public void SaveAccountModule(int accountId, int moduleId)
        {
            AccountModule newAccountModule = new AccountModule()
            {
                AccountId = accountId,
                ModuleId = moduleId,
                CreatedDate = DateTime.Now,
            };
            db.AccountModules.Add(newAccountModule);
            db.SaveChanges();
        }

        public void SaveAccountModule(AccountModuleModel model) {
            AccountModule newAccountModule = new AccountModule()
            {
                AccountId = model.AccountId,
                ModuleId = model.ModuleId,
                CreatedDate = DateTime.Now,
            };
            db.AccountModules.Add(newAccountModule);
            db.SaveChanges();
        }

        public void DeleteAccountModule(AccountModuleModel model)
        {
            var accountModule = db.AccountModules.Where(l => l.AccountId == model.AccountId && l.ModuleId == model.ModuleId).FirstOrDefault();
            if (accountModule != null)
            {
                db.AccountModules.Remove(accountModule);
                db.SaveChanges();
            }
        }
    }
}
