﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SERApp.Models.Common;
using SERApp.Models;
using static SERApp.Models.Enums.IncidentStatisticsEnum;
using System.Drawing;
using SERApp.Models.Constants;

namespace SERApp.Repository.Repositories
{
    public class IncidentRepository : Repository<Incident>, IRepository<Incident>, IIncidentRepository
    {
        private IRepository<Type1> _type1Repository;
        private IRepository<Type2> _type2Repository;
        private IRepository<Type3> _type3Repository;
        private IRepository<Incident> _incidentRepository;
        public IncidentRepository()
        {
            _type1Repository = new Repository<Type1>();
            _type2Repository = new Repository<Type2>();
            _type3Repository = new Repository<Type3>();
            _incidentRepository = new Repository<Incident>();
        }
        public IEnumerable<IncidentListModel> GetAllForList(int siteId, int? accountId, DateTime? date, DateTime? endDate)
        {
            string query = string.Empty;

            var datequery = "";
            var accountquery = "";
           

            if (accountId.HasValue)
            {
                accountquery = $"WHERE i.AccountId = {accountId}";
            }

            if (date.HasValue)
            {
                datequery += $"AND Cast(Time as DATE)>= '{date.Value.ToString("yyyy-MM-dd")}' AND Cast(Time as DATE)<= '{endDate.Value.ToString("yyyy-MM-dd")}'";
            }

            if (siteId == 0)
            {

                query = "SELECT" +
                               " i.Id as Id," +
                               " i.Time as Date," +
                               " i.Time as Time," +
                               " t1.Id as Type1Id," +
                               " t1.Name as Type1Name," +
                               " t2.Id as Type2Id," +
                               " t2.Name as Type2Name," +
                               " t3.Id as Type3Id," +
                               " t3.Name + CASE t.Name WHEN null THEN '' ELSE ' - ' + t.Name END as Type3Name," +
                               " i.Quantity as Q," +
                               " i.PoliceResponse as P," +
                               " i.AccountId as AccountId," +
                               " i.SiteId as SiteId," +
                               " s.Name as SiteName," +
                               " i.ActionDescription as Description," +
                               " (SELECT TOP 1 Field_Values.[Value] FROM Field_Values LEFT JOIN Type1_Fields ON Type1_Fields.FieldId = Field_Values.Type1_FieldId WHERE Type1Id = i.Type1Id AND IncidentId = i.Id AND Type1_Fields.Label = 'Closed From') as ClosedFrom,"+
                               " (SELECT TOP 1 Field_Values.[Value] FROM Field_Values LEFT JOIN Type1_Fields ON Type1_Fields.FieldId = Field_Values.Type1_FieldId WHERE Type1Id = i.Type1Id AND IncidentId = i.Id AND Type1_Fields.Label = 'Closed To') as ClosedTo"+
                               " FROM Incidents i" +
                               " LEFT JOIN Type1 t1 ON i.Type1Id = t1.Id" +
                               " LEFT JOIN Type2 t2 ON i.Type2Id = t2.Id" +
                               " LEFT JOIN Type3 t3 ON i.Type3Id = t3.Id" +
                               " INNER JOIN Sites s ON i.SiteId = s.Id" +
                               " FULL JOIN Tenants t ON t.Id = i.TenantId" +
                               $" {accountquery} {datequery}";
            }
            else
            {
                query = "SELECT" +
                               " i.Id as Id," +
                               " i.Time as Date," +
                               " i.Time as Time," +
                               " t1.Id as Type1Id," +
                               " t1.Name as Type1Name," +
                               " t2.Id as Type2Id," +
                               " t2.Name as Type2Name," +
                               " t3.Id as Type3Id," +
                               " t3.Name + CASE t.Name WHEN null THEN '' ELSE ' - ' + t.Name END as Type3Name," +
                               " i.Quantity as Q," +
                               " i.PoliceResponse as P," +
                               " i.AccountId as AccountId," +
                               " i.SiteId as SiteId," +
                               " s.Name as SiteName," +
                               " i.ActionDescription as Description," +
                               " (SELECT TOP 1 Field_Values.[Value] FROM Field_Values LEFT JOIN Type1_Fields ON Type1_Fields.FieldId = Field_Values.Type1_FieldId WHERE Type1Id = i.Type1Id AND IncidentId = i.Id AND Type1_Fields.Label = 'Closed From') as ClosedFrom," +
                               " (SELECT TOP 1 Field_Values.[Value] FROM Field_Values LEFT JOIN Type1_Fields ON Type1_Fields.FieldId = Field_Values.Type1_FieldId WHERE Type1Id = i.Type1Id AND IncidentId = i.Id AND Type1_Fields.Label = 'Closed To') as ClosedTo" +
                               " FROM Incidents i" +
                               " LEFT JOIN Type1 t1 ON i.Type1Id = t1.Id" +
                               " LEFT JOIN Type2 t2 ON i.Type2Id = t2.Id" +
                               " LEFT JOIN Type3 t3 ON i.Type3Id = t3.Id" +
                               " INNER JOIN Sites s ON i.SiteId = s.Id" +
                               " FULL JOIN Tenants t ON t.Id = i.TenantId" +
                               $" WHERE i.SiteId = {siteId} {datequery}";
            }

            var data = dbContext.Database.SqlQuery<IncidentListModel>(query).ToList();

            var incidentIds = "(";
            int xr = 0;
            data.Select(x=>x.Id).Distinct().ToList().ForEach(x => 
            {
                if (xr == 0)
                {
                    incidentIds += "" + x;

                }
                else
                {
                    incidentIds += "," + x;
                }
               
                xr++;
            });
            incidentIds += ")";

            var guardsQuery = "SELECT g.*,ig.IncidentId FROM Guards" +
                        " g LEFT JOIN IncidentGuard ig ON g.Id = ig.GuardId" +
                        $" WHERE ig.IncidentId IN {incidentIds}";

            if (data.Any())
            {
                var guards = dbContext.Database.SqlQuery<GuardIncidentModel>(guardsQuery).ToList();

                data.ForEach(x =>
                {
                    x.Guards = guards.Where(r => r.IncidentId == x.Id);
                });
            }
                     
            return data;
            
        }

        public IEnumerable<Incident> GetAllIncidentsChildIncluded(int siteId)
        {
            if (siteId == 0)
            {
                return dbContext.Incidents.Include(x => x.Type2)
                              .Include(x => x.Type1)
                              .Include(x => x.Type3)
                              .Include(x => x.IncidentGuard)
                              .Include(x => x.Tenant)
                              .Include(x => x.Tenant.Site)
                              .Include(x => x.IncidentGuard.Select(y => y.Guard))
                              .Include(x => x.Reports)
                              .Include(x => x.Reports.Select(y => y.ReportType))                              
                              .ToList();
            }

            return dbContext.Incidents.Include(x => x.Type2)
                .Include(x => x.Type1)      
                .Include(x=>x.Type3)
                .Include(x => x.IncidentGuard)
                .Include(x => x.Tenant)     
                .Include(x=>x.Tenant.Site)
                .Include(x => x.IncidentGuard.Select(y => y.Guard))
                .Include(x=>x.Reports)
                .Include(x=>x.Reports.Select(y=>y.ReportType))
                .Where(x=>x.SiteId == siteId)
                .ToList();
        }

        public Incident GetWithChildsIncluded(int id)
        {
                     
            return dbContext.Incidents.Include(x => x.Type2)
            .Include(x => x.Type1)
            .Include(x => x.Type3)
            .Include(x => x.IncidentGuard)
            .Include(x => x.Tenant)
            .Include(x => x.Tenant.Site)
            .Include(x => x.IncidentGuard.Select(y => y.Guard))
            .Include(x => x.Reports)
            .Include(x => x.Reports.Select(y => y.ReportImages))
            .Include(x => x.Reports.Select(y => y.ReportType))             
             .SingleOrDefault(x=>x.Id == id);
        }

        public string GetImage(int order, int id)
        {
            var query = $"SELECT image{order} FROM Incidents WHERE Id = {id}";
            return dbContext.Database.SqlQuery<string>(query).FirstOrDefault();
        }

        public Incident GetWithChildsIncludedV2(int id)
        {
            var query = $"SELECT * FROM Incidents WHERE Id = {id}";
            var data = dbContext.Database.SqlQuery<Incident>(query).FirstOrDefault();

            var type1s = $"SELECT * FROM Type1 WHERE AccountId = {data.AccountId}";
            var type2s = $"SELECT * FROM Type2 WHERE AccountId = {data.AccountId}";
            var type3s = $"SELECT * FROM Type3 WHERE AccountId = {data.AccountId}";

            var incidentGuards = $"SELECT * FROM IncidentGuard WHERE IncidentId = {id}";
            var tenants = $"SELECT * FROM Tenants WHERE SiteId = {data.SiteId}";

            var tenantsData = dbContext.Database.SqlQuery<Tenant>(tenants).ToList();
            var guardsData = new List<Guard>();
            if (tenantsData.Any())
            {
                var idsstring = string.Join(",", tenantsData.Select(x => x.SiteId.ToString()).ToArray());
                var sites = $"SELECT * FROM Sites WHERE Id IN ({idsstring})";

                var sitesData = dbContext.Database.SqlQuery<Site>(sites).ToList();

                var siteidsstring = string.Join(",", sitesData.Select(x => x.Id.ToString()).ToArray());
                var guards = $"SELECT * FROM Guards WHERE SiteId IN ({siteidsstring})";
                 guardsData = dbContext.Database.SqlQuery<Guard>(guards).ToList();
            }

            var type1Data = dbContext.Database.SqlQuery<Type1>(type1s).ToList();
            var type2Data = dbContext.Database.SqlQuery<Type2>(type2s).ToList();
            var type3Data = dbContext.Database.SqlQuery<Type3>(type3s).ToList();
            var incidentGuardData = dbContext.Database.SqlQuery<IncidentGuard>(incidentGuards).ToList();
         


            data.Type1 = type1Data.SingleOrDefault(x => x.Id == data.Type1Id);
            data.Type2 = type2Data.SingleOrDefault(x => x.Id == data.Type2Id);
            data.Type3 = type3Data.SingleOrDefault(x => x.Id == data.Type3Id);

            data.Tenant = tenantsData.SingleOrDefault(x => x.Id == data.TenantId);
            data.IncidentGuard = incidentGuardData.Where(x => x.IncidentId == data.Id).ToList();
            data.IncidentGuard.ToList().ForEach(x => 
            {
                x.Guard = guardsData.SingleOrDefault(r => r.Id == x.GuardId);
            });

            return data;
        }

        public List<Type1FieldValue> GetType1FieldValue(int id)
        {
            var query = $"SELECT * FROM Field_Values WHERE IncidentId = {id}";
            var data = dbContext.Database.SqlQuery<Type1FieldValue>(query).ToList();

            return data;
        }

        public IncidentStatisticsModel GetStatistics(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            return GetStatsByCategory(start, end, model);
        }

        public IncidentStatisticsModel GetStatsByMonthComparison(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };

            var dates = new List<DateTime>();

            string type = string.Empty;
            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            var monthCtr = 0;
            while(monthCtr < 12)
            {
                var date = DateTime.MinValue.AddMonths(monthCtr);
                stats.labels.Add(date.ToString("MMMM"));
                monthCtr++;
            }

            var currYear = start.Date;
            dates.Add(currYear);
            while (currYear.Date <= end.Date)
            {
                currYear = currYear.AddYears(1);
                dates.Add(currYear);
            }


            var groupedQuery = CountByDateQuery(dates.First(), dates.Last(), model.AccountId, model.SiteId, "MMMM yyyy", type, model.SelectedTypes);
            var idQuery = IdQuery(model.SelectedTypes, type);

            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();
            var typeGrouped = dbContext.Database.SqlQuery<IncidentStatsGrouped>(groupedQuery).ToList();

            var index = 0;
            dates.RemoveAt(dates.Count - 1);
            foreach (var row in dates)
            {
                var values = new List<int>();
                values = stats.labels.Select(date => typeGrouped.FirstOrDefault(e => e.Date == $"{date} {row:yyyy}")?.Count ?? 0).ToList();
                stats.datasets.Add(new Datasets { data = values, label = $"{row:yyyy}" });
                index++;
            }

            stats.title = $"{typeInfos.First().Name} ({start:yyyy} to {end:yyyy})";
            return stats;
        }

        public IncidentStatisticsModel GetStatsByDate(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };
            string format = string.Empty;
            string type = string.Empty;
            var dates = new List<DateTime>();
            var labels = new List<string>();

            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            switch ((DateType)model.DateType)
            {
                case DateType.DAY:
                    format = "yyyy-MM-dd";
                    var currDay = start.Date;
                    dates.Add(currDay);
                    while (currDay.Date <= end.Date)
                    {
                        labels.Add(currDay.ToString("yyyy-MM-dd"));

                        currDay = currDay.AddDays(1);
                        dates.Add(currDay.Date);
                    }
                    //groupedQuery = ByDayQuery(dates.First(), dates.Last(), accountId, siteId);
                    break;
                case DateType.MONTH:
                    format = "yyyy-MM";
                    var currMonth = start.Date;
                    dates.Add(currMonth);
                    while (currMonth.Date <= end.Date)
                    {
                        labels.Add($"{currMonth:MMMM} ({currMonth:yyyy})");

                        currMonth = currMonth.AddMonths(1);
                        dates.Add(currMonth.Date);
                    }
                    //groupedQuery = ByMonthQuery(dates.First(), dates.Last(), accountId, siteId);
                    break;
                case DateType.YEAR:
                    format = "yyyy";
                    var currYear = start.Date;
                    dates.Add(currYear);
                    while (currYear.Date <= end.Date)
                    {
                        labels.Add(currYear.ToString("yyyy"));

                        currYear = currYear.AddYears(1);
                        dates.Add(currYear);
                    }
                    break;
                default:
                    break;
            }

            stats.labels = labels;

            var groupedQuery = CountByGroupDateQuery(dates.First(), dates.Last(), model.AccountId, model.SiteId, format, type);
            var idQuery = IdQuery(model.SelectedTypes, type);

            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();
            var typeGrouped = dbContext.Database.SqlQuery<IncidentStatsGrouped>(groupedQuery).ToList();

            var index = 0;
            dates.RemoveAt(dates.Count - 1);
            foreach (var row in typeInfos)
            {
                var values = new List<int>();
                values = dates.Select(date => typeGrouped.FirstOrDefault(e => e.Id == row.Id && e.Date == date.ToString(format))?.Count ?? 0).ToList();
                stats.datasets.Add(new Datasets { data = values, label = row.Name, backgroundColor = new Colors().Get(index) });
                index++;
            }

            stats.title = $"{string.Join(", ", typeInfos.Select(e => e.Name))} ({start.ToString(format)} to {end.ToString(format)})";
            return stats;
        }

        public IncidentStatisticsModel GetStatsByCategory(DateTime start, DateTime end, DailyReportStatisticsRequestModel model)
        {
            var idQuery = "";
            var type = "";
            var format = "";
            var actualEnd = end;
            switch ((DataType)model.DataType)
            {
                case DataType.Vad:
                    type = "Type1";
                    break;
                case DataType.Var:
                    type = "Type2";
                    break;
                case DataType.Vem:
                    type = "Type3";
                    break;
            }

            switch ((DateType)model.DateType)
            {
                case DateType.DAY:
                    format = "yyyy-MM-dd";
                    break;
                case DateType.MONTH:
                    format = "MMMM yyyy";
                    actualEnd = end.AddMonths(1);
                    break;
                case DateType.YEAR:
                    format = "yyyy";
                    actualEnd = end.AddYears(1);
                    break;
            }

            var isCompare = model.Start2.HasValue && model.End2.HasValue;


            var groupedQuery = CountByIdQuery(start, actualEnd, model.AccountId, model.SiteId, model.SelectedTypes, type);

            idQuery = IdQuery(model.SelectedTypes, type);
            var typeInfos = dbContext.Database.SqlQuery<TypeInfo>(idQuery).ToList();

            var typeGrouped = new List<IncidentStatsByIdGrouped>();
            typeGrouped = dbContext.Database.SqlQuery<IncidentStatsByIdGrouped>(groupedQuery).ToList();

            var index = 0;

            var stats = new IncidentStatisticsModel
            {
                labels = new List<string>(),
                datasets = new List<Datasets>()
            };
            foreach (var row in typeInfos)
            {
                stats.labels.Add(row.Name);
                var values = new List<int>
                {
                    typeGrouped.FirstOrDefault(e => e.Id == row.Id)?.Count ?? 0
                };
                stats.datasets.Add(new Datasets { data = values, label = row.Name, backgroundColor = new Colors().Get(index) });
                index++;
            }
            stats.title = $"{string.Join(", ", typeInfos.Select(e => e.Name))} ({start.ToString(format)} to {end.ToString(format)})";
            return stats;
        }

        public string IdQuery(List<int> ids, string type) =>
            $@"SELECT DISTINCT t.Id, t.Name 
                FROM  {type} t
                WHERE t.Id in ({string.Join(",", ids)})";

        public string DateQuery(DateTime start, DateTime end) =>
            $@"i.time >= '{start:yyyy-MM-dd}' and i.time < '{end:yyyy-MM-dd}' ";

        public string CountByIdQuery(DateTime start, DateTime end, int accountId, int siteId, List<int> ids, string type) =>
            $@"SELECT       t.Id, Count(*) as 'Count' FROM INCIDENTS i
                JOIN        {type} t on t.id = i.{type}id
                WHERE       i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)} and t.Id in ({string.Join(",", ids)})
                GROUP BY    t.Id";

        public string CountByDateQuery(DateTime start, DateTime end, int accountId, int siteId, string format, string type, List<int> ids) =>
            $@"SELECT       Count(*) as 'Count', FORMAT(CAST(i.[Time] AS DATE), '{format}') as 'Date' FROM INCIDENTS i
                JOIN        {type} t on t.id = i.{type}id and t.Id in ({string.Join(",", ids)})
                WHERE       i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)}
                GROUP BY    FORMAT(CAST(i.[Time] AS DATE), '{format}')";

        public string CountByGroupDateQuery(DateTime start, DateTime end, int accountId, int siteId, string format, string type) =>
            $@"SELECT FORMAT(CAST(i.[Time] AS DATE), '{format}') as 'Date', t.Id, COUNT(*) as 'Count' from INCIDENTS i
                JOIN {type} t on t.id = i.{type}id
                WHERE i.accountid={accountId} and (i.SiteId = {siteId} or {siteId} = 0) and {DateQuery(start, end)}
                GROUP BY FORMAT(CAST(i.[Time] AS DATE), '{format}'), t.Id";

        public List<dynamic> GetMinMaxStatsDate(int accountId, int siteId)
        {
            List<dynamic> values = new List<dynamic>();
            DateTime start = dbContext.Database.SqlQuery<DateTime?>($"SELECT TOP 1 Time FROM Incidents WHERE AccountId = {accountId} and (SiteId = {siteId} or {siteId} = 0) ORDER BY Time").First() ?? DateTime.Now;
            DateTime end = DateTime.Now;

            values.Add(new
            {
                DateMode = $"{start:yyyy-MM-dd}",
                MonthMode = $"{start:yyyy-MM}",
                YearMode = Convert.ToInt32($"{start:yyyy}")
            });
            values.Add(new
            {
                DateMode = $"{end:yyyy-MM-dd}",
                MonthMode = $"{end:yyyy-MM}",
                YearMode = Convert.ToInt32($"{end:yyyy}")
            });

            return values;
        }

        public void FixImages()
        {
            var query = $@"select ri.*, r.IncidentId from ReportImage ri 
                        join Reports r on r.Id = ri.ReportId
                        where image is not null";
            var images = dbContext.Database.SqlQuery<IncidentImages>(query).ToList();
            var groupByIncident = images.GroupBy(e => e.IncidentId);

            foreach(var group in groupByIncident)
            {

                var counter = 1;
                foreach(var image in group.ToList())
                {
                    var q = $@"UPDATE Incidents
                            set Image{counter}='{image.Image}'
                            where Id={group.Key}";
                    var res = dbContext.Database.ExecuteSqlCommand(q);
                    counter++;
                }
            }
        }

        public class IncidentImages: ReportImage
        {
            public int IncidentId { get; set; }
        }
    }
}
