﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class ContractRepository: Repository<Contract>, IContractRepository
    {
        public ContractRepository()
        {
        }

        public List<ContractModel> GetAllContractsForExport(ContractFilters filters)
        {
            var subQueryRamvtal = filters.Ramvtal == "0" ? $" and (Ramvtal = {filters.Ramvtal == "1"})": "";
            var subQueryLegalHolder = filters.LegalHolderId == 0 ? $" and (LegalHolderId = {filters.LegalHolderId})": "";
            var subQueryCategory = filters.CategoryId == 0 ? $" and (CategoryId = {filters.CategoryId})": "";
            var subQueryActive = filters.Active.HasValue ? $" and Active = {filters.Active.Value}" : "";
            var subQueryRemind = filters.isRemindList ? $" and RemindDate <= '{DateTime.Now:yyyy-MM-dd}'" : "";
            var subQueryTagWord = filters.TagWordId != 0 ? $"JOIN ContractTagWord ctw on ctw.contractId = c.Id " : "";
            var subQuerySearch = !string.IsNullOrEmpty(filters.SearchText) ? $" and (ContractNo LIKE '%{filters.SearchText}%' or OpponentHolder LIKE '%{filters.SearchText}%')" : "";

            var query = $@"SELECT c.Id, OpponentHolder, ContractNo
                            FROM Contracts c 
                            {subQueryTagWord}
                            WHERE SiteId={filters.SiteId} and AccountId={filters.AccountId} {subQueryRamvtal} {subQueryLegalHolder} {subQueryCategory} {subQueryActive} {subQueryRemind} {subQuerySearch}";
            return dbContext.Database.SqlQuery<ContractModel>(query).ToList();
        }

        public List<ContractModel> GetContractsByIds(string ids)
        {
            var query = $@"SELECT Id, OpponentHolder, ContractNo
                            FROM Contracts
                            WHERE Id in ({ids})";
            return dbContext.Database.SqlQuery<ContractModel>(query).ToList();
        }
    }
}
