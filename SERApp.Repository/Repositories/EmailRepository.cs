﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.Entity;
using SERApp.Models.Common;

namespace SERApp.Repository.Repositories
{
    public class EmailRepository
    {
        private string connString = AppSettings.ConnectionString;
        private string _superAdminOverride = ConfigurationManager.AppSettings["superadminEmailTemplateOverride"];

        private SERAppDBContext db;
        private bool superAdminOverride = false;
        public EmailRepository()
        {
            db = new SERAppDBContext(connString);
            superAdminOverride = !string.IsNullOrEmpty(_superAdminOverride) ? bool.Parse(_superAdminOverride) : false;
        }

        public EmailTemplate GetEmailTemplateFromRoutineName(string routineName)
        {
            var tmp = db.EmailTemplates.Where(x => x.RoutineName.ToLower() == routineName.ToLower());
            if (tmp.Count() > 1)
            {
                db.EmailTemplates.Remove(tmp.FirstOrDefault());
                db.SaveChanges();


            }

            return db.EmailTemplates.SingleOrDefault(x => x.RoutineName.ToLower() == routineName.ToLower());
        }

        public EmailTemplate GetTranslantionByEmailTemplateId(int id)
        {
            return db.EmailTemplates.SingleOrDefault(x => x.Id == id);
        }

        public EmailTemplate GetEmailTemplate(int id) {
            return db.EmailTemplates.Where(e => e.Id == id).SingleOrDefault();
        }

        public AccountEmailTemplate GetAccountEmailTemplate(int id) {
            return db.AccountEmailTemplates.Where(e => e.Id == id).SingleOrDefault();
        }

        public List<EmailTemplate> GetAllEmailTemplates() {
            return db.EmailTemplates.Include(x=>x.EmailTemplateVariables).ToList();
        }
        //EmailTemplatesTranslation are only for System Admins or will be used for default set up
        //If the account does not have any translation for the email template for the said routine
        //It can just use the default one
        //public List<EmailTemplatesTranslation> GetAllEmailTemplatesByLanguage(int languageId)
        //{
        //    return db.EmailTemplatesTranslations.Where(et => et.LanguageId == languageId).ToList();
        //}

        public List<EmailTemplate> GetAllNonSystemEmailTemplates()
        {
            return db.EmailTemplates.Where(e => e.IsSystem == false).ToList();
        }

        public List<AccountEmailTemplate> GetAllAccountEmailTemplateById(int accountId) {
            return db.AccountEmailTemplates.Where(a => a.AccountId == accountId).ToList();   
        }
        //AccountEmailTemplate contains languageid for filtering
        public List<AccountEmailTemplate> GetAllAccountEmailLanguageId(int accountId, int languageId)
        {
            return db.AccountEmailTemplates.Where(a => a.AccountId == accountId && a.LanguageId == languageId).ToList();
        }

        public EmailTemplate GetEmailTemplateTranslation(int emailTemplateId, int languageId)
        {
            return db.EmailTemplates.Where(a => a.Id == emailTemplateId && a.LanguageId == languageId).SingleOrDefault();
        }

        public AccountEmailTemplate GetAccountEmailTemplate(string routineName, int accountId) {
            var emailTemplate = db.EmailTemplates.Where(e => e.RoutineName == routineName).SingleOrDefault();
            if (emailTemplate != null)
            {
                //IF superAdminOverwrite is TRUE, directly use the superadmin template
                if (superAdminOverride) {
                    return new AccountEmailTemplate()
                    {
                        Id = 0,
                        EmailTemplateId = emailTemplate.Id,
                        Subject = emailTemplate.Subject,
                        Body = emailTemplate.Body,
                        SMSBody = emailTemplate.SMSBody
                    };
                }
                if (accountId == 0)
                {
                    var emailTemplateTranslate = db.EmailTemplates.Where(e => e.Id == emailTemplate.Id && e.LanguageId == 1).FirstOrDefault();
                    if (emailTemplateTranslate != null)
                    {
                        return new AccountEmailTemplate()
                        {
                            Id = 0,
                            EmailTemplateId = emailTemplateTranslate.Id,
                            Subject = emailTemplateTranslate.Subject,
                            Body = emailTemplateTranslate.Body,
                            SMSBody = emailTemplateTranslate.SMSBody
                        };
                    }
                    else {
                        return new AccountEmailTemplate()
                        {
                            Id = 0,
                            EmailTemplateId = emailTemplate.Id,
                            Subject = emailTemplate.Subject,
                            Body = emailTemplate.Body,
                            SMSBody = emailTemplate.SMSBody
                        };
                    }
                }
                else {
                    var accountEmailTemplate = db.AccountEmailTemplates.Where(a => a.AccountId == accountId && a.EmailTemplateId == emailTemplate.Id).SingleOrDefault();
                    if (accountEmailTemplate != null)
                    {
                        return new AccountEmailTemplate()
                        {
                            Id = accountEmailTemplate.Id,
                            EmailTemplateId = accountEmailTemplate.EmailTemplateId,
                            Subject = accountEmailTemplate.Subject,
                            Body = accountEmailTemplate.Body,
                            SMSBody = accountEmailTemplate.SMSBody
                        };
                    }
                    else
                    {
                        return new AccountEmailTemplate()
                        {
                            Id = 0,
                            EmailTemplateId = emailTemplate.Id,
                            Subject = emailTemplate.Subject,
                            Body = emailTemplate.Body,
                            SMSBody = emailTemplate.SMSBody
                        };
                    }
                }
                
            }
            return null;
            
        }

        public List<Language> GetAllLanguages() {
            return db.Languages.ToList();
        }

        public void SaveEmailTemplate(EmailTemplateTranslationModel model)
        {
            if (superAdminOverride)
            {
                //Use the SaveEmailTemplate for SUPERADMIN only
                SaveEmailTemplate(new EmailTemplateModel()
                {
                    Id = model.EmailTemplateId,
                    RoutineName = model.RoutineName,
                    Description = model.Description,
                    Subject = model.Subject,
                    Body = model.Body,
                    SMSBody = model.SMSBody,
                    IsSystem = model.IsSystem
                });
            }
            else {
                var data = db.EmailTemplates.Where(e => e.Id == model.EmailTemplateId && e.LanguageId == model.LanguageId).SingleOrDefault();
                if (data != null)
                {
                    data.LanguageId = model.LanguageId;
                    data.Subject = model.Subject;
                    data.Body = model.Body;
                    data.SMSBody = model.SMSBody;
                }
                else
                {
                    var emailTemplate = new EmailTemplate()
                    {
                        Id = model.EmailTemplateId,
                        Subject = model.Subject,
                        Body = model.Body,
                        SMSBody = model.SMSBody,
                        LanguageId = model.LanguageId == 0 ? 1 : model.LanguageId
                    };
                    db.EmailTemplates.Add(emailTemplate);
                }
            }
            db.SaveChanges();
        }
        //NOT USED, CURRENTLY USING THE TABLE WITH THE Translation
        public void SaveEmailTemplate(EmailTemplateModel model) {
            var data = db.EmailTemplates.Where(e => e.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.RoutineName = model.RoutineName;
                data.Description = model.Description;
                data.Subject = model.Subject;
                data.Body = model.Body;
                data.IsSystem = model.IsSystem;
                data.SMSBody = model.SMSBody;
            }
            else {
                var emailTemplate = new EmailTemplate()
                {
                    RoutineName = model.RoutineName,
                    Description = model.Description,
                    Subject = model.Subject,
                    Body = model.Body,
                    SMSBody = model.SMSBody,
                    IsSystem = model.IsSystem
                    //LanguageId = 1 //DEFAULT FOR NOW
                };
                db.EmailTemplates.Add(emailTemplate);
            }
            db.SaveChanges();
        }

        public void SaveAccountEmailTemplate(AccountEmailTemplateModel model)
        {
            var data = db.AccountEmailTemplates.Where(e => e.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.Subject = model.Subject;
                data.Body = model.Body;
                data.SMSBody = model.SMSBody;
            }
            else
            {
                var accountEmailTemplate = new AccountEmailTemplate()
                {
                    EmailTemplateId = model.EmailTemplateId,
                    AccountId = model.AccountId,
                    Subject = model.Subject,
                    Body = model.Body,
                    SMSBody = model.SMSBody,
                    LanguageId = model.LanguageId == 0 ? 1 : model.LanguageId //DEFAULT FOR NOW
                };
                db.AccountEmailTemplates.Add(accountEmailTemplate);
            }
            db.SaveChanges();
        }
    }
}
