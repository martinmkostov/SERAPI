﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SERApp.Repository.Repositories
{
    public class IssueRecipientRepository : Repository<IssueRecipients>, IRepository<IssueRecipients>, IIssueRecipientRepository
    {
        public List<IssueRecipients> GetAllRecipients(int siteId)
        {
            return dbContext.IssueRecipients
                .Include(x => x.IssueRecipientType)              
                .Include(x=>x.Site)
                .Where(x=>x.SiteId == siteId)
                .ToList();
        }

        public List<IssueRecipientTypes> GetAllTypes()
        {
            return dbContext.IssueRecipientTypes.ToList();
        }
    }
}
