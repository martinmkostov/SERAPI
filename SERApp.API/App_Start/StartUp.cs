﻿using System.Text;
using Hangfire;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Owin;
using SERApp.Models.Common;

[assembly: OwinStartup(typeof(SERApp.API.StartUp))]

namespace SERApp.API
{
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage(AppSettings.ConnectionString);

            app.UseHangfireDashboard();
            app.UseHangfireServer();


            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "https://ser4api.azurewebsites.net/", //some string, normally web url,  
                        ValidAudience = "https://ser4api.azurewebsites.net/",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ser4secretkey5512"))
                    }
                });
            //new BackgroundJobServerOptions()
            //{

            //    Queues = new[] { "daily_report", "week_report", "month_report" }
            //}
        }
    }
}