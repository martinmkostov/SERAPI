﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SERApp.API.Models.ResponseModels
{
    public class ResponseIssueModel
    {
        public int IssueWebId { get; set; }
        public int IssueNumber { get; set; }
    }
}