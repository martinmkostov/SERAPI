﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
namespace SERApp.API.Extensions
{
    public class APIKeyMessageHandler : DelegatingHandler
    {
        private const string publicApiKey = "SER4webApi";
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken) 
        {
            if (httpRequestMessage.RequestUri.AbsolutePath == "/Issue/Create" || httpRequestMessage.RequestUri.AbsolutePath == "/v1/Task/Confirm")
            {
                bool validKey = false;
                IEnumerable<string> requestHeaders;
                var checkApiKeyExists = httpRequestMessage.Headers.TryGetValues("APIKey", out requestHeaders);
                if (checkApiKeyExists)
                {
                    var hashedKey = GetStringSha256Hash(publicApiKey);
                    if (requestHeaders.FirstOrDefault().Equals(hashedKey))
                    {
                        validKey = true;
                    }
                }

                if (!validKey)
                {
                    return httpRequestMessage.CreateResponse(System.Net.HttpStatusCode.Forbidden, "Invalid API Key");
                }
            }

            var response = await base.SendAsync(httpRequestMessage, cancellationToken);
            return response;
        }

        private string GetStringSha256Hash(string text)
        {
            if (String.IsNullOrEmpty(text))
                return String.Empty;

            using (var sha = new System.Security.Cryptography.SHA256Managed())
            {
                byte[] textData = System.Text.Encoding.UTF8.GetBytes(text);
                byte[] hash = sha.ComputeHash(textData);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }


        private static string ComputeHash(string hashedPassword, string message)
        {
            var key = System.Text.Encoding.UTF8.GetBytes(hashedPassword.ToUpper());
            string hashString;

            using (var hmac = new System.Security.Cryptography.HMACSHA256(key))
            {
                var hash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(message));
                hashString = Convert.ToBase64String(hash);
            }

            return hashString;
        }
    }

}