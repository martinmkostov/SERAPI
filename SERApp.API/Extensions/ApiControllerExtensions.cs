﻿using System;
using System.Net;
using System.Web.Http;

namespace SERApp.API.Extensions
{
    public static class ApiControllerExtensions
    {
        public static object ConsistentApiHandling(this ApiController controller, Func<object> getResult)
        {
            try
            {   
                return getResult();
            }
            catch (Exception ex)
            {
                return new SERApp.Service.Models.ResponseModels.ResponseModel
                {
                    ErrorCode = ex.HResult,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = ex.Message,
                    Success = false
                };
            }
        }
    }
}