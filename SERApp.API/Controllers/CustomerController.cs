﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.API.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SEPAppBlack.Controllers
{
    public class CustomerController : ApiController
    {
        private CustomerService _customerService;
        public CustomerController() {
            _customerService = new CustomerService();
        }

        [HttpGet]
        [Route("Customer/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _customerService.Get(id);
                return new ResponseDataModel<CustomerModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Customer/GetAll")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<CustomerModel>>))]
        public IHttpActionResult GetAll() {
            
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _customerService.GetAll();
                return new ResponseDataModel<List<CustomerModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Customer/GetAllByAccountId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<CustomerModel>>))]
        public IHttpActionResult GetAllByAccountId(int accountId,int siteId)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _customerService.GetAllByAccountId(accountId, siteId);
                return new ResponseDataModel<List<CustomerModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Customer/Save")]
        [Authorize]
        public IHttpActionResult SaveCustomer(CustomerModel model) {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _customerService.SaveCustomer(model);
                return new ResponseDataModel<CustomerModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Customer Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Customer/Delete")]
        [Authorize]
        public IHttpActionResult DeleteCustomer(CustomerModel model)
        {
            
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value =_customerService.DeleteCustomer(model.Id);
                return new ResponseDataModel<CustomerModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Deleted Customer Record" : "Cannot delete! Customer has an active loan. "
                };
            }));
        }
    }
}