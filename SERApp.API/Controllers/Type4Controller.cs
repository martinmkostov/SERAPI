﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class Type4Controller : ApiController
    {
        private Type4Service _service;

        public Type4Controller()
        {
            _service = new Type4Service();
        }

        [HttpGet]
        [Route("Type4/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetType4ById(id);
                return new ResponseDataModel<Type4Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Type Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Type4/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllType4(accountId, siteId).OrderBy(x => x.Name);
                return new ResponseDataModel<IEnumerable<Type4Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Type4/Save")]
        [Authorize]
        public IHttpActionResult Save(Type4Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.AddType4(model);
                return new ResponseDataModel<Type4Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Saved Custom Type" : "Type 2 already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Type4/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = _service.DeleteType4ById(id);
                var message = success ? "Successfully Deleted Type2" : "Cannot delete type2 because it is associated to a daily report.";
                return new ResponseDataModel<Type2Model>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,
                };
            }));
        }
    }
}