﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class SchedulerController : ApiController
    {
        public SchedulerController()
        {

        }

        [HttpPost]
        [Route("Scheduler/Queue")]
        [Authorize]
        public void Enqueue(string to, string subject, string body)
        {
            BackgroundJob.Enqueue<EmailSender>(e => e.Send(to, subject, body));
        }

        [HttpPost]
        [Route("Scheduler/Recuring")]
        [Authorize]
        public void Recuring(string to, string subject, string body)
        {
            RecurringJob.AddOrUpdate<EmailSender>(e => e.Send(to, subject, body), Cron.MinuteInterval(5));
        }
    }

    internal class EmailSender
    {
        public void Send(string to, string subject, string body)
        {
            Service.Tools.Email.SendEmail(new SERApp.Models.Common.EmailModel(0)
            {
                From = ConfigurationManager.AppSettings["from"],
                To = to,
                Subject = subject,
                Body = body
            });
        }
    }
}
