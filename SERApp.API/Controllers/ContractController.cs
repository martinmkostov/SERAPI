﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class ContractController : ApiController
    {
        private IContractService _service;
        public ContractController()
        {
            _service = new ContractService();
        }

        [HttpGet]
        [Route("Contract/GetExcelBySiteId")]
        [Authorize]
        public HttpResponseMessage GetExcelBySiteId(int siteId, int accountId = 0,int userId =0, string searchValue = "")
        {
            var excel = _service.GetXls(siteId, accountId, userId, searchValue);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "s.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        [HttpPost]
        [Route("Contract/GetContracts")]
        [Authorize]
        public IHttpActionResult GetContracts([FromBody]ContractFilters filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                filter.isRemindList = !filter.isRemindList;
                var data = _service.GetContracts(filter);

                var count = data.Count();                
                return new ResponseDataModel<IEnumerable<ContractModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/GetTagWordById")]
        [Authorize]
        public IHttpActionResult GetTagWordById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagWordById(id);
                return new ResponseDataModel<TagWordModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tag Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/GetContractRoleById")]
        [Authorize]
        public IHttpActionResult GetContractRoleById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetContractRoleById(id);
                return new ResponseDataModel<ContractRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tag Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/DownloadReport")]
        [Authorize]
        public HttpResponseMessage DownloadReport(int id)
        {

            var sendReport = new SendReport();

            return sendReport.DownloadContractDetails(id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));

        }


        [HttpGet]
        [Route("Contract/GetTagWords")]
        [Authorize]
        public IHttpActionResult GetTagWords(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _service.GetTagWords(accountId).OrderBy(x=>x.Name, StringComparer.Create(se, false));
                return new ResponseDataModel<IEnumerable<TagWordModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tag Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/AddContractRole")]
        [Authorize]
        public IHttpActionResult AddTagWord([FromBody]ContractRoleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                model.Id = _service.AddContractRole(model);
                return new ResponseDataModel<ContractRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Contract/AddTagWord")]
        [Authorize]
        public IHttpActionResult AddTagWord([FromBody]TagWordModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddTagWord(model);
                return new ResponseDataModel<TagWordModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Contract/GetContract")]
        [Authorize]
        public IHttpActionResult GetContract(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetContract(id);
                return new ResponseDataModel<ContractModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/GetFiles")]
        [Authorize]
        public IHttpActionResult GetFiles(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetFiles(id);
                return new ResponseDataModel<IEnumerable<ContractFilesModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Contract/AddContract")]
        [Authorize]
        public IHttpActionResult AddContract([FromBody]ContractModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                model.ValidFrom = new DateTime(model.ValidFromModel.year, model.ValidFromModel.month, model.ValidFromModel.day);
                model.ValidTo = new DateTime(model.ValidToModel.year, model.ValidToModel.month, model.ValidToModel.day);
                model.RemindDate = new DateTime(model.RemindDateModel.year, model.RemindDateModel.month, model.RemindDateModel.day);
                model.CancelLatest = new DateTime(model.CancelLatestModel.year, model.CancelLatestModel.month, model.CancelLatestModel.day);

                var data = _service.AddContract(model);
                return new ResponseDataModel<ContractModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/AddLegalHolder")]
        [Authorize]
        public IHttpActionResult AddLegalHolder([FromBody]LegalHolderModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.AddLegalHolder(model);
                return new ResponseDataModel<LegalHolderModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/AddOpponent")]
        [Authorize]
        public IHttpActionResult AddOpponent([FromBody]OpponentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.AddOpponent(model);
                return new ResponseDataModel<OpponentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/AddContractCategory")]
        [Authorize]
        public IHttpActionResult AddContractCategory([FromBody]ContractCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.AddContractCategory(model);
                return new ResponseDataModel<ContractCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/GetContractTagWordsForCreate")]
        [Authorize]
        public IHttpActionResult GetContractTagWordsForCreate(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetContractTagWords(accountId);
                return new ResponseDataModel<IEnumerable<ContractTagWordModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/GetContractCategories")]
        [Authorize]
        public IHttpActionResult GetContractCategories([FromBody]dynamic filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

            
                var data = _service.GetAllContractCategory(Convert.ToInt32(filter.AccountId.Value), filter.IsActive != null ? Convert.ToBoolean(filter.IsActive.Value) : null);


               
                return new ResponseDataModel<IEnumerable<ContractCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }
        [HttpGet]
        [Route("Contract/GetCategory")]
        [Authorize]
        public IHttpActionResult GetCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetContractCategory(id);
                return new ResponseDataModel<ContractCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/DeleteContractCategory")]
        [Authorize]
        public IHttpActionResult DeleteContractCategory([FromBody]ContractCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteContractCategory(model.Id);
                return new ResponseDataModel<IEnumerable<ContractCategoryModel>>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    //Data = data
                };
            }));
        }


        //legal hodler

        [HttpPost]
        [Route("Contract/GetLegalHolders")]
        [Authorize]
        public IHttpActionResult GetLegalHolders([FromBody]dynamic filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetLegalHoldersByAccountId(Convert.ToInt32(filter.AccountId.Value), filter.IsActive !=null? Convert.ToBoolean(filter.IsActive.Value) : null);
                return new ResponseDataModel<IEnumerable<LegalHolderModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }
        [HttpGet]
        [Route("Contract/GetLegalHolder")]
        [Authorize]
        public IHttpActionResult GetLegalHolder(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetLegalHolder(id);
                return new ResponseDataModel<LegalHolderModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/DeleteLegalHolder")]
        [Authorize]
        public IHttpActionResult DeleteLegalHolder([FromBody]LegalHolderModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteLegalHolder(model.Id);
                return new ResponseDataModel<IEnumerable<LegalHolderModel>>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    //Data = data
                };
            }));
        }

        // opponents
        [HttpPost]
        [Route("Contract/GetOpponents")]
        [Authorize]
        public IHttpActionResult GetOpponents([FromBody]dynamic filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetOpponentsByAccountId(Convert.ToInt32(filter.AccountId.Value));
                return new ResponseDataModel<IEnumerable<OpponentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    Data = data
                };
            }));
        }
        [HttpGet]
        [Route("Contract/GetOpponent")]
        [Authorize]
        public IHttpActionResult GetOpponent(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetOpponent(id);
                return new ResponseDataModel<OpponentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/DeleteOpponent")]
        [Authorize]
        public IHttpActionResult DeleteOpponent([FromBody]OpponentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteOpponent(model.Id);
                return new ResponseDataModel<IEnumerable<OpponentModel>>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    //Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/DeleteTagWord")]
        [Authorize]
        public IHttpActionResult DeleteTagWord([FromBody]TagWordModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteTagWord(model.Id);
                return new ResponseDataModel<TagWordModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    //Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/DeleteContractRole")]
        [Authorize]
        public IHttpActionResult DeleteContractRole([FromBody]ContractRoleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteContractRole(model.Id);
                return new ResponseDataModel<ContractRoleModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Contracts",
                    //Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/Delete")]
        [Authorize]
        public IHttpActionResult Delete([FromBody]ContractModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteContract(model.Id);
                return new ResponseDataModel<IEnumerable<OpponentModel>>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Contracts",
                    //Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Contract/DownloadConractList")]
        [Authorize]
        public HttpResponseMessage DownloadConractList(int siteId)
        {
            var sendReport = new SendReport();
            return sendReport.DownloadDailyReport(DateTime.Now, DateTime.Now, "ContractList", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, 0, null);

        }

        [HttpGet]
        [Route("Contract/GetContractRoles")]
        [Authorize]
        public IHttpActionResult GetContractRoles(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _service.GetContractRoles(accountId).OrderBy(x => x.RoleName, StringComparer.Create(se, false));
                return new ResponseDataModel<IEnumerable<ContractRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tag Data",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Contract/SendFiles")]
        [Authorize]
        public IHttpActionResult SendFiles([FromBody]EmailModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.SendFiles(model);
                return new ResponseDataModel<LegalHolderModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Sent email successfully",
                  //  Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Contract/Copy")]
        public IHttpActionResult CopyContracts([FromBody] CopyContractModel model)
        {
            try
            {
                return Ok(_service.CopyContracts(model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Contract/ExportFiles")]
        public HttpResponseMessage ExportFiles(string filters)
        {
            var parse = JsonConvert.DeserializeObject<ContractFilters>(filters);
            var zip = _service.DownloadFiles(parse);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(zip)
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/zip");
            resp.Content.Headers.ContentLength = zip.Length;

            return resp;
        }

        [HttpPost]
        [Route("Contract/BulkUpdate")]
        public IHttpActionResult BulkUpdate([FromBody] UpdateContractYear model)
        {
            try
            {
                return Ok(_service.BulkUpdateYear(model));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
