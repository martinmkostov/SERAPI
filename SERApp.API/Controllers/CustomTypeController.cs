﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class CustomTypeController : ApiController
    {
        private Type2Service _service;
        public CustomTypeController()
        {
            _service = new Type2Service();
        }

        [HttpGet]
        [Route("CustomType/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetType2ById(id);
                return new ResponseDataModel<Type2Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Type Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("CustomType/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int id, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllType2(id, siteId).OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<Type2Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("CustomType/Save")]
        [Authorize]
        public IHttpActionResult Save(Type2Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.AddType2(model);
                return new ResponseDataModel<Type2Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Custom Type":"Type 2 already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("CustomType/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = _service.DeleteType2ById(id);
                var message = success ? "Successfully Deleted Type2" : "Cannot delete type2 because it is associated to a daily report.";
                return new ResponseDataModel<Type2Model>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,            
                };
            }));
        }
    }
}
