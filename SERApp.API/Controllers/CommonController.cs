﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class CommonController : ApiController
    {
        private CommonService _commonService;
        private ICompanyTypeService _companyTypeService;
        private AccountSettingsService _accountSettingService;
        public CommonController()
        {
            _commonService = new CommonService();
            _accountSettingService = new AccountSettingsService();
            _companyTypeService = new CompanyTypeService();
        }

        [HttpGet]
        public IHttpActionResult GetAllFacilites()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllFacilites();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully loaded All Facilities",
                    Data = data
                };
            }));
        }

        [HttpGet]
        public IHttpActionResult GetAllLoanTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllLoanTypes();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully loaded All Loan Types",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Common/GetAllCompanyTypes")]
        public IHttpActionResult GetAllCompanyTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllCompanyTypes().OrderBy(x=>x.Name).ToList();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Company Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllUserRoles")]
        public IHttpActionResult GetAllUserRoles()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllUserRoles();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All User Roles",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetDashboardCounters")]
        [ResponseType(typeof(ResponseDataModel<DashboardModel>))]
        public IHttpActionResult GetDashboardCounters()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetDashboardCounters();
                return new ResponseDataModel<DashboardModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Dashboard Counters",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetDashboardLogCounters")]
        [ResponseType(typeof(ResponseDataModel<DashboardLogModel>))]
        public IHttpActionResult GetDashboardLogCounters()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetDashboardLogCounters();
                return new ResponseDataModel<DashboardLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Dashboard Log Counters",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetMostRecentLogs")]
        [ResponseType(typeof(ResponseDataModel<LogModel>))]
        public IHttpActionResult GetMostRecentLogs()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetMostRecentLogs();
                return new ResponseDataModel<List<LogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Most Recent Logs",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetErrorLogs")]
        [ResponseType(typeof(ResponseDataModel<LogModel>))]
        public IHttpActionResult GetErrorLogs()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetErrorLogs();
                return new ResponseDataModel<List<LogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Error Logs",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllLanguages")]
        [ResponseType(typeof(ResponseDataModel<DropdownModel>))]
        public IHttpActionResult GetAllLanguages()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllLanguages();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Languages",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllActiveModules")]
        [ResponseType(typeof(ResponseDataModel<DropdownModel>))]
        public IHttpActionResult GetAllActiveModules()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllActiveModules();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Active Modules",
                    Data = data,
                    TotalRecords = 1
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllTenantTypes")]
        public IHttpActionResult GetAllTenantTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _commonService.GetAllTenantTypes(accountId).OrderBy(c => c.Name, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenant Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllPremiseTypes")]
        public IHttpActionResult GetAllPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _commonService.GetAllPremiseTypes(accountId).OrderBy(c => c.Name, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Premise Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllSubPremiseTypes")]
        public IHttpActionResult GetAllSubPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _commonService.GetAllSubPremiseTypes(accountId).OrderBy(c => c.Name, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Sub Premise Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetAllGetJobTitles")]
        public IHttpActionResult GetAllGetJobTitles(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _commonService.GetAllGetJobTitles(accountId).OrderBy(c => c.Name, StringComparer.Create(se, false)).ToList(); ;
                return new ResponseDataModel<List<DropdownModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Job Titles",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Common/GetSettings")]
        public IHttpActionResult GetSettings()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _commonService.GetAllConfigSettings();
                return new ResponseDataModel<List<ConfigSettingModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Account Settings",
                    Data = data
                };
            }));
        }

        [Route("Common/GetCompanyTypes")]
        public IHttpActionResult GetCompanyTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _companyTypeService.GetCompanyTypes().OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<CompanyTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Company Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Common/SaveCompanyType")]
        public IHttpActionResult SaveCompanyType(CompanyTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _companyTypeService.AddCompanyType(model);
                return new ResponseDataModel<IEnumerable<CompanyTypeModel>>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Loaded All Company Types":"Company Type Already exists",
                  //  Data = data
                };
            }));
        }

        [Route("Common/GetCompanyTypeById")]
        public IHttpActionResult GetCompanyTypeById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _companyTypeService.GetCompanyTypeById(id);
                return new ResponseDataModel<CompanyTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Company Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Common/DeleteCompanyType")]
        public IHttpActionResult DeleteCompanyType(CompanyTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                 _companyTypeService.DeleteCompanyType(model.Id);
                return new ResponseDataModel<IEnumerable<CompanyTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Company Types",
                    //  Data = data
                };
            }));
        }
    }
}