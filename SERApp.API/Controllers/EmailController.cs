﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Data.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class EmailController : ApiController
    {
        private EmailService _emailService;
        public EmailController()
        {
            _emailService = new EmailService();
        }

        public void test()
        {
            var secret = "nT2M8gPZs6YAshXa";

        }

        [HttpGet]
        [Route("Email/GetEmailTemplate")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<EmailTemplateModel>))]
        public IHttpActionResult GetEmailTemplate(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetEmailTemplate(id);
                return new ResponseDataModel<EmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllEmailTemplate")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<EmailTemplateTranslationModel>>))]
        public IHttpActionResult GetAllEmailTemplate(int id, int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllEmailTemplate(id, moduleId).OrderBy(x=>x.RoutineName).ToList();
                return new ResponseDataModel<List<EmailTemplateTranslationModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllNonSystemEmailTemplates")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<EmailTemplateModel>>))]
        public IHttpActionResult GetAllNonSystemEmailTemplates()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllNonSystemEmailTemplates();
                return new ResponseDataModel<List<EmailTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Non Systen Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Email/GetAllAccountEmailTemplate")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<AccountEmailTemplateModel>>))]
        public IHttpActionResult GetAllAccountEmailTemplate(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _emailService.GetAllAccountEmailTemplate(id);
                return new ResponseDataModel<List<AccountEmailTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Account Email Template Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Email/SaveEmailTemplate")]
        [Authorize]
        public IHttpActionResult SaveEmailTemplate(EmailTemplateTranslationModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SaveEmailTemplate(model);
                return new ResponseDataModel<EmailTemplateTranslationModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved EmailTemplateModel Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Email/SaveAccountEmailTemplate")]
        [Authorize]
        public IHttpActionResult SaveAccountEmailTemplate(AccountEmailTemplateModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SaveAccountEmailTemplate(model);
                return new ResponseDataModel<AccountEmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved AccountEmailTemplateModel Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Email/SendEmail")]
        [Authorize]
        public IHttpActionResult SendEmail(SendSER4UrlModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SendEmailURL(model);
                return new ResponseDataModel<AccountEmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved AccountEmailTemplateModel Record",
                    Data = null
                };
            }));
        }

        [HttpPost]
        [Route("Email/SendSupport")]
        [Authorize]
        public IHttpActionResult SendSupport(SupportEmailModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SendSupport(model);
                return new ResponseDataModel<AccountEmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved AccountEmailTemplateModel Record",
                    Data = null
                };
            }));
        }

        [HttpPost]
        [Route("Email/SendSMS")]
        [Authorize]
        public IHttpActionResult SendSMS(SendSER4UrlModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _emailService.SendSMSUrl(model);
                return new ResponseDataModel<AccountEmailTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved AccountEmailTemplateModel Record",
                    Data = null
                };
            }));
        }

        [HttpGet]
        [Route("Email/Suggestions")]
        public IHttpActionResult GetEmailSuggestions(int moduleId, int accountId)
        {
            try
            {
                return Ok(_emailService.EmailSuggestions(moduleId, accountId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
