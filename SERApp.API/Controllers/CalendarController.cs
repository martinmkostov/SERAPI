﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class CalendarController: ApiController
    {
        private ICalendarService _service;
        public CalendarController()
        {
            _service = new CalendarService();
        }

        [HttpGet]
        [Route("Calendar/GetCalendar")]
        [Authorize]
        public IHttpActionResult GetCalendar(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetCalendar(id);
                return new ResponseDataModel<CalendarModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Getting Calendar Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Calendar/GetCalendars")]
        [Authorize]
        public IHttpActionResult GetCalendars([FromBody] CalendarFilters filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetCalendars(filter);

                var count = data.Count();
                return new ResponseDataModel<IEnumerable<CalendarModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Getting Calendars",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Calendar/AddCalendar")]
        [Authorize]
        public IHttpActionResult AddCalendar([FromBody]CalendarModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                var data = _service.AddCalendar(model);

                return new ResponseDataModel<CalendarModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Calendar",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Calendar/Delete")]
        [Authorize]
        public IHttpActionResult Delete([FromBody] CalendarModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.DeleteCalendar(model.Id);
                return new ResponseDataModel<IEnumerable<OpponentModel>>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Calendar",
                };
            }));
        }

    }
}