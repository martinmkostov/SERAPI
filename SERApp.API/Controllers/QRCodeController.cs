﻿using QRCodeDecoderLibrary;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class QRCodeController : ApiController
    {
        public QRCodeController() { }

        [HttpPost]
        [Route("Qr/Decode")]
        public IHttpActionResult Get()
        {
            var files = HttpContext.Current.Request.Files;

            if (files.Count == 0)
            {
                return BadRequest("Please attach qr code to the request.");
            }

            var file = files[0];

            //var bitmap = new Bitmap(file.InputStream);

            using (var bs = new BufferedStream(file.InputStream, 1024))
            {
                var bitmap = new Bitmap(bs);

                QRDecoder decoder = new QRDecoder();

                var imgDecoder = decoder.ImageDecoder(bitmap);
                var decode = imgDecoder.FirstOrDefault();

                var Decoder = Encoding.UTF8.GetDecoder();
                int CharCount = Decoder.GetCharCount(decode, 0, decode.Length);
                char[] CharArray = new char[CharCount];
                Decoder.GetChars(decode, 0, decode.Length, CharArray, 0);

                var result = new string(CharArray);
                return Ok(result);
            }
        }
    }
}