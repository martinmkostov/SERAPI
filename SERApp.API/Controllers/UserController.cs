﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.API.SSRSExecution;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using SERApp.Models.Common;

namespace SERApp.API.Controllers
{
    public class UserController : ApiController
    {
        private UserService _userService;
        public UserController() {
            _userService = new UserService();
        }

        [HttpGet]
        [Route("User/Get")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<UserModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.Get(id);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("User/GetAll")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetAll().OrderBy(x=>x.FirstName).ThenBy(x=>x.LastName).ToList();
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Users",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("User/GetAllActiveUsers")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetAllActiveUsers()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetAllActiveUsers();
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Online Users",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("User/GetEmails")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetEmails(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetByAccountId(id).Where(x=>!string.IsNullOrEmpty(x.EmailAddress) && x.IsActive.HasValue && x.IsActive.Value).Select(x=>x.EmailAddress).ToList();
                return new ResponseDataModel<string>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data List",
                    Data = String.Join(";", data.ToArray())
                };
            }));
        }

        [HttpGet]
        [Route("User/GetByAccountId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetByAccountId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetByAccountId(id);
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("User/Save")]
        [Authorize]
        public IHttpActionResult SaveUser(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userService.SaveUser(model);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved User Record",
                    Data = model
                };
            }));
        }

        [HttpPut]
        [Route("User/Update")]
        [Authorize]
        public IHttpActionResult UpdateUser(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userService.UpdateUser(model);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Updated User Record",
                    Data = model
                };
            }));
        }


        [HttpPost]
        [Route("User/ResendCreateUserConfirmation")]
        [Authorize]
        public IHttpActionResult ResendCreateUserConfirmation(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _userService.ResendCreateUserConfirmation(model);
                return new ResponseDataModel<UserModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved User Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("User/Delete")]
        [Authorize]
        public IHttpActionResult DeleteAccount(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.DeleteUser(model.Id);
                return new ResponseDataModel<UserModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = data ? "Successfully deleted": "Cannot be deleted because it is being used on another data.",
                };
            }));
        }

        [HttpGet]
        [Route("User/GetPdf")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<UserModel>))]
        public HttpResponseMessage GetPdf(int accountId)
        {
            var pdfReport = new PdfReport();
            var data = pdfReport.DownloadPDF(accountId, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));
            return data;
        }
    }

    public class PdfReport
    {
        public HttpResponseMessage DownloadPDF(int id, string dumpPath)
        {
            var res = new ReportExecutionService();

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsPath = "";
            string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
            string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

            res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            reportPath = $"{ssrsPath}Users Report/Users";

            parameters.Add(new ParameterValue
            {
                Name = "accountId",
                Value = id.ToString()
            });

            var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                Directory.CreateDirectory(dumpPath);
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"users-{id.ToString()}{tt}");

            var stream = File.Create(path, result.Length);
            stream.Write(result, 0, result.Length);
            stream.Close();

            DateTime date1 = DateTime.Now;

            var file = File.OpenRead(path);
            var siteservice = new SiteService();
            // var site = siteservice.Get(siteId);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(file);
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = $"Users_{id.ToString()}_{date1.ToString("yyyyMMdd_HH:mm")}"
            };
            //response.Content.Headers.ContentDisposition.FileName = "file";
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return response;


        }
    }
}
