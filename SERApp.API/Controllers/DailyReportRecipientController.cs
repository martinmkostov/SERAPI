﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class DailyReportRecipientController : ApiController
    {
        private readonly IDailyReportRecipientService _service;
        public DailyReportRecipientController()
        {
            _service = new DailyReportRecipientService();
        }
        
        [HttpGet]
        [Route("DailyReportRecipient/GetAllEmailsBySiteAndAccount")]
        [Authorize]
        public IHttpActionResult GetAllEmailsBySiteAndAccount(int accountId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetDailyReportRecipientsByAccountAndSite(accountId, siteId).OrderBy(x=>x.Email).ToList();
                return new ResponseDataModel<IEnumerable<DailyReportRecipientModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Daily Report Recipients",
                    Data = data
                };
            }));
        }
        [HttpGet]
        [Route("DailyReportRecipient/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId,int siteId, string reportType)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetDailyReportRecipients(accountId,siteId, reportType).OrderBy(x=>x.Email).ToList();
                return new ResponseDataModel<IEnumerable<DailyReportRecipientModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Daily Report Recipients",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("DailyReportRecipient/GetAllReportTypes")]
        [Authorize]
        public IHttpActionResult GetAllReportTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {                
                var data = Enum.GetNames(typeof(ReportTypes)).OrderBy(x=>x).ToList();
                return new ResponseDataModel<IEnumerable<string>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Daily Report Recipients",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("DailyReportRecipient/Save")]
        [Authorize]
        public IHttpActionResult Save([FromBody]DailyReportRecipientModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveDailyReportRecipient(model);
                return new ResponseDataModel<DailyReportRecipientModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Daily Report Recipients",
                    Data = model
                };
            }));
        }


        [HttpGet]
        [Route("DailyReportRecipient/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                 var result =_service.DeleteDailyReportRecipient(id);
                return new ResponseDataModel<DailyReportRecipientModel>
                {
                    Success = result,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Daily Report Recipients",
                    Data = null
                };
            }));
        }
    }
}
