﻿using Hangfire;
using Hangfire.Storage;
using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using static SERApp.API.Controllers.TaskController;

namespace SERApp.API.Controllers
{
    public class ReportTypeScheduleController : ApiController
    {
        private AccountService accountService;
        private ReportTypeScheduleService reportTypeScheduleService;
        private IncidentService incidentService;
        private ModuleService moduleService;
        private SiteService siteService;
        private DailyReportRecipientService _dailyReportRecipientService;
        public ReportTypeScheduleController()
        {
            this.accountService = new AccountService();
            this.reportTypeScheduleService = new ReportTypeScheduleService();
            this.incidentService = new IncidentService();
            this.moduleService = new ModuleService();
            this.siteService = new SiteService();
            this._dailyReportRecipientService = new DailyReportRecipientService();
        }

        [HttpGet]
        [Route("ReportTypeSchulde")]
        [Authorize]
        public IHttpActionResult Get()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var listOfReportTypeSchedule = reportTypeScheduleService.GetAll();
                var data = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs().Select(x => new ReportTypeScheduleModel()
                {
                    AccountId = Convert.ToInt32(x.Id.Split('-')[0]),
                    ReportTypeId = x.Id.Split('-')[2],
                    ModuleId = Convert.ToInt32(x.Id.Split('-')[1]),
                    Cron = x.Cron,

                    //Schedule = listOfReportTypeSchedule.Where(r=>r.JobId == x.Id).Any() ? listOfReportTypeSchedule.SingleOrDefault(r => r.JobId == x.Id).Schedule : "",
                    Id = x.Id
                }).ToList();

                data.ForEach(ra =>
                {
                    var da = listOfReportTypeSchedule.Where(r => r.JobId == ra.Id).Any() ? listOfReportTypeSchedule.SingleOrDefault(r => r.JobId == ra.Id) : new ReportTypeScheduleModel();
                    ra.Schedule = listOfReportTypeSchedule.Where(r => r.JobId == ra.Id).Any() ? listOfReportTypeSchedule.SingleOrDefault(r => r.JobId == ra.Id).Schedule : "";

                    var result = JsonConvert.DeserializeObject<schedulerModel>(ra.Schedule);

                    if (result != null)
                    {
                        var date = new DateTime(1991, 08, 28, result.Time.hour, result.Time.minute, result.Time.second);

                        date = date.AddHours((Convert.ToInt32(da.BaseUTCOffset.Split(':')[0])));
                        date = date.AddMinutes((Convert.ToInt32(da.BaseUTCOffset.Split(':')[1])));

                        result.Time.hour = date.Hour;
                        result.Time.minute = date.Minute;
                        result.TimeZone = da.TimeZone;
                        result.Cron = ra.Cron;
                        ra.IsEnabled = listOfReportTypeSchedule.Where(r => r.JobId == ra.Id).Any() ? listOfReportTypeSchedule.SingleOrDefault(r => r.JobId == ra.Id).IsEnabled : false;
                        ra.Schedule = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                        ra.DataToSend = da.DataToSend;
                    }
                    else
                    {
                        RecurringJob.RemoveIfExists(ra.Id);
                        //RecurringJob.RemoveIfExists(result.);
                    }

                });

                return new ResponseDataModel<List<ReportTypeScheduleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("ReportTypeSchulde")]
        [Authorize]
        public IHttpActionResult Post([FromBody] ReportTypeScheduleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                if (string.IsNullOrEmpty(model.TimeZone))
                {
                    return new ResponseDataModel<ReportTypeScheduleModel>
                    {
                        Success = false,
                        StatusCode = HttpStatusCode.BadRequest,
                        Message = "Failed",
                        Data = model
                    };
                }
                var timezone = TimeZoneInfo.FindSystemTimeZoneById(model.TimeZone);

                var content = (ReportTypes)Enum.Parse(typeof(ReportTypes), model.ReportTypeId);
                var listofSites = this.siteService.GetByAccountId(model.AccountId).Select(e => e.Id).ToList();

                switch (content)
                {
                    case ReportTypes.DayReport:
                        if (model.ModuleId == 4)
                        {
                            this.ActivityReport(model);
                        }
                        else
                        {
                            var currentDate = DateTime.Now;

                            if (model.DataToSend == 1)
                            {
                                currentDate = currentDate.AddDays(-1);
                            }
                            this.IncidentReport(model, currentDate, DateTime.Now, "DailyReport", "default");
                        }

                        break;
                    case ReportTypes.WeekReport:
                        if (model.ModuleId == 4)
                        {
                            this.ActivityReport(model);
                        }
                        else
                        {
                            var monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                            var sunday = monday.AddDays(6);
                            this.IncidentReport(model, monday, sunday, "WeeklyReport", "week_report");
                        }
                        break;
                    case ReportTypes.MonthReport:
                        if (model.ModuleId == 4)
                        {
                            this.ActivityReport(model);
                        }
                        else
                        {
                            var firstDay = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                            var lastDay = firstDay.AddDays(6);
                            this.IncidentReport(model, firstDay, lastDay, "MonthReport", "month_report");
                        }
                        break;
                }

                return new ResponseDataModel<ReportTypeScheduleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Schedule",
                    Data = model
                };
            }));
        }

        public void ActivityReport(ReportTypeScheduleModel model)
        {
            var timezone = TimeZoneInfo.FindSystemTimeZoneById(model.TimeZone);
            var sendReportService = new SendReport();
            var jobId = $"{model.AccountId}-{model.ModuleId}-{model.ReportTypeId}";
            var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();
            var job = jobs.FirstOrDefault(e => e.Id == jobId);

            if (job != null)
            {
                RecurringJob.RemoveIfExists(jobId);
                reportTypeScheduleService.Delete(jobId);
            }
            RecurringJob.AddOrUpdate<SendActivityReport>(jobId, e => e.SchedulerReport(jobId, model.AccountId), model.Cron, TimeZoneInfo.Utc);
            reportTypeScheduleService.Add(new ReportTypeScheduleModel()
            {
                Schedule = model.Schedule,
                JobId = jobId,
                TimeZone = timezone.DisplayName,
                BaseUTCOffset = timezone.BaseUtcOffset.ToString(),
                IsEnabled = model.IsEnabled,
            });
        }

        public void IncidentReport(ReportTypeScheduleModel model, DateTime startDate, DateTime endDate, string reportType, string queue)
        {
            var timezone = TimeZoneInfo.FindSystemTimeZoneById(model.TimeZone);
            var listofSites = this.siteService.GetByAccountId(model.AccountId).Select(e => e.Id).ToList();
            listofSites.ForEach(x =>
            {
                var sendReportService = new SendReport();
                var jobId = $"{model.AccountId}-{model.ModuleId}-{model.ReportTypeId}-{x}";
                var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();
                var job = jobs.FirstOrDefault(e => e.Id == jobId);

                if (job != null)
                {
                    RecurringJob.RemoveIfExists(jobId);
                    reportTypeScheduleService.Delete(jobId);
                }

                RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(startDate, endDate, reportType, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService, jobId, model.DataToSend), model.Cron, TimeZoneInfo.Utc, queue);
                reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                {
                    Schedule = model.Schedule,
                    JobId = jobId,
                    TimeZone = timezone.DisplayName,
                    BaseUTCOffset = timezone.BaseUtcOffset.ToString(),
                    IsEnabled = model.IsEnabled,
                });
            });
        }
    }


    public class schedulerModel
    {
        public string Cron { get; set; }
        public string Schedule { get; set; }
        public TimeModel Time { get; set; }
        public string TimeZone { get; set; }
    }
}
