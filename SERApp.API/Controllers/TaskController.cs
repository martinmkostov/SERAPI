﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TaskController : ApiController
    {
        private ITaskService _taskService;
        private IIntervalService _intervalService;
        public TaskController()
        {
            _taskService = new TaskService();
            _intervalService = new IntervalService();
        }

        [HttpGet]
        [Route("Task/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTask(id);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId,string searchText)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _taskService.GetAllTasks(accountId, searchText).OrderBy(c=>c.TaskNo, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetByUserId")]
        [Authorize]
        public IHttpActionResult GetByUserId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetByUserId(id).ToList();
                return new ResponseDataModel<List<IGrouping<string, SiteTaskModel>>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/Save")]
        [Authorize]
        public IHttpActionResult Save(TaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var result = _taskService.AddTask(model);
                return new ResponseDataModel<TaskModel>
                {
                    Success = result,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.DeleteTask(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteSiteTaskLog")]
        [Authorize]
        public IHttpActionResult DeleteSiteTaskLog(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteSiteTaskLog(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = model.IsDeleted,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Site Task Log",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteMainCategory")]
        [Authorize]
        public IHttpActionResult DeleteMainCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
               var data = _taskService.DeleteMainCategory(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteSubCategory")]
        [Authorize]
        public IHttpActionResult DeleteSubCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteSubCategory(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteActivityRole")]
        [Authorize]
        public IHttpActionResult DeleteActivityRole(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteActivityRole(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteInterval")]
        [Authorize]
        public IHttpActionResult DeleteInterval(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteInterval(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/ArchiveTask")]
        [Authorize]
        public IHttpActionResult ArchiveTask(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.ArchiveTask(id);
                return new ResponseDataModel<ArchiveDataModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/UnarchiveTask")]
        [Authorize]
        public IHttpActionResult UnarchiveTask(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.UnarchiveTask(id);
                return new ResponseDataModel<ArchiveDataModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllMainCategories")]
        [Authorize]
        public IHttpActionResult GetAllMainCategories(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllMainCategories(accountId).OrderBy(c=>c.Name).ToList();
                return new ResponseDataModel<IEnumerable<MainCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllMainCategory")]
        [Authorize]
        public IHttpActionResult GetAllMainCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllMainCategory(id);
                return new ResponseDataModel<MainCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetSubCategory")]
        [Authorize]
        public IHttpActionResult GetSubCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetSubCategory(id);
                return new ResponseDataModel<SubCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetAllSubCategories")]
        [Authorize]
        public IHttpActionResult GetAllSubCategories(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllSubCategories(accountId).OrderBy(c=>c.Name).ToList();
                return new ResponseDataModel<IEnumerable<SubCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveMainCategory")]
        [Authorize]
        public IHttpActionResult SaveMainCategory(MainCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddMainCategory(model);
                return new ResponseDataModel<MainCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveSubCategory")]
        [Authorize]
        public IHttpActionResult SaveSubCategory(SubCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddSubCategory(model);
                return new ResponseDataModel<SubCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/PublishTask")]
        [Authorize]
        public IHttpActionResult PublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.PublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/UnPublishTask")]
        [Authorize]
        public IHttpActionResult UnPublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.UnPublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetPublishedTask")]
        [Authorize]
        public IHttpActionResult GetPublishedTask(int Id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetPublishedTask(Id);
                return new ResponseDataModel<IEnumerable<SiteTaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllBySiteId")]
        [Authorize]
        public IHttpActionResult GetAllBySiteId(int siteId, int userId =0,string searchValue="", int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var t = accountId;
                var se = new CultureInfo("sv-SE");
                var data = _taskService.GetAllTaskBySiteId(siteId, userId, searchValue, accountId).OrderBy(x=>x.TaskNo, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetExcelBySiteId")]
        [Authorize]
        public HttpResponseMessage GetExcelBySiteId(int siteId, int userId = 0, string searchValue = "")
        {
            var excel = _taskService.GetXls(siteId,userId,searchValue);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "s.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        [HttpGet]
        [Route("Task/GetXlsAllTask")]
        [Authorize]
        public HttpResponseMessage GetXlsAllTask(int accountId, string searchText)
        {
            var excel = _taskService.GetXlsAllTask(accountId, searchText);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "s.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        [HttpGet]
        [Route("Task/GetTaskBySiteIdAndTaskId")]
        [Authorize]
        public IHttpActionResult GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTaskBySiteIdAndTaskId(taskId,siteId);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("v1/Task/Confirm")]
        public IHttpActionResult BimControlConfirm(BimControlConfirmRequestModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
               
                var taskId = _taskService.GetTaskByTaskNumber(model.TaskNumber, model.AccountId);
                model.TaskId = taskId;
                var siteTask = _taskService.GetSiteTask(model.ObjectId, model.TaskId);

                var d = new SiteTaskLogModel() 
                {
                    Comment= "Kontrollrunda genomförd med få anmärkningar!",
                    Owner = model.Owner,
                    SiteTaskId = siteTask.Id,
                    LogDate = DateTime.UtcNow,
                    IntervalDate = _taskService.GetCurrentIntervalDate(siteTask.Id)
                };

          
                var res = _taskService.ConfirmBimControl(d, model.Items);

                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Bim Control Task Log",
                    Data = res
                };
            }));
        }


        [HttpPost]
        [Route("Task/Confirm")]
        [Authorize]
        public IHttpActionResult Confirm(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                model.LogDate = model.LogDate.ToUniversalTime();

                model.LogDate = model.LogDate.AddMinutes(Math.Abs(model.LogDateOffset));
                var res = _taskService.Confirm(model);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = res
                };
            }));
        }

        [HttpPost]
        [Route("Task/ReAssign")]
        [Authorize]
        public IHttpActionResult ReAssign(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.UnPublishTask(model);
                _taskService.PublishTask(model);
                //_taskService.ReAssign(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/ConfirmMultiple")]
        [Authorize]
        public IHttpActionResult ConfirmMultiple(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                model.LogDate = model.LogDate.ToUniversalTime();

                model.LogDate = model.LogDate.AddMinutes(Math.Abs(model.LogDateOffset));

                var result = _taskService.ConfirmMultiple(model);
                return new ResponseDataModel<ConfirmMultipleResultModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = result
                };
            }));
        }

        [HttpPost]
        [Route("Task/PerformAction")]
        [Authorize]
        public IHttpActionResult PerformAction(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                //model.LogDate = model.LogDate.AddMinutes(model.LogDateOffset);
                model.LogDate = model.LogDate.ToUniversalTime();

                model.LogDate = model.LogDate.AddMinutes(Math.Abs(model.LogDateOffset));
                _taskService.PerformAction(model);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetHistory")]
        [Authorize]
        public IHttpActionResult GetHistory(int siteTaskId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetHistory(siteTaskId).OrderByDescending(r => r.IsFromSer4).ThenBy(r => r.IsPreviousHistory).ThenByDescending(r => r.IntervalDate).ThenByDescending(r => r.LogDate).ToList();
                var oldData = data.Where(x => x.LogType == SERApp.Models.Enums.TaskLogType.OldSystem);
                if (oldData.Any()) 
                {
                    data = data.Where(x => x.LogType != SERApp.Models.Enums.TaskLogType.OldSystem).ToList();
                    data.AddRange(oldData);
                }

                return new ResponseDataModel<IEnumerable<SiteTaskLogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetSingleHistory")]
        [Authorize]
        public IHttpActionResult GetSingleHistory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetSingleHistory(id);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Task/SaveInterval")]
        [Authorize]
        public IHttpActionResult SaveInterval(IntervalModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _intervalService.Add(model);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/Transfer")]
        [Authorize]
        public IHttpActionResult Transfer(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                _taskService.Transfer(model);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task"
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetInterval")]
        [Authorize]
        public IHttpActionResult GetInterval(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _intervalService.Get(id);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetIntervals")]
        [Authorize]
        public IHttpActionResult GetIntervals(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _intervalService.GetIntervals(accountId);
                return new ResponseDataModel<IEnumerable<IntervalModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllActivityRoles")]
        [Authorize]
        public IHttpActionResult GetAllActivityRoles(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetActivityRoles(accountId).OrderBy(c => c.Name).ToList();
                return new ResponseDataModel<IEnumerable<ActivityRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveActivityRole")]
        [Authorize]
        public IHttpActionResult SaveActivityRole(ActivityRoleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                 _taskService.AddActivityRole(model);
                return new ResponseDataModel<IEnumerable<ActivityRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",                 
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetActivityRole")]
        [Authorize]
        public IHttpActionResult GetActivityRole(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetActivityRole(id);
                return new ResponseDataModel<ActivityRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/UploadFiles")]
        [Authorize]
        public IHttpActionResult UploadFiles()
        {

          
            int i = 0;
            int cntSuccess = 0;
            var uploadedFileNames = new List<string>();
            string result = string.Empty;

            HttpResponseMessage response = new HttpResponseMessage();
            var AccountId = HttpContext.Current.Request.Params["AccountId"];
            var TaskId = HttpContext.Current.Request.Params["TaskId"];
            var SiteId = HttpContext.Current.Request.Params["SiteId"];
            var ActionFolder = $"{HttpContext.Current.Request.Params["ActionFolder"]}";

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", AccountId, "activity", TaskId,SiteId,"Confirms", ActionFolder);
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;


            if (!Directory.Exists(fileManagerRoot))
            {
                Directory.CreateDirectory(fileManagerRoot);
            }

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[i];
                    var filePath = Path.Combine(fileManagerRoot, postedFile.FileName);// HttpContext.Current.Server.MapPath("~/UploadedFiles/" + postedFile.FileName);
                    try
                    {
                        postedFile.SaveAs(filePath);
                        uploadedFileNames.Add(httpRequest.Files[i].FileName);
                        cntSuccess++;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    i++;
                }
            }

            result = cntSuccess.ToString() + " files uploaded succesfully.<br/>";

            result += "<ul>";

            foreach (var f in uploadedFileNames)
            {
                result += "<li>" + f + "</li>";
            }

            result += "</ul>" + $"<br/>{fileManagerRoot}";

            return Json(result);
        }

        [HttpGet]
        [Route("Task/DownloadTask")]
        [Authorize]
        public HttpResponseMessage DownloadReport(int taskId)
        {
            return new SendReport().DownloadDailyReport(DateTime.Now, DateTime.Now, "Task", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), taskId, 0,null);
        }

        [HttpGet]
        [Route("Task/DownloadActivity")]
        [Authorize]
        public HttpResponseMessage DownloadActivity(int accountId)
        {
            var sendReport = new SendReport();
            return sendReport.DownloadDailyReport(DateTime.Now, DateTime.Now, "ActivityList", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), 0, accountId, null);

        }

        [HttpGet]
        [Route("Task/SendEmail")]
        public IHttpActionResult SendEmail()
        {
            var report = new SendActivityReport();
            report.SchedulerReport("170-4-WeekReport", 170);
            return Ok();
        }

        public class SendActivityReport
        {
            public void SchedulerReport(string jobId, int accountId)
            {
                var reportTypeScheduleService = new ReportTypeScheduleService(); 
                var isEnabled = reportTypeScheduleService.GetByJobId(jobId).IsEnabled;

                if (!isEnabled)
                {
                    return;
                }

                var userService = new UserService();
                var taskService = new TaskService();

                var users = userService.GetAllActiveFromActiveSites(accountId, 4);
                var dumpPath = System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump");
                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                foreach (var user in users)
                {
                    var body = taskService.GetOngoingTasksByUserIdEmailBody(accountId, user);
                    if (string.IsNullOrEmpty(body)) continue;

                    var server = ConfigurationManager.AppSettings["server"] ?? "Staging";
                    server = server.ToLower();

                    var isProduction = server == "production";

                    var testRecipient = ConfigurationManager.AppSettings[$"{server}testRecipient"] ?? "";
                    var recepients = isProduction ? new string[] { user.EmailAddress }: testRecipient.Split(',');

                    if (user.Id == 9041)
                    {
                        recepients = new string[] { user.EmailAddress, "ita@itmaskinen.se", "ellah@itmaskinen.se", "ellahmariejardin@gmail.com", "erika.lundstrom@grosvenor.com" }; 
                    }

                    var fileName = $"activity-report_{user.Id}_{DateTime.Now:MM-dd-yyyy}_{DateTime.Now:HH:mm}.xls";

                    var preTitle = isProduction ? "" : "[STAGING] ";

                    Service.Tools.Email.SendEmailWithAttachmentToMultipleEmails("", "", recepients.ToList(), $"{preTitle}ACTIVITY REPORT SeR", body, string.Empty, null, fileName, null, false, null, "SER4");
                }
            }
        }
    }
}
