﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueWebController : ApiController
    {
        private readonly IIssueWebService _service;
        private readonly ISettingService _settingService;

        public IssueWebController()
        {
            _service = new IssueWebService();
            _settingService = new SettingService();
        }

        [HttpGet]
        [Route("Issue/Get")]

        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetById(id);
                 
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Issue/GetByGuid")]
    
        public IHttpActionResult GetByGuid(Guid guid)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetByGuid(guid);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Issue/ExportXLS")]
       
        public HttpResponseMessage ExportXLS(int accountId, int siteId = 0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", string fromDateString = null, string toDateString = null)
        {
            var fromDate = Convert.ToDateTime(fromDateString);
            var toDate = Convert.ToDateTime(toDateString);

            var excel = _service.ExportXLS(accountId,siteId, statusId,contactId,issueType1Id, issueType2Id, issueType3Id, searchText, new DateModel() 
                {
                    day = fromDate.Day,
                    month = fromDate.Month,
                    year = fromDate.Year
                },

                new DateModel()
                {
                    day = toDate.Day,
                    month = toDate.Month,
                    year = toDate.Year
                }
            );
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "incidents.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        [HttpPost]
        [Route("Issue/GetAll")]
       
        public IHttpActionResult GetAll([FromBody]IssueFilter filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _settingService.GetSettings(filter.accountId, 7, filter.siteId).OrderBy(x => x.Name);
                var data = _service.GetIssueWeb(filter.accountId, filter.siteId, filter.statusId, filter.contactId, filter.issueType1Id, filter.issueType2Id, filter.issueType3Id, filter.searchText, filter.fromDate,filter.toDate).OrderBy(x=>x.DateCreated).ToList();
                return new ResponseDataModel<IEnumerable<IssueWebModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Issue/GetIssueDropdowns")]
        
        public IHttpActionResult GetIssueDropdowns(int accountId,int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueDropdowns(accountId, siteId);
                return new ResponseDataModel<IssuesDropdownModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Issue/DownloadCSV")]
        [Authorize]
        public IHttpActionResult DownloadCSV([FromBody]IssueFilter filter)
        {
       
           

            return Ok(this.ConsistentApiHandling(() =>
            {
                var stream = new MemoryStream();
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write("Hello, World!");
                    writer.Flush();
                    stream.Position = 0;

                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Export.csv" };
                    var data = result.Content.ReadAsByteArrayAsync().Result;

                    return new ResponseDataModel<FileResultModel>
                    {
                        Success = true,
                        StatusCode = HttpStatusCode.OK,
                        Message = "Successfully Loaded Incident",
                        Data = new FileResultModel()
                        {
                            Data = data
                        }
                    };
                }
            }));
        }

        [HttpGet]
        [Route("Issue/GetLogs")]
        
        public IHttpActionResult GetLogs(int issueWebId, bool showHidden = false)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueWebLog(issueWebId,showHidden).OrderByDescending(x => x.DateTime).ToList();
                return new ResponseDataModel<IEnumerable<IssueWebHistoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Issue/GetTimezone")]
        
        public IHttpActionResult GetTimezone(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTimezoneSetting(accountId);
                return new ResponseDataModel<string>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Issue/Create")]
        public IHttpActionResult Create(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var respData = new ResponseIssueModel();
                var dd = _service.SaveIssueWeb(model);
                respData.IssueWebId = dd.IssueWebId;
                respData.IssueNumber = dd.IssueNumber;

                return new ResponseDataModel<ResponseIssueModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = respData
                };
            }));
        }
        [HttpPost]
        [Route("IssueWeb/Save")]
        public IHttpActionResult Save(IssueWebModel model)
        {
            var saveIssue = Request.Headers.Contains("SaveIssue");
            //bool fromPublicPage = false;

            var fromPublicPage = !saveIssue;

            model.IsPublic = fromPublicPage;

            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model, false);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
            
        }

        [HttpPost]
        [Route("IssueWeb/SaveLog")]
        public IHttpActionResult SaveLog(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWebLog(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/SendDetails")]
        public IHttpActionResult SendDetails(SendDetailsModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SendDetailsModel(model);
                return new ResponseDataModel<SendDetailsModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/CloseIssue")]
        public IHttpActionResult CloseIssue(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                
                _service.CloseIssue(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/ReOpenIssue")]
        public IHttpActionResult ReOpenIssue(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                _service.ReOpenIssue(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Confirm")]
        public IHttpActionResult Confirm(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model, true);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Delete")]
        public IHttpActionResult Delete(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIssueWeb(model.IssueWebId);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Issue/Reminder")]
        public IHttpActionResult Reminder(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.sendReminder(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent reminder",
                    Data = model
                };
            }));
        }
    }
}
