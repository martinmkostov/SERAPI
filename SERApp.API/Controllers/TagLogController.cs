﻿using SERApp.API.Extensions;
using SERApp.API.Models;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TagLogController : ApiController
    {
        private TagLogService _service;
        public TagLogController()
        {
            _service = new TagLogService();
        }

        [HttpGet]
        [Route("TagLog/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagLogById(id);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded TagLog",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("TagLog/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId=0,int siteId=0, string tagLogDate="")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagLogs(accountId, siteId, tagLogDate);
                return new ResponseDataModel<IEnumerable<TagLogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded TagLogs",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("TagLog/Save")]
        [Authorize]
        public IHttpActionResult Save(TagLogCreateModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var m = new TagLogModel()
                {
                    Tag = new TagModel() {
                        RFIDData = model.RFID
                    },
                    GuardId = model.GuardId
                    
                };
                _service.AddTagLog(m);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tag Log",
                    Data = m
                };
            }));
        }

        [HttpPost]
        [Route("TagLog/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteTagLogById(id);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted TagLog",
                };
            }));
        }
    }
}
