﻿using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class FilesController : ApiController
    {
        [HttpGet]
        [Route("Files")]
        public IHttpActionResult GetFiles()
        {
            var service = new FileManagerService();
            var aa = service.GetFilesAndFolders();
            return Ok(aa);
        }
    }
}