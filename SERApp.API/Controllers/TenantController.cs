﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class TenantController : ApiController
    {
        private TenantService _tenantService;
        private TenantRepository _tenantRepository;
        private IType3Repository _type3Repository;
        public TenantController()
        {
            _tenantService = new TenantService();
            _type3Repository = new Type3Repository();
            _tenantRepository = new TenantRepository();
        }

        [HttpGet]
        [Route("Tenant/Get")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<TenantModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.Get(id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetContracts")]
        public IHttpActionResult GetContracts(int tenantId)
        {
            return Ok(_tenantService.GetContracts(tenantId));
        }

        [HttpGet]
        [Route("Tenant/GetXls")]
        [Authorize]
        public HttpResponseMessage GetXls(
            int accountId, 
            int siteId = 0, 
            int userId = 0,  
            int moduleId = 0,
            string searchValue = "", 
            int contactTypeData = 0, 
            int CategoryData = 0, 
            int SubCategoryData = 0, 
            int filterActive = 0)
        {
            var excel = _tenantService.GetXls(
                accountId, 
                siteId, 
                userId,
                moduleId,
                searchValue, 
                contactTypeData, 
                CategoryData, 
                SubCategoryData, 
                filterActive);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "s.xlsx"
            };

            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }


        [HttpGet]
        [Route("Tenant/GetAll")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService.GetAll());
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetTenantListByCSV")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetTenantListByCSV(string csv)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService.GetTenantListByCSV(csv));
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetTenantByTenantTypeId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetTenantByTenantTypeId(int siteId, int tenantTypeId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService.GetTenantByTenantTypeId(siteId,tenantTypeId));
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllByAccountIdAndSiteId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllByAccountIdAndSiteId(int accountId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService.GetAllByAccountIdAndSiteId(accountId, siteId));
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account and Site",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllByAccountId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllByAccountId(
            int accountId,  
            int siteId = 0,
            int userId = 0,  
            int moduleId = 0,
            string searchValue = "",
            int contactTypeData = 0,
            int categoryData = 0,
            int subCategoryData = 0,
            int filterActive = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService
                    .GetAllByAccountId(
                        accountId, 
                        siteId, 
                        userId,
                        moduleId,
                        searchValue, 
                        contactTypeData, 
                        categoryData, 
                        subCategoryData,
                        filterActive));
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllBySiteId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllBySiteId(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = SortTenants(_tenantService.GetAllBySiteId(siteId));
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        private static List<TenantModel> SortTenants(IEnumerable<TenantModel> tenants)
        {
            var sc = StringComparer.Create(new CultureInfo("sv-SE"), false);
            return tenants.OrderBy(x => x.Name, sc)
                .ThenBy(x => x.TenantTypeName, sc)
                .ToList();
        }

        [HttpGet]
        [Route("Tenant/GetAllTenantContactsByTenantId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<TenantContactModel>>))]
        public IHttpActionResult GetAllTenantContactsByTenantId(int tenantId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllTenantContactsByTenantId(tenantId).OrderBy(x=>x.JobTypeName).ThenBy(x=>x.FullName).ToList();
                return new ResponseDataModel<List<TenantContactModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Contacts for this Tenant",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveTenantContact")]
        [Authorize]
        public IHttpActionResult SaveTenantContact(TenantContactModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenantContact(model);
                return new ResponseDataModel<TenantContactModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Contact for this Tenant",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Save")]
        [Authorize]
        public IHttpActionResult SaveUser(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                model.Id = _tenantService.SaveTenant(model);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/saveTenantPublic")]
        [Authorize]
        public IHttpActionResult saveTenantPublic(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {

                model.LastUpdatedByCustomer = DateTime.Now;// model.LastUpdatedByCustomer.Value.ToUniversalTime();

                //model.LastUpdatedByCustomer = model.LastUpdatedByCustomer.Value.AddMinutes(Math.Abs(model.OffSet));


                model.Id = _tenantService.SaveTenant(model, true);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SendUpdateEmail")]
        [Authorize]
        public IHttpActionResult SendUpdateEmail(List<TenantModel> model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data =  _tenantService.SendEmail(model);
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Record",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Delete")]
        [Authorize]
        public IHttpActionResult DeleteAccount(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenant(model.Id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Record"
                };
            }));
        }

        [HttpGet]
        [Authorize]
        [Route("Tenant/GetAllTenantTypes")]
        public IHttpActionResult GetAllTenantTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllTenantTypes(accountId);
                return new ResponseDataModel<List<TenantTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenant Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetTenantTypeById")]
        [Authorize]
        public IHttpActionResult GetTenantTypeById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetTenantTypeModel(id);
                return new ResponseDataModel<TenantTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenant Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveTenantType")]
        [Authorize]
        public IHttpActionResult SaveTenantType(TenantTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _tenantService.SaveTenantType(model);
                return new ResponseDataModel<TenantTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Tenant Type Record":"Contact Type already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SavePremiseType")]
        [Authorize]
        public IHttpActionResult SavePremiseType(PremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _tenantService.SavePremiseType(model);
                return new ResponseDataModel<PremiseTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Premise Type Record":"Premise Type already exists",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllPremiseTypes")]
        [Authorize]
        public IHttpActionResult GetAllPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllPremiseTypes(accountId);
                return new ResponseDataModel<List<PremiseTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Premise Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetPremiseTypeById")]
        [Authorize]
        public IHttpActionResult GetPremiseTypeById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetPremiseTypeModel(id);
                return new ResponseDataModel<PremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Premise Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllSubPremiseTypes")]
        [Authorize]
        public IHttpActionResult GetAllSubPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllSubPremiseTypes(accountId);
                return new ResponseDataModel<List<SubPremiseTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Sub Premise Types",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Tenant/GetSubPremiseTypeById")]
        [Authorize]
        public IHttpActionResult GetSubPremiseTypeById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetSubPremiseTypeModel(id);
                return new ResponseDataModel<SubPremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Sub Premise Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveSubPremiseType")]
        [Authorize]
        public IHttpActionResult SaveSubPremiseType(SubPremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value =_tenantService.SaveSubPremiseType(model);
                return new ResponseDataModel<SubPremiseTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Saved Sub Premise Type Record":"Sub Premise type already exists",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveJobTitle")]
        [Authorize]
        public IHttpActionResult SaveJobTitle(JobTitleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveJobTitle(model);
                return new ResponseDataModel<JobTitleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Job Title Record",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllGetJobTitles")]
        [Authorize]
        public IHttpActionResult GetAllGetJobTitles(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllGetJobTitles(accountId);
                return new ResponseDataModel<List<JobTitleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Job Titles",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteTenantType")]
        [Authorize]
        public IHttpActionResult DeleteTenantType(TenantTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
               var value = _tenantService.DeleteTenantType(model.Id);
                return new ResponseDataModel<TenantTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value?"Successfully Deleted Tenant Type Record":"Cannot delete this contact type because its being used on another data.",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeletePremiseType")]
        [Authorize]
        public IHttpActionResult DeletePremiseType(PremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _tenantService.DeletePremiseType(model.Id);
                return new ResponseDataModel<PremiseTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value?"Successfully Deleted Premise Type Record" : "Cannot delete this category because its being used on another data.",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteSubPremiseType")]
        [Authorize]
        public IHttpActionResult DeleteSubPremiseType(SubPremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _tenantService.DeleteSubPremiseType(model.Id);
                return new ResponseDataModel<SubPremiseTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value?"Successfully Deleted Sub Premise Type Record" : "Cannot delete this sub-category type because its being used on another data.",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteJobTitle")]
        [Authorize]
        public IHttpActionResult DeleteJobTitle(JobTitleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteJobTittle(model.Id);
                return new ResponseDataModel<JobTitleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteTenantContact")]
        [Authorize]
        public IHttpActionResult DeleteTenantContact(TenantContactModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenantContact(model.TenantContactId);
                return new ResponseDataModel<TenantContactModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Contact Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenants/Qr")]
        public HttpResponseMessage GenerateQrCodes(TenantQrModel model)
        {
            var contacts = _tenantRepository.GetTenantsByIds(model.Ids);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            var env = ConfigurationManager.AppSettings["server"].ToLower();
            var isProd = env.ToLower().Contains("production");
            var baseUrl = ConfigurationManager.AppSettings[$"appurl{(isProd ? "" : env)}"];

            Document document = new Document();

            MemoryStream stream = new MemoryStream();

            PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
            pdfWriter.CloseStream = false;

            document.Open();

            PdfPTable table = new PdfPTable(3);
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            foreach (var contact in contacts)
            {
                QRCodeData qrCodeData = qrGenerator.CreateQrCode($"{baseUrl}incidents/create/{contact.SiteId}?tenantId={contact.Id}", QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                var qr = qrCode.GetGraphic(10);

                string qrb64 = string.Empty;
                using (MemoryStream ms = new MemoryStream())
                {
                    qr.Save(ms, ImageFormat.Jpeg);
                    byte[] byteImage = ms.ToArray();
                    qrb64 = Convert.ToBase64String(byteImage);
                }

                byte[] imageBytes = Convert.FromBase64String(qrb64);
                Image image = Image.GetInstance(imageBytes);
                image.Alignment = Element.ALIGN_CENTER;

                var rec = new Rectangle(100, 100);
                image.ScaleAbsolute(rec);

                Font bold = FontFactory.GetFont("Arial", 8, Font.BOLD);
                Paragraph name = new Paragraph(contact.Name, bold)
                {
                    Alignment = Element.ALIGN_CENTER,
                };

                var cell = new PdfPCell();
                cell.HorizontalAlignment = Element.ALIGN_MIDDLE;
                cell.PaddingBottom = 10;
                cell.AddElement(image);
                cell.AddElement(name);

                table.AddCell(cell);
            }

            var remainder = 3 - (contacts.Count % 3);
            if (remainder > 0)
            {
                for (var i = 1; i <= remainder; i++)
                {
                    Paragraph blank = new Paragraph("");
                    table.AddCell(blank);
                }
            }

            document.Add(table);

            document.Close();

            stream.Flush();
            stream.Position = 0;

            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StreamContent(stream)
            };
            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "a.pdf"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
            resp.Content.Headers.ContentLength = stream.Length;
            return resp;
        }

        [HttpGet]
        [Route("Tenant/Qr")]
        public HttpResponseMessage GenerateQrCode(int contactId)
        {
            var contact = _tenantService.Get(contactId);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            var env = ConfigurationManager.AppSettings["server"].ToLower();
            var isProd = env.ToLower().Contains("production");
            var baseUrl = ConfigurationManager.AppSettings[$"appurl{(isProd ? "" : env)}"];
            QRCodeData qrCodeData = qrGenerator.CreateQrCode($"{baseUrl}incidents/create/{contact.SiteId}?tenantId={contactId}", QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            var qr = qrCode.GetGraphic(10);

            string qrb64 = string.Empty;
            using (MemoryStream ms = new MemoryStream())
            {
                qr.Save(ms, ImageFormat.Jpeg);
                byte[] byteImage = ms.ToArray();
                qrb64 = Convert.ToBase64String(byteImage);
            }
            Document document = new Document();

            MemoryStream stream = new MemoryStream();

            PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
            pdfWriter.CloseStream = false;

            document.Open();

            Font bold = FontFactory.GetFont("Arial", 8, Font.BOLD);

            Paragraph name = new Paragraph(contact.Name, bold)
            {
                Alignment = Element.ALIGN_CENTER,
            };

            byte[] imageBytes = Convert.FromBase64String(qrb64);
            Image image = Image.GetInstance(imageBytes);
            image.Alignment = Element.ALIGN_CENTER;

            var rec = new Rectangle(100, 100);
            image.ScaleAbsolute(rec);

            document.Add(image);
            document.Add(name);

            document.Close();

            stream.Flush();
            stream.Position = 0;

            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StreamContent(stream)
            };
            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "a.pdf"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
            resp.Content.Headers.ContentLength = stream.Length;
            return resp;
        }
    }
}
