﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class ModuleController : ApiController
    {
        private ModuleService _moduleService;
        public ModuleController()
        {
            _moduleService = new ModuleService();
        }

        [HttpGet]
        [Route("Module/Get")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<ModuleModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _moduleService.Get(id);
                return new ResponseDataModel<ModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Module Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Module/GetAll")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<ModuleModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _moduleService.GetAll();
                return new ResponseDataModel<List<ModuleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Modules",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Module/GetByTitle")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<ModuleModel>))]
        public IHttpActionResult GetByTitle(string title)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _moduleService.GetByTitle(title);
                return new ResponseDataModel<ModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Module Data",
                    Data = data
                };
            }));
        }
    }
}
