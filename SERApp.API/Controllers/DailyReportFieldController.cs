﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class DailyReportFieldController : ApiController
    {
        private readonly IDailyReportFieldService _service;

        public DailyReportFieldController()
        {
            _service = new DailyReportFieldService();
        }

        [HttpGet]
        [Route("DailyReportField")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<DailyReportFieldModel>>))]
        public IHttpActionResult Get()
        {
            return Ok(this.ConsistentApiHandling(() => 
            {
                var data = this._service.GetDailyReportFields();

                return new ResponseDataModel<List<DailyReportFieldModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Daily Report Field Data",
                    Data = data.ToList()
                };
            }));
        }
    }
}