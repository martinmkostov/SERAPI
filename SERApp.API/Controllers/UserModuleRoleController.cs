﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class UserModuleRoleController : ApiController
    {
        private UserModuleRoleService _userModuleRoleService;
        public UserModuleRoleController()
        {
            _userModuleRoleService = new UserModuleRoleService();
        }

        [HttpGet]
        [Route("UserModuleRole/GetByUserId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModuleRoleModel>>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userModuleRoleService.GetUserModuleRolesById(id);
                return new ResponseDataModel<List<UserModuleRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Roles for this User",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("UserModuleRole/GetBySiteId")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetBySiteId(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _userModuleRoleService.GetUserModuleRolesBySiteId(siteId);

                data = data.Where(x => x.IsActive.HasValue && x.IsActive.Value).OrderBy(x => x.FirstName, StringComparer.Create(se, false)).ThenBy(x => x.LastName, StringComparer.Create(se, false)).ToList();

                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Users for this Site",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("UserModuleRole/Save")]
        [Authorize]
        public IHttpActionResult SaveUserModuleRole(UserModuleRoleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userModuleRoleService.SaveUserModuleRole(model);
                return new ResponseDataModel<UserModuleRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Roles for this User",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("UserModuleRole/Delete")]
        [Authorize]
        public IHttpActionResult DeleteUserModuleRoleByUserId(int userId)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userModuleRoleService.DeleteUserModuleRoleByUserId(userId);
                return new ResponseDataModel<UserModuleRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Roles for this User"
                };
            }));
        }
    }
}
