﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class SummaryController : ApiController
    {
        private ISummaryService _service;
        public SummaryController()
        {
            _service = new SummaryService();
        }

        [HttpPost]
        [Route("Summary/Get")]
        [Authorize]
        public IHttpActionResult Get([FromBody]SummaryFilterModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSummary(new DateTime(model.date.year, model.date.month, model.date.day), model.accountId,model.siteId);
                return new ResponseDataModel<SummaryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Summary",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Summary/Save")]
        [Authorize]
        public IHttpActionResult Save([FromBody]SummaryModel model)
        {
            bool response;
            return Ok(this.ConsistentApiHandling(() =>
            {
                var resp =_service.AddSummary(model, out response);
                return new ResponseDataModel<SummaryModel>
                {
                    Success = response,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Summary",
                    Data = resp
                };
            }));
        }
    }
}
