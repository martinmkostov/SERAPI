﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueType1Controller : ApiController
    {
        private readonly IIssueType1Service _service;
        public IssueType1Controller()
        {
            _service = new IssueType1Service();
        }

        [HttpGet]
        [Route("IssueType1/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.Get(id);
                return new ResponseDataModel<IssueType1Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IssueType1/GetAll")]
        public IHttpActionResult GetAll(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _service.GetAll(accountId).OrderBy(x => x.Name, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<IEnumerable<IssueType1Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueType1/Save")]
        [Authorize]
        public IHttpActionResult Save(IssueType1Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.Save(model);
                return new ResponseDataModel<IssueType1Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Guard",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueType1/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value =_service.Delete(id);
                return new ResponseDataModel<IssueType1Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Guard":"Cannot delete",
                    //Data = model
                };
            }));
        }
    }
}
