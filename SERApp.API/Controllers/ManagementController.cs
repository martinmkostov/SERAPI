﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class ManagementController : ApiController
    {
        private AccountModuleService _accountModuleService;
        public ManagementController()
        {
            _accountModuleService = new AccountModuleService();
        }

        [HttpGet]
        [Route("Account/Module/Get")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<AccountModuleModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountModuleService.Get(id);
                return new ResponseDataModel<AccountModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Module Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Authorize]
        [Route("Account/Module/GetByAccount")]
        [ResponseType(typeof(ResponseDataModel<AccountModuleModel>))]
        public IHttpActionResult GetByAccount(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountModuleService.GetByAccountId(id);
                return new ResponseDataModel<AccountModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Module Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/Module/GetAll")]
        [Authorize]
        [ResponseType(typeof(ResponseDataModel<List<AccountModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountModuleService.GetAll();
                return new ResponseDataModel<List<AccountModuleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Accounts Modules",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Authorize]
        [Route("Account/Module/GetByAccountId")]
        [ResponseType(typeof(ResponseDataModel<List<ModuleModel>>))]
        public IHttpActionResult GetByAccountId(int accountId, int userId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountModuleService.GetModulesByAccountId(accountId, userId);
                return new ResponseDataModel<List<ModuleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Accounts Modules",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Authorize]
        [Route("Account/Module/Save")]
        public IHttpActionResult SaveAccountModule(AccountModuleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountModuleService.SaveAccountModule(model);
                return new ResponseDataModel<AccountModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Module Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Account/Module/Delete")]
        [Authorize]
        public IHttpActionResult DeleteAccountModule(AccountModuleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountModuleService.DeleteAccountModule(model);
                return new ResponseDataModel<AccountModuleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Account Module Record"
                };
            }));
        }
    }
}
