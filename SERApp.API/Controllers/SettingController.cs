﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class SettingController : ApiController
    {
        private readonly ISettingService _service;
        public SettingController()
        {
            _service = new SettingService();
        }

        [HttpGet]
        [Route("Setting/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId, int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                if (moduleId == 4)
                {
                    siteId = 0;
                }

                var data = _service.GetSettings(accountId, moduleId, siteId).OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<SettingModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Setting/GetValue")]
        public IHttpActionResult GetValue(string setting, int accountId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetValue(setting, accountId, siteId);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Setting/GetValueById")]
        [Authorize]
        public IHttpActionResult GetValueById(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetValueById(id);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Setting/Save")]
        [Authorize]
        public IHttpActionResult Save([FromBody]SettingModel model)
        {

            var modelNames = new List<string> { "ContactEmailData", "ContactPublicPageData", "PublicPageImage", "DefaultJobCategories", "AccidentFolderName", "DailyReportFolder", "SecurityFolder", "AccountFolderName","ObjectFolderName", "Timezone", "Type4", "AccidentEmail", "NotifyIssueActions" };
            return Ok(this.ConsistentApiHandling(() =>
            {
                if (model.Name == "PublicText")
                {
                    model.Value = model.Value.Replace("<p>", "").Replace("</p>", "");
                    model.Value = HttpUtility.HtmlDecode(model.Value);
                    
                }

                if (modelNames.Contains(model.Name))
                {
                    model.SiteId = 0;
                }
               
                _service.Save(model);
                return new ResponseDataModel<SettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Type3",
                    Data = model
                };
            }));
        }
    }
}
