﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Repository.Repositories;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class AccountController : ApiController
    {
        private AccountService _accountService;
        private AccountRepository _accountRepo;
        private AccountSettingsService _accountSettingService;
        public AccountController() {
            _accountService = new AccountService();
            _accountSettingService = new AccountSettingsService();
            _accountRepo = new AccountRepository();
        }

        [HttpGet]
        [Route("Account/GetSupportEmailList")]
        [ResponseType(typeof(ResponseDataModel<AccountModel>))]
        public IHttpActionResult GetSupportEmailList(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetSupportRecipients(accountId);
                return new ResponseDataModel<IEnumerable<SupportRecipientModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/Get")]
        [ResponseType(typeof(ResponseDataModel<AccountModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.Get(id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/GetPopupMessage")]
        public IHttpActionResult GetPopupMessage(int id)
        {
            var data = _accountService.Get(id);
            return Ok(data.PopupMobileMessage);
        }

        [HttpGet]
        [Route("Account/GetAdmin")]
        [ResponseType(typeof(ResponseDataModel<UserModel>))]
        public IHttpActionResult GetAdmin(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetAdmin(id);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Admin User for this Account",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<AccountModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetAll();
                return new ResponseDataModel<List<AccountModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Accounts",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Account/Save")]
        public IHttpActionResult SaveAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                if (string.IsNullOrEmpty(model.ContactNumber))
                {
                    model.ContactNumber = model.Mobile;
                }
                
                var data = _accountService.SaveAccount(model);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Record",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Account/Save/PopupMessage")]
        public IHttpActionResult SavePopupMessage(AccountModel model)
        {
            return Ok(_accountRepo.SaveMobilePopupMessage(model));
        }

        [HttpPost]
        [Route("Account/SaveSupportEmail")]
        public IHttpActionResult SaveSupportEmail(dynamic model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {            

                var data = _accountService.SaveEmailSupport(model);
                return new ResponseDataModel<AccountModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Record",
                   // Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Account/DeleteEmail")]
        public IHttpActionResult DeleteEmail(SupportRecipientModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {

                var data = _accountService.DeleteSupportEmail(model);
                return new ResponseDataModel<AccountModel>
                {
                    Success = data,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Account Record",
                    // Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Account/Delete")]
        public IHttpActionResult DeleteAccount(AccountModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountService.DeleteAccount(model.Id);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Account Record"
                };
            }));
        }

        [HttpGet]
        [Route("Account/IsDuplicate")]
        public IHttpActionResult Get(string companyName)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.IsDuplicate(companyName);
                return new ResponseDataModel<AccountModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Extras = data
                };
            }));
        }


        [HttpGet]
        [Route("Account/GetEmail")]
        public IHttpActionResult GetEmail(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountService.GetEmail(id);
                return new ResponseDataModel<SupportRecipientModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Account/Settings")]
        [ResponseType(typeof(ResponseDataModel<List<AccountSettingModel>>))]
        public IHttpActionResult GetAccountSettings(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _accountSettingService.GetAccountSettings(accountId);
                return new ResponseDataModel<List<AccountSettingModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Account Settings",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Account/SaveSettings")]
        public IHttpActionResult SaveSettings(AccountSettingModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _accountSettingService.Save(model);
                return new ResponseDataModel<AccountSettingModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Save Account Settings"
                };
            }));
        }

        [HttpPost]
        [Route("Account/Contract/CC")]
        public IHttpActionResult SaveEmailContractCC(AccountModel data)
        {
            try
            {
                _accountRepo.SaveEmailContractCC(data);
                return Ok(true);
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }

        [HttpGet]
        [Route("Account/Contract/CC")]
        public IHttpActionResult GetEmailContractCC(int accountId)
        {
            try
            {
                return Ok(_accountRepo.GetEmailContractCC(accountId));
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }
    }
}
