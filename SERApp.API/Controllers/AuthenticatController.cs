﻿using Microsoft.IdentityModel.Tokens;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class AuthenticatController : ApiController
    {
        private AuthenticationService _authService;
        private SiteRoleAuthenticationService _siteRoleAuthService;
        public AuthenticatController()
        {
            _authService = new AuthenticationService();
            _siteRoleAuthService = new SiteRoleAuthenticationService();
        }

        [HttpGet]
        [Route("Authentication/GetToken")]    
        public Object GetToken()
        {
            string key = "ser4secretkey5512"; //Secret key which will be used later during validation    
            var issuer = "https://ser4api.azurewebsites.net/";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

           
            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(30),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = jwt_token };
        }

        [HttpPost]
        [Route("Authentication/Login")]
        public IHttpActionResult Login(AuthRequestModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _authService.Authenticate(model.UserName, model.Password);
                data.Token = GetTokenString();

                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = data.IsAuthenticated,
                    StatusCode = HttpStatusCode.OK,
                    Message = data.AuthResponseMessage,
                    Data = data
                };
            }));
        }

        private string GetTokenString() 
        {
            string key = "ser4secretkey5512"; //Secret key which will be used later during validation    
            var issuer = "https://ser4api.azurewebsites.net/";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));


            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(30),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_token;
        }

        [HttpPost]
        [Route("Authentication/Logoff")]
        public IHttpActionResult Logoff(int userId)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _authService.LogOff(userId);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = data.AuthResponseMessage,
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Authentication/ForgotPassword")]
        public IHttpActionResult ForgotPassword(AuthRequestModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _authService.SendPasswordToEmailAddress(model.UserName, model.EmailAddress, model.ResendToEmailAddress);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent Password on your email address.",
                };
            }));
        }

        [HttpGet]
        [Route("Authentication/GetRole")]
        [Authorize]
        public IHttpActionResult GetRole(int userId, int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteRoleAuthService.GetRole(userId,moduleId,siteId);
                return new ResponseDataModel<RoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }
        
        [HttpGet]
        [Route("Authentication/GetRoleByModule")]
        [Authorize]
        public IHttpActionResult GetRoleByModule(int userId, int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteRoleAuthService.GetRolesByModule(userId,moduleId);
                return new ResponseDataModel<List<UserModuleRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Authentication/GetUsers")]
        [Authorize]
        public IHttpActionResult GetUsers(int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var se = new CultureInfo("sv-SE");
                var data = _siteRoleAuthService.GetUsers(moduleId, siteId).OrderBy(x => x.User.FirstName, StringComparer.Create(se, false)).ThenBy(x => x.User.LastName, StringComparer.Create(se, false)).ToList();
                return new ResponseDataModel<List<UserModuleRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }
    }
}
