﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueType3Controller : ApiController
    {
        private readonly IIssueType3Service _service;
        public IssueType3Controller()
        {
            _service = new IssueType3Service();
        }

        [HttpGet]
        [Route("IssueType3/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.Get(id);
                return new ResponseDataModel<IssueType3Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IssueType3/GetAll")]
        public IHttpActionResult GetAll(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAll(accountId).OrderBy(x => x.Name);
                return new ResponseDataModel<IEnumerable<IssueType3Model>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueType3/Save")]
        [Authorize]
        public IHttpActionResult Save(IssueType3Model model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.Save(model);
                return new ResponseDataModel<IssueType3Model>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Guard",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueType3/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.Delete(id);
                return new ResponseDataModel<IssueType3Model>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value ? "Successfully Saved Guard" : "Cannot delete data because it is being used as a reference on an issue",
                    //Data = model
                };
            }));
        }
    }
}
