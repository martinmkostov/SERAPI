﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentAccidentController : ApiController
    {
        private IIncidentAccidentService _service;
        public IncidentAccidentController()
        {
            _service = new IncidentAccidentService();
        }

        [HttpGet]
        [Route("IncidentAccident/GetSkadetypes")]
        [Authorize]
        public IHttpActionResult GetAll(int type, int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSkadetypes(type, accountId);
                return new ResponseDataModel<IEnumerable<SkadetypModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("IncidentAccident/GetAllSkadetypes")]
        [Authorize]
        public IHttpActionResult GetAllSkadetypes(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllSkadetypes(accountId);
                return new ResponseDataModel<IEnumerable<SkadetypModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IncidentAccident/SaveSkadetyp")]
        [Authorize]
        public IHttpActionResult Save(SkadetypModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddSkadetype(model);
                return new ResponseDataModel<SkadetypModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Saved SkadetypModel" : "Incident already SkadetypModel",

                };
            }));
        }

        [HttpPost]
        [Route("IncidentAccident/ToggleClose")]
        [Authorize]
        public IHttpActionResult ToggleClose(IncidentAccidentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.ToggleStatus(model);
                return new ResponseDataModel<SkadetypModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Saved SkadetypModel" : "Incident already SkadetypModel",

                };
            }));
        }

        [HttpPost]
        [Route("IncidentAccident/Delete")]
        [Authorize]
        public IHttpActionResult Delete(IncidentAccidentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.Delete(model);
                return new ResponseDataModel<IncidentAccidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Deleted SkadetypModel" : "Incident Deleted SkadetypModel",

                };
            }));
        }


        [HttpPost]
        [Route("IncidentAccident/DeleteSkadetyp")]
        [Authorize]
        public IHttpActionResult DeleteSkadetyp(SkadetypModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var result = _service.DeleteSkadetyp(model);
                return new ResponseDataModel<IncidentAccidentModel>
                {
                    Success = result,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Deleted SkadetypModel" : "Incident Deleted SkadetypModel",

                };
            }));
        }

        [HttpGet]
        [Route("IncidentAccident/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(
            string selectedDate,
            string toDay,
            int accidentType,
            int siteId,
            int typeData,
            string searchText,
            int status,
            int accountId,
            int userId,
            int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                DateTime date;
                DateTime.TryParse(selectedDate, out date);
                DateTime date2;
                DateTime.TryParse(toDay, out date2);

                var data = _service.GetAllIncidentAccident(date, date2, accidentType, siteId, typeData, searchText, status, accountId, userId, moduleId);
                return new ResponseDataModel<IEnumerable<IncidentAccidentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentAccident/GetSkadetype")]
        [Authorize]
        public IHttpActionResult GetSkadetype(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                var data = _service.GetSkadetyp(id);
                return new ResponseDataModel<SkadetypModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentAccident/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                var data = _service.Get(id);
                return new ResponseDataModel<IncidentAccidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("IncidentAccident/Save")]
        [Authorize]
        public IHttpActionResult Save(IncidentAccidentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var sendReport = new SendReport();

                // return sendReport.DownloadIncideAccidentDetails(id, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));





                model.Id = _service.AddIncident(model);

                if (model.AccidentType != 0)
                {
                    model.dumppath = System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump");
                    string filePathData;
                    sendReport.DownloadIncideAccidentDetailsFile(model.Id, model.dumppath, out filePathData);
                    model.filepath = filePathData;

                    _service.SendIncident(model);
                }
                return new ResponseDataModel<GuardModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Saved Incident" : "Incident already exists",

                };
            }));
        }


        [HttpPost]
        [Route("IncidentAccident/AddComment")]
        [Authorize]
        public IHttpActionResult AddComment(IncidentAccidentCommentModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = false;
                var message = "";
                var incidentAccidenData = _service.Get(model.IncidentAccidentId);
                var currentDate = incidentAccidenData.Date;
                var currentTime = incidentAccidenData.Time;
                var comment = new List<IncidentAccidentCommentModel>();

                var currentDateTime = new DateTime(currentDate.year, currentDate.month, currentDate.day, currentTime.hour, currentTime.minute, currentTime.second);
                var newDateTime = new DateTime(model.DateModel.year, model.DateModel.month, model.DateModel.day, model.TimeModel.hour, model.TimeModel.minute, model.TimeModel.second);

                if (newDateTime < currentDateTime)
                {
                    message = "Comment cannot be added. Please check date/time selected.";
                }
                else
                {
                    comment = _service.AddComment(model);
                    message = "Successfully added comment!";
                    success = true;
                }

                return new ResponseDataModel<List<IncidentAccidentCommentModel>>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,
                    Data = comment
                };
            }));
        }

        [HttpGet]
        [Route("IncidentAccident/DeleteComment")]
        [Authorize]
        public IHttpActionResult DeleteComment(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteComment(id);
                return new ResponseDataModel<IncidentAccidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = true ? "Successfully Deleted Comment" : "Incident Deleted Comment",

                };
            }));
        }

        [HttpGet]
        [Route("IncidentAccident/DownloadReport")]
        [Authorize]
        public HttpResponseMessage DownloadReport(int id, int accountId, bool isAccident)
        {

            var sendReport = new SendReport();

            return sendReport.DownloadIncideAccidentDetails(id, accountId, isAccident, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));

        }


        [HttpGet]
        [Route("IncidentAccident/DownloadReportList")]
        [Authorize]
        public HttpResponseMessage DownloadReportList(string date, string date1, int siteId)
        {
            var sendReport = new SendReport();
            return sendReport.DownloadIncideAccidentList(Convert.ToDateTime(date), Convert.ToDateTime(date1), siteId, System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"));

        }

        [HttpGet]
        [Route("IncidentAccident/ExportXLSList")]
        public HttpResponseMessage ExportXLS(DateTime date, DateTime date1, int siteId, int type, int status, string search, int accountId, int userId, int accidentType)
        {
            var excel = _service.ExportXLSList(date, date1, siteId,type, status, search, accountId, userId, accidentType);

            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "incidents_accidents.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

    }
}
