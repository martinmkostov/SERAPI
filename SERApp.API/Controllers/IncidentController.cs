﻿using Hangfire;
using Hangfire.Storage;
using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.API.SSRSExecution;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentController : ApiController
    {
        private IncidentService _service;
        private AccountService _accountService;
        private IncidentTypeService _incidentTypeService;
        private DailyReportRecipientService _dailyReportRecipientService;
        private EmailService _emailService;
        private SiteService _siteService;
        private ReportTypeScheduleService _reportTypeScheduleService;
        private ModuleService _moduleService;
        private TenantService _tenantService;
        private IType3Repository _type3Repository;

        private IIncidentRepository _repository;
        private string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
        private string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

        public IncidentController()
        {
            _service = new IncidentService();
            _accountService = new AccountService();
            _incidentTypeService = new IncidentTypeService();
            _dailyReportRecipientService = new DailyReportRecipientService();
            _emailService = new EmailService();
            _siteService = new SiteService();
            _reportTypeScheduleService = new ReportTypeScheduleService();
            _moduleService = new ModuleService();
            _repository = new IncidentRepository();
            _tenantService = new TenantService();
            _type3Repository = new Type3Repository();
        }

        [HttpGet]
        [Route("Incident/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/Images-fix")]
        public IHttpActionResult Fix()
        {
            try
            {
                _repository.FixImages();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Incident/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll(int accountId =0,int siteId=0,string selectedDate = "",string selectedEndDate = "", int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                //var test = _service.GetHtmlTemplateWeekReport(new DateTime(2019,3,18), new DateTime(2019, 3, 24), 65);
                var data = _service.GetIncidents(accountId, siteId, selectedDate, selectedEndDate, selectedType1, selectedType2, selectedType3, searchText).ToList();
                return new ResponseDataModel<IEnumerable<IncidentListModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetTimezones")]
        [Authorize]
        public IHttpActionResult GetTimezones()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var tzis = TimeZoneInfo.GetSystemTimeZones();
                var arrTimeZoneInfo = new List<TimeZoneInfo>();
                foreach (var tzi in tzis)
                {
                    arrTimeZoneInfo.Add(tzi);
                }

                return new ResponseDataModel<List<TimeZoneInfo>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = arrTimeZoneInfo
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetGuardsToday")]
        [Authorize]
        public IHttpActionResult GetGuardsToday(int accountId = 0, int siteId = 0, string selectedDate = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuardsToday(accountId, siteId, selectedDate);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Incident/GetIncidentRange")]
        [Authorize]
        public IHttpActionResult GetIncidentRange([FromBody]WeekReportModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentRange(model);
                return new ResponseDataModel<WeekReportOverallReportModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }
        
        [HttpPost]
        [Route("Incident/Save")]
        [Authorize]
        public IHttpActionResult Save(IncidentModel model)
        {   
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddIncident(model);

                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAllSection")]
        [Authorize]
        public IHttpActionResult GetAllSection(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSectionModels().Where(r=>r.SiteId == siteId).OrderBy(x=>x.SectionValue);
                return new ResponseDataModel<IEnumerable<SectionModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Sections",
                    Data = data
                };
            }));
        }
      
        [HttpPost]
        [Route("Incident/Delete")]
        [Authorize]
        public IHttpActionResult Delete(int id, [FromUri] int moduleId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var incident = this._service.GetIncident(id);
                var name = this._accountService.GetName(incident.AccountId);
                var module = this._moduleService.Get((int)moduleId);

                incident.Reports.ForEach(report =>
                {
                    var schedule = this._reportTypeScheduleService.Get(incident.AccountId, report.ReportTypeId, moduleId);
                    if (schedule != null)
                    {
                        var jobId = $"{name} {module.Title} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                        RecurringJob.RemoveIfExists(jobId);
                    }
                });

                _service.DeleteIncident(id);
                
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }

        [HttpPost]
        [Route("Incident/SendDailyReport")]
        [Authorize]
        public IHttpActionResult SendDailyReport([FromBody]ReportEmailModel base64Data)
        {
            try
            {
                var returnData = true;
                var sendReport = new SendReport();
                switch (base64Data.routineName)
                {
                    case "WeeklyReport":
                        var date1 = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                        var date2 = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

                        var accountId = _siteService.Get(base64Data.siteId).AccountId;
                        sendReport.SendDailyReport(date2, date1, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId, _dailyReportRecipientService, base64Data.SenderName);
                        break;
                    case "DailyReport":
                        var accountId2 = _siteService.Get(base64Data.siteId).AccountId;
                        var newDate = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                        sendReport.SendDailyReport(newDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountId2, _dailyReportRecipientService, base64Data.SenderName);
                        break;
                    case "MonthReport":
                        var date1MonthReport = new DateTime(base64Data.Date1.year, base64Data.Date1.month, base64Data.Date1.day);
                        var date2MonthReport = new DateTime(base64Data.Date2.year, base64Data.Date2.month, base64Data.Date2.day);

                        var accountIdMonthReport = _siteService.Get(base64Data.siteId).AccountId;
                        sendReport.SendDailyReport(date1MonthReport, date2MonthReport, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), base64Data.siteId, accountIdMonthReport, _dailyReportRecipientService, base64Data.SenderName);
                        break;
                    default:
                        var incident = _service.GetIncident(base64Data.IncidentId);
                        var account = _accountService.GetAccountInfoOnly(incident.AccountId);

                        incident.Reports.ForEach(report =>
                        {
                            var jobId = $"{account.Name} {report.ReportType.Name} ({incident.Id}, {report.Id})";
                            // var sendReport = new SendReport();
                            var reportTemp = incident.Reports.FirstOrDefault(e => e.Id == report.Id);
                            sendReport.Send(System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), incident, reportTemp, base64Data.SenderName);

                        });

                        returnData = incident.Reports.Any();
                        break;
                }

                return Ok(returnData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        [Route("Incident/GetExcel")]
        [Authorize]
        public HttpResponseMessage GetExcel()
        {
            var path = @"C:\Users\lemon\Desktop\New folder (4)\EXPENSES.xlsx";
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            //result.Content.Headers.ContentType =
            //    new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        //[HttpGet]
        //[Route("Incident/Statistics")]
        //public IHttpActionResult GetStatistics(DateTime start, DateTime end, int accountId, int dateType, int dataType, int siteId)
        //{
        //    try
        //    {
        //        var stats = _service.GetStatistics(start, end, accountId, dateType, dataType, siteId);
        //        return Ok(stats);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}

        [HttpGet]
        [Route("Incident/Statistics/Export")]
        public HttpResponseMessage ExportStatistics(string filter)
        {
            var model = JsonConvert.DeserializeObject<DailyReportStatisticsRequestModel>(filter);
            //var lists = _repository.GetStatistics(model);

            var lists = new List<IncidentStatisticsModel>
            {
                _repository.GetStatistics(model.Start, model.End, model)
            };

            if (model.Start2.HasValue && model.End2.HasValue)
            {
                lists.Add(_repository.GetStatistics(model.Start2.Value, model.End2.Value, model));
            }

            var excel = _service.ExportStatisticsByCategory(lists, model.StackedColumn);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "s.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        [HttpGet]
        [Route("Incident/Statistics/Dates")]
        public IHttpActionResult GetMinMaxStatDates(int accountId, int siteId)
        {
            try
            {
                var stats = _service.GetMinMaxDates(accountId, siteId);
                return Ok(stats);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Incident/DownloadReport")]
        [Authorize]
        public HttpResponseMessage DownloadReport(string routineName, string date1data, string date2data, int siteId)
        {
            var sendReport = new SendReport();
            switch (routineName)
            {
                case "WeeklyReport":
                    var date1 = Convert.ToDateTime(date1data);
                    var date2 = Convert.ToDateTime(date2data);

                    var accountId = _siteService.Get(siteId).AccountId;
                    return sendReport.DownloadDailyReport(date2, date1, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountId, _dailyReportRecipientService);

                case "DailyReport":
                    var accountId2 = _siteService.Get(siteId).AccountId;
                    var newDate = Convert.ToDateTime(date1data);
                    return sendReport.DownloadDailyReport(newDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountId2, _dailyReportRecipientService);

                case "MonthReport":
                    var date1MonthReport = Convert.ToDateTime(date1data);
                    var date2MonthReport = Convert.ToDateTime(date2data);
                    var dd1 = new DateTime(date1MonthReport.Year, date1MonthReport.Month,1);
                    var dd2 = dd1.AddMonths(1).AddDays(-1);
                    var accountIdMonthReport = _siteService.Get(siteId).AccountId;
                    return sendReport.DownloadDailyReport(dd1, dd2, "MonthReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), siteId, accountIdMonthReport, _dailyReportRecipientService);

                default:
                    return new HttpResponseMessage();

            }
        

        }


        [HttpPost]
        [Route("Incident/EnableSchedule")]
        [Authorize]
        public IHttpActionResult EnableSchedule(dynamic job)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _reportTypeScheduleService.ToggleEnable(job);

                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = job == -1?"Please set the time first": "Deleted Incident",
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetType3ByContanctId")]
        [Authorize]
        public IHttpActionResult EnableSchedule(int contactId)
        {
            var contact = _tenantService.Get(contactId);
            var type = contact.TenantType;
            var type3 = _type3Repository.GetType3ByTenantTypeIdAndSiteId(type, contact.SiteId);
            return Ok(type3?.Id ?? null);
        }

        [HttpGet]
        [Route("Incident/ExportXLS")]
        [Authorize]
        public HttpResponseMessage ExportXLS(int accountId = 0, int siteId = 0, string selectedDate = "", string selectedEndDate = "", int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
            var excel = _service.ExportXLS(accountId, siteId,selectedDate,selectedEndDate,selectedType1, selectedType2, selectedType3, searchText);
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ByteArrayContent(excel)
            };

            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "incidents.xlsx"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            resp.Content.Headers.ContentLength = excel.Length;

            return resp;
        }

        public Image Base64ToImage(string base64String)
        {
            base64String = base64String.Replace("data:image/png;base64,", "");
            base64String = base64String.Replace("data:image/jpeg;base64,", "");
            byte[] imageBytes = Convert.FromBase64String(base64String);

            using (MemoryStream ms = new MemoryStream(imageBytes))
            {
                Image image = Image.FromStream(ms);
                return image;
            }
        }

        [HttpGet]
        [Route("Incident/Image")]
        public HttpResponseMessage GetImage(int id, int order)
        {
            var b64 = _repository.GetImage(order, id);
            if (string.IsNullOrEmpty(b64))
            {
                return new HttpResponseMessage();
            }
            Image image = Base64ToImage(b64);

            var imageByte = ResizeImage(image, 200, 200);

            MemoryStream ms = new MemoryStream();
            var patternImage = new Bitmap(image);
            patternImage.Save(ms, ImageFormat.Jpeg);

            var byteArray = imageByte;// ms.ToArray();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(byteArray);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");

            return response;
        }

        public static Image CropImageCenter(Image image, Size destinationSize)
        {
            ExifRotate(image);

            var originalWidth = image.Width;
            var originalHeight = image.Height;

            //how many units are there to make the original length
            var hRatio = (float)originalHeight / destinationSize.Height;
            var wRatio = (float)originalWidth / destinationSize.Width;

            //get the shorter side
            var ratio = Math.Min(hRatio, wRatio);

            var hScale = Convert.ToInt32(destinationSize.Height * ratio);
            var wScale = Convert.ToInt32(destinationSize.Width * ratio);

            //start cropping from the center
            var startX = (originalWidth - wScale) / 2;
            var startY = (originalHeight - hScale) / 2;

            //crop the image from the specified location and size
            var sourceRectangle = new Rectangle(startX, startY, wScale, hScale);

            //the future size of the image
            var bitmap = new Bitmap(destinationSize.Width, destinationSize.Height);

            //fill-in the whole bitmap
            var destinationRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            //generate the new image
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, destinationRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }

            return bitmap;

        }

        public static byte[] ResizeImage(Image image, int maxWidth, int maxHeight)
        {
            ExifRotate(image);

            byte[] result;
            using (var newImage = new Bitmap(maxWidth, maxHeight))
            {
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var solidBrush = new SolidBrush(Color.White))
                    {
                        graphics.FillRectangle(solidBrush, new Rectangle(0, 0, maxWidth, maxHeight));
                    }

                    graphics.DrawImage(image, 0, 0, maxWidth, maxHeight);

                    var encoder = GetEncoder(ImageFormat.Jpeg);
                    var encoderQuality = Encoder.Quality;
                    var encoderParams = new EncoderParameters(1);
                    encoderParams.Param[0] = new EncoderParameter(encoderQuality, 100L);

                    using (var ms = new MemoryStream())
                    {
                        newImage.Save(ms, encoder, encoderParams);
                        result = ms.ToArray();
                    }
                }

                return result;
            }
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }
        public static int exifOrientationID = 0x112;
        private static Image ExifRotate(Image img)
        {
            bool hasDefaultOrientation = false;
            for (int x = 0; x < img.PropertyIdList.Length; x++)
            {
                if (img.PropertyIdList[x] == exifOrientationID)
                {
                    hasDefaultOrientation = true;
                    break;
                }
            }

            if (!hasDefaultOrientation)
            {
                return null;
            }

            var prop = img.GetPropertyItem(exifOrientationID);
            int val = BitConverter.ToUInt16(prop.Value, 0);
            var rot = RotateFlipType.RotateNoneFlipNone;

            if (val == 3 || val == 4)
                rot = RotateFlipType.Rotate180FlipNone;
            else if (val == 5 || val == 6)
                rot = RotateFlipType.Rotate90FlipNone;
            else if (val == 7 || val == 8)
                rot = RotateFlipType.Rotate270FlipNone;

            if (val == 2 || val == 4 || val == 5 || val == 7)
                rot |= RotateFlipType.RotateNoneFlipX;

            if (rot != RotateFlipType.RotateNoneFlipNone)
                img.RotateFlip(rot);

            return img;
        }

    }


    public class SendReport
    {
        public void SchedulerReport(DateTime date1, DateTime date2, string reportType, string dumpPath, int siteId, int accountId = 0, DailyReportRecipientService dailyReportRecipientService = null, string jobId = "", int dataToSend = 0)
        {            
            var reportTypeScheduleService = new ReportTypeScheduleService();
            var isEnabled = reportTypeScheduleService.GetByJobId(jobId).IsEnabled;

            if (!isEnabled)
            {
                return;
            }

            var res = new ReportExecutionService();

            string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
            string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

            res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsPath = "";
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            string additionalBodyHtml = "";
            var service = new IncidentService();
            switch (reportType)
            {
                case "WeeklyReport":
                    reportPath = $"{ssrsPath}Week Report Template/Week Report Main";

                    var monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                    date1 = monday;
                    var sunday = monday.AddDays(6);

                    parameters.Add(new ParameterValue
                    {
                        Name = "date1",
                        Value = monday.ToString("MM-dd-yyyy")
                    });
                    parameters.Add(new ParameterValue
                    {
                        Name = "date2",
                        Value = sunday.ToString("MM-dd-yyyy")
                    });
                    parameters.Add(new ParameterValue
                    {
                        Name = "siteId",
                        Value = siteId.ToString()
                    });
                    break;
                case "DailyReport":
                        
                    date1 = DateTime.Now;
                    if (dataToSend == 1)
                    {
                        date1 = date1.AddDays(-1);
                    }
                    reportPath = $"{ssrsPath}Daily Report Template/Daily Report";

                    parameters.Add(new ParameterValue
                    {
                        Name = "date",
                        Value = date1.ToString("MM-dd-yyyy")
                    });
                    parameters.Add(new ParameterValue
                    {
                        Name = "siteId",
                        Value = siteId.ToString()
                    });
                    additionalBodyHtml = service.GetHtmlTemplateDayReport(date1, siteId);
                    break;
                case "MonthReport":
                    date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    reportPath = $"{ssrsPath}Month Report Template/Month Report Main";
                    date2 = date1.AddMonths(1).AddDays(-1);
                    parameters.Add(new ParameterValue
                    {
                        Name = "date1",
                        Value = date1.ToString("MM-dd-yyyy")
                    });
                    parameters.Add(new ParameterValue
                    {
                        Name = "date2",
                        Value = date2.ToString("MM-dd-yyyy")
                    });
                    parameters.Add(new ParameterValue
                    {
                        Name = "siteId",
                        Value = siteId.ToString()
                    });
                    break;

            }

            var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                try { 
                Directory.CreateDirectory(dumpPath);
                }
                catch {
                    return;
                }
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

            var stream = File.Create(path, result.Length);
            stream.Write(result, 0, result.Length);
            stream.Close();

            //var reportType = report.ReportType;


            var emails = dailyReportRecipientService.GetDailyReportRecipients(accountId, siteId, reportType).ToList();

            server = server.ToLower();

            var isProduction = server == "production";
            var testRecipient = ConfigurationManager.AppSettings[$"{server}testRecipient"] ?? "";
            var recepients = isProduction ? emails.Select(e => e.Email).ToArray() : testRecipient.Split(',');
            var reportEmailModel = new ReportEmailModel
            {
                recepients = recepients,
                EmailTitle = reportType,
                routineName = reportType,
                siteId = siteId
            };

            //var file = File.OpenRead(path);
            //this.Send(reportEmailModel, file, $"{reportType}_{date1.ToString("MM-dd-yyyy")}{tt}.pdf", "Automated Email Scheduler", additionalBodyHtml);

            using (var file = File.OpenRead(path))
            {
                this.Send(path,reportEmailModel, file, $"{reportType}_{date1.ToString("MM-dd-yyyy")}_{DateTime.Now.ToString("HH:mm")}.pdf", $"System Generated", additionalBodyHtml);
            }
            //file.Close();

            //delete the file
            if (File.Exists(path))
            {
                File.Delete(path);
            }

        }

        public void SendDailyReport(DateTime date1,DateTime date2, string reportType, string dumpPath, int siteId,int accountId =0, DailyReportRecipientService dailyReportRecipientService = null, string SenderName = "")
        {
            try
            {
                var res = new ReportExecutionService();

                string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
                string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

                res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
                var server = ConfigurationManager.AppSettings["server"];
                string ssrsPath = "";
                switch (server)
                {
                    case "Staging":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    case "Production":
                        ssrsPath = "/SER4/";
                        break;
                    case "Development":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    default:
                        ssrsPath = "/SER4/";
                        res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        break;
                }

                res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;
               
                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();
                var reportPath = "";
                string additionalBodyHtml = "";
                var service = new IncidentService();
                switch (reportType)
                {
                    case "WeeklyReport":
                        reportPath = $"{ssrsPath}Week Report Template/Week Report Main";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "DailyReport":

                        reportPath = $"{ssrsPath}Daily Report Template/Daily Report";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });

                       
                        additionalBodyHtml = service.GetHtmlTemplateDayReport(date1,siteId);
                        break;
                    case "MonthReport":
                        date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        reportPath = $"{ssrsPath}Month Report Template/Month Report Main";
                        date2 = date1.AddMonths(1).AddDays(-1);
                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                }

                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

                var stream = File.Create(path, result.Length);
                stream.Write(result, 0, result.Length);
                stream.Close();

                //var reportType = report.ReportType;

                
                var emails = dailyReportRecipientService.GetDailyReportRecipients(accountId, siteId, reportType).ToList();
                //emails.Add(new DailyReportRecipientModel
                //{
                //    Email = "clydeburgos@gmail.com"
                //});

                var reportEmailModel = new ReportEmailModel
                {
                    recepients = emails.Select(e => e.Email).ToArray(),
                    EmailTitle = reportType,
                    routineName = reportType,
                    siteId = siteId
                };

                var file = File.OpenRead(path);
                
                this.Send(path,reportEmailModel, file, $"{reportType}_{date1.ToString("MM-dd-yyyy")}_{DateTime.Now.ToString("HH:mm")}.pdf", SenderName, additionalBodyHtml);
                file.Close();

                //delete the file
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch(Exception ex)
            {
                var test = "";
            }


        }
        public HttpResponseMessage DownloadDailyReport(DateTime date1, DateTime date2, string reportType, string dumpPath, int siteId, int accountId = 0, DailyReportRecipientService dailyReportRecipientService = null)
        {
            try
            {
                var res = new ReportExecutionService();

                var server = ConfigurationManager.AppSettings["server"];
                string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
                string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

                res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
                string ssrsPath = "";
                switch (server)
                {
                    case "Staging":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    case "Production":
                        ssrsPath = "/SER4/";
                        break;
                    case "Development":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    default:
                        ssrsPath = "/SER4/";
                        res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        break;
                }

                res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;

                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                var parameters = new List<ParameterValue>();
                var reportPath = "";
                switch (reportType)
                {
                    case "ActivityList":
                        reportPath = $"{ssrsPath}Activity/ActivityList";
                        parameters.Add(new ParameterValue
                        {
                            Name = "accountId",
                            Value = accountId.ToString()
                        });
                        break;
                    case "ContractList":
                        reportPath = $"{ssrsPath}Contract/ContractList";
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "PublishedActivity":
                        reportPath = $"{ssrsPath}Activity/PublishedActivity";
                        parameters.Add(new ParameterValue
                        {
                            Name = "SiteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "WeeklyReport":
                        reportPath = $"{ssrsPath}Week Report Template/Week Report Main";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "DailyReport":

                        reportPath = $"{ssrsPath}Daily Report Template/Daily Report";

                        parameters.Add(new ParameterValue
                        {
                            Name = "date",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "MonthReport":
                     //   date1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        reportPath = $"{ssrsPath}Month Report Template/Month Report Main";
                    //    date2 = date1.AddMonths(1).AddDays(-1);
                        parameters.Add(new ParameterValue
                        {
                            Name = "date1",
                            Value = date1.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "date2",
                            Value = date2.ToString("MM-dd-yyyy")
                        });
                        parameters.Add(new ParameterValue
                        {
                            Name = "siteId",
                            Value = siteId.ToString()
                        });
                        break;
                    case "Task":
                        reportPath = $"{ssrsPath}Activity/Task";
                        parameters.Add(new ParameterValue
                        {
                            Name = "TaskId",
                            Value = siteId.ToString()
                        });
                        break;
                }

                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{reportType}-{date1.ToString("MM-dd-yyyy")}{tt}");

                //var file = File.OpenRead(path);
                using (var file = File.OpenRead(path))
                {
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StreamContent(file);

                    if (reportType == "ActivityList")
                    {
                        var accountService = new AccountService();
                        var account = accountService.Get(accountId);
                        response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = $"{reportType}_{account.Company.Replace(" ", "_")}_{date1.ToString("yyyyMMdd_HH:mm")}"
                        };
                    }
                    else if (reportType == "Task")
                    {
                        var taskId = siteId;
                        var company = new TaskService().GetCompanyNameByTaskId(taskId);
                        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = $"{reportType}_{company.Replace(" ", "_")}_{date1:yyyyMMdd_HH:mm}"
                        };
                    }
                    else
                    {
                        var siteservice = new SiteService();
                        var site = siteservice.Get(siteId);
                        response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = $"{reportType}_{site.Name.Replace(" ", "_")}_{date1.ToString("yyyyMMdd_HH:mm")}"
                        };
                    }


                    //response.Content.Headers.ContentDisposition.FileName = "file";
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                    file.Close();
                    return response;
                }
            }
            catch (Exception ex)
            {
               
                var test = "";
                return null;
            }
        }

        public void Send(string dumpPath, IncidentModel model, ReportModel report, string senderName="")
        {
            var incidentService = new IncidentService();
            var dailyReportRecipientService = new DailyReportRecipientService();

            if (report != null)
            {
                var res = new ReportExecutionService();

                string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
                string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

                res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
                var server = ConfigurationManager.AppSettings["server"];
                string ssrsPath = "";
                switch (server)
                {
                    case "Staging":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    case "Production":
                        ssrsPath = "/SER4/";
                        break;
                    case "Development":
                        ssrsPath = "/SER4_Staging/";
                        break;
                    default:
                        ssrsPath = "/SER4/";
                        res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        break;
                }

                res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
                res.ExecutionHeaderValue = new ExecutionHeader();

                string historyId = null;
                var reportPath = $"{ssrsPath}{report.ReportType.Name}";
                var format = "PDF";
                var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";


                var isProd = server.ToLower() == "production";

                var apiUrl = ConfigurationManager.AppSettings[$"apiurl{(isProd ? "": server)}"] ?? "";
                var appUrl = ConfigurationManager.AppSettings[$"appurl{(isProd ? "": server)}"] ?? "";

                var parameters = new List<ParameterValue>()
                {
                    new ParameterValue
                    {
                        Name = "ReportId",
                        Value = $"{report.Id}"
                    },
                    new ParameterValue
                    {
                        Name = "ApiUrl",
                        Value = apiUrl
                    },
                    new ParameterValue
                    {
                        Name = "AppUrl",
                        Value = appUrl
                    }
                };

                switch (report.ReportTypeId)
                {
                    case 1:
                        reportPath = "/SER4/Handelserapport Template/Handelserapport";
                        break;
                    case 2:
                        reportPath = "/SER4/IB-Larm och Ryckrapport Template/IB-Larm-och-Ryckrapport";
                        break;
                    case 3:
                        reportPath = "/SER4/Öppettider Template/Öppettider Main";
                        break;
                    case 4:
                        reportPath = "/SER4/Skadegorelse Klotterrapport Template/SkadegorelseKlotterrapport";
                        break;
                    case 5:
                        reportPath = "/SER4/Viten Template/Viten";
                        break;
                    case 7:
                    default:
                        reportPath = "/SER4/Bistro Solna Template/Bistro Solna";
                        break;
                        return;
                }
                
                var execInfo = res.LoadReport(reportPath, historyId);
                var extension = string.Empty;
                var encoding = string.Empty;
                var mimeType = string.Empty;
                Warning[] warnings;
                string[] streamsIds;

                res.SetExecutionParameters(parameters.ToArray(), "en-US");

                var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
                execInfo = res.GetExecutionInfo();


                if (!Directory.Exists(dumpPath))
                {
                    Directory.CreateDirectory(dumpPath);
                }

                var tt = Guid.NewGuid().ToString();
                var path = Path.Combine(dumpPath, $"{report.ReportType.Name.Replace("/","")}{model.Id}{tt}");

                try
                {
                    using (var stream = File.Create(path, result.Length))
                    {
                        stream.Write(result, 0, result.Length);
                        stream.Close();

                        var reportType = report.ReportType;

                        var emails = dailyReportRecipientService.GetDailyReportRecipients(model.AccountId, model.SiteId, reportType.Name).ToList();

                        var reportEmailModel = new ReportEmailModel
                        {
                            recepients = emails.Select(e => e.Email).ToArray(),
                            EmailTitle = reportType.Name,
                            routineName = reportType.Name,
                            siteId = model.SiteId
                        };


                        var siteService = new SiteService();
                        var siteName = siteService.Get(model.SiteId).Name;

                        var dateTime = DateTime.Now.ToString("yyyyMMdd_HHmm");

                        var file = File.OpenRead(path);

                        var images = new List<string>()
                        {
                            model.Image1,
                            model.Image2,
                            model.Image3,
                            model.Image4,
                            model.Image5,
                        };

                        images = images.Where(e => !string.IsNullOrEmpty(e)).ToList();

                        this.Send(path, reportEmailModel, file, $"{report.ReportType.Name.Replace("/", "").Replace(" ", "_")}_{siteName}_{dateTime}.pdf".Replace(" ", "_"), senderName, attachments: images);
                        file.Close();

                        //delete the file
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                }
                catch (Exception ex)
                {
                    File.Delete(path);
                }
            };
        }

        private void Send(string path,ReportEmailModel model, Stream stream, string fileName, string senderNameBody = "", string additionalBody = "", List<string> attachments = null)
        {
            var emailService = new EmailService();
            var siteService = new SiteService();

            model.routineName = Regex.Replace(model.routineName, @"\s+", "").Replace("/", "");
            if(model.routineName == "IB-LarmochRyckrapport(internal)")
            {
                model.routineName = "IB-LarmochRyckrapport";
            }
            var template = emailService.GetEmailTemplateFromRoutineName(model.routineName);
            var name = "";
            if (model.siteId != 0)
            {
                var site = siteService.Get(model.siteId);
                name = site.Name;
            }

            if (model.recepients != null)
            {
                if (!string.IsNullOrEmpty(senderNameBody))
                {
                    template.Body = template.Body.Replace("{SenderName}", $"<br/>Generated By: {senderNameBody}");
                }
                else
                {
                    template.Body = template.Body.Replace("{SenderName}", $"<br/>{senderNameBody}<br/>");
                }
                
                template.Subject = template.Subject.Replace("{siteName}", name).Replace("{date}",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                var server = ConfigurationManager.AppSettings["server"];
                var preTitle = server.ToLower() == "production" ? "" : "[STAGING] ";

                Service.Tools.Email.SendEmailWithAttachmentToMultipleEmails(path,"", model.recepients.ToList(), $"{preTitle}{template.Subject}", template.Body, string.Empty, stream, fileName,null,false,null, $"SER4 - {name}", attachments: attachments);                   
            }
            
        }

        public void DownloadIncideAccidentDetailsFile(int id, string dumpPath, out string filePath)
        {
            var res = new ReportExecutionService();

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
            string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

            res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
            string ssrsPath = "";
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            reportPath = $"{ssrsPath}IncidentAccident/IncidentAccidentDetails";

            parameters.Add(new ParameterValue
            {
                Name = "Id",
                Value = id.ToString()
            });

            var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                Directory.CreateDirectory(dumpPath);
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"incidentaccidentdetails-{id.ToString()}{tt}.pdf");

            using (var stream = File.Create(path, result.Length))
            {
                stream.Write(result, 0, result.Length);
                stream.Close();

                DateTime date1 = DateTime.Now;

                // var file = File.OpenRead(path);
                var siteservice = new SiteService();
                filePath = path;
            }
            //var stream = File.Create(path, result.Length);
           // return file;
      
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public HttpResponseMessage DownloadContractDetails(int id, string dumpPath)
        {
            var res = new ReportExecutionService();

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsPath = "";
            string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
            string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

            res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            reportPath = $"{ssrsPath}Contract/Contract";

            parameters.Add(new ParameterValue
            {
                Name = "Id",
                Value = id.ToString()
            });

            var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                Directory.CreateDirectory(dumpPath);
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"contract-{id.ToString()}{tt}");

            using (var stream = File.Create(path, result.Length))
            {
                stream.Write(result, 0, result.Length);
                stream.Close();

                DateTime date1 = DateTime.Now;

                var file = File.OpenRead(path);
                var siteservice = new SiteService();
                // var site = siteservice.Get(siteId);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(file);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"Contract_{id.ToString()}_{date1.ToString("yyyyMMdd_HH:mm")}"
                };
                //response.Content.Headers.ContentDisposition.FileName = "file";
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;

            }
        }



        public HttpResponseMessage DownloadIncideAccidentDetails(int id, int accountId, bool isAccident, string dumpPath)
        {
            var res = new ReportExecutionService();
            var reportName = isAccident ? "AccidentReport" : "IncidentReport";

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsPath = "";
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    res.Credentials = new NetworkCredential("itmaskinen", "Itmaskinen123");
                break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            reportPath = $"{ssrsPath}IncidentAccident/{reportName}";

            parameters.Add(new ParameterValue
            {
                Name = "Id",
                Value = id.ToString()
            });


            parameters.Add(new ParameterValue
            {
                Name = "AccountId",
                Value = accountId.ToString()
            });

        var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                Directory.CreateDirectory(dumpPath);
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"incidentaccidentdetails-{id.ToString()}{tt}");

            using (var stream = File.Create(path, result.Length))
            {
                stream.Write(result, 0, result.Length);
                stream.Close();

                DateTime date1 = DateTime.Now;

                var file = File.OpenRead(path);
                var siteservice = new SiteService();
                // var site = siteservice.Get(siteId);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(file);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"IncidentAccidentDetails_{id.ToString()}_{date1.ToString("yyyyMMdd_HH:mm")}"
                };
                //response.Content.Headers.ContentDisposition.FileName = "file";
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
            }
          
        }

        public HttpResponseMessage DownloadIncideAccidentList(DateTime date,DateTime date1data, int siteId, string dumpPath)
        {
            var res = new ReportExecutionService();

            var server = ConfigurationManager.AppSettings["server"];
            string ssrsPath = "";
            string ssrsUsername = ConfigurationManager.AppSettings["ssrsUsername"];
            string ssrsPassword = ConfigurationManager.AppSettings["ssrsPassword"];

            res.Credentials = new NetworkCredential(ssrsUsername, ssrsPassword);
            switch (server)
            {
                case "Staging":
                    ssrsPath = "/SER4_Staging/";
                    break;
                case "Production":
                    ssrsPath = "/SER4/";
                    break;
                case "Development":
                    ssrsPath = "/SER4_Staging/";
                    break;
                default:
                    ssrsPath = "/SER4/";
                    res.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    break;
            }

            res.Url = $"{AppSettings.SSRSEndPoint}ReportExecution2005.asmx";
            res.ExecutionHeaderValue = new ExecutionHeader();

            string historyId = null;

            var format = "PDF";
            var devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            var parameters = new List<ParameterValue>();
            var reportPath = "";
            reportPath = $"{ssrsPath}IncidentAccident/IncidentAccidentList";

            parameters.Add(new ParameterValue
            {
                Name = "date",
                Value = date.ToString("yyyy-MM-dd")
            });

            parameters.Add(new ParameterValue
            {
                Name = "date1",
                Value = date1data.ToString("yyyy-MM-dd")
            });

            parameters.Add(new ParameterValue
            {
                Name = "siteId",
                Value = siteId.ToString()
            });

            var execInfo = res.LoadReport(reportPath, historyId);
            var extension = string.Empty;
            var encoding = string.Empty;
            var mimeType = string.Empty;
            Warning[] warnings;
            string[] streamsIds;

            res.SetExecutionParameters(parameters.ToArray(), "en-US");

            var result = res.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamsIds);
            execInfo = res.GetExecutionInfo();


            if (!Directory.Exists(dumpPath))
            {
                Directory.CreateDirectory(dumpPath);
            }

            var tt = Guid.NewGuid().ToString();
            var path = Path.Combine(dumpPath, $"incidentaccidentList-{date.ToString("yyyy-MM-dd")}{tt}");

            using (var stream = File.Create(path, result.Length))
            {
                stream.Write(result, 0, result.Length);
                stream.Close();

                DateTime date1 = DateTime.Now;

                var file = File.OpenRead(path);
                var siteservice = new SiteService();
                // var site = siteservice.Get(siteId);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(file);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"IncidentAccidentList_{date.ToString("yyyy-MM-dd")}_{date1.ToString("yyyyMMdd_HH:mm")}"
                };
                //response.Content.Headers.ContentDisposition.FileName = "file";
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
            }
        }
    }
}
