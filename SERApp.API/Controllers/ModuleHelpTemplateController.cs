﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class ModuleHelpTemplateController : ApiController
    {
        private ModuleHelpTemplateService _moduleHelpService;
        public ModuleHelpTemplateController()
        {
            _moduleHelpService = new ModuleHelpTemplateService();
        }

        [HttpGet]
        [Route("ModuleHelp/Get")]
        [Authorize]
        public IHttpActionResult Get(int id)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _moduleHelpService.Get(id);
                return new ResponseDataModel<ModuleHelpTemplateModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded the Module Help Template",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("ModuleHelp/GetAll")]
        [Authorize]
        public IHttpActionResult GetAll()
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _moduleHelpService.GetAll();
                return new ResponseDataModel<List<ModuleHelpTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Module Help Templates",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("ModuleHelp/Save")]
        [Authorize]
        public IHttpActionResult Save(ModuleHelpTemplateModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _moduleHelpService.Save(model);
                return new ResponseDataModel<List<ModuleHelpTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved the Template"
                };
            }));
        }

        [HttpPost]
        [Route("ModuleHelp/Delete")]
        [Authorize]
        public IHttpActionResult Delete(ModuleHelpTemplateModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _moduleHelpService.Delete(model.Id);
                return new ResponseDataModel<List<ModuleHelpTemplateModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted the Template",
                };
            }));
        }
    }
}
