﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class TestController : ApiController
    {
        SMSService smsService;
        public TestController() {
            smsService = new SMSService();
        }

        [HttpPost]
        [Route("Test/TestSMS")]
        public IHttpActionResult TestSMS(string number) {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var response = smsService.TestSendSMS(number);
                return new ResponseDataModel<SMSResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Site Data",
                    Data = response.Result
                };
            }));
        }
    }
}
