import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { 
    ModulesComponent,
    AccountModuleComponent, UserModuleRoleComponent,
    AccountsComponent, AccountsCreateComponent, AccountsEditComponent,
    UsersComponent, UsersCreateComponent, UsersEditComponent,
    SitesComponent, SitesCreateComponent, SitesEditComponent,
    TenantsComponent, TenantsListComponent, TenantsCreateComponent, TenantsEditComponent,
    LoansComponent, LoansListComponent, LoansCreateComponent, LoansEditComponent,
    //MOBILE
    MobileLoansLandingComponent, MobileLoansCreateComponent, MobileLoansPreviewComponent, 
    MobileLoansListComponent, MobileLoansReturnComponent, CustomTypeComponent, CustomTypeCreateComponent, CustomTypeEditComponent,
    GuardComponent, GuardCreateComponent, GuardEditComponent, IncidentTypesComponent, IncidentTypesCreateComponent, IncidentTypesEditComponent, TagsComponent, TagsCreateComponent, TagsEditComponent, TagLogsComponent, TagLogsCreateComponent, TagLogsEditComponent
} from '../../components';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            //TEMPLATE MODULES (let's remove this as soon as we can finish majority of the modules to copy from)
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            //MAIN MODULES
            { path: 'modules', component:  ModulesComponent},
            { path: 'accounts/modules', component:  AccountModuleComponent },
            { path: 'accounts/create', component:  AccountsCreateComponent },
            { path: 'accounts', component:  AccountsComponent},
            { path: 'accounts/:id/edit', component:  AccountsEditComponent },
            { path: 'accounts/:id/edit/:mode', component:  AccountsEditComponent },
            { path: 'users', component:  UsersComponent},
            { path: 'users/create', component:  UsersCreateComponent },
            { path: 'users/create/:account', component:  UsersCreateComponent },
            { path: 'users/:id/edit', component:  UsersEditComponent },
            { path: 'users/:id/module-roles/:account', component:  UserModuleRoleComponent },
            { path: 'objects', component:  SitesComponent},
            { path: 'objects/create', component:  SitesCreateComponent },
            { path: 'objects/:id/edit', component:  SitesEditComponent },
            { path: 'objects/create/:account', component:  SitesCreateComponent },
            { path: 'tenants', component:  TenantsComponent },
            { path: 'tenants/create', component:  TenantsCreateComponent },
            { path: 'tenants/:id/edit', component:  TenantsEditComponent },
            { path: 'loans', component:  LoansComponent },
            { path: 'loans/create', component:  LoansCreateComponent },
            { path: 'loans/:id/edit', component:  LoansEditComponent },
            //MOBILE
            { path: 'mobile/loans', component:  MobileLoansLandingComponent },
            { path: 'mobile/loans/list', component:  MobileLoansListComponent },
            { path: 'mobile/loans/create', component:  MobileLoansCreateComponent },
            { path: 'mobile/loans/:id/preview', component:  MobileLoansPreviewComponent },
            { path: 'mobile/loans/preview', component:  MobileLoansPreviewComponent },
            { path: 'mobile/loans/return', component:  MobileLoansReturnComponent },
            { path: 'mobile/loans/:id/return', component:  MobileLoansReturnComponent },

            { path: 'custom-types', component:  CustomTypeComponent },
            { path: 'custom-types/create', component:  CustomTypeCreateComponent },
            { path: 'custom-types/:id/edit', component:  CustomTypeEditComponent },

            { path: 'guards', component:  GuardComponent },
            { path: 'guards/create', component:  GuardCreateComponent },
            { path: 'guards/:id/edit', component:  GuardEditComponent },

            { path: 'incident-types', component:  IncidentTypesComponent },
            { path: 'incident-types/create', component:  IncidentTypesCreateComponent },
            { path: 'incident-types/:id/edit', component:  IncidentTypesEditComponent },

            { path: 'incidents', component:  IncidentTypesComponent },
            { path: 'incidents/create', component:  IncidentTypesCreateComponent },
            { path: 'incidents/:id/edit', component:  IncidentTypesEditComponent },

            { path: 'tags', component:  TagsComponent },
            { path: 'tags/create', component:  TagsCreateComponent },
            { path: 'tags/:id/edit', component:  TagsEditComponent },

            { path: 'tag-logs', component:  TagLogsComponent },
            { path: 'tag-logs/create', component:  TagLogsCreateComponent },
            { path: 'tag-logs/:id/edit', component:  TagLogsEditComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
