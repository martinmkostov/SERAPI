﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.API.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Net.Http;

namespace SEPAppBlack.Controllers
{
    public class LoanController : ApiController
    {
        private LoanService _loanService;
        public LoanController()
        {
            _loanService = new LoanService();
        }

        [HttpGet]
        [Route("Loan/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.Get(id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Customer Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAll();
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllMergedCustomer")]
        public IHttpActionResult GetAllMergedCustomer(int id, string keyword = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllMergedCustomer(id, keyword);
                return new ResponseDataModel<List<LoanCustomerModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Merged Customers Data List",
                    Data = data,
                    result = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllByAccountId")]
        public IHttpActionResult GetAllByAccountId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllByAccountId(id);
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllLoanTypes")]
        public IHttpActionResult GetAllLoanTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllLoanTypes();
                return new ResponseDataModel<List<LoanTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Types List",
                    Data = data
                };
            }));
        }


        [HttpGet]
        public IHttpActionResult GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType)
        {
            
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllWithFilter(customerId, typeId, statusId, itemName, LoanedDate, ReturnedDate, confirmationType);
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Filtered Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Save")]
        public IHttpActionResult SaveLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.SaveLoan(model);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Loan Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Confirm")]
        public IHttpActionResult ConfirmLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.ConfirmLoan(model.Customer.Email, model.Id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Confirmed Loan",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Return")]
        public IHttpActionResult ReturnLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.ReturnLoan(model.Id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Returned Loan",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Delete")]
        public IHttpActionResult DeleteLoan(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.DeleteLoan(id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Loan Record"
                };
            }));
        }

    }
}