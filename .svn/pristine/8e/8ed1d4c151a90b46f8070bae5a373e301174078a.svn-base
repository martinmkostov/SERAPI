﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueWebController : ApiController
    {
        private readonly IIssueWebService _service;
        public IssueWebController()
        {
            _service = new IssueWebService();
        }

        [HttpGet]
        [Route("Issue/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetById(id);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Issue/GetAll")]
        public IHttpActionResult GetAll(int accountId, int siteId =0, int statusId = -1, int contactId =0,int issueType1Id =0,int issueType2Id =0,int issueType3Id =0,string searchText ="")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueWeb(accountId, siteId,statusId,contactId,issueType1Id,issueType2Id,issueType3Id,searchText).OrderBy(x=>x.DateCreated);
                return new ResponseDataModel<IEnumerable<IssueWebModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Save")]
        public IHttpActionResult Save(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/SaveLog")]
        public IHttpActionResult SaveLog(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWebLog(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/CloseIssue")]
        public IHttpActionResult CloseIssue(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                
                _service.CloseIssue(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Confirm")]
        public IHttpActionResult Confirm(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model, true);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Delete")]
        public IHttpActionResult Delete(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIssueWeb(model.IssueWebId);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Issue/Reminder")]
        public IHttpActionResult Reminder(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.sendReminder(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent reminder",
                    Data = model
                };
            }));
        }
    }
}
