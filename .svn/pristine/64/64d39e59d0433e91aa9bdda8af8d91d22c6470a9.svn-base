﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class EmailService
    {
        private string _superAdminOverride = ConfigurationManager.AppSettings["superadminEmailTemplateOverride"];
        private bool superAdminOverride = false;

        EmailRepository _emailRepository;
        LogRepository _logRepository;

        public EmailService() {
            _emailRepository = new EmailRepository();
            _logRepository = new LogRepository();

            superAdminOverride = !string.IsNullOrEmpty(_superAdminOverride) ? bool.Parse(_superAdminOverride) : false;
        }

        public EmailTemplateModel GetEmailTemplate(int id)
        {
            try
            {
                var data = _emailRepository.GetEmailTemplate(id);
                return entityToModel(data);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public AccountEmailTemplateModel GetAccountEMailTemplate(int id)
        {
            try
            {
                var data = _emailRepository.GetAccountEmailTemplate(id);
                return entityToModel(data);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<EmailTemplateTranslationModel> GetAllEmailTemplate(int languageId = 1, int moduleId = 0)
        {
            if (moduleId == -1)
            {
                return new List<EmailTemplateTranslationModel>();
            }

            var data = moduleId > 0 ? _emailRepository.GetAllEmailTemplates().Where(e => e.ModuleId == moduleId) : _emailRepository.GetAllEmailTemplates();

            var test = GetEmailTemplateId(data.First().Id, languageId);

            return data.Select(e => new EmailTemplateTranslationModel()
            {
                Id = GetEmailTemplateId(e.Id, languageId) > 0 ? GetEmailTemplateId(e.Id, languageId) : e.Id,
                EmailTemplateId = e.Id,
                LanguageId = languageId,
                LanguagenName = _emailRepository.GetAllLanguages().Where(l => l.Id == languageId).SingleOrDefault().Name,
                RoutineName = e.RoutineName,
                Description = e.Description,
                Subject = superAdminOverride ? e.Subject : GetEmailTemplateSubject(e.Id, languageId),
                Body = superAdminOverride ? e.Body : GetEmailTemplateBody(e.Id, languageId),
                SMSBody = superAdminOverride ? string.IsNullOrEmpty(e.SMSBody) ? "Not Set" : e.SMSBody : GetSMSTemplateBody(e.Id, languageId),
                IsSystem = e.IsSystem
            }).ToList();
        }

        public int GetEmailTemplateId(int emailTemplateId, int languageId) {
            var data = _emailRepository.GetEmailTemplateTranslation(emailTemplateId, languageId);
            if (data != null)
            {
                return data.Id;
            }
            return 0;
        }

        public string GetEmailTemplateSubject(int emailTemplateId, int languageId) {
            var data = _emailRepository.GetEmailTemplateTranslation(emailTemplateId, languageId);
            if (data != null) {
                return !string.IsNullOrEmpty(data.Subject) ? data.Subject : "Subject (Not Set)";
            }
            return "Subject (Not Set)";
        }

        public string GetEmailTemplateBody(int emailTemplateId, int languageId)
        {
            var data = _emailRepository.GetEmailTemplateTranslation(emailTemplateId, languageId);
            if (data != null)
            {
                return !string.IsNullOrEmpty(data.Body) ? data.Body : "Body (Not Set)";
            }
            return "Body (Not Set)";
        }

        public string GetSMSTemplateBody(int emailTemplateId, int languageId)
        {
            var data = _emailRepository.GetEmailTemplateTranslation(emailTemplateId, languageId);
            if (data != null)
            {
                return !string.IsNullOrEmpty(data.SMSBody) ? data.SMSBody : "SMS Body (Not Set)";
            }
            return "SMS Body (Not Set)";
        }

        public List<EmailTemplateModel> GetAllNonSystemEmailTemplates()
        {
            var data = _emailRepository.GetAllNonSystemEmailTemplates().ToList();
            return data.Select(e => new EmailTemplateModel()
            {
                Id = e.Id,
                RoutineName = e.RoutineName,
                Description = e.Description,
                Subject = e.Subject,
                Body = e.Body,
                IsSystem = e.IsSystem
            }).ToList();
        }

        public List<AccountEmailTemplateModel> GetAllAccountEmailTemplate(int id){
            var data = _emailRepository.GetAllAccountEmailTemplateById(id)
                .Select(e => new {
                    Id = e.Id,
                    AccountId = e.AccountId,
                    EmailTemplateId = e.EmailTemplateId,
                    Subject = e.Subject,
                    Body = e.Body,
                    RoutineName = _emailRepository.GetEmailTemplate(e.EmailTemplateId).RoutineName,
                    Description = _emailRepository.GetEmailTemplate(e.EmailTemplateId).Description,
                    SMSBody = e.SMSBody
                })
                .ToList();

            var accountTemplates = GetAllNonSystemEmailTemplates()
                .Select(e => new {
                    Id = 0,
                    AccountId = 0,
                    EmailTemplateId = e.Id,
                    Subject = e.Subject,
                    Body = e.Body,
                    RoutineName = e.RoutineName,
                    Description = e.Description,
                    SMSBody = e.SMSBody
                })
                .ToList();

            var merged = data.Union(accountTemplates)
                .GroupBy(a => a.EmailTemplateId, a => new
                {
                    Id = a.Id,
                    AccountId = a.AccountId,
                    EmailTemplateId = a.EmailTemplateId,
                    Subject = a.Subject,
                    Body = a.Body,
                    RoutineName = a.RoutineName,
                    Description = a.Description,
                    SMSBody = a.SMSBody
                })
                .ToList();

            return merged.Select(e => new AccountEmailTemplateModel()
            {
                Id = e.FirstOrDefault().Id,
                RoutineName = e.FirstOrDefault().RoutineName,
                Description = e.FirstOrDefault().Description,
                AccountId = e.FirstOrDefault().AccountId,
                EmailTemplateId = e.FirstOrDefault().EmailTemplateId,
                Subject = !string.IsNullOrEmpty(e.FirstOrDefault().Subject) ? e.FirstOrDefault().Subject : "Subject (Not Subject)",
                Body = !string.IsNullOrEmpty(e.FirstOrDefault().Body) ? e.FirstOrDefault().Body : "Body (Not Set)",
                SMSBody = !string.IsNullOrEmpty(e.FirstOrDefault().SMSBody) ? e.FirstOrDefault().SMSBody : "SMSBody (Not SMSBody)"
            }).ToList();
        }

        public void SaveEmailTemplate(EmailTemplateTranslationModel model) {
            try {
                _emailRepository.SaveEmailTemplate(model);
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void SaveAccountEmailTemplate(AccountEmailTemplateModel model) {
            try {
                _emailRepository.SaveAccountEmailTemplate(model);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public AccountEmailTemplateModel GetEmailTemplate(string routineName) {
            var data = _emailRepository.GetAccountEmailTemplate(routineName, 0);
            return entityToModel(data);
        }

        public AccountEmailTemplateModel GetAccountEmailTemplate(string routineName, int accountId) {
            var data = _emailRepository.GetAccountEmailTemplate(routineName, accountId);
            return entityToModel(data);
        }

        public EmailTemplateModel entityToModel(EmailTemplate entity) {
            return new EmailTemplateModel() {
                Id = entity.Id,
                RoutineName = entity.RoutineName,
                Description = entity.Description,
                Subject = entity.Subject,
                Body = entity.Body,
                IsSystem = entity.IsSystem
            };
        }

        public AccountEmailTemplateModel entityToModel(AccountEmailTemplate entity) {
            return new AccountEmailTemplateModel() {
                Id = entity.Id,
                AccountId = entity.AccountId,
                EmailTemplateId = entity.EmailTemplateId,
                Subject = entity.Subject,
                Body = entity.Body,
                SMSBody = entity.SMSBody
            };
        }

        public EmailTemplateModel GetEmailTemplateFromRoutineName(string routineName)
        {
            var data = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
            var translation = _emailRepository.GetTranslantionByEmailTemplateId(data.Id);

            data.Subject = translation.Subject;
            data.Body = translation.Body;
            return new EmailTemplateModel()
            {
                Body = data.Body,
                Description = data.Description,
                Id = data.Id,
                IsSystem = data.IsSystem,
                RoutineName = data.RoutineName,
                SMSBody = data.SMSBody,
                Subject = data.Subject
            };
        }
    }
}
