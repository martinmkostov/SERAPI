﻿using Newtonsoft.Json;
using SERApp.API.Extensions;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.API.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Net.Http;

namespace SEPAppBlack.Controllers
{
    public class LoanController : ApiController
    {
        private LoanService _loanService;
        public LoanController()
        {
            _loanService = new LoanService();
        }

        [HttpGet]
        [Route("Loan/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.Get(id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Loan Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/Get")]
        public IHttpActionResult Get(string loanId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetLoanByLoanId(loanId);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Loan Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAll();
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllMergedCustomer")]
        public IHttpActionResult GetAllMergedCustomer(int id, string keyword = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllMergedCustomer(id, keyword);
                return new ResponseDataModel<List<LoanCustomerModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Merged Customers Data List",
                    Data = data,
                    result = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllByAccountId")]
        public IHttpActionResult GetAllByAccountId(int id,int siteId, string keyword = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllByAccountId(id, siteId, keyword);
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetAllLoanTypes")]
        public IHttpActionResult GetAllLoanTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllLoanTypes();
                return new ResponseDataModel<List<LoanTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Loan Types List",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Loan/IsCustomerAllowedForMoreLoans")]
        public IHttpActionResult IsCustomerAllowedForMoreLoans(int customerId, int customerTypeId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.IsCustomerAllowedForMoreLoans(customerId, customerTypeId);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Validated Customer",
                    Extras = data
                };
            }));
        }

        [HttpGet]
        public IHttpActionResult GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _loanService.GetAllWithFilter(customerId, typeId, statusId, itemName, LoanedDate, ReturnedDate, confirmationType);
                return new ResponseDataModel<List<LoanModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Filtered Loan Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Save")]
        public IHttpActionResult SaveLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.SaveLoan(model);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Loan Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Confirm")]
        public IHttpActionResult ConfirmLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.ConfirmLoan(model.Email, model.Id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Confirmed Loan",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Return")]
        public IHttpActionResult ReturnLoan(LoanModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.ReturnLoan(model.Id);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Returned Loan",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Loan/Delete")]
        public IHttpActionResult DeleteLoan(LoanDeleteParamModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.DeleteLoan(model);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Loan Record"
                };
            }));
        }

        [HttpPost]
        [Route("Loan/SendMultipleEmails")]
        public IHttpActionResult SendMultipleEmails(LoanEmailParamModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.SendMultipleEmails(model.command, model.ids);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent Email Notification"
                };
            }));
        }

        [HttpGet]
        [Route("Loan/DeleteReturnedLoans")]
        public IHttpActionResult DeleteReturnedLoans(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.DeleteReturnedLoans(accountId);
                return new ResponseDataModel<LoanModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent Email Notification"
                };
            }));
        }

        [HttpGet]
        [Route("Loan/GetLoanTypesByAccountId")]
        public IHttpActionResult GetLoanTypesByAccountId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data =_loanService.GetLoanTypesByAccountId(id);
                return new ResponseDataModel<List<LoanTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Loan Types for this Account",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Loan/SaveLoanType")]
        public IHttpActionResult SaveLoanType(LoanTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _loanService.SaveLoanType(model);
                return new ResponseDataModel<LoanTypeModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Loan Type for this Account":"Loan Type already exists"
                };
            }));
        }

        [HttpPost]
        [Route("Loan/DeleteLoanType")]
        public IHttpActionResult DeleteLoanType(LoanTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _loanService.DeleteLoanType(model.Id);
                return new ResponseDataModel<LoanTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Loan Type for this Account"
                };
            }));
        }
    }
}