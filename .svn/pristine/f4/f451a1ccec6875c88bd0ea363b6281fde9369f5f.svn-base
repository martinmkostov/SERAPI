﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TagLogController : ApiController
    {
        private TagLogService _service;
        public TagLogController()
        {
            _service = new TagLogService();
        }

        [HttpGet]
        [Route("TagLog/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagLogById(id);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded TagLog",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("TagLog/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagLogs();
                return new ResponseDataModel<IEnumerable<TagLogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded TagLogs",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("TagLog/Save")]
        public IHttpActionResult Save(TagLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddTagLog(model);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tag Log",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("TagLog/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteTagLogById(id);
                return new ResponseDataModel<TagLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted TagLog",
                };
            }));
        }
    }
}
