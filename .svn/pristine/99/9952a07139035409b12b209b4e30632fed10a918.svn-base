﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TagController : ApiController
    {
        private TagService _service;
        public TagController()
        {
            _service = new TagService();
        }

        [HttpGet]
        [Route("Tag/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTagById(id);
                return new ResponseDataModel<TagModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tag",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tag/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetTags();
                return new ResponseDataModel<IEnumerable<TagModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tags",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tag/Save")]
        public IHttpActionResult Save(TagModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddTag(model);
                return new ResponseDataModel<TagModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tag",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tag/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteTagById(id);
                return new ResponseDataModel<IncidentTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Tag",
                };
            }));
        }
    }
}
