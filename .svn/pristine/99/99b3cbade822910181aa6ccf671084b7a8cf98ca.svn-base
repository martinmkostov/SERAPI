﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueWebController : ApiController
    {
        private readonly IIssueWebService _service;
        public IssueWebController()
        {
            _service = new IssueWebService();
        }

        [HttpGet]
        [Route("Issue/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetById(id);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Issue/GetAll")]
        public IHttpActionResult GetAll([FromBody]IssueFilter filter)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueWeb(filter.accountId, filter.siteId, filter.statusId, filter.contactId, filter.issueType1Id, filter.issueType2Id, filter.issueType3Id, filter.searchText, filter.fromDate,filter.toDate).OrderBy(x=>x.DateCreated);
                return new ResponseDataModel<IEnumerable<IssueWebModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Issue/DownloadCSV")]
        public IHttpActionResult DownloadCSV([FromBody]IssueFilter filter)
        {
       
           

            return Ok(this.ConsistentApiHandling(() =>
            {
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write("Hello, World!");
                writer.Flush();
                stream.Position = 0;

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Export.csv" };
                var data = result.Content.ReadAsByteArrayAsync().Result;

                return new ResponseDataModel<FileResultModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = new FileResultModel()
                    {
                        Data = data
                    }
                };
            }));
        }

        [HttpGet]
        [Route("Issue/GetLogs")]
        public IHttpActionResult GetLogs(int issueWebId, bool showHidden = false)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueWebLog(issueWebId,showHidden).OrderByDescending(x => x.IssueWebHistoryId).ToList();
                return new ResponseDataModel<IEnumerable<IssueWebHistoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Save")]
        public IHttpActionResult Save(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/SaveLog")]
        public IHttpActionResult SaveLog(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWebLog(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/SendDetails")]
        public IHttpActionResult SendDetails(SendDetailsModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SendDetailsModel(model);
                return new ResponseDataModel<SendDetailsModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/CloseIssue")]
        public IHttpActionResult CloseIssue(IssueWebHistoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                
                _service.CloseIssue(model);
                return new ResponseDataModel<IssueWebHistoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Confirm")]
        public IHttpActionResult Confirm(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model, true);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Delete")]
        public IHttpActionResult Delete(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIssueWeb(model.IssueWebId);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Issue/Reminder")]
        public IHttpActionResult Reminder(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.sendReminder(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent reminder",
                    Data = model
                };
            }));
        }
    }
}
