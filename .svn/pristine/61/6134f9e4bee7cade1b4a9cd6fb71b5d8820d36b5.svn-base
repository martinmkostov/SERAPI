﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIncidentTypeService
    {
        IEnumerable<IncidentTypeModel> GetIncidentTypes();
        IncidentTypeModel GetIncidentType(int id);

        void DeleteIncidentTypeById(int id);
        void AddIncidentType(IncidentTypeModel model);
        IncidentTypeModel GetIncidentTypeById(int id);
    }
    public class IncidentTypeService : IIncidentTypeService
    {
        private IRepository<IncidentType> _repository;
        public IncidentTypeService()
        {
            _repository = new Repository<IncidentType>();
        }

        public void AddIncidentType(IncidentTypeModel model)
        {
            _repository.Save(new IncidentType()
            {
               IsReportRequired = model.IsReportRequired,
               ShownReport = model.ShownReport,
               Name = model.Name,
               RequiredReport = model.RequiredReport
            });
        }

        public void DeleteIncidentTypeById(int id)
        {
            _repository.Delete(id);
        }

        public IncidentTypeModel GetIncidentType(int id)
        {
            var existingData = _repository.Get(id);
            return new IncidentTypeModel()
            {
                Id = existingData.Id,
                IsReportRequired = existingData.IsReportRequired,
                Name = existingData.Name,
                RequiredReport = existingData.RequiredReport,
                ShownReport = existingData.ShownReport
            };
        }

        public IncidentTypeModel GetIncidentTypeById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IncidentTypeModel> GetIncidentTypes()
        {
            return _repository.GetAll().Select(x => new IncidentTypeModel()
            {
                Id = x.Id,
                IsReportRequired = x.IsReportRequired,
                Name = x.Name,
                RequiredReport = x.RequiredReport,
                ShownReport = x.ShownReport
            }).ToList();
        }
    }
}
