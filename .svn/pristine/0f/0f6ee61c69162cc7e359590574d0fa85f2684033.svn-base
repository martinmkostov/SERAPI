﻿
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IType2Service
    {
        void AddType2(Type2Model model);
        void UpdateType2(Type2Model model);
        bool DeleteType2ById(int id);
        IEnumerable<Type2Model> GetAllType2(int accountId);
        Type2Model GetType2ById(int id);
    }

    public class Type2Service : IType2Service
    {
        private IRepository<Type2> _repository;
        private IIncidentRepository _incidentRepository;
        public Type2Service()
        {
            _incidentRepository = new IncidentRepository();
            _repository = new Repository<Type2>();
        }

        public void AddType2(Type2Model model)
        {
            if (model.Id == 0)
            {
                if (_repository.GetAll().Any(x => x.Name == model.Name))
                {
                    return;
                }

                _repository.Save(new Type2()
                {
                    CreatedDate = DateTime.Now,
                    Id = model.Id,
                    LastUpdatedDate = model.LastUpdatedDate,
                    Name = model.Name,
                    AccountId = model.AccountId
                });

            }
            else
            {
                UpdateType2(model);
            }

        }

        public void UpdateType2(Type2Model model)
        {
            var existingCustomType = _repository.Get(model.Id);
            existingCustomType.LastUpdatedDate = DateTime.Now;
            existingCustomType.Name = model.Name;
            existingCustomType.AccountId = model.AccountId;
            
            _repository.Update(existingCustomType);
        }

        public bool DeleteType2ById(int id)
        {
            if (_incidentRepository.GetAll().Any(r => r.Type2Id.HasValue && r.Type2Id == id))
            {
                return false;
            }

            _repository.Delete(id);

            return true;
        }

        public IEnumerable<Type2Model> GetAllType2(int accountId)
        {
            return _repository.GetAll().Where(x=>x.AccountId == accountId).Select(x=> new Type2Model()
            {
                CreatedDate = x.CreatedDate,
                Id = x.Id,
                LastUpdatedDate = x.LastUpdatedDate,
                Name = x.Name,
                AccountId = x.AccountId
            }).ToList();
        }

        public Type2Model GetType2ById(int id)
        {
            //use automapper for easy mapping
            var model = _repository.Get(id);
            
            return new Type2Model()
            {
                CreatedDate = model.CreatedDate,
                LastUpdatedDate = model.LastUpdatedDate,
                Name = model.Name,
                Id = model.Id
            };
        }
    }
}
