﻿using SERApp.Models;
using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using SERApp.Models.Constants;

namespace SERApp.Service.Services
{
    public class AuthenticationService
    {
        UserRepository _userRepository;
        LogRepository _logRepository;
        UserModuleRoleRepository _userRoleModuleRepository;
        EmailRepository _emailRepository;
        UserService userService;
        public AuthenticationService() {
            _userRepository = new UserRepository();
            _userRoleModuleRepository = new UserModuleRoleRepository();
            _logRepository = new LogRepository();
            _emailRepository = new EmailRepository();
            userService = new UserService();
        }

        public AuthResponseModel Authenticate(string userName, string password) {
            var user = _userRepository.GetAllActive().Where(u => u.UserName.ToLower() == userName.ToLower()).SingleOrDefault();
            try {
                if (user != null)
                {
                    if (Security.IsPasswordValid(password, user.HashedPassword))
                    {
                        var currentUser = new UserModel()
                        {
                            Id = user.Id,
                            IsActive = user.IsActive,
                            IsDeleted = user.IsDeleted,
                            CreatedDate = user.CreatedDate,
                            LastUpdatedDate = user.LastUpdatedDate,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Mobile = user.Mobile,
                            PhoneNumber = user.PhoneNumber,
                            UserName = user.UserName,
                            AccountId = user.AccountId,
                            RoleName = _userRoleModuleRepository.GetUserRole(user.Id) != null ? _userRoleModuleRepository.GetUserRole(user.Id).Name : "",
                            Account = AccountService.entityToModel(user.Account)
                        };
                        _logRepository.Log(LogTypeEnum.Information, LogMessagingSettings.ActionLogin, LogMessagingSettings.MessageLogin, user.AccountId, user.Id);
                        return new AuthResponseModel()
                        {
                            UserName = user.UserName,
                            IsAuthenticated = true,
                            AuthResponseMessage = LogMessagingSettings.ResponseLoginOk,
                            CurrentUser = currentUser
                        };

                    }
                    else
                    {
                        //invalid password
                        _logRepository.Log(LogTypeEnum.Warning, LogMessagingSettings.ActionLogin, 
                            LogMessagingSettings.MessageLoginFailed +  " - " + LogMessagingSettings.ResponseLoginInvalidPass, 
                            user.AccountId, user.Id);
                        return new AuthResponseModel()
                        {
                            UserName = user.UserName,
                            IsAuthenticated = false,
                            AuthResponseMessage = LogMessagingSettings.MessageLoginFailed
                        };
                    }
                }
                else
                {
                    //no record found with this username
                    _logRepository.Log(LogTypeEnum.Warning, LogMessagingSettings.ActionLogin,
                        LogMessagingSettings.MessageLoginFailed + " - " + LogMessagingSettings.ResponseLoginNoUserRecord,
                        user.AccountId, user.Id);
                    return new AuthResponseModel()
                    {
                        UserName = userName,
                        IsAuthenticated = false,
                        AuthResponseMessage = LogMessagingSettings.ResponseLoginNoUserRecord
                    };
                }
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.ActionLogin, user != null ? user.AccountId : 0, user != null ? user.Id : 0);
                return null;
            }
            
        }

        public AuthResponseModel LogOff(int userId) {
            var user = _userRepository.GetAllActive().Where(u => u.Id == userId).SingleOrDefault();
            try {
                if (user != null)
                {
                    //check if user has pending log in session, then log off
                    if (_logRepository.GetLog(LogTypeEnum.Information, LogMessagingSettings.ActionLogin, user.AccountId, userId) != null) 
                    {
                        _logRepository.Log(LogTypeEnum.Information, LogMessagingSettings.ActionLogoff, LogMessagingSettings.ResponseLogoffOk, user.AccountId, user.Id);
                    }
                    else {
                        return new AuthResponseModel()
                        {
                            IsAuthenticated = false,
                            AuthResponseMessage = LogMessagingSettings.ResponseAlreadyLogoff
                        };
                    }
                    
                    return new AuthResponseModel()
                    {
                        IsAuthenticated = false,
                        AuthResponseMessage =  LogMessagingSettings.ResponseLogoffOk
                    };
                }
                return null;

            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.ActionLogin, user != null ? user.AccountId : 0, user != null ? user.Id : 0);
                return null;
            }
        }

        public UserModel CreateAuth(string password) {
            string hashedPassword = Security.CreateSaltedHashedPassword(password);
            return new UserModel() {
                HashedPassword = hashedPassword,
                RandomSecret = hashedPassword.Split(':')[0]
            };
        }

        public void SendPasswordToEmailAddress(string userName, string emailAddress) {
            var user = _userRepository.GetAllActive().Where(u => u.UserName.ToLower() == userName.ToLower() && u.EmailAddress == emailAddress).SingleOrDefault();
            if (user != null)
            {
                string tempPassword = Security.GenerateTempPassword();

                //UPDATE PW and AUTH Properties w/ temp password
                UserModel model = userService.entityToModel(user);
                UserModel authModel = CreateAuth(tempPassword);

                model.Password = tempPassword;
                model.HashedPassword = authModel.HashedPassword;
                model.RandomSecret = authModel.RandomSecret;

                 _userRepository.SaveUser(model);

                string changePassLink = Email.AppUrl + "?changePass=1&login=" + Security.ChangePasswordLink(model.UserName, model.Password);

                var data = _emailRepository.GetEmailTemplateFromRoutineName("ForgotPassword");
                var translation = _emailRepository.GetTranslantionByEmailTemplateId(data.Id);
                data.Subject = translation.Subject;
                data.Body = translation.Body;

                Email.SendEmail(new SERApp.Models.Common.EmailModel()
                {
                    To = emailAddress,
                    Subject = data.Subject,
                    Body = data.Body.Replace("{changePassLink}",changePassLink)
                });
            }
        }
    }
}
