import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class UserService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getUsersList(page:number, pageSize: number = 10): Observable<IPagedResults<any>>{
        let url = `User/GetAll`;    
        return super.getAll<any>(url);
    }

     public getUsersByAccountList(id: number): Observable<IPagedResults<any>>{
        let url = `User/GetByAccountId?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getUser(id?: number): Observable<any>{
         let url = `User/Get?id=${id}`;
        return super.getAll<any>(url);
    }

    public saveUser(data){
        let url = `User/Save`;
        return super.save<any>(url,data);
    }

    public deleteUser(data){
         let url = `User/Delete`;
        return super.save<any>(url,data);
    }
}
