import { Component, OnInit } from '@angular/core';
import { IncidentTypeModel } from '../../models';
import { IncidentTypeService } from '../../services/incident-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-incident-types-edit',
  templateUrl: './incident-types-edit.component.html',
  styleUrls: ['./incident-types-edit.component.scss']
})
export class IncidentTypesEditComponent implements OnInit {

  photoSrc: string;
  isAdminOrUserEditor: boolean;
  sitesList: any[];
  currentUser: any;
  showSpinner: boolean;
  model: IncidentTypeModel = <IncidentTypeModel>
  {
     
  };
  constructor(private incidentTypeService: IncidentTypeService,
    private route: ActivatedRoute,    
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();

    this.route.params.subscribe((params) => {         
      if (params) {
          const id = params['id'];
          this.loadData(id);
      }
  });

  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

  public loadData(id: number){
    this.showSpinner = true;
        
    this.incidentTypeService.getIncidentType(id)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
      this.model = response.Data;      
           
      this.toastr.success(response.Message, "Success"); 
    });      
}


public saveChanges() : void{
  //alert('test');
  this.showSpinner = true;
  this.incidentTypeService.saveIncidentType(this.model)
  .catch((err: any) => {
      this.toastr.error(err, 'Error');
      return Observable.throw(err);
  }).finally(() => {
       setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response) => {
      if(response.ErrorCode)
      {
          this.toastr.error(response.Message, "Error"); 
      }
      else {
          this.toastr.success(response.Message, "Success"); 
      }
      this.ngOnInit();
  });
}
}
