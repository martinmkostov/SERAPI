using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class LoanMap : EntityTypeConfiguration<Loan>
    {
        public LoanMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ItemNameId)
                .HasMaxLength(150);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.Notes)
                .HasMaxLength(500);

            this.Property(t => t.PhotoUrl)
                .HasMaxLength(550);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.LastModifiedBy)
                .HasMaxLength(50);

            this.Property(t => t.GivenBy)
                .HasMaxLength(50);

            this.Property(t => t.ReceivedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Loans");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.CustomerId).HasColumnName("CustomerId");
            this.Property(t => t.CustomerTypeId).HasColumnName("CustomerTypeId");
            this.Property(t => t.TypeId).HasColumnName("TypeId");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.PhotoUrl).HasColumnName("PhotoUrl");
            this.Property(t => t.LoanedDate).HasColumnName("LoanedDate");
            this.Property(t => t.ReturnedDate).HasColumnName("ReturnedDate");
            this.Property(t => t.MessageTypeId).HasColumnName("MessageTypeId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastModifiedDate).HasColumnName("LastModifiedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.LastModifiedBy).HasColumnName("LastModifiedBy");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.GivenBy).HasColumnName("GivenBy");
            this.Property(t => t.ReceivedBy).HasColumnName("ReceivedBy");
            this.Property(t => t.ConfirmedDate).HasColumnName("ConfirmedDate");
            this.Property(t => t.ActualReturnedDate).HasColumnName("ActualReturnedDate");
            this.Property(t => t.LoanId).HasColumnName("LoanId");
            this.Property(t => t.ItemNameId).HasColumnName("ItemNameId");
        }
    }
}
