import { Component, OnInit } from '@angular/core';
import { IncidentService } from '../../services/incident.service';
import { SiteService, TenantService, CustomTypeService, GuardService } from '../../services';
import { IncidentTypeService } from '../../services/incident-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IncidentModel, GuardModel, IncidentGuardModel } from '../../models';
import { Observable } from 'rxjs/Observable';
import {GrowlModule,Message} from 'primeng/primeng';
import { IPagedResults } from '../../core/interfaces';


@Component({
  selector: 'app-incidents-edit',
  templateUrl: './incidents-edit.component.html',
  styleUrls: ['./incidents-edit.component.scss']
})
export class IncidentsEditComponent implements OnInit {

  isAdminOrUserEditor: boolean;
  photoSrc: string;
  SiteId: number = 0;
  tenants: any[];
  model : any;
  incidenttypes: any[];
  customTypes: any[];
  sites: any[];
  currentUser: any;
  SelectedDate: Date;
  SelectedGuards: any[] = [];
  Guards: any[] = [];
  showGuards: boolean = false;
  public showSpinner: boolean = false;
  constructor(private incidentService: IncidentService, 
    private siteService: SiteService,
    private tenantService: TenantService,
    private incidentTypeService: IncidentTypeService,
    private customTypeService: CustomTypeService,
    private guardService: GuardService,    
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();
    this.model = new IncidentModel(); 
    this.model.CustomTypeId = 0;   
    this.model.IncidentTypeId = 0;
    this.model.TenantId = 0;
    this.route.params.subscribe((params) => {         
      if (params) {
          const id = params['id'];
          this.loadData(id);
      }
  });

    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        this.sites = response.Data;
      //  this.toastr.success(response.Message, "Success"); 
    });  

    this.loadCustomTypes();
    this.loadIncidentTypes();
    //this.loadGuards();
  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

public loadData(id: number){
  this.showSpinner = true;
  
  this.tenants = [];
  this.tenantService.getTenantList(null,null,null)
  .catch((err: any) => {
      return Observable.throw(err);
  })
  .finally(() => {
          setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response : any) => {
      
      this.tenants = response.Data;
     // this.toastr.success(response.Message, "Success"); 
  }); 
  
  this.incidentService.getIncident(id)
  .catch((err: any) => {
      return Observable.throw(err);
  })
  .finally(() => {
          setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response : IPagedResults<any>) => {
    this.model = response.Data;     
    let newDate = new Date(this.model.Time); 
    this.SelectedDate = newDate;

    this.SelectedGuards = [];


    this.Guards = [];
    this.guardService.getAllGuards(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.Guards = response.Data;
        this.model.IncidentGuard.forEach(element => {
          var guard = new GuardModel();
          guard = element.Guard;
          this.SelectedGuards.push(guard);
    
                      
          this.Guards = this.Guards.filter(gElement => gElement.Id != guard.Id)
        });
        //this.toastr.success(response.Message, "Success"); 
    }); 
    
    //this.toastr.success(response.Message, "Success"); 
  });  
  



}

loadTenant()
{
  this.tenants = [];
  this.tenantService.getTenantBySiteId(this.SiteId)
  .catch((err: any) => {
      return Observable.throw(err);
  })
  .finally(() => {
          setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response : any) => {
      
      this.tenants = response.Data;
     // this.toastr.success(response.Message, "Success"); 
  }); 

}

loadIncidentTypes()
{
    this.incidenttypes = [];
    this.incidentTypeService.getAllIncidentTypes(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.incidenttypes = response.Data;
       // this.toastr.success(response.Message, "Success"); 
    });  
};

loadCustomTypes()
{
    this.customTypes = [];
    this.customTypeService.getAllCustomTypes(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.customTypes = response.Data;
       // this.toastr.success(response.Message, "Success"); 
    });  
};

loadGuards()
{
    this.Guards = [];
    this.guardService.getAllGuards(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.Guards = response.Data;
        //this.toastr.success(response.Message, "Success"); 
    }); 
};


  public saveChanges() : void{
    this.showSpinner = true;

    this.model.IncidentGuard = [];

    this.SelectedGuards.forEach((guard)=>
    {
        var incidentGuardmodel = new IncidentGuardModel();
        incidentGuardmodel.GuardId = guard.Id;    
        this.model.IncidentGuard.push(incidentGuardmodel);

    });
    this.model.Time = this.SelectedDate;

    this.incidentService.saveIncident(this.model)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
            this.toastr.error(response.Message, "Error"); 
        }
        else {
            this.toastr.success(response.Message, "Success"); 
        }
        this.ngOnInit();
    });
}
}
