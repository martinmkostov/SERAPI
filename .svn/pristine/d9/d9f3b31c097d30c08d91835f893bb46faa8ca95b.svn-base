﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class AuthenticatController : ApiController
    {
        private AuthenticationService _authService;
        private SiteRoleAuthenticationService _siteRoleAuthService;
        public AuthenticatController()
        {
            _authService = new AuthenticationService();
            _siteRoleAuthService = new SiteRoleAuthenticationService();
        }

        [HttpPost]
        [Route("Authentication/Login")]
        public IHttpActionResult Login(AuthRequestModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _authService.Authenticate(model.UserName, model.Password);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = data.IsAuthenticated,
                    StatusCode = HttpStatusCode.OK,
                    Message = data.AuthResponseMessage,
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Authentication/Logoff")]
        public IHttpActionResult Logoff(int userId)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _authService.LogOff(userId);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = data.AuthResponseMessage,
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Authentication/ForgotPassword")]
        public IHttpActionResult ForgotPassword(AuthRequestModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _authService.SendPasswordToEmailAddress(model.UserName, model.EmailAddress, model.ResendToEmailAddress);
                return new ResponseDataModel<AuthResponseModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent Password on your email address.",
                };
            }));
        }

        [HttpGet]
        [Route("Authentication/GetRole")]
        public IHttpActionResult GetRole(int userId, int moduleId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteRoleAuthService.GetRole(userId,moduleId,siteId);
                return new ResponseDataModel<RoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Role",
                    Data = data
                };
            }));
        }
    }
}
