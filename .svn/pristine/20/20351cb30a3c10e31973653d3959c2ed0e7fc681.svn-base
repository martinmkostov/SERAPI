using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SERApp.Data.Models.Mapping;

namespace SERApp.Data.Models
{
    public partial class SERAppDBContext : DbContext
    {
        static SERAppDBContext()
        {
            Database.SetInitializer<SERAppDBContext>(null);
        }

        public SERAppDBContext()
            : base("Name=SERAppDBContext")
        {
        }
        public SERAppDBContext(string connString)
           : base(!string.IsNullOrEmpty(connString) ? connString : "Name=SERAppDBContext")
        {
        }
        public DbSet<AccountModule> AccountModules { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<MessageAudit> ConfirmationAudits { get; set; }
        public DbSet<MessageType> MessageTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<LoanType> LoanTypes { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<UserModuleRoles> UserModuleRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<TenantContact> TenantContacts { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Language> Languages { get; set; }
        //public DbSet<EmailTemplatesTranslation> EmailTemplatesTranslations { get; set; }
        public DbSet<TenantType> TenantTypes { get; set; }
        public DbSet<PremiseType> PremiseTypes { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }
        public DbSet<ConfigSetting> ConfigSettings { get; set; }
        public DbSet<SubPremiseType> SubPremiseTypes { get; set; }
        //        
        public DbSet<Guard> Guards { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<IncidentGuard> IncidentGuard { get; set; } 
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportType> ReportTypes { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagLog> TagLogs { get; set; }
        public DbSet<CompanyType> CompanyTypes { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<AccountEmailTemplate> AccountEmailTemplates { get; set; }
        public DbSet<Type1> Type1 { get; set; }
        public DbSet<Type2> Type2 { get; set; }
        public DbSet<Type3> Type3 { get; set; }
        public DbSet<Type4> Type4 { get; set; }
        public DbSet<Summary> Summaries { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<DailyReportRecipient> DailyReportRecipients { get; set; }
        public DbSet<RyckrapportData> RyckrapportDatas { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<RyckrapportDataSection> RyckrapportDataSections { get; set; }
        public DbSet<ReportImage> ReportImages { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<IssueWeb> IssueWebs { get; set; }
        public DbSet<IssueType1> IssueType1 { get; set; }
        public DbSet<IssueType2> IssueType2 { get; set; }
        public DbSet<IssueRecipientTypes> IssueRecipientTypes { get; set; }
        public DbSet<IssueRecipients> IssueRecipients { get; set; }

        public DbSet<ModuleHelpTemplate> ModulelHelpTemplates { get; set; }

        public DbSet<IssueWebHistory> IssueWebHistories { get; set; }

        public DbSet<IssueSettings> IssueSettings { get; set; }
        public DbSet<EmailTemplateVariables> EmailTemplateVariables { get; set; }
        public DbSet<IssueType3> IssueType3 { get; set; }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskMainCategory> TaskMainCategories { get; set; }
        public DbSet<TaskSubCategory> TaskSubCategories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<MainCategory> MainCategories { get; set; }
        public DbSet<SiteTask> SiteTasks { get; set; }
        public DbSet<SiteTaskLog> SiteTaskLogs { get; set; }
        public DbSet<Interval> Intervals { get; set; }
        public DbSet<FieldType> FieldTypes { get; set; }
        public DbSet<DailyReportFields> DailyReportFields { get; set; }
        public DbSet<Type1Field> Type1Fields { get; set; }
        public DbSet<Type3TenantType> Type3TenantTypes { get; set; }
        public DbSet<Type1FieldValue> Type1FieldValues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountModuleMap());
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new MessageAuditMap());
            modelBuilder.Configurations.Add(new MessageTypeMap());
            modelBuilder.Configurations.Add(new CustomerMap());
            modelBuilder.Configurations.Add(new FacilityMap());
            modelBuilder.Configurations.Add(new ItemMap());
            modelBuilder.Configurations.Add(new LoanMap());
            modelBuilder.Configurations.Add(new LoanTypeMap());
            modelBuilder.Configurations.Add(new ModuleMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new SiteMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new UserModuleRoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new LogMap());
            modelBuilder.Configurations.Add(new TenantMap());
            modelBuilder.Configurations.Add(new Type2Map());
            modelBuilder.Configurations.Add(new GuardMap());
            modelBuilder.Configurations.Add(new IncidentMap());
            modelBuilder.Configurations.Add(new IncidentGuardMap());
            modelBuilder.Configurations.Add(new Type1Map());
            modelBuilder.Configurations.Add(new ReportMap());
            modelBuilder.Configurations.Add(new ReportTypeMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new TagLogMap());
            modelBuilder.Configurations.Add(new CompanyTypeMap());
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new EmailTemplateMap());
            modelBuilder.Configurations.Add(new AccountEmailTemplateMap());
            modelBuilder.Configurations.Add(new SummaryMap());
            modelBuilder.Configurations.Add(new Type3Map());
            modelBuilder.Configurations.Add(new Type4Map());
            modelBuilder.Configurations.Add(new TenantContactMap());
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new LangaugesMap());
            //modelBuilder.Configurations.Add(new EmailTemplatesTranslationsMap());
			modelBuilder.Configurations.Add(new SettingMap());

            modelBuilder.Configurations.Add(new DailyReportRecipientMap());
            modelBuilder.Configurations.Add(new RyckrapportDataMap());
            modelBuilder.Configurations.Add(new SectionMap());
            modelBuilder.Configurations.Add(new RyckrapportDataSectionMap());
            modelBuilder.Configurations.Add(new ReportImageMap());

            modelBuilder.Configurations.Add(new TenantTypeMap());
            modelBuilder.Configurations.Add(new PremiseTypeMap());
            modelBuilder.Configurations.Add(new JobTitleMap());
            modelBuilder.Configurations.Add(new ConfigSettingMap());
            modelBuilder.Configurations.Add(new SubPremiseTypeMap());
            modelBuilder.Configurations.Add(new IssueMap());
            modelBuilder.Configurations.Add(new EmployeeMap());

            modelBuilder.Configurations.Add(new IssueWebMap());
            modelBuilder.Configurations.Add(new IssueType1Map());
            modelBuilder.Configurations.Add(new IssueType2Map());

            modelBuilder.Configurations.Add(new IssueRecipientMap());
            modelBuilder.Configurations.Add(new IssueRecipientTypeMap());

            modelBuilder.Configurations.Add(new ModuleHelpTemplateMap());
            modelBuilder.Configurations.Add(new IssueWebHistoryMap());
            modelBuilder.Configurations.Add(new IssueSettingsMap());

            modelBuilder.Configurations.Add(new EmailTemplateVariableMap());
            modelBuilder.Configurations.Add(new IssueType3Map());

            modelBuilder.Configurations.Add(new TaskMap());
            modelBuilder.Configurations.Add(new TaskSubCategoryMap());
            modelBuilder.Configurations.Add(new TaskMainCategoryMap());
            modelBuilder.Configurations.Add(new SubCategoryMap());
            modelBuilder.Configurations.Add(new MainCategoryMap());
            modelBuilder.Configurations.Add(new SiteTaskMap());
            modelBuilder.Configurations.Add(new SiteTaskLogMap());
            modelBuilder.Configurations.Add(new IntervalMap());
            modelBuilder.Configurations.Add(new DailyReportFieldMap());
            modelBuilder.Configurations.Add(new FieldTypeMap());
            modelBuilder.Configurations.Add(new Type1FieldMap());
            modelBuilder.Configurations.Add(new Type3TenantTypeMap());
            modelBuilder.Configurations.Add(new Type1FieldValueMap());
        }
    }
}
