﻿using Newtonsoft.Json;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class CustomerService
    {
        CustomerRepository _customerRepository;
        FacilityRepository _facilityRepository;
        public CustomerService() {
            _customerRepository = new CustomerRepository();
            _facilityRepository = new FacilityRepository();
        }

        public CustomerModel Get(int id) {
            
            var customer = _customerRepository.Get(id);
            var data = new CustomerModel()
            {
                Id = customer.Id,
                Company = customer.Company,
                Name = customer.Name,
                Address1 = customer.Address1,
                Address2 = customer.Address2,
                City = customer.City,
                ZipCode = customer.ZipCode,
                Email = customer.Email,
                MobileNumber = customer.MobileNumber,
                CreatedDate = customer.CreatedDate.Value,
                IsDeleted = customer.IsDeleted,
                LoanCounter = _customerRepository.LoanCounter(customer.Id)
            };
            return data;
        }

        public List<CustomerModel> GetAll() { 
            var data = _customerRepository.GetAll()
                .Select(c => new CustomerModel()
                {
                    Company = c.Company,
                    Name = c.Name,
                    Address1 = c.Address1,
                    Address2 = c.Address2,
                    City = c.City,
                    ZipCode = c.ZipCode,
                    Email = c.Email,
                    MobileNumber = c.MobileNumber,
                    CreatedDate = c.CreatedDate.Value,
                    IsDeleted = c.IsDeleted,
                }).ToList();
            return data;
        }

        public List<CustomerModel> GetAllByAccountId(int accountId) {
            return _customerRepository.GetAllByAccountId(accountId).Select(c => new CustomerModel()
            {
                Company = c.Company,
                Name = c.Name,
                Address1 = c.Address1,
                Address2 = c.Address2,
                City = c.City,
                ZipCode = c.ZipCode,
                State = c.State,
                Email = c.Email,
                MobileNumber = c.MobileNumber,
                CreatedDate = c.CreatedDate.Value,
                IsDeleted = c.IsDeleted,
            }).ToList();
        }

        public void SaveCustomer(CustomerModel model) {
            try {
                _customerRepository.SaveCustomer(model);
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteCustomer(int id)
        {
            try
            {
                _customerRepository.DeleteCustomer(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static CustomerModel entityToModel(Customer entity)
        {
            return new CustomerModel()
            {
                Id = entity.Id,
                Company = entity.Company,
                Name = entity.Name,
                Address1 = entity.Address1,
                City = entity.City,
                ZipCode = entity.ZipCode,
                Email = entity.Email,
                MobileNumber = entity.MobileNumber,
                CreatedDate = entity.CreatedDate.Value,
                IsDeleted = entity.IsDeleted
            };
        }

        public static FacilityModel entityToModel(Facility entity)
        {
            return new FacilityModel()
            {
                Id = entity.Id,
                FacilityName = entity.FacilityName,
                Description = entity.Description,
                CreatedDate = entity.CreatedDate.Value,
                CreatedBy = entity.CreatedBy,
                LastModifiedDate = entity.LastModifiedDate.HasValue ? entity.LastModifiedDate.Value : DateTime.MinValue,
                LastModifiedBy = entity.LastModifiedBy
            };
        }
    }
}
