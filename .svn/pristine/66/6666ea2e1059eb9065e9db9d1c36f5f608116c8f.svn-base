﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ISettingService {

        IEnumerable<SettingModel> GetSettings(int accountId, int moduleId, int siteId);
        SettingModel GetValue(string setting, int accountId, int siteId);
        SettingModel GetValueById(int id);
        void Save(SettingModel model);
    }
    public class SettingService : ISettingService
    {

        private List<LabelSettings> CustomizableLabel = new List<LabelSettings>()
        {
            new LabelSettings()
            {
                Name = "Type1",
                Value = "Type 1",
                ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "Type2",
                Value = "Type 2",
                ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "Type3",
                Value = "Type 3",
                ModuleId = 6
            },
            new LabelSettings()
            {
                    Name = "StartRond",
                    Value = "",
                    ModuleId = 6
            },
            new LabelSettings()
            {
                    Name = "StopRond",
                    Value = "",
                    ModuleId = 6
            },
            new LabelSettings()
            {
                Name = "IssueType1",
                Value = "Issue Type 1",
                ModuleId = 7
            },
                  new LabelSettings()
            {
                Name = "IssueType2",
                    Value = "Issue Type 2",
                    ModuleId = 7
                },
                new LabelSettings()
            {
                Name = "IssueType3",
                    Value = "Issue Type 3",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                        Name = "WithSpecialOption",
                        Value = "false",
                        ModuleId = 6
                },
                new LabelSettings()
                {
                        Name = "SpecialOptions",
                        Value = "",
                        ModuleId = 6
                },
                new LabelSettings()
                {
                        Name = "PublicText",
                        Value = "To set this text message, please login to SER and update setting Public Text under Settings menu on Issues Module.",
                        ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="PublicPageContactOptions",
                    Value="",
                    ModuleId = 7
                },
                new LabelSettings()
                {
                    Name ="ContactType",
                    Value = "0",
                    ModuleId = 7
                }
        };

       

        

        private IRepository<Setting> _repository;
        private IRepository<ConfigSetting> _configSettingRepository;
        public SettingService()
        {
            _repository = new Repository<Setting>();
            _configSettingRepository = new Repository<ConfigSetting>();
        }

        public IEnumerable<SettingModel> GetSettings(int accountId, int moduleId,int siteId)
        {
            var data = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == 0).ToList();

            if (siteId != 0)
            {
                var siteSettings = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == siteId).ToList();

                data.AddRange(siteSettings);
            }


            CustomizableLabel.Where(x=>x.ModuleId == moduleId).ToList().ForEach(r => 
            {
                if (r.Name.ToLower() == "type1")
                {
                    r.Value = "Vad";
                }

                if (r.Name.ToLower() == "type2")
                {
                    r.Value = "Var";
                }

                if (r.Name.ToLower() == "type3")
                {
                    r.Value = "Vem";
                }


                if (!data.Any(v => v.Name.ToLower() == r.Name.ToLower()))
                {

                    var configSetting = _configSettingRepository.GetByPredicate(x => x.SettingName.ToLower() == r.Name.ToLower()).SingleOrDefault();
                    int setSiteId = 0;
                    if (r.Name == "PublicText")
                    {
                        setSiteId = siteId;
                    }

                    _repository.Save(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value,
                        ConfigSettingId = configSetting.Id,
                        SiteId = setSiteId
                    });

                    data.Add(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value,
                        ConfigSettingId = configSetting.Id,
                        SiteId = setSiteId
                    });
                }
            });

            data = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == 0).ToList();

            if (siteId != 0)
            {
                var siteSettings = _repository.GetAll().Where(x => x.AccountId == accountId && x.ConfigSetting.ModuleId == moduleId && x.SiteId == siteId).ToList();

                data.AddRange(siteSettings);
            }
            

            return data.Select(r=> new SettingModel()
            { 
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value,
                ConfigSettingId = r.ConfigSettingId,  
                SiteId = r.SiteId
            }).ToList();
        }

        public SettingModel GetValue(string setting, int accountId, int siteId)
        {

            if (siteId != 0)
            {
                return _repository.GetAll().Where(x => x.AccountId == accountId && x.Name.ToLower() == setting.ToLower() && x.SiteId == siteId)
                               .Select(r => new SettingModel()
                               {
                                   AccountId = r.AccountId,
                                   Name = r.Name,
                                   SettingId = r.SettingId,
                                   Value = r.Value
                               })
                               .SingleOrDefault();
            }

            return _repository.GetAll().Where(x => x.AccountId == accountId && x.Name.ToLower() == setting.ToLower())
                .Select(r => new SettingModel()
                {
                    AccountId = r.AccountId,
                    Name = r.Name,
                    SettingId = r.SettingId,
                    Value = r.Value
                })
                .SingleOrDefault();
        }

        public SettingModel GetValueById(int id)
        {
            var r = _repository.Get(id);

            r.ConfigSetting = _configSettingRepository.Get(r.ConfigSettingId);

            return new SettingModel()
            {
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value,
                InputType = r.ConfigSetting.InputType
            };
        }

        public void Save(SettingModel model)
        {
            if (model.SettingId == 0)
            {
                return;
            }

            var data = _repository.Get(model.SettingId);

            if (model.InputType == "checkbox")
            {
                if (model.tempValue == true)
                {
                    data.Value = "true";
                }
                else
                {
                    data.Value = "false";
                }
            }
            else {


                if (model.Name == "SpecialOptions")
                {
                    data.Value = model.Value.TrimEnd(',');
                }
                else
                {
                    data.Value = model.Value;
                }
               
            }
            

            _repository.Update(data);
        }
    }
}
