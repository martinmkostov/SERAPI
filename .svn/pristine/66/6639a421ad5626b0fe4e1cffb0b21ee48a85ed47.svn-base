﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class LoanRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public LoanRepository() {
            db = new SERAppDBContext(connString);
        }

        public Loan Get(int id)
        {
            return db.Loans
                .Where(c => c.Id == id).SingleOrDefault();
        }

        public List<Loan> GetAll()
        {
            return db.Loans
                .OrderByDescending(l => l.CreatedDate).ToList();
        }

        public List<Loan> GetAllByAccountId(int accountId)
        {
            var data = (from t in db.Loans
                        join s in db.Sites on t.SiteId equals s.Id
                        where s.AccountId == accountId
                        select t).ToList();
            return data;
        }

        public List<Loan> GetAllByCustomerId(int customerId)
        {
            return db.Loans
                .Where(c => c.CustomerId == customerId).ToList();
        }

        public List<Loan> GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType) {
            List<Loan> loans = db.Loans.ToList();
            if (customerId > 0)
            {
                loans = loans.Where(l => l.CustomerId == customerId).ToList();
            }
            if (typeId > 0)
            {
                loans = loans.Where(l => l.TypeId == typeId).ToList();
            }
            if (statusId > 0)
            {
                loans = loans.Where(l => l.StatusId == statusId).ToList();
            }
            if (!string.IsNullOrEmpty(itemName))
            {
                loans = loans.Where(l => l.ItemName.StartsWith(itemName)).ToList();
            }
            if (!string.IsNullOrEmpty(LoanedDate))
            {
                loans = loans.Where(l => l.LoanedDate.Value == DateTime.Parse(LoanedDate)).ToList();
            }
            if (!string.IsNullOrEmpty(ReturnedDate))
            {
                loans = loans.Where(l => l.ReturnedDate.Value == DateTime.Parse(ReturnedDate)).ToList();
            }
            else if (!string.IsNullOrEmpty(LoanedDate) && !string.IsNullOrEmpty(ReturnedDate))
            {
                loans = loans.Where(l => l.ReturnedDate.Value >= DateTime.Parse(LoanedDate) && l.LoanedDate.Value <= DateTime.Parse(ReturnedDate)).ToList();
            }
            return loans;
        }

        public void DeleteLoan(int id) {
            var loan = db.Loans.Where(l => l.Id == id).SingleOrDefault();
            if (loan != null)
            {
                loan.IsDeleted = true;
                loan.LastModifiedDate = DateTime.Now;
                loan.LastModifiedBy = "Admin";
                db.SaveChanges();
            }
        }

        public LoanModel SaveLoan(LoanModel model)
        {
            var loan = db.Loans.Where(l => l.Id == model.Id).SingleOrDefault();
            if (loan != null)
            {
                loan.SiteId = model.SiteId;
                loan.CustomerId = model.CustomerId;
                loan.CustomerTypeId = model.CustomerTypeId;
                loan.TypeId = model.TypeId;
                loan.StatusId = model.StatusId;
                loan.ItemName = model.ItemName;
                loan.Description = model.Description;
                loan.Notes = model.Notes;
                //loan.PhotoUrl = model.PhotoUrl; THIS IS SAVED AFTER THE LOAN IS SAVED
                loan.MessageTypeId = model.MessageTypeId;
                loan.LoanedDate = model.LoanedDate;
                loan.ReturnedDate = model.ReturnedDate;
                loan.LastModifiedDate = DateTime.Now;
                loan.IsDeleted = model.IsDeleted;
                loan.GivenBy = model.GivenBy;
                loan.ReceivedBy = model.ReceivedBy;
                loan.LastModifiedBy = model.LastModifiedBy;
                if(model.ConfirmedDate > DateTime.MinValue)
                {
                    loan.ConfirmedDate = model.ConfirmedDate;
                }
                if (model.ActualReturnedDate > DateTime.MinValue)
                {
                    loan.ActualReturnedDate = model.ActualReturnedDate;
                }
                db.SaveChanges();
            }
            else {
                if (model.CustomerId == 0)
                {
                    model.CustomerId = CreateCustomer(model);
                    model.CustomerTypeId = 1;
                }
                Loan newLoan = new Loan()
                {
                    SiteId = model.SiteId,
                    CustomerId = model.CustomerId,
                    CustomerTypeId = model.CustomerTypeId,
                    TypeId = model.TypeId,
                    StatusId = 4, //initially set it to 4 (NotConfirmed)
                    ItemName = model.ItemName,
                    Description = model.Description,
                    Notes = model.Notes,
                    //PhotoUrl = model.PhotoUrl, THIS IS SAVED AFTER THE LOAN IS SAVED
                    MessageTypeId = model.MessageTypeId,
                    LoanedDate = model.LoanedDate,
                    ReturnedDate = model.ReturnedDate,
                    LastModifiedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    IsDeleted = false,
                    GivenBy = model.GivenBy,
                    ReceivedBy = model.ReceivedBy,
                    CreatedBy = model.CreatedBy,
                    LastModifiedBy = model.LastModifiedBy
                };
                
                db.Loans.Add(newLoan);
                db.SaveChanges();
                model.Id = newLoan.Id;
            }

            return model;
        }

        public void SaveLoanPhoto(int id, string photoURL) {
            var loan = db.Loans.Where(l => l.Id == id).SingleOrDefault();
            if (loan != null)
            {
                loan.PhotoUrl = photoURL;
                db.SaveChanges();
            }
        }

        public int CreateCustomer(LoanModel model) {
            var customer = model.Customer;
            Customer newCustomer = new Customer()
            {
                Name = customer.Name,
                Address1 = customer.Address1,
                City = customer.City,
                ZipCode = customer.ZipCode,
                State = customer.State,
                Email = customer.Email,
                MobileNumber = customer.MobileNumber,
                CreatedBy = model.CreatedBy,
                LastModifiedBy = model.LastModifiedBy,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now,
                IsDeleted = false
            };
            db.Customers.Add(newCustomer);
            db.SaveChanges();
            return newCustomer.Id;
        }

        public void ConfirmLoan(int loanId)
        {
            var loan = db.Loans.Where(l => l.Id == loanId).SingleOrDefault();
            if (loan != null)
            {
                loan.StatusId = 1; //confirm - Active
                loan.ConfirmedDate = DateTime.Now;
                loan.LastModifiedDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        public void ReturnLoan(int loanId)
        {
            var loan = db.Loans.Where(l => l.Id == loanId).SingleOrDefault();
            if (loan != null)
            {
                loan.StatusId = 2; //Returned
                loan.ActualReturnedDate = DateTime.Now;
                loan.LastModifiedDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        #region Loan Type

        public LoanType GetLoanType(int id) {
            return db.LoanTypes.Where(l => l.Id == id).SingleOrDefault();
        }

        public List<LoanType> GetAllLoanTypes() {
            return db.LoanTypes.ToList();
        }

        #endregion
    }
}
