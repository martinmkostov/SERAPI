﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class LoanService
    {
        LoanRepository _loanRepository;
        CustomerRepository _customerRepository;
        SiteRepository _siteRepository;
        CommonRepository _commonRepository;
        TenantRepository _tenantRepository;
        LogRepository _logRepository;
        EmailRepository _emailRepository;

        CustomerService customerService;
        TenantService tenantService;
        SiteService siteService;
        AccountSettingsService settingService;
        private const int noDaysOfReturn = 10;
        private const int noOfLoansPerCustomer = 5;
        SMS sms;
        public LoanService() {
            _loanRepository = new LoanRepository();
            _customerRepository = new CustomerRepository();
            _siteRepository = new SiteRepository();
            _commonRepository = new CommonRepository();
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
            _emailRepository = new EmailRepository();

            customerService = new CustomerService();
            tenantService = new TenantService();
            siteService = new SiteService();
            settingService = new AccountSettingsService();
            sms = new SMS();
        }

        public LoanModel Get(int id) {
            var loan = _loanRepository.Get(id);
            if (loan == null)
            {
                return null;
            }
            var data = new LoanModel()
            {
                Id = loan.Id,
                CustomerId = loan.CustomerId,
                SiteId = loan.SiteId,
                TypeId = loan.TypeId.Value,
                StatusId = loan.StatusId.Value,
                ItemNameId = loan.ItemNameId,
                Description = loan.Description,
                Notes = loan.Notes,
                PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                MessageTypeId = loan.MessageTypeId.Value,
                LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = loan.ReturnedDate,
                LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                CustomerTypeId = loan.CustomerTypeId,
                Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                GivenBy = loan.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                LoanId = loan.LoanId,
                FirstName = loan.FirstName,
                Email = loan.Email,
                Mobile = loan.Mobile,                
            };

            if (data.CustomerTypeId == 1)
            {
                data.FirstName = data.Customer.Name;
                data.Mobile = data.Customer.MobileNumber;
                data.Email = data.Customer.Email;
            }

            return data;
        }

        public List<LoanModel> GetAllBySiteId(int siteId)
        {

            var data = _loanRepository.GetAll();

            return data.Where(l => l.SiteId == siteId)
            .Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : TenantToCustomer(_tenantRepository.Get(l.CustomerId)),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                LoanId = l.LoanId
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();
        }

        public List<LoanModel> GetAll() {

            var data = _loanRepository.GetAll();

            return data.Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                CustomerTypeId = l.CustomerTypeId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : TenantToCustomer(_tenantRepository.Get(l.CustomerId)),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                LoanId = l.LoanId
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();
        }

        public List<LoanCustomerModel> GetAllMergedCustomer(int accountId, string keyword) {
            var customers = _customerRepository.GetAllByAccountId(accountId);
            var tenants = _tenantRepository.GetAllByAccountId(accountId);
            var finalCustomers = customers.Select(c => new LoanCustomerModel()
            {
                Id = c.Id,
                TypeId = 1,
                Type = "Customer",
                Name = c.Name,
                LoanCount = CustomerLoanCount(c.Id, 1),
                RemainingLoanCount = noOfLoansPerCustomer - CustomerLoanCount(c.Id, 1),
                AllowedForMoreLoans = IsCustomerAllowedForMoreLoans(c.Id, 1)
            })
            .Union(tenants.Select(t => new LoanCustomerModel() {
                Id = t.Id,
                TypeId = 2,
                Type = "Contacts",
                Name = t.Name,
                LoanCount = CustomerLoanCount(t.Id, 2),
                RemainingLoanCount = noOfLoansPerCustomer - CustomerLoanCount(t.Id, 2),
                AllowedForMoreLoans = IsCustomerAllowedForMoreLoans(t.Id, 2)
            }))
            .ToList();
            if (!string.IsNullOrEmpty(keyword)) {
                keyword = keyword.ToLower();
                var filtered = finalCustomers.Where(c => c.Name.ToLower().StartsWith(keyword) || c.Type.ToLower().StartsWith(keyword)).ToList();
                return filtered;
            }
            return finalCustomers;
        }

        public List<LoanModel> GetAllByAccountId(int id, int siteId, string keyword) {
            List<LoanModel> loans = new List<LoanModel>();
            var data = _loanRepository.GetAllByAccountId(id);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = _loanRepository.GetAllWithFilter(id, keyword);
            }

            if (siteId != 0)
            {
                data = data.Where(x => x.SiteId == siteId).ToList();
            }
            else {
                if (string.IsNullOrEmpty(keyword))
                {
                    return new List<LoanModel>();
                }
                
            }
            
            
            loans = data.Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                CustomerTypeId = l.CustomerTypeId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : (_tenantRepository.Get(l.CustomerId) != null ? TenantToCustomer(_tenantRepository.Get(l.CustomerId)) : null),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                SiteId = l.SiteId,
                LoanId = l.LoanId,
                FirstName = l.FirstName,
                Email = l.Email,
                Mobile = l.Mobile
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();

            return loans;
        }

        public List<LoanTypeModel> GetAllLoanTypes() {
            return _loanRepository.GetAllLoanTypes()
                .Select(l => new LoanTypeModel() {
                    Id = l.Id,
                    Name = l.Name,
                    Description = l.Description
                })
                .OrderBy(l => l.Name)
                .ToList();
        }

        public List<LoanModel> GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType)
        {
            var data = _loanRepository.GetAllWithFilter(customerId, typeId, statusId, itemName, LoanedDate, ReturnedDate, confirmationType)
                .Select(l => new LoanModel()
                {
                    Id = l.Id,
                    LoanId = l.LoanId,
                    CustomerId = l.CustomerId,
                    TypeId = l.TypeId.Value,
                    StatusId = l.StatusId.Value,
                    ItemNameId = l.ItemNameId,
                    Description = l.Description,
                    Notes = l.Notes,
                    PhotoUrl = l.PhotoUrl,
                    MessageTypeId = l.MessageTypeId.Value,
                    LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                    ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                    LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                    CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                    IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                    GivenBy = l.GivenBy,
                    ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                    ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                    Site = entityToModel(_siteRepository.Get(l.SiteId))
                })
                .OrderByDescending(l => l.CreatedDate)
                .ToList();
            return data;
        }

        public LoanModel GetLoanByLoanId(string loanId) {
            var loan = _loanRepository.Get(loanId);
            if (loan == null)
            {
                return null;
            }
            var data = new LoanModel()
            {
                Id = loan.Id,
                CustomerId = loan.CustomerId,
                SiteId = loan.SiteId,
                TypeId = loan.TypeId.Value,
                StatusId = loan.StatusId.Value,
                ItemNameId = loan.ItemNameId,
                Description = loan.Description,
                Notes = loan.Notes,
                PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                MessageTypeId = loan.MessageTypeId.Value,
                LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = loan.ReturnedDate,
                LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                CustomerTypeId = loan.CustomerTypeId,
                Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                GivenBy = loan.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                LoanId = loan.LoanId
            };
            return data;
        }

        public LoanModel SaveLoan(LoanModel model)
        {
            try
            {
                if (model.MessageTypeId == 0)
                {
                    model.MessageTypeId = 4;
                }
                int originalCustomerId = model.CustomerId;
                string photoString = model.PhotoString;
                bool isNew = model.Id == 0;

                if (model.ItemNameId.Contains(','))
                {
                    string originalItemNameId = model.ItemNameId;
                    foreach (string itemNameId in model.ItemNameId.Split(','))
                    {
                        if (!string.IsNullOrEmpty(itemNameId))
                        {
                            model.Id = 0; //reset back to zero
                            model.ItemNameId = itemNameId;
                            _loanRepository.SaveLoan(model);
                        }
                    }
                    model.ItemNameId = originalItemNameId; // this will be used for the email
                }
                else
                {
                    if (model.CustomerTypeId == 2)
                    {
                        model.FirstName = model.Customer.Name;
                        model.Email = model.Customer.Email;
                        model.Mobile = model.Customer.MobileNumber;
                    }
                    //else {
                    //    model.FirstName = model.Customer.Name;
                    //    model.Email = model.Customer.Email;
                    //    model.Mobile = model.Customer.MobileNumber;
                    //}

                    _loanRepository.SaveLoan(model);
                }

                if (!string.IsNullOrEmpty(photoString)) {
                    //photoString, "LOANS", model.Id.ToString()
                    model.PhotoUrl = Service.Tools.File.SaveFile(new FileModel() {
                        AccountId = model.Site.AccountId,
                        FileName = model.Id.ToString(), // using loan id as the name for now
                        ModuleType = "LOANS",
                        ByteString = photoString
                    });
                    _loanRepository.SaveLoanPhoto(model.Id, model.PhotoUrl);
                }

                model.Site = entityToModel(_siteRepository.Get(model.SiteId));
                if (originalCustomerId == 0)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                        LogMessagingSettings.Save,
                        LogMessagingSettings.MessageCreateCustomer, model.Site.AccountId, 0);
                }

                if (isNew && model.CustomerTypeId == 1 && model.CustomerId != 0)
                {
                    var customer = _customerRepository.Get(model.CustomerId);
                    customer.Name = model.Customer.Name;
                    customer.Address1 = model.Customer.Address1;
                    customer.Address2 = model.Customer.Address2;
                    customer.City = model.Customer.City;
                    customer.Company = model.Customer.Company;
                    customer.Email = model.Customer.Email;
                    customer.MobileNumber = model.Customer.MobileNumber;

                    _customerRepository.SaveCustomer(new CustomerModel()
                    {
                        Id = model.Customer.Id,
                        Name = model.Customer.Name,
                        Address1 = model.Customer.Address1,
                        Address2 = model.Customer.Address2,
                        City = model.Customer.City,
                        Company = model.Customer.Company,
                        Email = model.Customer.Email,
                        MobileNumber = model.Customer.MobileNumber,
                });
                }

               
                if (isNew)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                       LogMessagingSettings.Save,
                       LogMessagingSettings.MessageCreateLoan, model.Site.AccountId, 0);

                    _emailRepository.GetAccountEmailTemplate(EmailTemplates.LoanCreate, model.Site.AccountId);

                    switch (model.MessageTypeId)
                    {
                        case 0:

                            break;
                        case 1:
                            Email.SendCreateLoandConfirmation(model);
                            break;
                        case 2:
                            sms.SendCreateLoandConfirmation(model).Wait();
                            break;
                        case 3:
                            Email.SendCreateLoandConfirmation(model);
                            sms.SendCreateLoandConfirmation(model).Wait();
                            break;
                        case 4:

                            break;
                        default:
                            Email.SendCreateLoandConfirmation(model);
                            break;
                    }

             
                }
                
                if (model.SendNotification)
                {

                    switch (model.MessageTypeId)
                    {
                        case 1:
                            // CONFIRM LOAN if it's not yet confirmed and Save and Send Button is clicked
                            if (model.StatusId == 4)
                            {
                                ConfirmLoan(model.Customer.Email, model.Id);
                            }
                            else
                            {
                                Email.SendLoanUpdateNotification(model);
                            }
                            break;
                        case 2:
                            sms.SendLoanUpdateNotification(model).Wait();
                            break;
                        case 3:
                            // CONFIRM LOAN if it's not yet confirmed and Save and Send Button is clicked
                            if (model.StatusId == 4)
                            {
                                ConfirmLoan(model.Customer.Email, model.Id);
                            }
                            else
                            {
                                Email.SendLoanUpdateNotification(model);
                            }
                            sms.SendCreateLoandConfirmation(model).Wait();
                            break;
                        case 4:

                            break;
                        default:
                            Email.SendCreateLoandConfirmation(model);
                            break;
                    }
                    
                    
                }


                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsCustomerAllowedForMoreLoans(int customerId, int customerTypeId) {
            return CustomerLoanCount(customerId, customerTypeId) < noOfLoansPerCustomer;
        }

        public int CustomerLoanCount(int customerId, int customerTypeId) {
            return _loanRepository.GetAll().Where(l => l.CustomerId == customerId && l.CustomerTypeId == customerTypeId && (l.IsDeleted == null || l.IsDeleted == false)).Count();
        }

        public void ConfirmLoan(string encryptedEmail, int id) {
            string email = string.Empty;
            if ((Email.IsValidEmailAddress(encryptedEmail)))
            {
                email = encryptedEmail;
            }
            else {
                email = Security.decrypt64BasedString(encryptedEmail);
            }
            var model = Get(id);
            if (model != null)
            {
                string customerEmail = encryptedEmail;//model.CustomerTypeId == 1 ? _customerRepository.Get(model.CustomerId).Email : _tenantRepository.Get(model.CustomerId).Email;
                if ((!string.IsNullOrEmpty(customerEmail) && customerEmail  == email))
                {
                    string loanId = model.LoanId;
                    var loans = _loanRepository.GetByLoanId(loanId);
                    if (loans.Count() > 1)
                    {
                        // confirm all related loans if one of them is being confirmed
                        foreach (var loan in loans) {
                            _loanRepository.ConfirmLoan(loan.Id);
                        }
                    }
                    else {
                        _loanRepository.ConfirmLoan(model.Id);
                    }
                    
                    var site = entityToModel(_siteRepository.Get(model.SiteId));
                    model.Customer.MobileNumber = model.Mobile;
                    model.Customer.Email = model.Email;
                    
                    if (model.MessageTypeId == 2)
                    {
                        sms.SendLoanConfirm(model).Wait();
                    }
                    else if (model.MessageTypeId == 3)
                    {
                        sms.SendLoanConfirm(model).Wait();
                        Email.SendLoanConfirm(model);
                    }
                    else
                    {
                        Email.SendLoanConfirm(model);
                    }

                    
                    string subject = "LOAN CONFIRMATION";
                    string message = model.Customer.Name + " Confirmed loan on " + site.Name + " at " + DateTime.Now.ToShortDateString();

                    _logRepository.Log(LogTypeEnum.Information,
                      subject,
                      message, 
                      site.AccountId, 0);
                }
            }
           
        }

        public void ReturnLoan(int id)
        {
            var model = Get(id);
            if (model != null)
            {
                var customer = _customerRepository.Get(model.CustomerId);
                if (customer != null)
                {
                    _loanRepository.ReturnLoan(model.Id);
                    var site = entityToModel(_siteRepository.Get(model.SiteId));

                    if (model.MessageTypeId == 2)
                    {
                        sms.SendLoanReturn(model).Wait();
                    }
                    else if (model.MessageTypeId == 3)
                    {
                        sms.SendLoanReturn(model).Wait();
                        Email.SendLoanReturn(model);
                    }
                    else
                    {
                        Email.SendLoanReturn(model);
                    }

                    string subject = "LOAN RETURN";
                    string message = customer.Name + " Returned the loan on " + site.Name + " at " + DateTime.Now.ToShortDateString();

                    _logRepository.Log(LogTypeEnum.Information,
                      subject,
                      message,
                      site.AccountId, 0);
                }
                else {
                    var contact = _tenantRepository.Get(model.CustomerId);
                    if (contact != null)
                    {
                        _loanRepository.ReturnLoan(model.Id);
                        var site = entityToModel(_siteRepository.Get(model.SiteId));
                        model.Customer.Email = model.Email;
                        model.Customer.MobileNumber = model.Mobile;
                        if (model.MessageTypeId == 2)
                        {
                            sms.SendLoanReturn(model).Wait();
                        }
                        else if (model.MessageTypeId == 3)
                        {
                            sms.SendLoanReturn(model).Wait();
                            Email.SendLoanReturn(model);
                        }
                        else
                        {
                            Email.SendLoanReturn(model);
                        }

                        string subject = "LOAN RETURN";
                        string message = customer.Name + " Returned the loan on " + site.Name + " at " + DateTime.Now.ToShortDateString();

                        _logRepository.Log(LogTypeEnum.Information,
                          subject,
                          message,
                          site.AccountId, 0);
                    }
                }
            }
        }

        public void DeleteLoan(int id, bool deleteCustomer = false)
        {
            try
            {
                var customer = Get(id).Customer;
                _loanRepository.DeleteLoan(id);
                if (deleteCustomer)
                {
                    _customerRepository.DeleteCustomer(customer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLoan(LoanDeleteParamModel model) {
            if (!string.IsNullOrEmpty(model.ids))
            {
                foreach (string id in model.ids.Split(','))
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        DeleteLoan(int.Parse(id), model.deleteCustomer);
                    }
                }
            }
            else {
                DeleteLoan(model.id, model.deleteCustomer);
            }
        }

        public void DeleteReturnedLoans(int accountId) {
            var sites = _siteRepository.GetAllByAccountId(accountId);
            var returnedLoans = _loanRepository.GetAllReturnedLoans().Where(l => sites.Any(s => s.Id == l.SiteId));
            foreach (var loan in returnedLoans) {
                DeleteLoan(loan.Id);
            }
        }

        public void DeleteReturnedLoansWithTimeSetting(int accountId) {
            var sites = _siteRepository.GetAllByAccountId(accountId);
            var numberDays = settingService.Get("NumberDaysReturnedDeleteRecord", accountId).Value;
            var returnedLoans = _loanRepository.GetAllReturnedLoans()
                .Where(l => sites.Any(s => s.Id == l.SiteId) && l.ActualReturnedDate.Value.AddDays(int.Parse(numberDays)) <= DateTime.Now);
            foreach (var loan in returnedLoans)
            {
                DeleteLoan(loan.Id);
            }
        }

        public void SendMultipleEmails(string command, string loanIds) {
            if (!string.IsNullOrEmpty(loanIds))
            {
                foreach (string id in loanIds.Split(','))
                {
                    var loan = Get(int.Parse(id));
                    if (command == "notification")
                    {
                        Email.SendLoanNotifcation(loan);
                    }
                    else if (command == "passeddate")
                    {
                        Email.SendLoanPassedDateNotification(loan);
                    }
                }
            }
            else {
                if (command == "passeddate") {
                    foreach (var loan in GetAllPassedDateLoans()) {
                        Email.SendLoanPassedDateNotification(loan);
                    }

                }
            }
            
        }

        public List<LoanModel> GetAllPassedDateLoans() {
            return _loanRepository.GetAllPassedDateLoans()
                .Select(loan => new LoanModel()
                {
                    Id = loan.Id,
                    CustomerId = loan.CustomerId,
                    SiteId = loan.SiteId,
                    TypeId = loan.TypeId.Value,
                    StatusId = loan.StatusId.Value,
                    ItemNameId = loan.ItemNameId,
                    Description = loan.Description,
                    Notes = loan.Notes,
                    PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                    MessageTypeId = loan.MessageTypeId.Value,
                    LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                    ReturnedDate = loan.ReturnedDate,
                    LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                    CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                    IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                    LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                    CustomerTypeId = loan.CustomerTypeId,
                    Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                    Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                    GivenBy = loan.GivenBy,
                    ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                    ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                    ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                    ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                    Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                    LoanId = loan.LoanId
                }).ToList();
        }

        public static LoanTypeModel entityToModel(LoanType entity)
        {
            return new LoanTypeModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description
            };
        }

        public static MessageTypeModel entityToModel(MessageType entity) {
            return new MessageTypeModel()
            {
                Id = entity.Id,
                ConfirmationName = entity.ConfirmationName,
                Description = entity.Description,
                IsUsed = entity.IsUsed.Value
            };
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        private static string GetStatus(LoanModel loan) {
            //will implement this soon
            return string.Empty;
        }

        private static CustomerModel TenantToCustomer(Tenant tenant) {
            return new CustomerModel()
            {
                Id = tenant.Id,
                Name = tenant.Name,
                Company = tenant.Name,
                Email = tenant.Email,
                MobileNumber = tenant.Phone,
                Address1 = tenant.VisitingAddress,
                City = tenant.City,//!string.IsNullOrEmpty(tenant.InvoiceZipAndCity) && tenant.InvoiceZipAndCity.Contains(",") ? tenant.InvoiceZipAndCity.Split(',')[1] : tenant.InvoiceZipAndCity,
                ZipCode = !string.IsNullOrEmpty(tenant.InvoiceZipAndCity) && tenant.InvoiceZipAndCity.Contains(",") ? tenant.InvoiceZipAndCity.Split(',')[0] : "",
            };
        }

        #region LOAN SETTINGS

        public LoanTypeModel GetLoanType(int id) {
            return entityToModel(_loanRepository.GetLoanType(id));
        }

        public List<LoanTypeModel> GetLoanTypesByAccountId(int accountId) {
            return _loanRepository.GetLoanTypesByAccountId(accountId)
                .Select(lt => new LoanTypeModel() {
                    Id = lt.Id,
                    Name = lt.Name,
                    Description = lt.Description,
                    AccountId = lt.AccountId,
                    IsPhotoRequired = lt.IsPhotoRequired
                })
                .OrderBy(l => l.Name)
                .ToList();
        }

        public bool SaveLoanType(LoanTypeModel model) {

            if (model.Id == 0)
            {
                if (_loanRepository.GetAllLoanTypes().Any(x => x.Name == model.Name && x.AccountId == model.AccountId))
                {
                    return false;
                }
            }
        
                
            _loanRepository.SaveLoanType(model);
            return true;
        }

        public void DeleteLoanType(int id) {
            _loanRepository.DeleteLoanType(id);
        }
        #endregion
    }
}
