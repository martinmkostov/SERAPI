﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TaskController : ApiController
    {
        private ITaskService _taskService;
        private IIntervalService _intervalService;
        public TaskController()
        {
            _taskService = new TaskService();
            _intervalService = new IntervalService();
        }

        [HttpGet]
        [Route("Task/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTask(id);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllTasks().OrderBy(c=>c.TaskNo).ToList();
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetByUserId")]
        public IHttpActionResult GetByUserId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetByUserId(id).ToList();
                return new ResponseDataModel<List<IGrouping<string, SiteTaskModel>>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/Save")]
        public IHttpActionResult Save(TaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddTask(model);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.DeleteTask(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteMainCategory")]
        public IHttpActionResult DeleteMainCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
               var data = _taskService.DeleteMainCategory(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteSubCategory")]
        public IHttpActionResult DeleteSubCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteSubCategory(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteActivityRole")]
        public IHttpActionResult DeleteActivityRole(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteActivityRole(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/DeleteInterval")]
        public IHttpActionResult DeleteInterval(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var model = _taskService.DeleteInterval(id);
                return new ResponseDataModel<DeleteResultModel>
                {
                    Data = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/ArchiveTask")]
        public IHttpActionResult ArchiveTask(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.ArchiveTask(id);
                return new ResponseDataModel<ArchiveDataModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpPost]
        [Route("Task/UnarchiveTask")]
        public IHttpActionResult UnarchiveTask(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.UnarchiveTask(id);
                return new ResponseDataModel<ArchiveDataModel>
                {
                    Data = data,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllMainCategories")]
        public IHttpActionResult GetAllMainCategories()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllMainCategories().OrderBy(c=>c.Name).ToList();
                return new ResponseDataModel<IEnumerable<MainCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllMainCategory")]
        public IHttpActionResult GetAllMainCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllMainCategory(id);
                return new ResponseDataModel<MainCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetSubCategory")]
        public IHttpActionResult GetSubCategory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetSubCategory(id);
                return new ResponseDataModel<SubCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetAllSubCategories")]
        public IHttpActionResult GetAllSubCategories()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllSubCategories().OrderBy(c=>c.Name).ToList();
                return new ResponseDataModel<IEnumerable<SubCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveMainCategory")]
        public IHttpActionResult SaveMainCategory(MainCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddMainCategory(model);
                return new ResponseDataModel<MainCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveSubCategory")]
        public IHttpActionResult SaveSubCategory(SubCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddSubCategory(model);
                return new ResponseDataModel<SubCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/PublishTask")]
        public IHttpActionResult PublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.PublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/UnPublishTask")]
        public IHttpActionResult UnPublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.UnPublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetPublishedTask")]
        public IHttpActionResult GetPublishedTask(int Id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetPublishedTask(Id);
                return new ResponseDataModel<IEnumerable<SiteTaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllBySiteId")]
        public IHttpActionResult GetAllBySiteId(int siteId, int userId =0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllTaskBySiteId(siteId, userId);
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetTaskBySiteIdAndTaskId")]
        public IHttpActionResult GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTaskBySiteIdAndTaskId(taskId,siteId);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Task/Confirm")]
        public IHttpActionResult Confirm(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.Confirm(model);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/ReAssign")]
        public IHttpActionResult ReAssign(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {                
                _taskService.ReAssign(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/ConfirmMultiple")]
        public IHttpActionResult ConfirmMultiple(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var result = _taskService.ConfirmMultiple(model);
                return new ResponseDataModel<List<DateTime>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = result
                };
            }));
        }

        [HttpPost]
        [Route("Task/PerformAction")]
        public IHttpActionResult PerformAction(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.PerformAction(model);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetHistory")]
        public IHttpActionResult GetHistory(int siteTaskId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetHistory(siteTaskId).OrderByDescending(r=>r.Id);
                return new ResponseDataModel<IEnumerable<SiteTaskLogModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetSingleHistory")]
        public IHttpActionResult GetSingleHistory(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetSingleHistory(id);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Task/SaveInterval")]
        public IHttpActionResult SaveInterval(IntervalModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _intervalService.Add(model);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/Transfer")]
        public IHttpActionResult Transfer(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {

                _taskService.Transfer(model);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task"
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetInterval")]
        public IHttpActionResult GetInterval(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _intervalService.Get(id);
                return new ResponseDataModel<IntervalModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetIntervals")]
        public IHttpActionResult GetIntervals(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _intervalService.GetIntervals(accountId);
                return new ResponseDataModel<IEnumerable<IntervalModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllActivityRoles")]
        public IHttpActionResult GetAllActivityRoles(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetActivityRoles(accountId).OrderBy(c => c.Name).ToList();
                return new ResponseDataModel<IEnumerable<ActivityRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveActivityRole")]
        public IHttpActionResult SaveActivityRole(ActivityRoleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                 _taskService.AddActivityRole(model);
                return new ResponseDataModel<IEnumerable<ActivityRoleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",                 
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetActivityRole")]
        public IHttpActionResult GetActivityRole(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetActivityRole(id);
                return new ResponseDataModel<ActivityRoleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

    }
}
