﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class CommonRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public CommonRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public List<Facility> GetAllFacilites()
        {
            return db.Facilities.ToList();
        }

        public List<LoanType> GetAllLoanTypes()
        {
            return db.LoanTypes.ToList();
        }

        public List<MessageType> GetAllConfirmationTypes()
        {
            return db.MessageTypes.ToList();
        }

        public List<Role> GetAllUserRoles()
        {
            return db.Roles.Where(r => r.IsUser).ToList();
        }

        public List<Role> GetAllModuleRoles()
        {
            return db.Roles.Where(r => r.IsModule).ToList();
        }

        public List<Role> GetAllRoles() {
            return db.Roles.ToList();
        }

        public List<CompanyType> GetAllCompanyTypes()
        {
            return db.CompanyTypes.ToList();
        }

        public Role GetRoleByName(string roleName) {
            return db.Roles.Where(r => r.Name == roleName).SingleOrDefault();
        }

        public MessageType GetConfirmationType(int id) {
            return db.MessageTypes.Where(c => c.Id == id).SingleOrDefault();
        }

        #region DASHBOARD

        public int AccountsCount() {
            return db.Accounts.Where(a => a.IsDeleted == false).Count();
        }

        public int TotalUsersCount()
        {
            return db.Users.Where(u => u.IsDeleted == false).Count();
        }

        public int ActiveUsersCount()
        {
            return db.Users.Where(u => u.IsActive == true && (u.IsDeleted == null || u.IsDeleted == false)).Count();
        }

        public int InActiveUsersCount()
        {
            return db.Users.Where(u => u.IsActive == false || (u.IsDeleted == true)).Count();
        }

        public int SitesCount() {
            return db.Sites.Where(s => s.IsDeleted == false).Count();
        }

        public int GetUserCountByAccountId(int id) {
            return db.Users.Where(u => u.AccountId == id).Count();
        }

        public List<Log> GetMostRecentLogs() {
            return db.Logs.OrderByDescending(l => l.CreatedDate).Take(10).ToList();
        }

        public List<Log> GetErrorLogs()
        {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Error).OrderByDescending(l => l.CreatedDate).ToList();
        }

        public int LogInfoCount() {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Information).Count();
        }

        public int LogWarningCount() {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Warning).Count();
        }

        public int LogErrorCount() {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Error).Count();
        }

        public int LogNotificationCount() {
            return db.Logs.Where(l => l.LogType == (int)LogTypeEnum.Notification).Count();
        }
        #endregion
    }
}
