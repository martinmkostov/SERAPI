﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueWebService
    {
        IEnumerable<IssueWebModel> GetIssueWeb(int accountId);
        void SaveIssueWeb(IssueWebModel model);
        IssueWebModel GetById(int id);
    }

    public class IssueWebService : IIssueWebService
    {
        private readonly IIssueWebRepository _repository;
        public IssueWebService()
        {
            _repository = new IssueWebRepository();
        }
        public IssueWebModel GetById(int id)
        {
            var data = _repository.GetInlcudingChildrenId(id);
            return new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId =data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                }
            };
        }

        public IEnumerable<IssueWebModel> GetIssueWeb(int accountId)
        {
            return _repository.GetAllIncludingChildren(accountId).Select(data => new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                }
            });
        }

        public void SaveIssueWeb(IssueWebModel model)
        {
            if (model.IssueWebId == 0)
            {
                var data = new IssueWeb()
                {
                    //IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateCreated = DateTime.Now,
                    Status = (int)IssueStatusEnum.NotConfirmed,
                };

                _repository.Save(data);
            }
            else {
                var data = new IssueWeb()
                {
                    DateCreated = model.DateCreated,
                    IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateChecked = model.DateChecked,
                    DateFinished = model.DateFinished
                };

                _repository.Update(data);
            }
        }
    }
}
