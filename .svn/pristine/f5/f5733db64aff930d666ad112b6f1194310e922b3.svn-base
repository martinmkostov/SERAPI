﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class TaskController : ApiController
    {
        private ITaskService _taskService;
        public TaskController()
        {
            _taskService = new TaskService();
        }

        [HttpGet]
        [Route("Task/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTask(id);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllTasks();
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/Save")]
        public IHttpActionResult Save(TaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddTask(model);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.DeleteTask(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Task",
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllMainCategories")]
        public IHttpActionResult GetAllMainCategories()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllMainCategories();
                return new ResponseDataModel<IEnumerable<MainCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Task/GetAllSubCategories")]
        public IHttpActionResult GetAllSubCategories()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllSubCategories();
                return new ResponseDataModel<IEnumerable<SubCategoryModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveMainCategory")]
        public IHttpActionResult SaveMainCategory(MainCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddMainCategory(model);
                return new ResponseDataModel<MainCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/SaveSubCategory")]
        public IHttpActionResult SaveSubCategory(SubCategoryModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.AddSubCategory(model);
                return new ResponseDataModel<SubCategoryModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/PublishTask")]
        public IHttpActionResult PublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.PublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Task/UnPublishTask")]
        public IHttpActionResult UnPublishTask(SiteTaskModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.UnPublishTask(model);
                return new ResponseDataModel<SiteTaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetPublishedTask")]
        public IHttpActionResult GetPublishedTask(int Id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetPublishedTask(Id);
                return new ResponseDataModel<IEnumerable<SiteTaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetAllBySiteId")]
        public IHttpActionResult GetAllBySiteId(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetAllTaskBySiteId(siteId);
                return new ResponseDataModel<IEnumerable<TaskModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Tasks",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Task/GetTaskBySiteIdAndTaskId")]
        public IHttpActionResult GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _taskService.GetTaskBySiteIdAndTaskId(taskId,siteId);
                return new ResponseDataModel<TaskModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Task",
                    Data = data
                };
            }));
        }


        [HttpPost]
        [Route("Task/Confirm")]
        public IHttpActionResult Confirm(SiteTaskLogModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _taskService.Confirm(model);
                return new ResponseDataModel<SiteTaskLogModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Task",
                    Data = model
                };
            }));
        }

    }
}
