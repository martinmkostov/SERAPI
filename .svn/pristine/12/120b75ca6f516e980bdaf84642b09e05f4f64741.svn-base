import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { LoanService, TenantService, SiteService } from '../../../services';

import { LoanModel } from '../../../models';
@Component({
    selector: 'app-mobile-loans-preview',
    templateUrl: './loans-preview.component.html',
    styleUrls: ['../../layout/mobile.css', '../../layout/layout.component.scss']
})
export class MobileLoansPreviewComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    model : any;
    status : any;
    badgeClass: any;
    customerType: any;
    customerTypeClass: any;
    returned: boolean = false;
    public withData: boolean = true;
    public showSpinner: boolean = false;
    public hidden: string = 'hidden';
    public isDetail: boolean = false;
    constructor(
        private loanService : LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastsManager
    ) {}

    public ngOnInit() {
        this.model = new LoanModel();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        this.route.params.subscribe((params) => {
            const id = params['id'];
            if(id) {
                this.loadData(id);
                this.isDetail = true;
            }
        });

    }

    public loadLoanPreview(data: any){
        //this.model = data;
        this.loadData(data.Id);
    }

    public loadData(id: number){
        this.loanService.getLoan(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {

            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;

            if(response.ErrorCode || !this.model)
            {
                if(response.ErrorCode)
                {
                    this.toastr.error(response.Message, "Error"); 
                }
                
                this.model = new LoanModel();
                this.withData = false;
                this.hidden = 'hidden';
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.withData = true;
                this.hidden = '';

                this.model.PhotoUrl = response.Data.PhotoUrl ? response.Data.PhotoUrl : '';
                if(this.model.Status === 'Active'){
                    this.status = 'ACTIVE';
                    this.badgeClass = ' badge-info';
                }
                else if(this.model.Status === 'NotConfirmed'){
                    this.status = 'NOT CONFIRMED';
                    this.badgeClass = ' badge-primary';
                }
                else if(this.model.Status === 'PassedReturnedData'){
                    this.status = 'PASSED RETURNED DATE';
                    this.badgeClass = ' badge-warning';
                }
                else if(this.model.Status === 'Returned'){
                    this.status = 'RETURNED';
                    this.badgeClass = ' badge-success';
                    this.returned = true;
                }
                else if(this.model.Status === 'Deleted'){
                    this.status = 'DELETED';
                    this.badgeClass = ' badge-danger';
                }

                if(this.model.CustomerTypeId === 1)
                {
                    this.customerType = 'CUSTOMER';
                    this.customerTypeClass = 'badge-secondary';
                }
                else {
                    this.customerType = 'TENANT';
                    this.customerTypeClass = 'badge-dark';
                }

            }

        });
    }

    public search(){
        this.showSpinner = true;
        this.hidden = 'hidden';
        this.loanService.getLoanByLoanId(this.model.LoanId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            if(response.ErrorCode || !this.model)
            {
                if(response.ErrorCode)
                {
                    this.toastr.error(response.Message, "Error"); 
                }
                
                this.model = new LoanModel();
                this.withData = false;
                this.hidden = 'hidden';
            }
            else {
                this.withData = true;
                this.hidden = '';
                this.loadData(this.model.Id);
            }
        });
        
    }

    public navigateToLanding(){
        this.router.navigate([`mobile/loans/`]);
    }
}
