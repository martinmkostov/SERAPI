﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class TenantService
    {
        TenantRepository _tenantRepository;
        LogRepository _logRepository;
        public TenantService() {
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
        }

        public TenantModel Get(int id) {
            try
            {
               var tenant = _tenantRepository.Get(id);
                return entityToModel(tenant);
            }
            catch(Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
            
        }

        public List<TenantModel> GetAll()
        {
            try
            {
                return _tenantRepository.GetAll()
                    .Select(t => new TenantModel() {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }

        }

      
        public List<TenantModel> GetAllByAccountId(int id)
        {
            try
            {

                IEnumerable<Tenant> data;

                if (id == 0)
                {
                    data = _tenantRepository.GetAll();
                }
                else
                {
                    data = _tenantRepository.GetAllByAccountId(id);
                }

                return data.Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetAllBySiteId(int id) {
            try
            {
                return _tenantRepository.GetAllBySiteId(id)
                    .Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public void SaveTenant(TenantModel model) {
            try
            {
                _tenantRepository.SaveTenant(model);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void DeleteTenant(int id)
        {
            try
            {
                _tenantRepository.DeleteTenant(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
                throw ex;
            }
        }

        public TenantModel entityToModel(Tenant entity) {
            return new TenantModel()
            {
                Id = entity.Id,
                SiteId = entity.SiteId,
                Name = entity.Name,
                Email = entity.Email,
                Phone = entity.Phone,
                PermiseType = entity.PermiseType,
                VisitingAddress = entity.VisitingAddress,
                UserName = entity.UserName,
                Password = entity.Password,
                InsuranceCompany = entity.InsuranceCompany,
                SecurityCompany = entity.SecurityCompany,
                SiteManagerName = entity.SiteManagerName,
                SiteManagerMobilePhone = entity.SiteManagerPhone,
                SiteManagerPhone = entity.SiteManagerPhone,
                ContractName = entity.ContractName,
                ContractVatNo = entity.ContractVatNo,
                InvoiceAddress = entity.InsuranceCompany,
                InvoiceZipAndCity = entity.InvoiceZipAndCity,
                ContactComments = entity.ContactComments,
                CCTV = entity.CCTV,
                IntrusionAlarm = entity.IntrusionAlarm,
                LastUpdatedDate = entity.LastUpdatedDate,
                CreatedDate = entity.CreatedDate,
                SiteManagerEmail = entity.SiteManagerEmail,
                Flag = entity.Flag,
                IsActive = entity.IsActive,
                PropertyDesignation = entity.PropertyDesignation,
                UseWrittenStatement = entity.UseWrittenStatement
            };
        }
    }
}
