﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class TenantService
    {
        TenantRepository _tenantRepository;
        SiteRepository _siteRepository;
        CommonRepository _commonRepository;
        LogRepository _logRepository;
        
        public TenantService() {
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
            _siteRepository = new SiteRepository();
            _commonRepository = new CommonRepository();
        }

        public TenantModel Get(int id) {
            try
            {
               var tenant = _tenantRepository.Get(id);
                return entityToModel(tenant);
            }
            catch(Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
            
        }

        public TenantTypeModel GetTenantType(int id) {
            var data = _commonRepository.GetTenantTypes().Where(t => t.Id == id).SingleOrDefault();
            if (data != null) {
                return entityToModel(data);
            }
            return new TenantTypeModel();
        }

        public PremiseTypeModel GetPremiseType(int id)
        {
            var data = _tenantRepository.GetPremiseType(id);
            if (data != null) {
                return new PremiseTypeModel()
                {
                    Id = data.Id,
                    TenantTypeId = data.TenantTypeId,
                    AccountId = data.AccountId,
                    TypeName = data.TypeName,
                    Description = data.Description
                };
            }
            return new PremiseTypeModel();
            
        }

        public SubPremiseTypeModel GetSubPremiseType(int id)
        {
            var data = _tenantRepository.GetSubPremiseType(id);
            if (data != null) {
                return new SubPremiseTypeModel()
                {
                    Id = data.Id,
                    PremiseTypeId = data.PremiseTypeId,
                    AccountId = data.AccountId,
                    TypeName = data.TypeName,
                    Description = data.Description,
                    PremiseTypeName = _tenantRepository.GetPremiseType(data.PremiseTypeId) != null ? _tenantRepository.GetPremiseType(data.PremiseTypeId).TypeName : "N / A"
                };
            }
            return new SubPremiseTypeModel();
            
        }

        public JobTitleModel GetGetJobTitle(int id) {
            var data = _tenantRepository.GetJobTitle(id);
            return new JobTitleModel()
            {
                Id = data.Id,
                TitleName = data.TitleName,
                Description = data.Description
            };
        }

        public List<TenantModel> GetAll()
        {
            try
            {
                return _tenantRepository.GetAll()
                    .Select(t => new TenantModel() {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType.HasValue ? t.PermiseType.Value : 0,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }

        }
      
        public List<TenantModel> GetAllByAccountId(int id, int siteId = 0)
        {
            try
            {

                IEnumerable<Tenant> data;

                if (id == 0)
                {
                    data = _tenantRepository.GetAll();
                }
                else
                {
                    data = _tenantRepository.GetAllByAccountId(id);
                }

                if (siteId > 0)
                {
                    data = data.Where(x => x.SiteId == siteId);
                }



                SiteService siteService = new SiteService();
                return data.Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        Site = entityToModel(_siteRepository.Get(t.SiteId)),
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement,
                        TenantType = t.TenantType.HasValue ? t.TenantType.Value : 0,
                        PermiseType = t.PermiseType.HasValue ? t.PermiseType.Value : 0,
                        SubPremiseType = t.SubPremiseType.HasValue ? t.SubPremiseType.Value : 0,
                        TenantTypeName = t.TenantType.HasValue && t.TenantType.Value > 0 ?  GetTenantType(t.TenantType.Value).TypeName : "N / A" ,
                        PremiseTypeName = t.PermiseType.HasValue && t.PermiseType.Value > 0 ? GetPremiseType(t.PermiseType.Value).TypeName : "N / A",
                        SubPremiseTypeName = t.SubPremiseType.HasValue && t.SubPremiseType.Value > 0 ? GetSubPremiseType(t.SubPremiseType.Value).TypeName : "N / A"
                }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetAllBySiteId(int id) {
            try
            {
                return _tenantRepository.GetAllBySiteId(id)
                    .Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType.HasValue ? t.PermiseType.Value : 0,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement,
                        TenantType = t.TenantType.HasValue ? t.TenantType.Value : 0,
                        SubPremiseType = t.SubPremiseType.HasValue ? t.SubPremiseType.Value : 0
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantContactModel> GetAllTenantContactsByTenantId(int tenantId) {
            var data = _tenantRepository.GetAllTenantContactsByTenantId(tenantId).ToList();
            return data.Select(t => new TenantContactModel()
            {
                Id = t.Id,
                ContactId = t.Id,
                TenantContactId = t.TenantContactId,
                FullName = t.FullName,
                EmailAddress = t.EmailAddress,
                MobileNumber = t.MobileNumber,
                PhoneNumber = t.PhoneNumber,
                JobTypeId = t.JobTypeId,
                JobTypeName = GetGetJobTitle(t.JobTypeId).TitleName
            })
            .OrderBy(t => t.FullName)
            .ToList();
        }

        public void SaveTenantContact(TenantContactModel contact) {
            _tenantRepository.SaveTenantContacts(contact);
        }

        private string JobTypeName(int tenantId, int contactId) {
            var data = _tenantRepository.GetTenantContact(tenantId, contactId);
            switch(data.JobTypeId)
            {
                case 1: return "Shop Owner";
                case 2: return "Site Manager";
                case 3: return "Contact Manager";
                case 4: return "Cashier";
                case 5: return "Other";
                default: return "N / A";
            }
        }

        public int SaveTenant(TenantModel model) {
            try
            {
               model.Id = _tenantRepository.SaveTenant(model);
               return model.Id;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void DeleteTenant(int id)
        {
            try
            {
                _tenantRepository.DeleteTenant(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
                throw ex;
            }
        }

        public bool SaveTenantType(TenantTypeModel model) {

            if(_commonRepository.GetTenantTypes().Any(x=>x.TypeName == model.TypeName))
            {
                return false;
            }
            _tenantRepository.SaveTenantType(model);
            return true;
        }

        public bool SavePremiseType(PremiseTypeModel model) {
            if (_commonRepository.GetPremiseTypes().Any(r => r.TypeName == model.TypeName))
            {
                return false;
            }
            _tenantRepository.SavePremiseType(model);
            return true;
        }

        public bool SaveSubPremiseType(SubPremiseTypeModel model) {
            if (_commonRepository.GetSubPremiseTypes().Any(r => r.TypeName == model.TypeName))
            {
                return false;
            }
            _tenantRepository.SaveSubPremiseType(model);
            return true;
        }

        public void SaveJobTitle(JobTitleModel model) {
            if (_commonRepository.GetJobTitles().Any(r => r.TitleName == model.TitleName))
            {
                return;
            }
            _tenantRepository.SaveJobTite(model);
        }

        public void DeleteTenantType(int id) {

            if (_tenantRepository.GetAll().Any(r => r.TenantType == id))
            {
                return;
            }
            _tenantRepository.DeleteTenantType(id);
        }

        public void DeletePremiseType(int id) {
            if (_tenantRepository.GetAll().Any(r => r.PermiseType == id))
            {
                return;
            }
            _tenantRepository.DeletePremiseType(id);
        }

        public void DeleteSubPremiseType(int id)
        {
            if (_tenantRepository.GetAll().Any(r => r.SubPremiseType == id))
            {
                return;
            }
            _tenantRepository.DeleteSubPremiseType(id);
        }

        public void DeleteJobTittle(int id) {         
            _tenantRepository.DeleteJobTitle(id);
        }

        public void DeleteTenantContact(int id) {
            _tenantRepository.DeleteTenantContact(id);
        }

        public List<TenantTypeModel> GetAllTenantTypes(int accountId)
        {
            return _commonRepository.GetTenantTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new TenantTypeModel()
                {
                    Id = t.Id,
                    AccountId = t.AccountId,
                    TypeName = t.TypeName,
                    Description = t.Description,
                    IsSystem = t.AccountId == 0
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<PremiseTypeModel> GetAllPremiseTypes(int accountId)
        {
            return _commonRepository.GetPremiseTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new PremiseTypeModel()
                {
                    Id = t.Id,
                    TenantTypeId = t.TenantTypeId,
                    TypeName = t.TypeName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    IsSystem = t.AccountId == 0,
                    TenantTypeName = GetTenantType(t.TenantTypeId).TypeName
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<SubPremiseTypeModel> GetAllSubPremiseTypes(int accountId)
        {
            return _commonRepository.GetSubPremiseTypes()
                .Where(c => c.AccountId == accountId)
                .Select(t => new SubPremiseTypeModel()
                {
                    Id = t.Id,
                    PremiseTypeId = t.PremiseTypeId,
                    TypeName = t.TypeName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    PremiseTypeName = _tenantRepository.GetPremiseType(t.PremiseTypeId).TypeName,
                    IsSystem = t.AccountId == 0
                })
                .OrderBy(t => t.TypeName)
                .ToList();
        }

        public List<JobTitleModel> GetAllGetJobTitles(int accountId)
        {
            return _commonRepository.GetJobTitles()
                .Where(c => c.AccountId == accountId)
                .Select(t => new JobTitleModel()
                {
                    Id = t.Id,
                    TitleName = t.TitleName,
                    AccountId = t.AccountId,
                    Description = t.Description,
                    IsSystem = t.AccountId == 0
                })
                .OrderBy(t => t.TitleName)
                .ToList();
        }

        public TenantModel entityToModel(Tenant entity) {
            return new TenantModel()
            {
                Id = entity.Id,
                SiteId = entity.SiteId,
                Name = entity.Name,
                Email = entity.Email,
                Phone = entity.Phone,
                PermiseType = entity.PermiseType.HasValue ? entity.PermiseType.Value : 0,
                VisitingAddress = entity.VisitingAddress,
                UserName = entity.UserName,
                Password = entity.Password,
                InsuranceCompany = entity.InsuranceCompany,
                SecurityCompany = entity.SecurityCompany,
                SiteManagerName = entity.SiteManagerName,
                SiteManagerMobilePhone = entity.SiteManagerPhone,
                SiteManagerPhone = entity.SiteManagerPhone,
                Site = entityToModel(_siteRepository.Get(entity.SiteId)),
                ContractName = entity.ContractName,
                ContractVatNo = entity.ContractVatNo,
                InvoiceAddress = entity.InsuranceCompany,
                InvoiceZipAndCity = entity.InvoiceZipAndCity,
                ContactComments = entity.ContactComments,
                CCTV = entity.CCTV,
                IntrusionAlarm = entity.IntrusionAlarm,
                LastUpdatedDate = entity.LastUpdatedDate,
                CreatedDate = entity.CreatedDate,
                SiteManagerEmail = entity.SiteManagerEmail,
                Flag = entity.Flag,
                IsActive = entity.IsActive,
                PropertyDesignation = entity.PropertyDesignation,
                UseWrittenStatement = entity.UseWrittenStatement,
                TenantType = entity.TenantType.HasValue ? entity.TenantType.Value : 0,
                SubPremiseType = entity.SubPremiseType.HasValue ? entity.SubPremiseType.Value : 0
            };
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }

        public static TenantTypeModel entityToModel(TenantType entity) {
            return new TenantTypeModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description
            };
        }

        public static PremiseTypeModel entityToModel(PremiseType entity) {
            return new PremiseTypeModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description
            };
        }

        public static SubPremiseTypeModel entityToModel(SubPremiseType entity) {
            return new SubPremiseTypeModel() {
                Id = entity.Id,
                AccountId = entity.AccountId,
                TypeName = entity.TypeName,
                Description = entity.Description
            };
        }
    }
}
