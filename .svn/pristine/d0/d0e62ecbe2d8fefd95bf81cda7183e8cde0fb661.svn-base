﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class CustomTypeController : ApiController
    {
        private CustomTypeService _service;
        public CustomTypeController()
        {
            _service = new CustomTypeService();
        }

        [HttpGet]
        [Route("CustomType/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetCustomTypeById(id);
                return new ResponseDataModel<CustomTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Type Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("CustomType/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetCustomTypes();
                return new ResponseDataModel<IEnumerable<CustomTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Custom Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("CustomType/Save")]
        public IHttpActionResult Save(CustomTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddCustomType(model);
                return new ResponseDataModel<CustomTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Custom Type",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("CustomType/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteCustomTypeById(id);
                return new ResponseDataModel<CustomTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Custom Type",            
                };
            }));
        }
    }
}
