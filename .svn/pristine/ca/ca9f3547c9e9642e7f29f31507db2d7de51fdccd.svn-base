﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IssueWebController : ApiController
    {
        private readonly IIssueWebService _service;
        public IssueWebController()
        {
            _service = new IssueWebService();
        }

        [HttpGet]
        [Route("Issue/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetById(id);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }


        [HttpGet]
        [Route("Issue/GetAll")]
        public IHttpActionResult GetAll(int accountId, int siteId =0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIssueWeb(accountId, siteId).OrderBy(x=>x.DateCreated);
                return new ResponseDataModel<IEnumerable<IssueWebModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Save")]
        public IHttpActionResult Save(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.SaveIssueWeb(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IssueWeb/Delete")]
        public IHttpActionResult Delete(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIssueWeb(model.IssueWebId);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Issue",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Issue/Reminder")]
        public IHttpActionResult Reminder(IssueWebModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.sendReminder(model);
                return new ResponseDataModel<IssueWebModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Sent reminder",
                    Data = model
                };
            }));
        }
    }
}
