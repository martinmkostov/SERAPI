﻿using Microsoft.VisualStudio.DebuggerVisualizers;
using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SER.FileManager.Repository
{
    public class SiteRepository
    {
        private string connString;

        public SiteRepository()
        {
            connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;
        }

        public SiteFolderRole FindSiteFolderRole(int siteId, string folder, int userId)
        {
            var result = default(SiteFolderRole);

            using (var connection = new SqlConnection(connString))
            {
                var query = "SELECT Id, SiteId, Folder, UserId, RoleId FROM [SiteFolderRights] WHERE SiteId = @siteId AND Folder = @folder AND UserId = @userId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);
                    command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4])
                                };

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId, string folder)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = "SELECT Id, SiteId, Folder, UserId, RoleId FROM [SiteFolderRights] WHERE SiteId = @siteId AND Folder = @folder";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId, int userId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = "SELECT Id, SiteId, Folder, UserId, RoleId FROM [SiteFolderRights] WHERE SiteId = @siteId AND UserId = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);
                command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = "SELECT Id, SiteId, Folder, UserId, RoleId FROM [SiteFolderRights] WHERE SiteId = @siteId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public void UpdateSiteFolderRole(SiteFolderRole folderRole)
        {
            var existingRole = this.FindSiteFolderRole(folderRole.SiteId, folderRole.Folder, folderRole.UserId);
            var query = string.Empty;

            if (folderRole.RoleId != -1 && existingRole != null)
            {
                query = $"UPDATE [SiteFolderRights] SET RoleId = ${folderRole.RoleId} WHERE [Id]={existingRole.Id}";
            }
            else if (folderRole.RoleId != -1)
            {
                query = $"INSERT INTO [SiteFolderRights] (SiteId, Folder, UserId, RoleId) VALUES ({folderRole.SiteId}, '{folderRole.Folder}', {folderRole.UserId}, {folderRole.RoleId})";
            }
            else if (folderRole.RoleId == -1 && existingRole != null)
            {
                query = $"DELETE FROM [SiteFolderRights] WHERE [Id]={existingRole.Id}";
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
