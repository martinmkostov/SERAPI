﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class AccountService
    {
        AccountRepository _accountRepository;
        AccountModuleRepository _accountModuleRepository;
        UserService _userService;
        LogRepository _logRepository;
        DepartmentRepository _departmentRepository;
        AccountSettingsService _accountSettingService;

        public AccountService()
        {
            _accountRepository = new AccountRepository();
            _logRepository = new LogRepository();
            _accountModuleRepository = new AccountModuleRepository();
            _departmentRepository = new DepartmentRepository();

            _userService = new UserService();
            _accountSettingService = new AccountSettingsService();
        }

        public AccountModel Get(int id)
        {
            Account account = null;
            try
            {
                account = _accountRepository.Get(id);
                return new AccountModel()
                {
                    Id = account.Id,
                    Name = account.Name.ToLower().Replace(" ", "_"),
                    IsDeleted = account.IsDeleted,
                    Company = account.Company,
                    EmailAddress = account.EmailAddress,
                    IsActive = account.IsActive,
                    CompanyType = account.CompanyType,
                    ActiveUsers = account.Users.Select(u => new UserModel()
                    {
                        Id = u.Id,
                        AccountId = u.AccountId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        UserName = u.UserName,
                        Mobile = u.Mobile,
                        IsActive = u.IsActive.HasValue ? u.IsActive.Value : false
                    })
                    .Where(u => u.IsActive.HasValue && u.IsActive.Value == true)
                    .ToList(),
                    Departments = _departmentRepository.GetAllDepartmentsByAccountId(id).Select(d => new DepartmentModel()
                    {
                        Id = d.Id,
                        Name = d.Name,
                        DepartmentHead = d.DepartmentHead,
                        ContactNumber = d.ContactNumber,
                        Description = d.Description,
                        AccountId = d.AccountId,
                        CreatedDate = d.CreatedDate
                    }).ToList(),
                    InActiveUsers = account.Users.Select(u => new UserModel()
                    {
                        Id = u.Id,
                        AccountId = u.AccountId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        UserName = u.UserName,
                        Mobile = u.Mobile,
                        IsActive = u.IsActive.HasValue ? u.IsActive.Value : false
                    })
                    .Where(u => u.IsActive.HasValue && u.IsActive.Value == false)
                    .ToList(),
                    Sites = account.Sites.Select(s => new SiteModel()
                    {
                        Id = s.Id,
                        AccountId = s.AccountId,
                        Name = s.Name,
                        Address = s.Address,
                        City = s.City,
                        PostalCode = s.PostalCode,
                        PhoneNumber = s.PhoneNumber,
                        FaxNumber = s.FaxNumber,
                        ContactPerson = s.ContactPerson,
                        ContactPersonNumber = s.ContactPersonNumber,
                        IsActive = s.IsActive.HasValue ? s.IsActive.Value : false
                    }).ToList(),
                    Modules = _accountModuleRepository.GetModulesByAccountId(account.Id)
                    .Select(a => new ModuleModel() {
                        Id = a.Id,
                        ShortName = a.Module.ShortName,
                        Title = a.Module.Title
                    })
                    .OrderBy(o => o.Id)
                    .ToList(),
                    LastUpdatedDate = account.LastUpdatedDate,
                    CreatedDate = account.CreatedDate,
                    ContactName = account.ContactName,
                    ContactNumber = account.ContactNumber,
                    AccountSettings = GetAccountSettings(account.Id)
                };
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, account != null ? account.Id : 0, 0);
                return null;
            }
            
        }

        public UserModel GetAdmin(int id) {
            return _accountRepository.GetAccountAdmin(id);
        }

        public List<AccountModel> GetAll()
        {
            try
            {
                var data = _accountRepository.GetAll()
                .Select(a => new AccountModel()
                {
                    Id = a.Id,
                    Name = a.Name.ToLower().Replace(" ", "_"),
                    Company = a.Company,
                    EmailAddress = a.EmailAddress,
                    IsActive = a.IsActive,
                    CompanyType = a.CompanyType,
                    ActiveUsers = a.Users.Select(u => new UserModel()
                    {
                        Id = u.Id,
                        AccountId = u.AccountId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        UserName = u.UserName,
                        Mobile = u.Mobile,
                        IsActive = u.IsActive.HasValue ? u.IsActive.Value : false
                    })
                    .Where(u => u.IsActive.HasValue && u.IsActive.Value == true)
                    .ToList(),
                    InActiveUsers = a.Users.Select(u => new UserModel()
                    {
                        Id = u.Id,
                        AccountId = u.AccountId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        UserName = u.UserName,
                        Mobile = u.Mobile,
                        IsActive = u.IsActive.HasValue ? u.IsActive.Value : false
                    })
                    .Where(u => u.IsActive.HasValue && u.IsActive.Value == false)
                    .ToList(),
                    Sites = a.Sites.Select(s => new SiteModel()
                    {
                        Id = s.Id,
                        AccountId = s.AccountId,
                        Name = s.Name,
                        Address = s.Address,
                        City = s.City,
                        PostalCode = s.PostalCode,
                        PhoneNumber = s.PhoneNumber,
                        FaxNumber = s.FaxNumber,
                        ContactPerson = s.ContactPerson,
                        ContactPersonNumber = s.ContactPersonNumber,
                        IsActive = s.IsActive.HasValue ? s.IsActive.Value : false
                    }).ToList(),
                    Modules = _accountModuleRepository.GetModulesByAccountId(a.Id)
                    .Select(m => new ModuleModel()
                    {
                        Id = m.Id,
                        ShortName = m.Module.ShortName,
                        Title = m.Module.Title
                    })
                    .OrderBy(o => o.Id)
                    .ToList(),
                    CreatedDate = a.CreatedDate,
                    LastUpdatedDate = a.LastUpdatedDate,
                    ContactName = a.ContactName,
                    ContactNumber = a.ContactNumber
                })
                .OrderBy(a => a.Name)
                .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }
            
        }

        public AccountModel SaveAccount(AccountModel model)
        {
            try
            {
                var test = _accountRepository.Get(model.Company);
                var test2 = _accountRepository.Get(model.Name);
                var test3 = _accountRepository.Get(model.Id);
                if ((_accountRepository.Get(model.Company) != null || _accountRepository.Get(model.Name) != null) && _accountRepository.Get(model.Id).Company != model.Company)
                {
                    throw new Exception("Could not save this account since it's already been taken. Choose another Account Name.");
                }
                int originalId = model.Id;
                var newlyCreatedAccount = _accountRepository.SaveAccount(model);
                if (originalId == 0)
                {
                    //only auto create a user if account is new
                    //auto create user admin
                    var accountUserModel = _userService.CreateAdminUser(model);
                    //auto assign all new accounts to the default/active modules (Incident, Admin, Loan, Tenants)
                    _accountModuleRepository.SaveAllActiveModuleToAccount(newlyCreatedAccount.Id);
                    Email.SendCreateUserConfirmation(accountUserModel);
                    //log user / account creation
                    _logRepository.Log(LogTypeEnum.Information,
                        LogMessagingSettings.Save,
                        LogMessagingSettings.MessageCreateAccount,
                        accountUserModel != null ? accountUserModel.AccountId : 0, 0);

                }
                return newlyCreatedAccount;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void DeleteAccount(int id) {
            try
            {
                _accountRepository.DeleteAccount(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, id, 0);
            }
        }

        public bool IsDuplicate(string companyName)
        {
            try {
                return _accountRepository.Get(companyName) != null;
            } catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<AccountSettingModel> GetAccountSettings(int accountId) {
            return _accountSettingService.GetAccountSettings(accountId);
        }

        private static List<AccountSettingModel> AccountSettings(int accountId)
        {
            AccountService accountService = new AccountService();
            return accountService.GetAccountSettings(accountId);
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                ContactName = entity.ContactName,
                ContactNumber = entity.ContactNumber,
                AccountSettings = AccountSettings(entity.Id)
            };
        }
    }
}
