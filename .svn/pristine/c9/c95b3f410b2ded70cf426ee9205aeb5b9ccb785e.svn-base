﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class LoanService
    {
        LoanRepository _loanRepository;
        CustomerRepository _customerRepository;
        SiteRepository _siteRepository;
        CommonRepository _commonRepository;
        TenantRepository _tenantRepository;
        LogRepository _logRepository;
        EmailRepository _emailRepository;

        CustomerService customerService;
        TenantService tenantService;
        SiteService siteService;
        private const int noDaysOfReturn = 10;
        public LoanService() {
            _loanRepository = new LoanRepository();
            _customerRepository = new CustomerRepository();
            _siteRepository = new SiteRepository();
            _commonRepository = new CommonRepository();
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
            _emailRepository = new EmailRepository();

            customerService = new CustomerService();
            tenantService = new TenantService();
            siteService = new SiteService();
        }

        public LoanModel Get(int id) {
            var loan = _loanRepository.Get(id);
            if (loan == null)
            {
                return null;
            }
            var data = new LoanModel()
            {
                Id = loan.Id,
                CustomerId = loan.CustomerId,
                SiteId = loan.SiteId,
                TypeId = loan.TypeId.Value,
                StatusId = loan.StatusId.Value,
                ItemNameId = loan.ItemNameId,
                Description = loan.Description,
                Notes = loan.Notes,
                PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                MessageTypeId = loan.MessageTypeId.Value,
                LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = loan.ReturnedDate,
                LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                CustomerTypeId = loan.CustomerTypeId,
                Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                GivenBy = loan.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                LoanId = loan.LoanId
            };
            return data;
        }

        public List<LoanModel> GetAllBySiteId(int siteId)
        {

            var data = _loanRepository.GetAll();

            return data.Where(l => l.SiteId == siteId)
            .Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : TenantToCustomer(_tenantRepository.Get(l.CustomerId)),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                LoanId = l.LoanId
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();
        }

        public List<LoanModel> GetAll() {

            var data = _loanRepository.GetAll();

            return data.Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                CustomerTypeId = l.CustomerTypeId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : TenantToCustomer(_tenantRepository.Get(l.CustomerId)),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                LoanId = l.LoanId
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();
        }

        public List<LoanCustomerModel> GetAllMergedCustomer(int accountId, string keyword) {
            var customers = _customerRepository.GetAll();
            var tenants = _tenantRepository.GetAllByAccountId(accountId);
            var finalCustomers = customers.Select(c => new LoanCustomerModel()
            {
                Id = c.Id,
                Type = "Customer",
                Name = c.Name
            })
            .Union(tenants.Select(t => new LoanCustomerModel() {
                Id = t.Id,
                Type = "Tenant",
                Name = t.Name
            }))
            .ToList();
            if (!string.IsNullOrEmpty(keyword)) {
                keyword = keyword.ToLower();
                var filtered = finalCustomers.Where(c => c.Name.ToLower().StartsWith(keyword) || c.Type.ToLower().StartsWith(keyword)).ToList();
                return filtered;
            }
            return finalCustomers;
        }

        public List<LoanModel> GetAllByAccountId(int id, string keyword) {
            List<LoanModel> loans = new List<LoanModel>();
            var data = _loanRepository.GetAllByAccountId(id);
            if (!string.IsNullOrEmpty(keyword))
            {
                //keyword = keyword.ToLower();
                //data = _loanRepository.GetAllByAccountId(id)
                //    .Where(l => 
                //        l.LoanId.ToLower().StartsWith(keyword) ||
                //        l.ItemNameId.ToLower().StartsWith(keyword) ||
                //        l.Description.ToLower().StartsWith(keyword) ||
                //        l.GivenBy.ToLower().StartsWith(keyword) ||
                //        l.ReceivedBy.ToLower().StartsWith(keyword)
                //    )
                //    .ToList();
                data = _loanRepository.GetAllWithFilter(id, keyword);
            }
            
            
            loans = data.Select(l => new LoanModel()
            {
                Id = l.Id,
                CustomerId = l.CustomerId,
                CustomerTypeId = l.CustomerTypeId,
                TypeId = l.TypeId.Value,
                StatusId = l.StatusId.Value,
                ItemNameId = l.ItemNameId,
                Description = l.Description,
                Notes = l.Notes,
                PhotoUrl = l.PhotoUrl,
                MessageTypeId = l.MessageTypeId.Value,
                LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                Customer = l.CustomerTypeId == 1 ? customerService.Get(l.CustomerId) : TenantToCustomer(_tenantRepository.Get(l.CustomerId)),
                LoanType = entityToModel(_loanRepository.GetLoanType(l.TypeId.Value)),
                Status = Enum.GetName(typeof(StatusEnum), l.StatusId).ToString(),
                GivenBy = l.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                Site = entityToModel(_siteRepository.Get(l.SiteId)),
                SiteId = l.SiteId,
                LoanId = l.LoanId
            })
            .OrderByDescending(l => l.CreatedDate)
            .ToList();

            return loans;
        }

        public List<LoanTypeModel> GetAllLoanTypes() {
            return _loanRepository.GetAllLoanTypes()
                .Select(l => new LoanTypeModel() {
                    Id = l.Id,
                    Name = l.Name,
                    Description = l.Description
                }).ToList();
        }

        public List<LoanModel> GetAllWithFilter(int customerId, int typeId, int statusId, string itemName,
            string LoanedDate, string ReturnedDate, int confirmationType)
        {
            var data = _loanRepository.GetAllWithFilter(customerId, typeId, statusId, itemName, LoanedDate, ReturnedDate, confirmationType)
                .Select(l => new LoanModel()
                {
                    Id = l.Id,
                    LoanId = l.LoanId,
                    CustomerId = l.CustomerId,
                    TypeId = l.TypeId.Value,
                    StatusId = l.StatusId.Value,
                    ItemNameId = l.ItemNameId,
                    Description = l.Description,
                    Notes = l.Notes,
                    PhotoUrl = l.PhotoUrl,
                    MessageTypeId = l.MessageTypeId.Value,
                    LoanedDate = l.LoanedDate.HasValue ? l.LoanedDate.Value : DateTime.MinValue,
                    ReturnedDate = l.ReturnedDate.HasValue ? l.ReturnedDate.Value : DateTime.MinValue,
                    LastModifiedDate = l.LastModifiedDate.HasValue ? l.LastModifiedDate.Value : DateTime.MinValue,
                    CreatedDate = l.CreatedDate.HasValue ? l.CreatedDate.Value : DateTime.MinValue,
                    IsDeleted = l.IsDeleted.HasValue ? l.IsDeleted.Value : false,
                    GivenBy = l.GivenBy,
                    ReceivedBy = !string.IsNullOrEmpty(l.ReceivedBy) ? l.ReceivedBy : "( )",
                    ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(l.MessageTypeId.Value)),
                    Site = entityToModel(_siteRepository.Get(l.SiteId))
                })
                .OrderByDescending(l => l.CreatedDate)
                .ToList();
            return data;
        }

        public LoanModel GetLoanByLoanId(string loanId) {
            var loan = _loanRepository.Get(loanId);
            if (loan == null)
            {
                return null;
            }
            var data = new LoanModel()
            {
                Id = loan.Id,
                CustomerId = loan.CustomerId,
                SiteId = loan.SiteId,
                TypeId = loan.TypeId.Value,
                StatusId = loan.StatusId.Value,
                ItemNameId = loan.ItemNameId,
                Description = loan.Description,
                Notes = loan.Notes,
                PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                MessageTypeId = loan.MessageTypeId.Value,
                LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                ReturnedDate = loan.ReturnedDate,
                LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                CustomerTypeId = loan.CustomerTypeId,
                Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                GivenBy = loan.GivenBy,
                ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                LoanId = loan.LoanId
            };
            return data;
        }

        public LoanModel SaveLoan(LoanModel model)
        {
            try
            {
                
                int originalCustomerId = model.CustomerId;
                string photoString = model.PhotoString;
                bool isNew = model.Id == 0;

                if (model.ItemNameId.Contains(','))
                {
                    string originalItemNameId = model.ItemNameId;
                    foreach (string itemNameId in model.ItemNameId.Split(','))
                    {
                        model.Id = 0; //reset back to zero
                        model.ItemNameId = itemNameId;
                        _loanRepository.SaveLoan(model);
                    }
                    model.ItemNameId = originalItemNameId; // this will be used for the email
                }
                else
                {
                    _loanRepository.SaveLoan(model);
                }

                if (!string.IsNullOrEmpty(photoString)) {
                    //photoString, "LOANS", model.Id.ToString()
                    model.PhotoUrl = Service.Tools.File.SaveFile(new FileModel() {
                        AccountId = model.Site.AccountId,
                        FileName = model.Id.ToString(), // using loan id as the name for now
                        ModuleType = "LOANS",
                        ByteString = photoString
                    });
                    _loanRepository.SaveLoanPhoto(model.Id, model.PhotoUrl);
                }

                model.Site = entityToModel(_siteRepository.Get(model.SiteId));
                if (originalCustomerId == 0)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                        LogMessagingSettings.Save,
                        LogMessagingSettings.MessageCreateCustomer, model.Site.AccountId, 0);
                }
               
                if (isNew)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                       LogMessagingSettings.Save,
                       LogMessagingSettings.MessageCreateLoan, model.Site.AccountId, 0);

                    _emailRepository.GetAccountEmailTemplate(EmailTemplates.LoanCreate, model.Site.AccountId);
                    Email.SendCreateLoandConfirmation(model);
                }
                
                if (model.SendNotification)
                {
                    Email.SendLoanUpdateNotification(model);
                }


                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ConfirmLoan(string encryptedEmail, int id) {
            string email = Security.decrypt64BasedString(encryptedEmail);
            if (_loanRepository.Get(id) != null)
            {
                var loan = _loanRepository.Get(id);
                var customer = _customerRepository.Get(loan.CustomerId);
                if (customer != null && customer.Email == email)
                {
                    _loanRepository.ConfirmLoan(loan.Id);
                    var site = entityToModel(_siteRepository.Get(loan.SiteId));
                    var model = entityToModel(loan);

                    Email.SendLoanConfirm(model);
                    string subject = "LOAN CONFIRMATION";
                    string message = customer.Name + " Confirmed loan on " + site.Name + " at " + DateTime.Now.ToShortDateString();

                    _logRepository.Log(LogTypeEnum.Information,
                      subject,
                      message, 
                      site.AccountId, 0);
                }
            }
           
        }

        public void ReturnLoan(int id)
        {
            if (_loanRepository.Get(id) != null)
            {
                var loan = _loanRepository.Get(id);
                var customer = _customerRepository.Get(loan.CustomerId);
                if (customer != null)
                {
                    _loanRepository.ReturnLoan(loan.Id);
                    var site = entityToModel(_siteRepository.Get(loan.SiteId));
                    var model = entityToModel(loan);
                    Email.SendLoanReturn(model);

                    string subject = "LOAN RETURN";
                    string message = customer.Name + " Returned the loan on " + site.Name + " at " + DateTime.Now.ToShortDateString();

                    _logRepository.Log(LogTypeEnum.Information,
                      subject,
                      message,
                      site.AccountId, 0);
                }
            }
        }

        public void DeleteLoan(int id)
        {
            try
            {
                _loanRepository.DeleteLoan(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendMultipleEmails(string command, string loanIds) {
            if (!string.IsNullOrEmpty(loanIds))
            {
                foreach (string id in loanIds.Split(','))
                {
                    var loan = Get(int.Parse(id));
                    if (command == "notification")
                    {
                        Email.SendLoanNotifcation(loan);
                    }
                    else if (command == "passeddate")
                    {
                        Email.SendLoanPassedDateNotification(loan);
                    }
                }
            }
            else {
                if (command == "passeddate") {
                    foreach (var loan in GetAllPassedDateLoans()) {
                        Email.SendLoanPassedDateNotification(loan);
                    }

                }
            }
            
        }

        public List<LoanModel> GetAllPassedDateLoans() {
            return _loanRepository.GetAllPassedDateLoans()
                .Select(loan => new LoanModel()
                {
                    Id = loan.Id,
                    CustomerId = loan.CustomerId,
                    SiteId = loan.SiteId,
                    TypeId = loan.TypeId.Value,
                    StatusId = loan.StatusId.Value,
                    ItemNameId = loan.ItemNameId,
                    Description = loan.Description,
                    Notes = loan.Notes,
                    PhotoUrl = File.GetFileToString(loan.PhotoUrl),
                    MessageTypeId = loan.MessageTypeId.Value,
                    LoanedDate = loan.LoanedDate.HasValue ? loan.LoanedDate.Value : DateTime.MinValue,
                    ReturnedDate = loan.ReturnedDate,
                    LastModifiedDate = loan.LastModifiedDate.HasValue ? loan.LastModifiedDate.Value : DateTime.MinValue,
                    CreatedDate = loan.CreatedDate.HasValue ? loan.CreatedDate.Value : DateTime.MinValue,
                    IsDeleted = loan.IsDeleted.HasValue ? loan.IsDeleted.Value : false,
                    LoanType = entityToModel(_loanRepository.GetLoanType(loan.TypeId.Value)),
                    CustomerTypeId = loan.CustomerTypeId,
                    Customer = loan.CustomerTypeId == 1 ? customerService.Get(loan.CustomerId) : TenantToCustomer(_tenantRepository.Get(loan.CustomerId)),
                    Status = Enum.GetName(typeof(StatusEnum), loan.StatusId).ToString(),
                    GivenBy = loan.GivenBy,
                    ReceivedBy = !string.IsNullOrEmpty(loan.ReceivedBy) ? loan.ReceivedBy : "( )",
                    ConfirmationType = entityToModel(_commonRepository.GetConfirmationType(loan.MessageTypeId.Value)),
                    ActualReturnedDate = loan.ActualReturnedDate.HasValue ? loan.ActualReturnedDate.Value : DateTime.MinValue,
                    ConfirmedDate = loan.ConfirmedDate.HasValue ? loan.ConfirmedDate.Value : DateTime.MinValue,
                    Site = entityToModel(_siteRepository.Get(loan.SiteId)),
                    LoanId = loan.LoanId
                }).ToList();
        }

        public static LoanModel entityToModel(Loan entity) {
            return new LoanModel()
            {
            };
        }

        public static LoanTypeModel entityToModel(LoanType entity)
        {
            return new LoanTypeModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description
            };
        }

        public static MessageTypeModel entityToModel(MessageType entity) {
            return new MessageTypeModel()
            {
                Id = entity.Id,
                ConfirmationName = entity.ConfirmationName,
                Description = entity.Description,
                IsUsed = entity.IsUsed.Value
            };
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        private static string GetStatus(LoanModel loan) {
            //will implement this soon
            return string.Empty;
        }

        private static CustomerModel TenantToCustomer(Tenant tenant) {
            return new CustomerModel()
            {
                Id = tenant.Id,
                Name = tenant.Name,
                Company = tenant.Name,
                Email = tenant.Email,
                MobileNumber = tenant.Phone,
                Address1 = tenant.VisitingAddress,
                City = !string.IsNullOrEmpty(tenant.InvoiceZipAndCity) && tenant.InvoiceZipAndCity.Contains(",") ? tenant.InvoiceZipAndCity.Split(',')[1] : tenant.InvoiceZipAndCity,
                ZipCode = !string.IsNullOrEmpty(tenant.InvoiceZipAndCity) && tenant.InvoiceZipAndCity.Contains(",") ? tenant.InvoiceZipAndCity.Split(',')[0] : "",
            };
        }
    }
}
