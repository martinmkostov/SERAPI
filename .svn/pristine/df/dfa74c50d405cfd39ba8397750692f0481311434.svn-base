﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SERApp.Service.Services
{
    public class SiteService
    {
        SiteRepository _siteRepository;
        LogRepository _logRepository;
        public SiteService()
        {
            _siteRepository = new SiteRepository();
            _logRepository = new LogRepository();
        }

        public SiteModel Get(int id) {
            try
            {
                var site = _siteRepository.Get(id);
                return new SiteModel()
                {
                    Id = site.Id,
                    AccountId = site.AccountId,
                    Name = site.Name,
                    Address = site.Address,
                    City = site.City,
                    PostalCode = site.PostalCode,
                    AdditionalLocationInfo = site.AdditionalLocationInfo,
                    PhoneNumber = site.PhoneNumber,
                    FaxNumber = site.FaxNumber,
                    ContactPerson = site.ContactPerson,
                    ContactPersonNumber = site.ContactPersonNumber,
                    CreatedDate = site.CreatedDate,
                    LastUpdatedDate = site.LastUpdatedDate,
                    IsActive = site.IsActive,
                    IsDeleted = site.IsDeleted
                };
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
            
        }

        public List<SiteModel> GetByAccountId(int id)
        {
            try
            {
                var dataList = new List<Site>();
                if (id == 0)
                {
                    dataList = _siteRepository.GetAll();
                }
                else
                {
                    dataList = _siteRepository.GetAllByAccountId(id);
                }

               var data = dataList.
               Select(s => new SiteModel()
               {
                   Id = s.Id,
                   AccountId = s.AccountId,
                   //Account = entityToModel(s.Account),
                   Name = s.Name,
                   Address = s.Address,
                   City = s.City,
                   PostalCode = s.PostalCode,
                   AdditionalLocationInfo = s.AdditionalLocationInfo,
                   PhoneNumber = s.PhoneNumber,
                   FaxNumber = s.FaxNumber,
                   ContactPerson = s.ContactPerson,
                   ContactPersonNumber = s.ContactPersonNumber,
                   CreatedDate = s.CreatedDate,
                   LastUpdatedDate = s.LastUpdatedDate,
                   IsActive = s.IsActive,
                   IsDeleted = s.IsDeleted
               })
               .OrderBy(s => s.Name)
               .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
            
        }

        public List<SiteModel> GetAll() {
            try
            {
                var data = _siteRepository.GetAll().
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Account = entityToModel(s.Account),
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted
                })
                .OrderBy(s => s.Account.Name)
                .ThenBy(s => s.Name)
                .ToList();
                return data;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }
            
        }

        public List<SiteModel> GetAllByAccountId(int accountId)
        {
            try
            {
                var data = _siteRepository.GetAllByAccountId(accountId).
                Select(s => new SiteModel()
                {
                    Id = s.Id,
                    AccountId = s.AccountId,
                    Account = entityToModel(s.Account),
                    Name = s.Name,
                    Address = s.Address,
                    City = s.City,
                    PostalCode = s.PostalCode,
                    AdditionalLocationInfo = s.AdditionalLocationInfo,
                    PhoneNumber = s.PhoneNumber,
                    FaxNumber = s.FaxNumber,
                    ContactPerson = s.ContactPerson,
                    ContactPersonNumber = s.ContactPersonNumber,
                    CreatedDate = s.CreatedDate,
                    LastUpdatedDate = s.LastUpdatedDate,
                    IsActive = s.IsActive,
                    IsDeleted = s.IsDeleted
                })
                .OrderBy(s => s.Account.Name)
                .ThenBy(s => s.Name)
                .ToList();
                return data;
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                return null;
            }
        }

        public void SaveSite(SiteModel model) {
            try
            {
                _siteRepository.SaveSite(model);
                if (model.Id == 0)
                {
                    _logRepository.Log(LogTypeEnum.Information,
                     LogMessagingSettings.Save,
                     LogMessagingSettings.MessageCreateSite,
                     model.AccountId, 0);
                }
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
            }
        }

        public void DeleteSite(int id) {
            try
            {
                _siteRepository.DeleteSite(id);
                _logRepository.Log(LogTypeEnum.Information,
                     LogMessagingSettings.Delete,
                     LogMessagingSettings.MessageDeleteSite, 0, 0);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
            }
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }
    }
}
