﻿using MoreLinq;
using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class UserModuleRoleService
    {
        UserModuleRoleRepository _userRoleModuleRepository;
        ModuleRepository _moduleRepository;
        CommonService _commonService;
        public UserModuleRoleService()
        {
            _userRoleModuleRepository = new UserModuleRoleRepository();
            _commonService = new CommonService();
            _moduleRepository = new ModuleRepository();
        }

        public List<UserModuleRoleModel> GetUserModuleRolesById(int userId) {
            return _userRoleModuleRepository
                .GetUserModuleRolesById(userId)
                .Select(u => new UserModuleRoleModel() {
                    ModuleId = u.ModuleId,
                    UserId = u.UserId,
                    SiteId = u.SiteId,
                    RoleId = u.RoleId
                }).ToList();
        }

        public List<UserModel> GetUserModuleRolesBySiteId(int siteId)
        {

            var taskModule = _moduleRepository.GetAll();
            var taskModuleId = taskModule.Where(r => r.ShortName == "activity").SingleOrDefault().Id;
            var data = _userRoleModuleRepository
                .GetUserModuleRolesBySiteId(siteId)
                .Select(u => new UserModuleRoleModel()
                {
                    ModuleId = u.ModuleId,
                    UserId = u.UserId,
                    SiteId = u.SiteId,
                    RoleId = u.RoleId,
                    User = new UserModel()
                    {
                        FirstName = u.User.FirstName,
                        LastName = u.User.LastName,
                        UserName = u.User.UserName,
                        RoleName = u.Role.Name,
                        Id = u.UserId
                    }
                }).ToList();

            return data.Where(r=> r.ModuleId == taskModuleId).Select(v => v.User).DistinctBy(r=>r.Id).ToList();
        }

        public List<UserModuleRoleModel> GetUserModuleRolesBySiteIds(List<int> siteIds)
        {
            return _userRoleModuleRepository
                .GetUserModuleRolesBySiteIds(siteIds)
                .Select(u => new UserModuleRoleModel()
                {
                    ModuleId = u.ModuleId,
                    UserId = u.UserId,
                    SiteId = u.SiteId,
                    RoleId = u.RoleId,
                    User = new UserModel()
                    {
                        FirstName = u.User.FirstName,
                        LastName = u.User.LastName,
                        UserName = u.User.UserName,
                        RoleName = u.Role.Name
                    }
                }).ToList();
        }

        public void SaveUserModuleRole(UserModuleRoleModel model) {
            try {
                _userRoleModuleRepository.SaveUserModuleRole(model);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void DeleteUserModuleRoleByUserId(int userId) {
            try
            {
                _userRoleModuleRepository.DeleteUserModuleRoleByUserId(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
