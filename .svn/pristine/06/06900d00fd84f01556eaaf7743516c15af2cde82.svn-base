﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class UserController : ApiController
    {
        private UserService _userService; 
        public UserController() {
            _userService = new UserService();
        }

        [HttpGet]
        [Route("User/Get")]
        [ResponseType(typeof(ResponseDataModel<UserModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.Get(id);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("User/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetAll();
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Users",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("User/GetAllActiveUsers")]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetAllActiveUsers()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetAllActiveUsers();
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Online Users",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("User/GetByAccountId")]
        [ResponseType(typeof(ResponseDataModel<List<UserModel>>))]
        public IHttpActionResult GetByAccountId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _userService.GetByAccountId(id);
                return new ResponseDataModel<List<UserModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("User/Save")]
        public IHttpActionResult SaveUser(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userService.SaveUser(model);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved User Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("User/Delete")]
        public IHttpActionResult DeleteAccount(UserModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _userService.DeleteUser(model.Id);
                return new ResponseDataModel<UserModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted User Record"
                };
            }));
        }
    }
}
