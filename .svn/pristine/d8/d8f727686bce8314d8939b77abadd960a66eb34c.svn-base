import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../../core/interfaces';
import { routerTransition } from '../../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { ManagerService } from '../../../services';

@Component({
    selector: 'app-accounts-module-list',
    templateUrl: './accounts-module-list.component.html',
    styleUrls: ['../../layout/layout.component.scss']
})
export class AccountsModuleListComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 100;
    public modulesTotalItems: number = 100;
    public accountId: number = 0;
    public model: any;

    public hideAccountName : boolean = false;

    constructor(private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private managerService: ManagerService) {}

    packageData: Subject<any> = new Subject<any[]>();   
    dataList : Array<any> = [];
    public ngOnInit() {
        this.loadData(1);
    }
    public loadData(page?: number, id?: number, filter: string = ""): void{
        this.showSpinner = true;
        if (page != null) {
            this.currentPage = page; 
        } 
        if(id > 0)
        {
            this.hideAccountName = true;
            this.loadDataById(id);
        }
        else {
            this.managerService.getAccountsModuleList(this.currentPage, this.itemPerPage)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                this.dataList = response.Data;
                if(filter !== '')
                {
                    this.dataList = response.Data.filter((item) => {
                        return item.Account.Name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                    });
                }
                this.modulesTotalItems = response.Data[0].Modules.length;              
                this.totalItems = response.TotalRecords;
                //this.toastr.success(response.Message, "Success");
            });
        }
    }
   
    public loadDataById(id:number){
        this.showSpinner = true;
        this.managerService.getAccountsModuleListById(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.dataList = []; //make sure to clear
            this.dataList.push(response.Data);              
            this.totalItems = response.TotalRecords;
        });
    }

    public toggleModule(accountId,item){
        this.model = {
            AccountId : accountId,
            ModuleId: item.Id
        }
        if(!item.IsInAccount)
        {   
            this.addModuleToAccount();
        }
        else {
            this.removeModuleToAccount();
        }
    }

    public addModuleToAccount(){
        this.managerService.saveAccountsModule(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public removeModuleToAccount(){
        this.managerService.deleteAccountsModule(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            //this.toastr.success(response.Message, "Success"); 
        });
    }

}
