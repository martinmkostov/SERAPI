import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class LoanService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getLoanList(page:number, pageSize: number = 10, filter: any): Observable<IPagedResults<any>>{
        let url = `Loan/GetAll`;    
        return super.getAll<any>(url);
    }

    public getLoan(id?: number): Observable<any>{
         let url = `Loan/Get?id=${id}`;
        return super.getAll<any>(url);
    }

    public getLoanByAccountId(id? : number): Observable<any>{
         let url = `Loan/GetAllByAccountId?id=${id}`;
        return super.getAll<any>(url);
    }

    public getCustomerList(): Observable<any>{
        let url = `Customer/GetAll`;    
        return super.getAll<any>(url);
    }

    public getCustomer(id?: number){
         let url = `Customer/Get?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getAllMergedCustomerList(id? : number): Observable<any>{
        let url = `Loan/GetAllMergedCustomer?id=${id}`;    
        return super.getAll<any>(url);
    }

    public getAllMergedCustomerFilterList(id? : number, keyword? : string): Observable<any>{
        let url = `Loan/GetAllMergedCustomer?id=${id}&keyword=${keyword}`;    
        return super.getAll<any>(url).map(response => response.Data);
    }

    public getLoanTypes(){
        let url = `Loan/GetAllLoanTypes`;    
        return super.getAll<any>(url);
    }

    public saveLoan(data){
        let url = `Loan/Save`;
        return super.save<any>(url,data);
    }

    public deleteLoan(data){
         let url = `Loan/Delete`;
        return super.save<any>(url,data);
    }

    public confirmLoan(data){
        let url = `Loan/Confirm`;
        return super.save<any>(url,data);
    }

    public returnLoan(data){
        let url = `Loan/Return`;
        return super.save<any>(url,data);
    }
}
