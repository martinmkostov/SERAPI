﻿using GleamTech.FileSystems.AzureBlob;
using GleamTech.FileUltimate;
using SER.FileManager.Models;
using SER.FileManager.Presenter;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SER.FileManager
{
    public partial class FileManager : System.Web.UI.Page
    {
        private string userId;
        private string hashKey;
        private string moduleId;

        private FileManagerPresenter _fileManagerPresenter;
        private List<UserModuleAndRoleModel> _moduleRoles;

        private Dictionary<int, string> roles;
        private Dictionary<int, string> sites;

        public string RoleName;

        public FileManager()
        {
            _fileManagerPresenter = new FileManagerPresenter();

            this.roles = new Dictionary<int, string>();
            //this.roles.Add(-1, "Hide");

            this.sites = new Dictionary<int, string>();

            foreach (var role in _fileManagerPresenter.GetRoles())
            {
                this.roles.Add(role.Key, role.Value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.Headers["Referer"] == null)
            {
                Response.Redirect("~/unauthorized.aspx");
                return;
            }

            if (this.Request.Params["token"] == null)
            {
                Response.Redirect("~/unauthorized.aspx");
            }

            var token = this.Request.Params["token"];
            var bytes = Convert.FromBase64String(token);
            var refferer = Encoding.UTF8.GetString(bytes);

            if (!this.Request.Headers["Referer"].Contains(refferer))
            {
                Response.Redirect("~/unauthorized.aspx");
            }

            this.userId = Request.QueryString["userId"];
            this.hashKey = Request.QueryString["hashKey"];
            this.moduleId = Request.QueryString["moduleId"];

            this.ManageFile();
        }

        protected void userRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var user = e.Item.DataItem as UserModel;
                var row = e.Item.FindControl("rowid") as HtmlGenericControl;
                row.Attributes["belongs-to-site"] = user.SiteId.ToString();
                row.Attributes["user-id"] = user.Id.ToString();

                var nameControl = e.Item.FindControl("UserNameLit") as Literal;
                nameControl.Text = $"{user.FirstName} {user.LastName}";

                var roleDropDown = e.Item.FindControl("roleDropdown") as DropDownList;
                roleDropDown.DataSource = this.roles;
                roleDropDown.DataValueField = "Key";
                roleDropDown.DataTextField = "Value";
                roleDropDown.DataBind();
                roleDropDown.SelectedValue = "-1";
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static List<SiteFolderRole> FolderSetting(int siteId, string folder)
        {
            var repo = new SiteRepository();
            var folderRoles = repo.FindSiteFolderRole(siteId, folder);

            return folderRoles;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static bool SaveFolderSetting(int siteId, string folder, int roleId, int userId)
        {
            var repo = new SiteRepository();
            repo.CreateSiteFolderRole(new SiteFolderRole
            {
                SiteId = siteId,
                Folder = folder,
                RoleId = roleId,
                UserId = userId
            });

            return true;
        }

        protected void fileManager_Renamed(object sender, FileManagerRenamedEventArgs evt)
        {
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);

            var oldPath = Path.Combine(evt.Folder.Path, evt.ItemName.Replace("\\", ""));
            var newPath = Path.Combine(evt.Folder.Path, evt.ItemNewName.Replace("\\", ""));

            var rights = this._fileManagerPresenter.SiteFolderRole(site.Key, oldPath);

            foreach (var right in rights)
            {
                right.Folder = right.Folder.Replace(oldPath, newPath);

                this._fileManagerPresenter.UpdateSiteFolderRole(right);
            }
        }

        protected void fileManager_Renaming(object sender, FileManagerRenamingEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));

            if (!user.IsAdmin)
            {
                var root = evt.Folder.RootName;
                var site = this.sites.FirstOrDefault(e => e.Value == root);
                var path = Path.Combine(evt.Folder.Path, evt.ItemName.Replace("\\", ""));

                var rights = this._fileManagerPresenter.SiteFolderRole(site.Key, path, user.Id);

                if (rights != null)
                {
                    if (rights.RoleName.ToLower() == "viewer")
                    {
                        evt.Cancel("You don't have the rights to rename!");
                    }
                }
                else
                {
                    evt.Cancel("You don't have the rights to rename!");
                }
            }
        }

        protected void fileManager_Created(object sender, FileManagerCreatedEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);
            var path = Path.Combine(evt.Folder.Path, evt.ItemName.Replace("\\", ""));

            if (!user.IsAdmin)
            {

                SaveFolderSetting(site.Key, path, 1, int.Parse(this.userId));
            }
            else
            {
                var users = this._fileManagerPresenter.GetUsersForModule(int.Parse(this.moduleId));

                foreach (var usr in users)
                {
                    SaveFolderSetting(site.Key, path, 6, usr.Id);
                }
            }
        }

        protected void fileManager_Uploaded(object sender, FileManagerUploadedEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);

            if (!user.IsAdmin)
            {
                foreach (var item in evt.Items)
                {
                    var path = Path.Combine(evt.Folder.Path, item.ReceivedName.Replace("\\", ""));

                    SaveFolderSetting(site.Key, path, 1, user.Id);
                }
            }
            else
            {
                var users = this._fileManagerPresenter.GetUsersForModule(int.Parse(this.moduleId));

                foreach (var usr in users)
                {
                    foreach (var item in evt.Items)
                    {
                        var path = Path.Combine(evt.Folder.Path, item.ReceivedName.Replace("\\", ""));

                        SaveFolderSetting(site.Key, path, 6, usr.Id);
                    }
                }
            }
        }

        protected void fileManager_Deleted(object sender, FileManagerDeletedEventArgs evt)
        {
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);

            foreach (var itemName in evt.ItemNames)
            {
                var path = Path.Combine(evt.Folder.Path, itemName.Replace("\\", ""));

                this._fileManagerPresenter.DeleteSiteFolderRole(site.Key, path);
            }
        }

        protected void fileManager_Deleting(object sender, FileManagerDeletingEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));

            if (!user.IsAdmin)
            {
                var root = evt.Folder.RootName;
                var site = this.sites.FirstOrDefault(e => e.Value == root);

                foreach (var itemName in evt.ItemNames)
                {
                    var path = Path.Combine(evt.Folder.Path, itemName.Replace("\\", ""));

                    var rights = this._fileManagerPresenter.SiteFolderRole(site.Key, path, user.Id);

                    if (rights != null)
                    {
                        if (rights.RoleName.ToLower() == "viewer")
                        {
                            evt.Cancel("You don't have the rights to delete!");
                        }
                    }
                    else
                    {
                        evt.Cancel("You don't have the rights to delete!");
                    }
                }

            }
        }

        protected void fileManager_Copied(object sender, FileManagerCopiedEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);

            foreach (var item in evt.ItemNames)
            {
                var index = Array.IndexOf(evt.ItemNames, item);
                var targeItem = evt.TargetItemNames[index];

                var oldPath = Path.Combine(evt.Folder.Path, item.Replace("\\", ""));
                var targetPath = Path.Combine(evt.TargetFolder.Path, targeItem.Replace("\\", ""));
                var targetRightsPath = targetPath.Replace($@"\{targeItem.Replace("\\", "")}", "");

                foreach (var rights in this._fileManagerPresenter.SiteFolderRole(site.Key, oldPath))
                {
                    this._fileManagerPresenter.DeleteSiteFolderRole(site.Key, oldPath);
                }

                foreach (var rights in this._fileManagerPresenter.SiteFolderRole(site.Key, targetRightsPath))
                {
                    rights.Folder = targetPath;

                    this._fileManagerPresenter.CreateSiteFolderRole(rights);
                }
            }
        }

        protected void fileManager_Moved(object sender, FileManagerMovedEventArgs evt)
        {
            var user = this._fileManagerPresenter.GetUser(int.Parse(this.userId));
            var root = evt.Folder.RootName;
            var site = this.sites.FirstOrDefault(e => e.Value == root);

            foreach (var item in evt.ItemNames)
            {
                var oldPath = Path.Combine(evt.Folder.Path, item.Replace("\\", ""));
                var targetPath = Path.Combine(evt.TargetFolder.Path, item.Replace("\\", ""));
                var targetRightsPath = targetPath.Replace($@"\{item.Replace("\\", "")}", "");

                foreach (var rights in this._fileManagerPresenter.SiteFolderRole(site.Key, oldPath))
                {
                    this._fileManagerPresenter.DeleteSiteFolderRole(site.Key, oldPath);
                }

                foreach (var rights in this._fileManagerPresenter.SiteFolderRole(site.Key, targetRightsPath))
                {
                    rights.Folder = targetPath;

                    this._fileManagerPresenter.CreateSiteFolderRole(rights);
                }
            }
        }

        /// <summary>
        /// MAIN CONTROL ENTRY POINT
        /// </summary>
        private void ManageFile()
        {
            var user = this.GetUser(int.Parse(this.userId));
            if (user == null) Response.End();
            if (!_fileManagerPresenter.CanAccess(this.userId)) Response.End();
            //if (string.IsNullOrEmpty(moduleId)) Response.End();
            //if (!_fileManagerPresenter.ValidateUser(hashKey)) Response.End();

            this.RoleName = user.RoleName;

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", user.AccountId.ToString());
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;

            //gets the modules/sites/roles
            _moduleRoles = user.IsAdmin ? this.GetModuleAndRolesForAdmin(int.Parse(this.userId)) : this.GetModuleAndRoles(int.Parse(this.userId));

            //if no roles detected . exit
            if (_moduleRoles.Count == 0) return;

            //set directly to the module            //if moduleId is set

            if (!string.IsNullOrEmpty(this.moduleId) && (int.Parse(this.moduleId) > 0))
            {
                _moduleRoles = _moduleRoles.Where(m => m.ModuleId == int.Parse(this.moduleId)).ToList();
                //if no roles detected . exit
                if (_moduleRoles.Count == 0) return;
                fileManagerRoot = Path.Combine(fileManagerRoot, _moduleRoles.FirstOrDefault().ModuleName.ToUpper());
            }

            foreach (var moduleFolder in _moduleRoles)
            {
                string siteName = moduleFolder.SiteName;

                var siteFolderRoles = this._fileManagerPresenter.SiteFolderRole(moduleFolder.SiteId, int.Parse(userId)).OrderBy(e => e.Folder.Length).ToList();
                var rootFolder = default(FileManagerRootFolder);

                switch (ConfigurationManager.AppSettings["filesystem"])
                {
                    case "azureblob":
                        rootFolder = this.CreateAzureFolders(siteName, moduleFolder.RoleName);
                        break;
                    case "local":
                    default:
                        rootFolder = this.CreateFolders(fileManagerRoot, siteName, moduleFolder.RoleName);
                        break;
                }

                if (!user.IsAdmin)
                {
                    foreach (var folderRight in siteFolderRoles)
                    {
                        var accessControl = new FileManagerAccessControl
                        {
                            Path = folderRight.Folder,
                            AllowedPermissions = this.GetPermission(folderRight.RoleName)
                        };

                        if (folderRight.RoleName == "hidden")
                        {
                            accessControl.DeniedPermissions = FileManagerPermissions.ListFiles | FileManagerPermissions.ListSubfolders;
                        }

                        rootFolder.AccessControls.Add(accessControl);
                    }
                }

                this.sites.Add(moduleFolder.SiteId, rootFolder.Name);
            }

            // JS Controls
            var jsValue = this.sites.Select(d => $"['{d.Value}', {d.Key}]").Aggregate((c, n) => c + "," + n);
            var jsVariable = $"var Sites = [{jsValue}]";

            ClientScript.RegisterStartupScript(Page.GetType(), "TestArrayScript", jsVariable, true);

            setalldropdown.DataSource = this.roles;
            setalldropdown.DataValueField = "Key";
            setalldropdown.DataTextField = "Value";
            setalldropdown.DataBind();

            var users = this._fileManagerPresenter.GetUsersForModule(int.Parse(this.moduleId));

            userRepeater.DataSource = users;
            userRepeater.DataBind();
        }

        /// <summary>
        /// DEPRECATED
        /// </summary>
        /// <param name="rootName"></param>
        /// <param name="userId"></param>
        //private void ImplementFileManagement(string rootName, int userId = 84)
        //{
        //    string folderName = Path.GetFileName(rootName);
        //    GleamTech.FileUltimate.FileManagerRootFolder rootFolder = new GleamTech.FileUltimate.FileManagerRootFolder();
        //    rootFolder.Location.Path = $@"{rootName}\";
        //    rootFolder.Name = folderName;

        //    var parentRights = new GleamTech.FileUltimate.FileManagerAccessControl();
        //    parentRights.Path = $@"\";
        //    parentRights.AllowedPermissions = GleamTech.FileUltimate.FileManagerPermissions.ListSubfolders;
        //    rootFolder.AccessControls.Add(parentRights);

        //    var moduleAndRoles = this.GetModuleAndRoles(userId);
        //    foreach (var subFolder in _fileManagerPresenter.GetAvailableDirectories(rootName))
        //    {
        //        string subFolderName = Path.GetFileName(subFolder);
        //        var subRights = new GleamTech.FileUltimate.FileManagerAccessControl();

        //        subRights.Path = $@"\{ subFolderName }";

        //        if (!moduleAndRoles.Any(m => m.ModuleName.ToLower().Contains(subFolderName.ToLower())))
        //        {
        //            //anything here will be denied (hide the folder per permission)
        //            rootFolder.AccessControls.Add(subRights);
        //        }
        //        else
        //        {
        //            //else all access
        //            var moduleRight = moduleAndRoles.Where(m => m.ModuleName.ToLower().Contains(subFolderName.ToLower())).FirstOrDefault();
        //            subRights.AllowedPermissions = this.GetPermission(moduleRight.RoleName);
        //            rootFolder.AccessControls.Add(subRights);
        //        }
        //    }

        //    fileManager.RootFolders.Add(rootFolder);
        //}
        /// <summary>
        /// DEPRECATED
        /// IMPLEMENTS ROOT FOLDER for GLEAM
        /// </summary>
        /// <param name="rootName"></param>
        /// <returns></returns>
        //private FileManagerRootFolder RootFolderManage(string rootName) {
        //    string folderName = Path.GetFileName(rootName);
        //    GleamTech.FileUltimate.FileManagerRootFolder rootFolder = new GleamTech.FileUltimate.FileManagerRootFolder();
        //    rootFolder.Location.Path = $@"{rootName}\";
        //    rootFolder.Name = folderName;

        //    var parentRights = new GleamTech.FileUltimate.FileManagerAccessControl();
        //    parentRights.Path = $@"\";
        //    parentRights.AllowedPermissions = GleamTech.FileUltimate.FileManagerPermissions.ListSubfolders;
        //    rootFolder.AccessControls.Add(parentRights);

        //    return rootFolder;
        //}
        /// <summary>
        /// DEPRECATED
        /// IMPLEMENTS SUB FOLDERS for GLEAM
        /// </summary>
        /// <param name="subFolder"></param>
        /// <param name="moduleRight"></param>
        /// <param name="rootFolder"></param>
        //private void SubFolderManage(string parentFolder, string subFolder, string moduleRight, FileManagerRootFolder rootFolder) {
        //    string subFolderName = Path.GetFileName(subFolder);
        //    string path = $@"\{ (subFolderName) }";
        //    var subRights = new GleamTech.FileUltimate.FileManagerAccessControl();

        //    subRights.Path = path;

        //    if (!_moduleRoles.Any(m => m.SiteName.ToLower().Contains(subFolderName.ToLower())))
        //    {
        //        //anything here will be denied (hide the folder per permission)
        //        rootFolder.Name = subFolderName + $" ({ moduleRight })";
        //        rootFolder.AccessControls.Add(subRights);
        //    }
        //    else
        //    {
        //        //else all access
        //        rootFolder.Name = subFolderName + $" ({ moduleRight })";
        //        subRights.AllowedPermissions = this.GetPermission(moduleRight);
        //        rootFolder.AccessControls.Add(subRights);
        //    }
        //}

        private FileManagerRootFolder CreateFolders(string root, string name, string moduleRights)
        {
            string path = Path.Combine(root, name);

            _fileManagerPresenter.EnsurePath(path);

            FileManagerRootFolder rootFolder = new FileManagerRootFolder
            {
                Name = $"{name} ({moduleRights})"
            };

            rootFolder.Location.Path = path;

            FileManagerAccessControl rights = new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = this.GetPermission(moduleRights)
            };

            rootFolder.AccessControls.Add(rights);
            fileManager.RootFolders.Add(rootFolder);

            return rootFolder;
        }

        private FileManagerRootFolder CreateAzureFolders(string name, string moduleRights)
        {
            FileManagerRootFolder rootFolder = new FileManagerRootFolder
            {
                Name = $"{name} ({moduleRights})",
                Location = new AzureBlobLocation
                {
                    Path = @"\",
                    Container = ConfigurationManager.AppSettings["azureStorageContainer"],
                    AccountName = ConfigurationManager.AppSettings["azureStorageAccountName"],
                    AccountKey = ConfigurationManager.AppSettings["azureStorageAccountKey"]
                }
            };

            FileManagerAccessControl rights = new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = this.GetPermission(moduleRights)
            };

            rootFolder.AccessControls.Add(rights);
            fileManager.RootFolders.Add(rootFolder);

            return rootFolder;
        }

        private UserModel GetUser(int userId)
        {
            return _fileManagerPresenter.GetUser(userId);
        }

        private List<UserModuleAndRoleModel> GetModuleAndRoles(int userId)
        {
            return _fileManagerPresenter.GetModuleAndRoles(userId);
        }

        private List<UserModuleAndRoleModel> GetModuleAndRolesForAdmin(int userId)
        {
            return _fileManagerPresenter.GetModuleAndRolesForAdmin(userId);
        }

        private List<ModuleModel> GetModules()
        {
            return _fileManagerPresenter.GetModules();
        }

        private FileManagerPermissions GetPermission(string permission)
        {
            switch (permission)
            {
                case "admin":
                case "superadmin":
                    return FileManagerPermissions.Full;
                case "editor":
                    return
                        FileManagerPermissions.Create | FileManagerPermissions.Download | FileManagerPermissions.Copy | FileManagerPermissions.ListFiles | FileManagerPermissions.Print | FileManagerPermissions.Preview | FileManagerPermissions.ListSubfolders | FileManagerPermissions.Upload | FileManagerPermissions.Rename | FileManagerPermissions.Paste;
                default:
                    return FileManagerPermissions.ReadOnly;
            }
        }
        
        protected void fileManager_Listed(object sender, FileManagerListedEventArgs e)
        {

        }

        private List<string> DirSearch(string root)
        {
            List<string> paths = new List<String>();

            foreach (string file in Directory.GetFiles(root))
            {
                paths.Add(file);
            }

            foreach (string folder in Directory.GetDirectories(root))
            {
                paths.Add(folder);
                paths.AddRange(DirSearch(folder));
            }

            return paths;
        }
    }
}