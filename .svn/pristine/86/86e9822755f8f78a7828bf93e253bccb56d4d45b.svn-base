﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SERApp.Repository.Repositories
{
   
    public class AccountRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public AccountRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Account Get(int id) {
            return db.Accounts
                .Include("Users")
                .Include("Sites")
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public Account Get(string accountName)
        {
            return db.Accounts
                .Include("Users")
                .Include("Sites")
                .Where(c => c.Name == accountName || c.Company == accountName)
                .SingleOrDefault();
        }

        public List<Account> GetAll()
        {
            return db.Accounts
                .Include("Users")
                .Include("Sites")
                .Where(a => a.IsActive == true && a.IsDeleted == false)
                .ToList();
        }

        public List<Account> GetAllActive()
        {
            return db.Accounts
                .Include("Users")
                .Include("Sites")
                .Where(a => a.IsDeleted == false || a.IsDeleted == null)
                .Where(a => a.IsActive == true || a.IsActive == null).ToList();
        }

        public AccountModel SaveAccount(AccountModel model) {
            var account = db.Accounts.Where(l => l.Id == model.Id).SingleOrDefault();
            if (account != null)
            {
                account.Name = model.Name;
                account.Company = model.Company;
                account.EmailAddress = model.EmailAddress;
                account.IsActive = model.IsActive;
                account.IsDeleted = model.IsDeleted;
                account.LastUpdatedDate = DateTime.Now;
                account.ContactName = model.ContactName;
                account.ContactNumber = model.ContactNumber;
                account.CompanyType = model.CompanyType;
                db.SaveChanges();
            }
            else {
                Account newAccount = new Account()
                {
                    Name = model.Name,
                    Company = model.Company,
                    EmailAddress = model.EmailAddress,
                    IsDeleted = false,
                    IsActive = true,
                    LastUpdatedDate = DateTime.Now,
                    ContactName = model.ContactName,
                    ContactNumber = model.ContactNumber,
                    CompanyType = model.CompanyType
                };
                db.Accounts.Add(newAccount);
                db.SaveChanges();
                model.Id = newAccount.Id;
            }
            return model;
            
        }

        public void DeleteAccount(int id) {
            var account = db.Accounts.Where(l => l.Id == id).SingleOrDefault();
            if (account != null)
            {
                account.IsDeleted = true;
                account.LastUpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        #region STANDARD QUERIES
        public UserModel GetAccountAdmin(int accountid)
        {
            string query = $@" SELECT u.* from Users u
                  INNER JOIN UserRoles ur ON u.id = ur.UserId
                  INNER JOIN Roles r ON ur.RoleId = r.Id
                  WHERE ur.RoleId = 1 AND  u.AccountId = {accountid}";

            return db.Database.SqlQuery<UserModel>(query).FirstOrDefault();
        }
        #endregion
    }
}
