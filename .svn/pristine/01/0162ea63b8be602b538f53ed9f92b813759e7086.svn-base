﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class CommonService
    {
        CommonRepository _commonRepository;
        AccountRepository _accountRepository;
        ModuleRepository _moduleRepository;
        public CommonService() {
            _commonRepository = new CommonRepository();
            _accountRepository = new AccountRepository();
            _moduleRepository = new ModuleRepository();
        }

        public List<DropdownModel> GetAllFacilites() {
            return _commonRepository.GetAllFacilites().
                Select(f => new DropdownModel()
                {
                    Id = f.Id,
                    Name = f.FacilityName
                }).ToList();
        }

        public List<DropdownModel> GetAllLoanTypes()
        {
            return _commonRepository.GetAllLoanTypes().
                Select(l => new DropdownModel()
                {
                    Id = l.Id,
                    Name = l.Name
                }).ToList();
        }

        public List<DropdownModel> GetAllConfirmationTypes()
        {
            return _commonRepository.GetAllConfirmationTypes().
                Select(c => new DropdownModel()
                {
                    Id = c.Id,
                    Name = c.ConfirmationName
                }).ToList();
        }

        public List<DropdownModel> GetAllUserRoles()
        {
            return _commonRepository.GetAllUserRoles().
                Select(c => new DropdownModel()
                {
                    Id = c.Id,
                    Name = c.Description + " (" + c.Name + ")"
                }).ToList();
        }

        public List<DropdownModel> GetAllModuleRoles()
        {
            return _commonRepository.GetAllModuleRoles().
                Select(c => new DropdownModel()
                {
                    Id = c.Id,
                    Name = c.Name
                }).ToList();
        }

        public List<DropdownModel> GetAllCompanyTypes()
        {
            return _commonRepository.GetAllCompanyTypes()
                .Select(ct => new DropdownModel()
                {
                    Id = ct.Id,
                    Name = ct.Name
                })
                .ToList();
        }

        public List<DropdownModel> GetAllLanguages() {
            return _commonRepository.GetAllActiveLanguages()
                .Select(l => new DropdownModel()
                {
                    Id = l.Id,
                    Name = l.Name
                })
                .ToList();
        }

        public List<DropdownModel> GetAllActiveLanguages()
        {
            return _commonRepository.GetAllActiveLanguages()
                .Select(l => new DropdownModel()
                {
                    Id = l.Id,
                    Name = l.Name
                })
                .ToList();
        }

        public List<DropdownModel> GetAllActiveModules()
        {
            return _moduleRepository.GetAllActive()
                .Select(l => new DropdownModel()
                {
                    Id = l.Id,
                    Name = l.Title
                })
                .ToList();
        }

        public List<DropdownModel> GetAllTenantTypes(int accountId) {
            return _commonRepository.GetTenantTypes()
                .Where(c => c.AccountId == 0 || c.AccountId == accountId)
                .Select(t => new DropdownModel() {
                    Id = t.Id,
                    Name = t.TypeName
                })
                .ToList();
        }

        public List<DropdownModel> GetAllPremiseTypes(int accountId) {
            return _commonRepository.GetPremiseTypes()
                .Where(c => c.AccountId == 0 || c.AccountId == accountId)
                .Select(t => new DropdownModel()
                {
                    Id = t.Id,
                    Name = t.TypeName
                })
                .ToList();
        }

        public List<DropdownModel> GetAllGetJobTitles(int accountId)
        {
            return _commonRepository.GetJobTitles()
                .Where(c => c.AccountId == 0 || c.AccountId == accountId)
                .Select(t => new DropdownModel()
                {
                    Id = t.Id,
                    Name = t.TitleName
                })
                .ToList();
        }

        public List<ConfigSettingModel> GetAllConfigSettings() {
            return _commonRepository.GetAllConfigSettings().Select(s => new ConfigSettingModel() {
                Id = s.Id,
                SettingName = s.SettingName,
                SettingLabel = s.SettingLabel,
                DefaultValue = s.DefaultValue,
                InputType = s.InputType,
                ModuleId = s.ModuleId
            }).ToList();
        }

        #region DASHBOARD/COUNTERS

        public DashboardModel GetDashboardCounters() {
            return new DashboardModel()
            {
                AccountsCount = _commonRepository.AccountsCount(),
                TotalUsersCount = _commonRepository.TotalUsersCount(),
                ActiveUsersCount = _commonRepository.ActiveUsersCount(),
                InActiveUsersCount = _commonRepository.InActiveUsersCount(),
                SitesCount = _commonRepository.SitesCount(),
                AccountUserCounter = _accountRepository.GetAll()
                                    .Select(a => new AccountsUserCounterModel() {
                                        AccountName = a.Name,
                                        TotalUserCount = _commonRepository.GetUserCountByAccountId(a.Id)
                                    }).ToList()
            };
        }

        public DashboardLogModel GetDashboardLogCounters() {
            return new DashboardLogModel()
            {
                InformationCount = _commonRepository.LogInfoCount(),
                WarningCount = _commonRepository.LogWarningCount (),
                ErrorCount = _commonRepository.LogErrorCount(),
                NotifificationCount = _commonRepository.LogNotificationCount()
            };
        }

        public List<LogModel> GetMostRecentLogs() {
            return _commonRepository.GetMostRecentLogs().Select(l => new LogModel() {
                Id = l.Id,
                Account = l.AccountId > 0 ? entityToModel(_accountRepository.Get(l.AccountId)) : null,
                AccountId = l.AccountId,
                ShortDescription = l.ShortDescription,
                Message = l.Message,
                UserId = l.UserId,
                CreatedDate = l.CreatedDate,
            }).ToList();
        }

        public List<LogModel> GetErrorLogs()
        {
            return _commonRepository.GetErrorLogs().Select(l => new LogModel()
            {
                Id = l.Id,
                Account = l.AccountId > 0 ? entityToModel(_accountRepository.Get(l.AccountId)) : null,
                AccountId = l.AccountId,
                ShortDescription = l.ShortDescription,
                Message = l.Message,
                UserId = l.UserId,
                CreatedDate = l.CreatedDate,
            }).ToList();
        }

        #endregion


        #region ENTITYTOMODEL
        public UserModuleRoleModel entityToModel(UserModuleRoles entity)
        {
            return new UserModuleRoleModel()
            {
                ModuleId = entity.ModuleId,
                UserId = entity.UserId,
                SiteId = entity.SiteId,
                CreatedDate = entity.CreatedDate
            };
        }

        public RoleModel entityToModel(Role entity)
        {
            return new RoleModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                IsModule = entity.IsModule,
                IsUser = entity.IsUser
            };
        }

        public AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        public SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }
        #endregion

    }
}
