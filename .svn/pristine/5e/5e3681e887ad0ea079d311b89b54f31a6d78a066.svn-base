﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ISettingService {

        IEnumerable<SettingModel> GetSettings(int accountId);
        SettingModel GetValue(string setting, int accountId);
        SettingModel GetValueById(int id);
        void Save(SettingModel model);
    }
    public class SettingService : ISettingService
    {
        private List<LabelSettings> CustomizableLabel = new List<LabelSettings>()
        {
            new LabelSettings()
            {
                Name = "Type1",
                Value = "Type 1"
            },
            new LabelSettings()
            {
                Name = "Type2",
                Value = "Type 2"
            },
            new LabelSettings()
            {
                Name = "Type3",
                Value = "Type 3"
            },
        };

        private IRepository<Setting> _repository;
        public SettingService()
        {
            _repository = new Repository<Setting>();
        }

        public IEnumerable<SettingModel> GetSettings(int accountId)
        {
            var data = _repository.GetAll().Where(x => x.AccountId == accountId).ToList();

            CustomizableLabel.ForEach(r => 
            {
                if(!data.Any(v=>v.Name.ToLower() == r.Name.ToLower()))
                {
                    _repository.Save(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value                       
                    });

                    data.Add(new Setting()
                    {
                        AccountId = accountId,
                        Name = r.Name,
                        Value = r.Value
                    });
                }
            });

            return data.Select(r=> new SettingModel()
            { 
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value
            }).ToList();
        }

        public SettingModel GetValue(string setting, int accountId)
        {
            return _repository.GetAll().Where(x => x.AccountId == accountId && x.Name.ToLower() == setting.ToLower())
                .Select(r => new SettingModel()
                {
                    AccountId = r.AccountId,
                    Name = r.Name,
                    SettingId = r.SettingId,
                    Value = r.Value
                })
                .SingleOrDefault();
        }

        public SettingModel GetValueById(int id)
        {
            var r = _repository.Get(id);

            return new SettingModel()
            {
                AccountId = r.AccountId,
                Name = r.Name,
                SettingId = r.SettingId,
                Value = r.Value
            };
        }

        public void Save(SettingModel model)
        {
            _repository.Update(new Setting()
            {                
                Value = model.Value,
                SettingId = model.SettingId,
                AccountId = model.AccountId,
                Name = model.Name
            });
        }
    }
}
