﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System;
namespace SERApp.Repository.Repositories
{
    public class UserRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public UserRepository() {
            db = new SERAppDBContext(connString);
        }

        public User Get(int id)
        {
            return db.Users
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public User Get(string userName)
        {
            return db.Users.Where(u => u.UserName == userName).SingleOrDefault();
        }

        public List<User> GetAllByAccountId(int id)
        {
            return db.Users.Where(s => s.AccountId == id).ToList();
        }

        public List<User> GetAll()
        {
            return db.Users
                .ToList();
        }

        public List<User> GetAllActiveUsers()
        {
            var data = (from u in db.Users
                        join l in db.Logs on u.Id equals l.UserId
                        where l.ShortDescription == "LOGIN"
                        select u
                        ).ToList();
            return data;
        }

        public List<User> GetAllActive()
        {
            return db.Users
                .Where(a => a.IsDeleted == false || a.IsDeleted == null)
                .Where(a => a.IsActive == true || a.IsActive == null).ToList();
        }

        public UserModel SaveUser(UserModel model) {
            var user = db.Users.Where(l => l.Id == model.Id).SingleOrDefault();
            if (user != null)
            {
                user.AccountId = model.AccountId;
                user.Mobile = model.Mobile;
                user.EmailAddress = model.EmailAddress;
                user.PhoneNumber = model.PhoneNumber;
                user.UserName = model.UserName;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.IsActive = model.IsActive;
                user.DepartmentId = model.DepartmentId;
                user.LastUpdatedDate = DateTime.Now;
                if (!string.IsNullOrEmpty(model.RandomSecret) && !string.IsNullOrEmpty(model.RandomSecret))
                {
                    user.RandomSecret = model.RandomSecret;
                    user.HashedPassword = model.HashedPassword;
                }
                
                #region removing role
                //uncomment this soon if a user can change role
                //var userRole = db.UserRoles.Where(ur => ur.UserId == model.Id).SingleOrDefault();
                //if (userRole != null) {
                //    //remove first before adding
                //    db.UserRoles.Remove(userRole);
                //    db.SaveChanges();

                //    UserRole newUserRole = new UserRole()
                //    {
                //        RoleId = model.RoleId,
                //        UserId = model.Id
                //    };
                //    db.UserRoles.Add(newUserRole);

                //}
                #endregion

            }
            else {
                CreateNewUser(model);
            }
            db.SaveChanges();
            return model;
        }

        public User CreateNewUser(UserModel model) {
            User newUser = new User()
            {
                AccountId = model.AccountId,
                Mobile = model.Mobile,
                PhoneNumber = model.PhoneNumber,
                UserName = model.UserName,
                RandomSecret = model.RandomSecret,
                HashedPassword = model.HashedPassword,
                FirstName = model.FirstName,
                EmailAddress = model.EmailAddress,
                IsActive = true,
                IsDeleted = false,
                LastName = model.LastName,
                LastUpdatedDate = DateTime.Now,
                CreatedDate = DateTime.Now
            };
            db.Users.Add(newUser);

            UserRole newUserRole = new UserRole()
            {
                RoleId = model.RoleId,
                UserId = model.Id
            };
            db.UserRoles.Add(newUserRole);

            //CLEAR THESE BEFORE PASSING TO THE API
            model.RandomSecret = "";
            model.HashedPassword = "";
            return newUser;
        }

        public void DeleteUser(int id) {
            var user = db.Users.Where(l => l.Id == id).SingleOrDefault();
            if (user != null)
            {
                user.IsDeleted = true;
                user.LastUpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
        }
    }
}
