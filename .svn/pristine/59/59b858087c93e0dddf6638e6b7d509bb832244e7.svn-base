﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueWebService
    {
        IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId =0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null);
        void SaveIssueWeb(IssueWebModel model, bool isConfirm = false);
        void SaveIssueWebLog(IssueWebHistoryModel model);
        void SendDetailsModel(SendDetailsModel model);
        void CloseIssue(IssueWebHistoryModel model);
        void DeleteIssueWeb(int id);
        IssueWebModel GetById(int id);
        void sendReminder(IssueWebModel model);
        SettingModel GetIssueSettingByAccountId(int accountId, string settingName, string defaultValue = "1");
        IEnumerable<IssueWebHistoryModel> GetIssueWebLog(int issueWebId, bool showHidden = false);
    }

    public class IssueWebService : IIssueWebService
    {
        private int defaulSettingtInterval = 1;

        private readonly IIssueWebRepository _repository;
        private readonly AccountService _accountService;
        private readonly IIssueRecipientRepository _recipientRepository;
        private readonly EmailRepository _emailRepository;
        private readonly IIssueSettingsRepository _settingsRepository;
        private readonly IRepository<Issue_IssueType3> _issueIssueType3Repository;
        private readonly IRepository<Setting> _settingRepository;
        private readonly IRepository<ConfigSetting> _configSettingRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        public IssueWebService()
        {
            _repository = new IssueWebRepository();
            _accountService = new AccountService();
            _recipientRepository = new IssueRecipientRepository();
            _emailRepository = new EmailRepository();
            _settingsRepository = new IssueSettingsRepository();
            _issueIssueType3Repository = new Repository<Issue_IssueType3>();
            _settingRepository = new Repository<Setting>();
            _configSettingRepository = new Repository<ConfigSetting>();
            _tenantRepository = new Repository<Tenant>();
        }

        public SettingModel GetIssueSettingByAccountId(int accountId, string settingName, string defaultValue = "1")
        {
            var configSetting = _configSettingRepository.GetByPredicate(x => x.SettingName == settingName).SingleOrDefault();

            var data = _settingRepository.GetByPredicate(x=>x.AccountId == accountId && x.ConfigSettingId == configSetting.Id).SingleOrDefault();

            if (data == null)
            {
                //_settingsRepository.Save(new IssueSettings()
                //{
                //    AccountId = accountId,
                //    StatusInterval = defaulSettingtInterval
                //});

                //data = _settingsRepository.GetByAccountId(accountId);

                _settingRepository.Save(new Setting()
                {
                    Name = settingName,
                    Value = configSetting.DefaultValue,
                    ConfigSettingId = configSetting.Id,
                    AccountId = accountId,                    
                });

                data = _settingRepository.GetByPredicate(x => x.AccountId == accountId && x.ConfigSettingId == configSetting.Id).SingleOrDefault();

            }

            var completeData = data;
            return new SettingModel()
            {
                AccountId = completeData.AccountId,
                ConfigSettingId = completeData.ConfigSettingId,
                SettingId = completeData.SettingId,
                Name = completeData.Name,
                Value = completeData.Value
            };
        }

        public void DeleteIssueWeb(int id)
        {
            _repository.Delete(id);
        }

        public IssueWebModel GetById(int id)
        {
            var data = _repository.GetInlcudingChildrenId(id);

            data.IssueWebHistories = _repository.GetIssueWebHistoryByIssueWebId(data.IssueWebId).OrderByDescending(x=>x.IssueWebHistoryId).ToList();
            
            return new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                ReminderEmailSentCount = data.ReminderEmailSentCount,
                IssueType3 = data.Issue_IssueType3.Select(t=> new Issue_IssueType3Model()
                {
                    Id = t.Id,
                    IssueType3Id = t.IssueType3Id,
                    IssueWebId = t.IssueWebId,
                    Value = t.Value
                }).ToList(),
                IssueWebHistories = data.IssueWebHistories != null || data.IssueWebHistories.Any() ? data.IssueWebHistories.Select(x => new IssueWebHistoryModel()
                {
                    IssueWebHistoryId = x.IssueWebHistoryId,
                    Date = new SERApp.Models.Common.DateModel() {
                        day = x.Date.Day,
                        month = x.Date.Month,
                        year = x.Date.Year,
                    },
                    Time = new SERApp.Models.Common.TimeModel() {
                        hour = x.Date.Hour,
                        minute = x.Date.Minute,
                        second = x.Date.Second
                    },
                    DateTime = x.Date,
                    //Description = x.Description,
                    DescriptionExternal = x.DescriptionExternal,
                    DescriptionInternal = x.DescriptionInternal,
                    IsDamage = x.IsDamage,
                    IsRent = x.IsRent,
                    IsWarranty = x.IsWarranty,
                    MaterialCost = x.MaterialCost,
                    Minutes = x.Minutes,
                    ReactedBy =x.ReactedBy,
                    IssueWebId = x.IssueWebId,
                    IssueWebHistoryType = x.IssueWebHistoryType
                }).ToList() : new List<IssueWebHistoryModel>(),
                
            };
        }

        public IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId= 0, int statusId = -1, int contactId = 0, int issueType1Id = 0, int issueType2Id = 0, int issueType3Id = 0, string searchText = "", DateModel fromDate = null, DateModel toDate = null)
        {
            var dataList = _repository.GetAllIncludingChildren(accountId);

            if (fromDate != null && toDate != null)
            {
                var fDate = new DateTime(fromDate.year, fromDate.month, fromDate.day);
                var tDate = new DateTime(toDate.year, toDate.month, toDate.day);

                dataList = dataList.Where(x => x.DateCreated >= fDate && x.DateCreated <= tDate).ToList();
            }

            if (siteId != 0)
            {
                dataList = dataList.Where(x => x.SiteId == siteId).ToList();
            }

            if (statusId != -1)
            {
                dataList = dataList.Where(x => x.Status == statusId).ToList();
            }

            if (contactId != 0)
            {
                dataList = dataList.Where(x => x.TenantId == contactId).ToList();
            }


            if (issueType1Id != 0)
            {
                dataList = dataList.Where(x => x.IssueType1Id == issueType1Id).ToList();
            }
            if (issueType2Id != 0)
            {
                dataList = dataList.Where(x => x.IssueType2Id == issueType2Id).ToList();
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                dataList = dataList.Where(x => x.Email.ToLower().Contains(searchText) || x.ContactName.ToLower().Contains(searchText)).ToList();
            }
            
            var toCheck = dataList.Where(x => x.Status == 0).ToList();
            var accountInterval = GetIssueSettingByAccountId(accountId, "IssueInterval");
            GetIssueSettingByAccountId(accountId, "SendToSupervisorLimit");

            toCheck.ForEach(x => 
            {
                var dateMax = x.DateCreated.AddHours(Convert.ToInt32(accountInterval.Value));

                var dateNow = DateTime.Now;

                if (dateMax < dateNow)
                {
                    x.Status = 1;
                    _repository.Update(x);
                }
            });

            return
                dataList.Select(data => new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                    ReminderEmailSentCount = data.ReminderEmailSentCount
                });
        }

        public void SaveIssueWeb(IssueWebModel model, bool isConfirm = false)
        {
            SMS sms = new SMS();
            var data = new IssueWeb();
            if (model.IssueWebId == 0)
            {
                data = new IssueWeb()
                {
                    //IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateCreated = DateTime.Now,
                    Status = (int)IssueStatusEnum.NotConfirmed,
                    ReminderEmailSentCount = 0,
                    Issue_IssueType3 = model.IssueType3.Select(x=> new Issue_IssueType3()
                    {
                        IssueType3Id = x.IssueType3Id,
                        Value = x.Value,                        
                    }).ToList()
                };

                _repository.Save(data);                
            }
            else {

                var existingType3s = _issueIssueType3Repository.GetByPredicate(t => t.IssueWebId == model.IssueWebId).ToList();
                foreach (var s in existingType3s)
                {
                    _issueIssueType3Repository.Delete(s.Id);
                }

                model.IssueType3.ForEach(r =>
                {
                    _issueIssueType3Repository.Save(new Issue_IssueType3()
                    {
                        IssueType3Id =r.IssueType3Id,
                        IssueWebId = model.IssueWebId,
                        Value = r.Value,
                    });
                });


                data = new IssueWeb()
                {
                    DateCreated = model.DateCreated,
                    IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateChecked = model.DateChecked,
                    DateFinished = model.DateFinished,
                    Status = (int)model.Status
                };

                _repository.Update(data);
            }
         
            data =_repository.GetInlcudingChildrenId(data.IssueWebId);

            

            if (isConfirm)
            {
                _repository.AddIssueWebHistory(new IssueWebHistory()
                {
                    Date = DateTime.Now,
                    DescriptionExternal = "Technician confirmed the issue",
                    DescriptionInternal = "Technician confirmed the issue",
                    ReactedBy = "Technician",
                    IssueWebId = data.IssueWebId,
                    //to do add this to enum , confirm = 1, reminder =2
                    IssueWebHistoryType = "warning"
                });

                var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("ConfirmIssue");

                var tenant = _tenantRepository.Get(data.TenantId);

                if (string.IsNullOrEmpty(tenant.Email))
                {
                    tenant.Email = "";
                }
               
                Service.Tools.Email.SendEmail(new EmailModel()
                {
                    From = "",
                    To = data.Email,
                    Subject = creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                    Body = creatorTemplate.Body

                });

                
                sms.SendSMS("",data.Phone,creatorTemplate.SMSBody).Wait();

            }
            else {
               
                var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                string AppUrl = ConfigurationManager.AppSettings["appurl"];

                recipients.ForEach(recipient =>
                {
                    var routineName = Regex.Replace(recipient.IssueRecipientType.Name, @"\s+", "");
                    var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                    if (!string.IsNullOrEmpty(recipient.Email))
                    {

                        Service.Tools.Email.SendEmail(new EmailModel()
                        {
                            From = "",
                            To = recipient.Email,
                            Subject = template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                            Body = template.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                            .Replace("{object}", data.Site.Name)
                            .Replace("{tenant}", data.Tenant.Name)
                            .Replace("{type1}", data.IssueType1.Name)
                            .Replace("{phone}", data.Phone)
                            .Replace("{contactname}", data.ContactName)
                            .Replace("{address}", data.Address)
                            .Replace("{email}", data.Email)
                            .Replace("{apartment}", data.ApartmentNumber)
                            .Replace("{type}", data.IssueType2.Name)
                            .Replace("{description}", data.Description)
                            .Replace("{ispolice}", data.IsPolice.ToString())
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.IssueWebId.ToString()),

                        });                    
                     
                    }

                    if (!string.IsNullOrEmpty(recipient.Mobile))
                    {
                        var smsService = new SMS();
                        smsService.SendSMS("SER4 Application", recipient.Mobile, template.SMSBody
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                    }

                });

                //send email to creator of issue

                var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("CreateIssue");
                var tenant = _tenantRepository.Get(data.TenantId);

                if (string.IsNullOrEmpty(tenant.Email))
                {
                    tenant.Email = "";
                }

                Service.Tools.Email.SendEmail(new EmailModel()
                {
                    From ="",
                    To = data.Email,
                    Subject = creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                    Body = creatorTemplate.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                           .Replace("{sender}", data.Email)
                           .Replace("{object}", data.Site.Name)
                           .Replace("{tenant}", data.Tenant.Name)
                           .Replace("{type1}", data.IssueType1.Name)
                           .Replace("{phone}", data.Phone)
                           .Replace("{contactname}", data.ContactName)
                           .Replace("{address}", data.Address)
                           .Replace("{email}", data.Email)
                           .Replace("{apartment}", data.ApartmentNumber)
                           .Replace("{type}", data.IssueType2.Name)
                           .Replace("{description}", data.Description)
                           .Replace("{ispolice}", data.IsPolice.ToString())
                           .Replace("{appurl}", AppUrl)
                           .Replace("{IssueWebId}", data.IssueWebId.ToString()),

                });

                

                if (!string.IsNullOrEmpty(data.Phone))
                {
                    var smsService = new SMS();
                    smsService.SendSMS("SER4 Application", data.Phone, creatorTemplate.SMSBody
                        .Replace("{appurl}", AppUrl)
                        .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                }
            }            
        }

        public void SaveIssueWebLog(IssueWebHistoryModel model)
        {
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = new DateTime(model.Date.year,model.Date.month,model.Date.day,model.Time.hour,model.Time.minute,model.Time.second),
                //Description = model.Description,
                DescriptionExternal = model.DescriptionExternal,
                DescriptionInternal = model.DescriptionInternal,
                ReactedBy = model.ReactedBy,
                Minutes = model.Minutes,
                MaterialCost = model.MaterialCost,
                IsWarranty = model.IsWarranty,
                IsRent = model.IsRent,
                IsDamage = model.IsDamage,
                IssueWebHistoryType = "info",
                IssueWebId = model.IssueWebId,
                IsPrivate = model.IsPrivate
            });

            if (!model.IsPrivate)
            {
                string AppUrl = ConfigurationManager.AppSettings["appurl"];
                var routineName1 = "AddIssueAction";

                var template1 = _emailRepository.GetEmailTemplateFromRoutineName(routineName1);

                var data = _repository.Get(model.IssueWebId);
                var tenant = _tenantRepository.Get(data.TenantId);


                Service.Tools.Email.SendEmail(new EmailModel()
                {
                    From = "",
                    To = data.Email,
                    Subject = template1.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                     Body = template1.Body
                    .Replace("{appurl}", AppUrl)
                    .Replace("{IssueWebId}", data.IssueWebId.ToString())

                });
               
                var sms = new SMS();
                sms.SendSMS("", data.Phone, template1.Body
                    .Replace("{appurl}", AppUrl)
                    .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();




                var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                //string AppUrl = ConfigurationManager.AppSettings["appurl"];

                recipients.ForEach(recipient =>
                {
                    //var routineName = Regex.Replace(recipient.IssueRecipientType.Name, @"\s+", "");
                    var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName1);

                    if (!string.IsNullOrEmpty(recipient.Email))
                    {

                        Service.Tools.Email.SendEmail(new EmailModel()
                        {
                            From = "",
                            To = recipient.Email,
                            Subject = template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                            Body = template1.Body
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.IssueWebId.ToString()),

                        });

                    }

                    if (!string.IsNullOrEmpty(recipient.Mobile))
                    {
                        var smsService = new SMS();
                        smsService.SendSMS("SER4 Application", recipient.Mobile, template1.Body
                    .Replace("{appurl}", AppUrl)
                    .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                    }

                });


            }
        }

        public void CloseIssue(IssueWebHistoryModel model)
        {
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                //Description = model.Description,
                DescriptionExternal = model.DescriptionExternal,
                DescriptionInternal = model.DescriptionInternal,
                ReactedBy = model.ReactedBy,
                //Minutes = model.Minutes,
                //MaterialCost = model.MaterialCost,
                //IsWarranty = model.IsWarranty,
                //IsRent = model.IsRent,
                //IsDamage = model.IsDamage,
                IssueWebHistoryType = "success",
                IssueWebId = model.IssueWebId
            });

            var issue = _repository.Get(model.IssueWebId);
            issue.Status = 3;
            _repository.Update(issue);

            string AppUrl = ConfigurationManager.AppSettings["appurl"];
            var routineName = "CloseIssue";

            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

            var data = _repository.Get(model.IssueWebId);
            var tenant = _tenantRepository.Get(data.TenantId);
            
            
            //Service.Tools.Email.SendEmailWithAttachment("", data.Email, template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
            //template.Body
            //.Replace("{appurl}", AppUrl)
            //.Replace("{description}",data.Description)
            //.Replace("{IssueWebId}", data.IssueWebId.ToString()),
            //"", data.Image, null, true, new List<string>()
            //{
            //    tenant.Email
            //});

            Service.Tools.Email.SendEmail(new EmailModel()
            {
                From = "",
                To = data.Email,
                Subject = template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                Body = template.Body
                .Replace("{appurl}", AppUrl)
                .Replace("{description}", data.Description)
                .Replace("{IssueWebId}", data.IssueWebId.ToString())

            });
        }

        public void sendReminder(IssueWebModel model)
        {
            var issue = _repository.Get(model.IssueWebId);

            var setting = GetIssueSettingByAccountId(model.AccountId, "SendToSupervisorLimit");
           // setting.Value
            var recipientType = issue.ReminderEmailSentCount >= Convert.ToInt32(setting.Value) ? 6 : 1;
            var recipients = _recipientRepository.GetAllRecipients(model.SiteId).Where(x => x.IssueRecipientTypeId == recipientType).ToList();
            string AppUrl = ConfigurationManager.AppSettings["appurl"];

            bool technicianReminderHasBeenSent = false;
            bool supervisorReminderHasBeenSent = false;

            recipients.ForEach(recipient =>
            {

                switch (recipientType)
                {
                    //reminder sent to technician
                    case 1:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmail";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
                         
                            var list = new List<string>();
                            if (!technicianReminderHasBeenSent)
                            {
                                var tenant = _tenantRepository.Get(model.TenantId);
                                list.Add(tenant.Email);
                            }
                    
                            //Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            //template.Body.Replace("{name}", recipient.Name)
                            //    .Replace("{object}", model.Site.Name)
                            //    .Replace("{tenant}", model.Tenant.Name)
                            //    .Replace("{type1}", model.IssueType1.Name)
                            //    .Replace("{phone}", model.Phone)
                            //    .Replace("{contactname}", model.ContactName)
                            //    .Replace("{address}", model.Address)
                            //    .Replace("{email}", model.Email)
                            //    .Replace("{apartment}", model.ApartmentNumber.ToString())
                            //    .Replace("{type}", model.IssueType2.Name)
                            //    .Replace("{description}", model.Description)
                            //    .Replace("{ispolice}", model.IsPolice.ToString())
                            //    .Replace("{appurl}", AppUrl)
                            //    .Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            //"", model.Image, null, true, list
                            //    );

                            Service.Tools.Email.SendEmail(new EmailModel()
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                                Body = template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber)
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString())

                            });
                        }

                        technicianReminderHasBeenSent = true;
                        break;

                    //reminder sent to supervisor
                    case 6:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmailLimitExceed";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
                           
                            var list = new List<string>();
                            if (!supervisorReminderHasBeenSent)
                            {
                                var tenant = _tenantRepository.Get(model.TenantId);
                                list.Add(tenant.Email);
                            }

                            Service.Tools.Email.SendEmail(new EmailModel()
                            {
                                From = "",
                                To = recipient.Email,
                                Subject = template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                                Body = template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber)
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString())

                            });
                            
                        }
                        break;
                }


               
            });

            issue.ReminderEmailSentCount = issue.ReminderEmailSentCount + 1;
            _repository.Update(issue);

        
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                DescriptionExternal = recipientType == 1? "A reminder has been sent by the issuer to the technician": "A reminder has been sent by the issuer to the supervisor",
                DescriptionInternal = recipientType == 1 ? "A reminder has been sent by the issuer to the technician" : "A reminder has been sent by the issuer to the supervisor",
                ReactedBy = model.ContactName + " ("+model.Email+")",
                IssueWebId = model.IssueWebId,
                IssueWebHistoryType = "warning"
            });
        }

        public IEnumerable<IssueWebHistoryModel> GetIssueWebLog(int issueWebId, bool showHidden = false)
        {

            var data = _repository.GetIssueWebHistoryByIssueWebId(issueWebId);

            if (!showHidden)
            {
               data = data.Where(x => !x.IsPrivate).ToList();
            }

            return 
            data  
            .Select(x=> new IssueWebHistoryModel()
            {
                IssueWebHistoryId = x.IssueWebHistoryId,
                Date = new SERApp.Models.Common.DateModel()
                {
                    day = x.Date.Day,
                    month = x.Date.Month,
                    year = x.Date.Year,
                },
                Time = new SERApp.Models.Common.TimeModel()
                {
                    hour = x.Date.Hour,
                    minute = x.Date.Minute,
                    second = x.Date.Second
                },
                DateTime = x.Date,
                DescriptionExternal = x.DescriptionExternal,
                DescriptionInternal = x.DescriptionInternal,
                IsDamage = x.IsDamage,
                IsRent = x.IsRent,
                IsWarranty = x.IsWarranty,
                MaterialCost = x.MaterialCost,
                Minutes = x.Minutes,
                ReactedBy = x.ReactedBy,
                IssueWebId = x.IssueWebId,
                IssueWebHistoryType = x.IssueWebHistoryType
            });
        }

        public void SendDetailsModel(SendDetailsModel model)
        {
            var body = model.additionalMessage;
            var routineName = "IssueDetails";
            string AppUrl = ConfigurationManager.AppSettings["appurl"];
            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);
            var issueWeb = _repository.Get(model.IssueWebId);
            
            //throw new NotImplementedException();
            model.Recipients.Where(x => x.IsChecked).ToList().ForEach(x => 
            {               
                var completeBody = "";
                if (!string.IsNullOrEmpty(body))
                {
                    var templ = template.Body
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString());

                    completeBody = $"{body}<br/><br><hr/>{templ}";
                }
                else {
                    completeBody = template.Body
                               .Replace("{appurl}", AppUrl)
                               .Replace("{IssueWebId}", model.IssueWebId.ToString());
                }

                var list = new List<string>();
                var tenant = _tenantRepository.Get(issueWeb.TenantId);
                list.Add(tenant.Email);

                Service.Tools.Email.SendEmailWithAttachment("", x.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            completeBody,
                            "", issueWeb.Image, null, true, list);
              
            });


            model.otherRecipients.ForEach(x => 
            {
                var completeBody = "";
                if (!string.IsNullOrEmpty(body))
                {
                    var templ = template.Body
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString());

                    completeBody = $"{body}<br/><br><hr/>{templ}";
                }
                else
                {
                    completeBody = template.Body
                               .Replace("{appurl}", AppUrl)
                               .Replace("{IssueWebId}", model.IssueWebId.ToString());
                }


                var list = new List<string>();                
                var tenant = _tenantRepository.Get(issueWeb.TenantId);
                list.Add(tenant.Email);

                Service.Tools.Email.SendEmailWithAttachment("", x.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                        completeBody,
                        "", issueWeb.Image, null, true,list);
            });
        }
    }
}
