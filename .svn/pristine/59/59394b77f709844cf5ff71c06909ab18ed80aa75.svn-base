﻿using Hangfire;
using Hangfire.Storage;
using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class ReportTypeScheduleController : ApiController
    {
        private AccountService accountService;
        private ReportTypeScheduleService reportTypeScheduleService;
        private IncidentService incidentService;
        private ModuleService moduleService;
        private DailyReportRecipientService _dailyReportRecipientService;
        public ReportTypeScheduleController()
        {
            this.accountService = new AccountService();
            this.reportTypeScheduleService = new ReportTypeScheduleService();
            this.incidentService = new IncidentService();
            this.moduleService = new ModuleService();
            this._dailyReportRecipientService = new DailyReportRecipientService();
        }

        [HttpGet]
        [Route("ReportTypeSchulde")]
        public IHttpActionResult Get()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var listOfReportTypeSchedule = reportTypeScheduleService.GetAll();
                var data = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs().Select(x=> new ReportTypeScheduleModel()
                {
                    AccountId = Convert.ToInt32(x.Id.Split('-')[0]),
                    ReportTypeId = x.Id.Split('-')[2],
                    ModuleId = Convert.ToInt32(x.Id.Split('-')[1]),
                    Cron = x.Cron,
                    Schedule = listOfReportTypeSchedule.Where(r=>r.JobId == x.Id).Any() ? listOfReportTypeSchedule.SingleOrDefault(r => r.JobId == x.Id).Schedule : "",
                    Id = x.Id
                }).ToList();

                return new ResponseDataModel<List<ReportTypeScheduleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Account Data",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("ReportTypeSchulde")]
        public IHttpActionResult Post([FromBody] ReportTypeScheduleModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                //var value = reportTypeScheduleService.Add(model);

                //if (value)
                //{
               
                    var content = (ReportTypes)Enum.Parse(typeof(ReportTypes), model.ReportTypeId);

                    var incidents = this.incidentService.GetIncidents(accountId: model.AccountId, checkForSiteId: false);

                    var currentDate = DateTime.Now;
                    var currentDateString = currentDate.ToString("yyyy-MM-dd");
                    var listofSites = incidents.Select(r => r.SiteId).Distinct().ToList();
                    switch (content)
                        {
                            case ReportTypes.DayReport:
                
                                listofSites.ForEach(x => 
                                {                                
                                                             
                                        var sendReportService = new SendReport();
                                        var jobId = $"{model.AccountId}-{model.ModuleId}-{model.ReportTypeId}-{x}";
                                        var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();
                                        var job = jobs.FirstOrDefault(e => e.Id == jobId);

                                        if (job == null)
                                        {
                                            RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(currentDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                            reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                            {
                                                Schedule = model.Schedule,
                                                JobId = jobId
                                            });
                                        }
                                        else
                                        {
                                            RecurringJob.RemoveIfExists(jobId);
                                            reportTypeScheduleService.Delete(jobId);
                                            RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(currentDate, new DateTime(), "DailyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                            reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                            {
                                                Schedule = model.Schedule,
                                                JobId = jobId
                                            });
                                        }                                                                                               
                                });                           
                       
                                break;
                            case ReportTypes.WeekReport:                       
                                var monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                                var sunday = monday.AddDays(6);                            
                                listofSites.ForEach(x =>
                                {
                                    var sendReportService = new SendReport();
                                    var jobId = $"{model.AccountId}-{model.ModuleId}-{model.ReportTypeId}-{x}";
                                    var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();
                                    var job = jobs.FirstOrDefault(e => e.Id == jobId);

                                    if (job == null)
                                    {
                                        RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(monday, sunday, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                        reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                        {
                                            Schedule = model.Schedule,
                                            JobId = jobId
                                        });
                                    }
                                    else
                                    {
                                        RecurringJob.RemoveIfExists(jobId);
                                        reportTypeScheduleService.Delete(jobId);
                                        RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(monday, sunday, "WeeklyReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                        reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                        {
                                            Schedule = model.Schedule,
                                            JobId = jobId
                                        });
                                    }
                                });

                            break;
                            case ReportTypes.MonthReport:
                                var firstDay = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
                                var lastDay = firstDay.AddDays(6);
                                listofSites.ForEach(x =>
                                {
                                    var sendReportService = new SendReport();
                                    var jobId = $"{model.AccountId}-{model.ModuleId}-{model.ReportTypeId}-{x}";
                                    var jobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();
                                    var job = jobs.FirstOrDefault(e => e.Id == jobId);

                                    if (job == null)
                                    {
                                        RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(firstDay, lastDay, "MonthReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                        reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                        {
                                            Schedule = model.Schedule,
                                            JobId = jobId
                                        });
                                    }
                                    else
                                    {
                                        RecurringJob.RemoveIfExists(jobId);
                                        reportTypeScheduleService.Delete(jobId);
                                        RecurringJob.AddOrUpdate<SendReport>(jobId, e => e.SchedulerReport(firstDay, lastDay, "MonthReport", System.Web.Hosting.HostingEnvironment.MapPath("~/dpfdump"), x, model.AccountId, _dailyReportRecipientService), model.Cron);
                                        reportTypeScheduleService.Add(new ReportTypeScheduleModel()
                                        {
                                            Schedule = model.Schedule,
                                            JobId = jobId
                                        });
                                    }
                                });
                        break;
                        }
            
                    return new ResponseDataModel<ReportTypeScheduleModel>
                    {
                        Success = true,
                        StatusCode = HttpStatusCode.OK,
                        Message = "Successfully Saved Schedule" ,
                        Data = model
                    };
            }));
        }
    }
}
