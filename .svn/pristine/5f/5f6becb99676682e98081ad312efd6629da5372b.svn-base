﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class TenantController : ApiController
    {
        private TenantService _tenantService;
        public TenantController()
        {
            _tenantService = new TenantService();
        }

        [HttpGet]
        [Route("Tenant/Get")]
        [ResponseType(typeof(ResponseDataModel<TenantModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.Get(id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded User Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAll();
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllByAccountId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllByAccountId(int accountId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllByAccountId(accountId);
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllBySiteId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantModel>>))]
        public IHttpActionResult GetAllBySiteId(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllBySiteId(siteId);
                return new ResponseDataModel<List<TenantModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenants By Account",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllTenantContactsByTenantId")]
        [ResponseType(typeof(ResponseDataModel<List<TenantContactModel>>))]
        public IHttpActionResult GetAllTenantContactsByTenantId(int tenantId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllTenantContactsByTenantId(tenantId);
                return new ResponseDataModel<List<TenantContactModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Contacts for this Tenant",
                    Data = data,
                    TotalRecords = data.Count
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveTenantContact")]
        public IHttpActionResult SaveTenantContact(TenantContactModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenantContact(model);
                return new ResponseDataModel<TenantContactModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Contact for this Tenant",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Save")]
        public IHttpActionResult SaveUser(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenant(model);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/Delete")]
        public IHttpActionResult DeleteAccount(TenantModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenant(model.Id);
                return new ResponseDataModel<TenantModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Record"
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllTenantTypes")]
        public IHttpActionResult GetAllTenantTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllTenantTypes(accountId);
                return new ResponseDataModel<List<TenantTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Tenant Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveTenantType")]
        public IHttpActionResult SaveTenantType(TenantTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveTenantType(model);
                return new ResponseDataModel<TenantTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Tenant Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SavePremiseType")]
        public IHttpActionResult SavePremiseType(PremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SavePremiseType(model);
                return new ResponseDataModel<PremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Premise Type Record",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllPremiseTypes")]
        public IHttpActionResult GetAllPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllPremiseTypes(accountId);
                return new ResponseDataModel<List<PremiseTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Premise Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllSubPremiseTypes")]
        public IHttpActionResult GetAllSubPremiseTypes(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllSubPremiseTypes(accountId);
                return new ResponseDataModel<List<SubPremiseTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Sub Premise Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveSubPremiseType")]
        public IHttpActionResult SaveSubPremiseType(SubPremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveSubPremiseType(model);
                return new ResponseDataModel<SubPremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Sub Premise Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/SaveJobTitle")]
        public IHttpActionResult SaveJobTitle(JobTitleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.SaveJobTitle(model);
                return new ResponseDataModel<JobTitleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Job Title Record",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("Tenant/GetAllGetJobTitles")]
        public IHttpActionResult GetAllGetJobTitles(int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _tenantService.GetAllGetJobTitles(accountId);
                return new ResponseDataModel<List<JobTitleModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Job Titles",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteTenantType")]
        public IHttpActionResult DeleteTenantType(TenantTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenantType(model.Id);
                return new ResponseDataModel<TenantTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeletePremiseType")]
        public IHttpActionResult DeletePremiseType(PremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeletePremiseType(model.Id);
                return new ResponseDataModel<PremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Premise Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteSubPremiseType")]
        public IHttpActionResult DeleteSubPremiseType(SubPremiseTypeModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteSubPremiseType(model.Id);
                return new ResponseDataModel<SubPremiseTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Sub Premise Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteJobTitle")]
        public IHttpActionResult DeleteJobTitle(JobTitleModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteJobTittle(model.Id);
                return new ResponseDataModel<JobTitleModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Type Record",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Tenant/DeleteTenantContact")]
        public IHttpActionResult DeleteTenantContact(TenantContactModel model)
        {

            return Ok(this.ConsistentApiHandling(() =>
            {
                _tenantService.DeleteTenantContact(model.TenantContactId);
                return new ResponseDataModel<TenantContactModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Deleted Tenant Contact Record",
                    Data = model
                };
            }));
        }
    }
}
