﻿using GleamTech.FileUltimate;
using SER.FileManager.Models;
using SER.FileManager.Presenter;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SER.FileManager
{
    public partial class FileManager : System.Web.UI.Page
    {
        private FileManagerPresenter _fileManagerPresenter;
        private List<UserModuleAndRoleModel> _moduleRoles;

        private Dictionary<int, string> roles;

        public FileManager()
        {
            _fileManagerPresenter = new FileManagerPresenter();

            this.roles = new Dictionary<int, string>();
            this.roles.Add(-1, "Not Set");
            
            foreach (var role in _fileManagerPresenter.GetRoles())
            {
                this.roles.Add(role.Key, role.Value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.Headers["Referer"] == null)
            {
                Response.Redirect("~/unauthorized.aspx");
                return;
            }

            if (this.Request.Params["token"] == null)
            {
                Response.Redirect("~/unauthorized.aspx");
            }

            var token = this.Request.Params["token"];
            var bytes = Convert.FromBase64String(token);
            var refferer = Encoding.UTF8.GetString(bytes);

            if (!this.Request.Headers["Referer"].Contains(refferer))
            {
                Response.Redirect("~/unauthorized.aspx");
            }

            this.ManageFile();
        }

        protected void userRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var user = e.Item.DataItem as UserModel;
                var row = e.Item.FindControl("rowid") as HtmlGenericControl;
                    row.Attributes["belongs-to-site"] = user.SiteId.ToString();
                    row.Attributes["user-id"] = user.Id.ToString();

                var nameControl = e.Item.FindControl("UserNameLit") as Literal;
                    nameControl.Text = $"{user.FirstName} {user.LastName}";

                var roleDropDown = e.Item.FindControl("roleDropdown") as DropDownList;
                    roleDropDown.DataSource = this.roles;
                    roleDropDown.DataValueField = "Key";
                    roleDropDown.DataTextField = "Value";
                    roleDropDown.DataBind();
                    roleDropDown.SelectedValue = "-1";
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static List<SiteFolderRole> FolderSetting(int siteId, string folder)
        {
            var repo = new SiteRepository();
            var folderRoles = repo.FindSiteFolderRole(siteId, folder);

            return folderRoles;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static bool SaveFolderSetting(int siteId, string folder, int roleId, int userId)
        {
            var repo = new SiteRepository();
                repo.UpdateSiteFolderRole(new SiteFolderRole
                {
                    SiteId = siteId,
                    Folder = folder,
                    RoleId = roleId,
                    UserId = userId
                });

            return true;
        }
        
        /// <summary>
        /// MAIN CONTROL ENTRY POINT
        /// </summary>
        private void ManageFile()
        {
            string userId = Request.QueryString["userId"];
            string hashKey = Request.QueryString["hashKey"];
            string moduleId = Request.QueryString["moduleId"];

            var user = this.GetUser(int.Parse(userId));
            if (user == null) Response.End();
            if (!_fileManagerPresenter.CanAccess(userId)) Response.End();
            //if (string.IsNullOrEmpty(moduleId)) Response.End();
            //if (!_fileManagerPresenter.ValidateUser(hashKey)) Response.End();

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolder = Path.Combine(fileDirectoryRoot, "FILES", user.AccountId.ToString());
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolder;

            //gets the modules/sites/roles
            _moduleRoles = user.IsAdmin ? this.GetModuleAndRolesForAdmin(int.Parse(userId)) : this.GetModuleAndRoles(int.Parse(userId));

            //if no roles detected . exit
            if (_moduleRoles.Count == 0) return;

            //if moduleId is set
            //set directly to the module
            if (!string.IsNullOrEmpty(moduleId) && (int.Parse(moduleId) > 0)) {
                _moduleRoles = _moduleRoles.Where(m => m.ModuleId == int.Parse(moduleId)).ToList();
                //if no roles detected . exit
                if (_moduleRoles.Count == 0) return;
                fileManagerRoot = Path.Combine(fileManagerRoot, _moduleRoles.FirstOrDefault().ModuleName.ToUpper());
            }

            var sites = new Dictionary<int, string>();

            foreach (var moduleFolder in _moduleRoles)
            {
                string folderName = moduleFolder.ModuleName.ToUpper();
                string siteName = moduleFolder.SiteName;
                string subFolder = Path.Combine(fileManagerRoot, siteName);
                _fileManagerPresenter.EnsurePath(subFolder);

                var name = this.CreateFolders(subFolder, moduleFolder.RoleName);

                sites.Add(moduleFolder.SiteId, name);
            }

            var jsValue = sites.Select(d => $"['{d.Value}', {d.Key}]").Aggregate((c, n) => c + "," + n);
            var jsVariable = $"var Sites = [{jsValue}]";

            ClientScript.RegisterStartupScript(Page.GetType(), "TestArrayScript", jsVariable, true);

            setalldropdown.DataSource = this.roles;
            setalldropdown.DataValueField = "Key";
            setalldropdown.DataTextField = "Value";
            setalldropdown.DataBind();

            var users = this._fileManagerPresenter.GetUsersForModule(int.Parse(moduleId));

            userRepeater.DataSource = users;
            userRepeater.DataBind();
        }
        /// <summary>
        /// DEPRECATED
        /// </summary>
        /// <param name="rootName"></param>
        /// <param name="userId"></param>
        private void ImplementFileManagement(string rootName, int userId = 84)
        {
            string folderName = Path.GetFileName(rootName);
            GleamTech.FileUltimate.FileManagerRootFolder rootFolder = new GleamTech.FileUltimate.FileManagerRootFolder();
            rootFolder.Location.Path = $@"{rootName}\";
            rootFolder.Name = folderName;

            var parentRights = new GleamTech.FileUltimate.FileManagerAccessControl();
            parentRights.Path = $@"\";
            parentRights.AllowedPermissions = GleamTech.FileUltimate.FileManagerPermissions.ListSubfolders;
            rootFolder.AccessControls.Add(parentRights);

            var moduleAndRoles = this.GetModuleAndRoles(userId);
            foreach (var subFolder in _fileManagerPresenter.GetAvailableDirectories(rootName))
            {
                string subFolderName = Path.GetFileName(subFolder);
                var subRights = new GleamTech.FileUltimate.FileManagerAccessControl();

                subRights.Path = $@"\{ subFolderName }";

                if (!moduleAndRoles.Any(m => m.ModuleName.ToLower().Contains(subFolderName.ToLower())))
                {
                    //anything here will be denied (hide the folder per permission)
                    rootFolder.AccessControls.Add(subRights);
                }
                else
                {
                    //else all access
                    var moduleRight = moduleAndRoles.Where(m => m.ModuleName.ToLower().Contains(subFolderName.ToLower())).FirstOrDefault();
                    subRights.AllowedPermissions = this.GetPermission(moduleRight.RoleName);
                    rootFolder.AccessControls.Add(subRights);
                }
            }

            fileManager.RootFolders.Add(rootFolder);
        }
        /// <summary>
        /// DEPRECATED
        /// IMPLEMENTS ROOT FOLDER for GLEAM
        /// </summary>
        /// <param name="rootName"></param>
        /// <returns></returns>
        private FileManagerRootFolder RootFolderManage(string rootName) {
            string folderName = Path.GetFileName(rootName);
            GleamTech.FileUltimate.FileManagerRootFolder rootFolder = new GleamTech.FileUltimate.FileManagerRootFolder();
            rootFolder.Location.Path = $@"{rootName}\";
            rootFolder.Name = folderName;

            var parentRights = new GleamTech.FileUltimate.FileManagerAccessControl();
            parentRights.Path = $@"\";
            parentRights.AllowedPermissions = GleamTech.FileUltimate.FileManagerPermissions.ListSubfolders;
            rootFolder.AccessControls.Add(parentRights);

            return rootFolder;
        }
        /// <summary>
        /// DEPRECATED
        /// IMPLEMENTS SUB FOLDERS for GLEAM
        /// </summary>
        /// <param name="subFolder"></param>
        /// <param name="moduleRight"></param>
        /// <param name="rootFolder"></param>
        private void SubFolderManage(string parentFolder, string subFolder, string moduleRight, FileManagerRootFolder rootFolder) {
            string subFolderName = Path.GetFileName(subFolder);
            string path = $@"\{ (subFolderName) }";
            var subRights = new GleamTech.FileUltimate.FileManagerAccessControl();

            subRights.Path = path;

            if (!_moduleRoles.Any(m => m.SiteName.ToLower().Contains(subFolderName.ToLower())))
            {
                //anything here will be denied (hide the folder per permission)
                rootFolder.Name = subFolderName + $" ({ moduleRight })";
                rootFolder.AccessControls.Add(subRights);
            }
            else
            {
                //else all access
                rootFolder.Name = subFolderName + $" ({ moduleRight })";
                subRights.AllowedPermissions = this.GetPermission(moduleRight);
                rootFolder.AccessControls.Add(subRights);
            }
        }

        private string CreateFolders(string directory, string moduleRight) {

            FileManagerRootFolder rootFolder = new FileManagerRootFolder();
            FileManagerAccessControl rights = new FileManagerAccessControl();
            string folderName = Path.GetFileName(directory);
            string path = $@"\";

            rootFolder.Location.Path = $@"{directory}\";
            rights.Path = path;

            if (!_moduleRoles.Any(m => m.SiteName.ToLower().Contains(folderName.ToLower())))
            {
                //anything here will be denied (hide the folder per permission)
                rootFolder.Name = folderName + $" ({ moduleRight })";
                rootFolder.AccessControls.Add(rights);
            }
            else
            {
                //else all access
                rootFolder.Name = folderName + $" ({ moduleRight })";
                rights.AllowedPermissions = this.GetPermission(moduleRight);
                rootFolder.AccessControls.Add(rights);
            }
            fileManager.RootFolders.Add(rootFolder);

            return rootFolder.Name;
        }

        private UserModel GetUser(int userId) {
            return _fileManagerPresenter.GetUser(userId);
        }

        private List<UserModuleAndRoleModel> GetModuleAndRoles(int userId)
        {
            return _fileManagerPresenter.GetModuleAndRoles(userId);
        }

        private List<UserModuleAndRoleModel> GetModuleAndRolesForAdmin(int userId)
        {
            return _fileManagerPresenter.GetModuleAndRolesForAdmin(userId);
        }

        private List<ModuleModel> GetModules()
        {
            return _fileManagerPresenter.GetModules();
        }
        
        private GleamTech.FileUltimate.FileManagerPermissions GetPermission(string permission)
        {
            switch (permission)
            {
                case "admin":
                case "superadmin":
                    return GleamTech.FileUltimate.FileManagerPermissions.Full;
                case "editor":
                    return
                        GleamTech.FileUltimate.FileManagerPermissions.Create | GleamTech.FileUltimate.FileManagerPermissions.Download | GleamTech.FileUltimate.FileManagerPermissions.Copy | GleamTech.FileUltimate.FileManagerPermissions.ListFiles | GleamTech.FileUltimate.FileManagerPermissions.Print | GleamTech.FileUltimate.FileManagerPermissions.Preview | GleamTech.FileUltimate.FileManagerPermissions.ListSubfolders | GleamTech.FileUltimate.FileManagerPermissions.Upload | GleamTech.FileUltimate.FileManagerPermissions.Rename | GleamTech.FileUltimate.FileManagerPermissions.Paste;
                default:
                    return GleamTech.FileUltimate.FileManagerPermissions.ReadOnly;
            }
        }
    }
}