﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class IssueRecipientController : ApiController
    {
        private IssueRecipientService _service;
        public IssueRecipientController()
        {
            _service = new IssueRecipientService();
        }

        [HttpGet]
        [Route("IssueRecipient/GetAllRecipients")]
        [ResponseType(typeof(ResponseDataModel<List<IssueRecipientModel>>))]
        public IHttpActionResult GetAllEmailTemplate(int siteId)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAll(siteId).OrderBy(x=>x.IssueRecipientType.Name).ThenBy(x=>x.Email).ToList();
                return new ResponseDataModel<List<IssueRecipientModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Issue Recipients",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IssueRecipient/Get")]        
        public IHttpActionResult GetAllTypes(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.Get(id);
                return new ResponseDataModel<IssueRecipientModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Issue Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IssueRecipient/GetAllTypes")]
        [ResponseType(typeof(ResponseDataModel<List<IssueRecipientTypeModel>>))]
        public IHttpActionResult GetAllTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllTypes().OrderBy(x=>x.Name).ToList();
                return new ResponseDataModel<List<IssueRecipientTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded All Issue Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IssueRecipient/Save")]
        public IHttpActionResult Save(IssueRecipientModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _service.Save(model);
                return new ResponseDataModel<IssueRecipientModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value? "Successfully Saved Issue":"Email already exists!",
                    Data = model
                };
            }));
        }

        [HttpGet]
        [Route("IssueRecipient/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var result = _service.Delete(id);
                return new ResponseDataModel<IssueRecipientModel>
                {
                    Success = result,
                    StatusCode = HttpStatusCode.OK,
                    Message = result? "Successfully Loaded Daily Report Recipients":"Cannot delete data",
                    Data = null
                };
            }));
        }

    }
}
