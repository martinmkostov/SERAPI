﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SERApp.API.Controllers
{
    public class SiteController : ApiController
    {
        private SiteService _siteService;
        public SiteController() {
            _siteService = new SiteService();
        }

        [HttpGet]
        [Route("Site/Get")]
        [ResponseType(typeof(ResponseDataModel<SiteModel>))]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteService.Get(id);
                return new ResponseDataModel<SiteModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Site Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Site/GetAll")]
        [ResponseType(typeof(ResponseDataModel<List<SiteModel>>))]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteService.GetAll();
                return new ResponseDataModel<List<SiteModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Site Data",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Site/GetByAccountId")]
        [ResponseType(typeof(ResponseDataModel<List<SiteModel>>))]
        public IHttpActionResult GetByAccountId(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _siteService.GetByAccountId(id);
                return new ResponseDataModel<List<SiteModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Site Data List",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Site/Save")]
        [ResponseType(typeof(ResponseDataModel<SiteModel>))]
        public IHttpActionResult SaveSite(SiteModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var value = _siteService.SaveSite(model);
                return new ResponseDataModel<SiteModel>
                {
                    Success = value,
                    StatusCode = HttpStatusCode.OK,
                    Message = value?"Successfully Saved Site Data":"Object name already exists"
                };
            }));
        }

        [HttpPost]
        [Route("Site/Delete")]
        [ResponseType(typeof(ResponseDataModel<SiteModel>))]
        public IHttpActionResult Delete(SiteModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _siteService.DeleteSite(model.Id);
                return new ResponseDataModel<SiteModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Site Data"
                };
            }));
        }
    }
}
