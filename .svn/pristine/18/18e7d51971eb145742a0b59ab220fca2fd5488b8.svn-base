﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class TenantService
    {
        TenantRepository _tenantRepository;
        SiteRepository _siteRepository;
        LogRepository _logRepository;
        public TenantService() {
            _tenantRepository = new TenantRepository();
            _logRepository = new LogRepository();
            _siteRepository = new SiteRepository();
        }

        public TenantModel Get(int id) {
            try
            {
               var tenant = _tenantRepository.Get(id);
                return entityToModel(tenant);
            }
            catch(Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
            
        }

        public List<TenantModel> GetAll()
        {
            try
            {
                return _tenantRepository.GetAll()
                    .Select(t => new TenantModel() {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }

        }

      
        public List<TenantModel> GetAllByAccountId(int id)
        {
            try
            {

                IEnumerable<Tenant> data;

                if (id == 0)
                {
                    data = _tenantRepository.GetAll();
                }
                else
                {
                    data = _tenantRepository.GetAllByAccountId(id);
                }
                SiteService siteService = new SiteService();
                return data.Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        Site = entityToModel(_siteRepository.Get(t.SiteId)),
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement,
                        TenantType = t.TenantType
                }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<TenantModel> GetAllBySiteId(int id) {
            try
            {
                return _tenantRepository.GetAllBySiteId(id)
                    .Select(t => new TenantModel()
                    {
                        Id = t.Id,
                        SiteId = t.SiteId,
                        Name = t.Name,
                        Email = t.Email,
                        Phone = t.Phone,
                        PermiseType = t.PermiseType,
                        VisitingAddress = t.VisitingAddress,
                        UserName = t.UserName,
                        Password = t.Password,
                        InsuranceCompany = t.InsuranceCompany,
                        SecurityCompany = t.SecurityCompany,
                        SiteManagerName = t.SiteManagerName,
                        SiteManagerMobilePhone = t.SiteManagerPhone,
                        SiteManagerPhone = t.SiteManagerPhone,
                        ContractName = t.ContractName,
                        ContractVatNo = t.ContractVatNo,
                        InvoiceAddress = t.InsuranceCompany,
                        InvoiceZipAndCity = t.InvoiceZipAndCity,
                        ContactComments = t.ContactComments,
                        CCTV = t.CCTV,
                        IntrusionAlarm = t.IntrusionAlarm,
                        LastUpdatedDate = t.LastUpdatedDate,
                        CreatedDate = t.CreatedDate,
                        SiteManagerEmail = t.SiteManagerEmail,
                        Flag = t.Flag,
                        IsActive = t.IsActive,
                        PropertyDesignation = t.PropertyDesignation,
                        UseWrittenStatement = t.UseWrittenStatement,
                        TenantType = t.TenantType
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public List<ContactModel> GetAllTenantContactsByTenantId(int tenantId) {
            var data = _tenantRepository.GetAllTenantContactsByTenantId(tenantId).ToList();
            return data.Select(t => new ContactModel()
            {
                FullName = t.FullName,
                EmailAddress = t.EmailAddress,
                MobileNumber = t.MobileNumber,
                PhoneNumber = t.PhoneNumber,
                JobTypeName = JobTypeName(tenantId, t.Id)
            }).ToList();
        }

        public void SaveTenantContact(TenantContactModel contact) {
            _tenantRepository.SaveTenantContacts(contact);
        }

        private string JobTypeName(int tenantId, int contactId) {
            var data = _tenantRepository.GetTenantContact(tenantId, contactId);
            switch(data.JobTypeId)
            {
                case 1: return "Shop Owner";
                case 2: return "Site Manager";
                case 3: return "Contact Manager";
                case 4: return "Cashier";
                case 5: return "Other";
                default: return "N / A";
            }
        }

        public void SaveTenant(TenantModel model) {
            try
            {
                _tenantRepository.SaveTenant(model);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void DeleteTenant(int id)
        {
            try
            {
                _tenantRepository.DeleteTenant(id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, 0);
                throw ex;
            }
        }

        public void SaveTenantType(TenantTypeModel model) {
            _tenantRepository.SaveTenantType(model);
        }

        public void SavePremiseType(PremiseTypeModel model) {
            _tenantRepository.SavePremiseType(model);
        }

        public void SaveJobTitle(JobTitleModel model) {
            _tenantRepository.SaveJobTite(model);
        }

        public TenantModel entityToModel(Tenant entity) {
            return new TenantModel()
            {
                Id = entity.Id,
                SiteId = entity.SiteId,
                Name = entity.Name,
                Email = entity.Email,
                Phone = entity.Phone,
                PermiseType = entity.PermiseType,
                VisitingAddress = entity.VisitingAddress,
                UserName = entity.UserName,
                Password = entity.Password,
                InsuranceCompany = entity.InsuranceCompany,
                SecurityCompany = entity.SecurityCompany,
                SiteManagerName = entity.SiteManagerName,
                SiteManagerMobilePhone = entity.SiteManagerPhone,
                SiteManagerPhone = entity.SiteManagerPhone,
                Site = entityToModel(_siteRepository.Get(entity.SiteId)),
                ContractName = entity.ContractName,
                ContractVatNo = entity.ContractVatNo,
                InvoiceAddress = entity.InsuranceCompany,
                InvoiceZipAndCity = entity.InvoiceZipAndCity,
                ContactComments = entity.ContactComments,
                CCTV = entity.CCTV,
                IntrusionAlarm = entity.IntrusionAlarm,
                LastUpdatedDate = entity.LastUpdatedDate,
                CreatedDate = entity.CreatedDate,
                SiteManagerEmail = entity.SiteManagerEmail,
                Flag = entity.Flag,
                IsActive = entity.IsActive,
                PropertyDesignation = entity.PropertyDesignation,
                UseWrittenStatement = entity.UseWrittenStatement,
                TenantType = entity.TenantType
            };
        }

        public static SiteModel entityToModel(Site entity)
        {
            return new SiteModel()
            {
                Id = entity.Id,
                AccountId = entity.AccountId,
                Name = entity.Name,
                Address = entity.Address,
                City = entity.City,
                PostalCode = entity.PostalCode,
                AdditionalLocationInfo = entity.AdditionalLocationInfo,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                ContactPerson = entity.ContactPerson,
                ContactPersonNumber = entity.ContactPersonNumber,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted
            };
        }
    }
}
