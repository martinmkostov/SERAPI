﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentController : ApiController
    {
        private IncidentService _service;
        public IncidentController()
        {
            _service = new IncidentService();
        }

        [HttpGet]
        [Route("Incident/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetAll")]
        public IHttpActionResult GetAll(int accountId =0,int siteId=0,string selectedDate = "",int selectedType1 = 0, int selectedType2 = 0, int selectedType3 = 0, string searchText = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidents(accountId,siteId,selectedDate,selectedType1,selectedType2,selectedType3, searchText);
                return new ResponseDataModel<IEnumerable<IncidentModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Incident/GetGuardsToday")]
        public IHttpActionResult GetGuardsToday(int accountId = 0, int siteId = 0, string selectedDate = "")
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuardsToday(accountId, siteId, selectedDate);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("Incident/GetIncidentRange")]
        public IHttpActionResult GetIncidentRange([FromBody]WeekReportModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentRange(model);
                return new ResponseDataModel<WeekReportOverallReportModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incidents",
                    Data = data
                };
            }));
        }
        

        [HttpPost]
        [Route("Incident/Save")]
        public IHttpActionResult Save(IncidentModel model)
        {   
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddIncident(model);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident",
                    Data = model
                };
            }));
        }
        [HttpGet]
        [Route("Incident/GetAllSection")]
        public IHttpActionResult GetAllSection()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetSectionModels().OrderBy(x=>x.SectionValue);
                return new ResponseDataModel<IEnumerable<SectionModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Sections",
                    Data = data
                };
            }));
        }
      

        [HttpPost]
        [Route("Incident/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIncident(id);
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident",
                };
            }));
        }

        [HttpPost]
        [Route("Incident/SendDailyReport")]
        public IHttpActionResult SendDailyReport([FromBody]ReportEmailModel base64Data)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                base64Data.recepients.ToList().ForEach(e =>
                {
                    Service.Tools.Email.SendEmailWithAttachment("", e, string.IsNullOrEmpty(base64Data.EmailTitle)? "Daily Report": base64Data.EmailTitle,
                        "We have attached a pdf copy of the daily report.", "", base64Data.base64);
                });
                return new ResponseDataModel<IncidentModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Sent Daily Report",
                };
            }));
        }
    }
}
