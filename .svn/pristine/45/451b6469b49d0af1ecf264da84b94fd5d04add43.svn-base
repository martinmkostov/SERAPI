﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Repositories
{
    public class TenantRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public TenantRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Tenant Get(int id) {
            return db.Tenants.Where(t => t.Id == id).SingleOrDefault();
        }

        public List<Tenant> GetAll()
        {
            return db.Tenants.ToList();
        }

        public List<Tenant> GetAllByAccountId(int accountId)
        {
            var data = (from t in db.Tenants
                        join s in db.Sites on t.SiteId equals s.Id
                        where s.AccountId == accountId
                        select t).ToList();
            return data;
        }

        public List<Tenant> GetAllBySiteId(int siteId)
        {
            return db.Tenants.Where(t => t.SiteId == siteId).ToList();
        }

        public List<Contact> GetAllTenantContactsByTenantId(int tenantId) {
            var contacts = (from tc in db.TenantContacts
                            join c in db.Contacts on tc.ContactId equals c.Id
                            where tc.TenantId == tenantId
                            select c).ToList();
            return contacts;
        }

        public TenantContact GetTenantContact(int tenantId, int contactId) {
            return db.TenantContacts.Where(tc => tc.TenantId == tenantId && tc.ContactId == contactId).FirstOrDefault();
        }

        public void SaveTenantContacts(TenantContactModel tenantContact) {
            //CONTACT
            int contactId = 0;
            ContactModel contactModel = tenantContact.Contact;
            var contactData = db.Contacts.Where(c => c.Id == tenantContact.ContactId).SingleOrDefault();
            if (contactData != null)
            {
                contactData.FirstName = contactModel.FirstName;
                contactData.LastName = contactModel.LastName;
                contactData.EmailAddress = contactModel.EmailAddress;
                contactData.PhoneNumber = contactModel.PhoneNumber;
                contactData.MobileNumber = contactModel.MobileNumber;
                contactData.LastUpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
            else {
                var newContact = new Contact() {
                    FirstName = contactModel.FirstName,
                    LastName = contactModel.LastName,
                    EmailAddress = contactModel.EmailAddress,
                    PhoneNumber = contactModel.PhoneNumber,
                    MobileNumber = contactModel.MobileNumber,
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now
                };
                db.Contacts.Add(newContact);
                db.SaveChanges();
                contactId = newContact.Id;
            }
            
            
            //TENANT CONTACT
            var newTenantContact = new TenantContact() {
                ContactId = contactId,
                TenantId = tenantContact.TenantId,
                JobTypeId = tenantContact.JobTypeId
            };
            db.TenantContacts.Add(newTenantContact);
            db.SaveChanges();
        }

        public void SaveTenant(TenantModel model)
        {
            var data = db.Tenants.Where(t => t.Id == model.Id).SingleOrDefault();
            if (data != null)
            {
                data.Name = model.Name;
                data.Email = model.Email;
                data.Phone = model.Phone;
                data.PermiseType = model.PermiseType;
                data.VisitingAddress = model.VisitingAddress;
                data.UserName = model.UserName;
                data.Password = model.Password;
                data.InsuranceCompany = model.InsuranceCompany;
                data.SecurityCompany = model.SecurityCompany;
                data.SiteManagerName = model.SiteManagerName;
                data.SiteManagerMobilePhone = model.SiteManagerPhone;
                data.SiteManagerPhone = model.SiteManagerPhone;
                data.ContractName = model.ContractName;
                data.ContractVatNo = model.ContractVatNo;
                data.InvoiceAddress = model.InsuranceCompany;
                data.InvoiceZipAndCity = model.InvoiceZipAndCity;
                data.ContactComments = model.ContactComments;
                data.CCTV = model.CCTV;
                data.IntrusionAlarm = model.IntrusionAlarm;
                data.LastUpdatedDate = DateTime.Now;
                data.SiteManagerEmail = model.SiteManagerEmail;
                data.Flag = model.Flag;
                data.PropertyDesignation = model.PropertyDesignation;
                data.UseWrittenStatement = model.UseWrittenStatement;
                data.SiteId = model.SiteId;
            }
            else {
                Tenant newTenant = new Tenant()
                {
                    Name = model.Name,
                    Email = model.Email,
                    Phone = model.Phone,
                    PermiseType = model.PermiseType,
                    VisitingAddress = model.VisitingAddress,
                    UserName = model.UserName,
                    Password = model.Password,
                    InsuranceCompany = model.InsuranceCompany,
                    SecurityCompany = model.SecurityCompany,
                    SiteManagerName = model.SiteManagerName,
                    SiteManagerMobilePhone = model.SiteManagerPhone,
                    SiteManagerPhone = model.SiteManagerPhone,
                    ContractName = model.ContractName,
                    ContractVatNo = model.ContractVatNo,
                    InvoiceAddress = model.InsuranceCompany,
                    InvoiceZipAndCity = model.InvoiceZipAndCity,
                    ContactComments = model.ContactComments,
                    CCTV = model.CCTV,
                    IntrusionAlarm = model.IntrusionAlarm,
                    LastUpdatedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    SiteManagerEmail = model.SiteManagerEmail,
                    Flag = false,
                    PropertyDesignation = model.PropertyDesignation,
                    UseWrittenStatement = model.UseWrittenStatement,
                    SiteId = model.SiteId,
                    IsActive = true
                };
                db.Tenants.Add(newTenant);
            }
            db.SaveChanges();
        }

        public void DeleteTenant(int id) {
            var data = db.Tenants.Where(t => t.Id == id).SingleOrDefault();
            if (data != null)
            {
                db.Tenants.Remove(data);
                db.SaveChanges();
            }
        }

    }
}
