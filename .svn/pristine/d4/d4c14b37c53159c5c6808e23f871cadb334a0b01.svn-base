<div [hidden]="!showSpinner">
    <app-spinner></app-spinner>
</div>
<div class="accounts-edit">
    <div class="row">
        <div class="col-md-12"> 
                <div *ngIf="isAccountEditMode"  class="card mb-3">
                    <div class="card-header">
                        View / Edit Account
                        <div class="pull-right">
                                <button [hidden]="!canSave" type="button" class="btn btn-xs btn-success rounded-btn" (click)="saveChanges()"><i class="fa fa-check-circle fa-lg"></i> Save </button>&nbsp;
                                <button [hidden]="!isSuperAdmin" type="button" class="btn btn-xs btn-danger rounded-btn" (click)="delete()"><i class="fa fa-minus-circle fa-lg"></i> Delete </button>&nbsp;
                                <a class="btn btn-xs btn-secondary rounded-btn" [routerLink]="['/accounts']"><i class="fa fa-chevron-circle-left fa-lg"></i> Cancel </a>&nbsp;
                        </div>
                    </div>
                    <div class="card-body flex">
                        <div class="col-sm-6 form-content">
                            <div class="form-group mb-1">
                                <span class="form-span-label">Company</span>
                                <input type="text" [(ngModel)]="model.Company" class="form-control input-underline input-sm" id="" placeholder="Company">
                            </div>
                            <div class="form-group mb-1">
                                <span class="form-span-label">Account Name</span>
                                <input type="text" [disabled]="true" [(ngModel)]="model.Name" class="form-control input-underline input-sm" id="" placeholder="Name">
                            </div>
                            <div class="form-group mb-1">
                                <span class="form-span-label">Email Address</span>
                                <input type="text" [(ngModel)]="model.EmailAddress" class="form-control input-underline input-sm" id="" placeholder="Email">
                            </div>

                            <div class="form-group mb-1" [hidden]="!isSuperAdmin">
                                <input type="checkbox" #isActiveCheck id="isActiveCheck" (change)="onIsActiveChange(isActiveCheck.checked)" [(ngModel)]="model.IsActive"> Active &nbsp;
                                <input type="checkbox" #isDeletedCheck id="isDeletedCheck" (change)="onIsDeletedChange(isDeletedCheck.checked)" [(ngModel)]="model.IsDeleted"> Deleted
                            </div>
                        </div>
                         <div class="col-sm-6 form-content">
                            <div class="form-group mb-1">
                                <span class="form-span-label">Contact Name</span>
                                <input type="text" [(ngModel)]="model.ContactName" class="form-control input-underline input-sm" id="" placeholder="Contact Name">
                            </div>
                            <div class="form-group mb-1">
                                <span class="form-span-label">Contact #</span>
                                <input type="text" [(ngModel)]="model.ContactNumber" class="form-control input-underline input-sm" id="" placeholder="Contac #">
                            </div>
                        </div>
                    </div>
                </div>
                <div *ngIf="!isAccountEditMode" class="card mb-3">
                    <div class="card-header">
                         View / Edit {{ mode | uppercase }} &nbsp;&nbsp; <span class="badge badge-pill badge-primary" style="font-size:15px"> {{ model.Company }} </span>
                         <div class="pull-right">
                              <a class="btn btn-xs btn-secondary rounded-btn" [routerLink]="['/accounts']"><i class="fa fa-chevron-circle-left fa-lg"></i> Cancel </a>&nbsp;
                         </div>
                    </div>
                    <div class="card-body">
                        <div class="form-content">
                            <ngb-tabset #accountTabs (tabChange)="tabChange($event)" [destroyOnHide]="false">
                                <ngb-tab id="tab-0" title="Account Modules" *ngIf="isSuperAdmin">
                                    <ng-template ngbTabContent>
                                        <div class="col-md-12">
                                            <div class="card-body">
                                                <app-accounts-module-list #accountsModuleList></app-accounts-module-list>
                                            </div>
                                        </div>
                                    </ng-template>
                                </ngb-tab>
                                <ngb-tab id="tab-1" title="Users">
                                    <ng-template ngbTabContent>
                                        <div class="col-md-12">
                                            <div class="card-body">
                                                <div *ngIf="isAccountEditMode" class="pull-right" style="padding-bottom:10px;">
                                                    <button type="button" (click)="createUser(model.Id)" class="btn btn-xs btn-primary rounded-btn"><i class="fa fa-plus-square fa-lg"></i> Assign a User </button>
                                                </div>
                                                <app-users-list #usersList></app-users-list>
                                            </div>
                                        </div>
                                    </ng-template>
                                </ngb-tab>
                                <ngb-tab id="tab-2" title="Objects">
                                    <ng-template ngbTabContent>
                                        <div class="col-md-12">
                                            <div class="card-body">
                                                <div *ngIf="isAccountEditMode" class="pull-right" style="padding-bottom:10px;">
                                                    <button type="button" (click)="createSite(model.Id)" class="btn btn-xs btn-primary rounded-btn"><i class="fa fa-plus-square fa-lg"></i> Add a Site </button>
                                                </div>
                                                <app-sites-list #sitesList></app-sites-list>
                                            </div>
                                        </div>
                                    </ng-template>
                                </ngb-tab>
                            </ngb-tabset>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
