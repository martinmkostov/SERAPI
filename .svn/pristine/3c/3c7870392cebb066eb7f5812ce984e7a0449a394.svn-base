import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';

import { AccountService, SiteService } from '../../services';
import { SiteModel } from '../../models';

@Component({
    selector: 'app-sites-edit',
    templateUrl: './sites-edit.component.html',
    styleUrls: ['./sites.component.scss']
})
export class SitesEditComponent extends BaseComponent implements OnInit {
    @ViewChild('accountsFilter') accountsFilterEl: ElementRef;
    public model: any;
    public selectedAccount: any;
    public accountsList: any;
    public hideAccountSelect: boolean = true;
    public showSpinner: boolean = false;
    constructor(
        private siteService: SiteService, 
        private accountService: AccountService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router) {
            super();
        }

    public ngOnInit() {
        this.model = new SiteModel();
        this.route.params.subscribe((params) => {
            if (params) {
                const accountId = params['account'];
                this.selectedAccount = accountId;
                this.model.AccountId = accountId;
                const id = params['id'];
                this.loadData(id);
            }
        });
    }

    public loadData(id? : number){
        this.showSpinner = true;
        this.siteService.getSite(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.model = response.Data;
            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public saveChanges(){
        this.showSpinner = true;
        this.siteService.saveSite(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            //this.toastr.success(response.Message, "Success"); 
            this.ngOnInit();
        });
    }

    public onIsActiveChange(value: boolean){
        this.model.IsActive = value;
    }

    public onIsDeletedChange(value: boolean){
        this.model.IsDeleted = value;
    } 

}
