﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ITaskService
    {
        IEnumerable<MainCategoryModel> GetAllMainCategories();
        IEnumerable<SubCategoryModel> GetAllSubCategories();
        IEnumerable<SiteTaskModel> GetPublishedTask(int Id);
        IEnumerable<TaskModel> GetAllTasks();
        IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId);
        void AddTask(TaskModel model);
        void DeleteTask(int id);
        void AddMainCategory(MainCategoryModel model);
        void AddSubCategory(SubCategoryModel model);
        void PublishTask(SiteTaskModel model);
        void UnPublishTask(SiteTaskModel model);
        void Confirm(SiteTaskLogModel model);
        TaskModel GetTask(int id);
        TaskModel GetTaskBySiteIdAndTaskId(int taskId,int siteId);
    }

    public class TaskService : ITaskService
    {
        private ITaskRepository _taskRepository;
        private IRepository<MainCategory> _mainCategoryRepository;
        private IRepository<SubCategory> _subCategoryRepository;
        private IRepository<TaskMainCategory> _taskMainCategory;
        private IRepository<TaskSubCategory> _taskSubCategory;
        private IRepository<SiteTask> _siteTaskRepository;
        private IRepository<SiteTaskLog> _siteTaskLogRepository;
        public TaskService()
        {
            _taskRepository = new TaskRepository();
            _mainCategoryRepository = new Repository<MainCategory>();
            _subCategoryRepository = new Repository<SubCategory>();
            _taskMainCategory = new Repository<TaskMainCategory>();
            _taskSubCategory = new Repository<TaskSubCategory>();
            _siteTaskRepository = new Repository<SiteTask>();
            _siteTaskLogRepository = new Repository<SiteTaskLog>();
        }

        public void AddMainCategory(MainCategoryModel model)
        {

            if (model.Id == 0)
            {
                _mainCategoryRepository.Save(new MainCategory()
                {
                    Name = model.Name
                });
            }
            else
            {
                var data = _mainCategoryRepository.Get(model.Id);
                data.Name = model.Name;

                _mainCategoryRepository.Update(data);
            }
           
        }

        public void AddSubCategory(SubCategoryModel model)
        {
            if (model.Id == 0)
            {
                _subCategoryRepository.Save(new SubCategory()
                {
                    Name = model.Name
                });
            }
            else
            {
                var data = _subCategoryRepository.Get(model.Id);
                data.Name = model.Name;

                _subCategoryRepository.Update(data);
            }

        }

        public void AddTask(TaskModel model)
        {
            if (model.Id == 0)
            {
                _taskRepository.Save(new Data.Models.Task()
                {
                    TaskSubCategories = model.TaskSubCategories.Select(r => new TaskSubCategory()
                    {
                        SubCategoryId = r.SubCategoryId,
                    }).ToList(),
                    TaskMainCategory = model.TaskMainCategory.Select(r => new TaskMainCategory()
                    {
                        MainCategoryId = r.MainCategoryId
                    }).ToList(),
                    Description = model.Description,
                    Interval = model.Interval,
                    NuberOfDays = model.NuberOfDays,
                    Purpose = model.Purpose,
                    TaskNo = model.TaskNo,
                    RemindAfter = model.RemindAfter,
                    Title = model.Title,
                });
            }
            else
            {
                var task = _taskRepository.GetWithChildsIncluded(model.Id);
                task.Description = model.Description;
                task.Interval = model.Interval;
                task.NuberOfDays = model.NuberOfDays;
                task.Purpose = model.Purpose;
                task.TaskNo = model.TaskNo;
                task.Title = model.Title;
                task.RemindAfter = model.RemindAfter;

                task.TaskMainCategory.ToList().ForEach(x => 
                {
                    _taskMainCategory.Delete(x.Id);
                });

                task.TaskSubCategories.ToList().ForEach(x =>
                {
                    _taskSubCategory.Delete(x.Id);
                });

                model.TaskMainCategory.ForEach(x => 
                {
                    _taskMainCategory.Save(new TaskMainCategory()
                    {
                        MainCategoryId = x.MainCategoryId,
                        TaskId = task.Id
                    });
                });

                model.TaskSubCategories.ForEach(x =>
                {
                    _taskSubCategory.Save(new TaskSubCategory()
                    {
                        SubCategoryId = x.SubCategoryId,
                        TaskId = task.Id
                    });
                });


                _taskRepository.Update(task);
            }
        }

        public void Confirm(SiteTaskLogModel model)
        {
            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = DateTime.Now,
                LogType = (int)TaskLogType.Confirm,
                SiteTaskId = model.SiteTaskId

            };

            _siteTaskLogRepository.Save(tasklog);
        }

        public void DeleteTask(int id)
        {
            _taskRepository.Delete(id);
        }

        public IEnumerable<MainCategoryModel> GetAllMainCategories()
        {
            return _mainCategoryRepository.GetAll().Select(x => new MainCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,                
            });
        }

        public IEnumerable<SubCategoryModel> GetAllSubCategories()
        {
            return _subCategoryRepository.GetAll().Select(x => new SubCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,
            });
        }

        public IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId)
        {
           var tasksites = _siteTaskRepository.GetByPredicate(x => x.SiteId == siteId).ToList();

            var models = new List<TaskModel>();

            tasksites.ForEach(x => 
            {
                
                var task = _taskRepository.GetWithChildsIncluded(x.TaskId);

                var tModel = new TaskModel()
                {
                    Id = x.TaskId,
                    Description = task.Description,
                    Interval = task.Interval,
                    NuberOfDays = task.NuberOfDays,
                    Purpose = task.Purpose,
                    TaskNo = task.TaskNo,
                    Title = task.Title,
                    RemindAfter = task.RemindAfter,
                    TaskMainCategory = task.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                    {
                        Id = r.Id,
                        MainCategoryId = r.MainCategoryId,
                        TaskId = r.TaskId,
                        MainCategory = new MainCategoryModel
                        {
                            Name = r.MainCategory.Name
                        }
                    }).ToList(),
                    TaskSubCategories = task.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                    {
                        Id = r.Id,
                        SubCategoryId = r.SubCategoryId,
                        TaskId = r.TaskId,
                        SubCategory = new SubCategoryModel
                        {
                            Name = r.SubCategory.Name
                        }
                    }).ToList(),
                };

                tModel.StartDate = x.StartDate;
                var logtype = (int)TaskLogType.Confirm;
                _siteTaskLogRepository.GetByPredicate(r=>r.SiteTaskId == x.Id && r.LogType == logtype).ToList().ForEach(r=> 
                {
                    tModel.StartDate = tModel.StartDate.AddDays(tModel.NuberOfDays);
                });


                tModel.EndDate = tModel.StartDate.AddDays(tModel.NuberOfDays);

                if (tModel.StartDate > DateTime.Now)
                {
                    tModel.Status = "Ready";
                }
                else if (tModel.StartDate <= DateTime.Now)
                {
                    tModel.Status = "Ongoing";
                }

                if (tModel.EndDate <= DateTime.Now)
                {
                    tModel.Status = "Delayed";
                }

                models.Add(tModel);
            });

            return models;
        }

        public IEnumerable<TaskModel> GetAllTasks()
        {
            return _taskRepository.GetAllTasksChildIncluded().Select(x => new TaskModel()
            {
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                Id = x.Id
            }).ToList();
        }

        public IEnumerable<SiteTaskModel> GetPublishedTask(int Id)
        {
            return _siteTaskRepository.GetByPredicate(x => x.TaskId == Id).Select(x=> new SiteTaskModel()
            {
                Id = x.Id,
                InfoRoles = x.InfoRoles,
                Performer = x.Performer,
                SiteId = x.SiteId,
                TaskId = x.TaskId,
                StartDate = new SERApp.Models.Common.DateModel()
                {
                    day = x.StartDate.Day,
                    month = x.StartDate.Month,
                    year = x.StartDate.Year
                },
                
            }).ToList();

        }

        public TaskModel GetTask(int id)
        {
            var x = _taskRepository.GetWithChildsIncluded(id);

            return new TaskModel()
            {
                Id = x.Id,
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList(),
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList(),
            };
        }

        public TaskModel GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            var x = _taskRepository.GetWithChildsIncluded(taskId);

            var task = new TaskModel()
            {
                Id = x.Id,
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList(),
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList(),
            };

            var sitetask = _siteTaskRepository.GetByPredicate(v => v.SiteId == siteId && v.TaskId == taskId).SingleOrDefault();

            task.StartDate = sitetask.StartDate;
            var logtype = (int)TaskLogType.Confirm;
            _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == logtype).ToList().ForEach(r =>
            {
                task.StartDate = task.StartDate.AddDays(task.NuberOfDays);
            });

            task.EndDate = sitetask.StartDate.AddDays(x.NuberOfDays);
           
            task.SiteTaskId = sitetask.Id;

            if (task.StartDate > DateTime.Now)
            {
                task.Status = "Ready";
            }
            else if (task.StartDate <= DateTime.Now)
            {
                task.Status = "Ongoing";
            }

            if (task.EndDate <= DateTime.Now)
            {
                task.Status = "Delayed";
            }

            return task;

        }

        public void PublishTask(SiteTaskModel model)
        {
            _siteTaskRepository.Save(new SiteTask()
            {
                SiteId = model.SiteId,
                InfoRoles = model.InfoRoles,
                Performer = model.Performer,
                StartDate = new DateTime(model.StartDate.year,model.StartDate.month,model.StartDate.day),
                TaskId = model.TaskId
            });
        }

        public void UnPublishTask(SiteTaskModel model)
        {
            _siteTaskRepository.Delete(model.Id);
        }
    }
}
