﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class GuardController : ApiController
    {
        private GuardService _service;
        public GuardController()
        {
            _service = new GuardService();
        }

        [HttpGet]
        [Route("Guard/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuardById(id);
                return new ResponseDataModel<GuardModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Guard/GetAll")]
        public IHttpActionResult GetAll(int accountId = 0,int siteId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetGuards(accountId,siteId).OrderBy(x => x.Name);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("Guard/GetAllGuardsPerDayAndAccount")]
        public IHttpActionResult GetAllGuardsPerDayAndAccount(string date="",int accountId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllGuardsPerDayAndAccount(date,accountId).OrderBy(x=>x.Name);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }
        [HttpGet]
        [Route("Guard/GetAllGuardsByDayAccountAndSiteId")]
        public IHttpActionResult GetAllGuardsByDayAccountAndSiteId(string date = "", int accountId = 0, int siteId = 0)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetAllGuardsByDayAccountAndSiteId(date, accountId, siteId).OrderBy(x => x.Name);
                return new ResponseDataModel<IEnumerable<GuardModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Guard",
                    Data = data
                };
            }));
        }



        [HttpPost]
        [Route("Guard/Save")]
        public IHttpActionResult Save(GuardModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddGuard(model);
                return new ResponseDataModel<GuardModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Guard",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("Guard/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var success = _service.DeleteGuardById(id);
                var message = success ? "Successfully Deleted Guard" : "Cannot delete guard because it is associated to an incident.";
                return new ResponseDataModel<GuardModel>
                {
                    Success = success,
                    StatusCode = HttpStatusCode.OK,
                    Message = message,
                };
            }));
        }
    }
}
