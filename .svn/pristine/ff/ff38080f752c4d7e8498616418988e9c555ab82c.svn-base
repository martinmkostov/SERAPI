﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ITaskService
    {
        IEnumerable<MainCategoryModel> GetAllMainCategories();
        MainCategoryModel GetAllMainCategory(int id);
        SubCategoryModel GetSubCategory(int id);
        IEnumerable<SubCategoryModel> GetAllSubCategories();
        IEnumerable<SiteTaskModel> GetPublishedTask(int Id);
        IEnumerable<TaskModel> GetAllTasks();
        List<IGrouping<string, SiteTaskModel>> GetByUserId(int id);
        IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId);
        void AddTask(TaskModel model);
        void DeleteTask(int id);
        DeleteResultModel DeleteMainCategory(int id);
        DeleteResultModel DeleteSubCategory(int id);
        DeleteResultModel DeleteInterval(int id);

        ArchiveDataModel ArchiveTask(int id);
        ArchiveDataModel UnarchiveTask(int id);
        void AddMainCategory(MainCategoryModel model);
        void AddSubCategory(SubCategoryModel model);
        void Transfer(SiteTaskModel model);
        void PublishTask(SiteTaskModel model);
        void UnPublishTask(SiteTaskModel model);
        void Confirm(SiteTaskLogModel model);
        void ReAssign(SiteTaskModel model);
        void ConfirmMultiple(SiteTaskLogModel model);
        void PerformAction(SiteTaskLogModel model);
        TaskModel GetTask(int id);
        TaskModel GetTaskBySiteIdAndTaskId(int taskId,int siteId);
        IEnumerable<SiteTaskLogModel> GetHistory(int siteTaskId);
        SiteTaskLogModel GetSingleHistory(int id);
    }

    public class TaskService : ITaskService
    {
        private ITaskRepository _taskRepository;
        private IRepository<MainCategory> _mainCategoryRepository;
        private IRepository<SubCategory> _subCategoryRepository;
        private IRepository<TaskMainCategory> _taskMainCategory;
        private IRepository<TaskSubCategory> _taskSubCategory;
        //private IRepository<SiteTask> _siteTaskRepository;
        private ISiteTaskRepository _siteTaskRepository;
        private IRepository<SiteTaskLog> _siteTaskLogRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<Interval> _intervalRepository;
        private IRepository<User> _userRepository;
        private IRepository<Site> _siteRepository;
        private IRepository<UserModuleRoles> _userModuleRoleRepository;
        public TaskService()
        {
            _taskRepository = new TaskRepository();
            _mainCategoryRepository = new Repository<MainCategory>();
            _subCategoryRepository = new Repository<SubCategory>();
            _taskMainCategory = new Repository<TaskMainCategory>();
            _taskSubCategory = new Repository<TaskSubCategory>();
            _siteTaskRepository = new SiteTaskRepository();
            _siteTaskLogRepository = new Repository<SiteTaskLog>();
            _roleRepository = new Repository<Role>();
            _intervalRepository = new Repository<Interval>();
            _userRepository = new Repository<User>();
            _siteRepository = new Repository<Site>();
            _userModuleRoleRepository = new Repository<UserModuleRoles>();
        }

        public void AddMainCategory(MainCategoryModel model)
        {

            if (model.Id == 0)
            {
                _mainCategoryRepository.Save(new MainCategory()
                {
                    Name = model.Name
                });
            }
            else
            {
                var data = _mainCategoryRepository.Get(model.Id);
                data.Name = model.Name;

                _mainCategoryRepository.Update(data);
            }
           
        }

        public void AddSubCategory(SubCategoryModel model)
        {
            if (model.Id == 0)
            {
                _subCategoryRepository.Save(new SubCategory()
                {
                    Name = model.Name
                });
            }
            else
            {
                var data = _subCategoryRepository.Get(model.Id);
                data.Name = model.Name;

                _subCategoryRepository.Update(data);
            }

        }

        public void AddTask(TaskModel model)
        {
            if (model.Id == 0)
            {
                if (_taskRepository.GetByPredicate(r => r.TaskNo == model.TaskNo).Any())
                {
                    return;
                }

                _taskRepository.Save(new Data.Models.Task()
                {
                    TaskSubCategories = model.TaskSubCategories.Select(r => new TaskSubCategory()
                    {
                        SubCategoryId = r.SubCategoryId,
                    }).ToList(),
                    TaskMainCategory = model.TaskMainCategory.Select(r => new TaskMainCategory()
                    {
                        MainCategoryId = r.MainCategoryId
                    }).ToList(),
                    Description = model.Description,
                    Interval = model.Interval,
                    NuberOfDays = model.NuberOfDays,
                    Purpose = model.Purpose,
                    TaskNo = model.TaskNo,
                    RemindAfter = model.RemindAfter,
                    Title = model.Title,
                    RoleId = model.RoleId,
                    ExtensionsAllowed = model.ExtensionsAllowed,
                    DayToComplete = model.DayToComplete,
                    IsArchived = false,
                    RecommendedRole = model.RecommendedRole
                });
            }
            else
            {
                var task = _taskRepository.GetWithChildsIncluded(model.Id);
                task.Description = model.Description;
                task.Interval = model.Interval;
                task.NuberOfDays = model.NuberOfDays;
                task.Purpose = model.Purpose;
                task.IsArchived = false;
                task.TaskNo = model.TaskNo;
                task.Title = model.Title;
                task.RemindAfter = model.RemindAfter;
                task.RoleId = model.RoleId;
                task.ExtensionsAllowed = model.ExtensionsAllowed;
                task.DayToComplete = model.DayToComplete;
                task.RecommendedRole = model.RecommendedRole;
                task.TaskMainCategory.ToList().ForEach(x => 
                {
                    _taskMainCategory.Delete(x.Id);
                });

                task.TaskSubCategories.ToList().ForEach(x =>
                {
                    _taskSubCategory.Delete(x.Id);
                });

                model.TaskMainCategory.ForEach(x => 
                {
                    _taskMainCategory.Save(new TaskMainCategory()
                    {
                        MainCategoryId = x.MainCategoryId,
                        TaskId = task.Id
                    });
                });

                model.TaskSubCategories.ForEach(x =>
                {
                    _taskSubCategory.Save(new TaskSubCategory()
                    {
                        SubCategoryId = x.SubCategoryId,
                        TaskId = task.Id
                    });
                });


                _taskRepository.Update(task);
            }
        }

        public void Confirm(SiteTaskLogModel model)
        {
            var sitetasklog = _siteTaskLogRepository.GetByPredicate(x=>x.SiteTaskId== model.SiteTaskId);
            var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
            int instance = 0;
            if (sitetasklog.Any())
            {
                 instance = sitetasklog.Max(x => x.Instance) + 1;
            }

            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = DateTime.Now,
                LogType = (int)TaskLogType.Confirm,
                SiteTaskId = model.SiteTaskId,
                Instance = instance,
                Owner = model.Owner,
                IntervalDate = model.IntervalDate
            };

            _siteTaskLogRepository.Save(tasklog);
        }

        public void DeleteTask(int id)
        {
            _taskRepository.Delete(id);
        }

        public DeleteResultModel DeleteMainCategory(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskMainCategory.GetByPredicate(r => r.MainCategoryId == id);

            if (data.Any()) {
                model.IsDeleted = false;
                return model;
            }


            _mainCategoryRepository.Delete(id);
            return model;
        }

        public DeleteResultModel DeleteSubCategory(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskSubCategory.GetByPredicate(r => r.SubCategoryId == id);

            if (data.Any())
            {
                model.IsDeleted = false;
                return model;
            }


            _subCategoryRepository.Delete(id);
            return model;
        }

        public DeleteResultModel DeleteInterval(int id)
        {
            var model = new DeleteResultModel() { };
            model.IsDeleted = true;
            var data = _taskRepository.GetByPredicate(r => r.Interval == id);

            if (data.Any())
            {
                model.IsDeleted = false;
                return model;
            }


            _intervalRepository.Delete(id);
            return model;
        }

        public IEnumerable<MainCategoryModel> GetAllMainCategories()
        {
            return _mainCategoryRepository.GetAll().Select(x => new MainCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,                
            });
        }

        public IEnumerable<SubCategoryModel> GetAllSubCategories()
        {
            return _subCategoryRepository.GetAll().Select(x => new SubCategoryModel()
            {
                Id = x.Id,
                Name = x.Name,
            });
        }

        public IEnumerable<TaskModel> GetAllTaskBySiteId(int siteId)
        {
           var tasksites = _siteTaskRepository.GetByPredicate(x => x.SiteId == siteId && x.IsCurrentlyPublished).ToList();
            
            var models = new List<TaskModel>();

            tasksites.ForEach(x => 
            {
                
                var task = _taskRepository.GetWithChildsIncluded(x.TaskId);
                var role = _roleRepository.Get(task.RoleId);
                var tModel = new TaskModel()
                {
                    Id = x.TaskId,
                    Description = task.Description,
                    Interval = task.Interval,
                    NuberOfDays = task.NuberOfDays,
                    Purpose = task.Purpose,
                    TaskNo = task.TaskNo,
                    Title = task.Title,
                    RemindAfter = task.RemindAfter,
                    ExtensionsAllowed = task.ExtensionsAllowed,
                    DayToComplete = task.DayToComplete,
                    RecommendedRole = task.RecommendedRole,
                    TaskMainCategory = task.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                    {
                        Id = r.Id,
                        MainCategoryId = r.MainCategoryId,
                        TaskId = r.TaskId,
                        MainCategory = new MainCategoryModel
                        {
                            Name = r.MainCategory.Name
                        }
                    }).ToList(),
                    TaskSubCategories = task.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                    {
                        Id = r.Id,
                        SubCategoryId = r.SubCategoryId,
                        TaskId = r.TaskId,
                        SubCategory = new SubCategoryModel
                        {
                            Name = r.SubCategory.Name
                        }
                    }).ToList(),
                    RoleId = task.RoleId,
                    Role = task.RoleId != 0? new RoleModel()
                    {
                        Description= role.Description,
                        Id = role.Id,
                        Name = role.Name,                        
                    } : new RoleModel()
                };

                tModel.StartDate = x.StartDate;
                var logtype = (int)TaskLogType.Confirm;
                _siteTaskLogRepository.GetByPredicate(r=>r.SiteTaskId == x.Id && r.LogType == logtype).ToList().ForEach(r=> 
                {
                    var interval = _intervalRepository.Get(tModel.Interval);

                    switch ((IntervalEnum)interval.IntervalType)
                    {
                        case IntervalEnum.Day:
                                tModel.StartDate = tModel.StartDate.AddDays(interval.Value);
                            break;
                        case IntervalEnum.Month:
                                tModel.StartDate = tModel.StartDate.AddMonths(interval.Value);
                            break;
                    }

                    
                });              


                tModel.EndDate = tModel.StartDate.AddDays(tModel.NuberOfDays);

                var extensionLogType = (int)TaskLogType.ActionExtension;
                if (_siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == x.Id && r.LogType == extensionLogType).Any())
                {
                    var maxInstance = _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == x.Id).Max(r => r.Instance);
                    _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == x.Id && r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                    {
                        tModel.EndDate = tModel.EndDate.AddDays(r.ExtendDay);
                    });
                }
                
                if (tModel.StartDate > DateTime.Now)
                {
                    tModel.Status = "Ready";
                }
                else if (tModel.StartDate <= DateTime.Now)
                {
                    tModel.Status = "Ongoing";
                }

                if (tModel.EndDate <= DateTime.Now)
                {
                    tModel.Status = "Delayed";
                }

                models.Add(tModel);
            });

            return models;
        }

        public IEnumerable<TaskModel> GetAllTasks()
        {
            return _taskRepository.GetAllTasksChildIncluded().Select(x => new TaskModel()
            {
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                Id = x.Id,
                RoleId = x.RoleId,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete = x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                IsArchived = x.IsArchived
            }).ToList();
        }

        public IEnumerable<SiteTaskLogModel> GetHistory(int siteTaskId)
        {
            return _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == siteTaskId).OrderByDescending(r=>r.LogDate).Select(r=> new SiteTaskLogModel()
            {
                Comment = r.Comment,
                Id = r.Id,
                Instance = r.Instance,
                LogDate = r.LogDate,
                LogType = (TaskLogType)r.LogType,
                SiteTaskId = r.SiteTaskId,
                Owner = r.Owner,
                IntervalDate = r.IntervalDate
            }).ToList();
        }

        public SiteTaskLogModel GetSingleHistory(int id)
        {

            var r = _siteTaskLogRepository.Get(id);
            return new SiteTaskLogModel()
            {
                Comment = r.Comment,
                Id = r.Id,
                Instance = r.Instance,
                LogDate = r.LogDate,
                LogType = (TaskLogType)r.LogType,
                SiteTaskId = r.SiteTaskId,
                Owner = r.Owner,
                IntervalDate = r.IntervalDate
            };
        }

        public IEnumerable<SiteTaskModel> GetPublishedTask(int Id)
        {
            
            return _siteTaskRepository.GetByPredicate(x => x.TaskId == Id && x.IsCurrentlyPublished == true).Select(x=> new SiteTaskModel()
            {
                Id = x.Id,
                //InfoRoles = x.InfoRoles,
                Performer = x.Performer,
                PerformerId = x.PerformerId,
                SiteId = x.SiteId,
                TaskId = x.TaskId,
                StartDate = new SERApp.Models.Common.DateModel()
                {
                    day = x.StartDate.Day,
                    month = x.StartDate.Month,
                    year = x.StartDate.Year
                },
               
            }).ToList();

        }

        public TaskModel GetTask(int id)
        {
            var x = _taskRepository.GetWithChildsIncluded(id);
            var role = _roleRepository.Get(x.RoleId);

            var archivedData = _siteTaskRepository.GetByPredicate(r => r.TaskId == id).ToList();
            archivedData.ForEach(r => 
            {
                r.Site = _siteRepository.Get(r.SiteId);
            });

            return new TaskModel()
            {
                Id = x.Id,
                IsArchived = x.IsArchived,
                Description = x.Description,
                Interval = x.Interval,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete =x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                ArchivedData = archivedData.Select(r=> new SiteTaskModel()
                {
                    Site = new SiteModel()
                    {
                        Name = r.Site.Name
                    },
                    Performer = r.Performer,
                    PerformerId = r.PerformerId,
                    Id = r.Id,
                    StartDate = new SERApp.Models.Common.DateModel()
                    {
                        day = r.StartDate.Day,
                        month = r.StartDate.Month,
                        year = r.StartDate.Year
                    },
                    StartDateString = r.StartDate.ToString("yyyy-MM-dd"),
                    PublishDate = r.PublishDate
                }).ToList(),
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList(),
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList(),
                RoleId = x.RoleId,
                Role = x.RoleId != 0 ? new RoleModel()
                {
                    Description = role.Description,
                    Id = role.Id,
                    Name = role.Name,
                } : new RoleModel()
            };
        }

        public TaskModel GetTaskBySiteIdAndTaskId(int taskId, int siteId)
        {
            var x = _taskRepository.GetWithChildsIncluded(taskId);
            var role = _roleRepository.Get(x.RoleId);

            var task = new TaskModel()
            {
                Id = x.Id,
                Description = x.Description,
                Interval = x.Interval,
                CurrentServerDate = DateTime.Now,
                NuberOfDays = x.NuberOfDays,
                Purpose = x.Purpose,
                TaskNo = x.TaskNo,
                Title = x.Title,
                RemindAfter = x.RemindAfter,
                ExtensionsAllowed = x.ExtensionsAllowed,
                DayToComplete = x.DayToComplete,
                RecommendedRole = x.RecommendedRole,
                Role = new RoleModel
                {
                    Id = role.Id,
                    Name = role.Name
                },
                RoleId = x.RoleId,
                TaskMainCategory = x.TaskMainCategory.Select(r => new TaskMainCategoryModel()
                {
                    Id = r.Id,
                    MainCategoryId = r.MainCategoryId,
                    TaskId = r.TaskId,
                    MainCategory = new MainCategoryModel
                    {
                        Name = r.MainCategory.Name
                    }
                }).ToList(),
                TaskSubCategories = x.TaskSubCategories.Select(r => new TaskSubCategoryModel()
                {
                    Id = r.Id,
                    SubCategoryId = r.SubCategoryId,
                    TaskId = r.TaskId,
                    SubCategory = new SubCategoryModel
                    {
                        Name = r.SubCategory.Name
                    }
                }).ToList(),
            };

            var sitetask = _siteTaskRepository.GetByPredicate(v => v.SiteId == siteId && v.TaskId == taskId &&  v.IsCurrentlyPublished).SingleOrDefault();

            task.StartDate = sitetask.StartDate;
            var logtype = (int)TaskLogType.Confirm;
            _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == logtype).ToList().ForEach(r =>
            {
                var interval = _intervalRepository.Get(task.Interval);

                switch ((IntervalEnum)interval.IntervalType)
                {
                    case IntervalEnum.Day:
                        task.StartDate = task.StartDate.AddDays(interval.Value);
                        break;
                    case IntervalEnum.Month:
                        task.StartDate = task.StartDate.AddMonths(interval.Value);
                        break;
                }
            });
           
            task.EndDate = task.StartDate.AddDays(x.NuberOfDays);
          
            var extensionLogType = (int)TaskLogType.ActionExtension;
            bool hasInstance = false;
            var maxI = 0;
            if (_siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType).Any())
            {
                var maxInstance = _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id).Max(r => r.Instance);
                _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                {
                    task.EndDate = task.EndDate.AddDays(r.ExtendDay);
                });
                hasInstance = true;
                maxI = maxInstance;
            }
          
            task.SiteTaskId = sitetask.Id;

            if (task.StartDate > DateTime.Now)
            {
                task.Status = "Ready";
            }
            else if (task.StartDate <= DateTime.Now)
            {
                task.Status = "Ongoing";
            }

            if (task.EndDate <= DateTime.Now)
            {
                task.Status = "Delayed";
            }

            if (hasInstance)
            {
                int extensionsCount = 0;
                _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType && r.Instance == maxI).ToList().ForEach(r =>
                {
                    extensionsCount++;
                });

                task.RemainingExtensionsAllowed = task.ExtensionsAllowed - extensionsCount;
            }
            else {
                int extensionsCount = 0;
                _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType).ToList().ForEach(r =>
                {
                    extensionsCount++;
                });

                task.RemainingExtensionsAllowed = task.ExtensionsAllowed - extensionsCount;
            }

            if (DateTime.Now >= task.StartDate)
            {
                task.IsAllowedToAddExtension = true;
            }
            else {
                task.IsAllowedToAddExtension = false;
            }

            task.StartDateModel = new SERApp.Models.Common.DateModel()
            {
                day = task.StartDate.Day,
                month = task.StartDate.Month,
                year = task.StartDate.Year
            };

            var ma = _siteTaskRepository.Get(task.SiteTaskId);
            task.SiteTask = new SiteTaskModel()
            {
                Id = ma.Id,
                Performer = ma.Performer,
                PerformerId = ma.PerformerId,
                IsCurrentlyPublished = ma.IsCurrentlyPublished,
                TaskId = ma.TaskId,
                SiteId = ma.SiteId
            };

            return task;

        }

        public void PerformAction(SiteTaskLogModel model)
        {
            var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
            sitetask.Task = _taskRepository.Get(sitetask.TaskId);

            var data = _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id);
            var maxInstance = 0;
            if (data.Any())
            {
                maxInstance = data.Max(r => r.Instance);
            }
           

            if (model.LogType == TaskLogType.ActionExtension)
            {              
                var extensionLogType = (int)TaskLogType.ActionExtension;
                int extensionsCount = 0;

                if (data.Any())
                {
                    _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType && r.Instance == maxInstance).ToList().ForEach(r =>
                    {
                        extensionsCount++;
                    });
                }
                else
                {
                    _siteTaskLogRepository.GetByPredicate(r => r.SiteTaskId == sitetask.Id && r.LogType == extensionLogType).ToList().ForEach(r =>
                    {
                        extensionsCount++;
                    });
                }
              


                if (sitetask.Task.ExtensionsAllowed <= extensionsCount)
                {
                    return;
                }
            }
            
            var tasklog = new SiteTaskLog()
            {
                Comment = model.Comment,
                LogDate = DateTime.Now,
                LogType = (int)model.LogType,
                SiteTaskId = model.SiteTaskId,
                Instance = maxInstance,
                Owner = model.Owner,
                IntervalDate = model.IntervalDate,
                ExtendDay = model.ExtendDay
            };

            _siteTaskLogRepository.Save(tasklog);
        }

        public void PublishTask(SiteTaskModel model)
        {
            var user = _userRepository.Get(model.PerformerId);
            var role = _userModuleRoleRepository.GetByPredicate(r => r.ModuleId == 4 && r.UserId == user.Id && r.SiteId == model.SiteId).SingleOrDefault();
            role.Role = _roleRepository.Get(role.Id);
            _siteTaskRepository.Save(new SiteTask()
            {
                SiteId = model.SiteId,
                InfoRoles = model.InfoRoles,
                Performer = user.FirstName+ " "+user.LastName+" - "+role.Role.Name,
                PerformerId = model.PerformerId,
                StartDate = new DateTime(model.StartDate.year,model.StartDate.month,model.StartDate.day),
                TaskId = model.TaskId,
                IsCurrentlyPublished = true,
                PublishDate = DateTime.Now
            });
        }

        public void UnPublishTask(SiteTaskModel model)
        {
            var data = _siteTaskRepository.Get(model.Id);

            data.IsCurrentlyPublished = false;

            _siteTaskRepository.Update(data);
        }

        public void ConfirmMultiple(SiteTaskLogModel model)
        {
            var loop = model.loopConfirm;

            for (int w = 0; w < loop; w++) {

                var sitetasklog = _siteTaskLogRepository.GetByPredicate(x => x.SiteTaskId == model.SiteTaskId);
                var sitetask = _siteTaskRepository.Get(model.SiteTaskId);
                int instance = 0;
                if (sitetasklog.Any())
                {
                    instance = sitetasklog.Max(x => x.Instance) + 1;
                }

                var tasklog = new SiteTaskLog()
                {
                    Comment = model.Comment,
                    LogDate = DateTime.Now,
                    LogType = (int)TaskLogType.Confirm,
                    SiteTaskId = model.SiteTaskId,
                    Instance = instance,
                    Owner = model.Owner,
                    IntervalDate = model.IntervalDate
                };

                _siteTaskLogRepository.Save(tasklog);

                var d = GetTaskBySiteIdAndTaskId(sitetask.TaskId, sitetask.SiteId);
                model = new SiteTaskLogModel()
                {
                    Comment = model.Comment,
                    LogDate = DateTime.Now,
                    SiteTaskId = model.SiteTaskId,
                    LogType = TaskLogType.Confirm,
                    Instance = instance,
                    Owner = model.Owner,
                    IntervalDate = d.StartDate
                };
            }

        
        }

        public void ReAssign(SiteTaskModel model)
        {
            var data = _siteTaskRepository.Get(model.Id);
            data.Performer = model.Performer;
            data.PerformerId = model.PerformerId;
            data.StartDate = new DateTime(model.StartDate.year,model.StartDate.month,model.StartDate.day);

            _siteTaskRepository.Update(data);
        }

        public ArchiveDataModel ArchiveTask(int id)
        {
            var data = _siteTaskRepository.GetByPredicate(t => t.IsCurrentlyPublished && t.TaskId == id);

            if (data.Any())
            {
                return new ArchiveDataModel()
                {
                    IsArchived = false
                };
            }

            var task = _taskRepository.Get(id);
            task.IsArchived = true;
            _taskRepository.Update(task);


            return new ArchiveDataModel() { IsArchived = true };
        }

        public ArchiveDataModel UnarchiveTask(int id)
        {
            var data = _siteTaskRepository.GetByPredicate(t => t.IsCurrentlyPublished && t.TaskId == id);

            if (data.Any())
            {
                return new ArchiveDataModel()
                {
                    IsArchived = false
                };
            }

            var task = _taskRepository.Get(id);
            task.IsArchived = false;
            _taskRepository.Update(task);


            return new ArchiveDataModel() { IsArchived = false };
        }

        public MainCategoryModel GetAllMainCategory(int id)
        {
            var data = _mainCategoryRepository.Get(id);
            return new MainCategoryModel()
            {
                Id = data.Id,
                Name = data.Name
            };
        }

        public SubCategoryModel GetSubCategory(int id)
        {
            var data = _subCategoryRepository.Get(id);
            return new SubCategoryModel()
            {
                Id = data.Id,
                Name = data.Name,
            };
        }

        public List<IGrouping<string, SiteTaskModel>> GetByUserId(int id)
        {
            var data = _siteTaskRepository.GetAllSiteTasksChildIncluded().Where(r => r.PerformerId == id && r.IsCurrentlyPublished)
                .Select(x=> new SiteTaskModel()
                {
                    Id = x.Id,
                    InfoRoles = x.InfoRoles,
                    IsCurrentlyPublished = x.IsCurrentlyPublished,
                    IsPublished = x.IsCurrentlyPublished,
                    Performer = x.Performer,
                    PerformerId = x.PerformerId,
                    PublishDate = x.PublishDate,
                    Site = new SiteModel()
                    {
                        Name = x.Site.Name,
                        Id = x.Site.Id
                    },
                    SiteId = x.SiteId,
                    StartDate = new SERApp.Models.Common.DateModel()
                    {
                        day = x.StartDate.Day,
                        month = x.StartDate.Month,
                        year = x.StartDate.Year
                    },
                    TaskId = x.TaskId,
                    Task = new TaskModel()
                    {
                        Id = x.Task.Id,
                        IsArchived = x.Task.IsArchived,
                        CurrentServerDate = new DateTime(),
                        DayToComplete = x.Task.DayToComplete,
                        NuberOfDays = x.Task.NuberOfDays,
                        Title = x.Task.Title,
                        TaskNo = x.Task.TaskNo,
                        Interval = x.Task.Interval,
                        Description = x.Task.Description,
                        Purpose = x.Task.Purpose,
                        RoleId= x.Task.RoleId,
                        
                    }
                })
                .ToList();

            var model = data.GroupBy(v => v.Site.Name).ToList();
            
            return model;
        }

        public void Transfer(SiteTaskModel model)
        {
            var data = _siteTaskRepository.Get(model.Id);
            data.PerformerId = model.PerformerId;
            _siteTaskRepository.Update(data);
        }
    }
}
