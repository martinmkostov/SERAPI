import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { ToastsManager } from 'ng2-toastr';

import { UserService } from '../../services';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { BaseComponent } from '../../core/components';
@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class UsersListComponent extends BaseComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public accountId: number = 0;
    public model: any;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;

    public selectedAdminId: number = 0;

    constructor(private router: Router,
        private route:ActivatedRoute, 
        private location: Location,
        private toastr: ToastsManager,
        private userService: UserService,
        private modalService: NgbModal) {
            super(location, router, route);
        }
    
    packageData: Subject<any> = new Subject<any[]>();  
    dataList : Array<any> = [];

    public ngOnInit() {
        if(this.isSuperAdmin)
        {
            this.loadData();
        }
    }

    public loadData(page?: number, id?: number, filter: string = ""): void{
        this.showSpinner = true;
        if (page != null) {
            this.currentPage = page; 
        } 
        if(id > 0)
        {
            this.loadDataById(id);
        }
        else {
            this.userService.getUsersList(this.currentPage, this.itemPerPage)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                let data = response.Data.filter((item)=>{
                    return item.RoleName === 'admin';
                });
                this.packageData.next(data);
                if(filter !== '')
                {
                    this.dataList = data.filter((item) => {
                        return item.Account.Name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                    });
                }        
                this.totalItems = response.TotalRecords;
                //this.toastr.success(response.Message, "Success");
            });
        }
    }
   
    public loadDataById(id:number){
        this.isAdminOrUserEditor = true;
        this.accountId = id;
        this.showSpinner = true;
        this.userService.getUsersByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public editUser(id:number){
        this.router.navigate([`users/${id}/edit`]);  
    }

    public editUserRoleModules(id:number){
        //users/:id/module-roles/:account
        this.router.navigate([`users/${id}/module-roles/${this.accountId}`]); 
    }

    public editAccountUser(id:number){
        if(this.isSuperAdmin)
        {
            this.router.navigate([`accounts/${id}/edit/users`]);  
        }
    }

    public confirmLoginAsUser(content: string, adminId: number){
        this.modalService.open(content, { size: 'sm' });
        this.selectedAdminId = adminId;
    }

    public loginAsAdminUser(){
        this.superAdminAsAdmin(this.selectedAdminId);
    }

    public superAdminAsAdmin(adminId: number){
        this.showSpinner = true;
        //make sure to clear the secondary admin data before adding a new one
        localStorage.removeItem('secondaryAdminUser');
        this.userService.getUser(adminId)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.model = response.Data;
            localStorage.setItem('secondaryAdminUser', JSON.stringify(response.Data));
            const url = `/accounts/${this.model.AccountId}/edit?superAdminAccess=1&accountId=${this.model.AccountId}`
            super.setLastUrl(url);
            window.open(url);
        });
    }
}
