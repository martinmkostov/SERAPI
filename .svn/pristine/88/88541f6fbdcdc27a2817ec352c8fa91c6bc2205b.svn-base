﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Models.Enums;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class UserService
    {
        UserRepository _userRepository;
        UserModuleRoleRepository _userRoleRepository;
        CommonRepository _commonRepository;
        AccountRepository _accountRepository;
        LogRepository _logRepository;
        public UserService() {
            _userRepository = new UserRepository();
            _userRoleRepository = new UserModuleRoleRepository();
            _commonRepository = new CommonRepository();
            _accountRepository = new AccountRepository();
            _logRepository = new LogRepository();
        }
        private UserModel CreateAuth(UserModel model) {
            AuthenticationService authService = new AuthenticationService();
            return authService.CreateAuth(model.Password);
        }

        public UserModel Get(int id) {
            try {
                var user = _userRepository.Get(id);
                return entityToModel(user);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, id);
                throw ex;
            }
            
        }

        public List<UserModel> GetByAccountId(int id) {
            try
            {
                return _userRepository.GetAll()
                .Where(u => u.AccountId == id)
                .Select(u => new UserModel()
                {
                    Id = u.Id,
                    IsActive = u.IsActive,
                    IsDeleted = u.IsDeleted,
                    CreatedDate = u.CreatedDate,
                    LastUpdatedDate = u.LastUpdatedDate,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Mobile = u.Mobile,
                    PhoneNumber = u.PhoneNumber,
                    EmailAddress = u.EmailAddress,
                    UserName = u.UserName,
                    AccountId = u.AccountId,
                    Account = entityToModel(u.Account),
                    Role = entityToModel(_userRoleRepository.GetUserRole(u.Id))
                }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, id);
                return null;
            }
            
        }

        public List<UserModel> GetAll()
        {
            try
            {
                return _userRepository.GetAll()
                .Select(u => new UserModel()
                {
                    Id = u.Id,
                    IsActive = u.IsActive,
                    IsDeleted = u.IsDeleted,
                    CreatedDate = u.CreatedDate,
                    LastUpdatedDate = u.LastUpdatedDate,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Mobile = u.Mobile,
                    PhoneNumber = u.PhoneNumber,
                    EmailAddress = u.EmailAddress,
                    UserName = u.UserName,
                    AccountId = u.AccountId,
                    Account = entityToModel(u.Account),
                    RoleName = entityToModel(_userRoleRepository.GetUserRole(u.Id)).Name
                }).ToList();
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.GetAll, 0, 0);
                return null;
            }
            
        }

        public UserModel SaveUser(UserModel model) {
            try
            {
                if (_userRepository.Get(model.UserName) != null && _userRepository.Get(model.Id).UserName != model.UserName)
                {
                    throw new Exception("Could not save this user since it's already been taken. Choose another username.");
                }
                if (_userRepository.Get(model.Id) == null)
                {
                    CreateUser(model);
                    _logRepository.Log(LogTypeEnum.Information,
                        LogMessagingSettings.Save,
                        LogMessagingSettings.MessageCreateUser, model.AccountId, 0);

                }

                if (_userRepository.Get(model.Id) == null || !string.IsNullOrEmpty(model.Password))
                {
                    string tempPass = Security.GenerateTempPassword();
                    model.Password = !string.IsNullOrEmpty(model.Password) ? model.Password : tempPass;
                    UserModel authModel = CreateAuth(model);
                    model.HashedPassword = authModel.HashedPassword;
                    model.RandomSecret = authModel.RandomSecret;
                }
                var newModel = _userRepository.SaveUser(model);
                if (_userRepository.Get(model.Id) == null)
                {
                    Email.SendCreateUserConfirmation(newModel);
                }
                return newModel;
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, model.AccountId, 0);
                throw ex;
            }
        }

        private UserModel CreateUser(UserModel model) {
            //for now, should always be a user
            var role = _commonRepository.GetRoleByName("user");
            model.RoleId = role.Id;
            model.RoleName = role.Name;
            model.Account = entityToModel(_accountRepository.Get(model.AccountId));
            return model;
        }

        public UserModel CreateAdminUser(AccountModel accountModel) {
            try
            {
                string tempPass = Security.GenerateTempPassword();
                var role = _commonRepository.GetRoleByName("admin");
                var model = new UserModel()
                {
                    EmailAddress = accountModel.EmailAddress,
                    AccountId = accountModel.Id,
                    UserName = "admin_" + accountModel.Name.ToLower().Replace(" ", "_"),
                    Password = tempPass, //candidate for change
                    RoleId = role.Id,
                    RoleName = role.Name,
                    Account = accountModel
                };

                UserModel authModel = CreateAuth(model);
                model.HashedPassword = authModel.HashedPassword;
                model.RandomSecret = authModel.RandomSecret;
                return _userRepository.SaveUser(model);
                //NO NEED FOR LOGGING, CREATE ADMIN USER LOG IS DONE ON THE EMAIL SECTION

            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Create, 0, 0);
                return null;
            }
           
        }

        public void DeleteUser(int id)
        {
            try
            {
                _userRepository.DeleteUser(id);
                _logRepository.Log(LogTypeEnum.Notification,
                    LogMessagingSettings.Delete,
                    LogMessagingSettings.MessageDeleteUser, 0, id);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Delete, 0, id);
            }
        }
        //@TODO : have a common service for this one
        public static RoleModel entityToModel(Role entity) {
            return new RoleModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                IsModule = entity.IsModule,
                IsUser = entity.IsUser
            };
        }

        public static AccountModel entityToModel(Account entity)
        {
            return new AccountModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Company = entity.Company,
                EmailAddress = entity.EmailAddress,
                IsActive = entity.IsActive,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate
            };
        }

        public UserModel entityToModel(User entity)
        {
            return new UserModel()
            {
                Id = entity.Id,
                IsActive = entity.IsActive,
                IsDeleted = entity.IsDeleted,
                CreatedDate = entity.CreatedDate,
                LastUpdatedDate = entity.LastUpdatedDate,
                FirstName = entity.FirstName,
                EmailAddress = entity.EmailAddress,
                LastName = entity.LastName,
                Mobile = entity.Mobile,
                PhoneNumber = entity.PhoneNumber,
                UserName = entity.UserName,
                AccountId = entity.AccountId,
                Account = entityToModel(entity.Account),
                Role = entityToModel(_userRoleRepository.GetUserRole(entity.Id))
            };
        }
    }
}
