﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ITagService
    {
        IEnumerable<TagModel> GetTags();
        bool AddTag(TagModel model);
        void DeleteTagById(int id);
        TagModel GetTagById(int id);
    }
    public class TagService : ITagService
    {
        private IRepository<Tag> _repository;
        public TagService()
        {
            _repository = new Repository<Tag>();
        }
        public bool AddTag(TagModel model)
        {
            try
            {
                _repository.Save(new Tag()
                {
                    Description = model.Description,
                    RFIDData = model.RFIDData,
                    TenantID = model.TenantID
                });
                return true;
            } catch(Exception ex)
            {
                return false;
            }       
        }

        public void DeleteTagById(int id)
        {
            _repository.Delete(id);
        }

        public TagModel GetTagById(int id)
        {
            var existingData = _repository.Get(id);
            return new TagModel()
            {
                Description = existingData.Description,
                Id = existingData.Id,
                RFIDData = existingData.RFIDData,
                TenantID = existingData.TenantID
            };
        }

        public IEnumerable<TagModel> GetTags()
        {
            return _repository.GetAll().Select(x => new TagModel()
            {
                Description = x.Description,
                Id = x.Id,
                RFIDData = x.RFIDData,
                TenantID = x.TenantID               
            }).ToList();
        }
    }
}
