﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

/// <summary>
/// https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/microservice-ddd-cqrs-patterns/enumeration-classes-over-enum-types
/// </summary>
public abstract class Enumeration<T> : IComparable
{
    [Key]
    //[MaxLength(50)]
    public virtual T Id { get; set; }
    //[MaxLength(250)]
    public virtual string Name { get; set; }
    //public virtual int SortOrder { get; set; }
    //public virtual bool IsActive { get; set; }

    protected Enumeration()
    {
    }

    protected Enumeration(T id, string name)
    {
        Id = id;
        Name = name;
        //SortOrder = 1;
        //IsActive = true;
    }

    //protected Enumeration(T id, string name, int sortOrder, bool isActive)
    //{
    //    Id = id;
    //    Name = name;
    //    SortOrder = sortOrder;
    //    IsActive = isActive;
    //}

    public override string ToString()
    {
        return Name;
    }


    private static readonly Dictionary<string, IEnumerable<object>> ListDictionary = new Dictionary<string, IEnumerable<object>>();
    public static List<TT> GetAll<TT>() where TT : Enumeration<T>, new()
    {
        var type = typeof(TT);
        var typeName = type.Name;

        if (ListDictionary.ContainsKey(typeName))
            return (ListDictionary.SingleOrDefault(x => x.Key.Equals(typeName)).Value as List<TT>);

        var fields = type.GetTypeInfo().GetMembers(BindingFlags.Public |
                                                   BindingFlags.Static |
                                                   BindingFlags.DeclaredOnly);
        var list = new List<TT>();
        foreach (var info in fields)
        {
            var instance = new TT();
            if (GetValue(info, instance) is TT locatedValue)
            {
                list.Add(locatedValue);
            }
        }

        ListDictionary.Add(typeName, list);

        return list;
    }

    public static object GetValue(MemberInfo memberInfo, object forObject)
    {
        switch (memberInfo.MemberType)
        {
            case MemberTypes.Field:
                return ((FieldInfo)memberInfo).GetValue(forObject);
            case MemberTypes.Property:
                return ((PropertyInfo)memberInfo).GetValue(forObject);
            default:
                return null;
        }
    } 

    public override bool Equals(object obj)
    {
        var otherValue = obj as Enumeration<T>;
        if (otherValue == null)
        {
            return false;
        }
        var typeMatches = GetType() == obj.GetType();
        var valueMatches = Id.Equals(otherValue.Id);
        return typeMatches && valueMatches;
    }

    public int CompareTo(object other)
    {
        return string.Compare(Id.ToString(), ((Enumeration<T>)other).Id.ToString(), StringComparison.OrdinalIgnoreCase);
    }

    public static TT GetEnumerationById<TT>(string id) where TT : Enumeration<T>, new()
    {
        return id == null ? null : GetAll<TT>().SingleOrDefault(x => x.Id.Equals(id));
    }
    
    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
    // Other utility methods ...
}