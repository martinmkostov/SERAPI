﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data
{
    public class BimControlItemImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int BimControlItemId { get; set; }
        public virtual BimControlItem BimControlItem { get; set; }
    }
}
