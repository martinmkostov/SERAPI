﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Tenant
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string EmailTemplate { get; set; }
        public string Image { get; set; }

        public int? PermiseType { get; set; }
        [ForeignKey(nameof(PermiseType))]
        public PremiseType PremiseTypeEnt { get; set; }

        public string VisitingAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string InsuranceCompany { get; set; }
        public string SecurityCompany { get; set; }
        public string SiteManagerName { get; set; }
        public string SiteManagerEmail { get; set; }
        public string SiteManagerMobilePhone { get; set; }
        public string SiteManagerPhone { get; set; }
        public string ContractName { get; set; }
        public string ContractVatNo { get; set; }
        public string InvoiceAddress { get; set; }
        public string InvoiceZipAndCity { get; set; }
        public string ContactComments { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool? CCTV { get; set; }
        public bool? IntrusionAlarm { get; set; }
        public bool Flag { get; set; }
        public string PropertyDesignation { get; set; }
        public bool UseWrittenStatement { get; set; }

        public int? TenantType { get; set; }
        [ForeignKey(nameof(TenantType))]
        public TenantType TenantTypeEnt { get; set; }

        public int? SubPremiseType { get; set; }
        [ForeignKey(nameof(SubPremiseType))]
        public SubPremiseType SubPremiseTypeEnt { get; set; }

        [ForeignKey(nameof(SiteId))]
        public virtual Site Site { get; set; }

        public string City { get; set; }
        public string ZipCode { get; set; }

        public string PostAddress { get; set; }
        public string PostZipCode { get; set; }
        public string PostCity { get; set; }

        public DateTime? LastUpdatedByCustomer { get; set; }
        public int LastUpdatedBy { get; set; }

        public DateTime? LastSentUpdateEmail { get; set; }
    }

    public class TenantType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }

        public ICollection<Type3TenantType> Type3TenantTypes { get; set; }
    }

    public class PremiseType
    {
        public int Id { get; set; }
        public int TenantTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
    }

    public class JobTitle
    {
        public int Id { get; set; }
        public string TitleName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IncludedOnExcelExport { get; set; }
    }

    public class SubPremiseType
    {
        public int Id { get; set; }
        public int PremiseTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
    }
}
