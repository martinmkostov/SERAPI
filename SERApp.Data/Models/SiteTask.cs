﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class SiteTask
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int TaskId { get; set; }
        public string Performer { get; set; }
        public int PerformerId { get; set; }
        public DateTime StartDate { get; set; }
        public string InfoRoles { get; set; }
        public DateTime PublishDate { get; set; }

        public virtual Site Site { get; set; }
        public virtual Task Task { get; set; }

        public bool IsCurrentlyPublished { get; set; }
    }
}
