﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IssueRecipientTypes
    {
        public int IssueRecipientTypeId { get; set; }
        public string Name { get; set; }
        public bool Hidden { get; set; }
    }
}
