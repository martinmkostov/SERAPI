using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Customer
    {
        public Customer()
        {
            this.Loans = new List<Loan>();
        }

        public int Id { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public int AccountId { get; set; }
        public virtual ICollection<Loan> Loans { get; set; }
    }
}
