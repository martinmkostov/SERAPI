﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class ContractLog
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
