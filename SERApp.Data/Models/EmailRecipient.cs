﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class EmailRecipient
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int ModuleId { get; set; }
        public string Recipient { get; set; }
        public DateTime SentDate { get; set; }
    }
}
