﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IncidentAccidentComment
    {
        public int Id { get; set; }
        public int IncidentAccidentId { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }
        public int Requirements { get; set; }
        public int Disbursed { get; set; }
        public int CompensationFromInsurance { get; set; }
        public int CompensationFromOthers { get; set; }
        public string Name { get; set; }
    }
}
