﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class RyckrapportData
    {
        public int RyckrapportDataId { get; set; }
        public int ReportId { get; set; }
        public int TenantId { get; set; }
        //public int SectionId { get; set; }
        public DateTime Time { get; set; }

        public virtual Report Report { get; set; }
        public virtual ICollection<RyckrapportDataSection> RyckrapportDataSections { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
