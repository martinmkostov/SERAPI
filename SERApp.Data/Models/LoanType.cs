using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class LoanType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IsPhotoRequired { get; set; }
        public bool ShowReturnDate { get; set; }
    }
}
