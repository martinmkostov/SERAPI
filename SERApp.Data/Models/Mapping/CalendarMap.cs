﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class CalendarMap : EntityTypeConfiguration<Calendar>
    {
        public CalendarMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("Calendars");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Desription).HasColumnName("Desription");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.CalendarDate).HasColumnName("CalendarDate");
            this.Property(t => t.CalendarTime).HasColumnName("CalendarTime");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");

        }
    }
}
