using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class SiteMap : EntityTypeConfiguration<Site>
    {
        public SiteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.Property(t => t.Name)
                .HasMaxLength(150);

            this.Property(t => t.Address)
                .HasMaxLength(150);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.PostalCode)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.AdditionalLocationInfo)
                .HasMaxLength(500);

            this.Property(t => t.PhoneNumber)
                .HasMaxLength(50);

            this.Property(t => t.FaxNumber)
                .HasMaxLength(50);

            this.Property(t => t.ContactPerson)
                .HasMaxLength(250);

            this.Property(t => t.ContactPersonNumber)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Sites");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.PostalCode).HasColumnName("PostalCode");
            this.Property(t => t.AdditionalLocationInfo).HasColumnName("AdditionalLocationInfo");
            this.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");
            this.Property(t => t.FaxNumber).HasColumnName("FaxNumber");
            this.Property(t => t.ContactPerson).HasColumnName("ContactPerson");
            this.Property(t => t.ContactPersonNumber).HasColumnName("ContactPersonNumber");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.Guid).HasColumnName("Guid");

            this.Property(t => t.FacebookLink).HasColumnName("FacebookLink");
            this.Property(t => t.InstagramLink).HasColumnName("InstagramLink");
            this.Property(t => t.YoutubeLink).HasColumnName("YoutubeLink");
            this.Property(t => t.PublicEmailAddress).HasColumnName("PublicEmailAddress");
            this.Property(t => t.PublicContactNumber).HasColumnName("PublicContactNumber");


            this.Property(t => t.CompanyLogo).HasColumnName("CompanyLogo");
            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.Sites)
                .HasForeignKey(d => d.AccountId);
            
            
        }
    }
}
