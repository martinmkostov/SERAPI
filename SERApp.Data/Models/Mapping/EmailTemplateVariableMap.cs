﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class EmailTemplateVariableMap : EntityTypeConfiguration<EmailTemplateVariables>
    {

        public EmailTemplateVariableMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("EmailTemplateVariables");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.VariableName).HasColumnName("VariableName");
            this.Property(t => t.VariableData).HasColumnName("VariableData");
            this.Property(t => t.EmailTemplateId).HasColumnName("EmailTemplateId");

            this.HasRequired(x => x.EmailTemplate)
                .WithMany(x => x.EmailTemplateVariables)
                .HasForeignKey(x => x.EmailTemplateId);
        }
    }
}
