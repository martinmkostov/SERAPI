﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IssueMap : EntityTypeConfiguration<Issue>
    {
        public IssueMap()
        {
            // Primary Key
            this.HasKey(t => t.IssueId);

            // Table & Column Mappings
            this.ToTable("Issues");
            this.Property(t => t.IssueId).HasColumnName("IssueId");




            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.Tenant).HasColumnName("Tenant");
            this.Property(t => t.Yta).HasColumnName("Yta");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.ApartmentNumber).HasColumnName("ApartmentNumber");
            this.Property(t => t.Typ).HasColumnName("Typ");

            this.Property(t => t.Image).HasColumnName("Image");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsPolice).HasColumnName("IsPolice");
            

        }
    }
}
