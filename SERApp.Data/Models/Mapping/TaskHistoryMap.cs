﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    

    public class TaskHistoryMap : EntityTypeConfiguration<TaskHistory>
    {
        public TaskHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("TaskHistory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TaskId).HasColumnName("TaskId");
            this.Property(t => t.Actions).HasColumnName("Actions");
            this.Property(t => t.LogDateTime).HasColumnName("LogDateTime");
            this.Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
