﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class TenantTypeMap : EntityTypeConfiguration<TenantType>
    {
        public TenantTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("TenantTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
