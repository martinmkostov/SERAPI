﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SERApp.Data.Models.Mapping
{
    public class ContractTagWordMap : EntityTypeConfiguration<ContractTagWord>
    {
        public ContractTagWordMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ContractTagWord");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ContractId).HasColumnName("ContractId");
            this.Property(t => t.TagWordId).HasColumnName("TagWordId");


            // Relationships
            this.HasRequired(t => t.Contract).WithMany().HasForeignKey(x => x.TagWordId);
            this.HasRequired(t => t.TagWord).WithMany().HasForeignKey(x => x.TagWordId);
         
        }
    }
}
