﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class SectionMap : EntityTypeConfiguration<Section>
    {
        public SectionMap()
        {
            // Primary Key
            this.HasKey(t => t.SectionId);

            // Table & Column Mappings
            this.ToTable("Section");
            this.Property(t => t.SectionId).HasColumnName("SectionId");
            this.Property(t => t.SectionValue).HasColumnName("SectionValue");
            this.Property(t => t.SiteId).HasColumnName("SiteId");

            //this.HasRequired(x => x.RyckrapportData)
            //    .WithMany(x => x.Sections)
            //    .HasForeignKey(x => x.RyckrapportDataId);
        }
    }
}
