﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class FieldTypeMap : EntityTypeConfiguration<FieldType>
    {
        public FieldTypeMap()
        {
            this.HasKey(t => t.PkFieldTypeId);

            this.ToTable("FieldType");

            this.Property(t => t.Flag)
                .HasMaxLength(250);

            this.Property(t => t.Name)
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(5000);

            this.ToTable("FieldType");
            this.Property(t => t.PkFieldTypeId).HasColumnName("PkFieldTypeId");
            this.Property(t => t.Flag).HasColumnName("Flag");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
