﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    class SiteFolderRightMap :  EntityTypeConfiguration<SiteFolderRight>
    {
        public SiteFolderRightMap()
        {
            this.HasKey(t => t.Id);

            this.ToTable("SiteFolderRights");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.Folder).HasColumnName("Folder");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");

        }
    }
}
