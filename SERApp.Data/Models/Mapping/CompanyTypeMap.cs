﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class CompanyTypeMap : EntityTypeConfiguration<CompanyType>
    {
        public CompanyTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("CompanyTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
