﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class Type3TenantTypeMap : EntityTypeConfiguration<Type3TenantType>
    {
        public Type3TenantTypeMap()
        {
            this.HasKey(e => e.Id);

            this.ToTable("Type3TenantTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type3Id).HasColumnName("Type3Id");
            this.Property(t => t.TenantTypeId).HasColumnName("TenantTypeId");

            this.HasRequired(t => t.Type3)
                .WithMany(t => t.Type3TenantTypes)
                .HasForeignKey(t => t.Type3Id);

            this.HasRequired(t => t.TenantType)
                .WithMany(t => t.Type3TenantTypes)
                .HasForeignKey(t => t.TenantTypeId);
        }
    }
}
