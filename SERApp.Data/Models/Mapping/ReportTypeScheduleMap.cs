﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ReportTypeScheduleMap : EntityTypeConfiguration<ReportTypeSchedule>
    {
        public ReportTypeScheduleMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Schedule)
                .HasMaxLength(500);

            this.ToTable("ReportTypeSchedule");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.JobId).HasColumnName("JobId");
            this.Property(t => t.Schedule).HasColumnName("Schedule");
            this.Property(t => t.BaseUTCOffset).HasColumnName("BaseUTCOffset");
            this.Property(t => t.TimeZone).HasColumnName("TimeZone");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.DataToSend).HasColumnName("DataToSend");
        }
    }
}
