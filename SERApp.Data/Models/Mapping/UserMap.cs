using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(150);

            this.Property(t => t.LastName)
                .HasMaxLength(150);

            this.Property(t => t.Mobile)
                .HasMaxLength(20);

            this.Property(t => t.PhoneNumber)
                .HasMaxLength(20);

            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.HashedPassword)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.RandomSecret)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Users");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress"); 
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.HashedPassword).HasColumnName("HashedPassword");
            this.Property(t => t.RandomSecret).HasColumnName("RandomSecret");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.DepartmentId).HasColumnName("DepartmentId");
            this.Property(t => t.IsActivityAdmin).HasColumnName("IsActivityAdmin");
            
            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.AccountId);

        }
    }
}
