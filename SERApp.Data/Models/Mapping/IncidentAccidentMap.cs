﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IncidentAccidentMap : EntityTypeConfiguration<IncidentAccident>
    {
        public IncidentAccidentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IncidentAccident");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.Affected).HasColumnName("Affected");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.ProbableCause).HasColumnName("ProbableCause");
            this.Property(t => t.Damages).HasColumnName("Damages");
            this.Property(t => t.Reporter).HasColumnName("Reporter");
            this.Property(t => t.SkadetypId).HasColumnName("SkadetypId");
            this.Property(t => t.EstimatedCost).HasColumnName("EstimatedCost");

            this.Property(t => t.Status).HasColumnName("Status");

            this.Property(t => t.InternalCaseNumber).HasColumnName("InternalCaseNumber");
            this.Property(t => t.ExternalCaseNumber).HasColumnName("ExternalCaseNumber");

            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Phone).HasColumnName("Phone");

            this.Property(t => t.Cost).HasColumnName("Cost");
        }
    }
}
