﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class DailyReportFieldMap : EntityTypeConfiguration<DailyReportFields>
    {
        public DailyReportFieldMap()
        {
            this.HasKey(e => e.Id);
            
            this.Property(e => e.DefaultLabel)
                .HasMaxLength(500);

            this.ToTable("DailyReportFields");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FieldTypeId).HasColumnName("FieldTypeId");
            this.Property(t => t.DefaultLabel).HasColumnName("DefaultLabel");
            this.Property(t => t.DefaultDataSource).HasColumnName("DefaultDataSource");
            this.Property(t => t.DefaultValue).HasColumnName("DefaultValue");
            this.Property(t => t.DefaultIsRequired).HasColumnName("DefaultIsRequired");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.CustomEditorName).HasColumnName("CustomEditorName");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.ControlName).HasColumnName("ControlName");
            this.Property(t => t.DefaultCustomScript).HasColumnName("DefaultCustomScript");
            this.Property(t => t.ControlGroup).HasColumnName("ControlGroup");

            this.HasRequired(t => t.FieldType)
                .WithMany(t => t.DailyReportFields)
                .HasForeignKey(t => t.FieldTypeId);
        }
    }
}
