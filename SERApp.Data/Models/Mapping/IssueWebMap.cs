﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IssueWebMap : EntityTypeConfiguration<IssueWeb>
    {
        public IssueWebMap()
        {
            // Primary Key
            this.HasKey(t => t.IssueWebId);

            // Properties
            // Table & Column Mappings
            this.ToTable("IssueWeb");
            this.Property(t => t.IssueWebId).HasColumnName("IssueWebId");

            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateChecked).HasColumnName("DateChecked");
            this.Property(t => t.DateFinished).HasColumnName("DateFinished");

            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.TenantId).HasColumnName("TenantId");
            this.Property(t => t.IssueType1Id).HasColumnName("IssueType1Id");
            this.Property(t => t.IssueType2Id).HasColumnName("IssueType2Id");
            this.Property(t => t.Email).HasColumnName("Email").IsUnicode(false);
            this.Property(t => t.Phone).HasColumnName("Phone").IsUnicode(false);
            this.Property(t => t.ContactName).HasColumnName("ContactName").IsUnicode(false);
            this.Property(t => t.Address).HasColumnName("Address").IsUnicode(false);
            this.Property(t => t.ApartmentNumber).HasColumnName("ApartmentNumber").IsUnicode(false);
            this.Property(t => t.Image).HasColumnName("Image").IsUnicode(false);
            this.Property(t => t.Image2).HasColumnName("Image2").IsUnicode(false);
            this.Property(t => t.Image3).HasColumnName("Image3").IsUnicode(false);
            this.Property(t => t.Image4).HasColumnName("Image4").IsUnicode(false);
            this.Property(t => t.Image5).HasColumnName("Image5").IsUnicode(false);
            
            this.Property(t => t.Description).HasColumnName("Description").IsUnicode(false);
            this.Property(t => t.IsPolice).HasColumnName("IsPolice");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ReminderEmailSentCount).HasColumnName("ReminderEmailSentCount");
            this.Property(t => t.IssueNumber).HasColumnName("IssueNumber");
            this.Property(t => t.Guid).HasColumnName("Guid");

            this.HasRequired(t => t.Site);
            this.HasRequired(t => t.Tenant);
            this.HasRequired(t => t.IssueType1);
            this.HasRequired(t => t.IssueType2);


        }
    }
}
