﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    class IssueWebHistoryMap : EntityTypeConfiguration<IssueWebHistory>
    {

        public IssueWebHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.IssueWebHistoryId);

            // Properties
            // Table & Column Mappings
            this.ToTable("IssueWebHistory");
            this.Property(t => t.IssueWebHistoryId).HasColumnName("IssueWebHistoryId");
            this.Property(t => t.ReactedBy).HasColumnName("ReactedBy");
            this.Property(t => t.Minutes).HasColumnName("Minutes");
            this.Property(t => t.MaterialCost).HasColumnName("MaterialCost");
            this.Property(t => t.DescriptionExternal).HasColumnName("DescriptionExternal");
            this.Property(t => t.DescriptionInternal).HasColumnName("DescriptionInternal");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.IsWarranty).HasColumnName("IsWarranty");
            this.Property(t => t.IsRent).HasColumnName("IsRent");
            this.Property(t => t.IsDamage).HasColumnName("IsDamage");
            this.Property(t => t.IsPrivate).HasColumnName("IsPrivate");

            this.Property(t => t.IssueWebHistoryType).HasColumnName("IssueWebHistoryType");

            this.Property(t => t.IssueWebId).HasColumnName("IssueWebId");

        }
    }
}
