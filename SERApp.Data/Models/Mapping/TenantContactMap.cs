﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class TenantContactMap : EntityTypeConfiguration<TenantContact>
    {
        public TenantContactMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("TenantContacts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.TenantId).HasColumnName("TenantId");
            this.Property(t => t.JobTypeId).HasColumnName("JobTypeId");
        }
    }
}
