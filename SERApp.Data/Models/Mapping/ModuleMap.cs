using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class ModuleMap : EntityTypeConfiguration<Module>
    {
        public ModuleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ShortName)
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .HasMaxLength(150);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.NodeLevel)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Modules");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ShortName).HasColumnName("ShortName");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.NodeLevel).HasColumnName("NodeLevel");
            this.Property(t => t.IsDefault).HasColumnName("IsDefault");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
