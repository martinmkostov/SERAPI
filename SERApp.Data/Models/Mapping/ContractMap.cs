﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ContractMap : EntityTypeConfiguration<Contract>
    {
        public ContractMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("Contracts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ContractNo).HasColumnName("ContractNo");
            this.Property(t => t.LegalHolderId).HasColumnName("LegalHolderId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.OpponentHolder).HasColumnName("OpponentHolder");
            this.Property(t => t.ValidFrom).HasColumnName("ValidFrom");
            this.Property(t => t.ValidTo).HasColumnName("ValidTo");
            this.Property(t => t.RemindDate).HasColumnName("RemindDate");
            this.Property(t => t.CancelLatest).HasColumnName("CancelLatest");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.FileDir).HasColumnName("FileDir");
            this.Property(t => t.ChangedBy).HasColumnName("ChangedBy");
            this.Property(t => t.ChangedTime).HasColumnName("ChangedTime");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.AmountYearly).HasColumnName("AmountYearly");
            this.Property(t => t.InformationText).HasColumnName("InformationText");
            this.Property(t => t.AmountYearlyDK).HasColumnName("AmountYearlyDK");
            this.Property(t => t.PostPoneMonths).HasColumnName("PostPoneMonths");
            this.Property(t => t.Status).HasColumnName("Status");

            this.Property(t => t.Ramavtal).HasColumnName("Ramavtal");

            this.Property(t => t.UserId).HasColumnName("UserId");
            // Relationships
            this.HasRequired(t => t.LegalHolder).WithMany().HasForeignKey(x => x.LegalHolderId);
            this.HasRequired(t => t.Site).WithMany().HasForeignKey(x => x.SiteId);
            //this.HasRequired(t => t.Opponent).WithMany().HasForeignKey(x => x.OpponentHolderId);
            this.HasRequired(t => t.Category).WithMany().HasForeignKey(x => x.CategoryId);
        }
    }
}
