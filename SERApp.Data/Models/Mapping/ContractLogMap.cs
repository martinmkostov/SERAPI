﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    
    public class ContractLogMap : EntityTypeConfiguration<ContractLog>
    {
        public ContractLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ContractLog");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ContractId).HasColumnName("ContractId");
            this.Property(t => t.LogDateTime).HasColumnName("LogDateTime");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UserId).HasColumnName("UserId");            
        }
    }
}
