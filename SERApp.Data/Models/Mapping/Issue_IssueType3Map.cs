﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class Issue_IssueType3Map : EntityTypeConfiguration<Issue_IssueType3>
    {
        public Issue_IssueType3Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Issue_IssueType3");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IssueType3Id).HasColumnName("IssueType3Id");
            //this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.IssueWebId).HasColumnName("IssueWebId");

            this.HasRequired(x => x.IssueWeb).WithMany(x => x.Issue_IssueType3).HasForeignKey(x => x.IssueWebId);
            this.HasRequired(x => x.IssueType3);
        }
    }
}
