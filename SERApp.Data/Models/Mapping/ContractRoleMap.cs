﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ContractRoleMap : EntityTypeConfiguration<ContractRole>
    {
        public ContractRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ContractRole");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoleName).HasColumnName("RoleName");
            this.Property(t => t.AccountId).HasColumnName("AccountId");


            //// Relationships
            //this.HasRequired(t => t.LegalHolder).WithMany().HasForeignKey(x => x.LegalHolderId);
            //this.HasRequired(t => t.Site).WithMany().HasForeignKey(x => x.SiteId);
            ////this.HasRequired(t => t.Opponent).WithMany().HasForeignKey(x => x.OpponentHolderId);
            //this.HasRequired(t => t.Category).WithMany().HasForeignKey(x => x.CategoryId);
        }
    }
}
