﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IncidentAccidentCommentMap : EntityTypeConfiguration<IncidentAccidentComment>
    {
        public IncidentAccidentCommentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("IncidentAccidentComment");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IncidentAccidentId).HasColumnName("IncidentAccidentId");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.Requirements).HasColumnName("Requirements");
            this.Property(t => t.Disbursed).HasColumnName("Disbursed");
            this.Property(t => t.CompensationFromInsurance).HasColumnName("CompensationFromInsurance");
            this.Property(t => t.CompensationFromOthers).HasColumnName("CompensationFromOthers");
            this.Property(t => t.Name).HasColumnName("Name");

            
        }
    }
}
