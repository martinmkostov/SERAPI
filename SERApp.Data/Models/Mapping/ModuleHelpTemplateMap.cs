﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class ModuleHelpTemplateMap : EntityTypeConfiguration<ModuleHelpTemplate>
    {
        public ModuleHelpTemplateMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Name)
                .HasMaxLength(250);

            this.ToTable("ModuleHelpTemplates");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ContentValue).HasColumnName("ContentValue");
        }
    }
}
