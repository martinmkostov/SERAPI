﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class TaskMap : EntityTypeConfiguration<Task>
    {
        public TaskMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Task");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TaskNo).HasColumnName("TaskNo");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Interval).HasColumnName("Interval");
            this.Property(t => t.NuberOfDays).HasColumnName("NuberOfDays");
            this.Property(t => t.RemindAfter).HasColumnName("RemindAfter");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.ExtensionsAllowed).HasColumnName("ExtensionsAllowed");
            this.Property(t => t.DayToComplete).HasColumnName("DayToComplete");
            this.Property(t => t.RecommendedRole).HasColumnName("RecommendedRole");
            this.Property(t => t.IsArchived).HasColumnName("IsArchived");
            this.Property(t => t.AccountId).HasColumnName("AccountId");

            this.Property(t => t.LastUpdatedTime).HasColumnName("LastUpdatedTime");
            this.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");
            this.Property(t => t.IsBimControl).HasColumnName("IsBimControl");
            
        }
    }
}
