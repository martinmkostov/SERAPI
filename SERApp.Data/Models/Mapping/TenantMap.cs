﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class TenantMap : EntityTypeConfiguration<Tenant>
    {
        public TenantMap() {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Tenants");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SiteId).HasColumnName("SiteId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.PermiseType).HasColumnName("PermiseType");
            this.Property(t => t.VisitingAddress).HasColumnName("VisitingAddress");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.InsuranceCompany).HasColumnName("InsuranceCompany");
            this.Property(t => t.SecurityCompany).HasColumnName("SecurityCompany");
            this.Property(t => t.SiteManagerName).HasColumnName("SiteManagerName");
            this.Property(t => t.SiteManagerEmail).HasColumnName("SiteManagerEmail");
            this.Property(t => t.SiteManagerPhone).HasColumnName("SiteManagerPhone");
            this.Property(t => t.ContractName).HasColumnName("ContractName");
            this.Property(t => t.ContractVatNo).HasColumnName("ContractVatNo");
            this.Property(t => t.InvoiceAddress).HasColumnName("InvoiceAddress");
            this.Property(t => t.InvoiceZipAndCity).HasColumnName("InvoiceZipAndCity");
            this.Property(t => t.ContactComments).HasColumnName("ContactComments");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Flag).HasColumnName("Flag");
            this.Property(t => t.PropertyDesignation).HasColumnName("PropertyDesignation");
            this.Property(t => t.UseWrittenStatement).HasColumnName("UseWrittenStatement");
            this.Property(t => t.TenantType).HasColumnName("TenantType");
            this.Property(t => t.SubPremiseType).HasColumnName("SubPremiseType");

            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");

            this.Property(t => t.PostAddress).HasColumnName("PostAddress");
            this.Property(t => t.PostZipCode).HasColumnName("PostZipCode");
            this.Property(t => t.PostCity).HasColumnName("PostCity");
            this.Property(t => t.LastUpdatedByCustomer).HasColumnName("LastUpdatedByCustomer");

            this.Property(t => t.LastUpdatedBy).HasColumnName("LastUpdatedBy");

            this.Property(t => t.LastSentUpdateEmail).HasColumnName("LastSentUpdateEmail");
            this.HasRequired(t => t.Site)
                .WithMany(t => t.Tenants)
                .HasForeignKey(t => t.SiteId);
        }
    }
}
