﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class TagLogMap : EntityTypeConfiguration<TagLog>
    {
        public TagLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("TagLogs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TagId).HasColumnName("TagId");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.GuardId).HasColumnName("GuardId");
        }
    }
}
