﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class LegalHolderMap : EntityTypeConfiguration<LegalHolders>
    {
        public LegalHolderMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("LegalHolders");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LegalHolder).HasColumnName("LegalHolder");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.Account).WithMany().HasForeignKey(x => x.AccountId);
        }
    }
}
