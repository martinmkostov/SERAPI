﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class JobTitleMap : EntityTypeConfiguration<JobTitle>
    {
        public JobTitleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("JobTitles");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TitleName).HasColumnName("TitleName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.IncludedOnExcelExport).HasColumnName("IncludedOnExcelExport");
            
        }
    }
}
