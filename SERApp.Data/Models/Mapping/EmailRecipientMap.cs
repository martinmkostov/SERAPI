﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class EmailRecipientMap: EntityTypeConfiguration<EmailRecipient>
    {
        public EmailRecipientMap()
        {
            //Primary Key
            this.HasKey(e => e.Id);

            this.ToTable("EmailRecipient");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.SentDate).HasColumnName("SentDate");
        }
    }
}
