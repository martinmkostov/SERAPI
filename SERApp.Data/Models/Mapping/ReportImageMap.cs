﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ReportImageMap : EntityTypeConfiguration<ReportImage>
    {
        public ReportImageMap()
        {
            // Primary Key
            this.HasKey(t => t.ReportImageId);


            // Table & Column Mappings
            this.ToTable("ReportImage");
            this.Property(t => t.ReportImageId).HasColumnName("ReportImageId");
            this.Property(t => t.ReportId).HasColumnName("ReportId");
            this.Property(t => t.Image).HasColumnName("Image");

            this.HasRequired(x => x.Report)
                .WithMany(x => x.ReportImages)
                    .HasForeignKey(x => x.ReportId);
        }
    }
}
