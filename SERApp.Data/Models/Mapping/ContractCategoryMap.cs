﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class ContractCategoryMap : EntityTypeConfiguration<ContractCategory>
    {
        public ContractCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("ContractCategories");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.CategoryDescription).HasColumnName("CategoryDescription");
            this.Property(t => t.GroupId).HasColumnName("GroupId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
