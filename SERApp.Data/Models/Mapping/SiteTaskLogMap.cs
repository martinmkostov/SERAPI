﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    
    public class SiteTaskLogMap : EntityTypeConfiguration<SiteTaskLog>
    {
        public SiteTaskLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("SiteTaskLog");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SiteTaskId).HasColumnName("SiteTaskId");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.LogDate).HasColumnName("LogDate");
            this.Property(t => t.LogType).HasColumnName("LogType");
            this.Property(t => t.Instance).HasColumnName("Instance");
            this.Property(t => t.Owner).HasColumnName("Owner");
            this.Property(t => t.IntervalDate).HasColumnName("IntervalDate");
            this.Property(t => t.ExtendDay).HasColumnName("ExtendDay");

            this.Property(t => t.IsFromSer4).HasColumnName("IsFromSer4");
        }
    }
}
