﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IssueType3Map : EntityTypeConfiguration<IssueType3>
    {
        public IssueType3Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IssueType3");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            //this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.AccountId).HasColumnName("AccountId");



        }
    }
}
