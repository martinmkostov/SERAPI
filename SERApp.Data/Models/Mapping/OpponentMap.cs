﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class OpponentMap : EntityTypeConfiguration<Opponent>
    {
        public OpponentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("Opponents");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.OpponentHolderName).HasColumnName("OpponentHolderName");
            this.Property(t => t.AccountId).HasColumnName("AccountId");


            // Relationships
            this.HasRequired(t => t.Account).WithMany().HasForeignKey(x => x.AccountId);
        }
    }
}
