﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public int LegalHolderId { get; set; }
        public int SiteId { get; set; }
        //public int OpponentHolderId { get; set; }
        public string OpponentHolder { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public DateTime RemindDate { get; set; }
        public DateTime CancelLatest { get; set; }
        public bool Active { get; set; }
        public string Note { get; set; }
        public string FileDir { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedTime { get; set; }
        public int CategoryId { get; set; }
        public Int64 AmountYearly { get; set; }
        public string InformationText { get; set; }
        public Int64 AmountYearlyDK { get; set; }
        public Int64 PostPoneMonths { get; set; }

        public virtual LegalHolders LegalHolder { get; set; }
       // public virtual Opponent Opponent { get; set; }
        public virtual ContractCategory Category { get; set; }
        public virtual Site Site { get; set; }


        public bool Ramavtal { get; set; }

        public int Status { get; set; }

        public int UserId { get; set; }
        public int? TenantId { get; set; }
        
    }
}
