﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class DailyReportRecipient
    {
        public int RecipientId { get; set; }
        public int SiteId { get; set; }
        public int AccountId { get; set; }
        public int ReportType { get; set; }
        public string Email { get; set; }
    }
}
