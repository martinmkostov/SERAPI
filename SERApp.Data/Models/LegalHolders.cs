﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class LegalHolders
    {
        public int Id { get; set; }
        public string LegalHolder { get; set; }
        public int AccountId { get; set; }
        public bool IsActive { get; set; }

        public Account Account { get; set; }
    }
}
