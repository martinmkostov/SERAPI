﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set;  }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
    }
}
