﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IncidentAccident
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public int SiteId { get; set; }
        public string Location { get; set; }

        public string Affected { get; set; }
        public string Event { get; set; }
        public string ProbableCause { get; set; }
        public string Damages { get; set; }
        public string Reporter { get; set; }
        public int SkadetypId { get; set; }
        public int EstimatedCost { get; set; }

        public int AccidentType { get; set; }

        public int Status { get; set; }

        public string InternalCaseNumber { get; set; }
        public string ExternalCaseNumber { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }
        public decimal Cost { get; set; }

        public string ChangedBy { get; set; }
        public DateTime? ChangedTime { get; set; }
    }
}
