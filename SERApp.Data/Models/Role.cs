using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public class Role: Enumeration<int>
    {
        public static Role Admin => new Role(1, "admin");
        public static Role Editor => new Role(1, "editor");
        public static Role Viewer => new Role(1, "viewer");
        public static Role SuperAdmin => new Role(1, "superadmin");
        public static Role User => new Role(1, "user");
        public static Role Hidden => new Role(1, "hidden");
        
        public string Description { get; set; }
        public bool IsModule { get; set; }
        public bool IsUser { get; set; }

        public Role() : base() { }

        public Role(int id, string name)
            : base(id, name)
        {
        }
    }
}
