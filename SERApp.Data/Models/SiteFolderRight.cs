﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class SiteFolderRight
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Folder { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
