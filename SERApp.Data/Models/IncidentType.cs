﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IncidentType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsReportRequired { get; set; }
        
        public string RequiredReport { get; set; }
        public string ShownReport { get; set; }
    }
}
