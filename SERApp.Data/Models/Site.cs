using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Site
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string AdditionalLocationInfo { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNumber { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string ObjectNumber { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public virtual Account Account { get; set; }
        public Guid Guid { get; set; }

        public string FacebookLink { get; set; }
        public string InstagramLink { get; set; }
        public string YoutubeLink { get; set; }
        public string PublicEmailAddress { get; set; }
        public string PublicContactNumber { get; set; }

        public string CompanyLogo { get; set; }
        public string GDPRTitle { get; set; }
        public string GDPRContent { get; set; }
        public string SenderEmailAddress { get; set; }
        public string SenderName { get; set; }

        public virtual ICollection<Tenant> Tenants { get; set; }
        public virtual ICollection<Guard> Guards { get; set; }
        public virtual ICollection<SiteTask> SiteTask { get; set; }
    }
}
