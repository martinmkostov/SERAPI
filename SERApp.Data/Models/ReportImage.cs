﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class ReportImage
    {
        public int ReportImageId { get; set; }
        public int ReportId { get; set; }
        public string Image { get; set; }

        public virtual Report Report { get; set; }
    }
}
