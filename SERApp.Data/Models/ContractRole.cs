﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class ContractRole
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public int AccountId { get; set; }
    }
}
