﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IssueWebHistory
    {
        public int IssueWebHistoryId { get; set; }
        public string ReactedBy { get; set; }
        public int Minutes { get; set; }
        public int MaterialCost { get; set; }
        public string DescriptionExternal { get; set; }
        public string DescriptionInternal { get; set; }
        public DateTime Date { get; set; }
        public bool IsWarranty { get; set; }
        public bool IsRent { get; set; }
        public bool IsDamage { get; set; }
        public bool IsPrivate { get; set; }

        public string IssueWebHistoryType { get; set; }

        public int IssueWebId { get; set; }
        public virtual IssueWeb IssueWeb { get; set; }
    }
}
