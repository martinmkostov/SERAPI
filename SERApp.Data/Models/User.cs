using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class User
    {
        public User()
        {
            this.UserModules = new List<UserModuleRoles>();
        }

        public int Id { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Mobile { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string HashedPassword { get; set; }
        public string RandomSecret { get; set; }
        public int AccountId { get; set; }
        public int DepartmentId { get; set; }
        public bool IsActivityAdmin { get; set; }
        public bool IsAdministrationAdmin { get; set; }
        public bool? EnablePopupMobileMessage { get; set; }
        public virtual Account Account { get; set; }
        public virtual ICollection<UserModuleRoles> UserModules { get; set; }
    }
}
