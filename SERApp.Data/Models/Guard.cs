﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Guard
    {
        public int Id { get; set; }
        public string Name { get; set; }        
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }

        public int SiteId { get; set; }
        public virtual Site Site { get; set; }
        public virtual ICollection<IncidentGuard> IncidentGuard { get; set; }
    }
}
