﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class EmailTemplateVariables
    {
        public int Id { get; set; }
        public string VariableName { get; set; }
        public string VariableData { get; set; }
        public int EmailTemplateId { get; set; }

        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
