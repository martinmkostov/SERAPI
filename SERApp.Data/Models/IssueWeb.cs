﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class IssueWeb
    {
        public int IssueWebId { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateChecked { get; set; }
        public DateTime? DateFinished { get; set; }

        public int AccountId { get; set; }

        public int SiteId { get; set; }
        public Site Site { get; set; }

        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }

        public int IssueType1Id { get; set; }
        public IssueType1 IssueType1 { get; set; }

        public int IssueType2Id { get; set; }
        public IssueType2 IssueType2 { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string ApartmentNumber { get; set; }


        public string Image { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }



        public string Description { get; set; }
        public bool IsPolice { get; set; }
        public bool? IsPublic { get; set; }
        public int Status { get; set; }
        public int ReminderEmailSentCount { get; set; }

        public int IssueNumber { get; set; }

        public Guid Guid { get; set; }

        public virtual ICollection<IssueWebHistory> IssueWebHistories { get; set; }
        public virtual ICollection<Issue_IssueType3> Issue_IssueType3 { get; set; }
    }
}
