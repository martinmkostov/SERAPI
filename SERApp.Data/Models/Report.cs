﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Report
    {
        public int Id { get; set; }

        public int ReportTypeId { get; set; }
        public ReportType ReportType { get; set; }

        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }

        public DateTime? AlarmTime { get; set; }
        public DateTime? GuardTime { get; set; }
        public DateTime? ActionTime { get; set; }

        public DateTime? ClosedFrom { get; set; }
        public DateTime? ClosedTo { get; set; }

        public string Cause { get; set; }

        public int GuardId { get; set; }
        public virtual Guard Guard { get; set; }

        public string AdditionalText { get; set; }
        public bool PoliceReport { get; set; }
        public string Resolution { get; set; }
        public string PoliceAuthority { get; set; }
        public string Plaintiff { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public string IncidentText { get; set; }
        public string By { get; set; }
        public string OrderNumber { get; set; }

        public bool VisibleEffect { get; set; }
        public bool AlarmReset { get; set; }
        public bool PoliceCalled { get; set; }
        public bool KManInformed { get; set; }
        public bool KManUnavailable { get; set; }

        public int IncidentId { get; set; }
        public virtual Incident Incident { get; set; }

        public virtual ICollection<RyckrapportData> RyckrapportDatas { get; set; }
        public virtual ICollection<ReportImage> ReportImages { get; set; }
    }  
}
