﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Setting
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int AccountId { get; set; }
        public int ConfigSettingId { get; set; }
        public int SiteId { get; set; }
        public ConfigSetting ConfigSetting { get; set; }
    }
}
