﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class BimControlItem
    {
        public int Id { get; set; }
        public int SiteTaskLogId { get; set; }
        public SiteTaskLog SiteTaskLog { get; set; }
        public DateTime Date { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }

        public int IssueId { get; set; }
        public virtual Issue Issue { get; set; }
        public virtual ICollection<BimControlItemImage> Photos { get; set; }

        public string LittraId { get; set; }
        public string ErrorDescription { get; set; }
    }
}
