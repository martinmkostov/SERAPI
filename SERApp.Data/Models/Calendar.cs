﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Calendar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desription { get; set; }
        public bool IsActive { get; set; }
        public int UserId { get; set; }
        public int SiteId { get; set; }
        public DateTime CalendarDate { get; set; }
        public DateTime CalendarTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }

    }
}
