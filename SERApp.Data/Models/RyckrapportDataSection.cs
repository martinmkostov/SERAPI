﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class RyckrapportDataSection
    {
        public int RyckrapportDataSectionId { get; set; }
        public int RyckrapportDataId { get; set; }
        public int SectionId { get; set; }

        public virtual Section Section { get; set; }
        public virtual RyckrapportData RyckrapportData { get; set; }
    }
}
