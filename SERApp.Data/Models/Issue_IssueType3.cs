﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Issue_IssueType3
    {
        public int Id { get; set; }
        public int IssueType3Id { get; set; }

        public virtual IssueType3 IssueType3 { get;set; }

        public bool Value { get; set; }
        public int IssueWebId { get; set; }

        public virtual IssueWeb IssueWeb { get; set; }
    }
}
