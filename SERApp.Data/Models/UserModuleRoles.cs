using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SERApp.Data.Models
{
    public partial class UserModuleRoles
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> SiteId { get; set; }
        public virtual Module Module { get; set; }
        public virtual User User { get; set; }

        public virtual Role Role { get; set; }

        [ForeignKey(nameof(SiteId))]
        public virtual Site Site { get; set; }
    }
}
