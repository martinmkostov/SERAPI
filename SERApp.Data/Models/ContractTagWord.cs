﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class ContractTagWord
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int TagWordId { get; set; }

        public Contract Contract { get; set; }
        public TagWord TagWord { get; set; }
    }
}
