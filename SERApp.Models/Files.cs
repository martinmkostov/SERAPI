﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class Files
    {
        public string Name { get; set; }
        public string Format { get; set; }
        public string Icon { get; set; }
        public bool IsFolder { get; set; }
        public List<Files> Children { get; set; }
    }
}
