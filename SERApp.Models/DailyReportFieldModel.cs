﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class DailyReportFieldModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string DataSource { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string FieldType { get; set; }
        public string FieldTypeFlag { get; set; }
        public bool IsRequired { get; set; }
        public string CustomEditorName { get; set; }
        public int SortOrder { get; set; }
        public string ExcludedValues { get; set; }
        public string FieldName { get; set; }
        public string FieldGroup { get; set; }
        public string CustomScript { get; set; }
    }
}