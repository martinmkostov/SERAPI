﻿using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class DailyReportRecipientModel
    {
        public int RecipientId { get; set; }
        public int SiteId { get; set; }
        public int AccountId { get; set; }
        public ReportTypes ReportType { get; set; }
        public string Email { get; set; }
    }
}
