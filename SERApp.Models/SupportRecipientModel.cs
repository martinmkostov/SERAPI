﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SupportRecipientModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int AccountId { get; set; }
    }
}
