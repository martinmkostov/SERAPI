﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IncidentStatisticsModel
    {
        public List<string> labels { get; set; }
        public List<Datasets> datasets { get; set; }
        public string title { get; set; }
    }

    public class Datasets
    {
        public string label { get; set; }
        public List<int> data { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor => "black";
    }

    public class IncidentStatsGrouped
    {
        public string Date { get; set; }
        public int Id { get; set; }
        public int Count { get; set; }
    }

    public class IncidentStatsByIdGrouped
    {
        public int Id { get; set; }
        public int Count { get; set; }
    }

    public class TypeInfo
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
