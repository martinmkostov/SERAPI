﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IncidentAccidentModel
    {
        public int Id { get; set; }
        public DateModel Date { get; set; }
        public TimeModel Time { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string Location { get; set; }

        public string Affected { get; set; }
        public string Event { get; set; }
        public string ProbableCause { get; set; }
        public string Damages { get; set; }
        public string Reporter { get; set; }
        public int Skadetyp { get; set; }
        public int EstimatedCost { get; set; }

        public int AccidentType { get; set; }
        public int Status { get; set; }

        public string InternalCaseNumber { get; set; }
        public string ExternalCaseNumber { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }

        public List<IncidentAccidentCommentModel> Comments { get; set; }

        public string Name { get; set; }

        public SiteModel Site { get; set; }

        public bool SendToEmail { get; set; }
        public string DefaultEmail { get; set; }
        public FileStream content { get; set; }
        public string dumppath { get; set; }
        public string filepath { get; set; }

        public decimal Cost { get; set; }

        public string ChangedBy { get; set; }
        public DateTime? ChangedTime { get; set; }
    }
}
