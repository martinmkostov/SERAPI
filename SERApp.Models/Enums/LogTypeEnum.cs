﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public enum LogTypeEnum
    {
        Information = 1, //Specific Messages for Application Level Transactions (CRUD functions) and Login / Logout
        Error = 2, //Internal or Code Base Errors
        Warning = 3, //Information that the Superadmin or Admins should notice and/or take action. Probably something that should take caution
        Notification = 4, //Information that the Superadmin or Admins should notice and/or take action (light). Logs for email notification (create account, create loan, send email to superadmin)
    }
}
