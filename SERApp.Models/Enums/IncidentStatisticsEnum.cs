﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public class IncidentStatisticsEnum
    {
        public enum DateType
        {
            DAY,
            MONTH,
            YEAR
        }

        public enum DataType
        {
            Vad,
            Var,
            Vem
        }
    }
}
