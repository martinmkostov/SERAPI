﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public enum StatusEnum
    {
        All = 0,
        Active = 1, //important - means confirmed
        NotConfirmed = 4, //important
        PassedReturnedData = 5, //important
        Returned = 2,
        Deleted = 3
    }
}
