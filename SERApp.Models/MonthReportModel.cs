﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class MonthReportModel
    {
        public AuthoritiesInterventionEnum authoritiesInterventionEnum { get; set; }
        public string authoritiesInterventionEnumString { get; set; }
        public int Id { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public int Count { get; set; }
        public List<SiteCountModel> SiteCount { get; set; } = new List<SiteCountModel>();
    }

    public class SiteCountModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public enum AuthoritiesInterventionEnum
    {
        WithoutEmergencyService =0,
        EmergencyService =1,
        WithoutPolice =2,
        WithPolice =3,
        Hide = 4,
    }
}
