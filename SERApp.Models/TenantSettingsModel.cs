﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TenantSettingsModel
    {
        public string InitialMessage { get; set; }
        public string NameOfStore { get; set; }
        public string EmailToUnit { get; set; }
        public string TelephonNumberToUnit { get; set; }
        public string PostalAddressToUnit { get; set; }
        public string PostalCodeToUnit { get; set; }
        public string ZipCodeToUnit { get; set; }
        public string VisitingAddressToUnit { get; set; }
        public string IntrusionAlarm { get; set; }
    }
}
