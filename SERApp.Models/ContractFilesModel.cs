﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ContractFilesModel
    {
        public string FileName { get; set; }
        public string Url { get; set; }
    }
}
