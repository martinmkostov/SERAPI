﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class GuardModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }

        public int SiteId { get; set; }
        public SiteModel Site { get; set; }
        public List<IncidentGuardModel> IncidentGuard { get; set; }
    }
}
