﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string EmailAddress { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int CompanyType { get; set; }
        public string PopupMobileMessage { get; set; }

        public List<SiteModel> Sites { get; set; }
        public List<UserModel> Users { get; set; }
        public List<UserModel> ActiveUsers { get; set; }
        public List<UserModel> InActiveUsers { get; set; }
        public List<ModuleModel> Modules { get; set; }
        public List<DepartmentModel> Departments { get; set; }
        public List<AccountSettingModel> AccountSettings { get; set; }

        public string Mobile { get; set; }
    }

    public class AccountModules {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int ModuleId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public AccountModel Account { get; set; }
        public List<ModuleModel> Modules { get; set; }
    }

    public class AccountSites {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public List<SiteModel> Sites { get; set; }
    }

    public class AccountUsers {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public List<UserModel> Users { get; set; }
    }

    public class AccountDepartments
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public List<UserModel> Users { get; set; }
    }

    public class AccountSettingModel {
        public int Id { get; set; }
        public int ConfigSettingId { get; set; }
        public string InputType { get; set; }
        public string Module { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public int AccountId { get; set; }
    }
}
