﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ContractModel
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public int LegalHolderId { get; set; }
        public int SiteId { get; set; }
        //public int OpponentHolderId { get; set; }
        public string OpponentHolder { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public DateTime RemindDate { get; set; }
        public DateTime CancelLatest { get; set; }
        public bool Active { get; set; }
        public string Note { get; set; }
        public string FileDir { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedTime { get; set; }
        public int CategoryId { get; set; }
        public Int64 AmountYearly { get; set; }
        public string InformationText { get; set; }
        public Int64 AmountYearlyDK { get; set; }
        public Int64 PostPoneMonths { get; set; }

        public DateModel ValidFromModel { get; set; }
        public DateModel ValidToModel { get; set; }
        public DateModel RemindDateModel { get; set; }
        public DateModel CancelLatestModel { get; set; }

        public virtual LegalHolderModel LegalHolder { get; set; }
        //public virtual OpponentModel Opponent { get; set; }
        public virtual ContractCategoryModel Category { get; set; }
        public virtual SiteModel Site { get; set; }
        public string SiteName { get; set; }

        public bool Ramavtal { get; set; }

        public List<ContractTagWordModel> ContractTagWords { get; set; }
        public int Status { get; set; }

        public List<ContractLogModel> Logs { get; set; }
        public int UserId { get; set; }

        public int ChangedById { get; set; }
        public int? TenantId { get; set; }
        public TenantModel Tenant { get; set; }
    }

    public class CopyContractModel
    {
        public List<int> ContractIds { get; set; }
        public int SiteId { get; set; }
    }

    public class UpdateContractYear
    {
        public List<int> ContractIds { get; set; }
        public int Year { get; set; }
    }
}
