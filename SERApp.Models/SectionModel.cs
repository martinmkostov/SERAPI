﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SectionModel
    {
        public int SectionId { get; set; }
        public string SectionValue { get; set; }
        //public int RyckrapportDataId { get; set; }
        public int SiteId { get; set; }

        public List<RyckrapportDataSectionModel> RyckrapportDataSections { get; set; }
    }
}
