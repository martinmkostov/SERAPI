﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class LoanModel
    {
        public int Id { get; set; }
        public int CustomerTypeId { get; set; }
        public int CustomerId { get; set; }
        public int SiteId { get; set; }
        public int TypeId { get; set; }
        public int StatusId { get; set; }
        public string ItemNameId { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string PhotoUrl { get; set; }
        public string thumbnail { get; set; }
        public string PhotoString { get; set; }
        public int MessageTypeId { get; set; }
        public DateTime LoanedDate { get; set; }
        public DateTime? ReturnedDate { get; set; }

        public DateModel LoanedDateModel { get; set; }
        public DateModel ReturnedDateModel { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string GivenBy { get; set; }
        public string ReceivedBy { get; set; }
        public string LoanId { get; set; }

        public DateTime? ConfirmedDate { get; set; }
        public DateTime? ActualReturnedDate { get; set; }

        public SiteModel Site { get; set; }
        public CustomerModel Customer { get; set; }
        public LoanTypeModel LoanType { get; set; }
        public MessageTypeModel ConfirmationType { get; set; }

        public string CustomerName { get; set; }
        public string FacilityName { get; set; }
        public string LoanTypeName { get; set; }
        public string Status { get; set; }
        public string ConfirmationTypeName { get; set; }
        public bool SendNotification { get; set; }
        public int SendProcess { get; set; }

        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }

    public class LoanCustomerModel
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int LoanCount { get; set; }
        public int RemainingLoanCount { get; set; }
        public bool AllowedForMoreLoans { get; set; }
        public string Company { get; set; }
    }

    public class LoanEmailParamModel {
        public string ids { get; set; }
        public string command { get; set; }
    }

    public class LoanDeleteParamModel {
        public int id { get; set; }
        public string ids { get; set; }
        public bool deleteCustomer { get; set; }
    }
}
