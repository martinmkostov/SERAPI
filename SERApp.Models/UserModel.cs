﻿using System;

namespace SERApp.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSendCreds { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string HashedPassword { get; set; }
        public string RandomSecret { get; set; }
        public int AccountId { get; set; }
        public int DepartmentId { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public DateTime ? LastLoginDateTime { get; set; }
        public AccountModel Account { get; set; }
        public RoleModel Role { get; set; }
        public DepartmentModel Department { get; set; }
        public bool IsActivityAdmin { get; set; }
        public bool IsAdministrationAdmin { get; set; }
        public bool IsUpdate { get; set; }
        public bool? EnablePopupMobileMessage { get; set; }

        public bool UpdateRoleOnly { get; set; }
    }
}