﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.HtmlTemplatesModel
{
    public class WeekReport
    {
        public string AccountName { get; set; }
        public string Title { get; set; }
        public string SiteName { get; set; }
        public string Date1 { get; set; }
        public string Date2 { get; set; }
        public List<WeekReportSub1> WeekReportMain { get; set; }

        public List<WeekReportSubType> Type1WeekReport { get; set; }
        public List<WeekReportSubType> Type2WeekReport { get; set; }
        public List<WeekReportSubType> Type3WeekReport { get; set; }
    }

    public class WeekReportSub1
    {
        public string WeekDay { get; set; }
        public string Summary { get; set; }
        public int Q { get; set; }
        public int P { get; set; }
    }
    public class WeekReportSubType
    {
        public string Name { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
        public int Total { get; set; }
    }
}
