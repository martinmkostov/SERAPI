﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.HtmlTemplatesModel
{
    public class DayReport
    {
        public string AccountName { get; set; }
        public string Title { get; set; }
        public string SiteName { get; set; }
        public DateTime Date { get; set; }
        public string Summary { get; set; }
        public string Guards { get; set; }
        public int TotalP { get; set; }
        public int TotalQ { get; set; }
        public IEnumerable<IncidentModel> Incidents { get; set; }
    }
}
