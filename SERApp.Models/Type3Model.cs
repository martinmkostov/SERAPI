﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class Type3Model
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsTenantRequired { get; set; }
        public int TenantTypeId { get; set; }
        public int AccountId { get; set; }
        public int SiteId { get; set; }
        public List<int> TenantTypeTypeIds { get; set; }

        public List<TenantModel> Tenants { get; set; }
    }
}
