﻿using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SiteTaskLogModel
    {
        public int Id { get; set; }
        public int SiteTaskId { get; set; }
        public string Comment { get; set; }
        public DateTime LogDate { get; set; }
        public TaskLogType LogType { get; set; }
        public int Instance { get; set; }
        public string Owner { get; set; }
        public DateTime IntervalDate { get; set; }
        public int ExtendDay { get; set; }
        public int loopConfirm { get; set; }

        public bool IsPreviousHistory { get; set; }
        //todo add for files
        public bool IsFromSer4 { get; set; }

        public string TaskNo { get; set; }
        public string Taskname { get; set; }
        public int LogDateOffset { get; set; }

        public bool IsBimControl { get; set; }
        public List<BimControlItemModel> Items { get; set; }
    }
}
