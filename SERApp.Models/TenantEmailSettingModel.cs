﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
   public class TenantEmailSettingModel
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Foot { get; set; }
    }
}
