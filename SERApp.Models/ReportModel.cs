﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ReportModel
    {
        public int Id { get; set; }

        public int moduleId { get; set; }

        public int ReportTypeId { get; set; }
        public ReportTypeModel ReportType { get; set; }

        public int TenantId { get; set; }
        public TenantModel Tenant { get; set; }

        public DateModel nbDate { get; set; }
        public TimeModel nbTime { get; set; }

        public TimeModel nbTimeClosedFrom { get; set; }
        public TimeModel nbTimeClosedTo { get; set; }

        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }

        public string Cause { get; set; }

        public int GuardId { get; set; }
        public GuardModel Guard { get; set; }

        public string AdditionalText { get; set; }
        public bool PoliceReport { get; set; }
        public string Resolution { get; set; }
        public string PoliceAuthority { get; set; }
        public string Plaintiff { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public string IncidentText { get; set; }
        public string By { get; set; }
        public string OrderNumber { get; set; }

        public bool VisibleEffect { get; set; }
        public bool AlarmReset { get; set; }
        public bool PoliceCalled { get; set; }
        public bool KManInformed { get; set; }
        public bool KManUnavailable { get; set; }

        public int IncidentId { get; set; }
        public IncidentModel Incident { get; set; }
        public List<RyckrapportDataModel> RyckrapportDatas { get; set; }
        public List<ReportImageModel> ReportImages { get; set; }


        public TimeModel alarmTime { get; set; }
        public TimeModel guardTime { get; set; }
        public TimeModel actionTime { get; set; }

        public DateTime? AlarmTime { get; set; }
        public DateTime? GuardTime { get; set; }
        public DateTime? ActionTime { get; set; }
    }
}
