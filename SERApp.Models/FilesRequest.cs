﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SendFilesRequest
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public List<FilesRequest> Files { get; set; }
    }

    public class FilesRequest
    {
        public string Base64 { get; set; }
        public string Name { get; set; }
        public string Mime { get; set; }
    }
}
