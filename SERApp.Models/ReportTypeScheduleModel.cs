﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ReportTypeScheduleModel
    {
        public string Id { get; set; }
        public string ReportTypeId { get; set; }
        public int ModuleId { get; set; }
        public int AccountId { get; set; }
        public string Schedule { get; set; }
        public string Cron { get; set; }
        public string JobId { get; set; }
        public string TimeZone {get;set;}
        public string BaseUTCOffset { get; set; }
        public bool IsEnabled { get; set; }

        public int DataToSend { get; set; }
    }

  
}
