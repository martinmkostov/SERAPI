﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class DailyReportStatisticsRequestModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int AccountId { get;set; }
        public int DateType { get;set; }
        public int DataType { get;set; }
        public int SiteId { get;set; }
        public DateTime? Start2 { get; set; }
        public DateTime? End2 { get; set; }
        public bool StackedColumn { get; set; }
        public int Column { get; set; }
        public List<int> SelectedTypes { get; set; } 
    }
}
