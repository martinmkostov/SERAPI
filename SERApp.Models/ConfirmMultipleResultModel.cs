﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ConfirmMultipleResultModel
    {
        public List<DateTime> Dates { get; set; }
        public List<int> Ids { get; set; }
    }
}
