﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SiteModel
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string AdditionalLocationInfo { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNumber { get; set; }
        public DateTime ? CreatedDate { get; set; }
        public DateTime ? LastUpdatedDate { get; set; }
        public bool? IsActive { get; set; }
        public string ObjectNumber { get; set; }
        public bool? IsDeleted { get; set; }
        public AccountModel Account { get; set; }

        public Guid Guid { get; set; }

        public string FacebookLink { get; set; }
        public string InstagramLink { get; set; }
        public string YoutubeLink { get; set; }
        public string PublicEmailAddress { get; set; }
        public string PublicContactNumber { get; set; }

        public string CompanyLogo { get; set; }
        public string CompanyLogoBase64 { get; set; }
        public string GDPRTitle { get; set; }
        public string GDPRContent { get; set; }
        public string SenderEmailAddress { get; set; }
        public string SenderName { get; set; }
    }

    public class SiteModelQuery {
        public int accountId { get; set; }
        public int userId { get; set; }
        public int[] moduleId { get; set; }
    }
}
