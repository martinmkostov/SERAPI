﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class OpponentModel
    {
        public int Id { get; set; }
        public string OpponentHolderName { get; set; }
        public int AccountId { get; set; }

        public AccountModel Account { get; set; }
    }
}
