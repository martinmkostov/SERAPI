﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TagModel
    {
        public int Id { get; set; }
        public string RFIDData { get; set; }
        public string Description { get; set; }

        public int TenantID { get; set; }
        public TenantModel Tenant { get; set; }

        public int SiteId { get; set; }
    }
}
