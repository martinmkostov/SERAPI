﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ContractRoleModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public int AccountId { get; set; }
    }
}
