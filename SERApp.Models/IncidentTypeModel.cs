﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IncidentTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsReportRequired { get; set; }
        public List<ReportTypeModel> RequiredReport { get; set; }
        public List<ReportTypeModel> ShownReport { get; set; }
        public bool isSelected { get; set; }
    }
}
