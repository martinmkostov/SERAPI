﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Constants
{
    public class ConfigSettings
    {
        public const string SERAppEntitiesKey = "SERAppDBContext";
        public const string SERAppWebConfigFileSource = @"\SEPAppBlack\Web.config";
        public const string SERAppAppConfigFileSource = @"\SEPAppBlack\App.Settings.config";
    }
}
