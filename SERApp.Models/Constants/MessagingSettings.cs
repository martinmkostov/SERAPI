﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Constants
{
    public class MailMessagingSettings
    {
        public const string AccountAdminCreateEmailSubject = "SER Account Admin Creation";
        public const string AccountAdminCreateEmailBody = @"Please confirm that your email will be used as the Account Admin of <b>{accountName}</b>. 
                                                          <br /> Click on this link <a href={link} style='color:#ffc107;font-weight:bold'>Change Profle Info</a> to update your user account details or you can use these credentials to login :
                                                          <br /> 
                                                          <br /> If the link is not working, kindly copy and paste this link <span style='color:#ffc107;font-weight:bold'>{link}</span> to your web browser.
                                                          <br /> 
                                                          <br /> <b>Your Username : <span style='color:#007bff'>{userName}</span></b>
                                                          <br /> <b>Your Password : <span style='color:#007bff'>{password}</span></b>
                                                          <br />
                                                          <br /> Thank you!";

        public const string AccountUserCreateEmailSubject = "SER Account User Creation";
        public const string AccountUserCreateEmailBody = @"Please confirm that your email will be used as the Account User of <b>{accountName}</b>. 
                                                          <br /> Click on this link <a href={link} style='color:#ffc107;font-weight:bold'>Change Profle Info</a> to update your user account details or you can use these credentials to login :
                                                          <br /> 
                                                          <br /> If the link is not working, kindly copy and paste this link <span style='color:#ffc107;font-weight:bold'>{link}</span> to your web browser.
                                                          <br /> 
                                                          <br /> <b>Your Username : <span style='color:#007bff'>{userName}</span></b>
                                                          <br /> <b>Your Password : <span style='color:#007bff'>{password}</span></b>
                                                          <br />
                                                          <br /> Thank you!";

        public const string LoanCreateEmailSubject = "SER Loan Confirmation";
        public const string LoanCreateEmailBody = @"Please confirm that your email was registered as a customer / tenant of <b>{siteName}</b> Site / Object, of the company <b>{accountName}</b>. 
                                                          <br /> To confirm, please click on this link <a href={link} style='color:#ffc107;font-weight:bold'>CONFIRM</a>
                                                          <br /> 
                                                          <br /> If the link is not working, kindly copy and paste this link <span style='color:#ffc107;font-weight:bold'>{link}</span> to your web browser.
                                                          <br /> 
                                                          <br />
                                                          <br /> Details : 
                                                          <br /> <h4>{loanId}</h4>
                                                          <br /> Item : <b>{loanItemNameId}</b>
                                                          <br /> Loaned Date : <b>{loanedDate}</b>
                                                          <br /> Expected Return Date : <b>{returnedDate}</b>
                                                          <br /> Thank you!";

        public const string LoanUpdateNotifcationEmailSubject = "SER Loan Update Notification";
        public const string LoanUpdateNotifcationEmailBody = @"This Loan has been updated. <br /><br />
                                                          Please make sure that these information are correct and valid. 
                                                          <br /> Customer / Tenant of <b>{siteName}</b> Site / Object, of the company <b>{accountName}</b> 
                                                          <br />
                                                          <br /> Details : 
                                                          <br /> <h4>{loanId}</h4>
                                                          <br /> Item : <b>{loanItemNameId}</b>
                                                          <br /> Description : <b>{loanDescription}</b>
                                                          <br /> Given By : <b>{loanGivenBy}</b>
                                                          <br /> Received By : <b>{loanReceivedBy}</b>
                                                          <br /> Loaned Date : <b>{loanedDate}</b>
                                                          <br /> Confirmed Date : <b>{loanConfirmedDate}</b>
                                                          <br /> Expected Return Date : <b>{returnedDate}</b>
                                                          <br /> Actual Return Date : <b>{actualReturnedDate}</b>
                                                          <br /> Thank you!";

        public const string LoanNotifcationEmailSubject = "SER Loan Notification";
        public const string LoanNotifcationEmailBody = @"This is to notify that you have an existing Loan from <b>{accountName}</b>. <br /><br />
                                                          Please make sure that these information are correct and valid. 
                                                          <br /> Customer / Tenant of <b>{siteName}</b> Site / Object, of the company <b>{accountName}</b> 
                                                          <br />
                                                          <br /> Details : 
                                                          <br /> <h4>{loanId}</h4>
                                                          <br /> Item : <b>{loanItemNameId}</b>
                                                          <br /> Description : <b>{loanDescription}</b>
                                                          <br /> Given By : <b>{loanGivenBy}</b>
                                                          <br /> Received By : <b>{loanReceivedBy}</b>
                                                          <br /> Loaned Date : <b>{loanedDate}</b>
                                                          <br /> Confirmed Date : <b>{loanConfirmedDate}</b>
                                                          <br /> Expected Return Date : <b>{returnedDate}</b>
                                                          <br /> Actual Return Date : <b>{actualReturnedDate}</b>
                                                          <br /> Thank you!";

        public const string LoanPassedLoanDateNotifcationEmailSubject = "SER Loan Reminder - PASSED LOAN DATE";
        public const string LoanPassedLoanDateNotifcationEmailBody = @"This is to notify that your loan has already passed it's original date to be returned<br /><br />
                                                          Please make sure to return the Loan ASAP. 
                                                          <br /> Customer / Tenant of <b>{siteName}</b> Site / Object, of the company <b>{accountName}</b> 
                                                          <br />
                                                          <br /> Details : 
                                                          <br /> <h4>{loanId}</h4>
                                                          <br /> Item : <b>{loanItemNameId}</b>
                                                          <br /> Description : <b>{loanDescription}</b>
                                                          <br /> Given By : <b>{loanGivenBy}</b>
                                                          <br /> Received By : <b>{loanReceivedBy}</b>
                                                          <br /> Loaned Date : <b>{loanedDate}</b>
                                                          <br /> Confirmed Date : <b>{loanConfirmedDate}</b>
                                                          <br /> Expected Return Date : <b>{returnedDate}</b>
                                                          <br /> Actual Return Date : <b>{actualReturnedDate}</b>
                                                          <br /> Thank you!";

        public const string ForgotPasswordSubject = "SER FORGOT/CHANGE PASSWORD";
        public const string ForgotPasswordEmailBody = @"Change your password by clicking this link. {changePassLink} <br />                  
                                            <br />
                                            Please make sure change your password as often as possible to avoid security issues
                                            <br />
                                            <br />
                                            Best regards
                                            <br />
                                            <a href='http://ser4.serapp.com'>SER APP</a>";


    }

    public class LogMessagingSettings {

        public const string Get = "GET";
        public const string GetAll = "GETALL";
        public const string Save = "SAVE";
        public const string Create = "CREATE";
        public const string Update = "UPDATE";
        public const string Delete = "DELETE";

        public const string ActionLogin = "LOGIN";
        public const string ActionLogoff = "LOGOFF";
        public const string ActionSendEmail = "SENDEMAIL";
        public const string ActionSendEmailReport = "SENDEMAIL_REPORT";
        public const string ActionSendEmailAudit = "SENDEMAIL_AUDIT";

        public const string MessageLogin = "User Login.";
        public const string MessageLoginFailed = "User Login Failed.";

        public const string MessageCreateAccount = "Create User Account";

        public const string MessageCreateUser = "Create User";
        public const string MessageUpdateUser = "Update User";
        public const string MessageDeleteUser = "Delete User";

        public const string MessageCreateSite = "Create Site";
        public const string MessageUpdateSite = "Update Site";
        public const string MessageDeleteSite = "Delete Site";

        public const string MessageCreateTenant = "Create Tenant";
        public const string MessageUpdateTenant = "Update Tenant";
        public const string MessageDeleteTenant = "Delete Tenant";

        public const string MessageCreateLoan = "Create Loan";
        public const string MessageUpdateLoan = "Update Loan";
        public const string MessageDeleteLoan = "Delete Loan";

        public const string MessageCreateCustomer = "Create Customer";
        public const string MessageUpdateCustomer = "Update Customer";
        public const string MessageDeleteCustomer = "Delete Customer";

        public const string ResponseLoginOk = "Authenticated!";
        public const string ResponseLogoffOk = "User Logged off!";
        public const string ResponseAlreadyLogoff = "User Logged off already!";

        public const string ResponseLoginInvalidPass = "Invalid Password!";
        public const string ResponseLoginNoUserRecord = "No record found with this username!";

        
        
    }

    public class EmailTemplates {
        public const string LoanCreate = "CreateLoan";
        public const string LoanUpdate = "UpdateLoan";
        public const string LoanNotify = "NotifyLoan";
        public const string LoanPassedDate = "PassedDateLoan";
        public const string LoanConfirm = "ConfirmLoan";
        public const string LoanReturn = "LoanReturn";
    }
}
