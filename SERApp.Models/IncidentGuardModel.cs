﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IncidentGuardModel
    {
        public int Id { get; set; }
        public string GuardName { get; set; }
        public int IncidentId { get; set; }
        public IncidentModel Incident { get; set; }

        public int GuardId { get; set; }
        public GuardModel Guard { get; set; }
    }
}
