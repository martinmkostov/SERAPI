using System;
using System.Collections.Generic;

namespace SERApp.Models
{
    public partial class FacilityModel
    {
        public int Id { get; set; }
        public string FacilityName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
    }
}
