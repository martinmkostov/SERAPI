﻿using System;

namespace SERApp.Models
{
    public class ModuleModel
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NodeLevel { get; set; }
        public bool IsActive { get; set; }
        public bool IsInAccount { get; set; }
        public DateTime ? DateAdded { get; set; }
    }
}