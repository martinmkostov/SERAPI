﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class BimControlConfirmRequestModel
    {
        //public int Id { get; set; }
        public string TaskNumber { get; set; }
        public int ObjectId { get; set; }
        public int AccountId { get; set; }
        public int TaskId { get; set; }

        public string Owner { get; set; }
        public string Comment { get; set; }
        public List<BimControlItemModel> Items { get; set; }
    }

    public class BimControlItemModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public BimControlItemStatus Status { get; set; }
        public string Description { get; set; }

        public string LittraId { get; set; }
        public string ErrorDescription { get; set; }


        public int IssueId { get; set; }

        public List<BimControlItemImageModel> Photos { get; set; }
    }

    public enum BimControlItemStatus 
    {
        Green = 1,
        Red = 0
    }

    public class BimControlItemImageModel 
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string ImageBase64 { get; set; }
    }
}
