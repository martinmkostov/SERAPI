﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class LegalHolderModel
    {
        public int Id { get; set; }
        public string LegalHolder { get; set; }
        public int AccountId { get; set; }
        public bool IsActive { get; set; }
        public AccountModel Account { get; set; }
    }
}
