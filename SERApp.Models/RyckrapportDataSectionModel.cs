﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class RyckrapportDataSectionModel
    {
        public int RyckrapportDataSectionId { get; set; }
        public int RyckrapportDataId { get; set; }
        public int SectionId { get; set; }

        public SectionModel Section { get; set; }
        public RyckrapportDataModel RyckrapportData { get; set; }
    }
}
