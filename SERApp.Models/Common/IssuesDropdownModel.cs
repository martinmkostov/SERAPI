﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class IssuesDropdownModel
    {
        public List<DropdownModel> Contacts { get; set; }
        public List<DropdownModel> IssueType1 { get; set; }
        public List<DropdownModel> IssueType2 { get; set; }
    }
}
