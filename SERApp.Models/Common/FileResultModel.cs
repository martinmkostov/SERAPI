﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class FileResultModel
    {
        public byte[] Data { get; set; }
    }
}
