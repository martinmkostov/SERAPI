﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class EmailModel
    {
        
        public EmailModel(int accountId) 
        {
            this.AccountId = accountId;
        }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string BCC { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public List<string> AttachmentPaths { get; set; }
     }

    public class SaveEmailRecipient
    {
        public int AccountId { get; set; }
        public int ModuleId { get; set; }
        public string Recipient { get; set; }
    }
}
