﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class DateModel
    {
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
    }
}
