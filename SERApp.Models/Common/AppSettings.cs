﻿using System.Configuration;
using SERApp.Models.Constants;

namespace SERApp.Models.Common
{
    public class AppSettings
    {
        private static string _connectionString => ConfigSettings.SERAppEntitiesKey;
        private const string _server = "server";
        private const string _ssrsEndPoint = "SSRSEndPoint";

        public static string Server => ConfigurationManager.AppSettings[_server];
        public static string ConnectionString => ConfigurationManager.ConnectionStrings[$"{_connectionString}{Server}"].ConnectionString;
        public static string SSRSEndPoint => ConfigurationManager.AppSettings[$"{_ssrsEndPoint}{Server}"];
    }
}