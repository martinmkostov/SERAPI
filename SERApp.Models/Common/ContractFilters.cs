﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class ContractFilters
    {
        public string ContractNumber { get; set; }
        public int LegalHolderId { get; set; }
        public int OpponentHolderId { get; set; }

        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }

        public DateTime RemindDate { get; set; }

        public bool? Active { get; set; }
        public string Ramvtal { get; set; }
        public int CategoryId { get; set; }
        public bool isRemindList { get; set; }
        public int SiteId { get; set; }
        public int AccountId { get; set; }

        public string SearchText { get; set; }
        public int UserId { get; set; }
        public int TagWordId { get; set; }
        public List<int> ContractIds { get; set; }
    }
}
