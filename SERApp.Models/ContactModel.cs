﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ContactModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public int TenantContactId { get; set; }
        public int JobTypeId { get; set; }
        public string JobTypeName { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
       
    }
}
