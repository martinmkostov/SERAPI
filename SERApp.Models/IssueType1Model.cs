﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IssueType1Model
    {
        public int IssueType1Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
