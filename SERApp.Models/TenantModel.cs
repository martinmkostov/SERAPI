﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class TenantModel
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string EmailTemplate { get; set; }
        public string Image { get; set; }
        public string Phone { get; set; }
        public int PermiseType { get; set; }
        public string VisitingAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string InsuranceCompany { get; set; }
        public string SecurityCompany { get; set; }
        public string SiteManagerName { get; set; }
        public string SiteManagerEmail { get; set; }
        public string SiteManagerMobilePhone { get; set; }
        public string SiteManagerPhone { get; set; }
        public string ContractName { get; set; }
        public string ContractVatNo { get; set; }
        public string InvoiceAddress { get; set; }
        public string InvoiceZipAndCity { get; set; }
        public string ContactComments { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool? CCTV { get; set; }
        public bool? IntrusionAlarm { get; set; }
        public bool Flag { get; set; }
        public string PropertyDesignation { get; set; }
        public bool UseWrittenStatement { get; set; }
        public int TenantType { get; set; }
        public int SubPremiseType { get; set; }

        public string TenantTypeName { get; set; }
        public string PremiseTypeName { get; set; }
        public string SubPremiseTypeName { get; set; }
        public string AccountCompany { get; set; }

        public SiteModel Site { get; set; }

        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public bool IsFromPublicPage { get; set; }

        public string PostAddress { get; set; }
        public string PostZipCode { get; set; }
        public string PostCity { get; set; }

        public bool IsSelected { get; set; }
        public int AccountId { get; set; }

        public DateTime? LastUpdatedByCustomer { get; set; }
        public int OffSet { get; set; }
        public int LastUpdatedBy { get; set; }
        public string LastUpdatedByName { get; set; }
        public DateTime? LastSentUpdateEmail { get; set; }

        public string ImageUrl { get; set; }

        public bool IsSent { get; set; }
    }

    public class TenantContactModel : ContactModel
    {
        public int ContactId { get; set; }
        public int TenantId { get; set; }
    }

    public class TenantTypeModel {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IsSystem { get; set; }
    }

    public class PremiseTypeModel
    {
        public int Id { get; set; }
        public int TenantTypeId { get; set; }
        public string TenantTypeName { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IsSystem { get; set; }
    }

    public class SubPremiseTypeModel
    {
        public int Id { get; set; }
        public int PremiseTypeId { get; set; }
        public string PremiseTypeName { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IsSystem { get; set; }
    }

    public class JobTitleModel
    {
        public int Id { get; set; }
        public string TitleName { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public bool IsSystem { get; set; }
        public bool IncludedOnExcelExport { get; set; }
        public bool isChecked { get; set; }
    }

    public class TenantQrModel
    {
        public List<int> Ids { get; set; }
    }


}
