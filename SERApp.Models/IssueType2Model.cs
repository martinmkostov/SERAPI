﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IssueType2Model
    {
        public int IssueType2Id { get; set; }
        public string Name { get; set; }

        public int AccountId { get; set; }
    }
}
