﻿(function (ko) {
    var componentId = "loan-create";
    //private functions
    var ViewModel = function (params) {
        var self = this;
        self.searchCustomerEnabled = ko.observable(true);
        self.customerFieldsEnabled = ko.observable(false);
        self.customers = {};

        self.facilities = ko.observableArray([]);
        self.types = ko.observableArray([]);
        

        //events
        self.bindData = function () {
            getFacilities().done(function (response) {
                var data = JSON.parse(response).data;
                self.facilities(data);
            });

            getLoanTypes().done(function (response) {
                var data = JSON.parse(response).data;
                self.types(data);
            })
        }

        self.existensRadioClick = function (value) {
            if (value == 0) //exists
            {
                self.searchCustomerEnabled(true);
                self.customerFieldsEnabled(false);
            }
            else //new
            {
                self.searchCustomerEnabled(false);
                self.customerFieldsEnabled(true);
            }
            return true;
        }

        self.backToList = function () {
            ko.ext.renderComponent("#main-component-container", "loan-list");
        }

        self.initTypeAhead = function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    strs.map(function (item) {
                        if (substrRegex.test(item.Name)) {
                            matches.push(item);
                        }
                    });

                    cb(matches);
                };
            };

            $('#search-customer').typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            },
            {
                name: 'customers',
                source: substringMatcher(self.customers),
                display: 'Name',
                templates: {
                    empty: [
                        '<div class="empty-message">',
                        'No data',
                        '</div>'
                    ].join('\n'),
                    suggestion: function (data) {
                        return '<p><strong>' + data.Name + '</strong> - <span class="tt-selectable-detail">' + data.Address + ', ' + data.City + ', ' + data.ZipCode +'</span>';
                    }
                }
                });
        }

        self.initUI = function () {
            $('.form-control').addClass('input-sm');
            $('.datetimepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        }
        self.init = function () {
            //makes sure that the dom is loaded before the events trigger
            _.defer(function () {
                self.initUI();
                self.bindData();
                self.initTypeAhead();
            });
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);