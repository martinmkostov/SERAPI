﻿using Newtonsoft.Json;
using SERApp.Models;
using SERApp.Repository.Helpers;
using SERApp.Service.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPAppBlack.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerService _customerService;
        public CustomerController() {
            _customerService = new CustomerService();
        }

        [HttpGet]
        public JsonResult Get(int id)
        {
            var data = _customerService.Get(id);
            var response = JsonConvert.SerializeObject(new ResponseDataModel<CustomerModel>
            {
                success = true,
                message = "Successfully Loaded Customer Data",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAll() {
            var data = _customerService.GetAll();
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<CustomerModel>>
            {
                success = true,
                message = "Successfully Loaded Customer Data List",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCustomer(CustomerModel model) {
            try
            {
                _customerService.SaveCustomer(model);
                var response = JsonResponseHelper.JsonSuccess("Successfully Saved Customer Record");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var response = JsonResponseHelper.JsonError(ex.Message, ex.HResult);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult DeleteCustomer(int id)
        {
            try
            {
                _customerService.DeleteCustomer(id);
                var response = JsonResponseHelper.JsonSuccess("Successfully Deleted Customer Record");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var response = JsonResponseHelper.JsonError(ex.Message, ex.HResult);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
                
        }
    }
}