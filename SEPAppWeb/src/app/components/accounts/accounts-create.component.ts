import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AccountModel } from '../../models';
import { AccountService } from '../../services';

@Component({
    selector: 'app-accounts-create',
    templateUrl: './accounts-create.component.html',
    styleUrls: ['./accounts.component.scss']
})
export class AccountsCreateComponent implements OnInit {
    model : any;
    public showSpinner: boolean = false;
    public accountName: string = '';
    constructor(
        private accountService: AccountService, 
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router) {

        }

    public ngOnInit() : void {
        this.model = new AccountModel();
    }

    public changeCompany(){
        this.model.Name = this.model.Company.toLowerCase().split(' ').join('_');
    }

    public saveChanges() : void{
        this.showSpinner = true;
        this.accountService.saveAccount(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            let data = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success");
                setTimeout(() => {
                    this.router.navigate([`/accounts`]);
                }, 1000); 
            }
            this.ngOnInit();
        });
    }
}
