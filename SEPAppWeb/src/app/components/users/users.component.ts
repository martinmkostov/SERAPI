import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { UsersListComponent } from './users-list.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class UsersComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    @ViewChild('usersList') usersList: UsersListComponent;
    constructor() {}

    public ngOnInit() 
    {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.usersList.isAdmin = true;
            this.usersList.isAdminOrUserEditor = true;
            this.usersList.loadData(1, currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
            this.usersList.isAdminOrUserEditor = false;
            this.usersList.loadData(1, currentUser.accountId);
        }
        else if(currentUser.role === 'superadmin')
        {
            this.usersList.isSuperAdmin = true;
        }
    }
}
