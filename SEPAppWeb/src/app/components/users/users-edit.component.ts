import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../core/components';

import { AccountService, UserService, CommonService } from '../../services';
import { UserModel } from '../../models';

@Component({
    selector: 'app-users-edit',
    templateUrl: './users-edit.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class UsersEditComponent extends BaseComponent implements OnInit {
    // @ViewChild('accountsFilter') accountsFilterEl: ElementRef;
    // @ViewChild('roleFilter') roleFilterEl: ElementRef;
    public model: any;
    public selectedAccount: any;
    public accountsList: any;
    public userRolesList: any;
    public hideAccountSelect: boolean = true;
    public showSpinner: boolean = false;
    constructor(
        private accountService : AccountService,
        private commonService: CommonService,
        private userService: UserService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location) {
        super(location, router, route) 
        }

    public ngOnInit() {
        this.model = new UserModel();
        this.route.params.subscribe((params) => {
            if (params) {
                const accountId = params['account'];
                this.selectedAccount = accountId;
                this.model.AccountId = accountId;
                const id = params['id'];
                this.loadData(id);
            }
        });
        this.loadUserRoles();
        if(this.selectedAccount == null)
        {
            this.loadAccountData();
            this.hideAccountSelect = false;
        }
    }

    public loadData(id: number){
        this.showSpinner = true;
        this.userService.getUser(id)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            this.model = response.Data;
            this.model.PasswordReType = '';
            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public saveChanges(){
        this.showSpinner = true;
        this.userService.saveUser(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
            }
            this.ngOnInit();
        });
    }

    public loadAccountData(): void{
        let filter = '';
        this.accountService.getAccountsList(1, 100, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            this.accountsList = response.Data;
        });
    }

    public loadUserRoles(): void {
        //getAllUserRoles
        this.commonService.getAllUserRoles()
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            this.userRolesList = response.Data;
        });
    }

    public isPasswordMatched(){
        return this.model.Password ? this.model.PasswordReType === this.model.Password : true;
    }
}
