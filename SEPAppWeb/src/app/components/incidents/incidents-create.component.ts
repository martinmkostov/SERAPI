import { Component, OnInit } from '@angular/core';
import { IncidentService } from '../../services/incident.service';
import { SiteService, TenantService, CustomTypeService, GuardService } from '../../services';
import { IncidentTypeService } from '../../services/incident-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IncidentModel, GuardModel, IncidentGuardModel, IncidentTypeModel, ReportModel, ReportTypeModel } from '../../models';
import { Observable } from 'rxjs/Observable';
import {GrowlModule,Message} from 'primeng/primeng';

@Component({
  selector: 'app-incidents-create',
  templateUrl: './incidents-create.component.html',
  styleUrls: ['./incidents-create.component.scss']
})
export class IncidentsCreateComponent implements OnInit {
  isAdminOrUserEditor: boolean;
  photoSrc: string;
  SiteId: number = 0;
  tenants: any[];
  model : any;  
  incidenttypes: any[];
  selectedIncidentType: IncidentTypeModel = new IncidentTypeModel();
  customTypes: any[];
  sites: any[];
  reportTypes: any[] =[];
  reportTypeSelected: any;
  currentUser: any;
  SelectedGuards: any[] = [];
  Guards: any[] = [];
  Tenants: any[] = [];
  Reports: ReportModel[] = [];
  showGuards: boolean = false;
  public showSpinner: boolean = false;
  constructor(private incidentService: IncidentService, 
    private siteService: SiteService,
    private tenantService: TenantService,
    private incidentTypeService: IncidentTypeService,
    private customTypeService: CustomTypeService,
    private guardService: GuardService,    
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();
    this.model = new IncidentModel(); 
    this.model.CustomTypeId = 0;   
    this.model.IncidentTypeId = 0;
    this.model.TenantId = 0;
    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        this.sites = response.Data;
      //  this.toastr.success(response.Message, "Success"); 
    });  

    this.loadCustomTypes();
    this.loadIncidentTypes();
    this.loadGuards();
  }
  

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

loadTenant()
{
  this.tenants = [];
  this.tenantService.getTenantBySiteId(this.SiteId)
  .catch((err: any) => {
      return Observable.throw(err);
  })
  .finally(() => {
          setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response : any) => {
      
      this.tenants = response.Data;
     // this.toastr.success(response.Message, "Success"); 
  }); 

}

loadIncidentTypes()
{
    this.incidenttypes = [];
    this.incidentTypeService.getAllIncidentTypes(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.incidenttypes = response.Data;
       // this.toastr.success(response.Message, "Success"); 
    });  
};

loadCustomTypes()
{
    this.customTypes = [];
    this.customTypeService.getAllCustomTypes(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.customTypes = response.Data;
       // this.toastr.success(response.Message, "Success"); 
    });  
};

loadGuards()
{
    this.Guards = [];
    this.guardService.getAllGuards(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.Guards = response.Data;
        //this.toastr.success(response.Message, "Success"); 
    }); 
};


  public saveChanges() : void{
    this.showSpinner = true;

    this.model.IncidentGuard = [];

    this.SelectedGuards.forEach((guard)=>
    {
        var incidentGuardmodel = new IncidentGuardModel();
        incidentGuardmodel.GuardId = guard.Id;    
        this.model.IncidentGuard.push(incidentGuardmodel);

    });

    this.model.Reports = this.Reports;

    this.incidentService.saveIncident(this.model)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
            this.toastr.error(response.Message, "Error"); 
        }
        else {
            this.toastr.success(response.Message, "Success"); 
        }
        this.ngOnInit();
    });
}

addReport()
{
   if(this.reportTypeSelected != 0){
    var report = new ReportModel();
    report.ReportTypeId = this.reportTypeSelected;
    this.Reports.push(report);
   }
  
};

removeReport(index:number){
    this.Reports.splice(index,1);
};

loadReport(id:number){

  this.incidentTypeService.getIncidentType(id)
  .catch((err: any) => {
      return Observable.throw(err);
  })
  .finally(() => {
          setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response : any) => {      
    this.selectedIncidentType = response.Data;
    this.reportTypes = this.selectedIncidentType.ShownReport;
   
  }); 
}
getIfRequired(id:number) {
    if(this.selectedIncidentType.RequiredReport.some(e => e.Id === id)) {
       return true;
      }

      return false;
}


}
