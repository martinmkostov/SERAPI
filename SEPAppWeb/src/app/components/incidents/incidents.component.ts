import { Component, OnInit, ViewChild } from '@angular/core';
import { IncidentsListComponent } from './incidents-list.component';
import { Location } from '@angular/common';
import { BaseComponent } from '../../core/components';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-incidents',
  templateUrl: './incidents.component.html',
  styleUrls: ['./incidents.component.scss']
})
export class IncidentsComponent extends BaseComponent implements OnInit {

  @ViewChild('incidentsList') incidentList: IncidentsListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private location: Location) {
    super(location, router, route) 
}

  ngOnInit() {
    //let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    let currentUser = super.currentLoggedUser();
        if(currentUser.role === 'admin')
        {
            //alert(currentUser.accountId);
            this.isAdminOrUserEditor = true;
            this.incidentList.isAdminOrUserEditor = true;
            this.incidentList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.incidentList.isSuperAdmin = true;
            let secondaryUserData = super.secondaryAdminUser();
           // console.log(secondaryUserData.AccountId);
            if(secondaryUserData){
                this.incidentList.loadDataById(secondaryUserData.AccountId);
            }
        }
  }
}
