import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { GuardService } from '../../services';
import { Subject, Observable } from 'rxjs';
import { IPagedResults } from '../../core/interfaces';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';
@Component({
  selector: 'app-guard-list',
  templateUrl: './guard-list.component.html',
  styleUrls: ['./guard-list.component.scss']
})
export class GuardListComponent extends BaseComponent implements OnInit {
  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;5
  public isPreviewOn: boolean = false;
  constructor(
    private toastr: ToastsManager,
    private guardService: GuardService,
    public router: Router,
    private route: ActivatedRoute,
    private location: Location)  {
        super(location, router, route)
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin && !super.secondaryAdminUser())
    {        
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.guardService.getAllGuards(this.currentPage, this.itemPerPage, filter)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
        this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
     this.guardService.getAllGuards(this.currentPage, this.itemPerPage, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
        this.toastr.success(response.Message, "Success"); 
    }); 
}

public editGuard(id:number){
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}



}
