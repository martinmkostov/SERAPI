import { Component, OnInit } from '@angular/core';
import { GuardService, SiteService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { GuardModel, SiteModel } from '../../models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-guard-create',
  templateUrl: './guard-create.component.html',
  styleUrls: ['./guard-create.component.scss']
})
export class GuardCreateComponent implements OnInit {
  isAdminOrUserEditor: boolean;
  photoSrc: string;
  model : any;
  sites: any[];
  currentUser: any;

  public showSpinner: boolean = false;
  constructor(private guardService: GuardService, 
    private siteService: SiteService,
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();
    this.model = new GuardModel();    
    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        this.sites = response.Data;
        this.toastr.success(response.Message, "Success"); 
    });  
  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

  public saveChanges() : void{
    this.showSpinner = true;
    this.guardService.saveGuard(this.model)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
            this.toastr.error(response.Message, "Error"); 
        }
        else {
            this.toastr.success(response.Message, "Success"); 
        }
        this.ngOnInit();
    });
}

}
