import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../core/components';
import { CustomTypeService } from '../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToastsManager } from 'ng2-toastr';
import { CustomTypeModel } from '../../models';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-custom-type-edit',
  templateUrl: './custom-type-edit.component.html',
  styleUrls: ['./custom-type-edit.component.scss']
})
export class CustomTypeEditComponent extends BaseComponent implements OnInit {
  public model: any;
  public selectedAccount: any;
  public customTypesList: any;
  public hideCustomTypeSelect: boolean = true;
  public showSpinner: boolean = false;
  constructor(
    private customTypeService: CustomTypeService, 
    private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    this.model = new CustomTypeModel();
    this.model.AccountId = currentUser.accountId;
    this.route.params.subscribe((params) => {         
        if (params) {
            const id = params['id'];
            this.loadData(id);
        }
    });
  }

  public loadData(id: number){
    this.showSpinner = true;
    this.customTypeService.getCustomType(id)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        this.model = response.Data;
       // this.toastr.success(response.Message, "Success"); 
    });
}

public saveChanges(){
  this.showSpinner = true;
  this.customTypeService.saveCustomType(this.model)
  .catch((err: any) => {
      this.toastr.error(err, 'Error');
      return Observable.throw(err);
  }).finally(() => {
       setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response) => {
      this.toastr.success(response.Message, "Success"); 
      this.ngOnInit();
  });
}

}
