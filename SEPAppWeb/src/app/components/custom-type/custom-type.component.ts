import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomTypeListComponent } from './custom-type-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from '../../core/components';
import { Location } from '@angular/common';

@Component({
  selector: 'app-custom-type',
  templateUrl: './custom-type.component.html',
  styleUrls: ['./custom-type.component.scss']
})
export class CustomTypeComponent extends BaseComponent implements OnInit {
  @ViewChild('customTypeList') customTypeList: CustomTypeListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private location: Location) {
    super(location, router, route) 
}

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.customTypeList.isAdminOrUserEditor = true;
            this.customTypeList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.customTypeList.isSuperAdmin = true;
            let secondaryUserData = super.secondaryAdminUser();
            if(secondaryUserData){
                this.customTypeList.loadDataById(secondaryUserData.AccountId);
            }
        }
  }

}
