import { Component, OnInit, Injector } from '@angular/core';
import { CustomTypeService } from '../../services';
import { ToastsManager } from 'ng2-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-custom-type-list',
  templateUrl: './custom-type-list.component.html',
  styleUrls: ['./custom-type-list.component.scss']
})
export class CustomTypeListComponent implements OnInit {
  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;5
  public isPreviewOn: boolean = false;
  //public parentComponent : any;
  constructor(
   // private parentInj: Injector,
    private router: Router,
    private route:ActivatedRoute, 
    private toastr: ToastsManager,
    private customTypeService: CustomTypeService) 
    {
     // this.parentComponent = this.parentInj.get(CustomTypeComponent);
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin)
    {
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.customTypeService.getAllCustomTypes(this.currentPage, this.itemPerPage, filter)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
       // this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
    let filter = '';
    this.customTypeService.getAllCustomTypes(this.currentPage, this.itemPerPage, filter)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        
        this.totalItems = response.Data.length;
    });
}

public editCustomType(id:number){
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}




}
