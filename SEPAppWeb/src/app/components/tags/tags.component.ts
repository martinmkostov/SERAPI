import { Component, OnInit, ViewChild } from '@angular/core';
import { TagsListComponent } from './tags-list.component';
import { Location } from '@angular/common';
import { BaseComponent } from '../../core/components';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent extends BaseComponent implements OnInit {
  @ViewChild('tagList') tagList: TagsListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private location: Location) {
    super(location, router, route) 
}

  ngOnInit() {
    //let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    let currentUser = super.currentLoggedUser();
        if(currentUser.role === 'admin')
        {          
            this.isAdminOrUserEditor = true;
            this.tagList.isAdminOrUserEditor = true;
            this.tagList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.tagList.isSuperAdmin = true;
            let secondaryUserData = super.secondaryAdminUser();
            if(secondaryUserData){
                this.tagList.loadDataById(secondaryUserData.AccountId);
            }
        }
  }
}
