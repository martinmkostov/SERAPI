import { Component, OnInit } from '@angular/core';
import { TagService } from '../../services/tag.service';
import { SiteService, TenantService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { TagModel } from '../../models';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-tags-edit',
  templateUrl: './tags-edit.component.html',
  styleUrls: ['./tags-edit.component.scss']
})
export class TagsEditComponent implements OnInit {

  photoSrc: string;
  isAdminOrUserEditor: boolean;
  sitesList: any[];
  tenants: any[];
  currentUser: any;
  showSpinner: boolean;
  model: TagModel = <TagModel>
  {    
  };
  constructor(private tagService: TagService,
    private route: ActivatedRoute,
    private siteService: SiteService,
    private tenantService: TenantService,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.setDefaults();

    this.route.params.subscribe((params) => {         
      if (params) {
          const id = params['id'];
          this.loadData(id);
      }
    });


    this.siteService.getSitesByAccountList(this.currentUser.accountId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
   
        this.sitesList = response.Data;
        this.toastr.success(response.Message, "Success"); 
    });  
  }

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}

  public loadData(id: number){
    this.showSpinner = true;
    
    
    this.tagService.getTag(id)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
      this.model = response.Data;      
     
      //alert(this.model.TenantID);
      this.tenantService.getTenantBySiteId(this.model.SiteId)
      .catch((err: any) => {
          return Observable.throw(err);
      })
      .finally(() => {
              setTimeout(() => {
              this.showSpinner = false;
          }, 1000);
      })
      .subscribe((response : any) => {
          
          this.tenants = response.Data;
      
        //  this.toastr.success(response.Message, "Success"); 
      }); 

      
     // this.toastr.success(response.Message, "Success"); 
    });      
}


public saveChanges() : void{  
  this.showSpinner = true;
  this.tagService.saveTag(this.model)
  .catch((err: any) => {
      this.toastr.error(err, 'Error');
      return Observable.throw(err);
  }).finally(() => {
       setTimeout(() => {
          this.showSpinner = false;
      }, 1000);
  })
  .subscribe((response) => {
      if(response.ErrorCode)
      {
          this.toastr.error(response.Message, "Error"); 
      }
      else {
          this.toastr.success(response.Message, "Success"); 
      }
      this.ngOnInit();
  });
}

loadTenant()
  {
  
    this.tenants = [];
    this.tenantService.getTenantBySiteId(this.model.SiteId)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.tenants = response.Data;
      //  this.toastr.success(response.Message, "Success"); 
    }); 

  }

 
}
