import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { BaseComponent } from '../../../core/components';

import { AccountsModuleListComponent } from './accounts-module-list.component';

import { ToastsManager } from 'ng2-toastr';

import { AccountService } from '../../../services';

@Component({
    selector: 'app-account-module',
    templateUrl: './accounts-module.component.html',
    styleUrls: ['../../layout/layout.component.scss']
})
export class AccountModuleComponent extends BaseComponent implements OnInit {
    @ViewChild('accountsModuleList') accountsModuleList: AccountsModuleListComponent;
    @ViewChild('accountsFilter') accountsFilterEl: ElementRef;
    public accountsList : any;
    public searchInput: string = "";
    public selectedAccount: any;
    public selectedAccountName: string = "";
    constructor(
        private accountService : AccountService, 
        private toastr: ToastsManager,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location) {
            super(location, router, route);
        }

    public ngOnInit() {
        this.loadData();
    }

    public loadData(){
        let filter = '';
        this.accountService.getAccountsList(1, 100, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
            
        })
        .subscribe((response : any) => {
            this.accountsList = response.Data;
            //this.toastr.success(response.Message, "Success"); 
        });
    }

    public search(){
        let value = this.searchInput;
        if(value !== '')
        {
            this.accountsModuleList.loadData(1, 0, value);
        }
        else {
            this.accountsModuleList.loadData();
        }
    }

    public onFilterChange(evt: any, account){
        let accountId = super.getTextFromDropDownList(this.accountsFilterEl);
        let selectedAccount = this.accountsList.filter((item) => {
            return item.Id === Number(accountId);
        });

        this.selectedAccount = selectedAccount[0];
        this.selectedAccountName = this.selectedAccount.Name;
        this.accountsModuleList.loadData(1, Number(accountId));
    }


}
