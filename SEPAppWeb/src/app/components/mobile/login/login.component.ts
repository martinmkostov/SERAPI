import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { BaseComponent } from '../../../core/components';

import { AuthenticationService } from '../../../services';
@Component({
    selector: 'app-mobile-login',
    templateUrl: './login.component.html',
    styleUrls: ['../../login/login.component.scss']
})
export class MobileLoginComponent implements OnInit {
    model: any = {}; 
    username : any = '';
    public showSpinner: boolean = false;

    private staticSuperAdminUserName = 'superadmin';
    private staticSuperAdminPassword = 'superadmin';
    constructor(
        private authService: AuthenticationService,
        private route: ActivatedRoute,
        private toastr: ToastsManager,
        private router: Router) 
        {

        }


    public ngOnInit(): void {}

    public onLoggedin(): void 
    {
        let isLoggedIn = false;
        let role = 'user';
        let displayName = '';
        this.showSpinner = true;

        if(this.model.UserName === this.staticSuperAdminUserName && 
            this.model.Password === this.staticSuperAdminPassword){
                isLoggedIn = true;
                let userData = {
                    id: -1,
                    isLoggedIn : isLoggedIn,
                    username: this.model.UserName,
                    role: 'superadmin',
                    displayName: 'SUPERADMIN',
                    accountId: 0,
                    currentUserRaw : []
                };
                
                localStorage.setItem('currentUser', JSON.stringify(userData));
                localStorage.setItem('isLoggedin', JSON.stringify(isLoggedIn));
                this.router.navigate(['/dashboard']);
                //this.toastr.success('Welcome Super Admin!', "Success"); 
        }
        else 
        {
            //log users
            this.logInUser();
        }
    }
    //@TODO : make this code shareable - this is used on the authguard
    public logInUser(userName?:string, password?:string){
        if(userName && password)
        {
            this.model = {
                UserName: userName,
                Password : password
            }
        }
        this.authService.auth(this.model)
            .catch((err: any) => {
                this.toastr.error(err, 'Error');
                return Observable.throw(err);
            }).finally(() => {
                setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response) => {
                if(response.Success){
                    let currentUser = response.Data.CurrentUser;
                    let displayName = currentUser.FirstName || currentUser.LastName ? 
                        currentUser.FirstName + ' ' + currentUser.LastName : currentUser.UserName
                        
                    let userData = {
                        id : currentUser.Id,
                        isLoggedIn : true,
                        username: currentUser.UserName,
                        role: currentUser.RoleName,
                        displayName: displayName,
                        accountId: currentUser.AccountId,
                        currentUserRaw : currentUser
                    };
                    localStorage.setItem('currentUser', JSON.stringify(userData));
                    localStorage.setItem('isLoggedin', JSON.stringify(true));
                    if((!currentUser.FirstName && !currentUser.LastName) || currentUser.RoleName === 'user')
                    {
                        this.router.navigate([`/users/${currentUser.Id}/edit`]); // force change account
                    }
                    else {
                        this.router.navigate(['mobile/loans']);
                    }
                    //this.toastr.success(response.Message, "Success"); 
                }
                else {
                    //this.toastr.error(response.Message, "Error"); 
                }
            });
    }
}
