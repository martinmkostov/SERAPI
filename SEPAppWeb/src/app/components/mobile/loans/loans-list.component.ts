import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../../core/interfaces';

import { ToastsManager } from 'ng2-toastr';

import { LoanService } from '../../../services';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
    selector: 'app-mobile-loans-list',
    templateUrl: './loans-list.component.html',
    styleUrls: ['../../layout/layout.component.scss', '../../layout/mobile.css']
})
export class MobileLoansListComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    public isPreviewOn: boolean = false;
    public parentComponent : any;

    public accountId: number = 0;
    public withData: boolean = false;
    public searchChangeObserver;
    constructor( 
        private parentInj: Injector,
        private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private loanService: LoanService) {
        }

    packageData: Subject<any> = new Subject<any[]>();  

    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.accountId = currentUser.accountId;
        //this.loadDataById(this.accountId);
    }


    public loadDataById(id:number, keyword:string = ""){
        this.showSpinner = true;
        this.loanService.getLoanByAccountId(id, keyword)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
            this.withData = response.Data.length > 0;
            if(response.Data.length === 1)
            {
                this.navigateDetails(response.Data[0].Id);   
            }
        });
    }

    public navigateDetails(id?: number){
        this.router.navigate([`mobile/loans/${id}/return`]);
    }
    
    public onSearchChange(searchValue: string){
        this.showSpinner = true;
        if (!this.searchChangeObserver) {
            Observable.create(observer => {
                this.searchChangeObserver = observer;
            }).debounceTime(500)
              .distinctUntilChanged()
              .subscribe((term) => {
                    let keyword = term;
                    this.loadDataById(this.accountId, keyword);
              });
        }

        this.searchChangeObserver.next(searchValue);
    }

    public navigateToLanding(){
        this.router.navigate([`mobile/loans/`]);
    }
}
