import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { LoanService } from '../../../services';

import { LoanModel } from '../../../models';

@Component({
    selector: 'app-mobile-loans-return',
    templateUrl: './loans-return.component.html',
    styleUrls: ['../../layout/mobile.css', '../../layout/layout.component.scss']
})
export class MobileLoansReturnComponent implements OnInit { 
    model : any;
    public withData: boolean = true;
    public showSpinner: boolean = false;
    public hidden: string = 'hidden';
    public isDetail: boolean = false;
    constructor(
        private loanService: LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastsManager
    ) {}
    public ngOnInit() { 
        this.model = new LoanModel();
        this.route.params.subscribe((params) => {
            const id = params['id'];
            if(id) {
                this.loadData(id);
                this.isDetail = true;
            }
        });
    }

    public search(){
        this.showSpinner = true;
        this.hidden = 'hidden';
        this.loanService.getLoanByLoanId(this.model.LoanId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            if(response.ErrorCode || !this.model)
            {
                if(response.ErrorCode)
                {
                    //this.toastr.error(response.Message, "Error"); 
                }
                
                this.model = new LoanModel();
                this.withData = false;
                this.hidden = 'hidden';
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.withData = true;
                this.hidden = '';
            }
        });
        
    }

    public returnLoan(){
        this.loanService.returnLoan(this.model)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
 
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                //this.toastr.success(response.Message, "Success"); 
                this.loadData(this.model.Id);
            }
        });
    }

    public loadData(id: number){
        this.showSpinner = true;
        this.hidden = 'hidden';
        this.loanService.getLoan(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model = response.Data;
            //this.toastr.success(response.Message, "Success"); 
            this.withData = true;
            this.hidden = '';
        });
    }

    public navigateDetails(id?: number){
        this.router.navigate([`mobile/loans/${id}/preview`]);
    }

    public navigateToLanding(){
        this.router.navigate([`mobile/loans/`]);
    }
}
