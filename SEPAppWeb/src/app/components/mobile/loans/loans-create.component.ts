import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IPagedResults } from '../../../core/interfaces';
import { LoanModel } from '../../../models';
import { LoanService, TenantService, SiteService } from '../../../services';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { SecurityHelper } from '../../../shared/helpers'

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';

import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
    selector: 'app-mobile-loans-create',
    templateUrl: './loans-create.component.html',
    styleUrls: ['../../layout/layout.component.scss', '../../layout/mobile.css'],
    //  animations: [
    //  trigger('toggleHeight', [
    //         state('hide', style({
    //             height: '0px',
    //             opacity: '0',
    //             overflow: 'hidden',
    //             // display: 'none'
    //         })),
    //         state('show', style({
    //             height: '*',
    //             opacity: '1',
    //             // display: 'block'
    //         })),
    //         transition('hide => show', animate('200ms ease-in')),
    //         transition('show => hide', animate('200ms ease-out'))
    //     ])
    // ],
    // styles: [`
    //     animation { display: block; }
    // `]
    animations: [
        trigger('slideIn', [
            state('*', style({ 'overflow-y': 'hidden', 'overflow-x' : 'hidden' })),
            state('void', style({ 'overflow-y': 'hidden', 'overflow-x' : 'hidden' })),
            transition('* => void', [
                style({ height: '*' }),
                animate(300, style({ height: 0 }))
            ]),
            transition('void => *', [
                style({ height: '0' }),
                animate(300, style({ height: '*' }))
            ])
        ])
    ]
})
export class MobileLoansCreateComponent implements OnInit {
    @ViewChild('imagePreview') imagePreviewEl: ElementRef;
    @ViewChild('imageUpload') imageUploadEl: ElementRef;
    //FORM
    public loanForm: any;

    isAdminOrUserEditor: boolean;
    model : any;
    currentUser: any;
    tenantsList : any;
    customersList: any;
    sitesList: any;
    loanTypesList: any;
    loanedDate: NgbDateStruct;
    returnedDate: NgbDateStruct;
    defaultDaysReturn: number = 365; //1 YEAR FROM LOANING
    photoSrc: string = "";
    fileName: string = "";
    public showSpinner: boolean = false;
    public finalCustomerList: any;
    public filteredCustomers: any;
    //STEPS
    isStep1: boolean = true;
    isStep2: boolean = false;
    isStep3: boolean = false;
    isStep4: boolean = false;
    isStep5: boolean = false;

    currentStep: any;

    //SEARCHING
    public searching = false;
    public searchFailed = false;
    public customerId: number = 0;
    public customerName: string = '';
    public hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false); 

    @ViewChild('customerSearch') customerSearchInput: ElementRef;

    constructor(
        private loanService : LoanService,
        private tenantService : TenantService,
        private siteService: SiteService,
        private toastr: ToastsManager,
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder
    ) {}

    public ngOnInit() { 
        this.model = new LoanModel();
        this.setDefaults();
        this.loadAllMergedCustomers();
        this.loadSites(this.currentUser.accountId);
        this.loadLoanTypes();

        this.loanForm = this.formBuilder.group({
            'Company': ['', Validators.required]
        });

    }

    public setDefaults(){
        let now = new Date();
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
        }
        else if(this.currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }

        
        this.loanedDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        let yearAfter = new Date(now.setDate(now.getDate() + this.defaultDaysReturn));
        this.returnedDate = { year: yearAfter.getFullYear(), month: yearAfter.getMonth() + 1, day: yearAfter.getDate() }

        this.model.ReceivedBy = this.currentUser.displayName;
        this.model.CreatedBy = this.currentUser.displayName;
        this.model.LastModifiedBy = this.currentUser.displayName;

        this.model.LoanId = SecurityHelper.createSixDigitLoanId();
        this.currentStep = 1;
    }

    public loadSites(id? :number){
        this.siteService.getSitesByAccountList(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.sitesList = response.Data;
        });
    }

    public loadLoanTypes(){
        this.loanService.getLoanTypes()
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.loanTypesList = response.Data;
        });
    }

    public loadAllMergedCustomers(){
        this.loanService.getAllMergedCustomerList(this.currentUser.accountId)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.finalCustomerList = response.Data;
        });
    }

    public loadTenant(id){
        this.tenantService.getTenant(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            let tenantData = response.Data;
            this.model.CustomerTypeId = 2;

            let city = tenantData.InvoiceZipAndCity && tenantData.InvoiceZipAndCity.indexOf(',') > -1 ? tenantData.InvoiceZipAndCity.split(",")[1] : '';
            let zipCode = tenantData.InvoiceZipAndCity && tenantData.InvoiceZipAndCity.indexOf(',') > -1 ? tenantData.InvoiceZipAndCity.split(",")[0] : '';

            this.model.Customer = {
                Name : tenantData.Name,
                Company: tenantData.Name,
                Email: tenantData.Email,
                MobilePhone : tenantData.Phone,
                Address1 : tenantData.VisitingAddress,
                City : city,
                ZipCode : zipCode
            };

            this.model.GivenBy = tenantData.SiteManagerName;
        });
        
    }

    public loadCustomer(id){
        this.loanService.getCustomer(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : any) => {
            this.model.Customer = response.Data;
            this.model.GivenBy = response.Data.Name;
            this.model.CustomerTypeId = 1;
        });
    }

    public loadData(id, type){
        if(type === 'Customer')
        {
            this.loadCustomer(id)
        }
        else {
            this.loadTenant(id);
        }
    }

    public saveChanges(){
        this.showSpinner = true;
        this.model.LoanedDate = new Date(
            this.loanedDate.year, 
            this.loanedDate.month, 
            this.loanedDate.day);

        this.model.ReturnedDate = new Date(
            this.returnedDate.year, 
            this.returnedDate.month, 
            this.returnedDate.day);

        this.loanService.saveLoan(this.model)
        .catch((err: any) => {
            this.toastr.error(err, 'Error');
            return Observable.throw(err);
        }).finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response) => {
            if(response.ErrorCode)
            {
                this.toastr.error(response.Message, "Error"); 
            }
            else {
                let id = response.Data.Id;
                //this.toastr.success(response.Message, "Success"); 
                this.router.navigate([`mobile/loans/${id}/preview`]);
            }
            
        });
    }

    public fileChange(fileInput: any){
        let self = this;
        let file = <File>fileInput.target.files[0];
         if(file.size > 3000000)
        {
            this.toastr.error(`Photo upload limits 3MB of file, this file is ${ (file.size / 1024 / 1024).toFixed(2) } MB. Please choose another one.`, "Error");
            return false;
        }
        this.fileName = file.name;
        var reader = new FileReader();
        reader.onload = function () {
            self.photoSrc = reader.result;
            self.model.PhotoString = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };

        reader.readAsDataURL(file);
    }

    public navigateToLanding(){
        this.router.navigate([`mobile/loans/`]);
    }
    
    public navigateToStep(step: number){
        if(step === 2){
            this.isStep1 = false;
            this.isStep2 = true;
            this.isStep3 = false;
            this.isStep4 = false;
            this.currentStep = 2;

            this.model.ItemNameId = !this.model.ItemNameId ? '' : this.model.ItemNameId;
            this.model.GivenBy = !this.model.GivenBy ? '' : this.model.GivenBy;
            this.model.ReceivedBy = !this.model.ReceivedBy ? '' : this.model.ReceivedBy;
            this.model.Description = !this.model.Description ? '' : this.model.Description;
        }
        else if(step === 3){
            this.isStep1 = false;
            this.isStep2 = false;
            this.isStep3 = true;
            this.isStep4 = false;
            this.currentStep = 3;
              if(!this.photoSrc)
            {
                setTimeout(() => {
                    let imageUploadElement: HTMLElement = document.getElementById('mobile-upload') as HTMLElement;
                    imageUploadElement.click();
                }, 500);
            } 

        }
        else if(step === 4){
            this.isStep1 = false;
            this.isStep2 = false;
            this.isStep3 = false;
            this.isStep4 = true;
            this.currentStep = 4;
        }
        else {
            this.isStep1 = true;
            this.isStep2 = false;
            this.isStep3 = false;
            this.isStep4 = false;
            this.currentStep = 1;
        }
    }

    public selectCustomer(customerId: number, type: string): void {
        this.model.CustomerId = customerId;
        this.showSpinner = true;
        this.customerSearchInput.nativeElement.value = customerId;
        this.loadData(customerId, type);

    }

    search = (text$: Observable<string>) =>
    text$
        .debounceTime(200)
        .distinctUntilChanged()
        .do(() => this.searching = true)
        .switchMap(term => {
            if (term.trim() === '') {
                return Observable.of([]);
            }
            return this.loanService.getAllMergedCustomerFilterList(this.currentUser.accountId, term);
        })
        .do(() => this.searching = false)
        .merge(this.hideSearchingWhenUnsubscribed);
    formatter = (x: { Name: string }) => x.Name;
}
