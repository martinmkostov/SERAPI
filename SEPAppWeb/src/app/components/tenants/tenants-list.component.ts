import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';
import { routerTransition } from '../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { TenantService } from '../../services';

@Component({
    selector: 'app-tenants-list',
    templateUrl: './tenants-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class TenantsListComponent implements OnInit {
    public showSpinner: boolean = false;
    public itemPerPage: number = 100;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public isAdminOrUserEditor = false;
    public isSuperAdmin = false;
    constructor( 
        private router: Router,
        private route:ActivatedRoute, 
        private toastr: ToastsManager,
        private tenantervice: TenantService) {}

    packageData: Subject<any> = new Subject<any[]>();  

    public ngOnInit() {
        if(this.isSuperAdmin)
        {
            this.loadData();
        }
    }

    public loadData(page?: number, id?: number){
        this.showSpinner = true;
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 

        this.tenantervice.getTenantList(this.currentPage, this.itemPerPage, filter)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
                setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
            //this.toastr.success(response.Message, "Success"); 
        });
        
    }

    public loadDataById(id:number){
        this.showSpinner = true;
        this.tenantervice.getTenantByAccountId(id)
        .catch((err: any) => {
            return Observable.throw(err);
        })
        .finally(() => {
             setTimeout(() => {
                this.showSpinner = false;
            }, 1000);
        })
        .subscribe((response : IPagedResults<any>) => {
            this.packageData.next(response.Data);
            this.totalItems = response.Data.length;
        });
    }

    public editTenant(id:number){
        this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
    }

}
