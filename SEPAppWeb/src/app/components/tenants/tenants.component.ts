import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { TenantsListComponent } from './tenants-list.component';

@Component({
    selector: 'app-sites',
    templateUrl: './tenants.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class TenantsComponent implements OnInit {
    @ViewChild('tenantsList') tenantList: TenantsListComponent;
    isAdminOrUserEditor: boolean;
    constructor() {}

    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.tenantList.isAdminOrUserEditor = true;
            this.tenantList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.tenantList.isSuperAdmin = true;
        }
    }
}
