import { Component, OnInit, ViewChild } from '@angular/core';
import { TagLogsListComponent } from './tag-logs-list.component';

@Component({
  selector: 'app-tag-logs',
  templateUrl: './tag-logs.component.html',
  styleUrls: ['./tag-logs.component.scss']
})
export class TagLogsComponent implements OnInit {

  @ViewChild('tagLogList') tagLogList: TagLogsListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor() { }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.tagLogList.isAdminOrUserEditor = true;
            this.tagLogList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.tagLogList.isSuperAdmin = true;
        }
  }
}
