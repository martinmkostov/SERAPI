import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Location } from '@angular/common';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../../../core/interfaces';
import { routerTransition } from '../../../../router.animations';

import { ToastsManager } from 'ng2-toastr';

import { ManagerService } from '../../../../services';
import { BaseComponent } from '../../../../core/components';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends BaseComponent implements OnInit {
    isActive: boolean = false;
    showModule = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    currentUser: any;
    isSuperAdmin: boolean = false;
    public showSpinner: boolean = false;
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public modules: Array<any>;

    public accountId : number = 0;
    public secondaryUserData : any;
    public superAdminAccess : any;
    public withSecondaryUser: boolean = false;

    public withSuperAccess: boolean = false;

    constructor(
        private translate: TranslateService, 
        public router: Router,
        private route: ActivatedRoute,
        private managerService: ManagerService,
        private location: Location
        ) {
        super(location, router, route);
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events
        .filter(e => e instanceof NavigationEnd)
        .subscribe((val : any) =>  {
            if 
            (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) 
            {
                this.toggleSidebar();
                
            }
        });
    }

    public ngOnInit(){
        let currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = currentUserData;
        this.withSecondaryUser = super.withSecondaryUserData();
        this.withSuperAccess = super.hasSuperAccess();
        if(this.currentUser.role == 'superadmin'){
            this.isSuperAdmin = true;
            this.addExpandClass('administration')
            if(this.withSecondaryUser)
            {
                this.accountId = super.secondaryAdminUser().AccountId;
                this.loadModulesByAccount(this.accountId);
            }
        }
        // if(this.secondaryUserData && Number(this.superAdminAccess) === 1) {
        //     this.withSecondaryUserData = true;
        //     this.accountId = this.secondaryUserData.AccountId;
        //     this.loadModulesByAccount(this.secondaryUserData.AccountId);
        //     //window.open(`/accounts/${this.model.AccountId}/edit?superAdminAccess=1&accountId=${this.model.AccountId}`);
        //     debugger;
        //     let url = window.location.href;
        //     if(url.indexOf('superAdminAccess') === -1)
        //     {
        //         url += `?superAdminAccess=1&accountId=${this.accountId}`;
        //         this.location.go(url);
        //     }
        // }
        else {
            this.accountId = this.currentUser.accountId;
            this.loadModulesByAccount(this.currentUser.accountId);
        }
        
    }

    public loadModulesByAccount(id:number){
        this.managerService.getModulesByAccountIdList(id)
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                let data = response.Data;
                let adminDefaultModuleNodes = [
                    {'ShortName' : 'administration', 'level' : 'main' }, 
                    {'ShortName' : 'accounts' , 'level' : 'sub'},
                    {'ShortName' : 'users' , 'level' : 'sub'}, 
                    {'ShortName' : 'sites' , 'level' : 'sub'}];
                let dataModules = data.map((item) => {
                    return {
                        ShortName : item.ShortName,
                        level: item.NodeLevel
                    }
                });
                this.modules = dataModules.concat(adminDefaultModuleNodes);
                this.modules.map((item) => {
                    this.setModuleVisibilityByRole(item.ShortName);
                })
            });
    }

    public eventCalled() {
        this.isActive = !this.isActive;
    }

    public addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    public toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    public rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    public changeLang(language: string) {
        this.translate.use(language);
    }

    public onLoggedout() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('secondaryAdminUser');
    }

    public setModuleVisibilityByRole(moduleName:string){
        let nodeItems = [];
        if(this.currentUser.role === 'superadmin')
        {
            let superAdminModules = { 
            'modules': [
               {'ShortName' : 'dashboard', 'level' : 'main' }, 
               {'ShortName' : 'administration', 'level' : 'main' }, 
               {'ShortName' : 'modules' , 'level' : 'sub'}, 
               {'ShortName' : 'accounts' , 'level' : 'sub'},
               {'ShortName' : 'account-modules' , 'level' : 'sub'}, 
               {'ShortName' : 'users' , 'level' : 'sub'}, 
               {'ShortName' : 'sites' , 'level' : 'sub'}] 
            };
            let superAdminModuleNodes = superAdminModules['modules'];
            if(super.hasSuperAccess()) {
                nodeItems = this.modules;
            }
            else {
                nodeItems = superAdminModuleNodes;
            }
            
        }
        else if(this.currentUser.role === 'admin')
        {
            nodeItems = this.modules;
        }
        else if(this.currentUser.role === 'user'){
            //default modules by account
            let userModuleNodes = [];
            userModuleNodes = this.modules;
            nodeItems = userModuleNodes;
             //should depend on the assigned information
             //code here should overwrite the account module assigned
        }
        if(nodeItems && nodeItems.length > 0)
        {
            let nodesFiltered = nodeItems.filter(function(item){
                return moduleName === item.ShortName;
            });
            return nodesFiltered.length > 0;
        }
        return false;
    }
}
