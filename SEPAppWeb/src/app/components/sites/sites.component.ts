import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { SitesListComponent } from './sites-list.component';

@Component({
    selector: 'app-sites',
    templateUrl: './sites.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class SitesComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    @ViewChild('sitesList') siteList: SitesListComponent;
    constructor() {}

    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.siteList.isAdminOrUserEditor = true;
            this.siteList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
            this.siteList.isAdminOrUserEditor = false;
            this.siteList.loadData(1, currentUser.accountId);
        }
        else if(currentUser.role === 'superadmin')
        {
            this.siteList.isSuperAdmin = true;
        }
    }
}
