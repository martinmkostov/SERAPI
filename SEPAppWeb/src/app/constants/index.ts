
let apiPath = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:55299/' : 'http://ser4api.itmaskinen.se/';
let sitePath = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:60745/' : 'http://ser4.itmaskinen.se';
let fileServerPath = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:9100/' : 'http://ser4files.itmaskinen.se';

export const API_URL = apiPath;
export const SITE_URL = sitePath;
