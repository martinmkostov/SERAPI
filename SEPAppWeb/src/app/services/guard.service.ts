import { Injectable } from '@angular/core';
import { BaseDataService, DataService } from '../core/services';
import { API_URL } from '../constants';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GuardService extends BaseDataService {

  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllGuards(page:number, pageSize: number = 10, filter: any): Observable<any>{
    let url = `Guard/GetAll`;    
    return super.getAll<any>(url);
  }

  public getGuard(id?: number): Observable<any>{
    let url = `Guard/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveGuard(data){
    let url = `Guard/Save`;
    return super.save<any>(url,data);
  }

  public deleteGuard(data){
      let url = `Guard/Delete`;
      return super.save<any>(url,data);
  }


}
