import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class ManagerService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    
    public getAccountsModuleList(page:number, pageSize: number = 10): Observable<IPagedResults<any>>{
        let url = `Account/Module/GetAll`;    
        return super.getAll<any>(url);
    }

    public getModulesByAccountIdList(id:number): Observable<IPagedResults<any>>{
        let url = `Account/Module/GetByAccountId?accountId=${id}`;    
        return super.getAll<any>(url);
    }

    public getAccountsModuleListById(id:number): Observable<IPagedResults<any>>{
        let url = `Account/Module/GetByAccount?id=${id}`;    
        return super.getAll<any>(url);
    }

    public saveAccountsModule(data){
        let url = `Account/Module/Save`;    
        return super.save<any>(url,data);
    }

    public deleteAccountsModule(data){
        let url = `Account/Module/Delete`;    
        return super.save<any>(url,data);
    }
}
