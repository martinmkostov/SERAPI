import { Injectable } from '@angular/core';
import { BaseDataService, DataService } from '../core/services';
import { API_URL } from '../constants';
import { Observable } from 'rxjs';

@Injectable()
export class TagLogService extends BaseDataService {

  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllTagLogs(page:number, pageSize: number = 10, filter: any): Observable<any>{
    let url = `TagLog/GetAll`;    
    return super.getAll<any>(url);
  }

  public getTagLog(id?: number): Observable<any>{
    let url = `TagLog/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveTagLog(data){
    let url = `TagLog/Save`;
    return super.save<any>(url,data);
  }

  public deleteTagLog(data){
      let url = `TagLog/Delete`;
      return super.save<any>(url,data);
  }

}
