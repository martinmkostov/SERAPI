import { Injectable } from '@angular/core';
import { BaseDataService, DataService } from '../core/services';
import { API_URL } from '../constants';
import { Observable } from 'rxjs';

@Injectable()
export class IncidentTypeService extends BaseDataService{

  constructor(protected dataService: DataService) 
  {
    super(dataService, API_URL);
  }

  public getAllReportTypes(): Observable<any>{
    let url = `IncidentType/GetReportTypes`;    
    return super.getAll<any>(url);
  }

  public getAllIncidentTypes(page:number, pageSize: number = 10, filter: any): Observable<any>{
    let url = `IncidentType/GetAll`;    
    return super.getAll<any>(url);
  }

  public getIncidentType(id?: number): Observable<any>{
    let url = `IncidentType/Get?id=${id}`;
    return super.getAll<any>(url);
  }

  public saveIncidentType(data){
    let url = `IncidentType/Save`;
    return super.save<any>(url,data);
  }

  public deleteIncidentType(data){
      let url = `IncidentType/Delete`;
      return super.save<any>(url,data);
  }

}
