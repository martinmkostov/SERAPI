import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod, URLSearchParams, Headers, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { UtilityService } from './utility.service';
import { DataServiceOptions } from '../models/dataServiceOptions';

@Injectable()
export class DataService {
    // Define the internal Subject we'll use to push the command count
    public pendingCommandsSubject = new Subject<number>();
    public pendingCommandCount = 0;

    // Provide the *public* Observable that clients can subscribe to
    public pendingCommands$: Observable<number>;

      constructor(public http: Http, public us: UtilityService) {
        this.pendingCommands$ = this.pendingCommandsSubject.asObservable();
    }

    public get(url: string, params?: any): Observable<Response> {
        const options = new DataServiceOptions();
        options.method = RequestMethod.Get;
        options.url = url;
        options.params = (params ? (params.params ? params.params : params) : '');
        options.headers = (params ? (params.headers ? params.headers : '') : '');;
        return this.request(options);
    }

   public post(url: string, data?: any, params?: any): Observable<Response> {
        if (!data) {
            data = params;
            params = {};
        }
        const options = new DataServiceOptions();
        options.method = RequestMethod.Post;
        options.url = url;
        options.params = params;
        options.data = data;
        return this.request(options);
    }

    public put(url: string, data?: any, params?: any): Observable<Response> {
        if (!data) {
            data = params;
            params = {};
        }
        const options = new DataServiceOptions();
        options.method = RequestMethod.Put;
        options.url = url;
        options.params = params;
        options.data = data;
        return this.request(options);
    }

    public delete(url: string): Observable<Response> {
        const options = new DataServiceOptions();
        options.method = RequestMethod.Delete;
        options.url = url;
        return this.request(options);
    }

    public downloadBlob(url: string, MimeType: string) {
        // TODO: we may need to add header to pass the token if token is not passed via query params
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map((res) => {
                return new Blob([res.blob()], { type: MimeType });
            });
    }

    // Private functions
    private request(options: DataServiceOptions): Observable<any> {
        options.method = (options.method || RequestMethod.Get);
        options.url = (options.url || '');
        options.headers = (options.headers || {});
        options.params = (options.params || {});
        options.data = (options.data || {});

        // this.addXsrfToken(options);
        // this.addAllowHeaders(options);
        this.addContentType(options);
        this.addAuthToken(options);      // Rob: TODO - Bring this back when we have the login module completed with authentication token set up... I am guessing we are ony storing simple token in local storage for now

        const requestOptions = new RequestOptions();
        requestOptions.method = options.method;
        requestOptions.url = options.url;
        requestOptions.headers = options.headers;
        requestOptions.params = this.buildUrlSearchParams(options.params);
        requestOptions.body = JSON.stringify(options.data);
        
        this.pendingCommandsSubject.next(++this.pendingCommandCount);

        return this.http.request(options.url, requestOptions)
            .catch((error: any) => {
                this.handleErrors(error);
                const response = JSON.parse(error._body);
                if (response.data.message === 'Token has expired') {
                    localStorage.removeItem('currentUser');
                    window.location.reload();
                }
                return Observable.throw(error);
            })
            .map(this.unwrapHttpValue)
            .catch((error: any) => {
                return Observable.throw(this.unwrapHttpError(error));
            })
            .finally(() => {
                this.pendingCommandsSubject.next(--this.pendingCommandCount);
            });
    }

    private interpolateUrl(options: DataServiceOptions): DataServiceOptions {
        options.url = options.url.replace(/:([a-zA-Z]+[\w-]*)/g, ($0, token) => {
            // Try to move matching token from the params collection.
            if (options.params.hasOwnProperty(token)) {
                return (this.extractValue(options.params, token));
            }
            // Try to move matching token from the data collection.
            if (options.data.hasOwnProperty(token)) {
                return (this.extractValue(options.data, token));
            }
            // If a matching value couldn't be found, just replace
            // the token with the empty string.
            return ('');
        });
        // Clean up any repeating slashes.
        options.url = options.url.replace(/\/{2,}/g, '/');
        // Clean up any trailing slashes.
        options.url = options.url.replace(/\/+$/g, '');

        return options;
    }

    private addAllowHeaders(options: DataServiceOptions): DataServiceOptions{
        options.headers['Access-Control-Expose-Headers'] = `UserId`;
        return options;
    }

    private addContentType(options: DataServiceOptions): DataServiceOptions {
        // if (options.method !== RequestMethod.Get) {
        options.headers['Content-Type'] = 'application/json; charset=UTF-8';
        // }
        return options;
    }

    private addAuthToken(options: DataServiceOptions): DataServiceOptions {
        const auth = localStorage.getItem('currentUser');
        if (auth) {
            // tslint:disable-next-line:whitespace
            options.headers.Authorization =  JSON.parse((<any>auth)).Token;
            options.headers.UserId = JSON.parse((<any>auth)).ID;
            options.headers.AccountId = JSON.parse((<any>auth)).AccountID;
            options.headers.UserTypeId = JSON.parse((<any>auth)).UserTypeID;
        }
        return options;
    }

    private addXsrfToken(options: DataServiceOptions): DataServiceOptions {
        const xsrfToken = this.getXsrfCookie();
        if (xsrfToken) {
            options.headers['X-XSRF-TOKEN'] = xsrfToken;
        }
        return options;
    }

    private getXsrfCookie(): string {
        const matches = document.cookie.match(/\bXSRF-TOKEN=([^\s;]+)/);
        try {
            return matches ? decodeURIComponent(matches[1]) : '';
        } catch (decodeError) {
            return '';
        }
    }

    private extractValue(collection: any, key: string): any {
        const value = collection[key];
        delete (collection[key]);
        return value;
    }

    private buildUrlSearchParams(params: any): URLSearchParams {
        const searchParams = new URLSearchParams();
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                searchParams.append(key, params[key]);
            }
        }
        return searchParams;
    }

    private unwrapHttpError(error: any): any {
        try {
            return (error.json());
        } catch (jsonError) {
            return ({
                code: -1,
                message: 'An unexpected error occurred.'
            });
        }
    }

    private unwrapHttpValue(value: Response): any {
        return (value.json());
    }

    private handleErrors(error: any) {
        if (error.status === 401) {
            sessionStorage.clear();
            this.us.navigateToSignIn();
        } else if (error.status === 403) {
            // Forbidden
            this.us.navigateToSignIn();
        }
    }
}