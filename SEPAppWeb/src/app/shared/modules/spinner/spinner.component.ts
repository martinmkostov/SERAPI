import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['../../../../styles/_spinner.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent { }
