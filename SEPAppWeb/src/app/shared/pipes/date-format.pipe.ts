import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

// under systemjs, moment is actually exported as the default export, so we account for that
const momentConstructor: (value?: any) => moment.Moment = (<any>moment).default || moment;

@Pipe({
    name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {
    transform(value: string, ...args: any[]): string {
        if (!value) {
            return '';
        }

        if (typeof value === "string" && value.indexOf('.000Z') > -1) {
            value = value.replace('.000Z', '');     // TODO: verify the whether the date is returned as UTC date? Do we still need to trim the zone???
        }

        let date = new Date(value);
        if (args.length > 1) {  // Convert UTC Date Time > User Local Date Time
            const utcDateFormat = moment(date).format('MM/DD/YYYY hh:mm:ss a');
            date = new Date(`${utcDateFormat} UTC`);
        }

        return momentConstructor(date).format(args[0]);
    }
}