export class StringHelper {
    public static toUpperCase (value: string) {
        if (value === null) {
            return '';
        }
        
        return value.toUpperCase();
    }


    private static escapeRegExp(str: string) {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    }

    public static replaceAll(text: string, stringToFind: string, replaceWith: string) {
        return text.replace(new RegExp(this.escapeRegExp(stringToFind), 'g'), replaceWith);
    }

    public static isNullOrEmpty(value: string): boolean {
        const type = typeof(value);
        if (type === "string") {
            return (typeof value !== "undefined" && value !== null && value.trim() !== "");
        } else if (type === "undefined") {
            return true;
        }
                
        return false;
    }
}
