export class DeviceHelper { 

    public static test(){
        return true;
    }

    public static isMobile(){
        return this.Android() || 
        this.BlackBerry() || 
        this.iOS() || 
        this.Opera() || 
        this.Windows()
    };


    public static Android(){
        return navigator.userAgent.match(/Android/i);
    };
    public static BlackBerry () {
        return navigator.userAgent.match(/BlackBerry/i);
    };
    public static iOS () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    };
    public static Opera () {
        return navigator.userAgent.match(/Opera Mini/i);
    };
    public static Windows () {
        return navigator.userAgent.match(/IEMobile/i);
    };
}
