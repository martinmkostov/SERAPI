import * as moment from 'moment';
import { NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';

export class DateHelper {
    public static toDateString(date: Date, dateFormat: string = 'YYYY-DD-MM'): string {
        if (date === null) {
            return "";
        }

        return moment(date).format(dateFormat);
    }

    public static addDays(date: Date, numberOfDays: number = 1): Date | undefined {
        if (date === null) {
            return null;
        }

        date.setDate(date.getDate() + numberOfDays);
        return date;
    }

    public static toNgbDate(date: Date): NgbDateStruct {
        const ngbDate: NgbDateStruct = new NgbDate(date.getFullYear(),  date.getMonth() + 1, date.getDate());
        return ngbDate;
    }
}