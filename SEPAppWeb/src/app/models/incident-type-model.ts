import { ReportTypeModel } from ".";

export class IncidentTypeModel {
    public Id: number;
    public Name : string;
    public IsReportRequired : boolean;
    public RequiredReport: ReportTypeModel[];
    public ShownReport: ReportTypeModel[];
}
