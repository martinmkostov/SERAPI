export class UserModuleRoleModel { 
    public RoleId : number;
    public ModuleId : number;
    public SiteId : number;
    public UserId : number;
}