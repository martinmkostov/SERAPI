export class ModuleModel {
    public Id:number = 0;
    public ShortName:string = "";
    public Title:string = "";
    public Description:string = "";
    public NodeLevel:string = "";
    public IsActive:boolean = false;
}