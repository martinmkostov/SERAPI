import { TenantsModel } from ".";

export class TagModel {
    public Id: number;
    public RFIDData: string;
    public Description: string;
    public TenantID: number;
    public Tenant : TenantsModel;
    public SiteId : number;
}
