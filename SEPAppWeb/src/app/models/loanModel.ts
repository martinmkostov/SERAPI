export class LoanModel { 
    public ItemNameId: string;
    public LoanId: string;
    public SiteName : string;
    public CustomerName: string;
    public CustomerId: number;
    public CustomerTypeId: number;
    public SiteId: number  = 0;
    public ItemName : string;
    public Description : string;
    public Type : string;
    public TypeId: number = 0;
    public LoanedDate : string;
    public ReturnedDate : string;
    public GivenBy : string;
    public ReceivedBy : string;
    public MessageTypeId : number = 1;

    public CreatedBy: string;
    public LastModifiedBy: string;

    public LastModifiedDate: string;
    public ConfirmedDate: string;
    public ActualReturnedDate: string;

    public PhotoString : string;
    public Customer : any =
    {
        Name : '',
        Company: '',
        Email: '',
        MobilePhone : '',
        Address1 : '',
        Address2 : '',
        City : '',
        ZipCode : '',
        MobileNumber: ''
    }

    public Site : any = {
        Name : ''
    }

    public LoanType : any = {
        Name : ''
    }


}