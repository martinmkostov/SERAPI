﻿using SER.FileManager.Models;
using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SER.FileManager.Repository
{
    public class ModuleRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;
        private SERAppDBContext db;
        public ModuleRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public ModuleModel GetModuleByName(string name)
        {
            var result = default(ModuleModel);

            using (SqlConnection connection = new SqlConnection(connString))
            {
                var query = $"SELECT Id,[Modules].[ShortName] FROM [Modules] WHERE ShortName = '{name}'";
                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            result = new ModuleModel();
                            result.ShortName = reader[1].ToString();
                            result.Id = Convert.ToInt32(reader[0]);
                        }
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }

        public ModuleModel GetModule(int id)
        {
            var result = default(ModuleModel);

            using (SqlConnection connection = new SqlConnection(connString))
            {
                var query = $"SELECT [Modules].[ShortName] FROM [Modules] WHERE Id = {id}";
                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            result = new ModuleModel();
                            result.ShortName = reader[0].ToString();
                        }
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }

        public void CreateLog(LogModel model)
        {
            Log newLog = new Log()
            {
                LogType = model.LogType,
                ShortDescription = model.ShortDescription,
                Message = model.Message,
                AccountId = model.AccountId,
                UserId = model.UserId,
                CreatedDate = DateTime.Now,
            };
            db.Logs.Add(newLog);
            db.SaveChanges();
        }
    }

}