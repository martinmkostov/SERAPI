﻿using SendGrid;
using SendGrid.Helpers.Mail;
using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;

namespace SER.FileManager.Repository
{
    public class FileRepository
    {
        public bool SendFiles(EmailModel email)
        {
            var apiKey = ConfigurationManager.AppSettings["sendgridkey"];
            var client = new SendGridClient(apiKey);

            var from = new EmailAddress("no-reply@serapp.com", "SER4");

            var res = client.RequestAsync(
                method: BaseClient.Method.GET,
                urlPath: "sender"
            ).Result;

            var body = email.Body ?? "";
            if (!body.Contains("<html>"))
            {
                body = $"<html>{body}<html>";
            }

            var msg = MailHelper.CreateSingleEmail(from, new EmailAddress(email.To), email.Subject, body, body);

            if (!string.IsNullOrEmpty(email.CC))
            {
                msg.AddCc(email.CC);
            }

            email.AttachmentPaths.ForEach(a =>
            {
                var attr = File.GetAttributes(a);
                var zipPath = "";
                var bytes = new byte[0];

                var name = new DirectoryInfo(a).Name;
                if (attr.HasFlag(FileAttributes.Directory))
                {
                    string startPath = a;
                    zipPath = Path.GetFullPath(Path.Combine(a, "..", $"{name}.zip"));


                    ZipFile.CreateFromDirectory(startPath, zipPath);

                    bytes = System.IO.File.ReadAllBytes(zipPath);
                    String base64 = Convert.ToBase64String(bytes);

                    msg.AddAttachment(Path.GetFileName(zipPath), base64);
                }
                else
                {
                    bytes = File.ReadAllBytes(a);
                    String base64 = Convert.ToBase64String(bytes);

                    msg.AddAttachment(Path.GetFileName(a), base64);
                }
                if (!string.IsNullOrEmpty(zipPath))
                    System.IO.File.Delete(zipPath);
            });

            var response = client.SendEmailAsync(msg).Result;

            var error = response.Body.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                return true;
            }
            else
            {
                throw new Exception(error.Result);
            }
        }
    }
}