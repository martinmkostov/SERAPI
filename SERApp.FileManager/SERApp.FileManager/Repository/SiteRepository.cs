﻿using SER.FileManager.Models;
using SERApp.Data.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace SER.FileManager.Repository
{
    public class SiteRepository
    {
        private string connString = AppSettings.ConnectionString;
        private UserRepository userRepo;
        private SERAppDBContext context;

        public SiteRepository()
        {
            context = new SERAppDBContext(connString);
            userRepo = new UserRepository();
        }
        
        public List<SiteModel> SitesPerAccount(int accountId)
        {
            var result = new List<SiteModel>();

            using (var connection = new SqlConnection(connString))
            {
                var query = $@"SELECT Id, Name FROM Sites WHERE AccountId = {accountId}";

                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteModel
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    Name = reader[1].ToString(),
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public string GetContractName(int id)
        {
            var result = "";
            using (var connection = new SqlConnection(connString))
            {
                var query = @"SELECT TOP 1 ContractNo FROM Contracts WHERE Id = @id";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@id", id);
                ///command.Parameters.AddWithValue("@folder", "%" + folder + "%");

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = reader[0].ToString();
                                //result.Add(new SiteFolderRole
                                //{
                                //    Id = Convert.ToInt32(reader[0]),
                                //    SiteId = Convert.ToInt32(reader[1]),
                                //    Folder = reader[2].ToString(),
                                //    UserId = Convert.ToInt32(reader[3]),
                                //    RoleId = Convert.ToInt32(reader[4]),
                                //    RoleName = reader[5].ToString()
                                //});
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public string GetURUnderLagFolder(int accountId)
        {
            var result = "UR Underlag";

            using (var connection = new SqlConnection(connString))
            {
                var query = @"SELECT Value From Setting WHERE AccountId = @accountId AND ConfigSettingId = 24";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@accountId", accountId);
                ///command.Parameters.AddWithValue("@folder", "%" + folder + "%");

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = reader[0].ToString();
                                //result.Add(new SiteFolderRole
                                //{
                                //    Id = Convert.ToInt32(reader[0]),
                                //    SiteId = Convert.ToInt32(reader[1]),
                                //    Folder = reader[2].ToString(),
                                //    UserId = Convert.ToInt32(reader[3]),
                                //    RoleId = Convert.ToInt32(reader[4]),
                                //    RoleName = reader[5].ToString()
                                //});
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public void DeleteSiteFolder(int siteId)
        {
            var siteFolder = context.SiteFolderRights.FirstOrDefault(a => a.Id == siteId);
            if (siteFolder != null)
            {
                context.SiteFolderRights.Remove(siteFolder);
                context.SaveChanges();
            }
        }

        public SiteFolderRole FindSiteFolderRole(int siteId, string folder, int userId)
        {
            var result = default(SiteFolderRole);

            using (var connection = new SqlConnection(connString))
            {
                var query = @" 
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.Folder = @folder AND SFR.UserId = @userId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);
                    command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                };

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteSubFolderRole(int siteId, string folder)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.Folder LIKE @folder";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);
                command.Parameters.AddWithValue("@folder", "%" + folder + "%");

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId, string folder)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.Folder = @folder";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);
                    command.Parameters.AddWithValue("@folder", folder);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public string GetUserRole(int userId)
        {
            var result = "UR Underlag";
            using (var connection = new SqlConnection(connString))
            {
                var query = @"SELECT Roles.Name FROM UserRoles INNER JOIN Roles ON UserRoles.RoleId = Roles.Id where UserRoles.UserId = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = reader[0].ToString();

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return result;
                }
            }

            return result;
        }

        public string GetURUnderlagSetting(int accountId)
        {
            var urUnderlagName = context.Settings.FirstOrDefault(x => x.AccountId == accountId && x.Name == "AccountFolderName" && x.SiteId == 0);

            return urUnderlagName == null ? "UR Underlag" : urUnderlagName.Value;
        }

        public string GetAktivitetsunderlagSetting(int accountId)
        {
            var activityName = context.Settings.FirstOrDefault(x => x.AccountId == accountId && x.Name == "ObjectFolderName" && x.SiteId == 0);

            return activityName == null ? "Aktivitetsunderlag" : activityName.Value;
        }

        public bool CheckIfEnabled(int siteId)
        {
            var result = false;
            using (var connection = new SqlConnection(connString))
            {
                var query = @"SELECT IsActive FROM Sites WHERE Id = @siteId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);
                

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result = Convert.ToBoolean(reader[0]);

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return result;
                }
            }
            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId, int userId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE SFR.SiteId = @siteId AND SFR.UserId = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);
                command.Parameters.AddWithValue("@userId", userId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> SiteFolderRole(int siteId, string path)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = $@"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId
WHERE (Folder = '{path}' OR Folder LIKE '{path}\%') AND SFR.SiteId = @siteId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@siteId", siteId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public List<SiteFolderRole> FindSiteFolderRole(int siteId)
        {
            var result = new List<SiteFolderRole>();

            using (var connection = new SqlConnection(connString))
            {
                var query = @"
SELECT SFR.Id, SFR.SiteId, SFR.Folder, SFR.UserId, SFR.RoleId, R.[Name] AS RoleName FROM [SiteFolderRights] AS SFR
JOIN Roles AS R ON R.Id = SFR.RoleId 
WHERE SFR.SiteId = @siteId";
                var command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@siteId", siteId);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteFolderRole
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    SiteId = Convert.ToInt32(reader[1]),
                                    Folder = reader[2].ToString(),
                                    UserId = Convert.ToInt32(reader[3]),
                                    RoleId = Convert.ToInt32(reader[4]),
                                    RoleName = reader[5].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }


        public void CreateSiteFolderRole(SiteFolderRole folderRole)
        {
            var existingRole = this.FindSiteFolderRole(folderRole.SiteId, folderRole.Folder, folderRole.UserId);
            var query = string.Empty;

            if (folderRole.RoleId != -1 && existingRole != null)
            {
                query = $"UPDATE [SiteFolderRights] SET RoleId = ${folderRole.RoleId} WHERE [Id]={existingRole.Id}";
            }
            else if (folderRole.RoleId != -1)
            {
                query = $"INSERT INTO [SiteFolderRights] (SiteId, Folder, UserId, RoleId) VALUES ({folderRole.SiteId}, '{folderRole.Folder}', {folderRole.UserId}, {folderRole.RoleId})";
            }
            else if (folderRole.RoleId == -1 && existingRole != null)
            {
                query = $"DELETE FROM [SiteFolderRights] WHERE [Id]={existingRole.Id}";
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateSiteFolderRole(SiteFolderRole folderRole)
        {
            var query = $"UPDATE [SiteFolderRights] SET [Folder] = '{folderRole.Folder}' WHERE [Id]={folderRole.Id}";

            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                }
            }
        }

        public void DeleteSiteFolderRole(int siteId, string path)
        {
            var siteFolderRight = context.SiteFolderRights.Where(x => x.SiteId == siteId && x.Folder == path).FirstOrDefault();
            if (siteFolderRight != null)
            {
                context.SiteFolderRights.Remove(siteFolderRight);
            }
        }

        public List<SiteModel> GetSitesByIds(List<int> ids) 
        {
            var result = new List<SiteModel>();

            using (var connection = new SqlConnection(connString))
            {
                string csv = String.Join(",", ids);

                var query = $"SELECT Id,Name FROM Sites WHERE Id IN ({csv})";
                var command = new SqlCommand(query, connection);

               
         //       command.Parameters.AddWithValue("@ids", csv);
                
                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new SiteModel
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    Name = reader[1].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }
        public void InsertContractLog(ContractModel model)
        {           
            var query = string.Empty;

            query = $"INSERT INTO [ContractLog] (ContractId, LogDateTime, Description, UserId) VALUES ({model.ContractId}, '{model.LogDateTime.ToString("yyyy-MM-dd")}', '{model.Description}', {model.UserId})";
           
            if (!string.IsNullOrWhiteSpace(query))
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();

                    var command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<int> GetActiveSites (int accountId, int userId)
        {
            var sites = new List<int>();
            if (userRepo.IsAccountAdmin(accountId, userId))
            {
                return context.Sites.Where(x => x.AccountId == accountId && x.IsActive == true).Select(y => y.Id).ToList();
            }
            else
            {
                return context.UserModuleRoles.Where(x => x.UserId == userId && x.ModuleId == 4).Select(y => (int)y.SiteId).ToList();
            }
        }

        public bool CheckTaskIfPublished(int siteId, int taskId)
        {
            var siteTask = context.SiteTasks.FirstOrDefault(x => x.SiteId == siteId && x.TaskId == taskId && x.IsCurrentlyPublished == true);
            return siteTask != null;
        }

    }
}
