﻿using SERApp.Models;
using SERApp.Models.Common;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SER.FileManager.Repository
{
    public class ContractRepository 
    {
        private readonly ContractService _contract = new ContractService();
        private readonly SiteService _site = new SiteService();
        private readonly AccountService _account = new AccountService();
        public string DownloadFiles(ContractFilters filters, string folderSuffix)
        {
            var contracts = new List<ContractModel>();
            if (filters.ContractIds != null && filters.ContractIds.Any())
            {
                var ids = string.Join(",", filters.ContractIds);
                contracts = _contract.GetContractsByIds(ids);
            }
            else
            {
                contracts = _contract.GetAllContractsForExport(filters).ToList();
            }

            var siteName = _site.Get(filters.SiteId)?.Name ?? "";
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{filters.AccountId}");
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", ".."));

            var siteIdSuffix = $"{filters.SiteId}{folderSuffix}";
            var contractParentFolder = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT");
            fileManagerRoot = Path.Combine(contractParentFolder, siteIdSuffix);

            var name = new DirectoryInfo(fileManagerRoot).Name;
            var rootFolder = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", siteName));

            if (Directory.Exists(rootFolder))
            {
                Directory.Delete(rootFolder, true);
            }
            Directory.CreateDirectory(rootFolder);

            Parallel.ForEach(contracts, contract =>
            {
                var contractFolder = Path.Combine(rootFolder, $"{contract.OpponentHolder.Replace("\\", "-").Replace("/", "-").TrimEnd().TrimStart()} - {contract.ContractNo.Replace("\\", "-").Replace("/", "-").TrimEnd().TrimStart()}");
                if (Directory.Exists(contractFolder))
                {
                    Directory.Delete(contractFolder, true);
                }
                Directory.CreateDirectory(contractFolder);
                var filemanagerContract = Path.Combine(contractParentFolder, filters.SiteId.ToString(), $"{contract.Id}");
                if (Directory.Exists(filemanagerContract))
                {
                    CopyDirectory(filemanagerContract, contractFolder, true);
                }
            });
            var zipPath = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", $"{name}.zip"));

            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }

            ZipFile.CreateFromDirectory(rootFolder, zipPath);

            if (Directory.Exists(rootFolder))
            {
                Directory.Delete(rootFolder, true);
            }

            return zipPath;
        }
        static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
        {
            // Get information about the source directory
            var dir = new DirectoryInfo(sourceDir);

            // Check if the source directory exists
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            // Cache directories before we start copying
            DirectoryInfo[] dirs = dir.GetDirectories();

            // Create the destination directory
            if (Directory.Exists(destinationDir))
            {
                Directory.Delete(destinationDir, true);
            }
            Directory.CreateDirectory(destinationDir);

            var files = dir.GetFiles();
            Parallel.ForEach(files, file =>
            {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            });

            // If recursive and copying subdirectories, recursively call this method
            if (recursive)
            {
                Parallel.ForEach(dirs, subDir =>
                {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyDirectory(subDir.FullName, newDestinationDir, true);
                });
            }
        }

        public void DeleteExportedFiles(string path, string siteId, string suffix, string fileName)
        {
            var site = _site.Get(Convert.ToInt32(siteId));
            var rootFolder = Path.Combine(path, site.Name);
            var parent = Path.Combine(path, $"{siteId}{suffix}");
            var zip = Path.Combine(path, $"{siteId}{suffix}.zip");

            var ctr = 0;
            while (true)
            {
                try
                {
                    if (Directory.Exists(parent))
                    {
                        Directory.Delete(parent, true);
                    }
                    if (Directory.Exists(rootFolder))
                    {
                        Directory.Delete(rootFolder, true);
                    }
                    if (File.Exists(zip))
                    {
                        File.Delete(zip);
                    }
                    SERApp.Service.Tools.File.DeleteFile($"CONTRACT/{fileName}.zip");

                    break;
                }
                catch (Exception e)
                {
                    //just to make sure that all created folders and file were deleted
                    //usually caused by "IOException: The process cannot access the file 'file path' because it is being used by another process"
                    System.Threading.Thread.Sleep(60000);
                    ctr++;
                    DeleteExportedFiles(path, siteId, suffix, fileName);
                }

                //break loop when it loops 5 times already
                if(ctr == 5)
                {
                    break;
                }
            }
        }

        public void SaveEmailRecipient(SaveEmailRecipient model)
        {
            _contract.SaveEmailRecipient(model);
        }

        public string GetEmailCC(int accountId)
        {
            return _account.GetEmailCC(accountId);
        }
    }
}