﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using System.Data;
using System.Data.SqlClient;
using SER.FileManager.Models;
using SERApp.Data.Models;

namespace SER.FileManager.Repository
{
    public class UserRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;
        private SERAppDBContext context;
        public UserRepository() {
            context = new SERAppDBContext(connString);
        }

        public bool UserExists(string hashKey) {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                string query = "SELECT * FROM Users WHERE IsDeleted = 0 AND RandomSecret=@RandomSecret";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@RandomSecret", hashKey);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        return reader.HasRows;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }
            };
            return false;
        }

        public UserModel GetUser(int userId) {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                string query = @"SELECT u.Id, FirstName, LastName, EmailAddress, Mobile, PhoneNumber, UserName, DepartmentId, AccountId,
                        CASE WHEN r.Name LIKE '%admin%' THEN 'true' ELSE 'false' END as IsAdmin, r.Name as RoleName, IsActivityAdmin FROM Users u
                        INNER JOIN UserRoles ur ON ur.UserId = u.Id
                        INNER JOIN Roles r ON ur.RoleId = r.Id
                        WHERE IsDeleted = 0 AND u.Id = @UserId";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@UserId", userId);
                try
                {
                    connection.Open();
                    var user = new UserModel();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows) {
                            user.Id = Convert.ToInt32(reader[0]);
                            user.FirstName = reader[1].ToString();
                            user.LastName = reader[2].ToString();
                            user.EmailAddress = reader[3].ToString();
                            user.Mobile = reader[4].ToString();
                            user.PhoneNumber = reader[5].ToString();
                            user.UserName = reader[6].ToString();
                            user.DepartmentId = Convert.ToInt32(reader[7]);
                            user.AccountId = Convert.ToInt32(reader[8]);
                            user.IsAdmin = Convert.ToBoolean(reader[9]);
                            user.RoleName = reader[10].ToString();
                            user.IsActivityAdmin = Convert.ToBoolean(reader[11]);
                            return user;
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    return null;
                }
            };
            return null;
        }

        public List<UserModel> GetUsersForModule(int moduleId, int accountId = 0, int siteId = 0)
        {
            var result = new List<UserModel>();

            using (SqlConnection connection = new SqlConnection(connString))
            {
                var query = @"SELECT U.Id, U.FirstName, U.LastName, U.UserName, UMR.ModuleId, UMR.RoleId, UMR.SiteId, U.IsActive FROM Users as U
                                RIGHT JOIN UserModuleRoles as UMR ON UMR.UserId = U.Id
                            WHERE UMR.ModuleId = @moduleId";
          

             
                if (accountId != 0)
                {
                    query = @"SELECT U.Id, U.FirstName, U.LastName, U.UserName, UMR.ModuleId, UMR.RoleId, UMR.SiteId,U.IsActive FROM Users as U
                                RIGHT JOIN UserModuleRoles as UMR ON UMR.UserId = U.Id
                            WHERE UMR.ModuleId = @moduleId AND U.AccountId = @accountId";
                  

                }

                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@moduleId", moduleId);
                if (accountId != 0)
                {
                    command.Parameters.AddWithValue("@accountId", accountId);
                }
        
                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                result.Add(new UserModel
                                {
                                    Id = Convert.ToInt32(reader[0]),
                                    FirstName = reader[1].ToString(),
                                    LastName = reader[2].ToString(),
                                    SiteId = Convert.ToInt32(reader[6]),
                                    RoleId = Convert.ToInt32(reader[5]),
                                    IsActive = Convert.ToBoolean(reader[7])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public string GetActivityUserRole(int userId, int accountId, int siteId = 0)
        {
            if (IsAccountAdmin(accountId, userId))
            {
                return "superadmin";
            }
            else if (siteId > 0)
            {
                var roleId = context.UserModuleRoles.FirstOrDefault(x => x.UserId == userId && x.ModuleId == 4 && x.SiteId == siteId).RoleId;
                var role = context.Roles.FirstOrDefault(y => y.Id == roleId).Description;

                return role.ToLower();
            }
            return "user";
        }

        public bool IsAccountAdmin(int accountId, int userId)
        {
            return (context.Users.FirstOrDefault(x => x.AccountId == accountId).Id == userId);
        }
    }
}