﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using SER.FileManager.Models;
using SER.FileManager.Presenter;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for GenericFileManageUploadHandler
    /// </summary>
    public class GenericFileManageUploadHandler : IHttpHandler
    {
        private FileManagerPresenter _fileManagerPresenter = new FileManagerPresenter();

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                var localPath = context.Request.Form["localPath"];
                var pathName = context.Request.Form["pathName"];
                var userId = Convert.ToInt32(context.Request.Form["userId"]);
                var siteId = Convert.ToInt32(context.Request.Form["siteId"]);
                var moduleId = Convert.ToInt32(context.Request.Form["moduleId"]);
                var uploadedFiles = context.Request.Form["files"];

                var files = new List<fileReturnData>();
                var httpRequest = HttpContext.Current.Request;

                var user = _fileManagerPresenter.GetUser(userId);


                for (int x = 0; x < httpRequest.Files.Count; x++)
                {
                    var file = httpRequest.Files[x];
                    var filePath = Path.Combine(localPath, file.FileName);

                    file.SaveAs(filePath);

                    var f = new FileInfo(filePath);

                    var mime = MimeMapping.GetMimeMapping(f.Name);

                    //Byte[] bytes = File.ReadAllBytes(filePath);
                    //String base64 = Convert.ToBase64String(bytes);

                    files.Add(new fileReturnData()
                    {
                        ParentPath = localPath,
                        //Base64 = base64,
                        FilePath = f.FullName,
                        Name = f.Name,
                        Mime = mime,
                        Icon = FileHelper.GetIconFromMimeType(mime)// $"data:image/png;base64,{base64ImageRepresentation}",
                    });

                    pathName = Path.Combine("\\", pathName, file.FileName.Replace("\\", ""));


                    if (!user.IsAdmin)
                    {
                        SaveFolderSetting(siteId, pathName, 1, userId);
                    }
                    else
                    {
                        var users = _fileManagerPresenter.GetUsersForModule(moduleId, user.AccountId).ToList();

                        Parallel.ForEach(users, usr =>
                        {
                            if (!string.IsNullOrEmpty(file.FileName))
                            {
                                SaveFolderSetting(siteId, pathName, -1, userId);
                            }
                        });
                    }
                }

                var data = new DataResponseModel() { 
                    files = files,
                    message = "File saved successfully."
                };

                var json = JsonConvert.SerializeObject(data);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                context.Response.Write("File is too large.");
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void SaveFolderSetting(int siteKey, string path, int roleId, int userId)
        {
            var repo = new SiteRepository();
            repo.CreateSiteFolderRole(new SiteFolderRole
            {
                SiteId = siteKey,
                Folder = path,
                RoleId = roleId,
                UserId = userId
            });
        }
    }
}