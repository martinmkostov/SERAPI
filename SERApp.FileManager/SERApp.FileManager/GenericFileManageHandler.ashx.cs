﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using SER.FileManager.Presenter;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for GenericFileManageHandler
    /// </summary>
    public class GenericFileManageHandler : IHttpHandler
    {
        private ModuleRepository _moduleRepository = new ModuleRepository();
        private SiteRepository _siteRepo = new SiteRepository();
        private UserRepository _userRepo = new UserRepository();
        private FileManagerPresenter _fileManagerPresenter = new FileManagerPresenter();

        public void ProcessRequest(HttpContext context)
        {

            var accountId = context.Request.Form["AccountId"];
            Int32.TryParse(accountId, out int accountIdConvertedToInt);
            var siteId = context.Request.Form["SiteId"];
            var moduleId = context.Request.Form["ModuleId"];
            var role = context.Request.Form["Role"];
            var userId = Convert.ToInt32(context.Request.Form["UserId"]);
            var name = context.Request.Form["Name"];

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", accountId);
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;

            var directories = new List<string>();

            var user = _fileManagerPresenter.GetUser(userId);

            var module = _moduleRepository.GetModule(Convert.ToInt32(moduleId));
            var filesAndFolders = new List<fileReturnData>();
            switch (module.ShortName) 
            {
                case "contract":
                case "contacts":
                    var id = context.Request.Form["ModelId"];

                    fileManagerRoot = Path.Combine(fileManagerRoot, module.ShortName.ToUpper(), siteId);

                
                    var path2 = Path.Combine(fileManagerRoot, id);

                    if (!Directory.Exists(path2))
                    {
                        Directory.CreateDirectory(path2);
                    }

                    directories = Directory.GetDirectories(path2).ToList();

                    var dia = new fileReturnData()
                    {
                        IsRootFolder = true,
                        Type = Type.Folder,
                        ParentPath = path2,
                        Path = path2,
                        CanModifyFolder = false,
                        Icon = Icon(),
                        children = GetFiles(path2),
                        Name = name //_siteRepo.GetContractName(Convert.ToInt32(id)) //GetFolderName(path2)
                    };

                    filesAndFolders.Add(dia);
                    break;

                case "incident-accident":
                    var accidentId = context.Request.Form["ModelId"];

                    fileManagerRoot = Path.Combine(fileManagerRoot, module.ShortName.ToUpper());
                    var path1 = Path.Combine(fileManagerRoot, accidentId);

                    if (!Directory.Exists(path1))
                    {
                        Directory.CreateDirectory(path1);
                    }

                    directories = Directory.GetDirectories(path1).ToList();


                    var div = new fileReturnData()
                    {
                        IsRootFolder = true,
                        Type = Type.Folder,
                        ParentPath = path1,
                        Path = path1,
                        CanModifyFolder = false,
                        Icon = Icon(),
                        children = GetFiles(path1),
                        Name = "Data"//GetFolderName(path1)
                    };

                    filesAndFolders.Add(div);
                    break;

                case "activity":
                    var activityId = context.Request.Form["ModelId"];

                    fileManagerRoot = Path.Combine(fileManagerRoot, module.ShortName.ToUpper(), activityId);

                    var isAllTemplate = context.Request.Form["AllTemplate"];
                    var isTmp = Convert.ToBoolean(isAllTemplate);
                    var path = Path.Combine(fileManagerRoot, siteId);
                    var taskId = context.Request.Form["TaskId"];
                    var userRole = "";

                    if (isTmp)
                    {
                        path = Path.Combine(fileManagerRoot);
                        var urUnderlagFolder = Path.Combine(path, "UR Underlag");

                        var activeSites = _siteRepo.GetActiveSites(Convert.ToInt32(accountId), userId);

                        var sites = _siteRepo.SitesPerAccount(Convert.ToInt32(accountId)).Select(y => y.Id);

                        Directory.CreateDirectory(path);

                        var activeSitesDirectories = new List<string>();

                        foreach (var row in sites)
                        {
                            Directory.CreateDirectory(Path.Combine(path, row.ToString()));
                            Directory.CreateDirectory(Path.Combine(path, row.ToString(), "Confirms"));

                            if (activeSites.Contains(row))
                            {
                                activeSitesDirectories.Add(Path.Combine(path, row.ToString()));
                            }
                            
                            Directory.CreateDirectory(urUnderlagFolder);
                        }

                        Directory.CreateDirectory(urUnderlagFolder);
                        activeSitesDirectories.Add(urUnderlagFolder);

                        directories = activeSitesDirectories;

                    }
                    else {
                        userRole = _userRepo.GetActivityUserRole(userId, Convert.ToInt32(accountId), Convert.ToInt32(siteId));
                        //main UR Underlag folder
                        var urUnderlagFolder = Path.Combine(path, "..", "UR Underlag");

                        var aktivitetsUnderlagFolder = path;
                        var confirmFolder = Path.Combine(path, "Confirms");


                        if (!Directory.Exists(confirmFolder))
                        {
                            Directory.CreateDirectory(confirmFolder);
                        }

                        if (!Directory.Exists(urUnderlagFolder))
                        {
                            Directory.CreateDirectory(urUnderlagFolder);
                        }

                        directories = new List<string>();
                        
                        directories.Add(urUnderlagFolder);

                        if (_userRepo.IsAccountAdmin(Convert.ToInt32(accountId), userId))
                        {
                            directories.Add(confirmFolder);
                        }

                        directories.Add(aktivitetsUnderlagFolder);
                    }

                    directories.ForEach(dir =>
                    {

                        string folderName = new DirectoryInfo(dir).Name;

                        var di = new fileReturnData()
                        {
                            IsRootFolder = true,
                            Type = Type.Folder,
                            CanModifyFolder = UploadPermission(dir, userRole, isTmp),
                            ParentPath = dir,
                            Path = dir,
                            Icon = Icon(),
                            children = GetFiles(dir, UploadPermission(dir, userRole, isTmp)),
                            Name = GetFolderName(dir, isTmp, accountIdConvertedToInt)
                        };

                        filesAndFolders.Add(di);

                    });

                    break;
            }
            
            filesAndFolders = filesAndFolders
                .OrderByDescending(x => x.Path.ToLower().Contains("ur underlag"))
                .ThenByDescending(x => x.Path.ToLower().Contains(siteId.ToString()) && !x.Path.ToLower().Contains("confirms"))
                .ThenByDescending(x => x.Path.ToLower().Contains("confirms"))
                .ThenBy(x => x.Name).ToList();


            var json = JsonConvert.SerializeObject(filesAndFolders);
            context.Response.Write(json);
        }

        public bool UploadPermission(string path, string role, bool isTemp)
        {
            ////tempopary implementation (must not implement file permission as of the moment)
            return true;
            if (role.Contains("admin"))
            {
                return true;
            }

            else
            {
                if (!isTemp && (path.ToLower().Contains("ur underlag") || role == "viewer"))
                {
                    return false;
                }
                return true;
            }
            
        }

        private string GetFolderName(string path, bool isId = false, int accountId = 0, bool isSubfolder = false) 
        {
            string folder = new DirectoryInfo(path).Name;

            if (isId) 
            {
                try { 
                    var st = _siteRepo.GetSitesByIds(new List<int>() { Convert.ToInt32(folder) });

                    folder = st.First().Name;
                }
                catch {
                    if (folder.ToLower() == "ur underlag")
                    {
                        return _siteRepo.GetURUnderlagSetting(accountId);
                    }

                    return folder;
                }
            }
            else if (!isSubfolder)
            {
                if (folder.ToLower() == "ur underlag")
                {
                    return _siteRepo.GetURUnderlagSetting(accountId);
                }
                if (folder.ToLower() != "confirms")
                {
                    return _siteRepo.GetAktivitetsunderlagSetting(accountId);
                }
            }

            return folder;
        }

        public List<fileReturnData> GetFiles(string path, bool canModifyFolder = true) 
        {
            var se = new CultureInfo("sv-SE");
            var files = new List<fileReturnData>();
            var folders = new List<fileReturnData>();
            var dir = new DirectoryInfo(path);

            foreach (var f in dir.GetFiles())
            {

                var mime = MimeMapping.GetMimeMapping(f.Name);
                //Byte[] bytes = File.ReadAllBytes(f.FullName);
                //String file = Convert.ToBase64String(bytes);


                files.Add(new fileReturnData()
                {
                    ParentPath = path,
                    CanModifyFolder = canModifyFolder,
                    //Base64 = file,
                    FilePath = f.FullName,
                    Name = f.Name,
                    Mime = mime,
                    Icon = FileHelper.GetIconFromMimeType(mime)// $"data:image/png;base64,{base64ImageRepresentation}",
                });
            }

            foreach (var f in dir.GetDirectories())
            {
                var fName = GetFolderName(f.FullName, false, 0, true);
                var getFiles = GetFiles(f.FullName);

                if ((getFiles.Count() == 0 && fName.ToLower() == "aktivitetsunderlag") || fName.ToLower() == "confirms")
                {
                    continue;
                }
                

                var di = new fileReturnData()
                {
                    Type = Type.Folder,
                    CanModifyFolder = canModifyFolder,
                    ParentPath = path,
                    Path = f.FullName,
                    Icon = Icon(),
                    children = getFiles,
                    Name = fName,
                    MaskName = fName.Contains("_confirm_") ? fName.Remove(fName.IndexOf("_confirm_"), fName.Length - fName.IndexOf("_confirm_")) +"_confirm" :"",
                };

                if (!string.IsNullOrEmpty(di.MaskName)) 
                {
                    di.Name = di.MaskName;
                }

                folders.Add(di);
            }
            files = files.OrderBy(u => u.Name, StringComparer.Create(se, false)).ToList();
            folders = folders.OrderBy(u => u.Name, StringComparer.Create(se, false)).ToList();

            folders.AddRange(files);

            return folders;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string Icon() => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAcjElEQVR42u1dS7Ac1Xk+/+hKBAkbWaJKYCMFKNs4sUNJrix42I5I7FQ5CwOrVMWLoPImlY2dZVaBVUI24EWq8nKkTbLFpCqsnAoVx9gQB8kJJCYmSAFcwhWEL0ISoMf86Z6Z7j6P/5w+/Zie0zPfVzX3zvQ87tw+5/v+7//Po4mZFQAAm4kJTgEAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAA+LHV9QOIyPvcmeNqb/brwez2QHY7mt324pRHYzu7ncpuT2W3b992TJ3BKQFsMHM3/nb+AI8AZOR/JPv1dZC+N3w7u30zE4JncCqAZAUgI/5t2a8ns9thNM9SkAvAMTgCoA8B6LUGkJE/J/1JkH+pyFOpk9m5fhinAuicwvflABaR/yQs/6DIncAJnAY4gBQcwJMg/+A4DicArFwAFgU/2H6IALBpKcD/nqA86p9G9Ec6AGxmCvAgyA8nAGxuCvAATiNEANhcATiK0wgRADZXAGD/0xSBx3EagCEEAEgT38hE4DhOAwAB2Fw8DBEAIAAQAYgAAAGACAAABAAiAAC9CQDjNqLbw6f/BiIAVOi8I1DHmYjACpzA6W8pdfvX1DGcCqDzWoDT3yJIwJgs38496pbP/6nadeMdOBnpYlvNN36ZbQdH+7+87Q/AK94R6NW/hgCMifwf/UJG/g8fUur880pdPY+TkhJ27ls01PXz+7sOZAzdysn/zUwEHklSAP7nryAAYyH/x34tJ/9BkH9MuO5jSu3+RC4Kp7JHD2VCcCYtAfhLCEDy5N81j/zX3QjyjzNR36nUnl/KxSB3A/dnInAqGQF45S8gAKmTP4/8IP+auIEb7spF4EjhBLryF6MAa0/+xxbkfy4j/7s4KWPGBz/Nf+7NRCDffu9IHx8JAVhT7MjIf2tO/jznf+c5RP51wftvZKzdd5jfevoRuum3HukcJDp/IUyuSe422crIf/Qx2P51xcX/Uurapa9nIrAXDgBwbP/BXwf51xp8Ran3XslTgQezRycgAEBp+w/dr+f8IP/61gOyVGD3nQ9AAICS/LPIvxfk3xhc/tnRlacACgKQhO0/9BuPqV8A+TcLV7dRAwD596hf/CLIv5GYvqcgABtu+2/70p9k5L81I/8PQP6NA0MAQH5E/o3FlXOrFwDUAFZj+2//zYz8HwH5ATiAjcKHD92jbv3cH6gdO3eB/AAEYFNww82/og4c+arak/2eWb/t53opAgEQACCx3P76ffPdenbecEBdv/+OLOrfrXZl92eEv/Cj+SQQAOgBnZcDq3NPwwMsE7nFzyP+5Z/1UvQB1gx3/hmt1gG89Q9oBADY3BQABgAAxgpcGAQAIAAAACAFQAoAAHAAAABAAAAAgAAAAIAaAGoAAAAHAAAABAAAAKQAyAAAADUAAACQAgAAAAEAAAApAAAAcAAAAEAAAACAAAAAgBoAAABwAAAAQAAAAIAAAACAGgAAAOMRAPAfAJACAACAFAAAADgAAAAgAAAAQAAAAEANAACA9RMAwJFEaOJSQIRzAAEAWdFWEBUIAAgPLKM/bJogbEQNAKQH2vSVTRCD9XMAvARJmn0mVCTNwoDvMPUqBuWfIghAkgZgGVGeYR1GIfjyYdYiOfX3p3i93MGoU4C++Fl9Do/gvwa5pShMjYScOpOY10QIRpkCdCF+bFQ3NIG6qwEEo4Wzp3ih4JoUgYTWkLpCU7cw9prB1joTP4bsHDpAVs9hkHnQ4M8tRMMjHLFmQuozsaIwRlewtW7Eb0V6oUfMPqbnvwv06BAElvkyuRAhOSKN0Ns2RgzGJATJ1wBieRUiIPfwN6KEBRowEPnr20Mnqu+lNkE5oq5Q/N1YIUhdBJJ1AMsmfujzpc+M/z4g6DKJH3+O6618MH8v6grkF4J1cANb60Z+bviZTckujxhQUG4gCt0I3+Q8FqSUX8vRr6eiSTUhsF1BrAik7Aa2xkd+dl7jcFKMFHLlV3qN73v4vxv7H7UYYtwMdtedSpqfb6p/ixuZfWmD9EnscRnkioFyxcCsD4S/ZYoikFQNoE0uzjbxWSJ5E2L7h4hCXMZMwb67jTvkYnou8gtL5Gfn0dtOA3SHZ7uDkrzFVyPbEdQTPDURSMYB1JG/lvjWZziv57CVr3MV3LPFR1pQb/lrfKD8elaybWDJEZjVfVcM5Od1IbDrBDFFwpREYGuU5LfS71jih0hfR/hmKYG/vgA07wc+MvmIZIiDZPstl2CSnQwnUKUEYUfAloDV1QdSEYGtpbN3GZFffzx1X8u2g2S5fsDO92Cv1WfxMde7W2hA51rBjEwhM09WQkBOvjAXhfJN5C0DEBV9xBUCO7rbJJ49VuMSgZU6gC7kl6K+L/dnOaX01gaapBJtuQ5zEGn/Wcr/3ZPJ+ueUEZnMN5Py5wPWU3MhKJSEamsI+p8akwhspUj+psSXCVxFe4n0EuGD6UMDiw9yL8dI2nyWUgQ9ClcRWyebFL3dCQHzz+FFfs/ie3VHYDgFu/xQUxdYpQhspdrYbaK+fYwDQlH7WQGy++cUtDkXm60WTRbf+CbVOES3P5ftiGx6iaAYsCxEdmogCUvMOrLx1wB6TnINokWS336Opbye5WjvrwnEzBuAE+izvWPTg4p85I2kOtF9giCJgUnyBYWpGvN3oj3Hi0AoHViVC9gavsHbkz8UyY18PyASPpdQJyzRLgCcXmqfIak9jGE4JVbrDbKL7oA9QrBIJa15yIUQjF0EEpoHUB/5Y8jflPixr/d/t/brBoCm6YI4mdct+lvCUIiCSVRfYUEWgpLGghvoUwTW2gE0mY9vvn6uwr40gJVV9NNI7BzTUgL7dZdovzo7OazemtypztPB2WNg/LiRX1fX8zl10/Rldcv0lNqd3c/5NyfwQgxKdrozhyqiu25gTnoSREDVVADkCsHQLmCwGkA4KrrR38nrg1HfFQAp4huioX3Oa5N71as7v6jeyUh/4YOpOrt9VV24nP0+/275Hc6+exVMGhFu+VDVtfft3q/27z6QHbtL3XDdb88E4Y6r31GHps8aUZyK+QKW1fBG7NJ6yGTWRcF1AX6iDykC1LkK/dJXuYsAxOb9Nmkl8ouR3yMiBfFf3vqK2p7uUz9563J2+0Cdu3QN7Flj7N+9Q33ipuuy2y61d/K2uvPq35dCUJC2cAd6DDeO2feVWZQsPsM+JtUxfKlAtAB8+m87ScUgKUDzrbz8ebgv3/eRXyL+RbVfndx5TJ3lT6qXzn6gXnzzHXX5GhL3TUAu8Odeu6Re+Ol76jM371HbBx5Wr+24Vx25clztUedUNTRYpAZU1hGKBULO/aIuMJtt6M4V0F1GcnWV7g7gd7itAMREfzGvF15jOAHlPseLvf3PTo6oUxn5X/n5TvXPpy+C+BuOXTtIfeH2PerjH7miDmcicMv0pJoE3IDXKZD7vO0WjON9uYBP/10nWVn+5cG5/nhT8iuJ/KyRn23yc2n5vzf5ffWPp1l955ULID8w6wN5X8j7RN438j4ynbLTf4rpxbzYLNJ4ruhzkmvliLkO3PB4UkXA2vn89dHfZ/0VC+RXOqmrA8xyWpA/ztpTvZ7ZvOcnx9TTP34XeT7gIK8BvZ33i08dm3Wkg9eezZwAa6F9bu+LAGOv/NMXJBlDgcocNtRpU403sLi/wRAzCVc+DyBmCy97Ao752CS/7hpmSp7dz4f2nt8B8gP19YG8j+QisMWXZkOGE4uqpTfXtiuSZg2adQB7PcJwBB8gBeDGN56t4bWPuc8xa8/bj1m7aY+r56az4xfVPvWvID/QUATyPpP3nWnRl9jqY8o8bvRLrR+bz7nHQpyIu61cAJrb/7bR3y3ssfNpuu3Pf/9o19fUd19XID/QSATyPpP3Hb0vhVaNMst9NZYLbTmUtAA0/Se9y3ulvN94zi74zX+/vuM+9cL5O2b5HQA0rQnkfSfvQ3qfMkeVAgEqksSrLkNPhid7uOTpj/5uHcAmfzHRt0gJXpx8ZTbUBwBtkPedvA+Vtl7pRWb29EG7r8r1qyarSkdZA5DynPpcJpT7Kyffct+3sP/TefT/4f99CEN9QGvkfSfvQ3lfKoYGK6JyoP+rUjRi+MCtcv8R1ABi8n9/XhXKp9hJC/Tx/vz+y5MvqRfffB+9GOiEvA/lfcnuX0bfC9am/I9TuNrUoAJQZ3Hk/N4qtniW31YWTalLar964ec3I/oDvbiAvC/lfcqX99f14aHInFQK0PbmGyIpjxvDftbxxe3Nrc/OFvYAQB/I+1Lep9yhZndYUHquSVrcLnUeWQrQ1P7HOAvdhr127ZMY9gN6Q96X8j6lF5/13hxzHcL4Pj6sRViKAPRxpRwWj8m5f5kGLI7/98WPotcCveLVCzd6hpuFPsrxlJa2phsSy1kLECtudiFVuO+c6MAAQvG6N97fmx1ACgD0h7PXDmoLz5Rx8RGeVmsG9M2FyzUCwqamTZcGL2s58YA7AnGDz+Do+6wN/xV/523Yf6BvATh/RfFu1oISlQuEhiTsKFKApvl7dOog7gFg1QKwIyewxL46xFWgh+zDkzRPsjz8x8q/SzB4DwzTQYW+6ewLwP6+vX7DgMtIGGKVUL6UFzbwBHpPAbI+Ze425QlEQh+Vj6dxVagBrwzkrwwy+/N/aX61fF8fRwWA5YevmJ19Y2oBq6wXrG4qMPf7OaA9kJqLbbwZ7iYJQD8Nwa1ONACkErg2pAbQdGKA/7603hrxHxgu4JipJkcNWcf2+eH7dO81AL+ydRQBluoB9msZYgAME89Iv6NfT1C6r8O5kmHk60bsAJZSmBN0A6kAsBJfYPe9EfXDZGoAIC8w/vrA+DrxQMOA8uoIdjZPqxS1cvTsTw3KlUFIAYAVRCleWHXSbH+eqpKWIdiu3kghrCyCpDSj5tjKBSCmpuE7Zi3wYeXhsL4II8Rz8B8YIv+XSCxxlDzvlUoFMaWBJZQF0hoG5NgKKQAA6ycAnnyqfgEGhAJIIF5F9c81HwZs9pom+UPcVGCIAbB0/98oF6eA//f7+qGmB0/QqACQqnPo530QAAAAlpUCLNdyuarHNelBujh+6+/NTR+h44Ui3LE3/nwcqYA4fie9lho8Hp0A9FkDCNUFYmsA6YKIreyQIt6zGZY3L/Sm/7+ykNfXjc3V5fy00n67pYBhRUCR2NF9nZ8obZGovQI0y8S235efF2Y1yJZbQKoCsFoxHIz8BSF0ctskMR9bXoH8pnHZYuAjPFvGjC1769tBpxCJ6iDyow1PAWLy/PGmAJMJaQLAJr01YutE9t537sSlFd1a22G591LYdfeZzP0c8gtwjiMFWJ8It5XUOW06EbDfqyQNIADarXADJfG1feXt4xb5SSO99DplP99Xl+fqE1mxKQDifnlUvs54TWH1mYxr5ObimL4IWOl/zGtY+RtjtTXAtNYCOKRWyn8V5mUI9BApwOL3hKoUwEgLFIlOgPTXSwQnOfZ3TQmYXVFhreNywNLPl8lSsUZm8XpePKbF58xrBFOeM4UUpU16aQuKuiX9FOifKzYFW2me5SYWYGQqsGhzk9hFH7FrBMV9MqO/MsXDnyZQ8DvEnTV52zVmPbdnY+YaE5XH9Y0x5wu6Fj1e+46zQqHahDmcAy3xG08NIMZGcORrRkD8/Eoys0tF8YLUXKYD1fOSM+CFaOikZ7mYKJyjGBdANdGfyDzrc0ILBNeiOpMSd3zmgv9ktiEpd6h0veoA0hDgaoPWeg4D8hjEQFnkVyL59YhvR3uybL/pAij4t2NOnf66wtaT9pr8b7BIjCqql2kDF99JF4FqLT3zuNpvXYB5AAmJgSsOZlpgR3xSbn0gRPIoJ0Ay/6i09VX0ryK5HN1iRMAQLBB/0wSgLodvMlMwlC4kmv9b3tscJXCPlY8DwuEju8PRQNJNJJxlPcfn6m8zm59prmKTRUAWmzFsqtcmX5e2/7H/T9QAeqwBpH51oCr/X8x/KyP4/HfVKYrH9ghAOX/AuPQ0OyMJYn5PNYm/L1ul0J530jJZ+6Kv9oxANmoB8/tjurpT7HY9IdVFDWDjrH6cS3Btv+4efCMA/mJgy+vR+6yBlrcTWR15MdavW/35U4RpvkgBVplapJv7GwU+ISwXxT4f+e25AyHCk513WDa0LPaRRxCKH5oQOPk9+2IhVdHeUwtQGzIguEYCMFQKEHNf3100UWFaDAOS0NGduQGlMIReV0yg8eT+JAmKXFuRdqGxBaEQAl4U8FkbamRtQYCpM+ykAvZrZ9+JVPrtF13p4U0RADiARlG/5Xscp6APIQrkt51C+xSFDDEox/1LEdCjetX526bxRONxcHAA4H8PaQB5ojsFbbx/yrCf+LFiIE4A4mIoMCwCdgQsRKFcFqzVAorPG3P7QQC6rAUoepYxkU9aNmY6fHl9AGsZQKI9qPyOGnOZqxyfNfKyFf2F6sBiIKESAd0RSJtKRp4WUnL+X41bVFN3ieW6ttGErE0gYv1ft1YWLq6xlW77aW1YnPjQxT+U0GBMcRnCQNcFWG0NgD3Ps7C0jBvUAEZiUajFMBCV72PT/iuzFtA2BYnZ22Zh8oWFbhZ1ub5+Q9q7aBTtpo94kDWdWdUM7VdF0uiVhHXHkALUeNi1qARQ8Ihk+8V9AyJCiDg+YE0A0tMBb7DqMrEPMwJRA1j3IgDV1AWKGX/SfACR5LHkJ/+mIWxV4L1TW0hZ04Ll50mzDCFOE4oAK8MkPbLGTAkd84UBpc0ObPLK50QSgtBjsvJPohrhKd5PPqdR/7eJ5DbyC1nxv42x/Zr27/Qufbdm8wCK3GwMc8nNnSSonB5sx173yjIUsauslCLEtkc51ydg8d16hr4PYLgtyakWjLX9Yn1es6sCwQGsqe2vCMlC1OQOnydH5FblBs97KeZvNfzO9tqA9Usx07/YbQ/DgNMIdx4Y6zWGVqwnnZEAa8hQf7wYQpoHkIQjCHO18y2xsc+eE/WLgXaBldVQoa9AqG+xxa35zyzseWVlMKRP/GHljtwIQ7X+9kt4MVDe15k87acVRZRwXG/jQNuJhZYlGoYEhgE9+0Wv4zBg3oHIHkayraXHcfbcLtJc/zY5r7vEx0oJuMmFXlKvA7DZMFL7GXz3LAVmFW734rw5jaRSHAacNu+MHBABth2AMFlE7yu6Oyh/p5xDFsSn6r4dTcg3tYaUu/S268h/R4FnD8m5xgaz3dbh4mh67afc9rPPNwtLgY1xVjIdAfXddoOkANw+BbAtouQAjMeRewKkbCGNDqKq6XwGeapEm/xL61rzhIT3ku+01aVviuWUznYBLImFLRRVGpB0CuBrv9CUSGkbZdKOragemIADYH9a4NstRpwCzEItIdEagG4f2Zr3a+eN2nRfcsTDzPgdv0CeXuWbZeZNRU1CG5/K7tyBcjFQ3UxPIwDwCCYAaQ7Abj+SHJq9tRIJM61o7A4gtggoFJLYF/Ftq2+RPngB0XRTAFJTpa+ldwp0lmUkwx6SGTHL9QEssLJmqMoXbTjk8tz2c4TAa/X97WcOCXLaKZzh4Kz2s4t6+oop/aSzLQa+9uORCECjIRH213+kqM+ev8MeF6ASdwHM4dMWunKMk2Lq0/Hi+0vdRCBu0n6SrXfaJLL99DpAyvsBSDFG2hTVZ+tJ2DXN2348AgGIcgChIpASri3lKfZxIC3QCMZqBMOAJBT4pMvwGA5ACRFEBRxAi9pBsP0CAuZxAFQG9Zr20/oAp14EtBMuaUiPyCK1NQLAvvYbthgwUA0gQtUsESjGoE0zG7l5aLIRZGrKPbNnu15VzfZja8tsMdfsqTrYtP04ECGVUAz0En8k7cf17Sev8LTdmjbSI7afLOGUpABEjQJ4Ok/IAVh2s5qU4nEFRhEw3f0AyA4kbNtD7fp40nhxuQifPHwvdh1ueRrYYxrYQ34W2k8pZy8HY7kw++o8IxgGLBycp/3K2oDdJhTbfgEdT3MiUF8OgMuOZg5JcaD4ZxeUUp9IMjXnAEitKo0KaDtvkJQSSA6gZQ2JPIG91gFo7RfOm2tcHCc+kcszv5/s9nOuz8jWyE3YAYxnPwCebmc/97apAei7xDiRX3hcvd4zpVQfUkq2CKhFENEC2I7KGiYqH7OHsdQ9WsS4OK5xch7CUzFVWygCUurzOLRhXNLIXq6PkKw9Wwsq2GqYJu3nHttOwAHwM9mPB1v1LOc8sBhZ7A5GVn8j8VriKdcA3Pl9VBKgfquY8CW0rcklyyC/1znEXcnJjaE85w2nPozLci2wUYiObJg4B/BMCg7gKUMAmowCKCXPAnSOa8VB1gqCbDVOytFfEzMiFqw6CRFTmrJXt9WONb10SAeglDDm7znmGealxFMAsqZDVQs5PcW/Vu0X7QCeWrkA0N3/doJ/8NnHjTQg+L1rqshRPZIDkSfh1WTO5bD1WB4bspc8QYS7vKBZ+1WUGUn7Gd+7zoktof3Mt27T3S+c6Hqq+tkSjKePZj8fj+sDbSKIkAqwtgxVe55SXw1Y5P/GQhB7QVAoia8L7bQcEVhC+4lLhcdQxGXlX+ijtR/13X7mWx9NowiYf697Tj3B37/rd7O7h+M2g4zfIcg/hiy9Nu0i0nyIT5raZ24QosQdfmN2ogl3LurNOMQvATf+U5IuG6Zf1HQERUDj4q7alZlIv/Kz/rzcfuSd9x/1v5+ie/79iWQEYNFoD2U/T85SgYgIYu8Tz0K00IcF5byxKiqR9/nEagBa0c8sAFYZApG1t39RN9AqyiTsQdFbetCx/ci6MrBxHQB7RSybtjrl9iPW+pqm4/acC+kxKRY2d9Ffz4EpQAa2s9tDff1PvQkA3fsfZ/jZz9yfNeg/heoBdVFE31RSDgR1OUbKcwHMmWQk/j++awaS55p+rSLI0lyA6QDrRwXc6wSuX/tVuzh1br+M/HQ/3fvimeQEYC4CL57i7/3ykUwEnszTAandvUtJpaX9zsw/JVaWafFYv75kqhaSyihI8vivOEYckzeyWrrDF1YAOnM5OrSfvIgozRpA0X6l9ee69vNFfqF95eY+lT3xEN330pk+/6PerwtA9/1n/gWPTP/lU9/Ifv9R6QbEKat1oyLstUvG0ni2ri6bcgSxL/BJlUUMXQ9QKhGZW3BTr3XANirh9lv5qsa+9psbndR3dPJvjKpvdOrbgp28n0shy//o5HM/fmIZ/9HSLgyy+MJPTL9758PZP/hAdv9o9n/ulXJiPRKwFTFsm8Wh8eXEVwMSV3PJq/FuEjuYN6q0KP714QDEab7inA0l1G/YbDvv/IC0VwPOawC+9mMxslf5v79GILRfTvpnsuNPTT7/8okl/0/dTjZReld0+9U//n6SPeiHf3hP+pe/Q/uNqv268heXBgPQfhuMCU4BAGwucHVgAO0HBwAAABwAIgiA9oMAjL7/oF3RfsAGOwB0IbQfgBQAQPsBYRDjZAPAxgKjAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAAAIAAAAEAAAACAAAABAAAAAgAAAAQAAAAIAAAAAAAQAAQML/A8vIw73ziok+AAAAAElFTkSuQmCC";
    }

    public class fileReturnData
    {
        public string MaskName { get; set; }
        public bool IsRootFolder { get; set; }
        public bool CanModifyFolder { get; set; }
        public string ParentPath { get; set; }
        public bool IsAdminSharedFolder { get; set; }
        public string Name { get; set; }
        public String Base64 { get; set; }
        public string Mime { get; set; }
        public String Icon { get; set; }

        public Type Type { get; set; } = Type.File;
        public string Path { get; set; }
        public string Alias { get; set; }
        public string FilePath { get; set; }

        public List<fileReturnData> children { get; set; } = new List<fileReturnData>();
    }

}