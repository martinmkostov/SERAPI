﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileManager.aspx.cs" Inherits="SER.FileManager.FileManager" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.FileUltimate" Assembly="GleamTech.FileUltimate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
         <div style="width: 100%;height: 100%;position: absolute;top:0;bottom: 0;left: 0;right: 0;margin: auto;">
             <GleamTech:FileManager OnRenaming="fileManager_Renaming" OnRenamed="fileManager_Renamed" OnCreated="fileManager_Created" OnUploaded="fileManager_Uploaded" OnDeleting="fileManager_Deleting" OnDeleted="fileManager_Deleted" OnCopied="fileManager_Copied" OnMoved="fileManager_Moved" ClientLoaded="fileManagerLoaded"
                            ID="fileManager" 
                            runat="server"
                            Width="100%"
                            Height="100%"
                            Resizable="false" >

            </GleamTech:FileManager>

        </div>

        <%--<div class="modal fade" id="rightsmodule" tabindex="-1" role="dialog">
            <div class="modal-dialog" id="settingsdialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Folder Rights</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>

                    <div class="modal-body">
                        <span style="background-color:#CCE8FF;padding:3px;" id="inlabel"></span> <span style="background-color:#CCE8FF;padding:3px;" id="folderlabel"></span>
                    
                        <br />
                        <br />

                        <span id="permission-load-indicator" class="d-none">Fetching Folder Permissions<br /></span>
                        <div id="permission-list">
                            <asp:Repeater runat="server" ID="userRepeater" OnItemDataBound="userRepeaterItemDataBound">
                                <ItemTemplate>
                                    <div id="rowid" belongs-to-site="0" user-id="0" runat="server" style="margin-bottom:4px;">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><asp:Literal ID="UserNameLit" runat="server"></asp:Literal></span>
                                            </div>

                                            <asp:DropDownList ID="roleDropdown" runat="server" onchange="setRole(this)" CssClass="form-control roles">
					                        </asp:DropDownList>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        
                        <br />

                        <p class="mb-0">If you set permission to an user below this will override his given role permission. Empty = permission based on role permission</p>
                    </div>

                    <div id="myfooter" class="modal-footer">
                        <div class="row no-gutters w-100">
                            <div class="col-sm-6">
                                Set all to:<br />

                                <div class="input-group input-group-sm">
                                    <asp:DropDownList ID="setalldropdown" runat="server" CssClass="form-control roles">
					                </asp:DropDownList>

                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="setAllRole()">Set</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 d-flex align-items-end justify-content-end">
                                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal" onclick="applyToSub()">Copy Roles to Subfolders</button>
                                &nbsp;
                                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </form>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>

    <style>
        /*.roles > option {
            display: none;
        }*/
    </style>

    <script type="text/javascript" src="Scripts/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        selectedFolder = "";
        selectedSite = "";

        String.prototype.replaceAll = function (str1, str2, ignore) {
			return this.replace(new RegExp(str1.replace(/([\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, function (c) { return "\\" + c; }), "g" + (ignore ? "i" : "")), str2);
		};

        function fileManagerLoaded(sender, e) {
            let fm = sender;

            //temporarily hide setting for folder rights
<%--        <% 
        if (RoleName == "admin")
        {
        %>
            if (fm.ribbon != null) {
                fm.ribbon.addTab({
                    title: "Folder Rights",
                    items: [
                        {
                            title: "Rights group",
                            items: [
                                {
                                    text: "Set rights...",
                                    iconCls: "", //css class for image
								    icon: "", //or url to the image
                                    scale: "small", //or medium or large
                                    handler: function () {
                                        if (fm.navigationSelection.data.depth == 1) {
                                            toastr.warning('You can not set rights on the root folder.');
                                        }
                                        else if (fm.navigationSelection.getPathData().rootFolderName.toString().search('admin') == -1) {
                                            toastr.warning('You have to be an admin on the site to set folder rights.');
                                        }
                                        else {
                                            showFolderRights(fm, '', 'File Folder');
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                });
            }

            Ext.override(fm, {
                createViewItemContextMenu: function () {
                    var me = this;

                    var menu = this.callParent();
                        menu.add([// or menu.insert(0, [
                            {
                                text: "Set Rights",
                                handler: function () {
                                    console.log(me);

                                    showFolderRights(fm, `\\\\${me.contextMenuSelection.data.name}`, me.contextMenuSelection.data.type);
                                }
                            }
                        ]);

                    return menu;
                }
            });
        <%
        }
        %>--%>
        }
        
        function showFolderRights(fm, file, type) {
            if (type !== "File Folder") {
                $('.roles>option[value=6]').css('display', 'none');
            } else {
                $('.roles>option[value=6]').css('display', 'block');
            }

            $('#permission-load-indicator').toggleClass('d-none');
            $('#permission-list').toggleClass('d-none');

            var site = Sites.find(s => s[0].find(e => e === fm.navigationSelection.getPathData().rootFolderName) != null);
            var siteId = site[1];
            var folder = fm.navigationSelection.getPathData().relativePath.replaceAll('\\', '\\\\');

            console.log(site);
            
            if (fm.navigationSelection.data.depth == 1) {
                folder = file;
            } else {
                folder = `${folder}${file}`;
            }

            $("#folderlabel").text(folder.replaceAll('\\\\', '\\'));
			$("#inlabel").text(fm.navigationSelection.getPathData().rootFolderName);

            $("#rightsmodule").modal();
                                        
            let userRoleContainers = document.querySelectorAll('div[belongs-to-site]');

            userRoleContainers.forEach(userRoleContainer => {
                if (userRoleContainer.getAttribute("belongs-to-site") == siteId) {
                    userRoleContainer.classList.remove("d-none");

                    let dpRole = userRoleContainer.querySelector('select');
                        dpRole.value = "-1";
                } else {
                    userRoleContainer.classList.add("d-none");
                }
            });

            let datac = `{siteId: ${siteId}, folder: '${folder}'}`;
                                        
            $.ajax({
				type: "POST",
				url: "FileManager.aspx/FolderSetting",
				data: datac,
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
				},
                success: function (result) {
                    $('#permission-load-indicator').toggleClass('d-none');
                    $('#permission-list').toggleClass('d-none');

                    result.d.forEach(role => {
                        let userRoleContainer = document.querySelector(`div[belongs-to-site='${role.SiteId}'][user-id='${role.UserId}']`);

                        if (userRoleContainer != null) {
                            let dpRole = userRoleContainer.querySelector('select');
                                dpRole.value = role.RoleId;
                        }
                    });
				}
            });

            selectedFolder = folder;
            selectedSite = siteId;
        }

        function setRole(el) {
            let roleId = $(el).val();
            
            setRoleId(el, roleId);
        }

        function setRoleId(el, roleId) {
            let siteId = $(el).parent().parent().attr('belongs-to-site');
            let userId = $(el).parent().parent().attr('user-id');

            let datac = `{siteId: ${siteId}, folder: '${selectedFolder}', roleId: ${roleId}, userId: ${userId}}`;

            $.ajax({
				type: "POST",
				url: "FileManager.aspx/SaveFolderSetting",
				data: datac,
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
				},
				success: function (result) {
                    if (result) {
                        toastr.success('Folder setting saved successfuly');
                    } else {
                        toastr.error('There was a problem updating folder setting.');
                    }
				}
			});
        }

         function applyToSubfolders(el, roleId) {
            let siteId = $(el).parent().parent().attr('belongs-to-site');
            let userId = $(el).parent().parent().attr('user-id');

            let datac = `{siteId: ${siteId}, folder: '${selectedFolder}', roleId: ${roleId}, applyToSubfolders: ${true},userId: ${userId}}`;

            $.ajax({
				type: "POST",
				url: "FileManager.aspx/ApplyToSubFolders",
				data: datac,
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
				},
				success: function (result) {
                    if (result) {
                        toastr.success('Folder setting saved successfuly');
                    } else {
                        toastr.error('There was a problem updating folder setting.');
                    }
				}
			});
        }

        <%--function setAllRole() {
            let roleId = document.querySelector('#<%=setalldropdown.ClientID%>').value;
            let userRoleContainers = document.querySelectorAll(`div[belongs-to-site='${selectedSite}']`);

            userRoleContainers.forEach(userRoleContainer => {
                let el = userRoleContainer.querySelector('select');

                setRoleId(el, roleId);
            });
        }

          function applyToSub() {
            let roleId = document.querySelector('#<%=setalldropdown.ClientID%>').value;
            let userRoleContainers = document.querySelectorAll(`div[belongs-to-site='${selectedSite}']`);

            userRoleContainers.forEach(userRoleContainer => {
                let el = userRoleContainer.querySelector('select');

                applyToSubfolders(el, roleId);
            });
        }--%>
    </script>
</body>
</html>
