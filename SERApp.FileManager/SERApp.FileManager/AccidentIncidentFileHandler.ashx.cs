﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for AccidentIncidentFileHandler
    /// </summary>
    public class AccidentIncidentFileHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try { 
            var AccountId = context.Request.Form["AccountId"];
            //var SiteId = context.Request.Form["SiteId"];
            var IncidentAccidentId = context.Request.Form["IncidentAccidentId"];
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", AccountId, "INCIDENT-ACCIDENT", IncidentAccidentId);
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;

            //  var files = Directory.GetFiles(fileManagerRoot);

            var dir = new DirectoryInfo(fileManagerRoot);
            var files = new List<fileReturnData>();
            var html = new StringBuilder();
            foreach (var f in dir.GetFiles())
            {
                //ConvertLargeFile(f.FullName, "test");


                Byte[] bytes = File.ReadAllBytes(f.FullName);
                String file = Convert.ToBase64String(bytes);

                var mime = MimeMapping.GetMimeMapping(f.Name);

                var theIcon = IconFromFilePath(f.FullName); //f.FullName - full path to file
                byte[] imageArray = ImageToByte(FromIconToBitmap(theIcon));
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                files.Add(new fileReturnData()
                {
                    Base64 = file,
                    Name = f.Name,
                    Mime = mime,
                    Icon = FileHelper.GetIconFromMimeType(mime)// $"data:image/png;base64,{base64ImageRepresentation}",
                });
                //  html.Append($"<a download='{f.Name}' target='_blank' href=\"data:{mime};base64," + file + "\">");
                // html.Append(f.Name.ToString() + "</a><br />");
            }

            //context.Response.ContentType = "text/html";
            context.Response.ContentType = "application/json";
            var json = JsonConvert.SerializeObject(files);


            var tt = html.ToString();
            context.Response.Write(json);
            }
            catch { 
            
            }
        }

        public Icon IconFromFilePath(string filePath)
        {
            var result = (Icon)null;

            try
            {
                result = Icon.ExtractAssociatedIcon(filePath);
            }
            catch (System.Exception)
            {
            }

            return result;
        }

        public Bitmap FromIconToBitmap(Icon icon)
        {
            Bitmap bmp = new Bitmap(icon.Width, icon.Height);
            using (Graphics gp = Graphics.FromImage(bmp))
            {
                gp.Clear(Color.Transparent);
                gp.DrawIcon(icon, new Rectangle(0, 0, icon.Width, icon.Height));
            }
            return bmp;
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class fileReturnData
        {
            public string Name { get; set; }
            public String Base64 { get; set; }
            public string Mime { get; set; }
            public String Icon { get; set; }
        }
    }
}