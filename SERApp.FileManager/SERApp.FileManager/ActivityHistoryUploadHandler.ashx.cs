﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using SER.FileManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for ActivityHistoryUploadHandler
    /// </summary>
    public class ActivityHistoryUploadHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int i = 0;
            var files = context.Request.Files["uploadedFiles"];
            var pathCount = context.Request.Form["PathCount"];
            var accountId = context.Request.Form["AccountId"];
            var Module = context.Request.Form["Module"];
            var CurrentPath = context.Request.Form["CurrentPath"];

            var repo = new SiteRepository();

     

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            //string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", AccountId, Module, SiteId, Id);

            string rootFolderPath = "";
            string fileManagerRoot = "";

            rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", accountId, "ACTIVITY", CurrentPath);

            fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;

            var IsMultipleConfirm = context.Request.Form["IsMultipleConfirm"];
            if (!string.IsNullOrEmpty(IsMultipleConfirm))
            {
                var SiteTaskLogId = context.Request.Form["SiteTaskLogId"];

                var findPath = fileManagerRoot.Remove(fileManagerRoot.IndexOf("Confirms") + 8, fileManagerRoot.Length - (fileManagerRoot.IndexOf("Confirms") + 8));

                var parentPathDirectories = Directory.GetDirectories(findPath);

                if (parentPathDirectories.Any(x => x.Contains($"_confirm_{SiteTaskLogId}")))
                {
                    var newPath = parentPathDirectories.Where(x => x.Contains($"_confirm_{SiteTaskLogId}"));
                    fileManagerRoot = newPath.FirstOrDefault();
                    //goto retryFind;
                }
            }

            var httpRequest = HttpContext.Current.Request;
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[i];
                var filePath = Path.Combine(fileManagerRoot, postedFile.FileName);// HttpContext.Current.Server.MapPath("~/UploadedFiles/" + postedFile.FileName);
                try
                {
                    var d = System.IO.Path.GetFileName(filePath);

                    var test = System.IO.Path.GetFullPath(filePath);
                    var g = filePath.Replace(d, "");
                    if (!System.IO.Directory.Exists(g))
                    {
                        Directory.CreateDirectory(g);
                    }


                    postedFile.SaveAs(filePath);

                 

                 }
                catch (Exception ex)
                {
                    throw ex;
                }

                i++;
            }


            var dir = new DirectoryInfo(fileManagerRoot);
            var filesadd = new List<fileReturnData>();
            var html = new StringBuilder();



            foreach (var f in dir.GetFiles())
            {
                //ConvertLargeFile(f.FullName, "test");


                Byte[] bytes = File.ReadAllBytes(f.FullName);
                String file = Convert.ToBase64String(bytes);

                var mime = MimeMapping.GetMimeMapping(f.Name);

                var theIcon = IconFromFilePath(f.FullName); //f.FullName - full path to file
                byte[] imageArray = ImageToByte(FromIconToBitmap(theIcon));
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

              

                filesadd.Add(new fileReturnData()
                {
                    FilePath = f.FullName,
                    //Base64 = file,
                    Name = f.Name,
                    Mime = mime,
                    Icon = FileHelper.GetIconFromMimeType(mime)//icon,
                });

                //  html.Append($"<a download='{f.Name}' target='_blank' href=\"data:{mime};base64," + file + "\">");
                // html.Append(f.Name.ToString() + "</a><br />");
            }

            //context.Response.ContentType = "text/html";
            context.Response.ContentType = "application/json";

            if (filesadd.Any())
            {
                filesadd = filesadd.OrderBy(x => x.Name).ToList();
            }
            var obj = new { CurrentPath = CurrentPath, Files = filesadd, IsAdminSharedFolder = "" };
            var json = JsonConvert.SerializeObject(obj);
            var tt = html.ToString();
            context.Response.Write(json);


            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public Icon IconFromFilePath(string filePath)
        {
            var result = (Icon)null;

            try
            {
                result = Icon.ExtractAssociatedIcon(filePath);
            }
            catch (System.Exception)
            {
            }

            return result;
        }

        public Bitmap FromIconToBitmap(Icon icon)
        {
            Bitmap bmp = new Bitmap(icon.Width, icon.Height);
            using (Graphics gp = Graphics.FromImage(bmp))
            {
                gp.Clear(Color.Transparent);
                gp.DrawIcon(icon, new Rectangle(0, 0, icon.Width, icon.Height));
            }
            return bmp;
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private class fileReturnData
        {
            public bool IsAdminSharedFolder { get; set; }
            public string Name { get; set; }
            public String Base64 { get; set; }
            public string Mime { get; set; }
            public String Icon { get; set; }

            public Type Type { get; set; } = Type.File;
            public string Path { get; set; }
            public string ParentPath { get; set; }
            public string Alias { get; set; }
            public string FilePath { get; set; }
        }
    }
}