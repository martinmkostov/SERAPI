﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;

public class Handler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        try {

            var files = context.Request.Files["uploadedFiles"];

            int i = 0;
            int cntSuccess = 0;
            var uploadedFileNames = new List<string>();
            string result = string.Empty;

            //HttpResponseMessage response = new HttpResponseMessage();
            var AccountId = context.Request.Form["AccountId"];
            var TaskId = context.Request.Form["TaskId"];
            var SiteId = context.Request.Form["SiteId"];
            var ActionFolder = context.Request.Form["ActionFolder"];

            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", AccountId, "activity", TaskId, SiteId, "Confirms", ActionFolder);
            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName + @"\" + rootFolderPath;


            if (!Directory.Exists(fileManagerRoot))
            {
                Directory.CreateDirectory(fileManagerRoot);
            }

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[i];
                    var filePath = Path.Combine(fileManagerRoot, postedFile.FileName);// HttpContext.Current.Server.MapPath("~/UploadedFiles/" + postedFile.FileName);
                    try
                    {

                        //if (File.Exists(filePath))
                        //{
                        //    replaceFilePath:
                        //    var newName = Path.GetFileNameWithoutExtension(postedFile.FileName);
                        //    replaceFilePath2:
                        //    var resulta = newName.Substring(newName.Length - 3);

                        //    if (resulta.Contains("(") && resulta.Contains(")"))
                        //    {
                        //        var resultb = resulta[1].ToString();
                        //        Int32.TryParse(resultb, out int newNumber);

                        //        var resultc = newName.Replace(resulta, $"({(newNumber + 1).ToString()})") + Path.GetExtension(postedFile.FileName);

                        //        filePath = Path.Combine(fileManagerRoot, resultc);

                        //        if (File.Exists(filePath))
                        //        {
                        //            goto replaceFilePath;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        var n = newName + "(1)" + Path.GetExtension(postedFile.FileName);
                        //        filePath = Path.Combine(fileManagerRoot, n);

                        //        if (File.Exists(filePath))
                        //        {
                        //            newName = Path.GetFileNameWithoutExtension(n);
                        //            goto replaceFilePath2;
                        //        }

                        //    }

                        //}
                        postedFile.SaveAs(filePath);
                        uploadedFileNames.Add(httpRequest.Files[i].FileName);
                        cntSuccess++;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    i++;
                }
            }
            var json = JsonConvert.SerializeObject(new { Success = true });
            context.Response.Write(json);
        } catch (Exception ex) 
        {
            throw ex;
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}