﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SER.FileManager
{
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var Path = context.Request.Form["Path"];
            Byte[] bytes = File.ReadAllBytes(Path);
            String file = Convert.ToBase64String(bytes);

            context.Response.ContentType = "application/json";
          
            var obj = new { File= file };
            var json = JsonConvert.SerializeObject(obj);

            context.Response.Write(json);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}