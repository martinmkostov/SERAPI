﻿using Newtonsoft.Json;
using SER.FileManager.Helper;
using SER.FileManager.Models;
using SER.FileManager.Repository;
using SERApp.Models.Common;
using SERApp.Models.Enums;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SER.FileManager.Controllers
{
    public class FileController : ApiController
    {
        FileRepository _fileRepository = new FileRepository();
        ModuleRepository _moduleRepository = new ModuleRepository();
        ContractRepository _contractRepository = new ContractRepository();

        private readonly SiteService _site = new SiteService();

        [HttpPost]
        [Route("api/download")]
        public HttpResponseMessage Download([FromBody] DownloadFileModel model)
        {
            var file = File.OpenRead(model.Path);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(file);
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(model.Path)
            };

            var mime = MimeMapping.GetMimeMapping(Path.GetFileName(model.Path));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(mime);
            response.Headers.Add("FileName", Path.GetFileName(model.Path));
            return response;
        }

        [HttpPost]
        [Route("MultiUpload")]
        public void MultiUpload()
        {
            if (HttpContext.Current.Request.Files["chunk"] == null)
            {
                return;
            }
            var chunkedFilesFolderName = HttpContext.Current.Request["chunkedFilesFolderName"];
            var prefixFileName = HttpContext.Current.Request["prefixFileName"];
            var file = HttpContext.Current.Request.Files["chunk"].InputStream;


            string tempParentPath = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Temp");
            tempParentPath = Path.Combine(tempParentPath, chunkedFilesFolderName);

            Directory.CreateDirectory(tempParentPath);


            string chunkedFileFileName = Path.Combine(tempParentPath, $"{prefixFileName}_{Path.GetRandomFileName()}");

            using (FileStream fs = File.Create(chunkedFileFileName))
            {
                byte[] bytes = new byte[90000000];

                int bytesRead;
                while ((bytesRead = file.Read(bytes, 0, bytes.Length)) > 0)
                {
                    fs.Write(bytes, 0, bytesRead);
                }
            }
        }

        [HttpPost]
        [Route("CheckDuplicate")]
        public bool CheckDuplicate()
        {
            var context = HttpContext.Current;
            var folderPath = context.Request.Form["folderPath"];
            string fileName = context.Request["fileName"];
            long fileSize = Convert.ToInt64(context.Request["fileSize"]);

            if (File.Exists(Path.Combine(folderPath, fileName)))
            {
                var getFiles = Directory.GetFiles(folderPath).Where(xx => xx.Substring(folderPath.Length + 1) == fileName).ToList();

                foreach (var dir in getFiles)
                {
                    var file = new FileInfo(dir);
                    if (file.Length == fileSize)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        [HttpPost]
        [Route("DeleteFile")]
        public bool DeleteFile()
        {
            var context = HttpContext.Current;
            var repo = new SiteRepository();

            var filePath = context.Request["filePath"];
            var fileName = context.Request["fileName"];
            int siteId = Convert.ToInt32(context.Request["siteId"]);
            string savedPath = context.Request["savedPath"];

            savedPath = Path.Combine("\\", savedPath, fileName.Replace("\\", ""));

            filePath = Path.Combine(filePath, fileName);

            repo.DeleteSiteFolderRole(siteId, savedPath);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            return true;
        }

        [HttpPost]
        [Route("DeleteUploadedChunks")]
        public void DeleteUploadedChunks()
        {
            var context = HttpContext.Current;
            var chunkedFilesFolderName = context.Request["chunkedFilesFolderName"];
            string chunkedFilesFilePath = context.Server.MapPath("~/App_Data/Uploads/Temp");
            chunkedFilesFilePath = Path.Combine(chunkedFilesFilePath, chunkedFilesFolderName);

            while (true)
            {
                var files = Directory.GetFiles(chunkedFilesFilePath);
                if (files.Count() > 0)
                {
                    foreach (var file in files)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception)
                        {
                            //possible issue for the api to crash is the file to be deleted is currently being used by another process
                            // stop the loop for deleting files
                            break;
                        }
                    }
                    //check again if the chunkedfilesfilepath still contains files
                    continue;
                }
                else
                {
                    //stop the infinite loop if chunkedfilesfilepath contains no files anymore
                    break;
                }
            }
            Directory.Delete(chunkedFilesFilePath);
        }

        [HttpPost]
        [Route("DeleteMergedChunks")]
        public void DeleteMergedChunks()
        {
            var repo = new SiteRepository();
            var context = HttpContext.Current;

            string fileName = context.Request["fileName"];
            string finalFilePath = context.Request["localpath"];
            int siteId = Convert.ToInt32(context.Request["siteId"]);
            string savedPath = context.Request["savedPath"];

            finalFilePath = Path.Combine(finalFilePath, fileName);

            savedPath = Path.Combine("\\", savedPath, fileName.Replace("\\", ""));

            while (true)
            {
                if (File.Exists(finalFilePath))
                {
                    File.Delete(finalFilePath);
                    repo.DeleteSiteFolderRole(siteId, savedPath);
                    break;
                }
            }
        }

        [HttpPost]
        [Route("MergeFiles")]
        public DataResponseModel MergeFiles()
        {
            var siteRepo = new SiteRepository();
            var userRepo = new UserRepository();
            var context = HttpContext.Current;

            var userId = Convert.ToInt32(context.Request.Form["userId"]);
            var siteId = Convert.ToInt32(context.Request.Form["siteId"]);
            var moduleId = Convert.ToInt32(context.Request.Form["moduleId"]);
            var pathName = context.Request.Form["pathName"];
            string fileName = context.Request["fileName"];
            string finalFilePath = context.Request["path"];
            string chunkedFilesFolderName = context.Request["chunkedFilesFolderName"];

            var user = userRepo.GetUser(userId);

            string chunkedFilesFilePath = context.Server.MapPath("~/App_Data/Uploads/Temp");
            chunkedFilesFilePath = Path.Combine(chunkedFilesFilePath, chunkedFilesFolderName);

            try
            {
                //merged file
                string newpath = Path.Combine(chunkedFilesFilePath, fileName);

                DirectoryInfo info = new DirectoryInfo(chunkedFilesFilePath);
                FileInfo[] chunkedFiles = info.GetFiles().OrderBy(p => p.Name).ToArray();

                foreach (var item in chunkedFiles)
                {
                    MergeFiles(newpath, item.FullName);
                }


                var files = new List<fileReturnData>();

                var fileInfo = new FileInfo(Path.Combine(finalFilePath, fileName));

                var mime = MimeMapping.GetMimeMapping(fileInfo.Name);

                files.Add(new fileReturnData()
                {
                    ParentPath = finalFilePath,
                    FilePath = fileInfo.FullName,
                    Name = fileInfo.Name,
                    Mime = mime,
                    Icon = FileHelper.GetIconFromMimeType(mime)
                });

                if (!user.IsAdmin)
                {
                    SaveFolderSetting(siteId, pathName, 1, userId);
                }
                else
                {
                    var users = userRepo.GetUsersForModule(moduleId, user.AccountId).ToList();

                    Parallel.ForEach(users, usr =>
                    {
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            SaveFolderSetting(siteId, pathName, -1, userId);
                        }
                    });
                }

                Directory.Move(newpath, Path.Combine(finalFilePath, fileName));
                Directory.Delete(chunkedFilesFilePath);

                return new DataResponseModel()
                {
                    files = files,
                    message = "File saved successfully"
                };
            }
            catch (Exception e)
            {
                return new DataResponseModel()
                {
                    files = null,
                    message = e.Message
                };
            }
        }

        [HttpPost]
        [Route("SendFiles")]
        public DataResponseModel SendFiles(Models.EmailModel emailModel)
        {
            try
            {
                var message = "Email sent succesfully.";
                var cc = _contractRepository.GetEmailCC(emailModel.AccountId);
                if (cc != emailModel.To)
                {
                    emailModel.CC = _contractRepository.GetEmailCC(emailModel.AccountId);
                }
                var result = _fileRepository.SendFiles(emailModel);

                if (!result)
                {
                    message = "Unable to send email.";
                }
                else
                {
                    _contractRepository.SaveEmailRecipient(
                        new SaveEmailRecipient
                        {
                            AccountId = emailModel.AccountId,
                            ModuleId = emailModel.ModuleId,
                            Recipient = emailModel.To
                        }
                    );
                }

                return new DataResponseModel()
                {
                    success = result,
                    message = message
                };
            }
            catch(Exception e)
            {
                _moduleRepository.CreateLog(new LogModel()
                {
                    LogType = (int)LogTypeEnum.Error,
                    ShortDescription = "SENDEMAIL",
                    Message = e.Message,
                    AccountId = emailModel.AccountId,
                    UserId = emailModel.UserId
                });

                return new DataResponseModel()
                {
                    success = false,
                    message = e.Message
                };
            }
        }

        [HttpGet]
        [Route("Contract/Files/Export")]
        public HttpResponseMessage ExportFiles(string filters, string folderSuffix)
        {
            var parse = JsonConvert.DeserializeObject<ContractFilters>(filters);
            var zip = _contractRepository.DownloadFiles(parse, folderSuffix);
            var length = File.ReadAllBytes(zip).Length;
            var resp = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StreamContent(new FileStream(zip, FileMode.Open, FileAccess.Read, FileShare.Read))
            };
            resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "a.zip"
            };

            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/zip");
            resp.Content.Headers.ContentLength = length;

            return resp;
        }

        [HttpPost]
        [Route("Contract/Files/Delete")]
        public IHttpActionResult DeleteFiles(string fileName, string siteId, string accountId, string suffix)
        {
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{accountId}");

            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", ".."));
            fileManagerRoot = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT");

            _contractRepository.DeleteExportedFiles(fileManagerRoot, siteId, suffix, fileName);

            return Ok();
        }

        [HttpGet]
        [Route("Contract/Files/Progress")]
        public IHttpActionResult GetProgress(string siteName, string accountId)
        {
            var total = 0.0;
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{accountId}");

            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", ".."));

            string zipFolder = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT", siteName);

            if (Directory.Exists(zipFolder))
            {
                DirectoryInfo di = new DirectoryInfo(zipFolder);
                var directories = di.EnumerateFiles("*", SearchOption.AllDirectories);
                if (directories.Any())
                {
                    total += directories?.Sum(fi => fi.Length) ?? 0;
                }
            }
            return Ok(Math.Round(total / 1048576, 2));
        }

        [HttpGet]
        [Route("Contract/Files/Size")]
        public IHttpActionResult GetSize(string siteId, string accountId, string ids)
        {
            double size = 0;
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{accountId}");

            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", ".."));

            fileManagerRoot = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT", siteId);

            if (Directory.Exists(fileManagerRoot))
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    var listIds = ids.Split(',').ToList();
                    listIds.ForEach(id =>
                    {
                        var idPath = Path.Combine(fileManagerRoot, id);
                        if (Directory.Exists(idPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(idPath);
                            var directories = di.EnumerateFiles("*", SearchOption.AllDirectories);
                            if (directories.Any())
                            {
                                size += directories?.Sum(fi => fi.Length) ?? 0;
                            }
                        }
                    });
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(fileManagerRoot);
                    var directories = di.EnumerateFiles("*", SearchOption.AllDirectories);

                    if (directories.Any())
                    {
                        size = directories?.Sum(fi => fi.Length) ?? 0;
                    }
                }
            }

            var sizeInMb = Math.Round(size/1048576, 2);
            var time = sizeInMb == 0 ? 1: (sizeInMb * (3.78));
            return Ok(new { Size = sizeInMb, Time = Math.Round(time) });
        }

        [HttpGet]
        [Route("Contract/Files/ZipSize")]
        public IHttpActionResult GetZipSize(string siteId, string accountId)
        {
            string fileDirectoryRoot = ConfigurationManager.AppSettings["filesDirectory"];
            string rootFolderPath = Path.Combine(fileDirectoryRoot, "FILES", $"{accountId}");

            string fileManagerRoot = Directory.GetParent(HttpContext.Current.Server.MapPath("")).Parent.FullName;

            fileManagerRoot = Path.GetFullPath(Path.Combine(fileManagerRoot, "..", ".."));

            fileManagerRoot = Path.Combine(fileManagerRoot, rootFolderPath, "CONTRACT", $"{siteId}.zip");

            if (!File.Exists(fileManagerRoot)) return Ok(0);

            decimal file = new FileInfo(fileManagerRoot).Length;
            var sizeInMb = Math.Round(file / 1048576, 2);
            return Ok(sizeInMb);
        }

        private static void MergeFiles(string file1, string file2)
        {
            FileStream fs1 = null;
            FileStream fs2 = null;
            try
            {
                fs1 = File.Open(file1, FileMode.Append);
                fs2 = File.Open(file2, FileMode.Open);
                byte[] fs2Content = new byte[fs2.Length];
                fs2.Read(fs2Content, 0, (int)fs2.Length);
                fs1.Write(fs2Content, 0, (int)fs2.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " : " + ex.StackTrace);
            }
            finally
            {
                fs1.Close();
                fs2.Close();
                File.Delete(file2);

            }
        }

        private void SaveFolderSetting(int siteKey, string path, int roleId, int userId)
        {
            var repo = new SiteRepository();
            repo.CreateSiteFolderRole(new SiteFolderRole
            {
                SiteId = siteKey,
                Folder = path,
                RoleId = roleId,
                UserId = userId
            });
        }
    }
}
