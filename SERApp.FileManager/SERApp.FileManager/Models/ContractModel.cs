﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SER.FileManager.Models
{
    public class ContractModel
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}