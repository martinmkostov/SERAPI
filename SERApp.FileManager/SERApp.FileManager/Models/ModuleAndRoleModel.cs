﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SER.FileManager.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsModule { get; set; }
        public bool IsUser { get; set; }
    }

    public class ModuleModel {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string Title { get; set; }
    }

    public class UserModuleAndRoleModel {
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
        public string RoleName { get; set; }
        public string ModuleName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public bool SiteEnabled { get; set; }
    }
}
