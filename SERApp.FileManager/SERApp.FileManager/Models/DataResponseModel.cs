﻿using System;
using System.Collections.Generic;
using System.Web;

namespace SER.FileManager.Models
{
    public class DataResponseModel
    {
        public List<fileReturnData> files;
        public string message;
        public bool? success;
    }
}
