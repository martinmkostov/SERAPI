﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SER.FileManager.Models
{
    public class EmailModel
    {
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string To { get; set; }
        public List<string> AttachmentPaths { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public string CC { get; set; }
    }
}