﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SER.FileManager.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string HashedPassword { get; set; }
        public string RandomSecret { get; set; }
        public int AccountId { get; set; }
        public int DepartmentId { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public DateTime? LastLoginDateTime { get; set; }
        public bool IsAdmin { get; set; }
        public int SiteId { get; set; }
        public bool IsActivityAdmin { get; set; }
    }
}